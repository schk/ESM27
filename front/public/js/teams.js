// /**
//  * 打包部署修改
//  */
var baseUrl =
  window.location.protocol + '//' + window.location.host.replace(window.location.port, '') + '9090';
var baseFileUrl = 'http://39.100.153.242:9092';
/**
 * 绘制
 */
var map, plot;
var esmUtil = {};
var changeFeature = {};
var beforeFeature;
$(document).ready(function() {
  var loacltionCoordinate = parent.document.getElementById('loacltionCoordinate').value;
  var source = new ol.source.Vector({
    features: [],
  });
  var layer = new ol.layer.Vector({
    source: source,
    style: new ol.style.Style({
      // text : new ol.style.Text({
      // 	text : "123",
      // 	offsetY : 20,
      // 	font : 'bold 12px sans-serif',
      // 	fill : new ol.style.Fill({
      // 		color : '#84C1FF'
      // 	}),
      // 	stroke : new ol.style.Stroke({
      // 		color : '#ECF5FF',
      // 		width : 3
      // 	})
      // }),
      image: new ol.style.Icon({
        src: baseFileUrl + '/mapIcon/mapFixedIcon/zhongdian.png',
        anchor: [0.5, 0.5],
      }),
    }),
  });
  layer.setZIndex(100);

  map = new ol.Map({
    target: 'map',
    layers: [
      new ol.layer.Tile({
        source: new ol.source.BaiduMap(),
      }),
      layer,
    ],
    view: new ol.View({
      center: ol.proj.transform([104.094752, 30.667451], 'EPSG:4326', 'EPSG:3857'),
      zoom: 8,
    }),
  });

  /**
   * 此处的x,y值是传输过来的
   * */
  var x = 104.216309;
  var y = 30.614277;
  if (loacltionCoordinate) {
    x = parseFloat(loacltionCoordinate.split(',')[0]);
    y = parseFloat(loacltionCoordinate.split(',')[1]);
  }

  var feature = new ol.Feature({
    geometry: new ol.geom.Point(new ol.proj.fromLonLat([x, y])),
  });
  feature.setId('position');
  feature.set('isDrag', true);
  source.addFeature(feature);
  //定位坐标点
  map.getView().animate({ center: new ol.proj.fromLonLat([x, y]) });

  initPopup(map);

  //地图点击事件
  map.on('singleclick', onClickHandler);
  map.on('moveend', onMoveEndHandler);
  plot = new olPlot(map, {
    zoomToExtent: true,
    isClear: true,
  });

  plot.plotDraw.on('drawEnd', onDrawEnd);
  var params = {};
  params.oid = '123';
  params.name = '';
  params.clusterType = 'zddw';
  params.imagePath = baseFileUrl + '/mapIcon/mapFixedIcon/zhongdian.png';
  params.isCluster = true;
  params.clusterImagePath = baseFileUrl + '/mapIcon/mapFixedIcon/zhongdian.png';
  params.isSyn = false;
  params.isSave = false;
  params.recordType = 'getDataLocation';
  params.action = '获取坐标点';
  addMakerEngine(params);

  /**
   * 获取初始化地图数据(wzcbd,zddw,xfsy,yjdw,sczz,xfdw)等数据
   */
  var urls = baseUrl + '/api/geographicEdit/qryListAll.jhtml';
  var params = null;
  executeAjax(urls, params, function(data) {
    if (data.httpCode == 200) {
      if (data) {
        clusterDo(data);
      }
    } else {
      layer.msg(data.msg, { time: 1000 });
    }
  });
  //初始化获取道路列表
  getRoadList();
  //初始化场景数据(t_resource表)
  getResourceList();
});

function onMoveEndHandler(event) {
  //console.log("moveend");
}

/**
 * 获取初始化地图数据(wzcbd,zddw,xfsy,yjdw,sczz,xfdw)等数据
 */
function getClusterDataList() {
  var urls = baseUrl + '/api/geographicEdit/qryListAll.jhtml';
  var params = null;
  executeAjax(urls, params, function(data) {
    if (data.httpCode == 200) {
      if (data) {
        clusterDo(data);
      }
    } else {
      layer.msg(data.msg, { time: 1000 });
    }
  });
}

/**
 * 聚合接口
 * */
function clusterDo(data) {
  var fmap = new HashMap();
  var format = new ol.format.GeoJSON();
  var features = format.readFeatures(data.data);
  features.forEach(function(feature) {
    feature.set('cluster', true);
    var prop = feature.getProperties();
    var catecd = prop.catecd;
    if (fmap.containsKey(catecd)) {
      fmap.get(catecd).push(feature);
    } else {
      var fs = [];
      fs.push(feature);
      fmap.put(catecd, fs);
    }
  });
  var keys = fmap.keys();
  for (var i = 0; i < keys.length; i++) {
    var layerId = keys[i] + 'ClusterLayer';
    var layer = layerIsExist(layerId, map);
    if (!layer) {
      layer = createClusterLayer(layerId, keys[i]);
      map.addLayer(layer);
    }
    addFeatureToLayer(fmap.get(keys[i]), layer, true);
  }
}

/**
 * 获取道路列表
 * @returns
 */
function getRoadList() {
  var urls = baseUrl + '/api/geographicEdit/getRoadList.jhtml';
  var params = null;
  executeAjax(urls, params, function(data) {
    if (data.httpCode == 200) {
      if (data.data && data.data.length > 0) {
        for (j = 0, len = data.data.length; j < len; j++) {
          if (data.data[j].content && data.data[j].content != '') {
            var json = JSON.parse(data.data[j].content);
            var feature = plot.plotUtils.toFeature(json);
            addFeatureToMap(feature, data.data[j].id);
          }
        }
      }
    } else {
      layer.msg(data.msg, { time: 1000 });
    }
  });
}

/**
 * 初始化场景数据
 * @returns
 */
function getResourceList() {
  var urls = baseUrl + '/api/geographicEdit/queryResource.jhtml';
  var params = null;
  executeAjax(urls, params, function(data) {
    if (data.httpCode == 200) {
      if (data.data && data.data.length > 0) {
        var addtomap = [];
        for (j = 0, len = data.data.length; j < len; j++) {
          var d = data.data[j];
          if (d.position && d.position != '') {
            addtomap.push({ url: d.resourcePath, extent: d.position, id: d.id, imageId: d.id });
          }
        }
        addImagesToMap(addtomap);
      }
    } else {
      layer.msg(data.msg, { time: 1000 });
    }
  });
}
/*左侧点击道路定位并弹出详情*/
function gotoPointLocation(obj) {
  var json = JSON.parse(obj.content);
  var feature = plot.plotUtils.toFeature(json);
  addFeature(feature);
}

/**
 * 聚合接口
 * */
function clusterDo(data) {
  var fmap = new HashMap();
  var format = new ol.format.GeoJSON();
  var features = format.readFeatures(data.data);
  features.forEach(function(feature) {
    feature.set('cluster', true);
    var prop = feature.getProperties();
    var catecd = prop.catecd;
    if (fmap.containsKey(catecd)) {
      fmap.get(catecd).push(feature);
    } else {
      var fs = [];
      fs.push(feature);
      fmap.put(catecd, fs);
    }
  });
  var keys = fmap.keys();
  for (var i = 0; i < keys.length; i++) {
    var layerId = keys[i] + 'ClusterLayer';
    var layer = layerIsExist(layerId, map);
    if (!layer) {
      layer = createClusterLayer(layerId, keys[i]);
      map.addLayer(layer);
    }
    addFeatureToLayer(fmap.get(keys[i]), layer, true);
  }
}

/**
 * 图标点击事件
 * */
function onClickHandler(event) {
  feature = map.forEachFeatureAtPixel(event.pixel, function(feature) {
    if (feature.get('features')) {
      if (feature.get('features').length > 1) {
        var coordinate_ = feature.getGeometry().getCoordinates();
        map
          .getView()
          .animate(
            { zoom: map.getView().getZoom() + 1 },
            { center: coordinate_ },
            { duration: 1000 }
          );
        return null;
      } else {
        return feature.get('features')[0];
      }
    } else {
      return feature;
    }
  });
  if (feature && feature.get('isPlot') && !plot.plotDraw.isDrawing()) {
    plot.plotEdit.activate(feature);
    changeStyle(feature);
  } else {
    plot.plotEdit.deactivate();
  }

  var params = {};
  params.oid = '123';
  params.name = '';
  params.clusterType = 'zddw';
  params.imagePath = baseFileUrl + '/mapIcon/mapFixedIcon/zhongdian.png';
  params.isCluster = true;
  params.clusterImagePath = baseFileUrl + '/mapIcon/mapFixedIcon/zhongdian.png';
  params.isSyn = false;
  params.isSave = false;
  params.recordType = 'getDataLocation';
  params.action = '获取坐标点';
  addMakerEngine(params);

  /**
   * mark点击功能
   */
  if (feature && feature.get('cluster')) {
    return;
  }
}

/**
 * 第4个功能点
 * */
function changeStyle(feature) {
  changeFeature = feature;
  openInfoWindowByFeature(feature);
}

/**
 * 调用marker标绘
 * */
function addMakerEngine(params) {
  params.type = 'marker';
  activate('Point', params);
}

/**
 * 绘制功能结束的回调
 * */
function onDrawEnd(event) {
  var feature = event.feature;
  var params = feature.get('params');
  var fid = params.oid ? params.oid : createRandomId();
  feature.setId(createRandomId());
  feature.set('name', params.name);
  feature.set('scolor', '#ff00ff');
  feature.set('isDrag', true);
  var iconType = params.recordType;
  if (iconType == 'road') {
    var layer = layerIsExist('geographicEditLayer', map);
    if (!layer) {
      layer = createMarkLayer('geographicEditLayer');
      map.addLayer(layer);
    }
    feature.set('plotType', 'geographicEdit');
    layer.getSource().addFeature(feature);
    openInfoWindowByFeature(feature);
  } else if (iconType == 'getDataLocation') {
    var layerId = params.clusterType + 'MarkerClusterLayer';
    if (changeFeature.getId) {
      var fid = changeFeature.getId();
      removeFeatureById(layerId, fid);
    }
    var clayer = layerIsExist(layerId, map);
    if (!clayer) {
      clayer = createMakerClusterLayer(layerId, params.clusterType);
      map.addLayer(clayer);
    }
    featureIsExist(feature, clayer, true);
    feature.set('plotType', 'Marker');

    var lonlat3857 = feature.getGeometry().getCoordinates();
    var lon = ol.proj.transform(lonlat3857, 'EPSG:3857', 'EPSG:4326')[0].toFixed(6);
    var lat = ol.proj.transform(lonlat3857, 'EPSG:3857', 'EPSG:4326')[1].toFixed(6);
    var cLonLat = lon + ',' + lat;
    parent.document.getElementById('loacltionCoordinate').value = cLonLat;
  }

  changeFeature = feature;
  feature.scolor = '#000';
  plot.plotEdit.activate(feature);
}

/**
 * 标绘结束后的弹框
 * 根据feature 弹出弹出框
 *
 */
function openInfoWindowByFeature(feature) {
  var point = null;
  if (feature.getGeometry().getType() == 'Point') {
    point = feature.getGeometry().getCoordinates();
  } else if (feature.getGeometry().getType() == 'Polygon') {
    point = feature
      .getGeometry()
      .getInteriorPoint()
      .getCoordinates();
  } else if (feature.getGeometry().getType() == 'LineString') {
    point = feature.getGeometry().getFirstCoordinate();
  }
  var id = '#markerText';
  //type 为20的是需要输入是输入名字和数量，其他为需要输入数量
  var obj = feature.get('params');
  var value = obj.name ? obj.name : '';
  var desc = obj.desc ? obj.desc : '';
  var iconType = obj.recordType;
  var oid = obj.oid;
  var content = '';
  if (iconType == 'road') {
    content =
      '<div class="olmatter">' +
      '<div class="ca cf"><span class="fl olName">道路名称：</span><span class="fl olInput"><input id="roadName" type="text" value="' +
      value +
      '" /></span></div>' +
      '<div class="ca cf"><span class="fl olName">道路描述：</span><span class="fl olInput h40"><textarea name="roadDesc" class="textarea" id="roadDesc" type="text" rows="">' +
      desc +
      '</textarea></span></div>' +
      '</div>';
    /**
     * 弹出道路信息框
     */
    openPopup(point, content);
  }
}

/**
 * 初始化弹出窗口
 * */
function initPopup(map) {
  var element = document.createElement('div');
  element.className = 'ol-popup';
  element.id = 'popup';
  var child = '<a href="#" id="popup-closer" class="ol-popup-closer"></a>';
  child += '<div id="popup-content"></div>';
  element.innerHTML += child;
  var overlay = new ol.Overlay({
    id: 'overlay',
    element: element,
    offset: [0, 5],
    autoPan: true,
    positioning: 'top-center',
    autoPanAnimation: {
      duration: 250,
    },
  });
  map.addOverlay(overlay);
  $('#popup-closer').click(function(event) {
    close();
  });
}

/**
 * 窗口弹出
 * */
function openPopup(point, html) {
  setContent(html);
  setPopupPosition(point);
}

/**
 * 关闭弹出窗口
 * */
function close() {
  var overlay = map.getOverlayById('overlay');
  overlay.setPosition(undefined);
  $('#popup-closer').blur();
  $('#popup-content').html('');
  return false;
}

/**
 *
 * 根据ID删除要素
 * */
function removeFeatureById(layerType, id) {
  var layer = layerIsExist(layerType, map);
  if (layer) {
    var feature = layer
      .getSource()
      .getSource()
      .getFeatureById(id);
    if (feature) {
      layer
        .getSource()
        .getSource()
        .removeFeature(feature);
    }
  }
}

/**
 * 设置弹出内容
 * */
function setContent(html) {
  $('#popup-content').html(html);
}

/**
 * 在坐标点位置打开窗口
 * */
function setPopupPosition(coord) {
  var overlay = map.getOverlayById('overlay');
  overlay.setPosition(coord);
}

/**
 * 标注回显功能
 * */
function addFeatureToMap(feature, id) {
  feature.setId(id);
  var params = feature.get('params');
  feature.set('name', params.name);
  feature.set('desc', params.desc);
  addFeature(feature);
}

function buffer(geometry) {
  var parser = new jsts.io.OL3Parser();
  var jstsGeom = parser.read(geometry);
  var buffered = jstsGeom.buffer(40);
  var bufferGeom = parser.write(buffered);
  var feature = new ol.Feature({
    geometry: bufferGeom,
  });
  return feature;
}
/**
 * 激活绘制功能
 * */
function activate(type, params) {
  plot.plotEdit.deactivate();
  plot.plotDraw.active(type, params);
}

/**
 * 添加标绘要素
 * */
function addFeature(feature) {
  var params = feature.get('params');
  if (params) {
  }
  var layer = layerIsExist('geographicEditLayer', map);
  if (!layer) {
    layer = createMarkLayer('geographicEditLayer');
    map.addLayer(layer);
  }
  layer.getSource().addFeature(feature);
}

/**
 *
 * 根据ID删除要素
 * */
function removeFeature(id) {
  var layer = layerIsExist('geographicEditLayer', map);
  if (layer) {
    var feature = layer.getSource().getFeatureById(id);
    layer.getSource().removeFeature(feature);
    close();
  }
}
/**
 * 清空图层
 * */
function clearLayer() {
  clearLayerByIds(['plotLayer']);
}

function remove() {
  var feature = plot.plotEdit.getFeature();
  var layer = layerIsExist('plotLayer', map);
  if (layer) {
    layer.getSource().removeFeature(feature);
  }
  plot.plotEdit.deactivate();
}

/**
 * 显示场景搭建历史
 * */
function addImagesToMap(images) {
  var group = layerIsExist('imagesGroup', map);
  if (!group) {
    group = createGroupLayer('imagesGroup');
    map.addLayer(group);
  }
  for (var i = 0; i < images.length; i++) {
    var image = images[i];
    var layer = new ol.layer.Image({
      source: new ol.source.ImageStatic({
        url: baseFileUrl + image.url,
        crossOrigin: '',
        projection: 'EPSG:3857',
        imageExtent: image.extent.split(','),
      }),
    });
    if (image.id) {
      layer.set('id', image.id);
      removeImagesByid(image.id);
    }
    group.getLayers().push(layer);
  }
}

/**
 * 删除场景搭建历史
 * */
function removeImagesByid(id) {
  var layer;
  var group = layerIsExist('imagesGroup', map);
  group.getLayers().forEach(function(lay) {
    if (lay.get('id') == id) {
      layer = lay;
    }
  });
  if (layer) {
    group.getLayers().remove(layer);
  }
}

function clearImagesGroup() {
  var group = layerIsExist('imagesGroup', map);
  if (group) {
    map.removeLayer(group);
  }
}

/**
 * 定位
 * */
function centerAndZoom(point, zoom) {
  if (zoom) {
    map.getView().animate({ center: point }, { zoom: zoom });
  } else {
    map.getView().animate({ center: point });
  }
}

//生成随机数
function createRandomId() {
  return (
    (Math.random() * 10000000).toString(16).substr(0, 4) +
    '-' +
    new Date().getTime() +
    '-' +
    Math.random()
      .toString()
      .substr(2, 5)
  );
}
