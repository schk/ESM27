/**
 * 创建普通图层
 * 
 * @param id
 *            图层ID
 * @description 要素的图片是要素的类型
 */
function createCustomLayer(id,image) {
	var layer = new ol.layer.Vector({
		source : new ol.source.Vector({
			features : []
		}),
		style : function(feature) {
			var cate = image?image:feature.get('catecd');
			var name = feature.getProperties().name;
			var style = new ol.style.Style({
				image : new ol.style.Icon({
					src : baseFileUrl + '/mapIcon/mapFixedIcon/' + cate + '.png',
					anchor : [ 0.5, 1 ]
				}),
				text : new ol.style.Text({
					text : name,
					offsetY : 10,
					font : 'bold 12px sans-serif',
					fill : new ol.style.Fill({
						color : '#84C1FF'
					}),
					stroke : new ol.style.Stroke({
						color : '#ECF5FF',
						width : 3
					})
				})
			})
			return style;
		},
		zIndex:10
	});
	layer.set('id', id);
	return layer;
}

/**
 * 
 * 创建图层组
 * */
function createGroupLayer(layerId){
	var group = new ol.layer.Group({
		layers:[]
	});
	group.set('id',layerId);
	return group;
}

/**
 * 创建无样式图层
 * */
function createNoStyleLayer(id) {
	var layer = new ol.layer.Vector({
		source : new ol.source.Vector({
			features : []
		}),
		zIndex:5
	});
	layer.set('id', id);
	return layer;
}

/**
 * 创建虚线图层
 * */
function createDotLineLayer(id) {
	var layer = new ol.layer.Vector({
		source : new ol.source.Vector({
			features : []
		}),
		style: new ol.style.Style({
			stroke:new ol.style.Stroke({
				color:'green',
				lineDash:[2,5],
				width:2
			})
		}),
		zIndex:5
	});
	layer.set('id', id);
	return layer;
}

/**
 * 创建同步图层
 * 
 * @param id
 *            图层ID
 * @description 要素的图片是要素的类型
 */
function createMarkLayer(id) {
	var layer = new ol.layer.Vector({
		source : new ol.source.Vector({
			features : []
		}),
		style : function(feature) {
			var image = feature.get('params').imagePath;
			var geometry = feature.getGeometry();
			var name = feature.get('name')?feature.get('name'):'';
			var isSpecial = feature.get('params').isSpecial?feature.get('params').isSpecial:false;
			var scolor = feature.get('scolor')?feature.get('scolor'):'#4781d9';
			var fcolor = feature.get('fcolor')?feature.get('fcolor'):'rgba(67, 110, 238, 0.4)';
			var geoType = feature.getGeometry().getType();
			var style;
			var anchor = [0.5,0.5];
			if(feature.get('params').clusterType=="accident"){
				anchor = [0.5,1];
			}
			if(geoType=="Point"){
				if(image){
					style = [new ol.style.Style({
						image : new ol.style.Icon({
							src : image,
							anchor : anchor
						}),
						text : new ol.style.Text({
							text : name,
							offsetY : 20,
							font : 'bold 12px sans-serif',
							fill : new ol.style.Fill({
								color : '#84C1FF'
							}),
							stroke : new ol.style.Stroke({
								color : '#ECF5FF',
								width : 3
							})
						})
					})];
				}else{
					style = [new ol.style.Style({
						circle : new ol.style.Circle({
							fill : new ol.style.Fill({
								color:'rgba(67, 110, 238, 0.4)'
							}),
							radius:5,
							stroke:new ol.style.Stroke({
								width:1,
								color:'#4781d9'
							})
						}),
						text : new ol.style.Text({
							text : name,
							offsetY : 20,
							font : 'bold 12px sans-serif',
							fill : new ol.style.Fill({
								color : '#84C1FF'
							}),
							stroke : new ol.style.Stroke({
								color : '#ECF5FF',
								width : 3
							})
						})
					})];
				}
			}else if(geoType=="LineString"){
				if(!isSpecial){
					style = [new ol.style.Style({
						stroke:new ol.style.Stroke({
							width:8,
							color: scolor
						})
					})];
				}else{
					style = [new ol.style.Style({
						stroke:new ol.style.Stroke({
							width:5,
							lineDash:[5,3],
							lineCap:'butt',
							color: scolor
						})
					})];
					geometry.forEachSegment(function(start, end) {
						var dx = end[0] - start[0];
						var dy = end[1] - start[1];
						var rotation = Math.atan2(dy, dx);
						style.push(new ol.style.Style({
							geometry : new ol.geom.Point(end),
							image : new ol.style.Icon({
								src : baseUrl+'/images/arrow.png',
								anchor : [ 0.5, 0.5 ],
								color : scolor,
								rotateWithView : true,
								rotation : -rotation
							})
						}));
					});
				}
				
			}else if(geoType=="Polygon"){
				style = [new ol.style.Style({
					stroke:new ol.style.Stroke({
						width:2,
						color: scolor
					}),
					fill: new ol.style.Fill({
						color:fcolor
					})
				})];
			}
			return style;
		},
		zIndex:5
	});
	layer.set('id', id);
	return layer;
}

/**
 * 创建聚合图层
 * @param id 图层ID
 * @param type类型
 * */
function createClusterLayer(id, type) {
	var source = new ol.source.Vector({
		features : []
	});
	var clusterSource = new ol.source.Cluster({
		source : source,
		distance : 30
	});
	var layer = new ol.layer.Vector({
		source : clusterSource,
		style : function(feature) {
			var size = feature.get('features').length;
			var style;
			if (size > 1) {
				style = [ new ol.style.Style({
					image : new ol.style.Icon({
						src : baseFileUrl+'/mapIcon/iconCluster/' + type + '_c.png',
						anchor : [ 0.5, 0.5 ]
					}),
					text : new ol.style.Text({
						font : '12px Arial',
						text : size.toString(),
						offsetX : 5,
						fill : new ol.style.Fill({
							color : '#FFFFFF'
						})
					}),
					zIndex : 3
				})];
			} else {
				var name = feature.get('features')[0].get("name");
				style = [ new ol.style.Style({
					image : new ol.style.Icon({
						src : baseFileUrl+'/mapIcon/mapFixedIcon/' + type + '.png',
						anchor : [ 0.5, 0.5 ]
					}),
					text : new ol.style.Text({
						text : name,
						offsetY : 20,
						font : 'bold 12px sans-serif',
						fill : new ol.style.Fill({
							color : '#84C1FF'
						}),
						stroke : new ol.style.Stroke({
							color : '#ECF5FF',
							width : 3
						})
					})
				}) ];
			}
			return style;
		},
		zIndex:10
	});
	layer.set('id', id);
	return layer;
}

/**
 * 创建道路图层
 * 
 * @param id
 *            图层ID
 * @param color
 *            线路颜色
 */
function creatRoadLayer(id, color) {
	var roadLayer = new ol.layer.Vector({
		source : new ol.source.Vector({
			features : []
		}),
		style : function(feature){
			return new ol.style.Style({
				stroke : new ol.style.Stroke({
					color : '#4682B4',
					width : 6
				})
			})
		},
		zIndex:5
	});
	roadLayer.set('id', id);
	return roadLayer;
}

/**
 * 往图层添加数据
 * 
 * @param features
 *            需要添加的要素
 * @param layer
 *            图层
 * @param isCluster 是否为聚合图层
 */
function addFeatureToLayer(features, layer,isCluster) {
	if(isCluster){
		layer.getSource().getSource().clear();
		layer.getSource().getSource().addFeatures(features);
	}else{
		layer.getSource().clear();
		layer.getSource().addFeatures(features);
	}
}

/**
 * 判断图层是否存在
 * 
 * @param layerId 图层ID
 * @param map 地图
 */
function layerIsExist(layerId, map) {
	var lay = null;
	map.getLayers().forEach(function(layer) {
		if (layer.get('id') == layerId) {
			lay = layer;
		}
	});
	return lay;
}

/**
 * 根据ID情况Layer
 * */
function clearLayerByIds(ids,map){
	for(var i=0;i<ids.length;i++ ){
		var layer = layerIsExist(ids[i],map);
		if(layer){
			layer.getSource().clear();
		}
	}
}

/**
 * 清空所有自定义图层
 * */
function clearAllLayer(){
	map.getLayers().forEach(function(layer){
		if(layer.get("id").indexOf("Layer")>-1){
			layer.getSource().clear();
		}
	});
}