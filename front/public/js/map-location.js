/**
 * 坐标拾取功能
 */
var app = {};


    /**
     * @constructor
     * @extends {module:ol/interaction/Pointer}
     */
    app.Drag = function() {

    ol.interaction.Pointer.call(this, {
        handleDownEvent: app.Drag.prototype.handleDownEvent,
        handleDragEvent: app.Drag.prototype.handleDragEvent,
        handleMoveEvent: app.Drag.prototype.handleMoveEvent,
        handleUpEvent: app.Drag.prototype.handleUpEvent
      });

      /**
       * @type {module:ol~Pixel}
       * @private
       */
      this.coordinate_ = null;

      /**
       * @type {string|undefined}
       * @private
       */
      this.cursor_ = 'pointer';

      /**
       * @type {module:ol/Feature~Feature}
       * @private
       */
      this.feature_ = null;

      /**
       * @type {string|undefined}
       * @private
       */
      this.previousCursor_ = undefined;

    };
    ol.inherits(app.Drag, ol.interaction.Pointer);


    /**
     * @param {module:ol/MapBrowserEvent~MapBrowserEvent} evt Map browser event.
     * @return {boolean} `true` to start the drag sequence.
     */
    app.Drag.prototype.handleDownEvent = function(evt) {
      var map = evt.map;

      var feature = map.forEachFeatureAtPixel(evt.pixel,
        function(feature) {
          return feature;
        });

      if (feature) {
        this.coordinate_ = evt.coordinate;
        this.feature_ = feature;
      }

      return !!feature;
    };


    /**
     * @param {module:ol/MapBrowserEvent~MapBrowserEvent} evt Map browser event.
     */
    app.Drag.prototype.handleDragEvent = function(evt) {
      var deltaX = evt.coordinate[0] - this.coordinate_[0];
      var deltaY = evt.coordinate[1] - this.coordinate_[1];

      var geometry = this.feature_.getGeometry();
      geometry.translate(deltaX, deltaY);

      this.coordinate_[0] = evt.coordinate[0];
      this.coordinate_[1] = evt.coordinate[1];
    };


    /**
     * @param {module:ol/MapBrowserEvent~MapBrowserEvent} evt Event.
     */
    app.Drag.prototype.handleMoveEvent = function(evt) {
      if (this.cursor_) {
        var map = evt.map;
        var feature = map.forEachFeatureAtPixel(evt.pixel,
          function(feature) {
            return feature;
          });
        var element = evt.map.getTargetElement();
        if (feature) {
          if (element.style.cursor != this.cursor_) {
            this.previousCursor_ = element.style.cursor;
            element.style.cursor = this.cursor_;
          }
        } else if (this.previousCursor_ !== undefined) {
          element.style.cursor = this.previousCursor_;
          this.previousCursor_ = undefined;
        }
      }
    };


    /**
     * @return {boolean} `false` to stop the drag sequence.
     */
    app.Drag.prototype.handleUpEvent = function() {
      this.coordinate_ = null;
      this.feature_ = null;
      return false;
    };
    
		var source = new ol.source.Vector({
			features : []
		});
		var layer = new ol.layer.Vector({
			source : source,
			style : new ol.style.Style({
				image : new ol.style.Icon({
					src :'../images/zhongdian.png',
					anchor : [ 0.5, 0.5 ]
				})
			})
		});
	
		var map = new ol.Map({
			target : 'map',
			layers : [ new ol.layer.Tile({
				source : new ol.source.XYZ({
					url : 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}'
				})
			}),layer],
			view : new ol.View({
				center : ol.proj.transform([ 104.094752, 30.667451 ],'EPSG:4326', 'EPSG:3857'),
				zoom : 12
			}),
			interactions: ol.interaction.defaults().extend([new app.Drag()]),
		});
		
		/**
		 * 此处的x,y值是传输过来的
		 * */
		var x = 104.094752;
		var y = 30.667451;
		var feature = new ol.Feature({
			geometry:new ol.geom.Point(new ol.proj.fromLonLat([x,y]))
		})
		feature.setId("position");
		source.addFeature(feature);
		
		function getPosition(){
			var point = feature.getGeometry().getCoordinates();
			var result = ol.proj.toLonLat(point);
			return result;
		}