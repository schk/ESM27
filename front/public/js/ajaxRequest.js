//ajax读取数据
function getAjaxData(url,param){
	var defer = $.Deferred();
	$.ajax({
       url : url, 
       type : "POST",       
       data:JSON.stringify(param),
       dataType:"json",
	   headers: {"Content-Type": "application/json;charset=utf-8"},
       async: false,
       success: function(data){
           defer.resolve(data)
       },
	   error: function(request) {
		   layer.msg("网络错误", {time: 1000});
       },
    }); 
    return defer.promise();
}

//加载数据
function executeAjax(url,param,fn){
    $.when(getAjaxData(url,param)).done(function(data){
	   	fn(data);
    });
}
