import { message } from 'antd';
import * as exprService from '../../services/expr/expr';
import { LiteChart } from '../../../node_modules/viser-react';
//import { add } from 'gl-matrix/src/gl-matrix/mat4';

export default {
  namespace: 'expr',

  state: {
    list: [],
    loading: false,
    current: 1,
    total: 0,
    pageSize: 10,
    modalVisible: false,
    item: {},
    variables: [],
    index: 0,
    buttomLoading: false,
    modalType: '',
    searchObj: {},
    arrayIndex: 0,
    dressingMethod: '',
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
  },

  effects: {
    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(exprService.qryListByParams, payload);
      if (data && data.httpCode == 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            total: data.iTotalRecords,
            pageSize: data.size,
            current: data.current,
            pages: data.pages,
            searchObj: payload,
          },
        });
      } else {
        message.error(data.errorMsg);
      }
    },

    *del({ payload, search }, { call, put, select }) {
      const { data } = yield call(exprService.delExpr, payload);
      if (data.httpCode == 200) {
        message.success('删除成功');
        let searchObj = yield select(state => state.expr.searchObj);
        searchObj.pageNum = 1;
        searchObj.pageSize = 10;
        yield put({ type: 'qryListByParams', payload: searchObj });
      } else {
        message.error(data.msg);
        yield put({ type: 'qryListByParams', payload: search });
      }
    },

    *changeDressingMethod({ payload }, { put }) {
      yield put({
        type: 'updateState',
        payload: {
          dressingMethod: payload,
        },
      });
    },

    *addv({ payload }, { call, put, select }) {
      let variables = yield select(state => state.expr.variables);
      let vindex = yield select(state => state.expr.index);
      let obj = {};
      obj.type = payload;
      obj.empty = false;
      obj.realTime = false;
      variables.push(obj);
      vindex = vindex + 1;
      yield put({
        type: 'updateState',
        payload: {
          index: vindex,
          variables: variables,
        },
      });
    },

    *addArray({ payload }, { call, put, select }) {
      let variables = yield select(state => state.expr.variables);
      let arrayIndex = yield select(state => state.expr.arrayIndex);
      var currVariable = variables[payload];
      var childVariable = [];
      if (currVariable.childOption == undefined) {
        childVariable.push(arrayIndex);
        currVariable.childOption = childVariable;
      } else {
        currVariable.childOption.push(arrayIndex);
      }
      arrayIndex = arrayIndex + 1;
      yield put({
        type: 'updateState',
        payload: {
          arrayIndex: arrayIndex,
          variables: variables,
        },
      });
    },

    *delArray({ payload }, { call, put, select }) {
      let variables = yield select(state => state.expr.variables);
      var aa = variables[payload.index];
      aa.childOption.splice(payload.indexArray, 1);
      yield put({
        type: 'updateState',
        payload: {
          variables: variables,
        },
      });
    },

    *delv({ payload }, { call, put, select }) {
      let variables = yield select(state => state.expr.variables);
      variables.splice(payload, 1);
      yield put({
        type: 'updateState',
        payload: {
          variables: variables,
        },
      });
    },

    *changeEmpty({ payload }, { call, put, select }) {
      let variables = yield select(state => state.expr.variables);
      variables[payload.index].empty = payload.checkd;
      yield put({
        type: 'updateState',
        payload: {
          variables: variables,
        },
      });
    },

    *changeReal({ payload }, { call, put, select }) {
      let variables = yield select(state => state.expr.variables);
      variables[payload.index].realTime = payload.checkd;
      yield put({
        type: 'updateState',
        payload: {
          variables: variables,
        },
      });
    },

    *changeDefault({ payload }, { call, put, select }) {
      let variables = yield select(state => state.expr.variables);
      let childOption = variables[payload.index].childOption;
      console.log(childOption);
      for (var i = 0; i < childOption.length; i++) {
        var current = childOption[i];
        current.defaultVal = false;
      }
      variables[payload.index].childOption[payload.indexArray].defaultVal = payload.checkd;
      console.log(variables);
      yield put({
        type: 'updateState',
        payload: {
          variables: variables,
        },
      });
    },

    *create({ payload, search }, { call, put, select }) {
      yield put({ type: 'updateState', payload: { buttomLoading: true } });
      const { data } = yield call(exprService.create, payload);
      if (data.httpCode == 200) {
        message.success('创建成功');
        yield put({
          type: 'updateState',
          payload: {
            buttomLoading: false,
            variables: [],
            index: 0,
            arrayIndex: 0,
            modalVisible: false,
          },
        });
        let searchObj = yield select(state => state.expr.searchObj);
        yield put({ type: 'qryListByParams', payload: searchObj });
      } else {
        message.error(data.msg);
        yield put({ type: 'qryListByParams', payload: search });
      }
    },
    *update({ payload, search }, { call, put, select }) {
      yield put({ type: 'updateState', payload: { buttomLoading: true } });
      const { data } = yield call(exprService.update, payload);
      if (data.httpCode == 200) {
        message.success('更新成功');
        yield put({
          type: 'updateState',
          payload: {
            buttomLoading: false,
            variables: [],
            index: 0,
            arrayIndex: 0,
            modalVisible: false,
          },
        });
        let searchObj = yield select(state => state.expr.searchObj);
        yield put({ type: 'qryListByParams', payload: searchObj });
      } else {
        message.error(data.msg);
        yield put({ type: 'qryListByParams', payload: search });
      }
    },
    *qryInfo({ payload }, { call, put }) {
      const { data } = yield call(exprService.qryInfo, payload);
      if (data && data.httpCode == 200) {
        let variables = [];
        let vindex = 0;
        let arrayIndex = 0;
        if (data.data.exprParams != null && data.data.exprParams.length > 0) {
          for (var i = 0; i < data.data.exprParams.length; i++) {
            let obj = {};
            obj.name = data.data.exprParams[i].name;
            obj.value = data.data.exprParams[i].value;
            obj.type = data.data.exprParams[i].type;
            obj.empty = data.data.exprParams[i].empty == 1 ? true : false;
            obj.realTime = data.data.exprParams[i].realTime == 1 ? true : false;
            obj.childOption = data.data.exprParams[i].childOption;
            if (obj.childOption != null && obj.childOption.length > 0) {
              for (var j = 0; j < obj.childOption.length; j++) {
                (obj.childOption[j].defaultVal = obj.childOption[j].defaultVal == 1 ? true : false),
                  arrayIndex++;
              }
            }
            variables.push(obj);
            vindex++;
          }
        }
        // console.log(variables);
        yield put({
          type: 'updateState',
          payload: {
            item: data.data,
            modalVisible: true,
            variables: variables,
            dressingMethod: data.data.dressingMethod,
            index: vindex,
            modalType: 'update',
          },
        });
      } else {
        message.error(data.errorMsg);
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/expr/edit') {
          dispatch({ type: 'qryListByParams', payload: { pageNum: 1, pageSize: 10 } });
        }
      });
    },
  },
};
