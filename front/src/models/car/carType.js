import { qryByType, create, getInfo, edit, del } from '../../services/car/carType';
import * as plotting from '../../services/plotting/plotting';
import * as dictionary from '../../services/dictionary';
import { message } from 'antd';
export default {
  namespace: 'carType',
  state: {
    list: [],
    modalVisible: false,
    buttomLoading: false,
    modalType: null,
    item: {},
    total: 0,
    pageSize: 10,
    current: 1,
    selectObj: {},
    typeList: [],
    plottingList: [],
    plottingModalVisible: false,
    plottingTotal: 0,
    plottingPageSize: 10,
    plottingCurrent: 1,
    path: '',
    jhPath: '',
    dtPath: '',
    newKey: '',
  },
  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },
  effects: {
    *qryByType({ payload }, { call, put }) {
      const { data } = yield call(qryByType, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            selectObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *create({ payload, search }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
          },
        });
        message.success('新建成功');
        yield put({ type: 'qryByType', payload: search });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },
    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: true,
            item: data.data,
            path: data.data.path,
            jhPath: data.data.jhPath,
            dtPath: data.data.dtPath,
            modalType: 'update',
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *update({ payload, search }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            path: '',
            jhPath: '',
            dtPath: '',
          },
        });
        message.success('修改成功');
        yield put({ type: 'qryByType', payload: search });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *del({ payload, search }, { call, put }) {
      const { data } = yield call(del, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'qryByType', payload: search });
      } else {
        message.error(data.msg);
      }
    },
    *qryPlottingList({ payload }, { call, put }) {
      const { data } = yield call(plotting.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            newKey: new Date().getTime + '',
            plottingList: data.data,
            plottingModalVisible: true,
            plottingCurrent: data.current,
            plottingPageSize: data.size,
            plottingTotal: data.iTotalRecords,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qryTypes({ payload }, { call, put }) {
      const { data } = yield call(dictionary.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            typeList: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (pathname === '/conventionalEquip/carType') {
          dispatch({ type: 'qryByType', payload: 1 });
          dispatch({ type: 'qryTypes', payload: 4 });
        }
      });
    },
  },
};
