import * as fireEngine from '../../services/car/fireEngine';
import { qryFireBrigadeTree } from '../../services/emergency/fireBrigade';
import * as specialDutyEquip from '../../services/conventionalEquip/specialDutyEquip';
import * as plotting from '../../services/plotting/plotting';
import { qryExtinguishere } from '../../services/emergency/extinguisher';
import * as dictionary from '../../services/dictionary';
import * as fireEngineEquipment from '../../services/fireEngineEquipment/fireEngineEquipment';
import * as qryByType from '../../services/car/carType';
import { getUserBrigadeId } from '../../services/user';

import { message } from 'antd';

export default {
  namespace: 'fireEngine',
  state: {
    carTypeParentList: [],
    carTypeChildList: [],
    list: [],
    plottingList: [],
    modalVisible: false,
    plottingModalVisible: false,
    typeList: [],
    showVisible: false,
    equipmentVisible: false,
    buttomLoading: false,
    modalType: null,
    item: {},
    editEquipItem: {},
    total: 0,
    pageSize: 8,
    current: 1,
    equipmentTotal: 0,
    equipmentPageSize: 10,
    equipmentCurrent: 1,
    selectObj: {},
    path: '',
    dicList: [],
    loading: false,
    equipType: '',
    key: '',
    key2: '',
    equipmentKey: '',
    fireEngineId: '',
    fireBrigadeTree: [],
    foamList: [],
    activityKey: 'tab1',
    equipmentList: [],
    currentSelectList: [],
    currentDate: {},
    extinguisherQu: 0,
    powderQuantityQu: 0,
    brigadeId: null,
    isvehicularArtillery: 1,
  },
  reducers: {
    //改变State里面的值
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },
  effects: {
    //不关联查询器材
    *qryEquipmentList({ payload }, { call, put }) {
      const { data } = yield call(specialDutyEquip.getObject, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            equipmentList: data.data,
            modalVisible: true,
            equipmentCurrent: data.current,
            equipmentPageSize: data.size,
            equipmentTotal: data.iTotalRecords,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qryPlottingList({ payload }, { call, put }) {
      const { data } = yield call(plotting.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            plottingList: data.data,
            equipmentList: data.data,
            plottingModalVisible: true,
            equipmentCurrent: data.current,
            equipmentPageSize: data.size,
            equipmentTotal: data.iTotalRecords,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qryCarTypeParent({ payload }, { call, put }) {
      const { data } = yield call(qryByType.qryCarTypeParent, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            carTypeParentList: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryCarTypeChild({ payload }, { call, put }) {
      const { data } = yield call(qryByType.qryCarTypeChild, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            carTypeChildList: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(fireEngine.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            selectObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryFireBrigadeTree({ payload }, { call, put }) {
      const { data } = yield call(qryFireBrigadeTree);
      yield put({ type: 'updateState', payload: { fireBrigadeTree: data.data } });
    },

    *qryDicList({ payload }, { call, put }) {
      const { data } = yield call(dictionary.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { dicList: data.data },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qryFoamList({ payload }, { call, put }) {
      const { data } = yield call(dictionary.qryListByParams, 5);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { foamList: data.data },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryExtinguisherQu({ payload }, { call, put }) {
      const { data } = yield call(qryExtinguishere, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { extinguisherQu: data.data },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryPowderQuantityQu({ payload }, { call, put }) {
      const { data } = yield call(qryExtinguishere, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { powderQuantityQu: data.data },
        });
      } else {
        message.error(data.msg);
      }
    },

    *create({ payload, search }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(fireEngine.create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            showVisible: false,
            fileList: [],
          },
        });
        message.success('新建成功');
        yield put({ type: 'qryListByParams', payload: search });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *showCreateModal({ payload }, { call, put }) {
      const { data } = yield call(getUserBrigadeId, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            brigadeId: data.data,
            showVisible: true,
            modalType: 'create',
            path: '',
            item: {},
            equipmentKey: new Date().getTime() + '',
            fireBrigadeTree: payload.fireBrigadeTree,
          },
        });
        yield put({ type: 'qryCarTypeParent', payload: 1 });
        yield put({ type: 'qryFoamList', payload: {} });
      } else {
        message.error(data.msg);
      }
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(fireEngine.getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            showVisible: true,
            path: data.data.path,
            item: data.data,
            isvehicularArtillery: data.data.vehicularArtillery ? 1 : 2,
            currentSelectList: data.detailList,
            currentMapList: data.detailList,
            modalType: 'update',
          },
        });
        yield put({ type: 'qryCarTypeParent', payload: 1 });
        //  yield put({ type: 'qryCarTypeChild', payload: data.data.dicId});
        //  yield put({ type: 'qryExtinguisherQu', payload: { id: data.data.foamTypeId } });
        //  yield put({ type: 'qryPowderQuantityQu', payload: { id: data.data.foamTypeIdO } });
      } else {
        message.error(data.msg);
      }
    },

    *qryTypes({ payload }, { call, put }) {
      const { data } = yield call(dictionary.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            typeList: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *update({ payload, search }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(fireEngine.edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            showVisible: false,
            fileList: [],
            isvehicularArtillery: 1,
          },
        });
        message.success('修改成功');
        yield put({ type: 'qryListByParams', payload: search });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *del({ payload, search }, { call, put }) {
      const { data } = yield call(fireEngine.del, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        search.pageNum = 1;
        search.pageSize = 10;
        yield put({ type: 'qryListByParams', payload: search });
      } else {
        message.error(data.msg);
      }
    },

    *importExcelConfirm({ payload, search }, { call, put }) {
      const { data } = yield call(fireEngine.importExcelConfirm, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'qryDicList', payload: 1 });
        yield put({ type: 'qryTypes', payload: 4 });
        yield put({ type: 'qryListByParams', payload: { pageNum: 1, pageSize: 8 } });
      } else {
        message.error(data.msg);
      }
    },

    *delExcelTemp({ payload, search }, { call, put }) {
      const { data } = yield call(fireEngine.delExcelTemp, payload);
      if (data && data.httpCode === 200) {
      } else {
        message.error(data.msg);
      }
    },
  },

  //根据id删除随车器材
  *delEquipment({ payload, search }, { call, put }) {
    const { data } = yield call(fireEngineEquipment.del, payload);
    if (data && data.httpCode === 200) {
      message.success('删除成功');
      yield put({ type: 'qryListByEngineId', payload: search });
    } else {
      message.error(data.msg);
    }
  },

  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (pathname === '/conventionalEquip/fireEngine') {
          dispatch({ type: 'qryListByParams', payload: { pageNum: 1, pageSize: 8 } });
          dispatch({ type: 'qryFireBrigadeTree' });
          dispatch({ type: 'qryDicList', payload: 1 });
        }
      });
    },
  },
};
