import * as fireEngine from '../../services/car/fireEngine';
import { message } from 'antd';
import { qryFireBrigadeTree } from '../../services/emergency/fireBrigade';
import * as specialDutyEquip from '../../services/conventionalEquip/specialDutyEquip';
import * as dictionary from '../../services/dictionary';
import * as fireEngineEquipment from '../../services/fireEngineEquipment/fireEngineEquipment';
import * as qryByType from '../../services/car/carType';
import { getUserBrigadeId } from '../../services/user';
export default {
  namespace: 'carMassage',
  state: {
    list: [],
    carTypeParentList: [],
    carTypeChildList: [],
    modalVisible: false,
    showVisible: false,
    EquipmentVisible: false,
    buttomLoading: false,
    item: {},
    editEquipItem: {},
    activityKey: 'tab1',
    type: 1,
    total: 0,
    pageSize: 10,
    current: 1,
    selectObj: {},
    fireBrigadeTree: [],
    fileList: [],
    dicList: [],
    selectedRowKeys: [],
    loading: false,
    equipType: '',
    key: '',
    key2: '',
    selectedRows: [],
    editingKey: '',
    rowKey: '',
    equipmentKey: '',
    fireEngineId: '',
    foamList: [],
    path: '',
    currentSelectList: [],
    isvehicularArtillery: 1,
    showInfoVisible: false,
    equipmentTotal: 0,
    equipmentPageSize: 10,
    equipmentCurrent: 1,
  },
  reducers: {
    //改变State里面的值
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },
  effects: {
    //不关联查询器材
    *qryEquipmentList({ payload }, { call, put }) {
      const { data } = yield call(specialDutyEquip.getObject, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            equipmentList: data.data,
            modalVisible: true,
            equipmentCurrent: data.current,
            equipmentPageSize: data.size,
            equipmentTotal: data.iTotalRecords,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryListEngine({ payload }, { call, put }) {
      const { data } = yield call(specialDutyEquip.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list2: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(fireEngine.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryFireBrigadeTree({ payload }, { call, put }) {
      const { data } = yield call(qryFireBrigadeTree, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            fireBrigadeTree: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qryDicList({ payload }, { call, put }) {
      const { data } = yield call(dictionary.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { dicList: data.data },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qryFoamList({ payload }, { call, put }) {
      const { data } = yield call(dictionary.qryListByParams, 5);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { foamList: data.data },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qryCarTypeParent({ payload }, { call, put }) {
      const { data } = yield call(qryByType.qryCarTypeParent, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            carTypeParentList: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *showCreateModal({ payload }, { call, put }) {
      const { data } = yield call(getUserBrigadeId, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            brigadeId: data.data,
            showVisible: true,
            modalType: 'create',
            path: '',
            item: {},
            equipmentKey: new Date().getTime() + '',
            fireBrigadeTree: payload.fireBrigadeTree,
          },
        });
        yield put({ type: 'qryCarTypeParent', payload: 1 });
        yield put({ type: 'qryFoamList', payload: {} });
      } else {
        message.error(data.msg);
      }
    },

    *create({ payload, search }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(fireEngine.create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            showVisible: false,
            fileList: [],
          },
        });
        message.success('新建成功');
        yield put({ type: 'qryListByParams', payload: search });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(fireEngine.getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            showVisible: true,
            item: data.data,
            path: data.data.path,
            isvehicularArtillery: data.data.vehicularArtillery ? 1 : 2,
            currentSelectList: data.detailList,
            currentMapList: data.detailList,
            modalType: 'update',
          },
        });
        yield put({ type: 'qryCarTypeParent', payload: 1 });
      } else {
        message.error(data.msg);
      }
    },

    *getinfo({ payload }, { call, put }) {
      const { data } = yield call(fireEngine.getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            showInfoVisible: true,
            item: data.data,
            path: data.data.path,
            isvehicularArtillery: data.data.vehicularArtillery ? 1 : 2,
            currentSelectList: data.detailList,
            modalType: 'update',
          },
        });
      }
    },

    *update({ payload, search }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(fireEngine.edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            showVisible: false,
            fileList: [],
            isvehicularArtillery: 1,
          },
        });
        message.success('修改成功');
        yield put({ type: 'qryListByParams', payload: search });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *del({ payload, search }, { call, put }) {
      const { data } = yield call(fireEngine.del, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        search.pageNum = 1;
        search.pageSize = 10;
        yield put({ type: 'qryListByParams', payload: search });
      } else {
        message.error(data.msg);
      }
    },
  },

  //根据id删除随车器材
  *delEquipment({ payload, search }, { call, put }) {

    const { data } = yield call(fireEngineEquipment.del, payload);
    if (data && data.httpCode === 200) {
      message.success('删除成功');
      yield put({ type: 'qryListByEngineId', payload: search });
    } else {
      message.error(data.msg);
    }
  },

  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (pathname === '/conventionalEquip/carMassage') {
          dispatch({ type: 'qryListByParams', payload: { pageNum: 1, pageSize: 10 } });
          dispatch({ type: 'qryFireBrigadeTree' });
          dispatch({ type: 'qryDicList', payload: 1 });
        }
      });
    },
  },
};
