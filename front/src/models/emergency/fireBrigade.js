import { message } from 'antd';
import {
  qryListByParams,
  qryFireBrigadeTree,
  create,
  getInfo,
  edit,
  del,
  getPlans,
  getInfoDetail,
} from '../../services/emergency/fireBrigade';
import { qryPlottingList } from '../../services/plotting/plotting';
import * as planService from '../../services/plans/plansList';
import * as typeService from '../../services/dictionary';
import { baseFileUrl } from '../../config/system';
export default {
  namespace: 'fireBrigade',

  state: {
    list: [],
    modalVisible: false,
    buttomLoading: false,
    modalType: null,
    item: {},
    total: 0,
    pageSize: 10,
    current: 1,
    fireBrigadeTree: [],
    findModalVisible: false,
    plottingList: [],
    locationItem: {},
    searchObj: {},
    plansModalVisible: false,
    plansList: [],
    planItemModalVisible: false,
    planItem: {},
    typeList: [],
    fileList: [],
    catalogIds: [],
    catalogNames: {},
    uuid: 0,
    keyUnitId: null,
    documnetModalVisible: false,
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    updatePlottingList(state, { payload }) {
      return { ...state, ...payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            searchObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryFireBrigadeTree({ payload }, { call, put }) {
      const { data } = yield call(qryFireBrigadeTree, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            fireBrigadeTree: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryPlottingList({ payload }, { call, put }) {
      const { data } = yield call(qryPlottingList);
      yield put({ type: 'updateState', payload: { plottingList: data.data } });
    },

    *create({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            locationItem: {},
          },
        });
        message.success('新建成功');
        let searchObj = yield select(state => state.fireBrigade.searchObj);
        yield put({ type: 'qryListByParams', payload: searchObj });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: true,
            item: data.data,
            locationItem: { lonLat: data.data.lonLat, dtPath: data.data.dtPath },
            modalType: 'update',
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *getDocumnet({ payload }, { call, put }) {
      const { data } = yield call(planService.updateFreq, payload.id);
      if (data && data.httpCode === 200) {
        yield put({ type: 'updateState', payload: payload });
      } else {
        message.error(data.msg);
      }
    },

    *qryTypes({ payload }, { call, put }) {
      const { data } = yield call(typeService.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'updateState', payload: { typeList: data.data } });
      } else {
        message.error(data.msg);
      }
    },
    *getPlans({ payload }, { call, put }) {
      const { data } = yield call(getPlans, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            keyUnitId: payload,
            plansModalVisible: true,
            planList: data.data,
            modalType: 'getPlans',
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *getPlanItem({ payload }, { call, put }) {
      const { data } = yield call(planService.getInfo, payload);
      if (data && data.httpCode === 200) {
        var catalogIds = [];
        var catalogNames = {};
        var uuid = 0;
        if (
          data.data != null &&
          data.data.plansCatalogs != null &&
          data.data.plansCatalogs.length > 0
        ) {
          for (var i = 0; i < data.data.plansCatalogs.length; i++) {
            catalogIds.push(i);
            catalogNames['name_' + i] = data.data.plansCatalogs[i].name;
            catalogNames['page_' + i] = data.data.plansCatalogs[i].page;
            uuid++;
          }
        }
        var fileList = [];
        if (data.data.path) {
          var dat = {};
          dat.name = data.data.fileName;
          dat.url = baseFileUrl + data.data.path;
          dat.status = 'done';
          dat.uid = data.data.id_;
          fileList.push(dat);
        }
        yield put({
          type: 'updateState',
          payload: {
            levelName: data.data.level,
            planItemModalVisible: true,
            planItem: data.data,
            modalType: 'updatePlans',
            fileList: fileList,
            catalogIds: catalogIds,
            catalogNames: catalogNames,
            uuid: uuid,
          },
        });
      }
    },

    *updatePlans({ payload }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(planService.edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            planItemModalVisible: false,
            fileList: [],
            planItem: {},
          },
        });
        message.success('修改成功');
        let searchObj = yield select(state => state.fireBrigade.keyUnitId);
        yield put({ type: 'getPlans', payload: searchObj });
        yield put({ type: 'updateState', payload: { catalogIds: [], catalogNames: {}, uuid: 0 } });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *createPlans({ payload }, { call, put, select }) {
      let searchObj = yield select(state => state.fireBrigade.keyUnitId);
      yield put({ type: 'showButtomLoading' });
      payload.keyUnitId = searchObj;
      payload.type = 2;
      const { data } = yield call(planService.create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            planItemModalVisible: false,
            fileList: [],
            planItem: {},
          },
        });
        message.success('添加成功');
        yield put({ type: 'getPlans', payload: searchObj });
        yield put({ type: 'updateState', payload: { catalogIds: [], catalogNames: {}, uuid: 0 } });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *delatePlan({ payload }, { call, put, select }) {
      const { data } = yield call(planService.delKeyUnitByPlanId, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        let searchObj = yield select(state => state.fireBrigade.keyUnitId);
        yield put({ type: 'getPlans', payload: searchObj });
      } else {
        message.error(data.msg);
      }
    },

    *findFireBrigade({ payload }, { call, put }) {
      const { data } = yield call(getInfoDetail, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            item: data.data,
            locationItem: { lonLat: data.data.lonLat, dtPath: data.data.dtPath },
            findModalVisible: true,
            modalType: 'update',
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *update({ payload }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            locationItem: {},
          },
        });
        message.success('修改成功');
        let searchObj = yield select(state => state.fireBrigade.searchObj);
        yield put({ type: 'qryListByParams', payload: searchObj });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *del({ payload }, { call, put, select }) {
      const { data } = yield call(del, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        let searchObj = yield select(state => state.fireBrigade.searchObj);
        searchObj.pageNum = 1;
        searchObj.pageSize = 10;
        yield put({ type: 'qryListByParams', payload: searchObj });
      } else {
        message.error(data.msg);
      }
    },
    *qryPlanListByParams({ payload }, { call, put }) {
      const { data } = yield call(planService.qryNotAddedPlanList, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            planItemModalVisible: true,
            modalType: 'createPlans',
            allPlansList: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            searchObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *updateKeyUnit({ payload }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(planService.updateKeyUnit, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'updateState', payload: { planItemModalVisible: false } });
        message.success('添加成功');
        yield put({ type: 'getPlans', payload: payload.keyUnitId });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/emergency/fireBrigade') {
          // dispatch({ type: 'qryPlottingList' });
          dispatch({ type: 'qryListByParams', payload: {} });
          dispatch({ type: 'qryFireBrigadeTree' });
        }
      });
    },
  },
};
