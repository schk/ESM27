import { message } from 'antd';
import { qryListByParams, create, getInfo, edit, del } from '../../services/emergency/suppliesType';

export default {
  namespace: 'suppliesType',

  state: {
    list: [],
    modalVisible: false,
    buttomLoading: false,
    modalType: null,
    item: {},
    selectObj: {},
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            selectObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *create({ payload, search }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
          },
        });
        message.success('新建成功');
        yield put({ type: 'qryListByParams', payload: {} });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: true,
            item: data.data,
            modalType: 'update',
          },
        });
      }
    },

    *update({ payload, search }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
          },
        });
        message.success('修改成功');
        yield put({ type: 'qryListByParams', payload: search });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *del({ payload, search }, { call, put }) {
      const { data } = yield call(del, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        yield put({ type: 'qryListByParams', payload: search });
      } else {
        message.error(data.msg);
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/emergency/suppliesType') {
          dispatch({
            type: 'qryListByParams',
            payload: {},
          });
        }
      });
    },
  },
};
