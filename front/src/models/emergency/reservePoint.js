import { message } from 'antd';
import { qryListByParams, create, getInfo, edit, del } from '../../services/emergency/reservePoint';
import { qryKeyUnitList } from '../../services/keyUnit';
import { qryEquipmentTypeList } from '../../services/equipment/equipmentType';
import { qryFireBrigadeTree } from '../../services/emergency/fireBrigade';
import { getUserBrigadeId } from '../../services/user';
import { qryPlottingList } from '../../services/plotting/plotting';

export default {
  namespace: 'reservePoint',

  state: {
    list: [],
    modalVisible: false,
    buttomLoading: false,
    modalType: null,
    selectObj: {},
    item: {},
    total: 0,
    pageSize: 10,
    current: 1,
    keyUnitList: [],
    fireBrigadeTree: [],
    type: 1,
    findModalVisible: false,
    brigadeId: null,
    plottingList: [],
    locationItem: {},
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    updatePlottingList(state, { payload }) {
      return { ...state, ...payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *qryFireBrigadeTree({ payload }, { call, put }) {
      const { data } = yield call(qryFireBrigadeTree, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            fireBrigadeTree: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryPlottingList({ payload }, { call, put }) {
      const { data } = yield call(qryPlottingList);
      yield put({ type: 'updateState', payload: { plottingList: data.data } });
    },

    *qryKeyUnitList({ payload }, { call, put }) {
      const { data } = yield call(qryKeyUnitList, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { keyUnitList: data.data },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            selectObj: payload,
            type: payload.type,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *create({ payload, search }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            locationItem: {},
            type: 1,
          },
        });
        message.success('新建成功');
        yield put({ type: 'qryListByParams', payload: search });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: true,
            item: data.data,
            type: data.data.type,
            locationItem: { lonLat: data.data.lonLat, dtPath: data.data.dtPath },
            modalType: 'update',
          },
        });
      }
    },
    *showCreateModal({ payload }, { call, put }) {
      const { data } = yield call(getUserBrigadeId, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            brigadeId: data.data,
            modalVisible: true,
            modalType: 'create',
            type: 1,
            item: {},
            locationItem: {},
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *findInfo({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            findModalVisible: true,
            item: data.data,
            type: data.data.type,
            locationItem: { lonLat: data.data.lonLat, dtPath: data.data.dtPath },
          },
        });
      }
    },

    *update({ payload, search }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            locationItem: {},
            type: 1,
          },
        });
        message.success('修改成功');
        yield put({ type: 'qryListByParams', payload: search });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *del({ payload, search }, { call, put }) {
      const { data } = yield call(del, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        search.pageNum = 1;
        search.pageSize = 10;
        yield put({ type: 'qryListByParams', payload: search });
      } else {
        message.error(data.msg);
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/emergency/reservePoint') {
          // dispatch({ type: 'qryPlottingList' });
          dispatch({ type: 'qryFireBrigadeTree' });
          dispatch({ type: 'qryKeyUnitList' });
          dispatch({ type: 'qryListByParams', payload: { type: '' } });
        }
      });
    },
  },
};
