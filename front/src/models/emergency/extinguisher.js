import {
  qryListByParams,
  create,
  getInfo,
  edit,
  del,
  importExcelConfirm,
  delExcelTemp,
} from '../../services/emergency/extinguisher';
import { qryFireBrigadeTree } from '../../services/emergency/fireBrigade';
import * as dictionary from '../../services/dictionary';
import { getUserBrigadeId } from '../../services/user';
import { message } from 'antd';
export default {
  namespace: 'extinguisher',
  state: {
    list: [],
    current: 1,
    pageSize: 10,
    total: 0,
    typeId: undefined,
    item: {},
    modalVisible: false,
    buttomLoading: false,
    modalType: 'create',
    msdsPath: '',
    newKey: '',
    fireBrigadeTree: [],
    selectObj: {},
    dicList: [],
    findModalVisible: false,
    brigadeId: null,
  },
  reducers: {
    updateState(state, { payload }) {
      return { ...state, ...payload };
    },
    showModal(state, { payload }) {
      return { ...state, ...payload, newKey: new Date().getTime() + '', modalVisible: true };
    },
    hideModal(state) {
      return { ...state, modalVisible: false, newKey: new Date().getTime() + '' };
    },
    showButtomLoading(state, action) {
      return { ...state, ...action, buttomLoading: true };
    },
    hideButtomLoading(state, action) {
      return { ...state, ...action, buttomLoading: false };
    },
  },
  effects: {
    *qryFireBrigadeTree({ payload }, { call, put }) {
      const { data } = yield call(qryFireBrigadeTree);
      yield put({ type: 'updateState', payload: { fireBrigadeTree: data.data } });
    },

    *qryDicList({ payload }, { call, put }) {
      const { data } = yield call(dictionary.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { dicList: data.data },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            typeId: payload.typeId,
            selectObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *create({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        let selectObj = yield select(state => state.extinguisher.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
        yield put({ type: 'hideModal' });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },
    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'showModal',
          payload: { item: data.data, modalType: 'edit', title: '编辑用户' },
        });
      } else {
        message.error(data.msg);
      }
    },
    *showCreateModal({ payload }, { call, put }) {
      const { data } = yield call(getUserBrigadeId, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            brigadeId: data.data,
            modalVisible: true,
            modalType: 'create',
            item: {},
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *findInfo({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { item: data.data, findModalVisible: true },
        });
      } else {
        message.error(data.msg);
      }
    },

    *edit({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtonLoading' });
      const { data } = yield call(edit, payload);
      if (data && data.httpCode === 200) {
        let selectObj = yield select(state => state.extinguisher.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
        yield put({ type: 'hideModal' });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },
    *remove({ payload, search }, { call, put, select }) {
      const { data } = yield call(del, payload);
      if (data && data.httpCode === 200) {
        let selectObj = yield select(state => state.extinguisher.selectObj);
        selectObj.pageNum = 1;
        selectObj.pageSize = 10;
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
    },

    *importExcelConfirm({ payload, search }, { call, put }) {
      const { data } = yield call(importExcelConfirm, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'qryListByParams', payload: { pageNum: 1, pageSize: 10 } });
        yield put({ type: 'qryDicList', payload: 5 });
      } else {
        message.error(data.msg);
      }
    },

    *delExcelTemp({ payload, search }, { call, put }) {
      const { data } = yield call(delExcelTemp, payload);
      if (data && data.httpCode === 200) {
      } else {
        message.error(data.msg);
      }
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (pathname === '/extinguisher/extinguisherList') {
          dispatch({ type: 'qryFireBrigadeTree' });
          dispatch({ type: 'qryDicList', payload: 5 });
          dispatch({ type: 'qryListByParams', payload: { pageNum: 1, pageSize: 10 } });
        }
      });
    },
  },
};
