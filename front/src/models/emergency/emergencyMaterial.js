import { message } from 'antd';
import {
  qryListByParams,
  create,
  getInfo,
  edit,
  del,
  importConfirm,
  delTemp,
} from '../../services/emergency/EmergencyMaterial';
import { qryKeyUnitList, getkeyUnitName } from '../../services/keyUnit';
import * as reservePointService from '../../services/emergency/reservePoint';
import * as suppliesTypeService from '../../services/emergency/suppliesType';
import { qryFireBrigadeTree } from '../../services/emergency/fireBrigade';
export default {
  namespace: 'emergencyMaterial',

  state: {
    list: [],
    modalVisible: false,
    buttomLoading: false,
    modalType: null,
    item: {},
    total: 0,
    pageSize: 10,
    current: 1,
    selectObj: {},
    keyUnitList: [],
    reservePointList: [],
    reservePointId: undefined,
    typeList: [],
    keyUnitItem: {},
    findModalVisible: false,
    fireBrigadeTree: [],
    reservePointKeyIdList: [],
    type: 1,
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *qryFireBrigadeTree({ payload }, { call, put }) {
      const { data } = yield call(qryFireBrigadeTree, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { fireBrigadeTree: data.data },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryKeyUnitList({ payload }, { call, put }) {
      const { data } = yield call(qryKeyUnitList, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { keyUnitList: data.data },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qryTypeList({ payload }, { call, put }) {
      const { data } = yield call(suppliesTypeService.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { typeList: data.data },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qryReserveByType({ payload }, { call, put }) {
      const { data } = yield call(reservePointService.qryReserveByType, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { reservePointKeyIdList: data.data },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryReservePointList({ payload }, { call, put }) {
      const { data } = yield call(reservePointService.qryReservePointList, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { reservePointList: data.data },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            selectObj: payload,
            reservePointId: payload.reservePointId,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *create({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
          },
        });
        message.success('新建成功');
        let selectObj = yield select(state => state.emergencyMaterial.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'qryReserveByType',
          payload: { keyUnitId: data.data.keyUnitId, type: data.data.type },
        });
        yield put({
          type: 'updateState',
          payload: {
            type: data.data.type,
            modalVisible: true,
            item: data.data,
            modalType: 'update',
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *EmergencyMaterialInfo({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            item: data.data,
            keyUnitName: data.data.keyUnitName,
            findModalVisible: true,
            modalType: 'update',
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *getkeyUnitName({ payload }, { call, put }) {
      const { data } = yield call(getkeyUnitName, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            keyUnitName: data.data.name,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *update({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
          },
        });
        message.success('修改成功');
        let selectObj = yield select(state => state.emergencyMaterial.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *del({ payload, search }, { call, put, select }) {
      const { data } = yield call(del, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        let selectObj = yield select(state => state.emergencyMaterial.selectObj);
        selectObj.pageNum = 1;
        selectObj.pageSize = 10;
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
    },
    *importConfirm({ payload, search }, { call, put }) {
      const { data } = yield call(importConfirm, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'qryTypeList', payload: {} });
        yield put({ type: 'qryListByParams', payload: {} });
      } else {
        message.error(data.msg);
      }
    },

    *delTemp({ payload, search }, { call, put }) {
      const { data } = yield call(delTemp, payload);
      if (data && data.httpCode === 200) {
      } else {
        message.error(data.msg);
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/emergency/emergencyMaterial') {
          dispatch({ type: 'qryListByParams', payload: { pageNum: 1, pageSize: 10 } });
          dispatch({ type: 'qryFireBrigadeTree' });
          dispatch({ type: 'qryTypeList', payload: {} });
          dispatch({ type: 'qryKeyUnitList' });
          dispatch({ type: 'qryReservePointList' });
        }
      });
    },
  },
};
