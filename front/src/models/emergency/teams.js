import { message } from 'antd';
import { qryListByParams, create, getInfo, edit, del } from '../../services/emergency/teams';
import { qryPlottingList } from '../../services/plotting/plotting';

export default {
  namespace: 'teams',

  state: {
    list: [],
    modalVisible: false,
    buttomLoading: false,
    modalType: null,
    item: {},
    total: 0,
    pageSize: 10,
    current: 1,
    lonLat: '',
    findModalVisible: false,
    plottingList: [],
    locationItem: {},
    searchObj: {},
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    updatePlottingList(state, { payload }) {
      return { ...state, ...payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            searchObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryPlottingList({ payload }, { call, put }) {
      const { data } = yield call(qryPlottingList);
      yield put({ type: 'updateState', payload: { plottingList: data.data } });
    },

    *create({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            locationItem: {},
          },
        });
        message.success('新建成功');

        let searchObj = yield select(state => state.teams.searchObj);
        yield put({ type: 'qryListByParams', payload: searchObj });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: true,
            item: data.data,
            locationItem: { lonLat: data.data.lonLat, dtPath: data.data.dtPath },
            modalType: 'update',
          },
        });
      }
    },
    *findTeams({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            item: data.data,
            locationItem: { lonLat: data.data.lonLat, dtPath: data.data.dtPath },
            modalType: 'update',
            findModalVisible: true,
          },
        });
      }
    },

    *update({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            locationItem: {},
          },
        });
        message.success('修改成功');
        let searchObj = yield select(state => state.teams.searchObj);
        yield put({ type: 'qryListByParams', payload: searchObj });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *del({ payload, search }, { call, put, select }) {
      const { data } = yield call(del, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        let searchObj = yield select(state => state.teams.searchObj);
        searchObj.pageNum = 1;
        searchObj.pageSize = 10;
        yield put({ type: 'qryListByParams', payload: searchObj });
      } else {
        message.error(data.msg);
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/emergency/teams') {
          //  dispatch({ type: 'qryPlottingList' });
          dispatch({ type: 'qryListByParams', payload: {} });
        }
      });
    },
  },
};
