import { message } from 'antd';
import {
  qryListByParams,
  getEditRecord,
  delRecord,
  getEditRecordById,
  editRecord,
  getHistoryWeatherDataByRoom,
  getHistoryGasDataByRoom,
  getHistoryGasDataTitle,
} from '../../services/accident/accident';
export default {
  namespace: 'accident',

  state: {
    list: [],
    modalVisible: false,
    buttomLoading: false,
    modalType: null,
    keyUnitId: undefined,
    selectObj: {},
    accidentId: '',
    total: 0,
    pageSize: 10,
    current: 1,
    lonLat: '',
    type: 1,
    reportVisable: false,
    editRecordVisable: false,
    dataVisable:false,
    editList: [],
    editTotal: 0,
    editPageSize: 10,
    editCurrent: 1,
    roomId: '',
    currentRecordModalVisible: false,
    currentRecordItem: {},
    fileList: [],
    activityKey:'tab1',
    room:'',
    historyWeatherData:[],
    totalWeather: 0,
    pageSizeWeather: 10,
    currentWeather: 1,
    historyGasData:[],
    totalGas: 0,
    pageSizeGas: 10,
    currentGas: 1,
    historyGasTitle:[],
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            selectObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *getEditRecord({ payload }, { call, put }) {
      const { data } = yield call(getEditRecord, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            editList: data.data,
            roomId: payload.id,
            editCurrent: data.current,
            editPageSize: data.size,
            editTotal: data.iTotalRecords,
            editRecordVisable: true,
            selectObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *delRecord({ payload, search }, { call, put }) {
      const { data } = yield call(delRecord, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'getEditRecord', payload: search });
      } else {
        message.error(data.msg);
      }
    },

    *getEditRecordById({ payload }, { call, put }) {
      const { data } = yield call(getEditRecordById, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            currentRecordItem: data.data,
            currentRecordModalVisible: true,
            fileList: data.data.fileList,
            modalType: 'editRecord',
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *editRecord({ payload }, { call, put }) {
      const { data } = yield call(editRecord, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'getEditRecordByIdEditAfter',
          payload: payload.id,
        });
        yield put({
          type: 'updateState',
          payload: { currentRecordModalVisible: false },
        });
      } else {
        message.error(data.msg);
      }
    },

    *getEditRecordByIdEditAfter({ payload }, { call, put }) {
      const { data } = yield call(getEditRecordById, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            currentRecordItem: data.data,
            fileList: data.data.fileList,
            modalType: 'editRecord',
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *findInfo({ payload }, { call, put }) {
      yield put({
        type: 'updateState',
        payload: {
          accidentId: payload,
          type: 2,
        },
      });
    },
    *findReport({ payload }, { call, put }) {
      yield put({
        type: 'updateState',
        payload: {
          reportVisable: true,
          accidentId: payload,
        },
      });
    },
    *findData({ payload }, { call, put }) {
      yield put({ type: 'getHistoryWeatherDataByRoom', payload: payload });
      yield put({ type: 'getHistoryGasDataByRoom', payload: payload });
      yield put({ type: 'getHistoryGasDataTitle', payload: payload });
      yield put({
        type: 'updateState',
        payload: {
          dataVisable: true,
          room: payload.room,
        },
      });
    },

    *getHistoryWeatherDataByRoom({ payload }, { call, put }) {
      const { data } = yield call(getHistoryWeatherDataByRoom, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            historyWeatherData: data.data,
            currentWeather: data.current,
            pageSizeWeather: data.size,
            totalWeather: data.iTotalRecords,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *getHistoryGasDataByRoom({ payload }, { call, put }) {
      const { data } = yield call(getHistoryGasDataByRoom, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            historyGasData: data.data,
            currentGas: data.current,
            pageSizeGas: data.size,
            totalGas: data.iTotalRecords,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *getHistoryGasDataTitle({ payload }, { call, put }) {
      const { data } = yield call(getHistoryGasDataTitle, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            historyGasTitle: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/accident/accidentList') {
          dispatch({ type: 'qryListByParams', payload: {} });
          dispatch({ type: 'getHistoryGasDataTitle', payload: {} });
        }
      });
    },
  },
};
