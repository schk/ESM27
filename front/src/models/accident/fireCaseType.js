import { message } from 'antd';
import * as typeService from '../../services/dictionary';
export default {
  namespace: 'fireCaseType',

  state: {
    list: [],
    modalVisible: false,
    buttomLoading: false,
    modalType: null,
    item: {},
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *create({ payload, search }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(typeService.create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
          },
        });
        message.success('新建成功');
        yield put({ type: 'qryTypes', payload: 7 });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *info({ payload }, { call, put }) {
      yield put({
        type: 'updateState',
        payload: {
          modalVisible: true,
          item: payload,
          modalType: 'update',
        },
      });
    },

    *update({ payload }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(typeService.edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
          },
        });
        message.success('修改成功');
        yield put({ type: 'qryTypes', payload: 7 });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *del({ payload }, { call, put }) {
      const { data } = yield call(typeService.del, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        yield put({ type: 'qryTypes', payload: 7 });
      } else {
        message.error(data.msg);
      }
    },

    *qryTypes({ payload }, { call, put }) {
      const { data } = yield call(typeService.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/accidentCase/fireCaseType') {
          dispatch({ type: 'qryTypes', payload: 7 });
        }
      });
    },
  },
};
