import { message } from 'antd';
import { qryListByParams, create, getInfo, edit, del } from '../../services/accident/accidentCase';
import * as typeService from '../../services/dictionary';
import { baseFileUrl } from '../../config/system';
export default {
  namespace: 'accidentCase',

  state: {
    list: [],
    modalVisible: false,
    buttomLoading: false,
    modalType: 'create',
    selectObj: {},
    item: {},
    total: 0,
    pageSize: 10,
    current: 1,
    fileList: [],
    typeList: [],
    findModalVisible: false,
    curFileId: '',
    picturePath: '',
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            selectObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *create({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            fileList: [],
          },
        });
        message.success('新建成功');
        yield put({ type: 'qryListByParams', payload: search });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        var curFileId = '';
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: true,
            item: data.data,
            fileList: data.data.fileList,
            modalType: 'update',
          },
        });
      }
    },

    *qryTypes({ payload }, { call, put }) {
      const { data } = yield call(typeService.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'updateState', payload: { typeList: data.data } });
      } else {
        message.error(data.msg);
      }
    },

    *findInfo({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            findModalVisible: true,
            item: data.data,
            fileList: data.data.fileList,
          },
        });
      }
    },

    *update({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            fileList: [],
          },
        });
        message.success('修改成功');
        let selectObj = yield select(state => state.accidentCase.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *del({ payload, search }, { call, put, select }) {
      const { data } = yield call(del, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        let selectObj = yield select(state => state.accidentCase.selectObj);
        selectObj.pageNum = 1;
        selectObj.pageSize = 10;
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/accidentCase/accidentCaseList') {
          dispatch({ type: 'qryTypes', payload: 7 });
          dispatch({
            type: 'qryListByParams',
            payload: { pageNum: 1, pageSize: 10 },
          });
        }
      });
    },
  },
};
