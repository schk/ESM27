import { qryListByParams, create, getInfo, edit, del } from '../../services/dangers/dangers';
import { qryDangerType, qryDangerTypeNoTop } from '../../services/dangers/dangersType';
import { message } from 'antd';
import { baseFileUrl } from '../../config/system';
export default {
  namespace: 'dangers',
  state: {
    dangerTypeTree: [],
    list: [],
    dangerTypeNoTopTree: [],
    current: 1,
    pageSize: 10,
    total: 0,
    typeId: undefined,
    currentItem: {},
    modalVisible: false,
    buttomLoading: false,
    modalType: 'create',
    msdsPath: '',
    fileList: [],
    activeKey: 'tab1',
    newKey: '',
    selectObj: {},
    filePath: null,
    catalogIds: [],
    catalogNames: {},
    uuid: 0,
    findModalVisible: false,
    documnetModalVisible: false,
    viweSwfPath: '',
    imgPath:'',
  },
  reducers: {
    updateState(state, { payload }) {
      return { ...state, ...payload };
    },
    showModal(state, { payload }) {
      return { ...state, ...payload, newKey: new Date().getTime() + '', modalVisible: true };
    },
    hideModal(state) {
      return { ...state, modalVisible: false, newKey: new Date().getTime() + '' };
    },
    showButtomLoading(state, action) {
      return { ...state, ...action, buttomLoading: true };
    },
    hideButtomLoading(state, action) {
      return { ...state, ...action, buttomLoading: false };
    },
  },
  effects: {
    *qryDangerTypeNoTop({ payload }, { call, put }) {
      const { data } = yield call(qryDangerTypeNoTop);
      yield put({ type: 'updateState', payload: { dangerTypeNoTopTree: data.data } });
    },
    *qryDangerType({ payload }, { call, put }) {
      const { data } = yield call(qryDangerType);
      yield put({ type: 'updateState', payload: { dangerTypeTree: data.data } });
    },
    *getDocumnet({ payload }, { call, put }) {
      yield put({ type: 'updateState', payload: payload });
    },
    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            typeId: payload.typeId,
            selectObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *create({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        let selectObj = yield select(state => state.dangers.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
        yield put({ type: 'hideModal' });
        yield put({ type: 'updateState', payload: { catalogIds: [], catalogNames: {}, uuid: 0,imgPath:'', } });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        var catalogIds = [];
        var catalogNames = {};
        var uuid = 0;
        if (
          data.data != null &&
          data.data.dangersCatalogs != null &&
          data.data.dangersCatalogs.length > 0
        ) {
          for (var i = 0; i < data.data.dangersCatalogs.length; i++) {
            catalogIds.push(i);
            catalogNames['name_' + i] = data.data.dangersCatalogs[i].name;
            catalogNames['page_' + i] = data.data.dangersCatalogs[i].page;
            uuid++;
          }
        }
        var fileList = [];
        if (data.data.msdsPath) {
          var dat = {};
          dat.name = data.data.fileName;
          dat.url = baseFileUrl + data.data.msdsPath;
          dat.status = 'done';
          dat.uid = data.data.id_;
          fileList.push(dat);
        }
        yield put({
          type: 'showModal',
          payload: {
            currentItem: data.data,
            fileList: fileList,
            modalType: 'edit',
            title: '编辑用户',
            catalogIds: catalogIds,
            catalogNames: catalogNames,
            uuid: uuid,
            imgPath:data.data.imgPath,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *findInfo({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        var catalogIds = [];
        var catalogNames = {};
        var uuid = 0;
        if (
          data.data != null &&
          data.data.dangersCatalogs != null &&
          data.data.dangersCatalogs.length > 0
        ) {
          for (var i = 0; i < data.data.dangersCatalogs.length; i++) {
            catalogIds.push(i);
            catalogNames['name_' + i] = data.data.dangersCatalogs[i].name;
            catalogNames['page_' + i] = data.data.dangersCatalogs[i].page;
            uuid++;
          }
        }
        var fileList = [];
        if (data.data.msdsPath) {
          var dat = {};
          dat.name = data.data.fileName;
          dat.url = baseFileUrl + data.data.msdsPath;
          dat.status = 'done';
          dat.uid = data.data.id_;
          fileList.push(dat);
        }
        yield put({
          type: 'updateState',
          payload: {
            currentItem: data.data,
            fileList: fileList,
            catalogIds: catalogIds,
            catalogNames: catalogNames,
            uuid: uuid,
            findModalVisible: true,
            imgPath:data.data.imgPath,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *edit({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(edit, payload);
      if (data && data.httpCode === 200) {
        let selectObj = yield select(state => state.dangers.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
        yield put({ type: 'hideModal' });
        yield put({ type: 'updateState', payload: { catalogIds: [], catalogNames: {}, uuid: 0,imgPath:'', } });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },
    *remove({ payload, search }, { call, put, select }) {
      const { data } = yield call(del, payload);
      if (data && data.httpCode === 200) {
        let selectObj = yield select(state => state.dangers.selectObj);
        selectObj.pageNum = 1;
        selectObj.pageSize = 10;
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (pathname === '/dangers/dangersList') {
          dispatch({ type: 'qryDangerType' });
          dispatch({ type: 'qryDangerTypeNoTop' });
          dispatch({ type: 'qryListByParams', payload: { pageNum: 1, pageSize: 10 } });
        }
      });
    },
  },
};
