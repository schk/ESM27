import { message } from 'antd';
import { qryDangerTypeNoTop, create, getInfo, edit, del } from '../../services/dangers/dangersType';

export default {
  namespace: 'dangersType',

  state: {
    list: [],
    modalVisible: false,
    buttomLoading: false,
    modalType: null,
    item: {},
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *qryDangerTypeNoTop({ payload }, { call, put }) {
      const { data } = yield call(qryDangerTypeNoTop, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *create({ payload }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
          },
        });
        message.success('新建成功');
        yield put({ type: 'qryDangerTypeNoTop' });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: true,
            item: data.data,
            modalType: 'update',
          },
        });
      }
    },

    *update({ payload }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
          },
        });
        message.success('修改成功');
        yield put({ type: 'qryDangerTypeNoTop' });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *del({ payload, search }, { call, put }) {
      const { data } = yield call(del, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        yield put({ type: 'qryDangerTypeNoTop', payload: search });
      } else {
        message.error(data.msg);
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/dangers/dangersType') {
          dispatch({
            type: 'qryDangerTypeNoTop',
            payload: {},
          });
        }
      });
    },
  },
};
