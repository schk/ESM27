import { message } from 'antd';
import { qryListByParams, create, getInfo, edit, del } from '../../services/plotting/plotting';
import * as typeService from '../../services/dictionary';
import { baseFileUrl } from '../../config/system';
export default {
  namespace: 'plotting',

  state: {
    list: [],
    modalVisible: false,
    buttomLoading: false,
    modalType: null,
    typeModalType: 'createType',
    item: {},
    total: 0,
    pageSize: 10,
    current: 1,
    fileList: [],
    jhFileList: [],
    dtFileList: [],
    typeList: [],
    typeModalVisible: false,
    newKey: '',
    selectObj: {},
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            selectObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *create({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            fileList: [],
            jhFileList: [],
            dtFileList: [],
          },
        });
        message.success('新建成功');
        let selectObj = yield select(state => state.plotting.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        var fileList = [];
        var jhFileList = [];
        var dtFileList = [];
        if (data.data.path) {
          var dat = {};
          dat.name = data.data.fileName;
          dat.url = baseFileUrl + data.data.path;
          dat.status = 'done';
          dat.uid = data.data.id_;
          fileList.push(dat);
        }
        if (data.data.dtPath) {
          var dat = {};
          dat.name = data.data.fileName;
          dat.url = baseFileUrl + data.data.dtPath;
          dat.status = 'done';
          dat.uid = data.data.id_;
          dtFileList.push(dat);
        }
        if (data.data.jhPath) {
          var dat = {};
          dat.name = data.data.fileName;
          dat.url = baseFileUrl + data.data.jhPath;
          dat.status = 'done';
          dat.uid = data.data.id_;
          jhFileList.push(dat);
        }
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: true,
            item: data.data,
            modalType: 'update',
            fileList: fileList,
            jhFileList: jhFileList,
            dtFileList: dtFileList,
          },
        });
      }
    },

    *update({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            fileList: [],
            jhFileList: [],
            dtFileList: [],
          },
        });
        message.success('修改成功');
        let selectObj = yield select(state => state.plotting.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *del({ payload, search }, { call, put, select }) {
      const { data } = yield call(del, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        let selectObj = yield select(state => state.plotting.selectObj);
        selectObj.pageNum = 1;
        selectObj.pageSize = 10;
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
    },

    *createType({ payload, search }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(typeService.create, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'qryTypes', payload: 4 });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *typeInfo({ payload }, { call, put }) {
      const { data } = yield call(typeService.getInfo, payload);
      yield put({
        type: 'updateState',
        payload: {
          typeModalVisible: true,
          typeItem: data.data,
          typeModalType: 'updateType',
        },
      });
    },

    *updateType({ payload }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(typeService.edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            typeModalVisible: false,
          },
        });
        message.success('修改成功');
        yield put({ type: 'qryTypes', payload: 3 });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *delType({ payload }, { call, put }) {
      const { data } = yield call(typeService.del, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        yield put({ type: 'qryTypes', payload: 4 });
      } else {
        message.error(data.msg);
      }
    },

    *qryTypes({ payload }, { call, put }) {
      const { data } = yield call(typeService.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            typeList: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/plotting/plottingList') {
          dispatch({
            type: 'qryListByParams',
            payload: {},
          });
          dispatch({ type: 'qryTypes', payload: 4 });
        }
      });
    },
  },
};
