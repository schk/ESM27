import { message } from 'antd';
import {
  qryListByParams,
  create,
  getInfo,
  edit,
  del,
  importExcelConfirm,
  delExcelTemp,
} from '../../services/equipment/facilities';
import { qryKeyUnitList } from '../../services/keyUnit';
import { qryEquipmentTypeList } from '../../services/equipment/equipmentType';
export default {
  namespace: 'facilities',

  state: {
    list: [],
    modalVisible: false,
    buttomLoading: false,
    modalType: null,
    keyUnitId: undefined,
    selectObj: {},
    item: {},
    total: 0,
    pageSize: 10,
    current: 1,
    keyUnitList: [],
    equipmentTypeList: [],
    findModalVisible: false,
    locationItem: {},
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *qryKeyUnitList({ payload }, { call, put }) {
      const { data } = yield call(qryKeyUnitList, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { keyUnitList: data.data },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qryEquipmentTypeList({ payload }, { call, put }) {
      const { data } = yield call(qryEquipmentTypeList, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { equipmentTypeList: data.data },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            keyUnitId: payload.keyUnitId,
            selectObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *create({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            locationItem: {},
          },
        });
        message.success('新建成功');
        let selectObj = yield select(state => state.facilities.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: true,
            item: data.data,
            locationItem: { lonLat: data.data.lonLat },
            modalType: 'update',
          },
        });
      }
    },

    *findInfo({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            item: data.data,
            locationItem: { lonLat: data.data.lonLat },
            findModalVisible: true,
          },
        });
      }
    },

    *update({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            locationItem: {},
          },
        });
        message.success('修改成功');
        let selectObj = yield select(state => state.facilities.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *del({ payload, search }, { call, put, select }) {
      const { data } = yield call(del, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        let selectObj = yield select(state => state.facilities.selectObj);
        selectObj.pageNum = 1;
        selectObj.pageSize = 10;
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
    },

    *importExcelConfirm({ payload, search }, { call, put }) {
      const { data } = yield call(importExcelConfirm, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'qryKeyUnitList' });
        yield put({ type: 'qryEquipmentTypeList', payload: 1 });
        yield put({ type: 'qryListByParams', payload: { keyUnitId: undefined } });
      } else {
        message.error(data.msg);
      }
    },

    *delExcelTemp({ payload, search }, { call, put }) {
      const { data } = yield call(delExcelTemp, payload);
      if (data && data.httpCode === 200) {
      } else {
        message.error(data.msg);
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/unit/facilities') {
          dispatch({ type: 'qryKeyUnitList' });
          dispatch({ type: 'qryEquipmentTypeList', payload: 1 });
          dispatch({ type: 'qryListByParams', payload: { keyUnitId: undefined } });
        }
      });
    },
  },
};
