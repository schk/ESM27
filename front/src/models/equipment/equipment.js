import { message } from 'antd';
import {
  qryListByParams,
  create,
  getInfo,
  edit,
  del,
  importExcelConfirm,
  delExcelTemp,
} from '../../services/equipment/equipment';
import { qryKeyUnitList } from '../../services/keyUnit';
import { qryEquipmentTypeList } from '../../services/equipment/equipmentType';
import { baseFileUrl } from '../../config/system';
var flag = 0;
export default {
  namespace: 'equipment',

  state: {
    list: [],
    modalVisible: false,
    buttomLoading: false,
    modalType: null,
    keyUnitId: undefined,
    selectObj: {},
    item: {},
    total: 0,
    pageSize: 10,
    current: 1,
    keyUnitList: [],
    equipmentTypeList: [],
    fileList: [],
    findModalVisible: false,
    curFileId: '',
    fileCatalogs: [],
    catalogNames: {},
    picturePath: '',
    locationItem: {},
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *qryKeyUnitList({ payload }, { call, put }) {
      const { data } = yield call(qryKeyUnitList, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { keyUnitList: data.data },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qryEquipmentTypeList({ payload }, { call, put }) {
      const { data } = yield call(qryEquipmentTypeList, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { equipmentTypeList: data.data },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            keyUnitId: payload.keyUnitId,
            selectObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *create({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            fileList: [],
            curFileId: '',
            fileCatalogs: [],
            catalogNames: {},
            locationItem: {},
          },
        });
        message.success('新建成功');
        let selectObj = yield select(state => state.equipment.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      console.log(data);
      if (data && data.httpCode === 200) {
        var curFileId = '';
        var fileCatalogs = [];
        var catalogNames = {};
        var fileList = [];
        if (data.docList.length > 0) {
          for (var i = 0; i < data.docList.length; i++) {
            var obj = data.docList[i];
            var dat = {};
            dat.name = obj.docName;
            dat.url = obj.path;
            dat.status = 'done';
            dat.uid = obj.id;
            fileList.push(dat);
            var fileCatalog = {};
            var catalogs = [];
            if (i == 0) {
              curFileId = obj.id;
            }
            var uuid = 0;
            fileCatalog.fileId = obj.id;
            if (
              data.docList[i].equipmentDocCatalogs != null &&
              data.docList[i].equipmentDocCatalogs.length > 0
            ) {
              for (var j = 0; j < data.docList[i].equipmentDocCatalogs.length; j++) {
                catalogs.push(j + 1);
                catalogNames['name_' + fileCatalog.fileId + '_' + (j + 1)] =
                  data.docList[i].equipmentDocCatalogs[j].name;
                catalogNames['page_' + fileCatalog.fileId + '_' + (j + 1)] =
                  data.docList[i].equipmentDocCatalogs[j].page;
                uuid++;
              }
            }
            fileCatalog.uuid = uuid;
            fileCatalog.catalogs = catalogs;
            fileCatalogs.push(fileCatalog);
          }
        }
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: true,
            locationItem: { lonLat: data.data.lonLat },
            item: data.data,
            picturePath: data.docList.path,
            fileList: fileList,
            modalType: 'update',
            curFileId: curFileId,
            fileCatalogs: fileCatalogs,
            catalogNames: catalogNames,
          },
        });
      }
    },

    *findInfo({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        var fileList = [];
        var curFileId = '';
        var fileCatalogs = [];
        var catalogNames = {};
        if (data.docList.length > 0) {
          for (var i = 0; i < data.docList.length; i++) {
            var obj = data.docList[i];
            var dat = {};
            dat.name = obj.docName;
            dat.url = baseFileUrl + obj.path;
            dat.status = 'done';
            dat.uid = obj.id;
            fileList.push(dat);
            var fileCatalog = {};
            var catalogs = [];
            if (i == 0) {
              curFileId = obj.id;
            }
            fileCatalog.fileId = obj.id;
            var uuid = 0;
            if (
              data.docList[i].equipmentDocCatalogs != null &&
              data.docList[i].equipmentDocCatalogs.length > 0
            ) {
              for (var j = 0; j < data.docList[i].equipmentDocCatalogs.length; j++) {
                catalogs.push(j + 1);
                catalogNames['name_' + fileCatalog.fileId + '_' + (j + 1)] =
                  data.docList[i].equipmentDocCatalogs[j].name;
                catalogNames['page_' + fileCatalog.fileId + '_' + (j + 1)] =
                  data.docList[i].equipmentDocCatalogs[j].page;
                uuid++;
              }
            }
            fileCatalog.uuid = uuid;
            fileCatalog.catalogs = catalogs;
            fileCatalogs.push(fileCatalog);
          }
        }
        yield put({
          type: 'updateState',
          payload: {
            locationItem: { lonLat: data.data.lonLat },
            findModalVisible: true,
            item: data.data,
            fileList: fileList,
            curFileId: curFileId,
            fileCatalogs: fileCatalogs,
            catalogNames: catalogNames,
          },
        });
      }
    },

    *update({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            fileList: [],
            curFileId: '',
            locationItem: {},
            fileCatalogs: [],
            catalogNames: {},
          },
        });
        message.success('修改成功');
        let selectObj = yield select(state => state.equipment.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *del({ payload, search }, { call, put, select }) {
      const { data } = yield call(del, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        let selectObj = yield select(state => state.equipment.selectObj);
        selectObj.pageNum = 1;
        selectObj.pageSize = 10;
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
    },

    *importExcelConfirm({ payload, search }, { call, put }) {
      const { data } = yield call(importExcelConfirm, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'qryKeyUnitList' });
        yield put({ type: 'qryEquipmentTypeList', payload: 2 });
        yield put({
          type: 'qryListByParams',
          payload: { pageNum: 1, pageSize: 10, keyUnitId: undefined },
        });
      } else {
        message.error(data.msg);
      }
    },

    *delExcelTemp({ payload, search }, { call, put }) {
      const { data } = yield call(delExcelTemp, payload);
      if (data && data.httpCode === 200) {
      } else {
        message.error(data.msg);
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/unit/equipment') {
          dispatch({ type: 'qryKeyUnitList' });
          dispatch({ type: 'qryEquipmentTypeList', payload: 2 });
          dispatch({
            type: 'qryListByParams',
            payload: { pageNum: 1, pageSize: 10, keyUnitId: undefined },
          });
        }
      });
    },
  },
};
