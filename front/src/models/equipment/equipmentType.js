import { message } from 'antd';
import {
  qryListByParams,
  create,
  getInfo,
  edit,
  del,
} from '../../services/equipment/equipmentType';
import { qryPlottingList ,qryJhPlottingList} from '../../services/plotting/plotting';

export default {
  namespace: 'equipmentType',

  state: {
    list: [],
    modalVisible: false,
    buttomLoading: false,
    modalType: null,
    item: {},
    total: 0,
    pageSize: 10,
    current: 1,
    plottingList: [],
    jhPlottingList:[],
    locationItem: {},
    selectObj: {},
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *qryPlottingList({ payload }, { call, put }) {
      const { data } = yield call(qryPlottingList);
      yield put({ type: 'updateState', payload: { plottingList: data.data } });
    },
    *qryJhPlottingList({ payload }, { call, put }) {
      const { data } = yield call(qryJhPlottingList);
      yield put({ type: 'updateState', payload: { jhPlottingList: data.data } });
    },

    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            selectObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *create({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            locationItem: {},
          },
        });
        message.success('新建成功');

        let selectObj = yield select(state => state.equipmentType.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: true,
            item: data.data,
            modalType: 'update',
          },
        });
      }
    },

    *update({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            locationItem: {},
          },
        });
        message.success('修改成功');
        let selectObj = yield select(state => state.equipmentType.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *del({ payload, search }, { call, put, select }) {
      const { data } = yield call(del, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        let selectObj = yield select(state => state.equipmentType.selectObj);
        selectObj.pageNum = 1;
        selectObj.pageSize = 10;
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/unit/equipmentType') {
          dispatch({ type: 'qryPlottingList' });
          dispatch({ type: 'qryJhPlottingList' });
          dispatch({
            type: 'qryListByParams',
            payload: {},
          });
        }
      });
    },
  },
};
