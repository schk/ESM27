import { message } from 'antd';
import {
  qryListByParams,
  create,
  getInfo,
  edit,
  del,
  delDangers,
  saveDangersOfKeyParts,
  importExcelConfirm,
  delExcelTemp,
} from '../../services/equipment/keyParts';

import * as aa from '../../services/dangers/dangers';
import { qryKeyUnitList } from '../../services/keyUnit';
import * as planService from '../../services/plans/plansList';
import * as typeService from '../../services/dictionary';
import { baseFileUrl } from '../../config/system';
import { qryFireBrigadeTree } from '../../services/emergency/fireBrigade';

export default {
  namespace: 'keyParts',

  state: {
    list: [],
    modalVisible: false,
    buttomLoading: false,
    modalType: null,
    keyUnitId: undefined,
    selectObj: {},
    item: {},
    total: 0,
    pageSize: 10,
    current: 1,
    keyUnitList: [],
    findModalVisible: false,
    documnetModalVisible: false,
    path: '',
    processFlow: '',
    planList: [],
    totalPlan: 0,
    pageSizePlan: 10,
    currentPlan: 1,
    keyPartsId: '',
    dangersVisible: false,
    existDangersList: [],
    addDangersVisible: false,
    allDangersList: [],
    totalDangers: 0,
    pageSizeDangers: 10,
    currentDangers: 1,

    plansModalVisible: false,
    plansList: [],
    planItemModalVisible: false,
    planItem: {},
    typeList: [],
    fileList: [],
    catalogIds: [],
    catalogNames: {},
    uuid: 0,
    filePath: null,
    fireBrigadeTree: [],
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *qryFireBrigadeTree({ payload }, { call, put }) {
      const { data } = yield call(qryFireBrigadeTree);
      yield put({ type: 'updateState', payload: { fireBrigadeTree: data.data } });
    },
    *qryKeyUnitList({ payload }, { call, put }) {
      const { data } = yield call(qryKeyUnitList, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { keyUnitList: data.data },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            keyUnitId: payload.keyUnitId,
            selectObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *onShowDangers({ payload }, { call, put }) {
      yield put({
        type: 'updateState',
        payload: { dangersVisible: true, keyPartsId: payload },
      });
      yield put({
        type: 'getDangers',
        payload: {
          keyPartsId: payload,
        },
      });
    },

    *getDangers({ payload }, { call, put }) {
      const { data } = yield call(aa.qryDangersByKeyParts, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            existDangersList: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *getAllDangers({ payload }, { call, put }) {
      const { data } = yield call(aa.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            allDangersList: data.data,
            currentDangers: data.current,
            pageSizeDangers: data.size,
            totalDangers: data.iTotalRecords,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *delDangers({ payload, search }, { call, put, select }) {
      const { data } = yield call(delDangers, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        yield put({ type: 'getDangers', payload: search });
      } else {
        message.error(data.msg);
      }
    },

    *qryTypes({ payload }, { call, put }) {
      const { data } = yield call(typeService.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'updateState', payload: { typeList: data.data } });
      } else {
        message.error(data.msg);
      }
    },
    *getPlans({ payload }, { call, put }) {
      const { data } = yield call(planService.qryPlanByKeyParts, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            keyPartsId: payload,
            plansModalVisible: true,
            planList: data.data,
            modalType: 'getPlans',
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *getPlanItem({ payload }, { call, put }) {
      const { data } = yield call(planService.getInfo, payload);
      if (data && data.httpCode === 200) {
        var catalogIds = [];
        var catalogNames = {};
        var uuid = 0;
        if (
          data.data != null &&
          data.data.plansCatalogs != null &&
          data.data.plansCatalogs.length > 0
        ) {
          for (var i = 0; i < data.data.plansCatalogs.length; i++) {
            catalogIds.push(i);
            catalogNames['name_' + i] = data.data.plansCatalogs[i].name;
            catalogNames['page_' + i] = data.data.plansCatalogs[i].page;
            uuid++;
          }
        }
        var fileList = [];
        if (data.data.path) {
          var dat = {};
          dat.name = data.data.fileName;
          dat.url = baseFileUrl + data.data.path;
          dat.status = 'done';
          dat.uid = data.data.id_;
          fileList.push(dat);
        }
        yield put({
          type: 'updateState',
          payload: {
            levelName: data.data.level,
            planItemModalVisible: true,
            planItem: data.data,
            modalType: 'updatePlans',
            fileList: fileList,
            catalogIds: catalogIds,
            catalogNames: catalogNames,
            uuid: uuid,
          },
        });
      }
    },

    *updatePlans({ payload }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(planService.edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            planItemModalVisible: false,
            fileList: [],
            planItem: {},
          },
        });
        message.success('修改成功');
        let searchObj = yield select(state => state.keyParts.keyPartsId);
        yield put({ type: 'getPlans', payload: searchObj });
        yield put({ type: 'updateState', payload: { catalogIds: [], catalogNames: {}, uuid: 0 } });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *createPlans({ payload }, { call, put, select }) {
      let searchObj = yield select(state => state.keyParts.keyPartsId);
      yield put({ type: 'showButtomLoading' });
      payload.keyUnitId = searchObj;
      payload.type = 3;
      const { data } = yield call(planService.create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            planItemModalVisible: false,
            fileList: [],
            planItem: {},
          },
        });
        message.success('添加成功');
        yield put({ type: 'getPlans', payload: searchObj });
        yield put({ type: 'updateState', payload: { catalogIds: [], catalogNames: {}, uuid: 0 } });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *delatePlan({ payload }, { call, put, select }) {
      const { data } = yield call(planService.delKeyUnitByPlanId, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        let searchObj = yield select(state => state.keyParts.keyPartsId);
        yield put({ type: 'getPlans', payload: searchObj });
      } else {
        message.error(data.msg);
      }
    },

    *getDocumnet({ payload }, { call, put }) {
      const { data } = yield call(planService.updateFreq, payload.id);
      if (data && data.httpCode === 200) {
        yield put({ type: 'updateState', payload: payload });
      } else {
        message.error(data.msg);
      }
    },

    *saveDangersOfKeyParts({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(saveDangersOfKeyParts, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            dangersVisible: false,
            existDangersList: [],
          },
        });
        message.success('新建成功');
        yield put({ type: 'qryListByParams', payload: search });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *create({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
          },
        });
        message.success('新建成功');
        let selectObj = yield select(state => state.keyParts.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: true,
            item: data.data,
            processFlow: data.data.processFlow,
            path: data.data.path,
            modalType: 'update',
          },
        });
      }
    },

    *findInfo({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            item: data.data,
            processFlow: data.data.processFlow,
            path: data.data.path,
            findModalVisible: true,
          },
        });
      }
    },

    *update({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            processFlow: '',
            path: '',
          },
        });
        message.success('修改成功');
        let selectObj = yield select(state => state.keyParts.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *del({ payload, search }, { call, put, select }) {
      const { data } = yield call(del, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        let selectObj = yield select(state => state.keyParts.selectObj);
        selectObj.pageNum = 1;
        selectObj.pageSize = 10;
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
    },

    *importExcelConfirm({ payload, search }, { call, put }) {
      const { data } = yield call(importExcelConfirm, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'qryKeyUnitList' });
        yield put({ type: 'qryListByParams', payload: { keyUnitId: undefined } });
      } else {
        message.error(data.msg);
      }
    },

    *delExcelTemp({ payload, search }, { call, put }) {
      const { data } = yield call(delExcelTemp, payload);
      if (data && data.httpCode === 200) {
      } else {
        message.error(data.msg);
      }
    },

    *qryPlanListByParams({ payload }, { call, put }) {
      const { data } = yield call(planService.qryNotAddedPlanList, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            planItemModalVisible: true,
            modalType: 'createPlans',
            allPlansList: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            searchObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *updateKeyUnit({ payload }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(planService.updateKeyUnit, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'updateState', payload: { planItemModalVisible: false } });
        message.success('添加成功');
        yield put({ type: 'getPlans', payload: payload.keyUnitId });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/unit/keyParts') {
          dispatch({ type: 'qryKeyUnitList' });
          dispatch({ type: 'qryFireBrigadeTree' });
          dispatch({ type: 'qryListByParams', payload: {} });
        }
      });
    },
  },
};
