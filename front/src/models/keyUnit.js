import {
  queryList,
  remove,
  create,
  getInfo,
  edit,
  getkeyUnitInfo,
  getUnitAccident,
  addKeyUnitAccident,
  saveDangersOfKeyUnit,
} from '../services/keyUnit';
import { qryFireBrigadeTree } from '../services/emergency/fireBrigade';
import { qryDangerTypeNoTop } from '../services/dangers/dangersType';
import { qryPlottingList } from '../services/plotting/plotting';
import { message } from 'antd';
import { getUserBrigadeId } from '../services/user';
import { baseFileUrl } from '../config/system';
import * as planService from '../services/plans/plansList';
import * as typeService from '../services/dictionary';
import * as aa from '../services/dangers/dangers';
export default {
  namespace: 'keyUnit',

  state: {
    list: [],
    pagination: {},
    current: 1,
    pageSize: 10,
    total: 0,
    modalType: 'create',
    detailModalType: 'createDetail',
    buttomLoading: false,
    fireBrigadeTree: [],
    modalVisible: false,
    selectObj: {},
    fileList: [],
    imgPath: '',
    swfPath: '',
    msdsPath: '',
    activityKey: 'tab1',
    detailActiveKey: 'tab1',
    modalDangersVisible: false,
    FindModalDangersVisible: false,
    dangersItem: {},
    dangerTypeNoTopTree: [],
    locationModalVisible: false,
    findModalVisible: false,
    findItem: {},
    type: 1,
    dangersList: [],
    previewVisible: false,
    previewImage: '',
    catalogIds: [],
    catalogNames: {},
    uuid: 0,
    brigadeId: null,
    plottingList: [],
    locationItem: {},
    accidentItem: {},
    accidentFileList: [],
    showAccidentVisible: false,
    filePath: null,
    plansModalVisible: false,
    planItemModalVisible: false,
    planItem: {},
    typeList: [],
    pfileList: [],
    pcatalogIds: [],
    pcatalogNames: {},
    puuid: 0,
    keyUnitId: null,
    documnetModalVisible: false,

    dangersVisible: false,
    existDangersList: [],
    addDangersVisible: false,
    allDangersList: [],
    totalDangers: 0,
    pageSizeDangers: 10,
    currentDangers: 1,
  },

  reducers: {
    updateState(state, { payload }) {
      return { ...state, ...payload };
    },
    updatePlottingList(state, { payload }) {
      return { ...state, ...payload };
    },
    showButtomLoading(state, action) {
      return { ...state, ...action, buttomLoading: true };
    },
    hideButtomLoading(state, action) {
      return { ...state, ...action, buttomLoading: false };
    },
  },

  effects: {
    *delDangers({ payload, search }, { call, put, select }) {
      const { data } = yield call(delDangers, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        yield put({ type: 'getDangers', payload: search });
      } else {
        message.error(data.msg);
      }
    },
    *getAllDangers({ payload }, { call, put }) {
      const { data } = yield call(aa.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            allDangersList: data.data,
            currentDangers: data.current,
            pageSizeDangers: data.size,
            totalDangers: data.iTotalRecords,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *saveDangersOfKeyUnit({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(saveDangersOfKeyUnit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            dangersVisible: false,
            existDangersList: [],
          },
        });
        message.success('新建成功');
        yield put({ type: 'qryListByParams', payload: search });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },
    *onShowDangers({ payload }, { call, put }) {
      yield put({
        type: 'updateState',
        payload: { dangersVisible: true, keyUnitId: payload },
      });
      yield put({
        type: 'getDangers',
        payload: {
          keyUnitId: payload,
        },
      });
    },
    *getDangers({ payload }, { call, put }) {
      const { data } = yield call(aa.qryDangersByKeyUnit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            existDangersList: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qryFireBrigadeTree({ payload }, { call, put }) {
      const { data } = yield call(qryFireBrigadeTree);
      yield put({ type: 'updateState', payload: { fireBrigadeTree: data.data } });
    },
    *qryDangerTypeNoTop({ payload }, { call, put }) {
      const { data } = yield call(qryDangerTypeNoTop);
      yield put({ type: 'updateState', payload: { dangerTypeNoTopTree: data.data } });
    },

    *qryPlottingList({ payload }, { call, put }) {
      const { data } = yield call(qryPlottingList);
      yield put({ type: 'updateState', payload: { plottingList: data.data } });
    },

    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(queryList, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            selectObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *create({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        let selectObj = yield select(state => state.keyUnit.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            dangersList: [],
            locationItem: {},
            activityKey: 'tab1',
            catalogIds: [],
            catalogNames: {},
            uuid: 0,
            fileList: [],
          },
        });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode == 200) {
        var fileList = [];
        if (data.data.fileList) {
          for (var i = 0; i < data.data.fileList.length; i++) {
            var current = data.data.fileList[i];
            if (current.url) {
              current.url = baseFileUrl + current.url;
              fileList.push(current);
            }
          }
        }

        yield put({
          type: 'updateState',
          payload: {
            modalVisible: true,
            item: data.data,
            modalType: 'update',
            locationItem: { lonLat: data.data.lonLat, dtPath: data.data.dtPath },
            dangersList: data.data.dangersList,
            fileList: fileList,
          },
        });
      }
    },

    *getUnitAccident({ payload }, { call, put }) {
      const { data } = yield call(getUnitAccident, payload);
      if (data && data.httpCode == 200) {
        yield put({
          type: 'updateState',
          payload: {
            showAccidentVisible: true,
            accidentItem: data.data,
            accidentFileList: data.data.accidentFileList,
            modalType: 'addKeyUnitAccident',
          },
        });
      }
    },

    *addKeyUnitAccident({ payload, search }, { call, put }) {
      const { data } = yield call(addKeyUnitAccident, payload);
      if (data && data.httpCode == 200) {
        yield put({
          type: 'updateState',
          payload: {
            showAccidentVisible: false,
            accidentFileList: [],
          },
        });
        yield put({ type: 'qryListByParams', payload: search });
      }
    },

    *showCreateModal({ payload }, { call, put }) {
      const { data } = yield call(getUserBrigadeId, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            brigadeId: data.data,
            modalVisible: true,
            modalType: 'create',
            locationItem: {},
            dangersList: [],
            item: {},
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *findKeyUnit({ payload }, { call, put }) {
      const { data } = yield call(getkeyUnitInfo, payload);
      if (data && data.httpCode == 200) {
        var fileList = [];
        if (data.data.fileList) {
          for (var i = 0; i < data.data.fileList.length; i++) {
            var current = data.data.fileList[i];
            if (current.url) {
              current.url = baseFileUrl + current.url;
              fileList.push(current);
            }
          }
        }

        yield put({
          type: 'updateState',
          payload: {
            findModalVisible: true,
            findItem: data.data,
            modalType: 'find',
            locationItem: { lonLat: data.data.lonLat, dtPath: data.data.dtPath },
            dangersList: data.data.dangersList,
            fileList: fileList,
            accidentFileList: data.data.accidentFileList,
          },
        });
      }
    },
    *update({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(edit, payload);
      if (data.httpCode == 200) {
        yield put({ type: 'hideButtomLoading' });
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            dangersList: [],
            locationItem: {},
            activityKey: 'tab1',
            catalogIds: [],
            catalogNames: {},
            uuid: 0,
            fileList: [],
          },
        });
        message.success('修改成功');
        let selectObj = yield select(state => state.keyUnit.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
        yield put({ type: 'hideButtomLoading' });
      }
    },
    *remove({ payload, search }, { call, put, select }) {
      const { data } = yield call(remove, payload);
      if (data.httpCode == 200) {
        message.success('删除成功');
        let selectObj = yield select(state => state.keyUnit.selectObj);
        selectObj.pageNum = 1;
        selectObj.pageSize = 10;
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
    },
    *getDocumnet({ payload }, { call, put }) {
      const { data } = yield call(planService.updateFreq, payload.id);
      if (data && data.httpCode === 200) {
        yield put({ type: 'updateState', payload: payload });
      } else {
        message.error(data.msg);
      }
    },

    *getPlans({ payload }, { call, put }) {
      const { data } = yield call(planService.getPlans, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            keyUnitId: payload,
            plansModalVisible: true,
            planList: data.data,
            modalType: 'getPlans',
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *getPlanItem({ payload }, { call, put }) {
      const { data } = yield call(planService.getInfo, payload);
      if (data && data.httpCode === 200) {
        var catalogIds = [];
        var catalogNames = {};
        var uuid = 0;
        if (
          data.data != null &&
          data.data.plansCatalogs != null &&
          data.data.plansCatalogs.length > 0
        ) {
          for (var i = 0; i < data.data.plansCatalogs.length; i++) {
            catalogIds.push(i);
            catalogNames['name_' + i] = data.data.plansCatalogs[i].name;
            catalogNames['page_' + i] = data.data.plansCatalogs[i].page;
            uuid++;
          }
        }
        var fileList = [];
        if (data.data.path) {
          var dat = {};
          dat.name = data.data.fileName;
          dat.url = baseFileUrl + data.data.path;
          dat.status = 'done';
          dat.uid = data.data.id_;
          fileList.push(dat);
        }
        yield put({
          type: 'updateState',
          payload: {
            levelName: data.data.level,
            planItemModalVisible: true,
            planItem: data.data,
            modalType: 'updatePlans',
            pfileList: fileList,
            pcatalogIds: catalogIds,
            pcatalogNames: catalogNames,
            puuid: uuid,
          },
        });
      }
    },

    *updatePlans({ payload }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(planService.edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            planItemModalVisible: false,
            fileList: [],
            planItem: {},
          },
        });
        message.success('修改成功');
        let searchObj = yield select(state => state.keyUnit.keyUnitId);
        yield put({ type: 'getPlans', payload: searchObj });
        yield put({
          type: 'updateState',
          payload: { pcatalogIds: [], pcatalogNames: {}, puuid: 0 },
        });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *createPlans({ payload }, { call, put, select }) {
      let searchObj = yield select(state => state.keyUnit.keyUnitId);
      yield put({ type: 'showButtomLoading' });
      payload.keyUnitId = searchObj;
      payload.type = 1;
      const { data } = yield call(planService.create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            planItemModalVisible: false,
            fileList: [],
            planItem: {},
          },
        });
        message.success('添加成功');
        yield put({ type: 'getPlans', payload: searchObj });
        yield put({
          type: 'updateState',
          payload: { pcatalogIds: [], pcatalogNames: {}, puuid: 0 },
        });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *delatePlan({ payload }, { call, put, select }) {
      const { data } = yield call(planService.delKeyUnitByPlanId, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        let searchObj = yield select(state => state.keyUnit.keyUnitId);
        yield put({ type: 'getPlans', payload: searchObj });
      } else {
        message.error(data.msg);
      }
    },
    *qryTypes({ payload }, { call, put }) {
      const { data } = yield call(typeService.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'updateState', payload: { typeList: data.data } });
      } else {
        message.error(data.msg);
      }
    },

    *qryPlanListByParams({ payload }, { call, put }) {
      const { data } = yield call(planService.qryNotAddedPlanList, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            planItemModalVisible: true,
            modalType: 'createPlans',
            allPlansList: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            searchObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *updateKeyUnit({ payload }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(planService.updateKeyUnit, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'updateState', payload: { planItemModalVisible: false } });
        message.success('添加成功');
        yield put({ type: 'getPlans', payload: payload.keyUnitId });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },


  },
  
  

  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (pathname === '/unit/keyUnit') {
          // dispatch({ type: 'qryPlottingList' });
          dispatch({ type: 'qryDangerTypeNoTop' });
          dispatch({ type: 'qryFireBrigadeTree' });
          dispatch({ type: 'qryListByParams', payload: { name: '' } });
        }
      });
    },
  },
};
