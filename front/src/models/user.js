import {
  query as queryUsers,
  queryCurrent,
  updatePassword,
  checkOldPassword,
} from '../services/user';
import { routerRedux } from 'dva/router';
import { message } from 'antd';
export default {
  namespace: 'user',

  state: {
    list: [],
    currentUser: {},
    editPwModalVisible: false, //修改密码弹窗
    buttomLoading: false,
  },

  effects: {
    *fetch(_, { call, put }) {
      const response = yield call(queryUsers);
      yield put({
        type: 'save',
        payload: response,
      });
    },
    *fetchCurrent(_, { call, put }) {
      const { data } = yield call(queryCurrent);
      if (data.httpCode !== 200) {
        yield put(routerRedux.push('/user/login'));
      } else {
        yield put({
          type: 'saveCurrentUser',
          payload: data.data,
        });
      }
    },
    *checkOldPassword({ payload, callback }, { call, put }) {
      const { data } = yield call(checkOldPassword, payload);
      if (data && data.httpCode === 200) {
        if (data.data) {
          callback();
        } else {
          callback('输入的原密码不正确');
        }
      } else if (data && data.httpCode === 303) {
        callback('输入的原密码不正确');
      }
    },

    *updatePassword({ payload }, { call, put }) {
      yield put({ type: 'updateState', payload: { buttomLoading: true } });
      const { data } = yield call(updatePassword, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            editPwModalVisible: false,
            confirmDirty: false,
          },
        });
        yield put(routerRedux.push('/user/login'));
        message.success('修改成功，请重新登录');
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'updateState', payload: { buttomLoading: false } });
    },
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    save(state, action) {
      return {
        ...state,
        list: action.payload,
      };
    },
    saveCurrentUser(state, action) {
      return {
        ...state,
        currentUser: action.payload,
      };
    },
    changeNotifyCount(state, action) {
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          notifyCount: action.payload,
        },
      };
    },
  },
};
