import { message } from 'antd';
import {
  qryListByParams,
  qryByType,
  qryFireBrigade,
} from '../../services/extinguisher/extinguisherUseRecord';

export default {
  namespace: 'extinguisherUseRecord',

  state: {
    list: [],
    columns: [],
    pieData: [],
    typeNames: [],
    width: 0,
    dictionaryTypes: [],
    fireBrigades: [],
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
    qryPresetCountSuc(state, { payload }) {
      return { ...state, ...payload };
    },
  },

  effects: {
    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'qryPresetCountSuc',
          payload: {
            list: data.data.dataList,
            columns: data.data.columns,
            pieData: data.data.pidDataList,
            width: data.data.width,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qryByType({ payload }, { call, put }) {
      const { data } = yield call(qryByType, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'qryPresetCountSuc',
          payload: {
            dictionaryTypes: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qryFireBrigade({}, { call, put }) {
      const { data } = yield call(qryFireBrigade);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'qryPresetCountSuc',
          payload: {
            fireBrigades: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/extinguisher/extinguisherUseRecord') {
          dispatch({
            type: 'qryListByParams',
            payload: {},
          });
          dispatch({
            type: 'qryByType',
          });
          dispatch({
            type: 'qryFireBrigade',
          });
        }
      });
    },
  },
};
