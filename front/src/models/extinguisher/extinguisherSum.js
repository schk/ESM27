import { message } from 'antd';
import { qryByType, qryFireBrigade } from '../../services/extinguisher/extinguisherUseRecord';
import { qrySumByKeyId, create } from '../../services/extinguisher/extinguisher';

export default {
  namespace: 'extinguisherSum',

  state: {
    sumList: [],
    colums: [],
    dictionaryTypes: [],
    fireBrigades: [],
    width: 200,
    quantity: 0,
    carQuantity: 0,
    totalUsedAmount: 0,
    charDataList: [],
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *qryByType({ payload }, { call, put }) {
      const { data } = yield call(qryByType, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            dictionaryTypes: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qryFireBrigade({}, { call, put }) {
      const { data } = yield call(qryFireBrigade);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            fireBrigades: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qrySumByKeyId({ payload }, { call, put }) {
      const { data } = yield call(qrySumByKeyId, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            sumList: data.data.dataList,
            colums: data.data.columns,
            width: data.data.width,
            totalUsedAmount: data.data.totalUsedAmount,
            quantity: data.data.quantity,
            carQuantity: data.data.carQuantity,
            charDataList: data.data.charDataList,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/extinguisher/extinguisherSum') {
          dispatch({
            type: 'qrySumByKeyId',
            payload: {},
          });
          dispatch({
            type: 'qryByType',
          });
          dispatch({
            type: 'qryFireBrigade',
          });
        }
      });
    },
  },
};
