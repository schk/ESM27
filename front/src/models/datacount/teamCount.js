import { message } from 'antd';
import { qryFireBrigade } from '../../services/extinguisher/extinguisherUseRecord';
import { qryTeamCount } from '../../services/dataCount/dataCount';

export default {
  namespace: 'teamCount',

  state: {
    data: [],
    fireBrigades: [],
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *qryFireBrigade({}, { call, put }) {
      const { data } = yield call(qryFireBrigade);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            fireBrigades: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *queryData({ payload }, { call, put }) {
      const { data } = yield call(qryTeamCount, payload);
      let isEmpty = true;
      if (data && data.httpCode === 200) {
        if (data.data != null && data.data.length > 0) {
          for (var i = 0; i < data.data.length; i++) {
            if (data.data[i].value > 0) {
              isEmpty = false;
              break;
            }
          }
        }
        yield put({
          type: 'updateState',
          payload: {
            data: isEmpty ? [] : data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/dataCount/teamCount') {
          dispatch({
            type: 'qryFireBrigade',
          });
          dispatch({
            type: 'queryData',
            payload: {},
          });
        }
      });
    },
  },
};
