import { message } from 'antd';
import { qryFireBrigade } from '../../services/extinguisher/extinguisherUseRecord';
import { qryKeyUnitCount } from '../../services/dataCount/dataCount';
var flag = 0;
export default {
  namespace: 'keyunitcount',

  state: {
    list: [],
    fireBrigades: [],
    charData: [],
    fields: [],
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *qryFireBrigade({}, { call, put }) {
      const { data } = yield call(qryFireBrigade);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            fireBrigades: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *queryData({ payload }, { call, put }) {
      const { data } = yield call(qryKeyUnitCount, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            charData: data.data.dataList,
            fields: data.data.fields,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/dataCount/keyUnitCount') {
          dispatch({
            type: 'qryFireBrigade',
          });
          dispatch({
            type: 'queryData',
            payload: {},
          });
        }
      });
    },
  },
};
