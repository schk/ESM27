import { message } from 'antd';
import { qryFireBrigade } from '../../services/extinguisher/extinguisherUseRecord';
import { qryPoliceNumber, getYearList } from '../../services/dataCount/dataCount';

export default {
  namespace: 'policeNumber',

  state: {
    data: [],
    fireBrigades: [],
    yearList: [],
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *qryFireBrigade({}, { call, put }) {
      const { data } = yield call(qryFireBrigade);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            fireBrigades: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *getYearList({}, { call, put }) {
      const { data } = yield call(getYearList);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            yearList: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *queryData({ payload }, { call, put }) {
      console.log(payload);
      const { data } = yield call(qryPoliceNumber, payload);

      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            data: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/dataCount/policeNumber') {
          dispatch({
            type: 'qryFireBrigade',
          });
          dispatch({
            type: 'getYearList',
          });
          dispatch({
            type: 'queryData',
            payload: {},
          });
        }
      });
    },
  },
};
