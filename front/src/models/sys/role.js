import { message } from 'antd';
import * as userService from '../../services/sys/role';

export default {
  namespace: 'role',

  state: {
    list: [],
    pagination: {},
    modalVisible: false,
    buttomLoading: false,
    modalType: null,
    item: {},
    total: 0,
    pageSize: 10,
    current: 1,
    permissionVisible: false,
    loading: true,
    currentItem: {},
    allPermission: [],
    rolePermissions: [],
    rolePermissionsNoP: [],
    selectPermissions: [], //角色选择的权限
    newKey: '',
    searchObj: {},
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtonLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtonLoading(state) {
      return { ...state, buttomLoading: false };
    },
    hidePermissionModal(state, action) {
      return {
        ...state,
        ...action.payload,
        permissionVisible: false,
        rolePermissions: [],
        selectPermissions: [],
        rolePermissionsNoP: [],
      };
    },
    showPermissionModal(state, action) {
      return {
        ...state,
        ...action.payload,
        newKey: new Date().getTime() + '',
        permissionVisible: true,
      };
    },
  },

  effects: {
    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(userService.qryListByParams, payload);
      if (data && data.httpCode == 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            total: data.iTotalRecords,
            pageSize: data.size,
            current: data.current,
            pages: data.pages,
            searchObj: payload,
          },
        });
      } else {
        message.error(data.errorMsg);
      }
    },

    *create({ payload }, { call, put, select }) {
      yield put({ type: 'showButtonLoading' });
      const { data } = yield call(userService.addUser, payload);
      if (data.httpCode == 200) {
        yield put({ type: 'hideButtonLoading' });
        yield put({ type: 'updateState', payload: { modalVisible: false } });
        message.success('新建成功');
        let searchObj = yield select(state => state.role.searchObj);
        yield put({ type: 'qryListByParams', payload: searchObj });
      } else {
        message.error(data.msg);
        yield put({ type: 'hideButtonLoading' });
      }
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(userService.getInfo, payload);
      if (data && data.httpCode == 200) {
        yield put({
          type: 'updateState',
          payload: { modalVisible: true, item: data.data, modalType: 'update' },
        });
      }
    },

    *update({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtonLoading' });
      const { data } = yield call(userService.updateUser, payload);
      if (data.httpCode == 200) {
        yield put({ type: 'hideButtonLoading' });
        yield put({ type: 'updateState', payload: { modalVisible: false } });
        message.success('修改成功');
        let searchObj = yield select(state => state.role.searchObj);
        yield put({ type: 'qryListByParams', payload: searchObj });
      } else {
        message.error(data.msg);
        yield put({ type: 'hideButtonLoading' });
      }
    },

    *del({ payload, search }, { call, put }) {
      const { data } = yield call(userService.delUser, payload);
      if (data.httpCode == 200) {
        message.success('删除成功');
        yield put({ type: 'qryListByParams', payload: search });
      } else {
        message.error(data.msg);
      }
    },
    *setPermission({ payload, form, search }, { call, put, select }) {
      yield put({ type: 'showButtonLoading' });
      const { data } = yield call(userService.setRolePermission, payload);
      if (data && data.httpCode == 200) {
        message.success('设置角色权限成功');
        yield put({ type: 'hideButtonLoading' });
        yield put({ type: 'hidePermissionModal' });
        form.resetFields();
        let searchObj = yield select(state => state.role.searchObj);
        yield put({ type: 'qryListByParams', payload: searchObj });
      } else {
        yield put({ type: 'hideButtonLoading' });
        message.error('设置角色权限失败');
      }
    },
    *getAllPermission({}, { call, put }) {
      const { data } = yield call(userService.getAllPermission);
      if (data && data.httpCode == 200) {
        yield put({ type: 'updateState', payload: { allPermission: data.data } });
      }
    },
    *qryRolePermission({ payload }, { call, put }) {
      const { data } = yield call(userService.qryRolePermissionNoP, payload.id);
      if (data.httpCode == 200) {
        yield put({
          type: 'updateState',
          payload: { rolePermissionsNoP: data.data},
        });
        const perData = yield call(userService.qryRolePermission, payload.id);
        if (perData.data.httpCode == 200) {
          yield put({
            type: 'showPermissionModal',
            payload: { rolePermissions: perData.data.data,selectPermissions:perData.data.data, modalType: 'permission', item: payload.item },
          });
        }
      }
      
    },
    
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/sys/role') {
          dispatch({ type: 'qryListByParams', payload: { pageNum: 1, pageSize: 10 } });
          dispatch({ type: 'getAllPermission', payload: {} });
        }
      });
    },
  },
};
