import { message } from 'antd';
import * as logService from '../../services/sys/log';

export default {
  namespace: 'log',

  state: {
    list: [],
    pagination: {},
    modalVisible: false,
    buttomLoading: false,
    modalType: null,
    total: 0,
    pageSize: 10,
    current: 1,
    currentItem: {},
  },

  reducers: {
    updateState(state, { payload }) {
      return { ...state, ...payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *qryListByParam({ payload }, { call, put }) {
      const { data } = yield call(logService.qryListByParam, payload);
      if (data && data.httpCode == 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            total: data.iTotalRecords,
            pageSize: data.size,
            current: data.current,
            pages: data.pages,
          },
        });
      } else {
        message.error(data.errorMsg);
      }
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(logService.getInfo, payload);
      if (data && data.httpCode == 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: true,
            currentItem: data.data,
            modalType: 'update',
          },
        });
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/sys/log') {
          dispatch({
            type: 'qryListByParam',
            payload: { pageNum: 1, pageSize: 10 },
          });
        }
      });
    },
  },
};
