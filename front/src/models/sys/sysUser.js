import * as userService from '../../services/sys/sysUser';
import { getUserBrigadeId } from '../../services/user';
import { qryDeptsNoTop } from '../../services/sys/department';
import { qryFireBrigadeTree } from '../../services/emergency/fireBrigade';
import { message } from 'antd';
import { find } from '../../services/plans/plansList';
export default {
  namespace: 'sysUser',
  state: {
    deptTree: [],
    deptNoTopTree: [],
    userList: [],
    roleList: [],
    current: 1,
    pageSize: 10,
    total: 0,
    fireBrigadeId: undefined,
    currentItem: {},
    modalVisible: false,
    findModalVisible: false,
    buttonLoading: false,
    modalType: 'create',
    newKey: '',
    brigadeId: null,
    searchObj: {},
  },
  reducers: {
    updateState(state, { payload }) {
      return { ...state, ...payload };
    },
    showModal(state, { payload }) {
      return { ...state, ...payload, newKey: new Date().getTime() + '', modalVisible: true };
    },
    hideModal(state) {
      return { ...state, modalVisible: false, newKey: new Date().getTime() + '' };
    },
    showButtonLoading(state, action) {
      return { ...state, ...action, buttomLoading: true };
    },
    hideButtonLoading(state, action) {
      return { ...state, ...action, buttomLoading: false };
    },
  },
  effects: {
    *qryDeptsNoTop({ payload }, { call, put }) {
      const { data } = yield call(qryDeptsNoTop);
      yield put({ type: 'updateState', payload: { deptNoTopTree: data.data } });
    },
    *qryFireBrigadeTree({ payload }, { call, put }) {
      const { data } = yield call(qryFireBrigadeTree);
      yield put({ type: 'updateState', payload: { deptTree: data.data } });
    },
    *qryUserByParams({ payload }, { call, put }) {
      const { data } = yield call(userService.qryUserByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            userList: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            fireBrigadeId: payload.fireBrigadeId,
            brigadeId: payload.fireBrigadeId,
            searchObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qryRoles({ payload }, { call, put }) {
      const { data } = yield call(userService.qryRoleOption, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'updateState', payload: { roleList: data.data } });
      } else {
        message.error(data.msg);
      }
    },
    *create({ payload }, { call, put, select }) {
      yield put({ type: 'showButtonLoading' });
      const { data } = yield call(userService.create, payload);
      if (data && data.httpCode === 200) {
        let searchObj = yield select(state => state.sysUser.searchObj);
        yield put({ type: 'qryUserByParams', payload: searchObj });
        yield put({ type: 'hideModal' });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtonLoading' });
    },
    *qryUserById({ payload }, { call, put }) {
      const { data } = yield call(userService.getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'getUserBrigadeId', payload: {} });
        yield put({
          type: 'showModal',
          payload: { currentItem: data.data, modalType: 'edit', title: '编辑用户' },
        });
      } else {
        message.error(data.msg);
      }
    },

    *findInfo({ payload }, { call, put }) {
      const { data } = yield call(userService.findInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { currentItem: data.data, findModalVisible: true, title: '查看信息' },
        });
      } else {
        message.error(data.msg);
      }
    },
    *getUserBrigadeId({ payload }, { call, put }) {
      const { data } = yield call(getUserBrigadeId, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            fireBrigadeId: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *showCreateModal({ payload }, { call, put, select }) {
      yield put({ type: 'showButtonLoading' });
      let brigadeId = yield select(state => state.sysUser.brigadeId);
      if (!brigadeId) {
        const { data } = yield call(getUserBrigadeId, payload);
        if (data && data.httpCode === 200) {
          yield put({
            type: 'showModal',
            payload: {
              fireBrigadeId: data.data,
              modalVisible: true,
              item: {},
              modalType: 'create',
              currentItem: {},
            },
          });
        } else {
          message.error(data.msg);
        }
      } else {
        yield put({
          type: 'showModal',
          payload: {
            modalVisible: true,
            item: {},
            modalType: 'create',
            currentItem: {},
          },
        });
      }
    },
    *edit({ payload }, { call, put, select }) {
      yield put({ type: 'showButtonLoading' });
      const { data } = yield call(userService.edit, payload);
      if (data && data.httpCode === 200) {
        let searchObj = yield select(state => state.sysUser.searchObj);
        yield put({ type: 'qryUserByParams', payload: searchObj });
        yield put({ type: 'hideModal' });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtonLoading' });
    },
    *remove({ payload, search }, { call, put, select }) {
      const { data } = yield call(userService.remove, payload);
      if (data && data.httpCode === 200) {
        let searchObj = yield select(state => state.sysUser.searchObj);
        searchObj.pageNum = 1;
        searchObj.pageSize = 10;
        yield put({ type: 'qryUserByParams', payload: searchObj });
        yield put({ type: 'qryFiBrigade' });
      } else {
        message.error(data.msg);
      }
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (pathname === '/sys/sysUser') {
          dispatch({ type: 'qryDeptsNoTop' });
          dispatch({ type: 'qryFireBrigadeTree' });
          dispatch({ type: 'qryUserByParams', payload: {} });
          dispatch({ type: 'qryRoles' });
        }
      });
    },
  },
};
