import { message } from 'antd';
import {
  qryListByParams,
  create,
  getInfo,
  edit,
  del,
  updateFreq,
  importExcelConfirm,
  delExcelTemp,
} from '../../services/plans/plansList';
import * as typeService from '../../services/dictionary';
import * as keyPartsService from '../../services/equipment/keyParts';
import { qryKeyUnitList } from '../../services/keyUnit';
import { qryFireBrigadeTree } from '../../services/emergency/fireBrigade';
import { baseFileUrl } from '../../config/system';
import { getUserBrigadeId } from '../../services/user';
export default {
  namespace: 'plansList',

  state: {
    list: [],
    modalVisible: false,
    buttomLoading: false,
    modalType: null,
    item: {},
    total: 0,
    pageSize: 10,
    current: 1,
    keyUnitList: [],
    fileList: [],
    typeList: [],
    documnetModalVisible: false,
    filePath: null,
    newKey: null,
    fireBrigadeTree: [],
    catalogIds: [],
    catalogNames: {},
    uuid: 0,
    type: '1',
    levelName: '',
    findModalVisible: false,
    searchObj: {},
    keyPartsList: [],
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *qryFireBrigadeTree({ payload }, { call, put }) {
      const { data } = yield call(qryFireBrigadeTree);
      yield put({ type: 'updateState', payload: { fireBrigadeTree: data.data } });
    },
    *qryKeyUnitList({ payload }, { call, put }) {
      const { data } = yield call(qryKeyUnitList, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { keyUnitList: data.data },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qryKeyPartsList({ payload }, { call, put }) {
      const { data } = yield call(keyPartsService.qryKeyPartsList, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { keyPartsList: data.data },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            searchObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *create({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            fileList: [],
          },
        });
        message.success('新建成功');
        let searchObj = yield select(state => state.plansList.searchObj);
        yield put({ type: 'qryListByParams', payload: searchObj });
        yield put({
          type: 'updateState',
          payload: { catalogIds: [], catalogNames: {}, uuid: 0 },
        });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        debugger;
        var catalogIds = [];
        var catalogNames = {};
        var uuid = 0;
        if (
          data.data != null &&
          data.data.plansCatalogs != null &&
          data.data.plansCatalogs.length > 0
        ) {
          for (var i = 0; i < data.data.plansCatalogs.length; i++) {
            catalogIds.push(i);
            catalogNames['name_' + i] = data.data.plansCatalogs[i].name;
            catalogNames['page_' + i] = data.data.plansCatalogs[i].page;
            uuid++;
          }
        }
        var fileList = [];
        if (data.data.path) {
          var dat = {};
          dat.name = data.data.fileName;
          dat.url = baseFileUrl + data.data.path;
          dat.status = 'done';
          dat.uid = data.data.id_;
          fileList.push(dat);
        }
        yield put({
          type: 'updateState',
          payload: {
            levelName: data.data.level,
            modalVisible: true,
            item: data.data,
            type: data.data.type,
            modalType: 'update',
            fileList: fileList,
            catalogIds: catalogIds,
            catalogNames: catalogNames,
            uuid: uuid,
          },
        });
      }
    },
    *findInfo({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        var catalogIds = [];
        var catalogNames = {};
        var uuid = 0;
        if (
          data.data != null &&
          data.data.plansCatalogs != null &&
          data.data.plansCatalogs.length > 0
        ) {
          for (var i = 0; i < data.data.plansCatalogs.length; i++) {
            catalogIds.push(i);
            catalogNames['name_' + i] = data.data.plansCatalogs[i].name;
            catalogNames['page_' + i] = data.data.plansCatalogs[i].page;
            uuid++;
          }
        }
        var fileList = [];
        if (data.data.path) {
          var dat = {};
          dat.name = data.data.fileName;
          dat.url = baseFileUrl + data.data.path;
          dat.status = 'done';
          dat.uid = data.data.id_;
          fileList.push(dat);
        }
        yield put({
          type: 'updateState',
          payload: {
            findModalVisible: true,
            item: data.data,
            fileList: fileList,
            catalogIds: catalogIds,
            catalogNames: catalogNames,
            uuid: uuid,
          },
        });
      }
    },
    *showCreateModal({ payload }, { call, put }) {
      const { data } = yield call(getUserBrigadeId, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            brigadeId: data.data,
            modalVisible: true,
            modalType: 'create',
            fileList: [],
            item: {},
            catalogIds: [],
            catalogNames: {},
            uuid: 0,
            type: '4',
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *update({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            fileList: [],
          },
        });
        message.success('修改成功');
        let searchObj = yield select(state => state.plansList.searchObj);
        yield put({ type: 'qryListByParams', payload: searchObj });
        yield put({ type: 'updateState', payload: { catalogIds: [], catalogNames: {}, uuid: 0 } });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *del({ payload, search }, { call, put, select }) {
      const { data } = yield call(del, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        let searchObj = yield select(state => state.plansList.searchObj);
        searchObj.pageNum = 1;
        searchObj.pageSize = 10;
        yield put({ type: 'qryListByParams', payload: searchObj });
      } else {
        message.error(data.msg);
      }
    },

    *getDocumnet({ payload }, { call, put }) {
      const { data } = yield call(updateFreq, payload.id);
      if (data && data.httpCode === 200) {
        yield put({ type: 'updateState', payload: payload });
      } else {
        message.error(data.msg);
      }
    },

    *qryTypes({ payload }, { call, put }) {
      const { data } = yield call(typeService.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'updateState', payload: { typeList: data.data } });
      } else {
        message.error(data.msg);
      }
    },

    *importExcelConfirm({ payload, search }, { call, put }) {
      const { data } = yield call(importExcelConfirm, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'qryTypes', payload: 3 });
        yield put({ type: 'qryKeyUnitList' });
        yield put({ type: 'qryFireBrigadeTree' });
        yield put({ type: 'qryListByParams', payload: {} });
      } else {
        message.error(data.msg);
      }
    },

    *delExcelTemp({ payload, search }, { call, put }) {
      const { data } = yield call(delExcelTemp, payload);
      if (data && data.httpCode === 200) {
      } else {
        message.error(data.msg);
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/plans/plansList') {
          dispatch({ type: 'qryTypes', payload: 3 });
          dispatch({ type: 'qryKeyUnitList' });
          dispatch({ type: 'qryKeyPartsList' });
          dispatch({ type: 'qryFireBrigadeTree' });
          dispatch({ type: 'qryListByParams', payload: {} });
        }
      });
    },
  },
};
