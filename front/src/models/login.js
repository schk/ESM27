import { message } from 'antd';
import { routerRedux } from 'dva/router';
import { fakeAccountLogin, fakeAccountLoginOut } from '../services/sys/sysUser';
import { setAuthority } from '../utils/authority';
import { reloadAuthorized } from '../utils/Authorized';

export default {
  namespace: 'login',

  state: {
    status: undefined,
    loginLoading: false,
    user: {},
    confirmLoading: false,
    newKey: '',
  },

  effects: {
    *login({ payload }, { call, put }) {
      yield put({ type: 'showLoginLoading' });
      const response = yield call(fakeAccountLogin, payload);
      // console.log(response.data.data);
      yield put({ type: 'hideLoginLoading' });
      if (response.data.httpCode === 200) {
        yield put({
          type: 'changeLoginStatus',
          payload: response.data,
        });
        yield put({
          type: 'user/saveCurrentUser',
          payload: response.data.data,
        });
        localStorage.setItem('user', JSON.stringify(response.data));
        reloadAuthorized();
        yield put(routerRedux.push('/plans/plansList'));
      } else {
        message.error(response.data.msg);
      }
    },

    *logout({}, { call, put }) {
      const { data } = yield call(fakeAccountLoginOut);
      if (data.httpCode === 200) {
        localStorage.removeItem('user');
        localStorage.removeItem('antd-pro-authority');
        yield put({
          type: 'changeLoginStatus',
          payload: {
            status: false,
            currentAuthority: 'guest',
          },
        });
        reloadAuthorized();
        yield put(routerRedux.push('/user/login'));
      }
    },
  },

  reducers: {
    changeLoginStatus(state, { payload }) {
      if (payload.httpCode === 303) {
        return {
          ...state,
          status: 'error',
          type: 'account',
        };
      } else {
        setAuthority(payload.data.permset);
        return {
          ...state,
          status: 'ok',
          type: 'account',
        };
      }
    },
    showLoginLoading(state) {
      return {
        ...state,
        loginLoading: true,
      };
    },
    hideLoginLoading(state) {
      return {
        ...state,
        loginLoading: false,
      };
    },
  },
};
