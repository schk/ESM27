import {
  qryListByParams,
  create,
  getInfo,
  edit,
  del,
  importExpertConfirm,
  delExpertTemp,
} from '../../services/expert/expert';
import * as expertTypeService from '../../services/expert/expertType';
import { message } from 'antd';
export default {
  namespace: 'expert',
  state: {
    list: [],
    current: 1,
    pageSize: 10,
    total: 0,
    typeId: undefined,
    item: {},
    modalVisible: false,
    buttomLoading: false,
    modalType: 'create',
    msdsPath: '',
    newKey: '',
    typeList: [],
    selectObj: {},
    findModalVisible: false,
  },
  reducers: {
    updateState(state, { payload }) {
      return { ...state, ...payload };
    },
    showModal(state, { payload }) {
      return { ...state, ...payload, newKey: new Date().getTime() + '', modalVisible: true };
    },
    hideModal(state) {
      return { ...state, modalVisible: false, newKey: new Date().getTime() + '' };
    },
    showButtomLoading(state, action) {
      return { ...state, ...action, buttomLoading: true };
    },
    hideButtomLoading(state, action) {
      return { ...state, ...action, buttomLoading: false };
    },
  },
  effects: {
    *qryTypeList({ payload }, { call, put }) {
      const { data } = yield call(expertTypeService.qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            typeList: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            typeId: payload.typeId,
            selectObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *create({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        let selectObj = yield select(state => state.expert.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
        yield put({ type: 'hideModal' });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'showModal',
          payload: { item: data.data, modalType: 'edit', title: '编辑用户' },
        });
      } else {
        message.error(data.msg);
      }
    },

    *findInfo({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: { item: data.data, findModalVisible: true },
        });
      } else {
        message.error(data.msg);
      }
    },

    *edit({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtonLoading' });
      const { data } = yield call(edit, payload);
      if (data && data.httpCode === 200) {
        let selectObj = yield select(state => state.expert.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
        yield put({ type: 'hideModal' });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },
    *remove({ payload, search }, { call, put, select }) {
      const { data } = yield call(del, payload);
      if (data && data.httpCode === 200) {
        let selectObj = yield select(state => state.expert.selectObj);
        selectObj.pageNum = 1;
        selectObj.pageSize = 10;
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
    },
    *importExpertConfirm({ payload, search }, { call, put }) {
      const { data } = yield call(importExpertConfirm, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'qryTypeList', payload: {} });
        yield put({ type: 'qryListByParams', payload: {} });
      } else {
        message.error(data.msg);
      }
    },

    *delExpertTemp({ payload, search }, { call, put }) {
      const { data } = yield call(delExpertTemp, payload);
      if (data && data.httpCode === 200) {
      } else {
        message.error(data.msg);
      }
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (pathname === '/expert/expertList') {
          dispatch({ type: 'qryTypeList', payload: {} });
          dispatch({ type: 'qryListByParams', payload: { pageNum: 1, pageSize: 10 } });
        }
      });
    },
  },
};
