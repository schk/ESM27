import { message } from 'antd';
import { qryListByParams, create, getInfo, edit, del } from '../../services/expert/expertType';

export default {
  namespace: 'expertType',

  state: {
    list: [],
    modalVisible: false,
    buttomLoading: false,
    modalType: null,
    item: {},
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *create({ payload }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
          },
        });
        message.success('新建成功');
        yield put({ type: 'qryListByParams' });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: true,
            item: data.data,
            modalType: 'update',
          },
        });
      }
    },

    *update({ payload }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
          },
        });
        message.success('修改成功');
        yield put({ type: 'qryListByParams' });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *del({ payload, search }, { call, put }) {
      const { data } = yield call(del, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        yield put({ type: 'qryListByParams', payload: search });
      } else {
        message.error(data.msg);
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/expert/expertType') {
          dispatch({
            type: 'qryListByParams',
            payload: {},
          });
        }
      });
    },
  },
};
