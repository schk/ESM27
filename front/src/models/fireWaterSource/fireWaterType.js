import { create, getInfo, edit, del } from '../../services/car/carType';
import { qryByType } from '../../services/fireWaterSource/waterSource';
import { qryPlottingList } from '../../services/plotting/plotting';
import { message } from 'antd';
export default {
  namespace: 'fireWaterType',
  state: {
    list: [],
    modalVisible: false,
    buttomLoading: false,
    modalType: null,
    item: {},
    total: 0,
    pageSize: 10,
    current: 1,
    selectObj: {},
    plottingList: [],
    locationItem: {},
  },
  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },
  effects: {
    *qryPlottingList({ payload }, { call, put }) {
      const { data } = yield call(qryPlottingList);
      yield put({ type: 'updateState', payload: { plottingList: data.data } });
    },
    *qryByType({ payload }, { call, put }) {
      const { data } = yield call(qryByType, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            selectObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *create({ payload, search }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            locationItem: {},
          },
        });
        message.success('新建成功');
        yield put({ type: 'qryByType', payload: search });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },
    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: true,
            item: data.data,
            modalType: 'update',
          },
        });
      }
    },

    *update({ payload, search }, { call, put }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            locationItem: {},
          },
        });
        message.success('修改成功');
        yield put({ type: 'qryByType', payload: search });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *del({ payload, search }, { call, put }) {
      const { data } = yield call(del, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'qryByType', payload: search });
      } else {
        message.error(data.msg);
      }
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (pathname === '/water/waterSourceType') {
          dispatch({ type: 'qryPlottingList' });
          dispatch({ type: 'qryByType', payload: 6 });
        }
      });
    },
  },
};
