import { message } from 'antd';
import { getUserBrigadeId } from '../../services/user';
import {
  qryListByParams,
  create,
  getInfo,
  edit,
  del,
  getDeptName,
  qryType,
  importConfirm,
  delTemp,
} from '../../services/conventionalEquip/specialDutyEquip';
import { baseFileUrl } from '../../config/system';
import { qryFireBrigadeTree } from '../../services/emergency/fireBrigade';
export default {
  namespace: 'specialDutyEquip',

  state: {
    list: [],
    modalVisible: false,
    modalShowFile: false,
    buttomLoading: false,
    item: {},
    total: 0,
    pageSize: 10,
    current: 1,
    pictureFileList: [],
    technicalFileList: [],
    systemFileList: [],
    modalType: 'create',
    msdsPath: '',
    newKey: '',
    fireBrigadeTree: [],
    selectObj: {},
    equipType: 1,
    brigadeId: null,
    typeList: [],
    compute: 1,
  },

  reducers: {
    updateState(state, action) {
      return { ...state, ...action.payload };
    },
    showButtomLoading(state) {
      return { ...state, buttomLoading: true };
    },
    hideButtomLoading(state) {
      return { ...state, buttomLoading: false };
    },
  },

  effects: {
    *importConfirm({ payload, search }, { call, put }) {
      const { data } = yield call(importConfirm, payload);
      if (data && data.httpCode === 200) {
        yield put({ type: 'qryType', payload: 2 });
        yield put({ type: 'qryListByParams', payload: { pageNum: 1, equipType: 1, pageSize: 10 } });
      } else {
        message.error(data.msg);
      }
    },

    *delTemp({ payload, search }, { call, put }) {
      const { data } = yield call(delTemp, payload);
      if (data && data.httpCode === 200) {
      } else {
        message.error(data.msg);
      }
    },

    *qryFireBrigadeTree({ payload }, { call, put }) {
      const { data } = yield call(qryFireBrigadeTree, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            fireBrigadeTree: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qryType({ payload }, { call, put }) {
      const { data } = yield call(qryType, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            typeList: data.data,
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *qryListByParams({ payload }, { call, put }) {
      const { data } = yield call(qryListByParams, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.data,
            current: data.current,
            pageSize: data.size,
            total: data.iTotalRecords,
            equipType: 1,
            selectObj: payload,
          },
        });
      } else {
        message.error(data.msg);
      }
    },

    *create({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(create, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            pictureFileList: [],
            technicalFileList: [],
            systemFileList: [],
          },
        });
        message.success('新建成功');
        let selectObj = yield select(state => state.specialDutyEquip.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *info({ payload }, { call, put }) {
      const { data } = yield call(getInfo, payload);
      if (data && data.httpCode === 200) {
        var pictureFileList1 = [];
        var technicalFileList1 = [];
        var systemFileList1 = [];
        if (data.data.picture || data.data.systemRules || data.data.technicalParamFile) {
          var dat = {};
          var dat1 = {};
          var dat2 = {};
          dat.name = data.data.picture;
          dat.url = baseFileUrl + data.data.picture;
          dat.thumbUrl = baseFileUrl + data.data.picture;
          dat.status = 'done';
          dat.uid = -1;

          dat1.name = data.data.technicalParamFile;
          dat1.url = baseFileUrl + data.data.technicalParamFile;
          dat1.thumbUrl = baseFileUrl + data.data.technicalParamFile;
          dat1.status = 'done';
          dat1.uid = -2;

          dat2.name = data.data.systemRules;
          dat2.url = baseFileUrl + data.data.systemRules;
          dat2.thumbUrl = baseFileUrl + data.data.systemRules;
          dat2.status = 'done';
          dat2.uid = -3;
          pictureFileList1.push(dat);
          technicalFileList1.push(dat1);
          systemFileList1.push(dat2);
        }
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: true,
            newKey: new Date().getTime + '',
            item: data.data,
            compute: data.data.compute ? data.data.compute : 2,
            modalType: 'update',
            pictureFileList: pictureFileList1,
            technicalFileList: technicalFileList1,
            systemFileList: systemFileList1,
          },
        });
      }
    },
    *showCreateModal({ payload }, { call, put }) {
      const { data } = yield call(getUserBrigadeId, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            brigadeId: data.data,
            modalVisible: true,
            modalType: 'create',
            item: {},
          },
        });
      } else {
        message.error(data.msg);
      }
    },
    *infoFile({ payload }, { call, put }) {
      const { data } = yield call(getDeptName, payload);
      if (data && data.httpCode === 200) {
        var pictureFileList1 = [];
        var technicalFileList1 = [];
        var systemFileList1 = [];
        if (data.data.picture || data.data.systemRules || data.data.technicalParamFile) {
          var dat = {};
          var dat1 = {};
          var dat2 = {};
          dat.name = data.data.picture;
          dat.url = baseFileUrl + data.data.picture;
          dat.thumbUrl = baseFileUrl + data.data.picture;
          dat.status = 'done';
          dat.uid = -1;

          dat1.name = data.data.technicalParamFile;
          dat1.url = baseFileUrl + data.data.technicalParamFile;
          dat1.thumbUrl = baseFileUrl + data.data.technicalParamFile;
          dat1.status = 'done';
          dat1.uid = -2;

          dat2.name = data.data.systemRules;
          dat2.url = baseFileUrl + data.data.systemRules;
          dat2.thumbUrl = baseFileUrl + data.data.systemRules;
          dat2.status = 'done';
          dat2.uid = -3;
          pictureFileList1.push(dat);
          technicalFileList1.push(dat1);
          systemFileList1.push(dat2);
        }
        yield put({
          type: 'updateState',
          payload: {
            modalShowFile: true,
            item: data.data,
            modalType: 'update',
            pictureFileList: pictureFileList1,
            technicalFileList: technicalFileList1,
            systemFileList: systemFileList1,
          },
        });
      }
    },

    *update({ payload, search }, { call, put, select }) {
      yield put({ type: 'showButtomLoading' });
      const { data } = yield call(edit, payload);
      if (data && data.httpCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            modalVisible: false,
            pictureFileList: [],
            technicalFileList: [],
            systemFileList: [],
            compute: 1,
          },
        });
        message.success('修改成功');
        let selectObj = yield select(state => state.specialDutyEquip.selectObj);
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
      yield put({ type: 'hideButtomLoading' });
    },

    *del({ payload, search }, { call, put, select }) {
      const { data } = yield call(del, payload);
      if (data && data.httpCode === 200) {
        message.success('删除成功');
        let selectObj = yield select(state => state.specialDutyEquip.selectObj);
        selectObj.pageNum = 1;
        selectObj.pageSize = 10;
        yield put({ type: 'qryListByParams', payload: selectObj });
      } else {
        message.error(data.msg);
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/conventionalEquip/specialDutyEquip') {
          dispatch({
            type: 'qryListByParams',
            payload: { pageNum: 1, pageSize: 10, equipType: 1 },
          });
          dispatch({ type: 'qryFireBrigadeTree' });
          dispatch({ type: 'qryType', payload: 2 });
        }
      });
    },
  },
};
