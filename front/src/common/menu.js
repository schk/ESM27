import { isUrl, inArray } from '../utils/utils';
import { getAuthority } from '../utils/authority';
const permissionList = getAuthority() != '' ? getAuthority().split(',') : [];
console.log('*******************' );
console.log(permissionList);
let a = inArray('projectinfo', permissionList) > -1;
const menuData = [
  
  {
    name: '应急预案管理',
    icon: 'camera-o',
    path: 'plans',
    authority: 'plans',
    children: [
      {
        name: '应急预案',
        icon: 'shrink',
        path: 'plansList',
        authority: 'plansList',
      },
      {
        name: '预案类型',
        icon: 'shrink',
        path: 'plansType',
        authority: 'plansType',
      },
    ],
  },
  {
    name: '应急专家',
    icon: 'team',
    path: 'expert',
    children: [
      {
        name: '应急专家',
        icon: 'shrink',
        path: 'expertList',
        authority: 'expertList',
      },
      {
        name: '应急专家类型',
        icon: 'shrink',
        path: 'expertType',
        authority: 'expertType',
      },
    ],
  },

  {
    name: '危化品管理',
    icon: 'exclamation-circle-o',
    path: 'dangers',
    authority: 'dangers',
    children: [
      {
        name: '危化品库',
        icon: 'shrink',
        path: 'dangersList',
        authority: 'dangersList',
      },
      {
        name: '危化品类型',
        icon: 'shrink',
        path: 'dangersType',
        authority: 'dangersType',
      },
    ],
  },
  {
    name: '应急资源管理',
    icon: 'rocket',
    path: 'emergency',
    authority: 'emergency',
    children: [
      {
        name: '应急队伍',
        icon: 'shrink',
        path: 'teams',
        authority: 'teams',
      },
      {
        name: '消防队',
        icon: 'shrink',
        path: 'fireBrigade',
        authority: 'fireBrigade',
      },
      {
        name: '应急物资',
        icon: 'shrink',
        path: 'emergencyMaterial',
        authority: 'emergencyMaterial',
      },
      {
        name: '应急物资类型',
        icon: 'shrink',
        path: 'suppliesType',
        authority: 'suppliesType',
      },
      {
        name: '物资储备点',
        icon: 'shrink',
        path: 'reservePoint',
        authority: 'reservePoint',
      },
    ],
  },
  {
    name: '应急装备管理',
    icon: 'database',
    path: 'conventionalEquip',
    authority: 'conventionalEquip',
    children: [
      {
        name: '消防车管理',
        icon: 'shrink',
        path: 'fireEngine',
        authority: 'fireEngine',
      },
      {
        name: '车辆类型管理',
        icon: 'shrink',
        path: 'carType',
        authority: 'carType',
      },
      {
        name: '常规器材管理',
        icon: 'shrink',
        path: 'conventionalEquip',
        authority: 'conventionalEquip',
      },
      // {
      //   name: '专勤器材管理',
      //   icon: 'shrink',
      //   path: 'specialDutyEquip',
      //   authority: 'specialDutyEquip',
      // },
      {
        name: '特种器材管理',
        icon: 'shrink',
        path: 'specialDutyEquip',
        authority: 'specialDutyEquip',
      },
      {
        name: '空气呼吸器管理',
        icon: 'shrink',
        path: 'specialDutyEquipTwo',
        authority: 'specialDutyEquipTwo',
      },
      {
        name: '装备类型管理',
        icon: 'shrink',
        path: 'emergencyEquipType',
        authority: 'emergencyEquipType',
      },
    ],
  },
  {
    name: '消防水源',
    icon: 'disconnect',
    path: 'water',
    authority: 'water',
    children: [
      {
        name: '消防水源',
        icon: 'shrink',
        path: 'waterSource',
        authority: 'waterSource',
      },
      {
        name: '消防水源类型',
        icon: 'shrink',
        path: 'waterSourceType',
        authority: 'waterSourceType',
      },
    ],
  },
  {
    name: '重点单位',
    icon: 'tags-o',
    path: 'unit',
    authority: 'unit',
    children: [
      {
        name: '重点单位',
        icon: 'shrink',
        path: 'keyUnit',
        authority: 'keyUnit',
      },
      {
        name: '消防设施',
        icon: 'shrink',
        path: 'facilities',
        authority: 'facilities',
      },
      {
        name: '生产装置',
        icon: 'shrink',
        path: 'equipment',
        authority: 'equipment',
      },
       {
         name: '重点部位',
         icon: 'shrink',
         path: 'keyParts',
         authority: 'keyParts',
       },
       {
        name: '社会资源',
        icon: 'shrink',
        path: 'socialResource',
        authority: 'socialResource',
      },
      {
        name: '设备设施类型',
        icon: 'shrink',
        path: 'equipmentType',
        authority: 'equipmentType',
      },
    ],
  },

  {
    name: '灭火剂管理',
    icon: 'dashboard',
    path: 'extinguisher',
    authority: 'extinguisher',
    children: [
      // {
      //   name: '灭火剂管理',
      //   icon: 'shrink',
      //   path: 'extinguisherList',
      //   authority: 'extinguisherList',
      // },
      {
        name: '灭火剂类型',
        icon: 'shrink',
        path: 'extinguisherType',
        authority: 'extinguisherType',
      },
      // {
      //   name: '储备动态',
      //   icon: 'shrink',
      //   path: 'extinguisherSum',
      //   authority: 'extinguisherSum',
      // },
      // {
      //   name: '消耗统计',
      //   icon: 'shrink',
      //   path: 'extinguisherUseRecord',
      //   authority: 'extinguisherUseRecord',
      // },
    ],
  },
  {
    name: '标绘管理',
    icon: 'picture',
    path: 'plotting',
    authority: 'plotting',
    children: [
      {
        name: '会标管理',
        icon: 'shrink',
        path: 'plottingList',
        authority: 'plottingList',
      },
    ],
  },
  {
    name: '统计分析',
    icon: 'bar-chart',
    path: 'dataCount',
    authority: 'dataCount',
    children: [
      // {
      //   name: '消防设施统计',
      //   icon: 'shrink',
      //   path: 'keyUnitCount',
      //   authority: 'log',
      // },
      {
        name: '重点单位',
        icon: 'shrink',
        path: 'teamCount',
        authority: 'teamCount',
      },
      {
        name: '事故统计',
        icon: 'shrink',
        path: 'accidentCount',
        authority: 'accidentCount',
      },
      {
        name: '出警次数统计',
        icon: 'shrink',
        path: 'policeNumber',
        authority: 'policeNumber',
      },
      // {
      //   name: '地图',
      //   path: 'map',
      //   authority: 'log',
      // },
    ],
  },
  {
    name: '典型火灾',
    icon: 'copy',
    path: 'accidentCase',
    authority: 'accidentCase',
    children: [
      {
        name: '典型火灾案例',
        icon: 'shrink',
        path: 'accidentCaseList',
        authority: 'accidentCaseList',
      },
      {
        name: '火灾案例类型',
        icon: 'shrink',
        path: 'fireCaseType',
        authority: 'fireCaseType',
      },
    ],
  },
  {
    name: '救援记录',
    icon: 'copy',
    path: 'accident',
    authority: 'accident',
    children: [
      {
        name: '救援记录',
        icon: 'shrink',
        path: 'accidentList',
        authority: 'accidentList',
      },
    ],
  },

  {
    name: '地理信息编辑',
    icon: 'environment-o',
    path: 'geographic',
    authority: 'geographic',
    children: [
      {
        name: '地理信息编辑',
        icon: 'shrink',
        path: 'geographicEdit',
        authority: 'geographicEdit',
      },
      {
        name: 'GIS定位',
        icon: 'shrink',
        path: 'gisLocation',
        authority: 'gisLocation',
      },
    ],
  },
  {
    name: '场景搭建',
    icon: 'line-chart',
    path: 'scene',
    authority: 'scene',
    children: [
      {
        name: '场景搭建',
        icon: 'shrink',
        path: 'sceneList',
        authority: 'sceneList',
      },
    ],
  },
  {
    name: '公式管理',
    icon: 'edit',
    path: 'expr',
    authority: 'expr',
    children: [
      {
        name: '公式编辑',
        icon: 'shrink',
        path: 'edit',
        authority: 'exprList',
      },
    ],
  },
  {
    name: '系统管理',
    icon: 'setting',
    path: 'sys',
    authority: 'sys',
    children: [
      {
        name: '用户管理',
        icon: 'shrink',
        path: 'sysUser',
        authority: 'user',
      },
      {
        name: '角色管理',
        icon: 'shrink',
        path: 'role',
        authority: 'role',
      },
      {
        name: '组织机构',
        icon: 'shrink',
        path: 'department',
        authority: 'dept',
      },
      {
        name: '日志管理',
        icon: 'shrink',
        path: 'log',
        authority: 'log',
      },
    ],
  },
];

function formatter(data, parentPath = '/', parentAuthority) {
  return data.map(item => {
    let { path } = item;
    if (!isUrl(path)) {
      path = parentPath + item.path;
    }
    const result = {
      ...item,
      path,
      authority: item.authority || parentAuthority,
    };
    if (item.children) {
      result.children = formatter(item.children, `${parentPath}${item.path}/`, item.authority);
    }
    return result;
  });
}

export const getMenuData = () => formatter(menuData);
