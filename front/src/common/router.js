import { createElement } from 'react';
import dynamic from 'dva/dynamic';
import pathToRegexp from 'path-to-regexp';
import { getMenuData } from './menu';

let routerDataCache;

const modelNotExisted = (app, model) =>
  // eslint-disable-next-line
  !app._models.some(({ namespace }) => {
    return namespace === model.substring(model.lastIndexOf('/') + 1);
  });

// wrapper of dynamic
const dynamicWrapper = (app, models, component) => {
  // () => require('module')
  // transformed by babel-plugin-dynamic-import-node-sync
  if (component.toString().indexOf('.then(') < 0) {
    models.forEach(model => {
      if (modelNotExisted(app, model)) {
        // eslint-disable-next-line
        app.model(require(`../models/${model}`).default);
      }
    });
    return props => {
      if (!routerDataCache) {
        routerDataCache = getRouterData(app);
      }
      return createElement(component().default, {
        ...props,
        routerData: routerDataCache,
      });
    };
  }
  // () => import('module')
  return dynamic({
    app,
    models: () =>
      models.filter(model => modelNotExisted(app, model)).map(m => import(`../models/${m}.js`)),
    // add routerData prop
    component: () => {
      if (!routerDataCache) {
        routerDataCache = getRouterData(app);
      }
      return component().then(raw => {
        const Component = raw.default || raw;
        return props =>
          createElement(Component, {
            ...props,
            routerData: routerDataCache,
          });
      });
    },
  });
};

function getFlatMenuData(menus) {
  let keys = {};
  menus.forEach(item => {
    if (item.children) {
      keys[item.path] = { ...item };
      keys = { ...keys, ...getFlatMenuData(item.children) };
    } else {
      keys[item.path] = { ...item };
    }
  });
  return keys;
}

export const getRouterData = app => {
  const routerConfig = {
    '/': {
      component: dynamicWrapper(app, ['user', 'login'], () => import('../layouts/BasicLayout')),
    },
    '/unit/keyUnit': {
      component: dynamicWrapper(app, ['keyUnit'], () => import('../routes/Unit/KeyUnit')),
    },
    '/sys/sysUser': {
      component: dynamicWrapper(app, ['sys/sysUser'], () => import('../routes/Sys/User/SysUser')),
      //authority: 'user',
    },
    '/sys/role': {
      component: dynamicWrapper(app, ['sys/role'], () => import('../routes/Sys/Role/Role')),
    },
    '/sys/log': {
      component: dynamicWrapper(app, ['sys/log'], () => import('../routes/Sys/Log')),
    },
    '/sys/department': {
      component: dynamicWrapper(app, ['sys/department'], () =>
        import('../routes/Sys/Department/Department')
      ),
    },
    '/water/waterSource': {
      component: dynamicWrapper(app, ['fireWaterSource/waterSource'], () =>
        import('../routes/FireWaterSource/WaterSource/WaterSource')
      ),
    },
    '/accidentCase/accidentCaseList': {
      component: dynamicWrapper(app, ['accident/accidentCase'], () =>
        import('../routes/Accident/AccidentCase/AccidentCase')
      ),
    },
    '/accidentCase/fireCaseType': {
      component: dynamicWrapper(app, ['accident/fireCaseType'], () =>
        import('../routes/Accident/FireCaseType/FireCaseType')
      ),
    },
    '/accident/accidentList': {
      component: dynamicWrapper(app, ['accident/accident'], () =>
        import('../routes/Accident/AccidentRecord/Accident')
      ),
    },
    '/water/waterSourceType': {
      component: dynamicWrapper(app, ['fireWaterSource/fireWaterType'], () =>
        import('../routes/FireWaterSource/FireWaterType/FireWaterType')
      ),
    },
    '/unit/equipmentType': {
      component: dynamicWrapper(app, ['equipment/equipmentType'], () =>
        import('../routes/Equipment/EquipmentType/EquipmentType')
      ),
    },
    '/unit/equipment': {
      component: dynamicWrapper(app, ['equipment/equipment'], () =>
        import('../routes/Equipment/Equipment/Equipment')
      ),
    },
    '/unit/facilities': {
      component: dynamicWrapper(app, ['equipment/facilities'], () =>
        import('../routes/Equipment/Facilities/Facilities')
      ),
    },
    '/unit/keyParts': {
      component: dynamicWrapper(app, ['equipment/keyParts'], () =>
        import('../routes/Equipment/KeyParts/KeyParts')
      ),
    },
    '/unit/socialResource': {
      component: dynamicWrapper(app, ['equipment/socialResource'], () =>
        import('../routes/Equipment/SocialResource/SocialResource')
      ),
    },

    '/expert/expertType': {
      component: dynamicWrapper(app, ['expert/expertType'], () =>
        import('../routes/Expert/ExpertType/ExpertType')
      ),
    },
    '/expert/expertList': {
      component: dynamicWrapper(app, ['expert/expert'], () =>
        import('../routes/Expert/Expert/Expert')
      ),
    },
    '/dangers/dangersType': {
      component: dynamicWrapper(app, ['dangers/dangersType'], () =>
        import('../routes/Dangers/DangersType/DangersType')
      ),
    },
    '/dangers/dangersList': {
      component: dynamicWrapper(app, ['dangers/dangers'], () =>
        import('../routes/Dangers/Dangers/Dangers')
      ),
    },
    '/plans/plansList': {
      component: dynamicWrapper(app, ['plans/plansList'], () =>
        import('../routes/Plans/PlansList/PlansList')
      ),
    },
    '/plans/plansType': {
      component: dynamicWrapper(app, ['plans/plansType'], () =>
        import('../routes/Plans/PlansType/PlansType')
      ),
    },
    '/plotting/plottingList': {
      component: dynamicWrapper(app, ['plotting/plotting'], () =>
        import('../routes/Plotting/Plotting/Plotting')
      ),
    },
    '/emergency/teams': {
      component: dynamicWrapper(app, ['emergency/teams'], () =>
        import('../routes/Emergency/Teams/Teams')
      ),
    },
    '/emergency/fireBrigade': {
      component: dynamicWrapper(app, ['emergency/fireBrigade'], () =>
        import('../routes/Emergency/FireBrigade/FireBrigade')
      ),
    },
    '/emergency/emergencyMaterial': {
      component: dynamicWrapper(app, ['emergency/emergencyMaterial'], () =>
        import('../routes/Emergency/EmergencyMaterial/EmergencyMaterial')
      ),
    },
    '/conventionalEquip/emergencyEquipType': {
      component: dynamicWrapper(app, ['emergency/emergencyEquipType'], () =>
        import('../routes/Emergency/EmergencyEquipType/EmergencyEquipType')
      ),
    },
    '/emergency/suppliesType': {
      component: dynamicWrapper(app, ['emergency/suppliesType'], () =>
        import('../routes/Emergency/SuppliesType/SuppliesType')
      ),
    },
    '/emergency/reservePoint': {
      component: dynamicWrapper(app, ['emergency/reservePoint'], () =>
        import('../routes/Emergency/ReservePoint/ReservePoint')
      ),
    },
    '/extinguisher/extinguisherList': {
      component: dynamicWrapper(app, ['emergency/extinguisher'], () =>
        import('../routes/Emergency/Extinguisher/Extinguisher')
      ),
    },
    '/extinguisher/extinguisherType': {
      component: dynamicWrapper(app, ['emergency/extinguisherType'], () =>
        import('../routes/Emergency/ExtinguisherType/ExtinguisherType')
      ),
    },
    '/exception/403': {
      component: dynamicWrapper(app, [], () => import('../routes/Exception/403')),
    },
    '/exception/404': {
      component: dynamicWrapper(app, [], () => import('../routes/Exception/404')),
    },
    '/exception/500': {
      component: dynamicWrapper(app, [], () => import('../routes/Exception/500')),
    },
    '/exception/trigger': {
      component: dynamicWrapper(app, ['error'], () =>
        import('../routes/Exception/triggerException')
      ),
    },
    '/user': {
      component: dynamicWrapper(app, [], () => import('../layouts/UserLayout')),
    },
    '/user/login': {
      component: dynamicWrapper(app, ['login'], () => import('../routes/User/Login')),
    },
    '/user/register': {
      component: dynamicWrapper(app, ['register'], () => import('../routes/User/Register')),
    },
    '/user/register-result': {
      component: dynamicWrapper(app, [], () => import('../routes/User/RegisterResult')),
    },
    '/conventionalEquip/carType': {
      component: dynamicWrapper(app, ['car/carType'], () =>
        import('../routes/Car/CarType/CarType')
      ),
    },
    '/conventionalEquip/fireEngine': {
      component: dynamicWrapper(app, ['car/carMassage', 'car/fireEngine'], () =>
        import('../routes/Car/FireEngine/FireEngine')
      ),
    },
    '/conventionalEquip/carMassage': {
      component: dynamicWrapper(app, ['car/carMassage', 'car/fireEngine'], () =>
        import('../routes/Car/CarMassage/CarMassage')
      ),
    },
    '/extinguisher/extinguisherSum': {
      component: dynamicWrapper(app, ['extinguisher/extinguisherSum'], () =>
        import('../routes/Extinguisher/Extinguisher/ExtinguisherSum')
      ),
    },
    '/extinguisher/extinguisherUseRecord': {
      component: dynamicWrapper(app, ['extinguisher/extinguisherUseRecord'], () =>
        import('../routes/Extinguisher/ExtinguisherUseRecord/ExtinguisherUseRecord')
      ),
    },

    '/conventionalEquip/conventionalEquip': {
      component: dynamicWrapper(app, ['conventionalEquip/conventionalEquip'], () =>
        import('../routes/ConventionalEquip/ConventionalEquip/ConventionalEquip')
      ),
    },
    '/conventionalEquip/specialDutyEquip': {
      component: dynamicWrapper(app, ['conventionalEquip/specialDutyEquip'], () =>
        import('../routes/ConventionalEquip/SpecialDutyEquip/SpecialDutyEquip')
      ),
    },
    '/conventionalEquip/specialDutyEquipTwo': {
      component: dynamicWrapper(app, ['conventionalEquip/specialDutyEquipTwo'], () =>
        import('../routes/ConventionalEquip/SpecialDutyEquipTwo/SpecialDutyEquipTwo')
      ),
    },
    '/geographic/geographicEdit': {
      component: dynamicWrapper(app, ['geographicEdit/geographicEdit'], () =>
        import('../routes/GeographicEdit/GeographicEdit')
      ),
    },
    '/geographic/gisLocation': {
      component: dynamicWrapper(app, ['geographicEdit/gisLocation'], () =>
        import('../routes/GeographicEdit/GisLocation')
      ),
    },
    '/scene/sceneList': {
      component: dynamicWrapper(app, ['scene/scene'], () =>
        import('../routes/SceneList/SceneList')
      ),
    },

    '/dataCount/keyUnitCount': {
      component: dynamicWrapper(app, ['datacount/keyunitcount'], () =>
        import('../routes/DataCount/KeyUnitCount')
      ),
    },
    '/dataCount/teamCount': {
      component: dynamicWrapper(app, ['datacount/teamCount'], () =>
        import('../routes/DataCount/TeamCount')
      ),
    },
    '/dataCount/accidentCount': {
      component: dynamicWrapper(app, ['datacount/accidentCount'], () =>
        import('../routes/DataCount/AccidentCount')
      ),
    },
    '/dataCount/policeNumber': {
      component: dynamicWrapper(app, ['datacount/policeNumber'], () =>
        import('../routes/DataCount/PoliceNumber')
      ),
    },
    '/expr/edit': {
      component: dynamicWrapper(app, ['expr/expr'], () => import('../routes/Expr/Expr')),
    },
    // '/dataCount/map': {
    //   component: dynamicWrapper(app, ['datacount/map'], () =>
    //     import('../routes/DataCount/MapCount')
    //   ),
    // },
    // '/user/:id': {
    //   component: dynamicWrapper(app, [], () => import('../routes/User/SomeComponent')),
    // },
  };
  // Get name from ./menu.js or just set it in the router data.
  const menuData = getFlatMenuData(getMenuData());

  // Route configuration data
  // eg. {name,authority ...routerConfig }
  const routerData = {};
  // The route matches the menu
  Object.keys(routerConfig).forEach(path => {
    // Regular match item name
    // eg.  router /user/:id === /user/chen
    const pathRegexp = pathToRegexp(path);
    const menuKey = Object.keys(menuData).find(key => pathRegexp.test(`${key}`));
    let menuItem = {};
    // If menuKey is not empty
    if (menuKey) {
      menuItem = menuData[menuKey];
    }
    let router = routerConfig[path];
    // If you need to configure complex parameter routing,
    // https://github.com/ant-design/ant-design-pro-site/blob/master/docs/router-and-nav.md#%E5%B8%A6%E5%8F%82%E6%95%B0%E7%9A%84%E8%B7%AF%E7%94%B1%E8%8F%9C%E5%8D%95
    // eg . /list/:type/user/info/:id
    router = {
      ...router,
      name: router.name || menuItem.name,
      authority: router.authority || menuItem.authority,
      hideInBreadcrumb: router.hideInBreadcrumb || menuItem.hideInBreadcrumb,
    };
    routerData[path] = router;
  });
  return routerData;
};
