import React from 'react';
import { Form, Input, Modal, Button, Radio, Row, DatePicker, Col, Select } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
const RadioGroup = Radio.Group;
const Option = Select.Option;
const FormItem = Form.Item;
const { TextArea } = Input;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

let AddEditModal = ({ expert, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: '查看信息',
    visible: expert.findModalVisible,
    maskClosable: false,
    width: 600,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
    ],
  };

  if (!expert.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'expert/updateState',
      payload: {
        findModalVisible: false,
      },
    });
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = getFieldsValue();
      data.id = expert.modalType === 'create' ? '' : item.id;
      dispatch({
        type: `expert/${expert.modalType}`,
        payload: data,
        search: expert.selectObj,
      });
    });
  }

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={24}>
            <FormItem label="专家名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '请填写专家名称' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写专家名称" readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="性别:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('sex', {
                initialValue: item.sex == null ? 1 : item.sex,
              })(
                <RadioGroup style={{ width: '200px' }}>
                  <Radio value={1}>男</Radio>
                  <Radio value={2}>女</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="出生日期:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('birth', {
                initialValue: item.birth ? moment(item.birth) : undefined,
                rules: [{ required: true, message: '请选择出生日期' }],
              })(
                <DatePicker
                  style={{ width: '100%' }}
                  placeholder="请选择出生日期"
                  format={'YYYY-MM-DD'}
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="专家类型:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('expertTypeName', {
                initialValue: item.expertTypeName ? item.expertTypeName + '' : undefined,
                rules: [{ required: true, message: '请选择专家类型' }],
              })(<Input type="text" placeholder="" readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="所属单位:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('unit', {
                initialValue: item.unit,
                rules: [
                  { required: true, message: '请填写所属单位' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写所属单位" readOnly />)}
            </FormItem>
          </Col>
        </Row>

        <Row>
          <Col span={24}>
            <FormItem label="联系方式:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('phone', {
                initialValue: item.phone,
                rules: [
                  { required: true, message: '请填写联系方式' },
                  { max: 20, message: '最长不超过20个字' },
                ],
              })(<Input type="text" placeholder="请填写联系方式" readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="专业:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('major', {
                initialValue: item.major,
                rules: [
                  { required: true, message: '请填写专业' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写专业" readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="特长:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('speciality', {
                initialValue: item.speciality,
                rules: [
                  { required: true, message: '请填写特长' },
                  { max: 100, message: '最长不超过100个字' },
                ],
              })(<TextArea placeholder="请填写专家特长" autosize={{ minRows: 3 }} readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="家庭住址:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('address', {
                initialValue: item.address,
                rules: [{ max: 100, message: '最长不超过100个字' }],
              })(<TextArea placeholder="请填写家庭住址" autosize={{ minRows: 3 }} readOnly />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { expert: state.expert };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
