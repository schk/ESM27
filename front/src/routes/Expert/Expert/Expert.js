import React, { Fragment } from 'react';
import { message } from 'antd';
import {
  Table,
  Form,
  Upload,
  Button,
  Popconfirm,
  Card,
  Input,
  Divider,
  Row,
  Col,
  Select,
  Modal,
} from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import ShowFileModal from './ShowFileModal';
import PageHeader from '../../../layouts/PageHeaderLayout';
import { baseUrl } from '../../../config/system';
const confirm = Modal.confirm;
const Option = Select.Option;
const FormItem = Form.Item;

function Expert({ expert, form, dispatch, loading }) {
  const {
    getFieldDecorator,
    getFieldsValue,
    setFieldsValue,
    resetFields,
    getFieldProps,
    getFieldValue,
  } = form;
  const AddEditModalProps = {
    item: expert.item,
  };

  const FindModalProps = {
    item: expert.item,
  };

  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    {
      title: '专家名称',
      dataIndex: 'name',
      key: 'name',
      width: 100,
      render: (text, record, value) => (
        <Fragment>
          <a onClick={() => onFindInfo(record.id_)}>{record.name}</a>
        </Fragment>
      ),
    },
    { title: '专家类型', dataIndex: 'expertTypeName', key: 'expertTypeName', width: 100 },
    { title: '职务', dataIndex: 'position', key: 'position', width: 100 },
    { title: '专业', dataIndex: 'major', key: 'major', width: 100 },
    { title: '特长', dataIndex: 'speciality', key: 'speciality', width: 100 },
    { title: '联系方式', dataIndex: 'phone', key: 'phone', width: 100 },
    { title: '所属单位', dataIndex: 'unit', key: 'unit', width: 100 },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 100,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_)}>修改</a>
          <Divider type="vertical" />
          {/* <a onClick={() => onFindInfo(record.id_)}>查看</a>
          <Divider type="vertical" /> */}
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];
  const onFindInfo = id => {
    dispatch({
      type: 'expert/findInfo',
      payload: id,
    });
  };
  function onUpdate(id) {
    dispatch({
      type: 'expert/info',
      payload: id,
    });
  }

  function onAdd() {
    resetFields();
    dispatch({
      type: 'expert/updateState',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'expert/remove',
      payload: id,
      search: { pageNum: expert.current, pageSize: expert.pageSize, ...getFieldsValue() },
    });
  }

  function handleSearch() {
    dispatch({
      type: 'expert/qryListByParams',
      payload: { pageNum: 1, pageSize: expert.pageSize, ...getFieldsValue() },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'expert/qryListByParams',
      payload: { pageNum: 1, pageSize: expert.pageSize },
    });
  }

  function downloadTemplate() {
    window.open(baseUrl + '/execl/downLoadTemplate.jhtml?type=expertTemplate');
  }

  const pagination = {
    current: expert.current,
    pageSize: expert.pageSize,
    total: expert.total,
    showSizeChanger: true,
    showTotal: total => '共' + expert.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'expert/qryListByParams',
        payload: { pageNum: current, pageSize: size, ...getFieldsValue() },
      });
    },
    onChange(current) {
      dispatch({
        type: 'expert/qryListByParams',
        payload: { pageNum: current, pageSize: expert.pageSize, ...getFieldsValue() },
      });
    },
  };

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  function initLoopMsg(values) {
    return values != null && values.length > 0
      ? values.map((k, index) => {
          return <div key={index}>{k}</div>;
        })
      : null;
  }
  const props = {
    name: 'file',
    action: baseUrl + '/execl/importExpert.jhtml',
    beforeUpload(file) {
      // const isJPG = file.type === 'application/vnd.ms-excel' || file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
      // if (!isJPG) {
      //   message.error('只能上传EXCEL文件！');
      // }
      // return isJPG;
    },
    onChange(info) {
      let fileList = info.fileList;
      if (info.file.status === 'done') {
        if (info.file.response.httpCode == 200) {
          if (info.file.response.errorCode == 1) {
            //弹出确认取消框
            confirm({
              title: '信息提示框',
              content: initLoopMsg(info.file.response.data),
              okText: '继续导入',
              cancelText: '取消导入',
              onOk() {
                dispatch({
                  type: 'expert/importExpertConfirm',
                  payload: info.file.response.times,
                });
              },
              onCancel() {
                dispatch({
                  type: 'expert/delExpertTemp',
                  payload: info.file.response.times,
                });
              },
            });
          } else if (info.file.response.errorCode == 2) {
            if (info.file.response.data && info.file.response.data.length > 0) {
              Modal.error({
                title: '导入提示',
                content: initLoopMsg(info.file.response.data),
              });
            }
          } else {
            message.success(`${info.file.name} 上传成功`);
            dispatch({
              type: 'expert/qryListByParams',
              payload: { pageNum: 1, pageSize: expert.pageSize, ...getFieldsValue() },
            });
          }
        } else {
          Modal.error({
            title: '导入提示',
            content: info.file.response.msg,
          });
        }
        fileList = fileList.filter(file => {
          return file.response && file.response.fid;
        });
        //只保留最后一条记录
        fileList = fileList.slice(-1);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`);
      }
      let fildsValue = {};
      fildsValue['file'] = fileList.map(file => {
        return file;
      });
      setFieldsValue(fildsValue);
    },
  };

  return (
    <PageHeader>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={6} sm={24}>
                  <FormItem label="专家名称">
                    {getFieldDecorator('name')(<Input placeholder="请输入专家名称" />)}
                  </FormItem>
                </Col>
                <Col md={4} sm={24}>
                  <FormItem label="专家类型">
                    {getFieldDecorator('typeId')(
                      <Select allowClear placeholder="请选择专家类型">
                        <Option value="">全部</Option>
                        {loopOption(expert.typeList)}
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={8} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                      重置
                    </Button>

                    <Button
                      style={{ marginLeft: 80 }}
                      type="primary"
                      icon="download"
                      onClick={downloadTemplate}
                    >
                      下载模板
                    </Button>
                    {/* <a href={"template/应急专家.xls"} > <Button style={{ marginLeft: 80 }} type="primary" icon="download"  >下载模板</Button></a> */}
                    <Upload
                      {...getFieldProps(
                        'file',
                        {
                          validate: [
                            {
                              rules: [{ type: 'array', required: true, message: '请添加数据文件' }],
                              trigger: 'onBlur',
                            },
                          ],
                        },
                        { valuePropName: 'fileIds' }
                      )}
                      {...props}
                      fileList={getFieldValue('file')}
                    >
                      <Button style={{ marginLeft: 8 }} type="primary" icon="upload">
                        导入
                      </Button>
                    </Upload>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={onAdd}>
              新建
            </Button>
          </div>
          <Table
            columns={columns}
            dataSource={expert.list}
            rowKey={record => record.id}
            loading={loading}
            pagination={pagination}
          />
        </div>
      </Card>
      <AddEditModal {...AddEditModalProps} />
      <ShowFileModal {...FindModalProps} />
    </PageHeader>
  );
}

function mapStateToProps(state) {
  return {
    expert: state.expert,
    loading: state.loading.models.expert,
  };
}

Expert = Form.create()(Expert);

export default connect(mapStateToProps)(Expert);
