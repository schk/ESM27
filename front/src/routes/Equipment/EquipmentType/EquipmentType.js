import React, { Fragment } from 'react';
import { Table, Form, Button, Popconfirm, Card, Input, Divider, Row, Col, Select } from 'antd';
import { connect } from 'dva';
import { baseFileUrl } from '../../../config/system';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import PageHeader from '../../../layouts/PageHeaderLayout';
const Option = Select.Option;
const FormItem = Form.Item;

function EquipmentType({ location, equipmentType, form, dispatch, loading }) {
  const { getFieldDecorator, validateFields, getFieldsValue, setFieldsValue, resetFields } = form;
  const AddEditModalProps = {
    item: equipmentType.item,
  };

  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    { title: '名称', dataIndex: 'name', key: 'name', width: 100 },
    { title: '编码', dataIndex: 'code', key: 'code', width: 100 },
    { title: '所属类别', dataIndex: 'typeName', key: 'typeName', width: 100 },
    {
      title: '地图图标',
      dataIndex: 'dtPath',
      key: 'dtPath',
      width: 100,
      render: (value, row, index) => {
        if (row.dtPath) {
          return <img src={baseFileUrl + row.dtPath} style={{ width: '20px' }} />;
        }
      },
    },
    {
      title: '聚合图标',
      dataIndex: 'jhPath',
      key: 'jhPath',
      width: 100,
      render: (value, row, index) => {
        if (row.jhPath) {
          return <img src={baseFileUrl + row.jhPath} style={{ width: '40px' }} />;
        }
      },
    },
    { title: '描述', dataIndex: 'remark', key: 'remark', width: 100 },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 100,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  function onUpdate(id) {
    dispatch({
      type: 'equipmentType/info',
      payload: id,
    });
  }

  function onAdd() {
    dispatch({
      type: 'equipmentType/updateState',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'equipmentType/del',
      payload: id,
      search: {
        pageNum: equipmentType.current,
        pageSize: equipmentType.pageSize,
        ...getFieldsValue(),
      },
    });
  }

  function handleSearch() {
    dispatch({
      type: 'equipmentType/qryListByParams',
      payload: { pageNum: 1, pageSize: equipmentType.pageSize, ...getFieldsValue() },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'equipmentType/qryListByParams',
      payload: { pageNum: 1, pageSize: equipmentType.pageSize },
    });
  }

  const pagination = {
    current: equipmentType.current,
    pageSize: equipmentType.pageSize,
    total: equipmentType.total,
    showSizeChanger: true,
    showTotal: total => '共' + equipmentType.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'equipmentType/qryListByParams',
        payload: { pageNum: current, pageSize: size, ...getFieldsValue() },
      });
    },
    onChange(current) {
      dispatch({
        type: 'equipmentType/qryListByParams',
        payload: { pageNum: current, pageSize: equipmentType.pageSize, ...getFieldsValue() },
      });
    },
  };

  return (
    <PageHeader>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={6} sm={24}>
                  <FormItem label="名称">
                    {getFieldDecorator('name')(<Input placeholder="请输入名称" />)}
                  </FormItem>
                </Col>
                <Col md={6} sm={24}>
                  <FormItem label="所属类别">
                    {getFieldDecorator('type')(
                      <Select
                        showSearch
                        style={{ width: '100%' }}
                        placeholder="选择类别"
                        optionFilterProp="children"
                      >
                        <Option value="">全部</Option>
                        <Option value="1">消防设置</Option>
                        <Option value="2">生产装置</Option>
                        <Option value="3">医院</Option>
                        <Option value="4">学校</Option>
                        <Option value="5">其他</Option>
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={8} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                      重置
                    </Button>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={onAdd}>
              新建
            </Button>
          </div>
          <Table
            columns={columns}
            dataSource={equipmentType.list}
            rowKey={record => record.id}
            loading={loading}
            pagination={pagination}
          />
        </div>
      </Card>
      <AddEditModal {...AddEditModalProps} />
    </PageHeader>
  );
}

function mapStateToProps(state) {
  return {
    equipmentType: state.equipmentType,
    loading: state.loading.models.equipmentType,
  };
}

EquipmentType = Form.create()(EquipmentType);

export default connect(mapStateToProps)(EquipmentType);
