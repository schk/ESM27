import React from 'react';
import { Form, Input, Modal, Button, Radio, Row, Col, Select } from 'antd';
import { baseFileUrl } from '../../../config/system';
import { connect } from 'dva';
const RadioGroup = Radio.Group;
const Option = Select.Option;
const FormItem = Form.Item;
const { TextArea } = Input;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

let AddEditModal = ({ equipmentType, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: equipmentType.modalType == 'create' ? '新建生产装置类型' : '修改生产装置类型',
    visible: equipmentType.modalVisible,
    maskClosable: false,
    width: 800,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={equipmentType.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!equipmentType.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'equipmentType/updateState',
      payload: {
        modalVisible: false,
        locationItem: { lonLat: '' },
      },
    });
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = getFieldsValue();
      data.id = equipmentType.modalType === 'create' ? '' : item.id;
      dispatch({
        type: `equipmentType/${equipmentType.modalType}`,
        payload: data,
        search: { pageNum: equipmentType.current, pageSize: equipmentType.pageSize },
      });
    });
  }

  const loopOption = data =>
    data.map(item => {
      return (
        <Option key={item.dtPath}>
          <img style={{ width: '20px', height: '20px' }} src={baseFileUrl + item.dtPath} alt="" />
          {item.name}
        </Option>
      );
    });


    const loopOption2 = data =>
    data.map(item => {
      return (
        <Option key={item.jhPath}>
          <img style={{ width: '40px', height: '20px' }} src={baseFileUrl + item.jhPath} alt="" />
          {item.name}
        </Option>
      );
    });

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={24}>
            <FormItem label="名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '名称未填写' },
                  { max: 20, message: '最长不超过20个字' },
                ],
              })(<Input type="text" placeholder="请填写名称" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="编码:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('code', {
                initialValue: item.code,
                rules: [
                  { required: true, message: '编码未填写' },
                  { max: 20, message: '最长不超过20个字' },
                ],
              })(<Input type="text" placeholder="请填写编码" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="所属类别:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('type', {
                initialValue: item.type == null ? 1 : item.type,
                rules: [{ required: true, message: '角色名称未填写' }],
              })(
                <RadioGroup style={{ width: '450px' }}>
                  <Radio value={1}>消防设施</Radio>
                  <Radio value={2}>生产装置</Radio>
                  <Radio value={3}>医院</Radio>
                  <Radio value={4}>学校</Radio>
                  <Radio value={5}>其他</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="选择图标:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('dtPath', {
                initialValue: item.dtPath ? item.dtPath : undefined,
                //rules: [{ required: true, message: '请选择图标' }],
              })(
                <Select placeholder="请选择地图显示图标">
                  {loopOption(equipmentType.plottingList)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="选择图标:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('jhPath', {
                initialValue: item.jhPath ? item.jhPath : undefined,
                //rules: [{ required: true, message: '请选择图标' }],
              })(
                <Select placeholder="请选择聚合显示图标">
                  {loopOption2(equipmentType.jhPlottingList)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="描述：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('remark', {
                initialValue: item.remark,
                rules: [{ max: 100, message: '最长不超过100个字' }],
              })(<TextArea autosize={{ minRows: 4 }} />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { equipmentType: state.equipmentType };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
