import React from 'react';
import {
  Form,
  Input,
  Modal,
  Popover,
  Button,
  Row,
  Col,
  InputNumber,
  Icon,
  Select,
  Upload,
  message,
  Popconfirm,
} from 'antd';
import { connect } from 'dva';
const FormItem = Form.Item;
const Option = Select.Option;
import { baseUrl, baseFileUrl } from '../../../config/system';

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 17,
  },
};
const formItemLayout1 = {
  labelCol: {
    span: 10,
  },
  wrapperCol: {
    span: 14,
  },
};

let AddEditModal = ({ equipment, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
    getFieldProps,
  } = form;

  const modalOpts = {
    title: equipment.modalType == 'create' ? '新建生产装置' : '修改生产装置',
    visible: equipment.modalVisible,
    maskClosable: false,
    width: 1000,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={equipment.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!equipment.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'equipment/updateState',
      payload: {
        modalVisible: false,
        locationItem: { lonLat: '' },
        curFileId: '',
        fileCatalogs: [],
        catalogNames: {},
        fileList: [],
      },
    });
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      var lonLat = getFieldsValue(["lonLat"]);
          if(lonLat.lonLat==undefined){
          message.error("坐标不能为空");
          return;
      }
      const data = getFieldsValue();
      var equipmentDocs = [];
      if (equipment.fileList != null && equipment.fileList.length > 0) {
        for (var i = 0; i < equipment.fileList.length; i++) {
          var fileId = equipment.fileList[i].uid;
          var fileName = equipment.fileList[i].name;
          if (equipment.fileCatalogs != null && equipment.fileCatalogs.length > 0) {
            for (var j = 0; j < equipment.fileCatalogs.length; j++) {
              if (
                equipment.fileCatalogs[j].fileId == fileId &&
                equipment.fileCatalogs[j].catalogs != null &&
                equipment.fileCatalogs[j].catalogs.length > 0
              ) {
                for (var k = 0; k < equipment.fileCatalogs[j].catalogs.length; k++) {
                  var name = null;
                  if (fileId == equipment.curFileId) {
                    name = data['name_' + fileId + '_' + equipment.fileCatalogs[j].catalogs[k]];
                  } else {
                    name =
                      equipment.catalogNames[
                        'name_' + fileId + '_' + equipment.fileCatalogs[j].catalogs[k]
                      ];
                  }

                  for (var h = k + 1; h < equipment.fileCatalogs[j].catalogs.length; h++) {
                    if (fileId == equipment.curFileId) {
                      if (
                        name == data['name_' + fileId + '_' + equipment.fileCatalogs[j].catalogs[h]]
                      ) {
                        message.error('文件' + fileName + '目录名称不能重复');
                        return;
                      }
                    } else {
                      if (
                        name ==
                        equipment.catalogNames[
                          'name_' + fileId + '_' + equipment.fileCatalogs[j].catalogs[h]
                        ]
                      ) {
                        message.error('文件' + fileName + '目录名称不能重复');
                        return;
                      }
                    }
                  }
                }
              }
            }
          }
        }
        for (var i = 0; i < equipment.fileList.length; i++) {
          var fileId = equipment.fileList[i].uid;
          // if(equipment.fileList[i].response!=null){
          //   fileId=equipment.fileList[i].response.data.fid;
          // }else{
          //   fileId=equipment.fileList[i].uid;
          // }
          var doc = {};
          var names = equipment.fileList[i].name.split('.');
          doc.name = data['name_' + fileId] + '.' + names[1];
          if (equipment.fileList[i].response != null) {
            doc.path = equipment.fileList[i].response.data.url;
            doc.swfPath = equipment.fileList[i].response.data.swfUrl;
          } else {
            doc.path = equipment.fileList[i].url;
            doc.swfPath = equipment.fileList[i].swfPath;
          }
          var equipmentDocCatalogs = [];
          if (equipment.fileCatalogs != null && equipment.fileCatalogs.length > 0) {
            for (var j = 0; j < equipment.fileCatalogs.length; j++) {
              if (
                equipment.fileCatalogs[j].fileId == fileId &&
                equipment.fileCatalogs[j].catalogs != null &&
                equipment.fileCatalogs[j].catalogs.length > 0
              ) {
                for (var k = 0; k < equipment.fileCatalogs[j].catalogs.length; k++) {
                  var docCatalog = {};
                  if (fileId == equipment.curFileId) {
                    docCatalog.name =
                      data['name_' + fileId + '_' + equipment.fileCatalogs[j].catalogs[k]];
                    docCatalog.page =
                      data['page_' + fileId + '_' + equipment.fileCatalogs[j].catalogs[k]];
                  } else {
                    docCatalog.name =
                      equipment.catalogNames[
                        'name_' + fileId + '_' + equipment.fileCatalogs[j].catalogs[k]
                      ];
                    docCatalog.page =
                      equipment.catalogNames[
                        'page_' + fileId + '_' + equipment.fileCatalogs[j].catalogs[k]
                      ];
                  }
                  docCatalog.sort = i + 1;
                  equipmentDocCatalogs.push(docCatalog);
                }
              }
            }
          }
          doc.equipmentDocCatalogs = equipmentDocCatalogs;
          equipmentDocs.push(doc);
        }
      }
      data.equipmentDocs = equipmentDocs;
      //文档数据
      //文档目录数据
      data.id = equipment.modalType === 'create' ? '' : item.id;
      dispatch({
        type: `equipment/${equipment.modalType}`,
        payload: data,
        search: equipment.selectObj,
      });
    });
  }

  const onGetLocation = () => {
    const data = getFieldsValue();
    dispatch({
      type: 'equipment/updateState',
      payload: {
        locationModalVisible: true,
        newKey1: new Date().getTime() + '',
        locationItem: { lonLat: data.lonLat },
      },
    });
  };

  const uploadProps = {
    action: baseUrl + '/common/uploadFileSwf.jhtml',
    withCredentials: true,
    multiple: true,
    listType: 'text',
    onChange: handleChange,
    //accept: "application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.wordprocessingml.document,'application/msword,application/pdf",
    onRemove: file => {
      const index = equipment.fileList.indexOf(file);
      const newFileList = equipment.fileList.slice();
      newFileList.splice(index, 1);
      dispatch({
        type: 'equipment/updateState',
        payload: {
          fileList: newFileList,
        },
      });
    },
  };
  function handleChange(info) {
    let fileList = info.fileList;
    let fileCatalogs = equipment.fileCatalogs;

    if (info.file.status === 'done') {
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
        setFieldsValue({ path: info.file.response.data.swfUrl });
        let fileCatalog = {};
        fileCatalog.uuid = 0;
        fileCatalog.catalogs = [];
        fileCatalog.fileId = info.file.uid;
        fileCatalogs.push(fileCatalog);
      } else {
        message.error(info.file.response.msg);
      }
      fileList = fileList.filter(file => {
        return true;
      });
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }
    //console.log(fileList);
    dispatch({
      type: 'equipment/updateState',
      payload: {
        fileList: fileList,
        fileCatalogs: fileCatalogs,
      },
    });
    //setFieldsValue({ fileName: fileList[0].name });
  }

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  function initFile(files) {}
  function fileName(fileName, index) {
    let names = fileName.split('.');
    return names[index];
  }
  function onRemoveFile(fileId, oldFileId) {
    debugger;
    var formValue = getFieldsValue();
    //更新fileList
    var fileList = equipment.fileList;

    //更新catalogNames
    var catalogNames = equipment.catalogNames;
    var fileCatalogs = equipment.fileCatalogs;
    var newCatalogNames = {};
    var newFileCatalogs = [];
    if (fileList != null && fileList.length > 0) {
      if (fileCatalogs != null && fileCatalogs.length > 0) {
        for (var j = 0; j < fileCatalogs.length; j++) {
          if (fileCatalogs[j].fileId != fileId) {
            newFileCatalogs.push(fileCatalogs[j]);
            //fileCatalogs.splice(j, 1);
          }
        }
      }
      if (newFileCatalogs != null && newFileCatalogs.length > 0) {
        for (var i = 0; i < newFileCatalogs.length; i++) {
          if (newFileCatalogs[i].catalogs != null && newFileCatalogs[i].catalogs.length > 0) {
            for (var j = 0; j < newFileCatalogs[i].catalogs.length; j++) {
              var name = 'name_' + newFileCatalogs[i].fileId + '_' + newFileCatalogs[i].catalogs[j];
              var page = 'page_' + newFileCatalogs[i].fileId + '_' + newFileCatalogs[i].catalogs[j];
              if (newFileCatalogs[i].fileId == oldFileId) {
                newCatalogNames[name] = formValue[name];
                newCatalogNames[page] = formValue[page];
              } else {
                newCatalogNames[name] = catalogNames[name];
                newCatalogNames[page] = catalogNames[page];
              }
            }
          }
        }
      }
      for (var i = 0; i < fileList.length; i++) {
        if (fileList[i].uid == fileId) {
          fileList.splice(i, 1);
          break;
        }
      }
      //更新fileCatalogs
      dispatch({
        type: 'equipment/updateState',
        payload: {
          fileCatalogs: newFileCatalogs,
          catalogNames: newCatalogNames,
          fileList: fileList,
        },
      });
    }
  }

  /*
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a onClick={() => showMulu(k.uid,equipment.curFileId)}>添加目录</a>
*/
  function showMulu(fileId, prevFileId) {
    var catalogNames = equipment.catalogNames;
    var fileCatalogs = equipment.fileCatalogs;
    var formValue = getFieldsValue();

    if (fileCatalogs != null && fileCatalogs.length > 0) {
      for (var i = 0; i < fileCatalogs.length; i++) {
        if (
          fileCatalogs[i].catalogs != null &&
          fileCatalogs[i].catalogs.length > 0 &&
          fileCatalogs[i].fileId == prevFileId
        ) {
          for (var j = 0; j < fileCatalogs[i].catalogs.length; j++) {
            var name = 'name_' + fileCatalogs[i].fileId + '_' + fileCatalogs[i].catalogs[j];
            var page = 'page_' + fileCatalogs[i].fileId + '_' + fileCatalogs[i].catalogs[j];
            catalogNames[name] = formValue[name];
            catalogNames[page] = formValue[page];
          }
        }
      }
    }
    //console.log(catalogNames);
    dispatch({
      type: 'equipment/updateState',
      payload: {
        curFileId: fileId,
        catalogNames: catalogNames,
      },
    });
  }
  function initMulu(fileId) {
    if (fileId == null || fileId == '') {
      return '';
    }
    var fileCatalogs = equipment.fileCatalogs;
    var catalogs = [];
    if (fileCatalogs != null && fileCatalogs.length > 0) {
      for (var i = 0; i < fileCatalogs.length; i++) {
        if (fileCatalogs[i].fileId == fileId) {
          catalogs = fileCatalogs[i].catalogs;
        }
      }
    }
    //  console.log(equipment.catalogNames);
    return catalogs != null && catalogs.length > 0
      ? catalogs.map((k, index) => {
          return (
            <div>
              <Row key={'page' + k}>
                <Col span={11}>
                  <FormItem {...formItemLayout1} label="目录名称" hasFeedback>
                    {getFieldDecorator(`name_${fileId}_${k}`, {
                      initialValue: equipment.catalogNames['name_' + fileId + '_' + k],
                      rules: [
                        { required: true, whitespace: true, message: '请输入目录名称' },
                        { max: 50, message: '最长不超过50个字符' },
                      ],
                    })(<Input />)}
                  </FormItem>
                </Col>
                <Col span={11}>
                  <FormItem {...formItemLayout1} label="起始页码" hasFeedback>
                    {getFieldDecorator(`page_${fileId}_${k}`, {
                      initialValue: equipment.catalogNames['page_' + fileId + '_' + k],
                      rules: [{ required: true, message: '请输入起始页码' }],
                    })(<InputNumber min={1} max={99999999} style={{ width: '80%' }} />)}
                  </FormItem>
                </Col>
                <Col span={2}>
                  <Popconfirm title="确定要删除吗？" onConfirm={() => onRemoveClg(fileId, index)}>
                    <Icon
                      className="dynamic-delete-button"
                      type="minus-circle-o"
                      style={{ cursor: 'pointer' }}
                    />
                  </Popconfirm>
                </Col>
              </Row>
            </div>
          );
        })
      : null;
  }
  function onAddCatalog(fileId) {
    var fileCatalogs = equipment.fileCatalogs;
    var uuid = 0;
    var catalogs = [],
      newFileCatalogs = [];
    if (fileCatalogs != null && fileCatalogs.length > 0) {
      for (var i = 0; i < fileCatalogs.length; i++) {
        if (fileCatalogs[i].fileId == fileId) {
          catalogs = fileCatalogs[i].catalogs;
          uuid = fileCatalogs[i].uuid;
        } else {
          newFileCatalogs.push(fileCatalogs[i]);
        }
      }
    }
    uuid++;
    var fileCatalog = {};
    fileCatalog.fileId = fileId;
    fileCatalog.uuid = uuid;
    catalogs.push(uuid);
    fileCatalog.catalogs = catalogs;
    newFileCatalogs.push(fileCatalog);
    dispatch({
      type: 'equipment/updateState',
      payload: {
        fileCatalogs: newFileCatalogs,
      },
    });
  }
  function onChange(e) {
    console.log(e.target.value);
  }
  function onRemoveClg(fileId, index) {
    var fileCatalogs = equipment.fileCatalogs;
    var catalogs = [],
      newFileCatalogs = [];
    if (fileCatalogs != null && fileCatalogs.length > 0) {
      for (var i = 0; i < fileCatalogs.length; i++) {
        if (fileCatalogs[i].fileId == fileId) {
          catalogs = fileCatalogs[i].catalogs;
        } else {
          newFileCatalogs.push(fileCatalogs[i]);
        }
      }
    }
    catalogs.splice(index, 1);
    var fileCatalog = {};
    fileCatalog.fileId = fileId;
    fileCatalog.catalogs = catalogs;
    newFileCatalogs.push(fileCatalog);

    dispatch({
      type: 'equipment/updateState',
      payload: {
        fileCatalogs: newFileCatalogs,
      },
    });
  }
  console.log(equipment.fileCatalogs);
  function content(fileName) {
    return (
      <div>
        <p>选中设置“{fileName}”文档目录</p>
      </div>
    );
  }

  function checkVesselDiameter(rule, value, callback) {
    if (value > 999) {
      callback('消防设施半径不能超过999');
    } else {
      callback();
    }
  }

  function checkVolume(rule, value, callback) {
    if (value > 999) {
      callback('装置不能超过999');
    } else {
      callback();
    }
  }

  function handleDownload(url, index) {
    // let file = equipment.fileList[index];
    // var values = { filePath: file.url, fileName: file.name };
    // var param = JSON.stringify(values);
    // param = encodeURIComponent(param);
    // window.open(baseUrl + '/common/downLoadFile.jhtml?param=' + param);
    let file = equipment.fileList[index];
    if (!!!url) {
      if(!file.response)return false;
      url = baseFileUrl + file.response.data.url;
    }else{
      url = baseFileUrl+url;
    };
    window.open(url);
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={12}>
            <FormItem label="装置名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '请填写生产装置名称' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写生产装置名称" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="装置编码:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('code', {
                initialValue: item.code,
                rules: [
                  { required: true, message: '请填写装置编码' },
                  { max: 20, message: '最长不超过20个字' },
                ],
              })(<Input type="text" placeholder="请填写装置编码" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="所属类型:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('typeId', {
                initialValue: item.typeId == null ? undefined : item.typeId + '',

                rules: [{ required: true, message: '请选择所属类型' }],
              })(
                <Select placeholder="请选择所属类型">
                  {loopOption(equipment.equipmentTypeList)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="重点单位:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('keyUnitId', {
                initialValue: item.keyUnitId == null ? equipment.keyUnitId : item.keyUnitId + '',
                rules: [{ required: true, message: '请选择所属重点单位' }],
              })(
                <Select showSearch optionFilterProp="children" placeholder="请选择所属重点单位">
                  {loopOption(equipment.keyUnitList)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="介质:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('medium', {
                initialValue: item.medium,
                rules: [
                  { required: true, message: '请填写介质' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写编码" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="耐火等级:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('fireLevel', {
                initialValue: item.fireLevel ? item.fireLevel + '' : undefined,
                rules: [{ required: true, message: '请选择耐火等级' }],
              })(
                <Select placeholder="请选择耐火等级">
                  <Option value="一级">一级</Option>
                  <Option value="二级">二级</Option>
                  <Option value="三级">三级</Option>
                  <Option value="四级">四级</Option>
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="容器直径:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('vesselDiameter', {
                initialValue: item.vesselDiameter,
                rules: [
                  {
                    validator: checkVesselDiameter,
                  },
                ],
              })(<InputNumber style={{ width: '100%' }} placeholder="请填写容器直径" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="装置体积:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('volume', {
                initialValue: item.volume,
                rules: [
                  {
                    validator: checkVolume,
                  },
                ],
              })(<InputNumber style={{ width: '100%' }} placeholder="请填写装置体积" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="获取坐标点:" hasFeedback {...formItemLayout}>
              <div
                style={{
                  marginRight: '10px',
                  marginTop: '5px',
                  width: '28px',
                  height: '28px',
                  background: 'url(images/iconAdd.png)',
                  display: 'inlineBlock',
                  float: 'left',
                  cursor: 'pointer',
                }}
                onClick={onGetLocation}
              />
              {getFieldDecorator('lonLat', {
                initialValue: equipment.locationItem.lonLat
                  ? equipment.locationItem.lonLat + ''
                  : undefined,
              })(
                <Input type="text" readOnly style={{ width: '88%' }} placeholder="请选择经纬度" />
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem {...formItemLayout} label="上传文件">
              {getFieldDecorator('fileList', {
                initialValue: equipment.fileList.length > 0 ? equipment.fileList : undefined,
                // rules: [{ required: true, message: '请上传装置文件' }],
              })(
                <Upload {...uploadProps} showUploadList={false} fileList={equipment.fileList}>
                  <Button>
                    <Icon type="upload" /> 上传装置文件
                  </Button>
                </Upload>
              )}
            </FormItem>
          </Col>
        </Row>

        {equipment.fileList != null && equipment.fileList.length > 0 ? (
          <Row>
            <Col span={2}>&nbsp;&nbsp;</Col>
            <Col span={9} style={{ textAlign: 'center' }}>
              <span>文件名称</span>
            </Col>
            <Col span={6} style={{ textAlign: 'center' }}>
              <span>文件类型</span>
            </Col>
            <Col span={7} style={{ textAlign: 'center' }}>
              <span>操作</span>
            </Col>
          </Row>
        ) : (
          ''
        )}
        {equipment.fileList != null && equipment.fileList.length > 0
          ? equipment.fileList.map((k, index) => {
              return (
                <Row key={'page' + k}>
                  <Col span={2} style={{ left: '30px', top: '10px' }}>
                    <Popover
                      content={content(getFieldValue(`name_${k.uid}`))}
                      title="文档目录设置"
                      trigger="hover"
                    >
                      {k.uid == equipment.curFileId ? (
                        <Icon
                          type="check-circle"
                          style={{ color: 'blue', cursor: 'pointer', lineHeight: '42px' }}
                        />
                      ) : (
                        <Icon
                          type="check-circle-o"
                          style={{ cursor: 'pointer', lineHeight: '42px' }}
                          onClick={() => showMulu(k.uid, equipment.curFileId)}
                        />
                      )}
                    </Popover>
                  </Col>
                  <Col span={9} style={{ textAlign: 'center' }}>
                    <FormItem hasFeedback>
                      {getFieldDecorator(`name_${k.uid}`, {
                        initialValue: fileName(k.name, 0),
                        rules: [
                          { required: true, whitespace: true, message: '请输入文件名称' },
                          { max: 50, message: '最长不超过50个字符' },
                        ],
                      })(<Input style={{ width: '100%' }} />)}
                    </FormItem>
                  </Col>
                  <Col span={6} style={{ textAlign: 'center' }}>
                    <FormItem>
                      <span>{fileName(k.name, 1)}</span>
                    </FormItem>
                  </Col>
                  <Col span={7} style={{ textAlign: 'center', lineHeight: '42px' }}>
                    <Popconfirm
                      title="确定要删除吗？"
                      onConfirm={() => onRemoveFile(k.uid, equipment.curFileId)}
                    >
                      <a>删除</a>
                    </Popconfirm>
                    &nbsp;&nbsp;
                    <a href="javascript:void(0)" onClick={() => handleDownload(k.url, index)}>
                      下载
                    </a>
                  </Col>
                </Row>
              );
            })
          : ''}
        {initMulu(equipment.curFileId)}
        {equipment.curFileId != null && equipment.curFileId != '' ? (
          <Row>
            <Col span={16}>&nbsp;</Col>
            <Col span={8} style={{ textAlign: 'right' }}>
              <Button icon="plus" type="primary" onClick={() => onAddCatalog(equipment.curFileId)}>
                新增
              </Button>
            </Col>
          </Row>
        ) : (
          ''
        )}
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { equipment: state.equipment };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
