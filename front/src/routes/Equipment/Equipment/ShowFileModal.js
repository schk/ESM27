import React from 'react';
import { Form, Input, Modal, Popover, Button, Row, Col, InputNumber, Icon } from 'antd';
import { connect } from 'dva';
const FormItem = Form.Item;
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 16,
  },
};
const formItemLayout1 = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 14,
  },
};

let AddEditModal = ({ equipment, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: '查看信息',
    visible: equipment.findModalVisible,
    maskClosable: false,
    width: 1000,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
    ],
  };

  if (!equipment.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'equipment/updateState',
      payload: {
        findModalVisible: false,
        locationItem: {},
      },
    });
  }

  function initMulu(fileId) {
    if (fileId == null || fileId == '') {
      return '';
    }
    var fileCatalogs = equipment.fileCatalogs;
    var catalogs = [];
    if (fileCatalogs != null && fileCatalogs.length > 0) {
      for (var i = 0; i < fileCatalogs.length; i++) {
        if (fileCatalogs[i].fileId == fileId) {
          catalogs = fileCatalogs[i].catalogs;
        }
      }
    }
    //  console.log(equipment.catalogNames);
    return catalogs != null && catalogs.length > 0
      ? catalogs.map((k, index) => {
          return (
            <div>
              <Row key={'page' + k}>
                <Col span={12}>
                  <FormItem {...formItemLayout1} label="目录名称" hasFeedback>
                    {getFieldDecorator(`name_${fileId}_${k}`, {
                      initialValue: equipment.catalogNames['name_' + fileId + '_' + k],
                      rules: [
                        { required: true, whitespace: true, message: '请输入目录名称' },
                        { max: 50, message: '最长不超过50个字符' },
                      ],
                    })(<Input />)}
                  </FormItem>
                </Col>
                <Col span={12}>
                  <FormItem {...formItemLayout1} label="起始页码" hasFeedback>
                    {getFieldDecorator(`page_${fileId}_${k}`, {
                      initialValue: equipment.catalogNames['page_' + fileId + '_' + k],
                      rules: [{ required: true, message: '请输入起始页码' }],
                    })(<InputNumber min={1} max={99999999} style={{ width: '80%' }} />)}
                  </FormItem>
                </Col>
              </Row>
            </div>
          );
        })
      : null;
  }
  function showMulu(fileId, prevFileId) {
    var catalogNames = equipment.catalogNames;
    var fileCatalogs = equipment.fileCatalogs;
    var formValue = getFieldsValue();

    if (fileCatalogs != null && fileCatalogs.length > 0) {
      for (var i = 0; i < fileCatalogs.length; i++) {
        if (
          fileCatalogs[i].catalogs != null &&
          fileCatalogs[i].catalogs.length > 0 &&
          fileCatalogs[i].fileId == prevFileId
        ) {
          for (var j = 0; j < fileCatalogs[i].catalogs.length; j++) {
            var name = 'name_' + fileCatalogs[i].fileId + '_' + fileCatalogs[i].catalogs[j];
            var page = 'page_' + fileCatalogs[i].fileId + '_' + fileCatalogs[i].catalogs[j];
            catalogNames[name] = formValue[name];
            catalogNames[page] = formValue[page];
          }
        }
      }
    }
    //console.log(catalogNames);
    dispatch({
      type: 'equipment/updateState',
      payload: {
        curFileId: fileId,
        catalogNames: catalogNames,
      },
    });
  }
  function content(fileName) {
    return (
      <div>
        <p>选中设置“{fileName}”文档目录</p>
      </div>
    );
  }
  function fileName(fileName, index) {
    let names = fileName.split('.');
    return names[index];
  }
  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={12}>
            <FormItem label="装置名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [{ required: true }],
              })(<Input type="text" readOnly placeholder="生产装置名称" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="装置编码:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('code', {
                initialValue: item.code,
                rules: [{ required: true }],
              })(<Input type="text" readOnly placeholder="装置编码" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="所属类型:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('equipmentTypeName', {
                initialValue: item.equipmentTypeName == null ? undefined : item.equipmentTypeName,
                rules: [{ required: true }],
              })(<Input type="text" readOnly placeholder="" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="重点单位:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('keyUnitName', {
                initialValue: item.keyUnitName == null ? equipment.keyUnitId : item.keyUnitName,
                rules: [{ required: true, message: '所属重点单位' }],
              })(<Input type="text" readOnly placeholder="" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="介质:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('medium', {
                initialValue: item.medium,
                rules: [{ required: true }],
              })(<Input type="text" placeholder="介质" readOnly />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="耐火等级:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('fireLevel', {
                initialValue: item.fireLevel,
                rules: [{ required: true, message: '耐火等级' }],
              })(<Input type="text" placeholder="耐火等级" readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="容器直径:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('vesselDiameter', {
                initialValue: item.vesselDiameter,
              })(<InputNumber readOnly style={{ width: '100%' }} placeholder="容器直径" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="装置体积:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('volume', {
                initialValue: item.volume,
              })(<InputNumber readOnly style={{ width: '100%' }} placeholder="装置体积" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem required label="经纬度:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('lonLat', {
                initialValue: equipment.locationItem.lonLat
                  ? equipment.locationItem.lonLat + ''
                  : undefined,
              })(<Input type="text" readOnly placeholder="经纬度" />)}
            </FormItem>
          </Col>
        </Row>
        {equipment.fileList != null && equipment.fileList.length > 0 ? (
          <Row>
            <Col span={3}>&nbsp;&nbsp;</Col>
            <Col span={8} style={{ textAlign: 'center' }}>
              <span>文件名称</span>
            </Col>
            <Col span={6} style={{ textAlign: 'center' }}>
              <span>文件类型</span>
            </Col>
          </Row>
        ) : (
          ''
        )}
        {equipment.fileList != null && equipment.fileList.length > 0
          ? equipment.fileList.map((k, index) => {
              return (
                <Row key={'page' + k}>
                  <Col span={3} style={{ left: '80px', top: '10px' }}>
                    {k.uid == equipment.curFileId ? (
                      <Icon
                        type="check-circle"
                        style={{ color: 'blue', cursor: 'pointer', lineHeight: '42px' }}
                      />
                    ) : (
                      <Icon
                        type="check-circle-o"
                        style={{ cursor: 'pointer', lineHeight: '42px' }}
                        onClick={() => showMulu(k.uid, equipment.curFileId)}
                      />
                    )}
                  </Col>
                  <Col span={8} style={{ textAlign: 'center' }}>
                    <FormItem hasFeedback>
                      {getFieldDecorator(`name_${k.uid}`, {
                        initialValue: fileName(k.name, 0),
                        rules: [{ required: true, whitespace: true, message: '目录名称' }],
                      })(<Input style={{ width: '100%' }} />)}
                    </FormItem>
                  </Col>
                  <Col span={6} style={{ textAlign: 'center' }}>
                    <FormItem>
                      <span>{fileName(k.name, 1)}</span>
                    </FormItem>
                  </Col>
                </Row>
              );
            })
          : ''}
        {initMulu(equipment.curFileId)}
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { equipment: state.equipment };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
