import React, { Fragment } from 'react';
import {
  Table,
  Form,
  Button,
  Popconfirm,
  Card,
  Input,
  Divider,
  Row,
  Col,
  Select,
  Tabs,
  Tree,
  Modal,
  Icon,
  Upload,
  message,
} from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import LocationModal from './LocationModal';
import ShowFileModal from './ShowFileModal';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import { baseUrl } from '../../../config/system';
const Option = Select.Option;
const TreeNode = Tree.TreeNode;
const confirm = Modal.confirm;

function Equipment({ location, equipment, form, dispatch, loading }) {
  const {
    getFieldDecorator,
    getFieldsValue,
    getFieldValue,
    getFieldProps,
    setFieldsValue,
    resetFields,
  } = form;
  const AddEditModalProps = {
    item: equipment.item,
  };
  const findModalProps = {
    item: equipment.item,
  };

  const LocationModalProps = {};
  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    {
      title: '装置名称',
      dataIndex: 'name',
      key: 'name',
      width: 100,
      render: (text, record, value) => (
        <Fragment>
          <a onClick={() => onFindInfo(record.id_)}>{record.name}</a>
        </Fragment>
      ),
    },
    { title: '设备编码', dataIndex: 'code', key: 'code', width: 100 },
    { title: '所属类别', dataIndex: 'equipmentTypeName', key: 'equipmentTypeName', width: 100 },
    { title: '所属单位', dataIndex: 'keyUnitName', key: 'keyUnitName ', width: 100 },
    { title: '介质', dataIndex: 'medium', key: 'medium', width: 100 },
    {
      title: '经纬度',
      dataIndex: 'lon',
      key: 'lon',
      width: 130,
      render: (text, record) => {
        if(record.lon && record.lat){
          return record.lon+ ','+ record.lat;
        }
        return '';
      },
    },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 100,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_)}>修改</a>
          <Divider type="vertical" />
          {/* <a onClick={() => onFindInfo(record.id_)}>查看</a>
          <Divider type="vertical" /> */}
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];
  const onFindInfo = id => {
    dispatch({
      type: 'equipment/findInfo',
      payload: id,
    });
  };
  function onUpdate(id) {
    dispatch({
      type: 'equipment/info',
      payload: id,
    });
  }

  function onAdd() {
    dispatch({
      type: 'equipment/updateState',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'equipment/del',
      payload: id,
      search: {
        keyUnitId: equipment.keyUnitId,
        pageNum: equipment.current,
        pageSize: equipment.pageSize,
        ...getFieldsValue(),
      },
    });
  }

  function handleSearch() {
    dispatch({
      type: 'equipment/qryListByParams',
      payload: {
        pageNum: 1,
        pageSize: equipment.pageSize,
        ...getFieldsValue(),
        keyUnitId: equipment.keyUnitId,
      },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'equipment/qryListByParams',
      payload: { pageNum: 1, pageSize: equipment.pageSize, keyUnitId: equipment.keyUnitId },
    });
  }

  function callback(key) {
    console.log(key);
  }

  const loop = data =>
    data.map(item => {
      return (
        <TreeNode
          key={item.id_}
          title={item.name}
          icon={
            <Icon
              style={{
                width: '10px',
                height: '10px',
                background: 'url(images/liBgIcon.png)',
                display: 'inlineBlock',
                verticalAlign: '1px',
              }}
            />
          }
        />
      );
    });

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  const onSelect = info => {
    if (info.length > 0) {
      dispatch({
        type: 'equipment/qryListByParams',
        payload: { ...getFieldsValue(), ...{ keyUnitId: info[0] } },
      });
    } else {
      dispatch({
        type: 'equipment/qryListByParams',
        payload: { ...getFieldsValue(), ...{ keyUnitId: undefined } },
      });
    }
  };

  const pagination = {
    current: equipment.current,
    pageSize: equipment.pageSize,
    total: equipment.total,
    showSizeChanger: true,
    showTotal: total => '共' + equipment.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'equipment/qryListByParams',
        payload: {
          pageNum: current,
          pageSize: size,
          ...getFieldsValue(),
          keyUnitId: equipment.keyUnitId,
        },
      });
    },
    onChange(current) {
      dispatch({
        type: 'equipment/qryListByParams',
        payload: {
          pageNum: current,
          pageSize: equipment.pageSize,
          ...getFieldsValue(),
          keyUnitId: equipment.keyUnitId,
        },
      });
    },
  };

  function handleTableChange(pagination, filters, sorter) {
    let isChinese = ['account', 'name'].indexOf(sorter.field) >= 0;
    dispatch({
      type: 'equipment/qryListByParams',
      payload: {
        pageNum: pagination.current,
        pageSize: pagination.pageSize,
        ...getFieldsValue(),
        orderBy: sorter.field,
        order: sorter.order,
        isChinese,
      },
    });
  }

  function downloadTemplate() {
    window.open(baseUrl + '/execl/downLoadTemplate.jhtml?type=equipmentTemplate');
  }

  function initLoopMsg(values) {
    return values != null && values.length > 0
      ? values.map((k, index) => {
          return <div key={index}>{k}</div>;
        })
      : null;
  }

  const props = {
    name: 'file',
    action: baseUrl + '/equipment/excel/importExcel.jhtml',
    beforeUpload(file) {
      // const isJPG = file.type === 'application/vnd.ms-excel' || file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
      // if (!isJPG) {
      //   message.error('只能上传EXCEL文件！');
      // }
      // return isJPG;
    },
    onChange(info) {
      let fileList = info.fileList;
      if (info.file.status === 'done') {
        if (info.file.response.httpCode == 200) {
          if (info.file.response.errorCode == 1) {
            //弹出确认取消框
            confirm({
              title: '信息提示框',
              content: initLoopMsg(info.file.response.data),
              okText: '继续导入',
              cancelText: '取消导入',
              onOk() {
                dispatch({
                  type: 'equipment/importExcelConfirm',
                  payload: info.file.response.times,
                });
              },
              onCancel() {
                dispatch({
                  type: 'equipment/delExcelTemp',
                  payload: info.file.response.times,
                });
              },
            });
          } else if (info.file.response.errorCode == 2) {
            if (info.file.response.data && info.file.response.data.length > 0) {
              Modal.error({
                title: '导入提示',
                content: initLoopMsg(info.file.response.data),
              });
            }
          } else {
            message.success(`${info.file.name} 上传成功`);
            dispatch({
              type: 'equipment/qryListByParams',
              payload: { pageNum: 1, pageSize: 10, keyUnitId: undefined },
            });
          }
        } else {
          Modal.error({
            title: '导入提示',
            content: info.file.response.msg,
          });
        }
        fileList = fileList.filter(file => {
          return file.response && file.response.fid;
        });
        //只保留最后一条记录
        fileList = fileList.slice(-1);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`);
      }
      let fildsValue = {};
      fildsValue['file'] = fileList.map(file => {
        return file;
      });
      setFieldsValue(fildsValue);
    },
  };

  const LocationModalGen = () => <LocationModal {...LocationModalProps} />;
  return (
    <PageHeaderLayout>
      <div style={{ backgroundColor: '#fff', position: 'relative', overflow: 'hidden' }}>
        <div
          style={{
            width: '250px',
            position: 'absolute',
            left: '0',
            right: '0',
            bottom: '0',
            top: '0',
          }}
        >
          <div
            style={{
              width: '100%',
              height: '40px',
              backgroundColor: '#232f40',
              lineHeight: '40px',
              color: '#fff',
              paddingLeft: '20px',
              fontSize: '14px',
              fontWeight: '400',
            }}
          >
            重点单位
          </div>
          <div
            style={{
              position: 'absolute',
              left: '0',
              top: '40px',
              bottom: '0',
              right: '0',
              overflow: 'auto',
            }}
          >
            <Tree defaultExpandedKeys={['1']} onSelect={onSelect} showIcon>
              {equipment.keyUnitList && loop(equipment.keyUnitList)}
            </Tree>
          </div>
        </div>
        <div
          style={{
            marginLeft: '250px',
            paddingTop: '0px',
            borderLeft: '1px solid #e8e8e8',
            minHeight: '600px',
          }}
        >
          <Card bordered={false}>
            <div className={styles.tableList}>
              <div className={styles.tableListForm}>
                <Form layout="inline">
                  <Row gutter={{ md: 8, lg: 24, xl: 24 }}>
                    <Col span={5} sm={5}>
                      <Form.Item label="名称：">
                        {getFieldDecorator('name')(<Input placeholder="输入名称查找" />)}
                      </Form.Item>
                    </Col>
                    <Col span={7} sm={6}>
                      <Form.Item label="所属类型：">
                        {getFieldDecorator('typeId')(
                          <Select allowClear placeholder="请选择所属类型">
                            <Option value="">全部</Option>
                            {loopOption(equipment.equipmentTypeList)}
                          </Select>
                        )}
                      </Form.Item>
                    </Col>

                    <Col span={5} sm={5}>
                      <Form.Item label="编号：">
                        {getFieldDecorator('code')(<Input placeholder="输入编号查找" />)}
                      </Form.Item>
                    </Col>
                    <Col md={5} sm={4}>
                      <span className={styles.submitButtons}>
                        <Button type="primary" htmlType="submit" onClick={handleSearch}>
                          查询
                        </Button>
                        <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                          重置
                        </Button>
                      </span>
                    </Col>
                  </Row>
                </Form>
              </div>
              <div className={styles.submitButtons}>
                <Button icon="plus" type="primary" onClick={onAdd}>
                  新增
                </Button>

                <Button
                  style={{ marginLeft: 80 }}
                  type="primary"
                  icon="download"
                  onClick={downloadTemplate}
                >
                  下载模板
                </Button>
                <Upload
                  {...getFieldProps(
                    'file',
                    {
                      validate: [
                        {
                          rules: [{ type: 'array', required: true, message: '请添加数据文件' }],
                          trigger: 'onBlur',
                        },
                      ],
                    },
                    { valuePropName: 'fileIds' }
                  )}
                  {...props}
                  fileList={getFieldValue('file')}
                >
                  <Button style={{ marginLeft: 8 }} type="primary" icon="upload">
                    导入
                  </Button>
                </Upload>
              </div>
              <div style={{ width: '100%' }}>
                <Table
                  columns={columns}
                  dataSource={equipment.list}
                  style={{ marginTop: 10 }}
                  pagination={pagination}
                  loading={equipment.loading}
                  rowKey={record => record.id_}
                />
              </div>
            </div>
          </Card>
        </div>
      </div>
      <AddEditModal {...AddEditModalProps} />
      <ShowFileModal {...findModalProps} />
      <LocationModalGen />
    </PageHeaderLayout>
  );
}

function mapStateToProps(state) {
  return {
    equipment: state.equipment,
    loading: state.loading.models.equipment,
  };
}

Equipment = Form.create()(Equipment);

export default connect(mapStateToProps)(Equipment);
