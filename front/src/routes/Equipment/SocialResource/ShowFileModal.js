import React, { Fragment } from 'react';
import { Form, Input, Modal, Select, Button, Row, Table, Col, Upload, message, Tabs } from 'antd';
import { baseUrl, baseFileUrl } from '../../../config/system';
import { connect } from 'dva';
const FormItem = Form.Item;
const { TextArea } = Input;
const TabPane = Tabs.TabPane;

const formItemLayout = {
  labelCol: { span: 7 },
  wrapperCol: { span: 16 },
};

const formItemLayout1 = {
  labelCol: { span: 3 },
  wrapperCol: { span: 20 },
};
let ShowFileModal = ({ socialResource, item, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: '查看信息',
    visible: socialResource.findModalVisible,
    maskClosable: false,
    width: 800,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
    ],
  };

  if (!socialResource.findModalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'socialResource/updateState',
      payload: {
        findModalVisible: false,
        activityKey: 'tab1',
      },
    });
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = {
        ...getFieldsValue(),
        id: socialResource.modalType == 'create' ? '0' : item.id_,
      };

      dispatch({
        type: `socialResource/${socialResource.modalType}`,
        payload: data,
        search: socialResource.selectObj,
      });
    });
  }

  //图片上传
  const pictureUploadProps = {
    action: baseUrl + '/common/uploadImg.jhtml',
    withCredentials: true,
    listType: 'picture-card',
    onChange: pictureHandleChange,
    onPreview: handlePreview,
  };
  function handlePreview(info) {
    dispatch({
      type: 'socialResource/updateState',
      payload: {
        previewVisible: true,
        previewImage: info.url || info.thumbUrl,
      },
    });
  }
  //删除图片
  function pictureHandleRemove() {
    dispatch({
      type: 'socialResource/updateState',
      payload: {
        fileList: [],
      },
    });
  }

  //图片上传
  function pictureHandleChange(info) {
    let fileList = info.fileList;
    if (info.file.status === 'done') {
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
        setFieldsValue({ picture: info.file.response.data.fid });
      } else {
        message.error(info.file.response.msg);
      }
      fileList = fileList.filter(file => {
        return true;
      });
      //只保留最后一条记录
      fileList = fileList.slice(-1);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }

    dispatch({
      type: 'socialResource/updateState',
      payload: {
        fileList: fileList,
      },
    });
  }

  function onChangeTab(targetKey) {
    dispatch({
      type: 'socialResource/updateState',
      payload: { activityKey: targetKey },
    });
  }

  const onDetailUpdate = record => {
    var catalogIds = [];
    var catalogNames = {};
    var uuid = 0;
    if (record.keyUnitDangersCatalogs != null && record.keyUnitDangersCatalogs.length > 0) {
      for (var i = 0; i < record.keyUnitDangersCatalogs.length; i++) {
        catalogNames['name_' + (i + 1)] = record.keyUnitDangersCatalogs[i].name;
        catalogNames['page_' + (i + 1)] = record.keyUnitDangersCatalogs[i].page;
        catalogIds.push(i + 1);
        uuid++;
      }
    }
    dispatch({
      type: 'socialResource/updateState',
      payload: {
        FindModalDangersVisible: true,
        dangersItem: record,
        msdsPath: record.msdsPath,
        imgPath: record.imgPath,
        swfPath: record.swfPath,
        detailModalType: 'editDetail',
        catalogIds: catalogIds,
        uuid: uuid,
        catalogNames: catalogNames,
      },
    });
  };
  const columns = [
    { title: '中文名称', dataIndex: 'cName', key: 'cName', width: 120 },
    { title: '英文名', dataIndex: 'eName', key: 'eName', width: 100 },
    { title: '危险货物编号', dataIndex: 'dangerousNum', key: 'dangerousNum', width: 100 },
    { title: 'UN号', dataIndex: 'unNum', key: 'unNum', width: 100 },
    { title: 'CAS号', dataIndex: 'casNum', key: 'casNum', width: 100 },
    {
      title: '操作',
      dataIndex: 'operation',
      key: 'operation',
      width: 100,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onDetailUpdate(record)}>查看</a>
        </Fragment>
      ),
    },
  ];

  const accidentColumns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    { title: '文件名称', dataIndex: 'name', key: 'name', width: 120 },
    {
      title: '操作',
      dataIndex: 'operation',
      key: 'operation',
      width: 100,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onDownLoadAccidentFile(record)}>下载</a>
        </Fragment>
      ),
    },
  ];

  // function onDownLoadAccidentFile(record) {
  //   console.log(record);
  //   if (!!!record.url)
  //   var url = baseFileUrl + record.url;
  //   console.log(baseFileUrl + record.url);
  //   window.open(url);
  // }

  function onDownLoadAccidentFile(record) {
    var values = { filePath: record.url, fileName: record.name };
    var param = JSON.stringify(values);
    param = encodeURIComponent(param);
    window.open(baseFileUrl + '/common/downLoadFile.jhtml?param=' + param);
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Tabs type="card" activeKey={socialResource.activityKey} onChange={onChangeTab}>
          <TabPane tab="基本信息" key="tab1">
            <Row>
              <Col span={12}>
                <FormItem label="名称:" hasFeedback {...formItemLayout}>
                  {/* <label>{item.name}</label> */}
                  {getFieldDecorator('name', {
                    initialValue: item.name,
                  })(<Input type="text" readOnly placeholder="名称" />)}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem label="地址:" hasFeedback {...formItemLayout}>
                  {/* <label>{item.addr}</label> */}
                  {getFieldDecorator('addr', {
                    initialValue: item.addr,
                  })(<Input type="text" readOnly placeholder="地址" />)}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem label="所属消防战队:" hasFeedback {...formItemLayout}>
                  {/* <label>{item.fireBrigadeId}</label> */}
                  {getFieldDecorator('fireBrigade', {
                    initialValue: item.fireBrigade,
                  })(<Input type="text" readOnly placeholder="所属消防战队" />)}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem label="消防管理人员:" hasFeedback {...formItemLayout}>
                  {/* <label>{item.admin}</label> */}
                  {getFieldDecorator('admin', {
                    initialValue: item.admin,
                  })(<Input type="text" readOnly placeholder="消防管理人员" />)}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem label="值班室电话:" hasFeedback {...formItemLayout}>
                  {/* <label>{item.tel}</label> */}
                  {getFieldDecorator('tel', {
                    initialValue: item.tel,
                  })(<Input type="text" readOnly placeholder="重点单位值班室电话" />)}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem label="经纬度:" hasFeedback {...formItemLayout}>
                  {/* <label>{item.lonLat}</label> */}
                  {getFieldDecorator('lonLat', {
                    initialValue: item.lonLat,
                  })(<Input type="text" readOnly placeholder="重点单位位置坐标" />)}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem label="所属类型:" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('typeId', {
                    initialValue: item.typeName 
                  })(
                    <Input type="text" readOnly placeholder="所属类型" />
                  )}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem label="供水能力:" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('waterSupply', {
                    initialValue: item.waterSupply,
                  })(<Input type="text" readOnly placeholder="供水能力" />)}
                </FormItem>
              </Col>
            </Row>
            {/* <Row >
             <Col span={12}>
                <FormItem label="地图图标:" {...formItemLayout}>
                  {socialResource.locationItem.dtPath?
                    <div style={{borderRadius: '4px',border: '1px solid #d9d9d9',height: '64px',position: 'relative',clear: 'both',overflow: 'hidden',}}>
                    <div style={{width: '60px',height: '60px',textAlign: 'center',border: '1px solid #d9d9d9', position: 'relative', borderRadius: '5px'}}>
                      <img style={{position: 'absolute',left: '0',right: '0',bottom: '0',top: '0',margin: 'auto', width: '20px', height: '20px',}}
                        src={baseFileUrl + socialResource.locationItem.dtPath} alt=""
                      />
                    </div>
                  </div> :""}                    
                </FormItem>
              </Col>
            </Row> */}
            <Row style={{ marginLeft: '20px' }}>
              <FormItem label="平面图:" hasFeedback {...formItemLayout1}>
                {getFieldDecorator('fileList')(
                  <Upload {...pictureUploadProps} fileList={socialResource.fileList} />
                )}
              </FormItem>
            </Row>
            <Row style={{ marginLeft: '20px' }}>
              <Col span={24}>
                <FormItem label="重点单位简介:" hasFeedback {...formItemLayout1}>
                  {/* <label>{item.desc}</label> */}
                  {getFieldDecorator('desc', {
                    initialValue: item.desc,
                  })(<TextArea autosize={{ minRows: 4 }} />)}
                </FormItem>
              </Col>
            </Row>
          </TabPane>
        </Tabs>
      </Form>
    </Modal>
  );
};
function mapStateToProps(state) {
  return { socialResource: state.socialResource };
}

ShowFileModal = Form.create()(ShowFileModal);

export default connect(mapStateToProps)(ShowFileModal);
