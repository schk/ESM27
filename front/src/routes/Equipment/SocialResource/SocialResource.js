import React, { Fragment } from 'react';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Button,
  TreeSelect,
  message,
  Divider,
  Table,
  Popconfirm,
  Upload,
  Modal,
  Menu,
  Dropdown,
  Icon,
} from 'antd';
import { connect } from 'dva';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import LocationModal from './LocationModal';
import ShowFileModal from './ShowFileModal';
import { baseUrl } from '../../../config/system';
const FormItem = Form.Item;

function SocialResource({ socialResource, form, dispatch, loading }) {
  const {
    getFieldDecorator,
    getFieldsValue,
    setFieldsValue,
    resetFields,
    getFieldProps,
    getFieldValue,
  } = form;

  const AddEditModalProps = {
    item: socialResource.item,
  };
  const ShowEditModalProps = {
    item: socialResource.findItem,
  };
  const ShowAccidentFileModalProps = {
    item: socialResource.accidentItem,
  };

  const ShowPlansModalProps = {};

  const ShowPlansItemMadalProps = {
    item: socialResource.planItem,
  };
  const FindDangersModalProps = {
    item: socialResource.dangersItem,
    dispatch: dispatch,
    newKey: socialResource.newKey,
    msdsPath: socialResource.msdsPath,
    imgPath: socialResource.imgPath,
    swfPath: socialResource.swfPath,
    detailActiveKey: socialResource.detailActiveKey,
    visible: socialResource.FindModalDangersVisible,
    dangerTypeNoTopTree: socialResource.dangerTypeNoTopTree,
    title: '查看详情',
    catalogIds: socialResource.catalogIds,
    catalogNames: socialResource.catalogNames,
    uuid: socialResource.uuid,
    onCancel() {
      dispatch({
        type: 'socialResource/updateState',
        payload: {
          FindModalDangersVisible: false,
          dangersItem: {},
          msdsPath: '',
          swfPath: '',
          imgPath: '',
          detailActiveKey: 'tab1',
          newKey: new Date().getTime() + '',
          catalogIds: [],
          catalogNames: {},
          uuid: 0,
        },
      });
    },
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
    ],
  };

  const OldAddDangersModalProps = {
    item: socialResource.dangersItem,
    dispatch: dispatch,
    newKey: socialResource.newKey,
    msdsPath: socialResource.msdsPath,
    imgPath: socialResource.imgPath,
    swfPath: socialResource.swfPath,
    fileName: socialResource.fileName,
    detailActiveKey: socialResource.detailActiveKey,
    visible: socialResource.modalDangersVisible,
    dangerTypeNoTopTree: socialResource.dangerTypeNoTopTree,
    title: '编辑危化品信息',
    catalogIds: socialResource.catalogIds,
    catalogNames: socialResource.catalogNames,
    uuid: socialResource.uuid,
    onCancel() {
      dispatch({
        type: 'socialResource/updateState',
        payload: {
          modalDangersVisible: false,
          dangersItem: {},
          msdsPath: '',
          swfPath: '',
          imgPath: '',
          detailActiveKey: 'tab1',
          newKey: new Date().getTime() + '',
          catalogIds: [],
          catalogNames: {},
          uuid: 0,
        },
      });
    },
    onOk(data) {
      if (socialResource.detailModalType == 'createDetail') {
        data.id = new Date().getTime() + '';
        socialResource.dangersList.push(data);
      } else {
        for (var j = 0; j < socialResource.dangersList.length; j++) {
          if (socialResource.dangersList[j].id == data.id) {
            socialResource.dangersList[j] = data;
            break;
          }
        }
      }
      dispatch({
        type: 'socialResource/updateState',
        payload: {
          modalDangersVisible: false,
          dangersItem: {},
          dangersList: socialResource.dangersList,
          detailActiveKey: 'tab1',
          newKey: new Date().getTime() + '',
        },
      });
    },
  };

  Array.prototype.removeServer = function(dx) {
    if (isNaN(dx) || dx > this.length) {
      return false;
    }
    for (var i = 0, n = 0; i < this.length; i++) {
      if (this[i] != this[dx]) {
        this[n++] = this[i];
      }
    }
    this.length -= 1;
  };
  function handleCansel() {
    dispatch({
      type: 'socialResource/updateState',
      payload: {
        modalDangersVisible: false,
        activityKey: 'tab1',
      },
    });
  }
  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    {
      title: '名称',
      dataIndex: 'name',
      key: 'name',
      width: 120,
      render: (text, record, value) => (
        <Fragment>
          <a onClick={() => onFindKeyUnit(record.id_)}>{record.name}</a>
        </Fragment>
      ),
    },
    { title: '所属消防战队', dataIndex: 'fireBrigade', key: 'fireBrigade', width: 150 },
    { title: '所属类型', dataIndex: 'typeName', key: 'typeName', width: 150 },
    { title: '消防管理人员', dataIndex: 'admin', key: 'admin', width: 100 },
    { title: '值班室电话', dataIndex: 'tel', key: 'tel', width: 100 },
    { title: '地理位置', dataIndex: 'addr', key: 'addr', width: 150 },
    { title: '供水能力', dataIndex: 'waterSupply', key: 'waterSupply', width: 100 },
    {
      title: '经纬度',
      dataIndex: 'lon',
      width: 130,
      render: (text, record) => {
        if(record.lon && record.lat){
          return record.lon+ ','+ record.lat;
        }
        return '';
      },
    },
    {
      title: '操作',
      dataIndex: 'operation',
      key: 'operation',
      width: 150,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_)}>编辑</a>
          <Divider type="vertical" />
          <Popconfirm title="是否要删除此行？" onConfirm={deleteHandler.bind(this, record.id)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];
  const onFindKeyUnit = id => {
    dispatch({
      type: 'socialResource/findKeyUnit',
      payload: id,
    });
  };

  function onShowDangers(id) {
    dispatch({
      type: 'socialResource/onShowDangers',
      payload: id,
    });
  }

  function onShowPlans(id) {
    dispatch({
      type: 'socialResource/getPlans',
      payload: id,
    });
  }

  const onUploadAccident = id => {
    dispatch({
      type: 'socialResource/getUnitAccident',
      payload: id,
    });
  };

  const onUpdate = id => {
    dispatch({
      type: 'socialResource/info',
      payload: id,
    });
  };

  const deleteHandler = id => {
    dispatch({
      type: 'socialResource/remove',
      payload: id,
      search: { pageNum: socialResource.current, pageSize: socialResource.pageSize, ...getFieldsValue() },
    });
  };

  const pagination = {
    current: socialResource.current,
    pageSize: socialResource.pageSize,
    total: socialResource.total,
    showSizeChanger: true,
    showTotal: total => '共' + socialResource.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'socialResource/qryListByParams',
        payload: { pageNum: current, pageSize: size, ...getFieldsValue() },
      });
    },
    onChange(current) {
      dispatch({
        type: 'socialResource/qryListByParams',
        payload: { pageNum: current, pageSize: socialResource.pageSize, ...getFieldsValue() },
      });
    },
  };

  const handleSearch = () => {
    dispatch({
      type: 'socialResource/qryListByParams',
      payload: { pageNum: 1, pageSize: socialResource.pageSize, ...getFieldsValue() },
    });
  };

  const handleFormReset = () => {
    resetFields();
    dispatch({
      type: 'socialResource/qryListByParams',
      payload: { pageNum: 1, pageSize: socialResource.pageSize },
    });
  };

  const onAdd = () => {
    dispatch({
      type: 'socialResource/showCreateModal',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
        newKey: new Date().getTime() + '',
      },
    });
  };

  const loop = data =>
    data.map(d => {
      let children = null;
      if (d.children != null && d.children.length > 0) {
        children = loop(d.children);
      }
      return { title: d.name, value: d.id_, key: d.id_, children };
    });

  function handleTableChange(pagination, filters, sorter) {
    let isChinese = ['roleName'].indexOf(sorter.field) >= 0;
    dispatch({
      type: 'socialResource/qryListByParams',
      payload: {
        pageNum: pagination.current,
        pageSize: pagination.pageSize,
        ...getFieldsValue(),
        orderBy: sorter.field,
        order: sorter.order,
        isChinese,
      },
    });
  }

  //弹出地图选择
  const LocationModalGen = () => <LocationModal />;
  const DocumentModallGen = () => <DocumentModal {...DocumentModalProps} />;
  const DocumentModalProps = {
    viweSwfPath: socialResource.viweSwfPath,
    visible: socialResource.documnetModalVisible,
    title: '查看文档',
    dispatch: dispatch,
  };

  function downloadTemplate() {
    window.open(baseUrl + '/execl/downLoadTemplate.jhtml?type=socialResourceTemplate');
  }

  function initLoopMsg(values) {
    return values != null && values.length > 0
      ? values.map((k, index) => {
          return <div key={index}>{k}</div>;
        })
      : null;
  }

  const props = {
    name: 'file',
    action: baseUrl + '/socialResource/excel/importExpert.jhtml',
    beforeUpload(file) {
      // const isJPG = file.type === 'application/vnd.ms-excel' || file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
      // if (!isJPG) {
      //   message.error('只能上传EXCEL文件！');
      // }
      // return isJPG;
    },
    onChange(info) {
      let fileList = info.fileList;
      if (info.file.status === 'done') {
        if (info.file.response.httpCode == 200) {
          if (info.file.response.errorCode == 1) {
            //弹出确认取消框
          } else if (info.file.response.errorCode == 2) {
            if (info.file.response.data && info.file.response.data.length > 0) {
              Modal.error({
                title: '导入提示',
                content: initLoopMsg(info.file.response.data),
              });
            }
          } else {
            message.success(`${info.file.name} 上传成功`);
            dispatch({
              type: 'socialResource/qryListByParams',
              payload: { name: '' },
            });
          }
        } else {
          Modal.error({
            title: '导入提示',
            content: info.file.response.msg,
          });
        }
        fileList = fileList.filter(file => {
          return file.response && file.response.fid;
        });
        //只保留最后一条记录
        fileList = fileList.slice(-1);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`);
      }
      let fildsValue = {};
      fildsValue['file'] = fileList.map(file => {
        return file;
      });
      setFieldsValue(fildsValue);
    },
  };

  const AddDangersModalProps = {
    visible: socialResource.addDangersVisible,
    existDangersList: socialResource.existDangersList,
    allDangersList: socialResource.allDangersList,
    currentDangers: socialResource.currentDangers,
    pageSizeDangers: socialResource.pageSizeDangers,
    totalDangers: socialResource.totalDangers,
    maskClosable: false,
    dispatch: dispatch,
    title: '新增明细',
    wrapClassName: 'vertical-center-modal',
    onCancel() {
      dispatch({
        type: 'socialResource/hideSecondModal',
      });
    },
  };

  return (
    <PageHeaderLayout>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={5} sm={5}>
                  <FormItem label="名称">
                    {getFieldDecorator('name')(<Input placeholder="请输入名称" />)}
                  </FormItem>
                </Col>
                <Col md={6} sm={6}>
                  <FormItem label="所属消防队">
                    {getFieldDecorator('fireBrigade')(<Input placeholder="请输入消防队名称" />)}
                  </FormItem>
                </Col>
                <Col md={5} sm={4}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                      重置
                    </Button>

                    <Button
                      style={{ marginLeft: 80 }}
                      type="primary"
                      icon="download"
                      onClick={downloadTemplate}
                    >
                      下载模板
                    </Button>
                    <Upload
                      {...getFieldProps(
                        'file',
                        {
                          validate: [
                            {
                              rules: [{ type: 'array', required: true, message: '请添加数据文件' }],
                              trigger: 'onBlur',
                            },
                          ],
                        },
                        { valuePropName: 'fileIds' }
                      )}
                      {...props}
                      fileList={getFieldValue('file')}
                    >
                      <Button style={{ marginLeft: 8 }} type="primary" icon="upload">
                        导入
                      </Button>
                    </Upload>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={onAdd}>
              新建
            </Button>
          </div>
          <Table
            loading={loading}
            rowKey={record => record.id}
            dataSource={socialResource.list}
            columns={columns}
            pagination={pagination}
            onChange={handleTableChange}
          />
        </div>
      </Card>
      <AddEditModal {...AddEditModalProps} />
      <LocationModalGen />
      <ShowFileModal {...ShowEditModalProps} />
    </PageHeaderLayout>
  );
}

function mapStateToProps(state) {
  return {
    socialResource: state.socialResource,
    loading: state.loading.models.socialResource,
  };
}

SocialResource = Form.create()(SocialResource);

export default connect(mapStateToProps)(SocialResource);
