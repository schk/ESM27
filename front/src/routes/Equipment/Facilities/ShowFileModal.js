import React from 'react';
import { Form, Input, Modal, Button, Radio, Row, Col, DatePicker } from 'antd';
import { connect } from 'dva';
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
import moment from 'moment';

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 17,
  },
};

let AddEditModal = ({ facilities, item = {}, form, dispatch }) => {
  const { getFieldDecorator, resetFields } = form;

  const modalOpts = {
    title: '查看消防设施详情',
    visible: facilities.findModalVisible,
    maskClosable: false,
    width: 900,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
    ],
  };
  if (!facilities.findModalVisible) {
    resetFields();
  }
  function handleCansel() {
    dispatch({
      type: 'facilities/updateState',
      payload: {
        findModalVisible: false,
        locationItem: {},
      },
    });
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={12}>
            <FormItem label="消防设施名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [{ required: true, message: '消防设施名称' }],
              })(<Input type="text" readOnly placeholder="消防设施名称" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="消防设施编码:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('code', {
                initialValue: item.code,
                rules: [{ required: true, message: '消防设施编码' }],
              })(<Input type="text" readOnly placeholder="消防设施编码" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="所属重点单位:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('keyUnitName', {
                initialValue: item.keyUnitName == null ? facilities.keyUnitId : item.keyUnitName,
                rules: [{ required: true, message: '所属重点单位' }],
              })(<Input type="text" readOnly placeholder="" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="所属类型:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('equipmentTypeName', {
                initialValue: item.equipmentTypeName == null ? undefined : item.equipmentTypeName,
                rules: [{ required: true, message: '所属类型' }],
              })(<Input type="text" readOnly placeholder="" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="消防设施半径:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('fireRadius', {
                initialValue: item.fireRadius,
                rules: [{ required: true, message: '消防设施半径' }],
              })(<Input type="text" readOnly placeholder="消防设施半径" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem required label="储量:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('reserves', {
                initialValue: item.reserves,
              })(<Input type="text" readOnly placeholder="请输入消防设施储量" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem required label="流量:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('flow', {
                initialValue: item.flow,
              })(<Input type="text" readOnly placeholder="请输入消防设施流量" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="管网:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('pipeNetwork', {
                initialValue: item.pipeNetwork,
              })(<Input type="text" readOnly placeholder="请输入消防设施管网" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="检查情况:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('checkDesc', {
                initialValue: item.checkDesc,
              })(<Input type="text" readOnly placeholder="请输入检查情况" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem {...formItemLayout} label="检查时间">
              {getFieldDecorator('checkTime', {
                initialValue: item.checkTime ? moment(item.checkTime) : undefined,
              })(
                <DatePicker
                  readOnly
                  style={{ width: '100%' }}
                  placeholder="请选择检查时间"
                  format={'YYYY-MM-DD'}
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem {...formItemLayout} label="是否好用">
              {getFieldDecorator('goodUse', {
                initialValue: item.goodUse == null ? 1 : item.goodUse == true ? 1 : 0,
              })(
                <RadioGroup readOnly>
                  <Radio value={1}>好用</Radio>
                  <Radio value={0}>不好用</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem required label="经纬度:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('lonLat', {
                initialValue: facilities.locationItem.lonLat
                  ? facilities.locationItem.lonLat + ''
                  : undefined,
              })(<Input type="text" readOnly placeholder="经纬度" />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { facilities: state.facilities };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
