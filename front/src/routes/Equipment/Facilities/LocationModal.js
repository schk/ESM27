import React from 'react';
import { Form, Input, Modal, Button, Row, Col } from 'antd';
import { connect } from 'dva';
import { baseFileUrl,baseUrl } from '../../../config/system';
var this_;

class LocationModal extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    var x = 104.094752,
      y = 30.667451;
    if (this.props.facilities.locationItem.lonLat) {
      x = parseFloat(this.props.facilities.locationItem.lonLat.split(',')[0]);
      y = parseFloat(this.props.facilities.locationItem.lonLat.split(',')[1]);
    }
    setTimeout(function() {
      var app = {};
      /**
       * @constructor
       * @extends {module:ol/interaction/Pointer}
       */
      app.Drag = function() {
        ol.interaction.Pointer.call(this, {
          handleDownEvent: app.Drag.prototype.handleDownEvent,
          handleDragEvent: app.Drag.prototype.handleDragEvent,
          handleMoveEvent: app.Drag.prototype.handleMoveEvent,
          handleUpEvent: app.Drag.prototype.handleUpEvent,
        });

        /**
         * @type {module:ol~Pixel}
         * @private
         */
        this.coordinate_ = null;

        /**
         * @type {string|undefined}
         * @private
         */
        this.cursor_ = 'pointer';

        /**
         * @type {module:ol/Feature~Feature}
         * @private
         */
        this.feature_ = null;

        /**
         * @type {string|undefined}
         * @private
         */
        this.previousCursor_ = undefined;
      };
      ol.inherits(app.Drag, ol.interaction.Pointer);

      /**
       * @param {module:ol/MapBrowserEvent~MapBrowserEvent} evt Map browser event.
       * @return {boolean} `true` to start the drag sequence.
       */
      app.Drag.prototype.handleDownEvent = function(evt) {
        var map = evt.map;

        var feature = map.forEachFeatureAtPixel(evt.pixel, function(feature) {
          return feature;
        });

        if (feature) {
          this.coordinate_ = evt.coordinate;
          this.feature_ = feature;
        }

        return !!feature;
      };

      /**
       * @param {module:ol/MapBrowserEvent~MapBrowserEvent} evt Map browser event.
       */
      app.Drag.prototype.handleDragEvent = function(evt) {
        var deltaX = evt.coordinate[0] - this.coordinate_[0];
        var deltaY = evt.coordinate[1] - this.coordinate_[1];

        var geometry = this.feature_.getGeometry();
        geometry.translate(deltaX, deltaY);

        this.coordinate_[0] = evt.coordinate[0];
        this.coordinate_[1] = evt.coordinate[1];
        var lonlat = map.getCoordinateFromPixel(evt.pixel);
        var lon = ol.proj.transform(lonlat, 'EPSG:3857', 'EPSG:4326')[0].toFixed(6);
        var lat = ol.proj.transform(lonlat, 'EPSG:3857', 'EPSG:4326')[1].toFixed(6);
        var cLonLat = lon + ',' + lat;
        this_.props.form.setFieldsValue({ lonLat: cLonLat });
      };

      /**
       * @param {module:ol/MapBrowserEvent~MapBrowserEvent} evt Event.
       */
      app.Drag.prototype.handleMoveEvent = function(evt) {
        if (this.cursor_) {
          var map = evt.map;
          var feature = map.forEachFeatureAtPixel(evt.pixel, function(feature) {
            return feature;
          });
          var element = evt.map.getTargetElement();
          if (feature) {
            if (element.style.cursor != this.cursor_) {
              this.previousCursor_ = element.style.cursor;
              element.style.cursor = this.cursor_;
            }
          } else if (this.previousCursor_ !== undefined) {
            element.style.cursor = this.previousCursor_;
            this.previousCursor_ = undefined;
          }
        }
      };

      /**
       * @return {boolean} `false` to stop the drag sequence.
       */
      app.Drag.prototype.handleUpEvent = function() {
        this.coordinate_ = null;
        this.feature_ = null;
        return false;
      };

      var source = new ol.source.Vector({
        features: [],
      });
      var layer = new ol.layer.Vector({
        source: source,
        style: new ol.style.Style({
          image: new ol.style.Icon({
            src: baseFileUrl + '/mapIcon/mapFixedIcon/zhongdian.png',
            anchor: [0.5, 0.5],
          }),
        }),
      });

      map = new ol.Map({
        target: 'map',
        layers: [
          new ol.layer.Tile({
            source: new ol.source.XYZ({
              url:
                'https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}',
            }),
          }),
          layer,
        ],
        view: new ol.View({
          // 设置成都为地图中心
          center: ol.proj.transform([104.094752, 30.667451], 'EPSG:4326', 'EPSG:3857'),
          zoom: 8,
          minZoom: 3,
          maxZoom: 18,
        }),
        interactions: ol.interaction.defaults().extend([new app.Drag()]),
      });
      var feature = new ol.Feature({
        geometry: new ol.geom.Point(new ol.proj.fromLonLat([x, y])),
      });
      feature.setId('position');
      source.addFeature(feature);
      //定位坐标点
      map.getView().animate({ center: new ol.proj.fromLonLat([x, y]) });
    }, 100);
  }

  render() {
    const {
      getFieldDecorator,
      validateFields,
      getFieldsValue,
      getFieldValue,
      resetFields,
      setFieldsValue,
    } = this.props.form;
    const facilities = this.props.facilities;
    this_ = this;
    var dispatch = this.props.dispatch;

    const modalOpts = {
      title: '获取坐标点',
      visible: facilities.locationModalVisible,
      maskClosable: false,
      width: 1300,
      centered:true,
      onCancel: handleCansel,
      footer: [
        <Button key="back" type="ghost" size="large" onClick={handleCansel}>
          取消
        </Button>,
        <Button
          key="submit"
          type="primary"
          size="large"
          onClick={() => handleOk()}
          loading={facilities.buttomLoading}
        >
          保存
        </Button>,
      ],
    };

    if (!facilities.locationModalVisible) {
      resetFields();
    }

    function handleCansel() {
      dispatch({
        type: 'facilities/updateState',
        payload: {
          locationModalVisible: false,
        },
      });
    }

    function handleOk() {
      validateFields(errors => {
        if (errors) {
          return;
        }
        const data = getFieldsValue();
        dispatch({
          type: `facilities/updateState`,
          payload: {
            // locationItem:{ lonLat:$("#loacltionCoordinate").val(),dtPath:data.dtPath ,},
            locationItem: { lonLat: $('#loacltionCoordinate').val() },
            locationModalVisible: false,
          },
        });
      });
    }

    const loopOption = data =>
      data.map(item => {
        return (
          <Option key={item.dtPath}>
            <img style={{ width: '20px', height: '20px' }} src={baseFileUrl + item.dtPath} alt="" />
            {item.name}
          </Option>
        );
      });

      //监听子窗口信息
    window.addEventListener('message', function(event) {
      var origin = event.origin;
      if (origin !== baseUrl) {
        return;
      }
      $('#loacltionCoordinate').val(event.data);
    });

    return (
      <Modal {...modalOpts}>
        <Form layout="horizontal">
          <Row>
            <Col span={12}>
              <div label="">
                <div
                  style={{
                    position: 'relative',
                    lineHeight: '30px',
                    display: 'block',
                    float: 'left',
                    marginRight: '5px',
                  }}
                >
                  经纬度:
                </div>
                <input
                  type="text"
                  readOnly
                  id="loacltionCoordinate"
                  value={this.props.facilities.locationItem.lonLat}
                  style={{
                    height: '32px',
                    outline: 'none',
                    WebkitUserSelect: 'none',
                    MozUserSelect: 'none',
                    MsUserSelect: 'none',
                    userSelect: 'none',
                    WebkitBoxSizing: 'border-box',
                    boxSizing: 'border-box',
                    display: 'block',
                    backgroundColor: '#fff',
                    borderRadius: '4px',
                    border: '1px solid #d9d9d9',
                    borderTopWidth: ' 1.02px',
                    WebkitTransition: 'all 0.3s cubic-bezier(0.645, 0.045, 0.355, 1)',
                    transition: 'all 0.3s cubic-bezier(0.645, 0.045, 0.355, 1)',
                    padding: '0 10px',
                  }}
                />
              </div>
            </Col>
          </Row>
          {/* <div id="map" style={{ width: '890px', height: '600px',marginLeft:'30px' }} /> */}

          <iframe
            id="lacaltionMap"
            name="lacaltionMap"
            src={              baseUrl + '/api/gis/getMapLocation.jhtml?loacltionCoordinate=' + this.props.facilities.locationItem.lonLat
          }
            style={{
              width: '1230px',
              marginTop: '10px',
              marginLeft: '15px',
              height: '510px',
              border: '0',
            }}
          />
        </Form>
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return { facilities: state.facilities };
}

LocationModal = Form.create()(LocationModal);

export default connect(mapStateToProps)(LocationModal);
