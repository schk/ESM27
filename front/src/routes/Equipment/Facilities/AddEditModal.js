import React from 'react';
import { Form, Input, Modal, Button, Radio, Row, Col, Select, DatePicker, InputNumber,message, } from 'antd';
import { connect } from 'dva';
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const Option = Select.Option;
import moment from 'moment';

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 17,
  },
};

let AddEditModal = ({ facilities, item = {}, form, dispatch }) => {
  const { getFieldDecorator, validateFields, getFieldsValue, resetFields } = form;

  const modalOpts = {
    title: facilities.modalType == 'create' ? '新建消防设施' : '修改消防设施',
    visible: facilities.modalVisible,
    maskClosable: false,
    width: 900,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={facilities.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!facilities.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'facilities/updateState',
      payload: {
        modalVisible: false,
        locationItem: {},
      },
    });
  }

  const onGetLocation = () => {
    const data = getFieldsValue();
    dispatch({
      type: 'facilities/updateState',
      payload: {
        locationModalVisible: true,
        newKey1: new Date().getTime() + '',
        locationItem: { lonLat: data.lonLat },
      },
    });
  };

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      var lonLat = getFieldsValue(["lonLat"]);
          if(lonLat.lonLat==undefined){
          message.error("坐标不能为空");
          return;
      }
      const data = getFieldsValue();
      data.id = facilities.modalType === 'create' ? '' : item.id;
      dispatch({
        type: `facilities/${facilities.modalType}`,
        payload: data,
        search: facilities.selectObj,
      });
    });
  }

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  function checkFireRadius(rule, value, callback) {
    if (!value && $.trim(value) == '') {
      callback('请输入消防设施半径');
    } else if (value > 999) {
      callback('消防设施半径不能超过999');
    } else {
      callback();
    }
  }

  function checkFlow(rule, value, callback) {
    if (!value && $.trim(value) == '') {
      callback('请输入消防设施流量');
    } else if (value > 999) {
      callback('消防设施流量不能超过999');
    } else {
      callback();
    }
  }

  function checkReserves(rule, value, callback) {
    if (!value && $.trim(value) == '') {
      callback('请输入消防设施储量');
    } else if (value > 9999999999) {
      callback('消防设施储量不能超过9999999999');
    } else {
      callback();
    }
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={12}>
            <FormItem label="消防设施名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '消防设施名称' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="消防设施名称" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="消防设施编码:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('code', {
                initialValue: item.code,
                rules: [
                  { required: true, message: '请输入消防设施编码' },
                  { max: 20, message: '最长不超过20个字' },
                ],
              })(<Input type="text" placeholder="请输入消防设施编码" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="所属重点单位:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('keyUnitId', {
                initialValue: item.keyUnitId == null ? facilities.keyUnitId : item.keyUnitId,
                rules: [{ required: true, message: '请选择所属重点单位' }],
              })(
                <Select showSearch optionFilterProp="children" placeholder="请选择所属重点单位">
                  {loopOption(facilities.keyUnitList)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="所属类型:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('typeId', {
                initialValue: item.typeId == null ? undefined : item.typeId,
                rules: [{ required: true, message: '请选择所属类型' }],
              })(
                <Select placeholder="所属类型">{loopOption(facilities.equipmentTypeList)}</Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem required label="消防设施半径:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('fireRadius', {
                initialValue: item.fireRadius,
                rules: [
                  {
                    validator: checkFireRadius,
                  },
                ],
              })(<InputNumber style={{ width: '100%' }} placeholder="请输入消防设施半径" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem required label="储量:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('reserves', {
                initialValue: item.reserves,
                rules: [
                  {
                    validator: checkReserves,
                  },
                ],
              })(<InputNumber style={{ width: '100%' }} placeholder="请输入消防设施储量" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem required label="流量:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('flow', {
                initialValue: item.flow,
                rules: [
                  {
                    validator: checkFlow,
                  },
                ],
              })(<InputNumber style={{ width: '100%' }} placeholder="请输入消防设施流量" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="管网:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('pipeNetwork', {
                initialValue: item.pipeNetwork,
                rules: [{ max: 50, message: '最长不超过50个字' }],
              })(<Input type="text" placeholder="请输入消防设施管网" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="检查情况:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('checkDesc', {
                initialValue: item.checkDesc,
                rules: [{ max: 50, message: '最长不超过50个字' }],
              })(<Input placeholder="请输入检查情况" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem {...formItemLayout} label="检查时间">
              {getFieldDecorator('checkTime', {
                initialValue: item.checkTime ? moment(item.checkTime) : undefined,
              })(
                <DatePicker
                  style={{ width: '100%' }}
                  placeholder="请选择检查时间"
                  format={'YYYY-MM-DD'}
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem {...formItemLayout} label="是否好用">
              {getFieldDecorator('goodUse', {
                initialValue: item.goodUse == null ? 1 : item.goodUse == true ? 1 : 0,
              })(
                <RadioGroup>
                  <Radio value={1}>好用</Radio>
                  <Radio value={0}>不好用</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="获取坐标点:" hasFeedback {...formItemLayout}>
              <div
                style={{
                  marginRight: '10px',
                  marginTop: '5px',
                  width: '28px',
                  height: '28px',
                  background: 'url(images/iconAdd.png)',
                  display: 'inlineBlock',
                  float: 'left',
                  cursor: 'pointer',
                }}
                onClick={onGetLocation}
              />
              {getFieldDecorator('lonLat', {
                initialValue: facilities.locationItem.lonLat
                  ? facilities.locationItem.lonLat + ''
                  : undefined,
              //  rules: [{ required: true, message: '请选择位置坐标' }],
              })(
                <Input type="text" readOnly style={{ width: '87%' }} placeholder="请选择位置坐标" />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row />
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { facilities: state.facilities };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
