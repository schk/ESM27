import React, { Fragment } from 'react';
import {
  Table,
  Form,
  Button,
  Popconfirm,
  Card,
  Input,
  Divider,
  Row,
  Col,
  Select,
  Tag,
  Tree,
  Icon,
  Upload,
  message,
  Modal,
} from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import LocationModal from './LocationModal';
import ShowFileModal from './ShowFileModal';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import { baseUrl } from '../../../config/system';

const Option = Select.Option;
const TreeNode = Tree.TreeNode;
const confirm = Modal.confirm;

function Facilities({ location, facilities, form, dispatch }) {
  const {
    getFieldDecorator,
    getFieldsValue,
    getFieldValue,
    getFieldProps,
    setFieldsValue,
    resetFields,
  } = form;
  const AddEditModalProps = {
    item: facilities.item,
  };
  const LocationModalProps = {};
  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    {
      title: '消防设施名称',
      dataIndex: 'name',
      key: 'name',
      width: 100,
      render: (text, record, value) => (
        <Fragment>
          <a onClick={() => onFindInfo(record.id_)}>{record.name}</a>
        </Fragment>
      ),
    },
    { title: '设施编码', dataIndex: 'code', key: 'code', width: 100 },
    { title: '设施半径', dataIndex: 'fireRadius', key: 'fireRadius', width: 80 },
    { title: '设施储量', dataIndex: 'reserves', key: 'reserves', width: 80 },
    { title: '设施流量', dataIndex: 'flow', key: 'flow', width: 80 },
    { title: '所属类别', dataIndex: 'equipmentTypeName', key: 'equipmentTypeName', width: 100 },
    { title: '所属单位', dataIndex: 'keyUnitName', key: 'keyUnitName ', width: 150 },
    {
      title: '是否好用',
      dataIndex: 'goodUse',
      key: 'goodUse',
      width: 60,
      render: (text, record) => {
        if (record.goodUse == 1) {
          return <Tag color="#2db7f5">好用</Tag>;
        }
        if (record.goodUse == 0) {
          return <Tag color="#f50">不好用</Tag>;
        }
      },
    },
    {
      title: '经纬度',
      dataIndex: 'lon',
      key: 'lon',
      width: 130,
      render: (text, record) => {
        if(record.lon && record.lat){
          return record.lon + ',' + record.lat;
        }
        return '';
      },
    },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 100,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_)}>修改</a>
          <Divider type="vertical" />
          {/* <a onClick={() => onFindInfo(record.id_)}>查看</a>
          <Divider type="vertical" /> */}
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];
  const onFindInfo = id => {
    dispatch({
      type: 'facilities/findInfo',
      payload: id,
    });
  };

  function onUpdate(id) {
    dispatch({
      type: 'facilities/info',
      payload: id,
    });
  }

  function onAdd() {
    dispatch({
      type: 'facilities/updateState',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'facilities/del',
      payload: id,
      search: {
        keyUnitId: facilities.keyUnitId,
        pageNum: facilities.current,
        pageSize: facilities.pageSize,
        ...getFieldsValue(),
      },
    });
  }

  function handleSearch() {
    dispatch({
      type: 'facilities/qryListByParams',
      payload: {
        pageNum: 1,
        pageSize: facilities.pageSize,
        ...getFieldsValue(),
        keyUnitId: facilities.keyUnitId,
      },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'facilities/qryListByParams',
      payload: { facilities: 1, pageSize: facilities.pageSize, keyUnitId: facilities.keyUnitId },
    });
  }

  function callback(key) {
    console.log(key);
  }

  const loop = data =>
    data.map(item => {
      return (
        <TreeNode
          key={item.id_}
          title={item.name}
          icon={
            <Icon
              style={{
                width: '10px',
                height: '10px',
                background: 'url(images/liBgIcon.png)',
                display: 'inlineBlock',
                verticalAlign: '1px',
              }}
            />
          }
        />
      );
    });

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  const onSelect = info => {
    if (info.length > 0) {
      dispatch({
        type: 'facilities/qryListByParams',
        payload: { ...getFieldsValue(), ...{ keyUnitId: info[0] } },
      });
    } else {
      dispatch({
        type: 'facilities/qryListByParams',
        payload: { ...getFieldsValue(), ...{ keyUnitId: undefined } },
      });
    }
  };

  const pagination = {
    current: facilities.current,
    pageSize: facilities.pageSize,
    total: facilities.total,
    showSizeChanger: true,
    showTotal: total => '共' + facilities.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'facilities/qryListByParams',
        payload: {
          pageNum: current,
          pageSize: size,
          ...getFieldsValue(),
          keyUnitId: facilities.keyUnitId,
        },
      });
    },
    onChange(current) {
      dispatch({
        type: 'facilities/qryListByParams',
        payload: {
          pageNum: current,
          pageSize: facilities.pageSize,
          ...getFieldsValue(),
          keyUnitId: facilities.keyUnitId,
        },
      });
    },
  };

  function handleTableChange(pagination, filters, sorter) {
    let isChinese = ['account', 'name'].indexOf(sorter.field) >= 0;
    dispatch({
      type: 'facilities/qryListByParams',
      payload: {
        pageNum: pagination.current,
        pageSize: pagination.pageSize,
        ...getFieldsValue(),
        orderBy: sorter.field,
        order: sorter.order,
        isChinese,
      },
    });
  }

  function downloadTemplate() {
    window.open(baseUrl + '/execl/downLoadTemplate.jhtml?type=facilitiesTemplate');
  }

  function initLoopMsg(values) {
    return values != null && values.length > 0
      ? values.map((k, index) => {
          return <div key={index}>{k}</div>;
        })
      : null;
  }

  const props = {
    name: 'file',
    action: baseUrl + '/facilities/excel/importExcel.jhtml',
    beforeUpload(file) {
      // const isJPG = file.type === 'application/vnd.ms-excel' || file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
      // if (!isJPG) {
      //   message.error('只能上传EXCEL文件！');
      // }
      // return isJPG;
    },
    onChange(info) {
      let fileList = info.fileList;
      if (info.file.status === 'done') {
        if (info.file.response.httpCode == 200) {
          if (info.file.response.errorCode == 1) {
            //弹出确认取消框
            confirm({
              title: '信息提示框',
              content: initLoopMsg(info.file.response.data),
              okText: '继续导入',
              cancelText: '取消导入',
              onOk() {
                dispatch({
                  type: 'facilities/importExcelConfirm',
                  payload: info.file.response.times,
                });
              },
              onCancel() {
                dispatch({
                  type: 'facilities/delExcelTemp',
                  payload: info.file.response.times,
                });
              },
            });
          } else if (info.file.response.errorCode == 2) {
            if (info.file.response.data && info.file.response.data.length > 0) {
              Modal.error({
                title: '导入提示',
                content: initLoopMsg(info.file.response.data),
              });
            }
          } else {
            message.success(`${info.file.name} 上传成功`);
            dispatch({
              type: 'facilities/qryListByParams',
              payload: { keyUnitId: undefined },
            });
          }
        } else {
          Modal.error({
            title: '导入提示',
            content: info.file.response.msg,
          });
        }
        fileList = fileList.filter(file => {
          return file.response && file.response.fid;
        });
        //只保留最后一条记录
        fileList = fileList.slice(-1);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`);
      }
      let fildsValue = {};
      fildsValue['file'] = fileList.map(file => {
        return file;
      });
      setFieldsValue(fildsValue);
    },
  };

  const LocationModalGen = () => <LocationModal {...LocationModalProps} />;
  return (
    <PageHeaderLayout>
      <div style={{ backgroundColor: '#fff', position: 'relative', overflow: 'hidden' }}>
        <div
          style={{
            width: '250px',
            position: 'absolute',
            left: '0',
            right: '0',
            bottom: '0',
            top: '0',
          }}
        >
          <div
            style={{
              width: '100%',
              height: '40px',
              backgroundColor: '#232f40',
              lineHeight: '40px',
              color: '#fff',
              paddingLeft: '20px',
              fontSize: '14px',
              fontWeight: '400',
            }}
          >
            重点单位
          </div>
          <div
            style={{
              position: 'absolute',
              left: '0',
              top: '40px',
              bottom: '0',
              right: '0',
              overflow: 'auto',
            }}
          >
            <Tree defaultExpandedKeys={['1']} onSelect={onSelect} showIcon>
              {facilities.keyUnitList && loop(facilities.keyUnitList)}
            </Tree>
          </div>
        </div>
        <div style={{ marginLeft: '250px', paddingTop: '0px', borderLeft: '1px solid #e8e8e8' }}>
          <Card bordered={false} style={{ minHeight: '600px' }}>
            <div className={styles.tableList}>
              <div className={styles.tableListForm}>
                <Form layout="inline">
                  <Row gutter={{ md: 8, lg: 24, xl: 24 }}>
                    <Col span={6} sm={6}>
                      <Form.Item label="消防设施名称：">
                        {getFieldDecorator('name')(<Input placeholder="输入消防设施名称查找" />)}
                      </Form.Item>
                    </Col>
                    <Col span={7} sm={6}>
                      <Form.Item label="所属类型：">
                        {getFieldDecorator('typeId')(
                          <Select allowClear placeholder="请选择所属类型">
                            <Option value="">全部</Option>
                            {loopOption(facilities.equipmentTypeList)}
                          </Select>
                        )}
                      </Form.Item>
                    </Col>

                    <Col span={5} sm={5}>
                      <Form.Item label="编码：">
                        {getFieldDecorator('code')(<Input placeholder="输入编码查找" />)}
                      </Form.Item>
                    </Col>
                    <Col md={5} sm={4}>
                      <span className={styles.submitButtons}>
                        <Button type="primary" htmlType="submit" onClick={handleSearch}>
                          查询
                        </Button>
                        <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                          重置
                        </Button>
                      </span>
                    </Col>
                  </Row>
                </Form>
              </div>
              <div className={styles.submitButtons}>
                <Button icon="plus" type="primary" onClick={onAdd}>
                  新增
                </Button>

                <Button
                  style={{ marginLeft: 80 }}
                  type="primary"
                  icon="download"
                  onClick={downloadTemplate}
                >
                  下载模板
                </Button>
                <Upload
                  {...getFieldProps(
                    'file',
                    {
                      validate: [
                        {
                          rules: [{ type: 'array', required: true, message: '请添加数据文件' }],
                          trigger: 'onBlur',
                        },
                      ],
                    },
                    { valuePropName: 'fileIds' }
                  )}
                  {...props}
                  fileList={getFieldValue('file')}
                >
                  <Button style={{ marginLeft: 8 }} type="primary" icon="upload">
                    导入
                  </Button>
                </Upload>
              </div>
              <div style={{ width: '100%' }}>
                <Table
                  columns={columns}
                  dataSource={facilities.list}
                  style={{ marginTop: 10 }}
                  pagination={pagination}
                  loading={facilities.loading}
                  rowKey={record => record.id_}
                />
              </div>
            </div>
          </Card>
        </div>
      </div>
      <AddEditModal {...AddEditModalProps} />
      <ShowFileModal {...AddEditModalProps} />
      <LocationModalGen />
    </PageHeaderLayout>
  );
}

function mapStateToProps(state) {
  return {
    facilities: state.facilities,
    loading: state.loading.models.facilities,
  };
}

Facilities = Form.create()(Facilities);

export default connect(mapStateToProps)(Facilities);
