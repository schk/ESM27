import React from 'react';
import { Form, Input, Modal, Button, Radio, Row, Icon, Col, Select, Upload, message } from 'antd';
import { connect } from 'dva';
import { baseUrl, baseFileUrl } from '../../../config/system';
const FormItem = Form.Item;
const Option = Select.Option;
const { TextArea } = Input;

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
};

let AddEditModal = ({ keyParts, item = {}, form, dispatch }) => {
  console.log(keyParts);
  
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
    getFieldProps,
  } = form;

  const modalOpts = {
    title: keyParts.modalType == 'create' ? '新增重点部位' : '修改消重点部位',
    visible: keyParts.modalVisible,
    maskClosable: false,
    width: 600,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={keyParts.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!keyParts.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'keyParts/updateState',
      payload: {
        modalVisible: false,
        path: '',
        processFlow: '',
      },
    });
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = getFieldsValue();
      data.id = keyParts.modalType === 'create' ? '' : item.id;
      dispatch({
        type: `keyParts/${keyParts.modalType}`,
        payload: data,
        search: keyParts.selectObj,
      });
    });
  }

  const loopOption = data => 
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  //图片上传
  const pictureUploadProps = {
    action: baseUrl + '/common/uploadImg.jhtml',
    withCredentials: true,
    listType: 'picture',
    onChange: pictureHandleChange,
    onRemove: pictureHandleRemove,
  };

  function pictureHandleRemove() {
    dispatch({
      type: 'keyUnit/updateState',
      payload: {
        path: '',
      },
    });
  }
  function pictureHandleChange(info) {
    let fileList = info.fileList;
    if (info.file.status === 'done') {
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
        dispatch({
          type: 'keyParts/updateState',
          payload: {
            path: info.file.response.data.fid,
          },
        });
      } else {
        message.error(info.file.response.msg);
      }
      fileList = fileList.filter(file => {
        return true;
      });
      //只保留最后一条记录
      fileList = fileList.slice(-1);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }
  }

  //工艺流程图上传
  const processUploadProps = {
    action: baseUrl + '/common/uploadImg.jhtml',
    withCredentials: true,
    listType: 'picture',
    onChange: processHandleChange,
    onRemove: processHandleRemove,
  };

  function processHandleRemove() {
    dispatch({
      type: 'keyUnit/updateState',
      payload: {
        processFlow: '',
      },
    });
  }
  function processHandleChange(info) {
    let fileList = info.fileList;
    if (info.file.status === 'done') {
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
        dispatch({
          type: 'keyParts/updateState',
          payload: {
            processFlow: info.file.response.data.fid,
          },
        });
      } else {
        message.error(info.file.response.msg);
      }
      fileList = fileList.filter(file => {
        return true;
      });
      //只保留最后一条记录
      fileList = fileList.slice(-1);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={24}>
            <FormItem label="重点部位名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '重点部位名称' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请输入重点部位名称" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="所属重点单位:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('keyUnitId', {
                initialValue: item.keyUnitId == null ? keyParts.keyUnitId : item.keyUnitId,
                rules: [{ required: true, message: '请选择所属重点单位' }],
              })(
                <Select showSearch optionFilterProp="children" placeholder="请选择所属重点单位">
                  {loopOption(keyParts.keyUnitList)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem {...formItemLayout} label="平面图">
              <Input type="hidden" {...getFieldProps('path', { initialValue: keyParts.path })} />
              <Upload style={{ width: '300px' }} showUploadList={false} {...pictureUploadProps}>
                <Button>
                  <Icon type="upload" />上传平面图
                </Button>
              </Upload>
              {keyParts.path ? (
                <div
                  style={{
                    border: '1px solid #d9d9d9',
                    height: '66px',
                    position: 'relative',
                    clear: 'both',
                    overflow: 'hidden',
                    borderRadius: '4px',
                  }}
                >
                  <div
                    style={{
                      width: '60px',
                      height: '60px',
                      textAlign: 'center',
                      border: '1px solid #d9d9d9',
                      borderRadius: '4px',
                    }}
                  >
                    <img
                      style={{ width: '60px', height: '60px' }}
                      src={baseFileUrl + keyParts.path}
                      alt=""
                    />
                  </div>
                </div>
              ) : (
                ''
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem {...formItemLayout} label="工艺流程图">
              <Input
                type="hidden"
                {...getFieldProps('processFlow', { initialValue: keyParts.processFlow })}
              />
              <Upload style={{ width: '300px' }} showUploadList={false} {...processUploadProps}>
                <Button>
                  <Icon type="upload" />上传工艺流程图
                </Button>
              </Upload>
              {keyParts.processFlow ? (
                <div
                  style={{
                    border: '1px solid #d9d9d9',
                    height: '66px',
                    position: 'relative',
                    clear: 'both',
                    overflow: 'hidden',
                    borderRadius: '4px',
                  }}
                >
                  <div
                    style={{
                      width: '60px',
                      height: '60px',
                      textAlign: 'center',
                      border: '1px solid #d9d9d9',
                      borderRadius: '4px',
                    }}
                  >
                    <img
                      style={{ width: '60px', height: '60px' }}
                      src={baseFileUrl + keyParts.processFlow}
                      alt=""
                    />
                  </div>
                </div>
              ) : (
                ''
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="描述:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('remark', {
                initialValue: item.remark,
                rules: [{ max: 100, message: '最长不超过100个字' }],
              })(<TextArea placeholder="请填写重点部位描述" autosize={{ minRows: 3 }} />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { keyParts: state.keyParts };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
