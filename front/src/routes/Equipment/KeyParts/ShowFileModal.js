import React from 'react';
import { Form, Input, Modal, Button, Row, Col } from 'antd';
import { connect } from 'dva';
import { baseUrl, baseFileUrl } from '../../../config/system';
const FormItem = Form.Item;
const { TextArea } = Input;
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

let AddEditModal = ({ keyParts, item = {}, form, dispatch }) => {
  const { getFieldDecorator, resetFields } = form;

  const modalOpts = {
    title: '查看重点部位详情',
    visible: keyParts.findModalVisible,
    maskClosable: false,
    width: 600,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
    ],
  };
  if (!keyParts.findModalVisible) {
    resetFields();
  }
  function handleCansel() {
    dispatch({
      type: 'keyParts/updateState',
      payload: {
        findModalVisible: false,
      },
    });
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={24}>
            <FormItem label="重点部位名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
              })(<Input type="text" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="所属重点单位:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('keyUnitName', {
                initialValue: item.keyUnitName,
              })(<Input type="text" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem {...formItemLayout} label="平面图">
              {keyParts.path ? (
                <div
                  style={{
                    border: '1px solid #d9d9d9',
                    height: '66px',
                    position: 'relative',
                    clear: 'both',
                    overflow: 'hidden',
                    borderRadius: '4px',
                  }}
                >
                  <div
                    style={{
                      width: '60px',
                      height: '60px',
                      textAlign: 'center',
                      border: '1px solid #d9d9d9',
                      borderRadius: '4px',
                    }}
                  >
                    <img
                      style={{ width: '60px', height: '60px' }}
                      src={baseFileUrl + keyParts.path}
                      alt=""
                    />
                  </div>
                </div>
              ) : (
                ''
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem {...formItemLayout} label="工艺流程图">
              {keyParts.processFlow ? (
                <div
                  style={{
                    border: '1px solid #d9d9d9',
                    height: '66px',
                    position: 'relative',
                    clear: 'both',
                    overflow: 'hidden',
                    borderRadius: '4px',
                  }}
                >
                  <div
                    style={{
                      width: '60px',
                      height: '60px',
                      textAlign: 'center',
                      border: '1px solid #d9d9d9',
                      borderRadius: '4px',
                    }}
                  >
                    <img
                      style={{ width: '60px', height: '60px' }}
                      src={baseFileUrl + keyParts.processFlow}
                      alt=""
                    />
                  </div>
                </div>
              ) : (
                ''
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="描述:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('remark', {
                initialValue: item.remark,
              })(<TextArea autosize={{ minRows: 3 }} />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { keyParts: state.keyParts };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
