import React, { Fragment } from 'react';
import { Form, Modal, Button, Row, Col, Table, Tag, Popconfirm, Divider } from 'antd';
import { connect } from 'dva';

let ShowPlanModal = ({ keyParts, loading, item = {}, form, dispatch }) => {
  const { getFieldDecorator, resetFields } = form;

  const modalOpts = {
    title: '查看重点部位预案',
    visible: keyParts.plansModalVisible,
    maskClosable: false,
    width: 1100,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="primary" size="large" onClick={handleCansel}>
        关闭
      </Button>,
    ],
  };
  if (!keyParts.plansModalVisible) {
    resetFields();
  }
  function handleCansel() {
    dispatch({
      type: 'keyParts/updateState',
      payload: {
        plansModalVisible: false,
      },
    });
  }
  const columns = [
    {
      title: '预案名称',
      dataIndex: 'name',
      key: 'name',
      width: 120,
      render: (text, record, value) => (
        <Fragment>
          <a onClick={() => onFindInfo(record.id_)}>{record.name}</a>
        </Fragment>
      ),
    },
    { title: '预案类型', dataIndex: 'typeName', key: 'typeName', width: 100 },
    {
      title: '预案级别',
      dataIndex: 'level',
      key: 'level',
      width: 80,
      render: (value, row, index) => {
        return value == 1 ? 'Ⅰ级' : value == 2 ? 'Ⅱ级' : value == 3 ? 'Ⅲ级' : 'Ⅳ级';
      },
    },
    {
      title: '预案状态',
      dataIndex: 'status',
      key: 'status',
      width: 60,
      render: (value, row, index) => {
        if (row.status == 1) {
          return <Tag color="green">可用</Tag>;
        }
        if (row.status == 2) {
          return <Tag color="red">不可用</Tag>;
        }
      },
    },
    { title: '编写人', dataIndex: 'writer', key: 'writer', width: 100 },
    { title: '编写单位', dataIndex: 'fireBrigadeName', key: 'fireBrigadeName', width: 100 },

    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 100,
      render: (text, record) => (
        <Fragment>
          {/* <a onClick={() => edit(record.id_, record.type)}>修改</a> */}
          <Divider type="vertical" />
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
          {record.swfPath ? (
            <span>
              <Divider type="vertical" />
              <a onClick={() => onShowDocument(record.id, record.swfPath)}>预览</a>
            </span>
          ) : (
            ''
          )}
        </Fragment>
      ),
    },
  ];

  function onDelete(id) {
    dispatch({
      type: 'keyParts/delatePlan',
      payload: id,
    });
  }

  function edit(id, type) {
    dispatch({
      type: 'keyParts/getPlanItem',
      payload: {
        id: id,
        type: type,
      },
    });
    dispatch({
      type: 'keyParts/qryTypes',
      payload: 3,
    });
  }

  function onShow() {
    dispatch({
      type: 'keyParts/updateState',
      payload: {
        planItemModalVisible: true,
        modalType: 'createPlans',
        planItem: {},
      },
    });
    dispatch({
      type: 'keyParts/qryTypes',
      payload: 3,
    });
  }

  function onShowDocument(id, path) {
    dispatch({
      type: 'keyParts/getDocumnet',
      payload: {
        documnetModalVisible: true,
        id: id,
        filePath: path,
        newKey: new Date().getTime() + '',
      },
    });
  }

  function onShowChoosePlan() {
    dispatch({
      type: 'keyParts/qryPlanListByParams',
      payload: {
        keyUnitId:keyParts.keyPartsId,
        // planItemModalVisible: true,
        // modalType: 'createPlans',
        // planItem: {},
      },
    });
    dispatch({
      type: 'keyParts/qryTypes',
      payload: 3,
    });
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col style={{ top: '-12px' }} span={24}>
            <Button type="primary" htmlType="submit" onClick={() => onShowChoosePlan()}>
              添加预案
            </Button>
          </Col>
        </Row>
        <Table
          columns={columns}
          dataSource={keyParts.planList}
          rowKey={record => record.id}
          loading={loading}
          pagination={false}
        />
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { keyParts: state.keyParts };
}

ShowPlanModal = Form.create()(ShowPlanModal);

export default connect(mapStateToProps)(ShowPlanModal);
