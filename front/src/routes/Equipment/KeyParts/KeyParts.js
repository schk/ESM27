import React, { Fragment } from 'react';
import {
  Table,
  Form,
  Button,
  Popconfirm,
  Card,
  Input,
  Divider,
  Row,
  Col,
  Select,
  Tree,
  Icon,
  Upload,
  message,
  Modal,
} from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import ShowFileModal from './ShowFileModal';
import ShowPlanModal from './ShowPlanModal';
import ShowDangersModal from './ShowDangersModal';
import AddDangersModal from './AddDangersModal';
import DocumentModal from './DocumentModal';
import PlanEditModal from './PlanEditModal';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import { baseUrl } from '../../../config/system';
import ChoosePlanModal from './ChoosePlanModal';

const Option = Select.Option;
const TreeNode = Tree.TreeNode;

function KeyParts({ keyParts, form, dispatch }) {
  const {
    getFieldDecorator,
    getFieldsValue,
    getFieldValue,
    getFieldProps,
    setFieldsValue,
    resetFields,
  } = form;

  const AddEditModalProps = {
    item: keyParts.item,
  };

  const ShowPlansItemMadalProps = {
    item: keyParts.planItem,
  };

  const DocumentModalProps = {
    filePath: keyParts.filePath,
    visible: keyParts.documnetModalVisible,
    title: '查看文档',
    dispatch: dispatch,
  };

  const DocumentModallGen = () => <DocumentModal {...DocumentModalProps} />;

  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 60,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    {
      title: '重点部位名称',
      dataIndex: 'name',
      key: 'name',
      width: 100,
      render: (text, record, value) => (
        <Fragment>
          <a onClick={() => onFindInfo(record.id_)}>{record.name}</a>
        </Fragment>
      ),
    },
    { title: '所属单位', dataIndex: 'keyUnitName', key: 'keyUnitName ', width: 130 },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 150,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_)}>修改</a>
          <Divider type="vertical" />
          <a onClick={() => onShowDangers(record.id_)}>添加危化品</a>
          <Divider type="vertical" />
          <a onClick={() => onShowPlans(record.id_)}>增加预案</a>
          <Divider type="vertical" />
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  function onShowDangers(id) {
    dispatch({
      type: 'keyParts/onShowDangers',
      payload: id,
    });
  }

  function onShowPlans(id) {
    dispatch({
      type: 'keyParts/getPlans',
      payload: id,
    });
  }

  const onFindInfo = id => {
    dispatch({
      type: 'keyParts/findInfo',
      payload: id,
    });
  };

  function onUpdate(id) {
    dispatch({
      type: 'keyParts/info',
      payload: id,
    });
  }

  function onAdd() {
    dispatch({
      type: 'keyParts/updateState',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'keyParts/del',
      payload: id,
      search: {
        keyUnitId: keyParts.keyUnitId,
        pageNum: keyParts.current,
        pageSize: keyParts.pageSize,
        ...getFieldsValue(),
      },
    });
  }

  function handleSearch() {
    dispatch({
      type: 'keyParts/qryListByParams',
      payload: {
        pageNum: 1,
        pageSize: keyParts.pageSize,
        ...getFieldsValue(),
        keyUnitId: keyParts.keyUnitId,
      },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'keyParts/qryListByParams',
      payload: { keyParts: 1, pageSize: keyParts.pageSize, keyUnitId: keyParts.keyUnitId },
    });
  }

  function callback(key) {
    console.log(key);
  }

  const loop = data =>
    data.map(item => {
      return (
        <TreeNode
          key={item.id_}
          title={item.name}
          icon={
            <Icon
              style={{
                width: '10px',
                height: '10px',
                background: 'url(images/liBgIcon.png)',
                display: 'inlineBlock',
                verticalAlign: '1px',
              }}
            />
          }
        />
      );
    });

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  const onSelect = info => {
    if (info.length > 0) {
      dispatch({
        type: 'keyParts/qryListByParams',
        payload: { ...getFieldsValue(), ...{ keyUnitId: info[0] } },
      });
    } else {
      dispatch({
        type: 'keyParts/qryListByParams',
        payload: { ...getFieldsValue(), ...{ keyUnitId: undefined } },
      });
    }
  };

  const pagination = {
    current: keyParts.current,
    pageSize: keyParts.pageSize,
    total: keyParts.total,
    showSizeChanger: true,
    showTotal: total => '共' + keyParts.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'keyParts/qryListByParams',
        payload: {
          pageNum: current,
          pageSize: size,
          ...getFieldsValue(),
          keyUnitId: keyParts.keyUnitId,
        },
      });
    },
    onChange(current) {
      dispatch({
        type: 'keyParts/qryListByParams',
        payload: {
          pageNum: current,
          pageSize: keyParts.pageSize,
          ...getFieldsValue(),
          keyUnitId: keyParts.keyUnitId,
        },
      });
    },
  };

  function handleTableChange(pagination, filters, sorter) {
    let isChinese = ['account', 'name'].indexOf(sorter.field) >= 0;
    dispatch({
      type: 'keyParts/qryListByParams',
      payload: {
        pageNum: pagination.current,
        pageSize: pagination.pageSize,
        ...getFieldsValue(),
        orderBy: sorter.field,
        order: sorter.order,
        isChinese,
      },
    });
  }

  function downloadTemplate() {
    window.open(baseUrl + '/execl/downLoadTemplate.jhtml?type=keyParts');
  }

  function initLoopMsg(values) {
    return values != null && values.length > 0
      ? values.map((k, index) => {
          return <div key={index}>{k}</div>;
        })
      : null;
  }

  const props = {
    name: 'file',
    action: baseUrl + '/keyUnit/excel/importKeyPartsExcel.jhtml',
    beforeUpload(file) {
      // const isJPG = file.type === 'application/vnd.ms-excel' || file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
      // if (!isJPG) {
      //   message.error('只能上传EXCEL文件！');
      // }
      // return isJPG;
    },
    onChange(info) {
      let fileList = info.fileList;
      if (info.file.status === 'done') {
        if (info.file.response.httpCode == 200) {
          if (info.file.response.errorCode == 1) {
          } else if (info.file.response.errorCode == 2) {
            if (info.file.response.data && info.file.response.data.length > 0) {
              Modal.error({
                title: '导入提示',
                content: initLoopMsg(info.file.response.data),
              });
            }
          } else {
            message.success(`${info.file.name} 上传成功`);
            dispatch({
              type: 'keyParts/qryListByParams',
              payload: { keyUnitId: undefined },
            });
          }
        } else {
          Modal.error({
            title: '导入提示',
            content: info.file.response.msg,
          });
        }
        fileList = fileList.filter(file => {
          return file.response && file.response.fid;
        });
        //只保留最后一条记录
        fileList = fileList.slice(-1);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`);
      }
      let fildsValue = {};
      fildsValue['file'] = fileList.map(file => {
        return file;
      });
      setFieldsValue(fildsValue);
    },
  };

  const AddDangersModalProps = {
    visible: keyParts.addDangersVisible,
    existDangersList: keyParts.existDangersList,
    allDangersList: keyParts.allDangersList,
    currentDangers: keyParts.currentDangers,
    pageSizeDangers: keyParts.pageSizeDangers,
    totalDangers: keyParts.totalDangers,
    maskClosable: false,
    dispatch: dispatch,
    title: '新增明细',
    wrapClassName: 'vertical-center-modal',
    onCancel() {
      dispatch({
        type: 'keyParts/hideSecondModal',
      });
    },
  };

  return (
    <PageHeaderLayout>
      <div style={{ backgroundColor: '#fff', position: 'relative', overflow: 'hidden' }}>
        <div
          style={{
            width: '250px',
            position: 'absolute',
            left: '0',
            right: '0',
            bottom: '0',
            top: '0',
          }}
        >
          <div
            style={{
              width: '100%',
              height: '40px',
              backgroundColor: '#232f40',
              lineHeight: '40px',
              color: '#fff',
              paddingLeft: '20px',
              fontSize: '14px',
              fontWeight: '400',
            }}
          >
            重点单位
          </div>
          <div
            style={{
              position: 'absolute',
              left: '0',
              top: '40px',
              bottom: '0',
              right: '0',
              overflow: 'auto',
            }}
          >
            <Tree defaultExpandedKeys={['1']} onSelect={onSelect} showIcon>
              {keyParts.keyUnitList && loop(keyParts.keyUnitList)}
            </Tree>
          </div>
        </div>
        <div style={{ marginLeft: '250px', paddingTop: '0px', borderLeft: '1px solid #e8e8e8' }}>
          <Card bordered={false} style={{ minHeight: '600px' }}>
            <div className={styles.tableList}>
              <div className={styles.tableListForm}>
                <Form layout="inline">
                  <Row gutter={{ md: 8, lg: 24, xl: 24 }}>
                    <Col span={6} sm={6}>
                      <Form.Item label="重点部位名称：">
                        {getFieldDecorator('name')(<Input placeholder="输入重点部位名称查找" />)}
                      </Form.Item>
                    </Col>
                    <Col md={5} sm={4}>
                      <span className={styles.submitButtons}>
                        <Button type="primary" htmlType="submit" onClick={handleSearch}>
                          查询
                        </Button>
                        <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                          重置
                        </Button>
                        <Button
                          style={{ marginLeft: 80 }}
                          icon="plus"
                          type="primary"
                          onClick={onAdd}
                        >
                          新增
                        </Button>
                        <Button
                          style={{ marginLeft: 8 }}
                          type="primary"
                          icon="download"
                          onClick={downloadTemplate}
                        >
                          下载模板
                        </Button>
                        <Upload
                          {...getFieldProps(
                            'file',
                            {
                              validate: [
                                {
                                  rules: [
                                    { type: 'array', required: true, message: '请添加数据文件' },
                                  ],
                                  trigger: 'onBlur',
                                },
                              ],
                            },
                            { valuePropName: 'fileIds' }
                          )}
                          {...props}
                          fileList={getFieldValue('file')}
                        >
                          <Button style={{ marginLeft: 8 }} type="primary" icon="upload">
                            导入
                          </Button>
                        </Upload>
                      </span>
                    </Col>
                  </Row>
                </Form>
              </div>

              <div style={{ width: '100%' }}>
                <Table
                  columns={columns}
                  dataSource={keyParts.list}
                  style={{ marginTop: 10 }}
                  pagination={pagination}
                  loading={keyParts.loading}
                  rowKey={record => record.id_}
                />
              </div>
            </div>
          </Card>
        </div>
      </div>
      <AddEditModal {...AddEditModalProps} />
      <ShowFileModal {...AddEditModalProps} />
      <ShowPlanModal />
      <ShowDangersModal />
      <AddDangersModal {...AddDangersModalProps} />
      <ChoosePlanModal{...ShowPlansItemMadalProps} />
      <DocumentModallGen />
    </PageHeaderLayout>
  );
}

function mapStateToProps(state) {
  return {
    keyParts: state.keyParts,
    loading: state.loading.models.keyParts,
  };
}

KeyParts = Form.create()(KeyParts);

export default connect(mapStateToProps)(KeyParts);
