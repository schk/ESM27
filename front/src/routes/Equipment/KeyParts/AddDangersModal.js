import React, { Fragment, Component } from 'react';
import {
  Form,
  Input,
  Modal,
  Button,
  Row,
  Col,
  Table,
  Tag,
  Popconfirm,
  Divider,
  message,
} from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import moment from 'moment';
import { baseUrl, baseFileUrl } from '../../../config/system';
const FormItem = Form.Item;
const { TextArea } = Input;
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

var selectList = [],
  selectedRows = [],
  _dispatch;
function addSelectlist(obj) {
  var ind = selectList.indexOf(obj.id);
  console.log(3);
  if (ind == -1) {
    console.log(4);
    selectedRows.push(obj);
    selectList.push(obj.id);
  }
}
function delSelectlist(obj) {
  var ind = selectList.indexOf(obj.id);
  if (ind !== -1) {
    selectList.splice(ind, 1);
    selectedRows.splice(ind, 1);
  }
}
function retSelectlist(list) {
  selectList = [];
  selectedRows = [];

  for (var i = 0; i < list.length; i++) {
    selectedRows.push(list[i]);
    selectList.push(list[i].id);
  }
}

class AddDangersModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      selectList: [],
    };
  }

  componentWillReceiveProps(props) {
    if (!this.props.visible && props.visible) {
      retSelectlist(props.existDangersList);
      this.setState({ selectList });
    }
  }

  // 将选中的数据放在state里
  onSelectChange = (selectedRowKeys, selectedRows) => {
    selectList.push(selectedRowKeys);
    this.setState({ selectedRows });
  };
  onSelect = (record, selected, selectedRows) => {
    if (selected) {
      addSelectlist(record);
    } else {
      delSelectlist(record);
    }
    this.setState({ selectList });
  };
  onSelectAll = (selected, selectedRows, changeRows) => {
    if (selected) {
      for (var i = 0; i < changeRows.length; i++) {
        addSelectlist(changeRows[i]);
      }
    } else {
      for (var i = 0; i < changeRows.length; i++) {
        delSelectlist(changeRows[i]);
      }
    }
    this.setState({ selectList });
  };
  onSelectInvert = (selectedRows, dsd3, dsaf3) => {
    var i = 0,
      k = 0;
    for (i = 0; i < this.props.allDangersList.length; i++) {
      var obj = false;
      for (k = 0; k < selectedRows.length; k++) {
        if (this.props.allDangersList[i].id == selectedRows[k]) {
          obj = true;
        }
      }
      if (obj) {
        addSelectlist(this.props.allDangersList[i]);
      } else {
        delSelectlist(this.props.allDangersList[i]);
      }
    }
    this.setState({ selectList });
  };
  render() {
    const {
      children,
      dispatch,
      title,
      existDangersList,
      allDangersList,
      currentDangers,
      pageSizeDangers,
      totalDangers,
      visible,
      buttomLoading,
    } = this.props;
    const { getFieldDecorator, resetFields, getFieldsValue, validateFields } = this.props.form;

    const modalOpts = {
      title: '选择危化品',
      visible: visible,
      maskClosable: false,
      width: 1050,
      onCancel: handleCansel,
      footer: [
        <Button key="back" type="ghost" size="large" onClick={handleCansel}>
          取消
        </Button>,
        <Button
          key="submit"
          type="primary"
          size="large"
          onClick={() => handleOk()}
          loading={buttomLoading}
        >
          保存
        </Button>,
      ],
    };
    if (!visible) {
      resetFields();
    }
    function handleCansel() {
      dispatch({
        type: 'keyParts/updateState',
        payload: {
          addDangersVisible: false,
          currentDangers: 1,
          totalDangers: 0,
          allDangersList: [],
        },
      });
    }

    // 点击保存将state里的选中的值放到回显给父级页面上
    function handleOk() {
      var arr = [];
      for (var i = 0; i < selectedRows.length; i++) {
        arr.push(selectedRows[i]);
      }
      dispatch({
        type: 'keyParts/updateState',
        payload: {
          existDangersList: arr,
          currentDangers: 1,
          totalDangers: 0,
          allDangersList: [],
          addDangersVisible: false,
        },
      });
    }

    const columns = [
      {
        title: '危化品名称',
        dataIndex: 'cName',
        key: 'cName',
        width: 180,
        render: (text, record, value) => (
          <Fragment>
            <a onClick={() => onFindInfo(record.id_)}>{record.cName}</a>
          </Fragment>
        ),
      },
      { title: '危化品类型', dataIndex: 'typeName', key: 'typeName', width: 100 },
      {
        title: 'CAS号',
        dataIndex: 'casNum',
        key: 'casNum',
        width: 80,
      },
      {
        title: 'UN号',
        dataIndex: 'unNum',
        key: 'unNum',
        width: 80,
      },
      { title: '分子式', dataIndex: 'molecularFormula', key: 'molecularFormula', width: 100 },
      { title: '分子量', dataIndex: 'molecularWeight', key: 'molecularWeight', width: 100 },
    ];

    function onDelete(id) {
      dispatch({
        type: 'keyParts/delDangers',
        payload: id,
        search: { keyPartsId: keyParts.keyPartsId },
      });
    }

    function handleSearchDangers() {
      validateFields(errors => {
        if (errors) {
          return;
        }
        const data = getFieldsValue();
        dispatch({
          type: 'keyParts/getAllDangers',
          payload: {
            pageNum: 1,
            pageSize: pageSizeDangers,
            ...getFieldsValue(),
          },
        });
      });
    }

    function handleFormResetDangers() {
      resetFields();
    }

    const pagination = {
      current: currentDangers,
      pageSize: pageSizeDangers,
      total: totalDangers,
      showSizeChanger: true,
      showTotal: total => '共' + totalDangers + '条',
      onShowSizeChange(current, size) {
        dispatch({
          type: 'keyParts/getAllDangers',
          payload: {
            pageNum: current,
            pageSize: size,
            ...getFieldsValue(),
          },
        });
      },
      onChange(current) {
        dispatch({
          type: 'keyParts/getAllDangers',
          payload: {
            pageNum: current,
            pageSize: pageSizeDangers,
            ...getFieldsValue(),
          },
        });
      },
    };

    const rowSelection = {
      selections: true,
      selectedRowKeys: this.state.selectList,
      onSelect: this.onSelect.bind(this),
      onSelectAll: this.onSelectAll.bind(this),
      onSelectInvert: this.onSelectInvert.bind(this),
    };

    return (
      <Modal {...modalOpts}>
        <Form layout="inline">
          <Row style={{ bottom: '10px' }} gutter={{ md: 8, lg: 24, xl: 24 }}>
            <Col span={6} sm={8}>
              <Form.Item>
                {getFieldDecorator('keyWord', {
                  rules: [{ required: true, message: '输入关键字模糊查找' }],
                })(<Input placeholder="输入关键字模糊查找" />)}
              </Form.Item>
            </Col>
            <Col md={5} sm={4}>
              <span className={styles.submitButtons}>
                <Button type="primary" htmlType="submit" onClick={handleSearchDangers}>
                  查询
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={handleFormResetDangers}>
                  重置
                </Button>
              </span>
            </Col>
          </Row>
        </Form>

        <Table
          columns={columns}
          rowSelection={rowSelection}
          dataSource={allDangersList}
          rowKey={record => record.id}
          pagination={pagination}
        />
      </Modal>
    );
  }
}

export default Form.create()(AddDangersModal);
