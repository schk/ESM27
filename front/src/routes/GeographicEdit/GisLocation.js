import React from 'react';
import { connect } from 'dva';
import { Form } from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from '../../common/common.less';
import { baseUrl } from '../../config/system';

class GisLocation extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <PageHeaderLayout>
        <iframe
          src={baseUrl + '/api/gis/toLocationPage.jhtml'}
          style={{ width: '100%', height: '760px', border: '0' }}
        />
      </PageHeaderLayout>
    );
  }
}
function mapStateToProps(state) {
  return { gisLocation: state.gisLocation };
}

GisLocation = Form.create()(GisLocation);

export default connect(mapStateToProps)(GisLocation);
