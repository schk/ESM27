import React from 'react';
import { connect } from 'dva';
import { Form, Tree } from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { baseUrl } from '../../config/system';

class GeographicEdit extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <PageHeaderLayout>
        <iframe
          src={baseUrl + '/api/geographicEdit/qryList.jhtml'}
          style={{ width: '100%', height: '760px', border: '0' }}
        />
      </PageHeaderLayout>
    );
  }
}
function mapStateToProps(state) {
  return {
    sysUser: state.sysUser,
  };
}

GeographicEdit = Form.create()(GeographicEdit);

export default connect(mapStateToProps)(GeographicEdit);
