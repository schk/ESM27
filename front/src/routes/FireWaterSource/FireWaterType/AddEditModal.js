import React from 'react';
import { Form, Input, Modal, Button, Radio, Row, Col, Select } from 'antd';
import { baseFileUrl } from '../../../config/system';
import { connect } from 'dva';
const Option = Select.Option;
const FormItem = Form.Item;
const { TextArea } = Input;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

let AddEditModal = ({ fireWaterType, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: fireWaterType.modalType == 'create' ? '新建消防水源类型' : '修改消防水源类型',
    visible: fireWaterType.modalVisible,
    maskClosable: false,
    width: 600,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={fireWaterType.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!fireWaterType.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'fireWaterType/updateState',
      payload: {
        modalVisible: false,
        locationItem: { lonLat: '' },
      },
    });
  }
  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = getFieldsValue();
      data.id = fireWaterType.modalType === 'create' ? '' : item.id;
      data.type = 6;
      dispatch({
        type: `fireWaterType/${fireWaterType.modalType}`,
        payload: data,
        search: fireWaterType.selectObj,
      });
    });
  }

  const loopOption = data =>
    data.map(item => {
      return (
        <Option key={item.dtPath}>
          <img style={{ width: '20px', height: '20px' }} src={baseFileUrl + item.dtPath} alt="" />
          {item.name}
        </Option>
      );
    });

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={24}>
            <FormItem label="分类名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '请填写分类名称' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写分类名称" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="选择图标:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('dtPath', {
                initialValue: item.dtPath ? item.dtPath : undefined,
                rules: [{ required: true, message: '请选择图标' }],
              })(
                <Select placeholder="请选择地图显示图标">
                  {loopOption(fireWaterType.plottingList)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="分类描述：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('remark', {
                initialValue: item.remark,
                rules: [{ max: 100, message: '最长不超过100个字符' }],
              })(<TextArea autosize={{ minRows: 4 }} />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { fireWaterType: state.fireWaterType };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
