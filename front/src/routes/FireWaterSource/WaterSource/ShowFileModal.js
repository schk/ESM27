import React from 'react';
import { Form, Input, Modal, Button, Radio, Row, Col, Select } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const Option = Select.Option;
const { TextArea } = Input;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 17,
  },
};
const formItemLayout1 = {
  labelCol: {
    span: 3,
  },
  wrapperCol: {
    span: 21,
  },
};

let AddEditModal = ({ waterSource, item = {}, form, dispatch }) => {
  const { getFieldDecorator, resetFields } = form;

  const modalOpts = {
    title: '查看消防水源信息',
    visible: waterSource.findModalVisible,
    maskClosable: false,
    width: 1000,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
    ],
  };
  if (!waterSource.findModalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'waterSource/updateState',
      payload: {
        findModalVisible: false,
        locationItem: {},
      },
    });
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={12}>
            <FormItem required label="消防水源名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
              })(<Input type="text" readOnly placeholder="请填写消防水源名称" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem required label="消防水源类型:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('typeName', {
                initialValue: item.typeName ? item.typeName + '' : undefined,
              })(<Input type="text" readOnly placeholder="" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="所属重点单位:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('keyUnitName', {
                initialValue: item.keyUnitName == null ? waterSource.keyUnitId : item.keyUnitName,
              })(<Input type="text" readOnly placeholder="" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="水源地点:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('addr', {
                initialValue: item.addr,
              })(<Input type="text" readOnly placeholder="" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="储量" hasFeedback {...formItemLayout}>
              {getFieldDecorator('reserves', {
                initialValue: item.reserves,
              })(<Input readOnly placeholder="" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="流量" hasFeedback {...formItemLayout}>
              {getFieldDecorator('flow', {
                initialValue: item.flow,
              })(<Input readOnly placeholder="" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
              <FormItem label="勘察时间" hasFeedback {...formItemLayout}>
                {getFieldDecorator('surveyTime', {
                  initialValue: item.surveyTime? new moment(item.surveyTime).format('YYYY-MM-DD HH:mm:ss') : undefined,
                })(<Input type="text" readOnly/>)}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="勘察人" hasFeedback {...formItemLayout}>
                {getFieldDecorator('surveyor', {
                  initialValue: item.surveyor,
                })(<Input type="text" readOnly/>)}
              </FormItem>
            </Col>
        </Row>

        <Row>
          <Col span={12}>
              <FormItem label="核查人" hasFeedback {...formItemLayout}>
                {getFieldDecorator('verifier', {
                  initialValue: item.verifier,
                })(<Input type="text" readOnly/>)}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="水源点负责人" hasFeedback {...formItemLayout}>
                {getFieldDecorator('waterSourcePointHead', {
                  initialValue: item.waterSourcePointHead,
                })(<Input type="text" readOnly/>)}
              </FormItem>
            </Col>
        </Row>
        <Row>
          <Col span={12}>
              <FormItem label="电话" hasFeedback {...formItemLayout}>
                {getFieldDecorator('phone', {
                  initialValue: item.phone,
                })(<Input type="text" readOnly/>)}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="管辖单位" hasFeedback {...formItemLayout}>
                {getFieldDecorator('jurisdictionUnit', {
                  initialValue: item.jurisdictionUnit,
                })(<Input type="text" readOnly/>)}
              </FormItem>
            </Col>
        </Row>
        <Row>
            <Col span={12}>
              <FormItem {...formItemLayout} label="是否好用">
                {getFieldDecorator('goodUse', {
                  initialValue: item.goodUse == null ? 1 : item.goodUse == true ? 1 : 0,
                })(
                  <RadioGroup readOnly>
                    <Radio value={1}>好用</Radio>
                    <Radio value={0}>不好用</Radio>
                  </RadioGroup>
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem required label="经纬度:" hasFeedback {...formItemLayout}>
                {getFieldDecorator('lonLat', {
                  initialValue: waterSource.locationItem.lonLat
                    ? waterSource.locationItem.lonLat + ''
                    : undefined,
                })(<Input type="text" readOnly placeholder="经纬度" />)}
              </FormItem>
            </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="描述：" hasFeedback {...formItemLayout1}>
              {getFieldDecorator('remark', {
                initialValue: item.remark,
              })(<TextArea autosize={{ minRows: 4 }} style={{width:810}} readOnly />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { waterSource: state.waterSource };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
