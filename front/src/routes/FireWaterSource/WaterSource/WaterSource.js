import React, { Fragment } from 'react';
import {
  Table,
  Form,
  Button,
  Popconfirm,
  Card,
  Input,
  Divider,
  Row,
  Col,
  Tag,
  Select,
  Modal,
  Tree,
  Upload,
  message,
} from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import LocationModal from './LocationModal';
import ShowFileModal from './ShowFileModal';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import { baseUrl } from '../../../config/system';
import moment from 'moment';
const Option = Select.Option;
const TreeNode = Tree.TreeNode;
const confirm = Modal.confirm;

function WaterSource({ location, waterSource, form, dispatch, loading }) {
  const {
    getFieldDecorator,
    getFieldProps,
    getFieldValue,
    getFieldsValue,
    setFieldsValue,
    resetFields,
  } = form;
  const AddEditModalProps = {
    item: waterSource.item,
  };
  const LocationModalProps = {};
  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      fixed: 'left',
      render: (value, row, index) => {
        return index + 1;
      },
    },
    {
      title: '水源名称',
      dataIndex: 'name',
      key: 'name',
      width: 120,
      fixed: 'left',
      render: (text, record, value) => (
        <Fragment>
          <a onClick={() => onFindInfo(record.id_)}>{record.name}</a>
        </Fragment>
      ),
    },
    { title: '水源类型', dataIndex: 'typeName', key: 'typeName', width: 150 },
    { title: '所属重点单位', dataIndex: 'keyUnitName', key: 'keyUnitName', width: 150 },
    { title: '水源地点', dataIndex: 'addr', key: 'addr', width: 150 },
    { title: '储量(m³)', dataIndex: 'reserves', key: 'reserves', width: 150 },
    { title: '流量(L/S)', dataIndex: 'flow', key: 'flow', width: 150 },
    { title: '勘查时间', dataIndex: 'surveyTime', key: 'surveyTime', width: 150,
      render:(text,record,index)=>{
        if(record.surveyTime){
          return <span>{moment(record.surveyTime).format('YYYY-MM-DD HH:mm:ss')}</span>;
        }
       
      }
    },
    { title: '勘查人', dataIndex: 'surveyor', key: 'surveyor', width: 150 },
    { title: '核查人', dataIndex: 'verifier', key: 'verifier', width: 150 },
    { title: '水源点负责人', dataIndex: 'waterSourcePointHead', key: 'waterSourcePointHead', width: 150 },
    { title: '电话', dataIndex: 'phone', key: 'phone', width: 150 },
    { title: '管辖单位', dataIndex: 'jurisdictionUnit', key: 'jurisdictionUnit', width: 150 },

    {
      title: '是否好用',
      dataIndex: 'goodUse',
      key: 'goodUse',
      width: 100,
      render: (text, record) => {
        if (record.goodUse == 1) {
          return <Tag color="#2db7f5">好用</Tag>;
        }
        if (record.goodUse == 0) {
          return <Tag color="#f50">不好用</Tag>;
        }
      },
    },
    {
      title: '经纬度',
      dataIndex: 'lon',
      key: 'lon',
      width: 150,
      render: (text, record) => {
        if(record.lon && record.lat){
          return record.lon + ',' + record.lat;
        }
        return '';
      },
    },

    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 120,
      fixed: 'right',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_)}>修改</a>
          <Divider type="vertical" />
          {/* <a onClick={() => onFindInfo(record.id_)}>查看</a>
          <Divider type="vertical" /> */}
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];
  const onFindInfo = id => {
    dispatch({
      type: 'waterSource/findInfo',
      payload: id,
    });
  };
  function onUpdate(id) {
    dispatch({
      type: 'waterSource/info',
      payload: id,
    });
  }

  function onAdd() {
    dispatch({
      type: 'waterSource/updateState',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'waterSource/del',
      payload: id,
      search: {
        keyUnitId: waterSource.keyUnitId,
        pageNum: waterSource.current,
        pageSize: waterSource.pageSize,
        ...getFieldsValue(),
      },
    });
  }

  function handleSearch() {
    dispatch({
      type: 'waterSource/qryListByParams',
      payload: {
        pageNum: 1,
        pageSize: waterSource.pageSize,
        ...getFieldsValue(),
        keyUnitId: waterSource.keyUnitId,
      },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'waterSource/qryListByParams',
      payload: { pageNum: 1, pageSize: waterSource.pageSize, keyUnitId: waterSource.keyUnitId },
    });
  }

  function callback(key) {
    console.log(key);
  }

  const loop = data =>
    data.map(item => {
      return <TreeNode key={item.id_} title={item.name} />;
    });

  const onSelect = info => {
    if (info.length > 0) {
      dispatch({
        type: 'waterSource/qryListByParams',
        payload: { ...getFieldsValue(), ...{ keyUnitId: info[0] } },
      });
    } else {
      dispatch({
        type: 'waterSource/qryListByParams',
        payload: { ...getFieldsValue(), ...{ keyUnitId: undefined } },
      });
    }
  };

  const pagination = {
    current: waterSource.current,
    pageSize: waterSource.pageSize,
    total: waterSource.total,
    showSizeChanger: true,
    showTotal: total => '共' + waterSource.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'waterSource/qryListByParams',
        payload: {
          pageNum: current,
          pageSize: size,
          ...getFieldsValue(),
          keyUnitId: waterSource.keyUnitId,
        },
      });
    },
    onChange(current) {
      dispatch({
        type: 'waterSource/qryListByParams',
        payload: {
          pageNum: current,
          pageSize: waterSource.pageSize,
          ...getFieldsValue(),
          keyUnitId: waterSource.keyUnitId,
        },
      });
    },
  };

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });
  const LocationModalGen = () => <LocationModal {...LocationModalProps} />;

  function downloadTemplate() {
    window.open(baseUrl + '/execl/downLoadTemplate.jhtml?type=fireWaterSourceTemplate');
  }

  function initLoopMsg(values) {
    return values != null && values.length > 0
      ? values.map((k, index) => {
          return <div key={index}>{k}</div>;
        })
      : null;
  }

  const props = {
    name: 'file',
    action: baseUrl + '/fireWaterSource/excel/importExcel.jhtml',
    beforeUpload(file) {
      // const isJPG = file.type === 'application/vnd.ms-excel' || file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
      // if (!isJPG) {
      //   message.error('只能上传EXCEL文件！');
      // }
      // return isJPG;
    },
    onChange(info) {
      let fileList = info.fileList;
      if (info.file.status === 'done') {
        if (info.file.response.httpCode == 200) {
          if (info.file.response.errorCode == 1) {
            //弹出确认取消框
            confirm({
              title: '信息提示框',
              content: initLoopMsg(info.file.response.data),
              okText: '继续导入',
              cancelText: '取消导入',
              onOk() {
                dispatch({
                  type: 'waterSource/importExcelConfirm',
                  payload: info.file.response.times,
                });
              },
              onCancel() {
                dispatch({
                  type: 'waterSource/delExcelTemp',
                  payload: info.file.response.times,
                });
              },
            });
          } else if (info.file.response.errorCode == 2) {
            if (info.file.response.data && info.file.response.data.length > 0) {
              Modal.error({
                title: '导入提示',
                content: initLoopMsg(info.file.response.data),
              });
            }
          } else {
            message.success(`${info.file.name} 上传成功`);
            dispatch({
              type: 'waterSource/qryListByParams',
              payload: { keyUnitId: undefined },
            });
          }
        } else {
          Modal.error({
            title: '导入提示',
            content: info.file.response.msg,
          });
        }
        fileList = fileList.filter(file => {
          return file.response && file.response.fid;
        });
        //只保留最后一条记录
        fileList = fileList.slice(-1);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`);
      }
      let fildsValue = {};
      fildsValue['file'] = fileList.map(file => {
        return file;
      });
      setFieldsValue(fildsValue);
    },
  };

  return (
    <PageHeaderLayout>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 24 }}>
                <Col span={5} sm={5}>
                  <Form.Item label="名称：">
                    {getFieldDecorator('name')(<Input placeholder="输入名称查找" />)}
                  </Form.Item>
                </Col>
                <Col span={7} sm={6}>
                  <Form.Item label="所属类型：">
                    {getFieldDecorator('dicId')(
                      <Select allowClear placeholder="请选择所属类型">
                        <Option value="">全部</Option>
                        {loopOption(waterSource.typeList)}
                      </Select>
                    )}
                  </Form.Item>
                </Col>
                <Col md={5} sm={4}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                      重置
                    </Button>

                    <Button
                      style={{ marginLeft: 80 }}
                      type="primary"
                      icon="download"
                      onClick={downloadTemplate}
                    >
                      下载模板
                    </Button>
                    <Upload
                      {...getFieldProps(
                        'file',
                        {
                          validate: [
                            {
                              rules: [{ type: 'array', required: true, message: '请添加数据文件' }],
                              trigger: 'onBlur',
                            },
                          ],
                        },
                        { valuePropName: 'fileIds' }
                      )}
                      {...props}
                      fileList={getFieldValue('file')}
                    >
                      <Button style={{ marginLeft: 8 }} type="primary" icon="upload">
                        导入
                      </Button>
                    </Upload>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <div className={styles.submitButtons}>
            <Button icon="plus" type="primary" onClick={onAdd}>
              新增
            </Button>
          </div>
          <div style={{ width: '100%', paddingRight: '20px' }}>
            <Table
              columns={columns}
              scroll={{ x: 2000}}
              dataSource={waterSource.list}
              style={{ marginTop: 10 }}
              pagination={pagination}
              loading={waterSource.loading}
              rowKey={record => record.id_}
            />
          </div>
        </div>
      </Card>
      <AddEditModal {...AddEditModalProps} />
      <ShowFileModal {...AddEditModalProps} />
      <LocationModalGen />
    </PageHeaderLayout>
  );
}

function mapStateToProps(state) {
  return {
    waterSource: state.waterSource,
    loading: state.loading.models.waterSource,
  };
}

WaterSource = Form.create()(WaterSource);

export default connect(mapStateToProps)(WaterSource);
