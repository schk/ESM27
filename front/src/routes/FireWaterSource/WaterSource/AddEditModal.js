import React from 'react';
import { Form, Input, Modal, Button, Radio, Row, InputNumber, Col, Select, message,DatePicker } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;
const { TextArea } = Input;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 17,
  },
};
const formItemLayout1 = {
  labelCol: {
    span: 3,
  },
  wrapperCol: {
    span: 21,
  },
};
let AddEditModal = ({ waterSource, item = {}, form, dispatch }) => {
  const { getFieldDecorator, validateFields, getFieldsValue, resetFields } = form;

  const modalOpts = {
    title: waterSource.modalType == 'create' ? '新建消防水源' : '修改消防水源',
    visible: waterSource.modalVisible,
    maskClosable: false,
    width: 1000,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={waterSource.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!waterSource.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'waterSource/updateState',
      payload: {
        modalVisible: false,
        locationItem: {},
      },
    });
  }

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  const onGetLocation = () => {
    const data = getFieldsValue();
    dispatch({
      type: 'waterSource/updateState',
      payload: {
        locationModalVisible: true,
        newKey1: new Date().getTime() + '',
        locationItem: { lonLat: data.lonLat },
      },
    });
  };

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      var lonLat = getFieldsValue(["lonLat"]);
          if(lonLat.lonLat==undefined){
          message.error("坐标不能为空");
          return;
      }
      const data = getFieldsValue();
      data.id = waterSource.modalType === 'create' ? '' : item.id;
      dispatch({
        type: `waterSource/${waterSource.modalType}`,
        payload: data,
        search: waterSource.selectObj,
      });
    });
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={12}>
            <FormItem label="消防水源名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '请填写消防水源名称' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写消防水源名称" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="消防水源类型:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('dicId', {
                initialValue: item.dicId ? item.dicId + '' : undefined,
                rules: [{ required: true, message: '请选择消防水源类型' }],
              })(
                <Select placeholder="请选择消防水源类型">{loopOption(waterSource.typeList)}</Select>
              )}
            </FormItem>
          </Col>
        </Row>
     
        <Row>
            <Col span={12}>
              <FormItem label="所属重点单位:" hasFeedback {...formItemLayout}>
                {getFieldDecorator('keyUnitId', {
                  initialValue: item.keyUnitId == null ? waterSource.keyUnitId : item.keyUnitId,
                })(
                  <Select
                    allowClear
                    showSearch
                    optionFilterProp="children"
                    placeholder="请选择所属重点单位"
                  >
                    {loopOption(waterSource.keyUnitList)}
                  </Select>
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="水源地点:" hasFeedback {...formItemLayout}>
                {getFieldDecorator('addr', {
                  initialValue: item.addr,
                  rules: [
                    { required: true, message: '请填写消防水源地点' },
                    { max: 100, message: '最长不超过100个字' },
                  ],
                })(<Input type="text" placeholder="请填写消防水源地点" />)}
              </FormItem>
            </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="储量(m³)" hasFeedback {...formItemLayout}>
              {getFieldDecorator('reserves', {
                initialValue: item.reserves,
              })(
                <InputNumber
                  style={{ width: '100%' }}
                  min={0}
                  max={9999999}
                  placeholder="请填写消防水源储量"
                />
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="流量(L/S)" hasFeedback {...formItemLayout}>
              {getFieldDecorator('flow', {
                initialValue: item.flow,
              })(
                <InputNumber
                  style={{ width: '100%' }}
                  min={0}
                  max={9999999}
                  placeholder="请填写消防水源流量"
                />
              )}
            </FormItem>
          </Col>
        </Row>
                
        <Row>
          <Col span={12}>
              <FormItem label="勘察时间" hasFeedback {...formItemLayout}>
                {getFieldDecorator('surveyTime', {
                  initialValue: item.surveyTime? new moment(item.surveyTime) : undefined,
                })(<DatePicker showTime style={{ width: '100%' }} placeholder="请选择勘察时间"/>)}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="勘察人" hasFeedback {...formItemLayout}>
                {getFieldDecorator('surveyor', {
                  initialValue: item.surveyor,
                })(<Input type="text" placeholder="请填写勘察人"/>)}
              </FormItem>
            </Col>
        </Row>

        <Row>
          <Col span={12}>
              <FormItem label="核查人" hasFeedback {...formItemLayout}>
                {getFieldDecorator('verifier', {
                  initialValue: item.verifier,
                })(<Input type="text" placeholder="请填写核查人"/>)}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="水源点负责人" hasFeedback {...formItemLayout}>
                {getFieldDecorator('waterSourcePointHead', {
                  initialValue: item.waterSourcePointHead,
                })(<Input type="text" placeholder="请填写水源点负责人"/>)}
              </FormItem>
            </Col>
        </Row>
        <Row>
          <Col span={12}>
              <FormItem label="电话" hasFeedback {...formItemLayout}>
                {getFieldDecorator('phone', {
                  initialValue: item.phone,
                })(<Input type="text" placeholder="请填写电话"/>)}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="管辖单位" hasFeedback {...formItemLayout}>
                {getFieldDecorator('jurisdictionUnit', {
                  initialValue: item.jurisdictionUnit,
                })(<Input type="text" placeholder="请填写管辖单位"/>)}
              </FormItem>
            </Col>
        </Row>

        <Row>
            <Col span={12}>
              <FormItem {...formItemLayout} label="是否好用">
                {getFieldDecorator('goodUse', {
                  initialValue: item.goodUse == null ? 1 : item.goodUse == true ? 1 : 0,
                })(
                  <RadioGroup>
                    <Radio value={1}>好用</Radio>
                    <Radio value={0}>不好用</Radio>
                  </RadioGroup>
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="获取坐标点:" hasFeedback {...formItemLayout}>
                <div
                  style={{
                    marginRight: '10px',
                    marginTop: '5px',
                    width: '28px',
                    height: '28px',
                    background: 'url(images/iconAdd.png)',
                    display: 'inlineBlock',
                    float: 'left',
                    cursor: 'pointer',
                  }}
                  onClick={onGetLocation}
                />
                {getFieldDecorator('lonLat', {
                  initialValue: waterSource.locationItem.lonLat
                    ? waterSource.locationItem.lonLat + ''
                    : undefined,
                })(
                  <Input type="text" readOnly style={{ width: '88%' }} placeholder="请选择经纬度" />
                )}
              </FormItem>
            </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="水源描述：" hasFeedback {...formItemLayout1}>
              {getFieldDecorator('remark', {
                initialValue: item.remark,
                rules: [{ max: 100, message: '最长不超过100个字' }],
              })(<TextArea autosize={{ minRows: 4 }} style={{width:810}}/>)}
            </FormItem>
          </Col>
          
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { waterSource: state.waterSource };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
