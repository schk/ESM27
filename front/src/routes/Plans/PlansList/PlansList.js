import React, { Fragment } from 'react';
import {
  Table,
  Row,
  Form,
  Button,
  Popconfirm,
  Card,
  Input,
  Divider,
  message,
  Upload,
  Col,
  Select,
  Tag,
  DatePicker,
  Modal,
} from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import DocumentModal from './DocumentModal';
import ShowFileModal from './ShowFileModal';
import PageHeader from '../../../layouts/PageHeaderLayout';
const Option = Select.Option;
const FormItem = Form.Item;
const RangePicker = DatePicker.RangePicker;
import moment from 'moment';
import { baseUrl } from '../../../config/system';
const confirm = Modal.confirm;

function PlansList({ location, plansList, form, dispatch, loading }) {
  const {
    getFieldDecorator,
    getFieldsValue,
    setFieldsValue,
    resetFields,
    getFieldProps,
    getFieldValue,
  } = form;
  const AddEditModalProps = {
    item: plansList.item,
  };

  const DocumentModalProps = {
    filePath: plansList.filePath,
    visible: plansList.documnetModalVisible,
    title: '查看文档',
    dispatch: dispatch,
  };

  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    {
      title: '预案名称',
      dataIndex: 'name',
      key: 'name',
      width: 120,
      render: (text, record, value) => (
        <Fragment>
          <a onClick={() => onFindInfo(record.id_, record.type)}>{record.name}</a>
        </Fragment>
      ),
    },
    { title: '预案类型', dataIndex: 'typeName', key: 'typeName', width: 100 },
    {
      title: '预案级别',
      dataIndex: 'level',
      key: 'level',
      width: 80,
      render: (value, row, index) => {
        return value == 1 ? 'Ⅰ级' : value == 2 ? 'Ⅱ级' : value == 3 ? 'Ⅲ级' : 'Ⅳ级';
      },
    },
    {
      title: '预案编号',
      dataIndex: 'code',
      key: 'code',
      width: 120,
    },
    {
      title: '预案状态',
      dataIndex: 'status',
      key: 'status',
      width: 60,
      render: (value, row, index) => {
        if (row.status == 1) {
          return <Tag color="green">可用</Tag>;
        }
        if (row.status == 2) {
          return <Tag color="red">不可用</Tag>;
        }
      },
    },
    { title: '编写人', dataIndex: 'writer', key: 'writer', width: 100 },
    { title: '编写单位', dataIndex: 'fireBrigadeName', key: 'fireBrigadeName', width: 100 },
    {
      title: '编写日期',
      dataIndex: 'writingTime',
      key: 'writingTime',
      width: 100,
      render: (value, row, index) => {
        return value ? new moment(value).format('YYYY-MM-DD') : '';
      },
    },
    { title: '调用次数', dataIndex: 'freq', key: 'freq', width: 80 },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 100,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_, record.type)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
          {record.swfPath ? (
            <span>
              <Divider type="vertical" />
              <a onClick={() => onShowDocument(record.id, record.swfPath)}>预览</a>
            </span>
          ) : (
            ''
          )}
        </Fragment>
      ),
    },
  ];
  function onFindInfo(id, type) {

    dispatch({
      type: 'plansList/findInfo',
      payload: { id: id, type: type },
    });
  }

  function onShowDocument(id, path) {
    dispatch({
      type: 'plansList/getDocumnet',
      payload: {
        documnetModalVisible: true,
        id: id,
        filePath: path,
        newKey: new Date().getTime() + '',
      },
    });
  }

  function onUpdate(id, type) {
    dispatch({
      type: 'plansList/info',
      payload: { id: id, type: type },
    });
  }

  function onAdd() {
    resetFields();
    dispatch({
      type: 'plansList/showCreateModal',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'plansList/del',
      payload: id,
      search: { pageNum: plansList.current, pageSize: plansList.pageSize, ...getFieldsValue() },
    });
  }

  function handleSearch() {
    dispatch({
      type: 'plansList/qryListByParams',
      payload: { pageNum: 1, pageSize: plansList.pageSize, ...getFieldsValue() },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'plansList/qryListByParams',
      payload: { pageNum: 1, pageSize: plansList.pageSize },
    });
  }
  function downloadTemplate() {
    window.open(baseUrl + '/execl/downLoadTemplate.jhtml?type=plansTemplate');
  }

  function initLoopMsg(values) {
    return values != null && values.length > 0
      ? values.map((k, index) => {
          return <div key={index}>{k}</div>;
        })
      : null;
  }

  const props = {
    name: 'file',
    action: baseUrl + '/plan/excel/importExcel.jhtml',
    beforeUpload(file) {
      // const isJPG = file.type === 'application/vnd.ms-excel' || file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
      // if (!isJPG) {
      //   message.error('只能上传EXCEL文件！');
      // }
      // return isJPG;
    },
    onChange(info) {
      let fileList = info.fileList;
      if (info.file.status === 'done') {
        if (info.file.response.httpCode == 200) {
          if (info.file.response.errorCode == 1) {
            //弹出确认取消框
            confirm({
              title: '信息提示框',
              content: initLoopMsg(info.file.response.data),
              okText: '继续导入',
              cancelText: '取消导入',
              onOk() {
                dispatch({
                  type: 'plansList/importExcelConfirm',
                  payload: info.file.response.times,
                });
              },
              onCancel() {
                dispatch({
                  type: 'plansList/delExcelTemp',
                  payload: info.file.response.times,
                });
              },
            });
          } else if (info.file.response.errorCode == 2) {
            if (info.file.response.data && info.file.response.data.length > 0) {
              Modal.error({
                title: '导入提示',
                content: initLoopMsg(info.file.response.data),
              });
            }
          } else {
            message.success(`${info.file.name} 上传成功`);
            dispatch({
              type: 'plansList/qryListByParams',
              payload: {},
            });
          }
        } else {
          Modal.error({
            title: '导入提示',
            content: info.file.response.msg,
          });
        }
        fileList = fileList.filter(file => {
          return file.response && file.response.fid;
        });
        //只保留最后一条记录
        fileList = fileList.slice(-1);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`);
      }
      let fildsValue = {};
      fildsValue['file'] = fileList.map(file => {
        return file;
      });
      setFieldsValue(fildsValue);
    },
  };

  const pagination = {
    current: plansList.current,
    pageSize: plansList.pageSize,
    total: plansList.total,
    showSizeChanger: true,
    showTotal: total => '共' + plansList.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'plansList/qryListByParams',
        payload: { pageNum: current, pageSize: size, ...getFieldsValue() },
      });
    },
    onChange(current) {
      dispatch({
        type: 'plansList/qryListByParams',
        payload: { pageNum: current, pageSize: plansList.pageSize, ...getFieldsValue() },
      });
    },
  };

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  const CardDestroyModallGen = () => <DocumentModal {...DocumentModalProps} />;

  return (
    <PageHeader>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div id="zijId" style={{ display: 'none' }} />
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={6} sm={24}>
                  <FormItem label="预案名称">
                    {getFieldDecorator('name')(<Input placeholder="请输入预案名称" />)}
                  </FormItem>
                </Col>
                <Col md={6} sm={24}>
                  <FormItem label="预案类型">
                    {getFieldDecorator('dicId')(
                      <Select
                        allowClear
                        style={{ width: '100%' }}
                        placeholder="选择预案类型"
                        optionFilterProp="children"
                      >
                        <Option value="">全部</Option>
                        {loopOption(plansList.typeList)}
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={6} sm={24}>
                  <FormItem label="预案级别">
                    {getFieldDecorator('level')(
                      <Select
                        allowClear
                        style={{ width: '100%' }}
                        placeholder="选择级别"
                        optionFilterProp="children"
                      >
                        <Option value="">全部</Option>
                        <Option value="1">Ⅰ级</Option>
                        <Option value="2">Ⅱ级</Option>
                        <Option value="3">Ⅲ级</Option>
                        <Option value="4">Ⅳ级</Option>
                      </Select>
                    )}
                  </FormItem>
                </Col>

                <Col md={6} sm={5}>
                  <FormItem label="编写人 ">
                    {getFieldDecorator('writer')(<Input placeholder="请输入编写人查询" />)}
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={6} sm={24}>
                  <FormItem label="预案状态">
                    {getFieldDecorator('status')(
                      <Select
                        allowClear
                        style={{ width: '100%' }}
                        placeholder="选择预案状态"
                        optionFilterProp="children"
                      >
                        <Option value="">全部</Option>
                        <Option value="1">可用</Option>
                        <Option value="2">不可用</Option>
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={6} sm={24}>
                  <FormItem label="操作时间">
                    {getFieldDecorator('createTime')(<RangePicker />)}
                  </FormItem>
                </Col>
                <Col md={6} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                      重置
                    </Button>

                    <Button
                      style={{ marginLeft: 80 }}
                      type="primary"
                      icon="download"
                      onClick={downloadTemplate}
                    >
                      下载模板
                    </Button>
                    <Upload
                      {...getFieldProps(
                        'file',
                        {
                          validate: [
                            {
                              rules: [{ type: 'array', required: true, message: '请添加数据文件' }],
                              trigger: 'onBlur',
                            },
                          ],
                        },
                        { valuePropName: 'fileIds' }
                      )}
                      {...props}
                      fileList={getFieldValue('file')}
                    >
                      <Button style={{ marginLeft: 8 }} type="primary" icon="upload">
                        导入
                      </Button>
                    </Upload>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={onAdd}>
              新建
            </Button>
          </div>
          <Table
            columns={columns}
            dataSource={plansList.list}
            rowKey={record => record.id}
            loading={loading}
            pagination={pagination}
          />
        </div>
      </Card>
      <AddEditModal {...AddEditModalProps} />
      <ShowFileModal {...AddEditModalProps} />
      <CardDestroyModallGen />
    </PageHeader>
  );
}

function mapStateToProps(state) {
  return {
    plansList: state.plansList,
    loading: state.loading.models.plansList,
  };
}

PlansList = Form.create()(PlansList);

export default connect(mapStateToProps)(PlansList);
