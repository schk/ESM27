import React from 'react';
import {
  Form,
  Input,
  Modal,
  Button,
  Radio,
  Row,
  Col,
  Select,
  Upload,
  Icon,
  message,
  TreeSelect,
  InputNumber,
  Popconfirm,
  DatePicker,
} from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import { baseUrl } from '../../../config/system';
import { getAuthority } from '../../../utils/authority';
const permissionList = getAuthority() != '' ? getAuthority().split(',') : [];
const RadioGroup = Radio.Group;
const Option = Select.Option;
const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    span: 5,
  },
  wrapperCol: {
    span: 18,
  },
};
const formItemLayout1 = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 14,
  },
};

let AddEditModal = ({ plansList, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: plansList.modalType == 'create' ? '新建预案' : '修改预案',
    visible: plansList.modalVisible,
    maskClosable: false,
    width: 1000,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={plansList.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!plansList.modalVisible) {
    resetFields();
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      var catalogIds = plansList.catalogIds;
      // if (catalogIds == null || catalogIds.length == 0) {
      //   message.error('请添加目录名称和起始页码');
      //   return;
      // }
      var formData = getFieldsValue();
      var plansCatalogs = [];
      if (catalogIds != null && catalogIds.length > 0) {
        for (var i = 0; i < catalogIds.length; i++) {
          let name = formData['name_' + catalogIds[i]];
          for (var j = i + 1; j < catalogIds.length; j++) {
            if (name == formData['name_' + catalogIds[j]]) {
              message.error('目录名称不能重复');
              return;
            }
          }
        }
        for (var i = 0; i < catalogIds.length; i++) {
          var catalog = {};
          catalog.name = formData['name_' + catalogIds[i]];
          catalog.page = formData['page_' + catalogIds[i]];
          catalog.sort = i + 1;
          plansCatalogs.push(catalog);
        }
      }
      formData.plansCatalogs = plansCatalogs;
      formData.id = plansList.modalType === 'create' ? '' : item.id;
      dispatch({
        type: `plansList/${plansList.modalType}`,
        payload: formData,
        search: { pageNum: plansList.current, pageSize: plansList.pageSize },
      });
    });
  }
  function handleCansel() {
    dispatch({
      type: 'plansList/updateState',
      payload: {
        modalVisible: false,
        fileList: [],
        catalogIds: [],
        catalogNames: {},
        uuid: 0,
      },
    });
  }
  //根据储备点取重点单位名称
  const changeRadio = e => {
    form.setFieldsValue({ keyUnitId: undefined });
    dispatch({
      type: 'plansList/updateState',
      payload: { type: e.target.value },
    });
  };
  const uploadProps = {
    action: baseUrl + '/common/uploadFileSwf.jhtml',
    withCredentials: true,
    listType: 'text',
    //accept: "application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.wordprocessingml.document,'application/msword,application/pdf",
    onChange: handleChange,
    onRemove: handleRemove,
  };

  function handleRemove() {
    setFieldsValue({ fileName: "" });
    dispatch({
      type: 'plansList/updateState',
      payload: {
        fileList: [],
        catalogIds: [],
      },
    });
  }

  function handleChange(info) {
    let fileList = info.fileList;
    if (info.file.status === 'done') {
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
        setFieldsValue({
          path: info.file.response.data.fid,
          swfPath: info.file.response.data.swfUrl,
        });
      } else {
        message.error(info.file.response.msg);
      }
      fileList = fileList.filter(file => {
        return true;
      });
      //只保留最后一条记录
      fileList = fileList.slice(-1);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    } else if (info.file.status === undefined) {
      return;
    }

    dispatch({
      type: 'plansList/updateState',
      payload: {
        fileList: fileList,
        catalogIds: [],
      },
    });
    setFieldsValue({ fileName: fileList[0].name });
  }

  const showUnit = data => data.map(per=>{
	  if(per==='keyParts'){
		  return <Radio value={'3'}>重点部位</Radio>
	  }
  });
  
  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  // const loopOptionKeyParts = data =>
  //   data.map(item => {
  //     return <Option key={item.id_}>{item.name}</Option>;
  //   });
  const treeData = i => {
    return i.map(d => {
      d.title = d.name;
      d.key = d.id_ + '';
      d.value = d.id_ + '';
      d.children = d.children && d.children.length > 0 ? treeData(d.children) : [];
      return d;
    });
  };
  function initCatalogs(values) {
    return values != null && values.length > 0
      ? values.map((k, index) => {
          return (
            <Row key={'page' + k}>
              <Col span={11}>
                <FormItem {...formItemLayout1} label="目录名称" hasFeedback>
                  {getFieldDecorator(`name_${k}`, {
                    initialValue: plansList.catalogNames['name_' + k],
                    rules: [
                      { required: true, whitespace: true, message: '请输入目录名称' },
                      { max: 50, message: '最长不超过50个字符' },
                    ],
                  })(<Input />)}
                </FormItem>
              </Col>
              <Col span={11}>
                <FormItem {...formItemLayout1} label="起始页码" hasFeedback>
                  {getFieldDecorator(`page_${k}`, {
                    initialValue: plansList.catalogNames['page_' + k],
                    rules: [{ required: true, message: '请输入起始页码' }],
                  })(<InputNumber min={1} max={99999999} style={{ width: '80%' }} />)}
                </FormItem>
              </Col>
              <Col span={2}>
                <Popconfirm title="确定要删除吗？" onConfirm={onRemoveClg.bind(this, index)}>
                  <Icon
                    className="dynamic-delete-button"
                    type="minus-circle-o"
                    style={{ cursor: 'pointer' }}
                  />
                </Popconfirm>
              </Col>
            </Row>
          );
        })
      : null;
  }
  function onAdd() {
    var caseMore = plansList.catalogIds;
    var newUuid = plansList.uuid;
    newUuid++;
    caseMore.push(newUuid);
    dispatch({
      type: 'plansList/updateState',
      payload: {
        catalogIds: caseMore,
        uuid: newUuid,
      },
    });
  }
  function onRemoveClg(index) {
    var caseMore = plansList.catalogIds;
    caseMore.splice(index, 1);
    dispatch({
      type: 'plansList/updateState',
      payload: {
        catalogIds: caseMore,
      },
    });
  }

  console.log(plansList.fileList);

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <FormItem style={{ marginBottom: '0px' }}>
          {getFieldDecorator('path', {
            initialValue: item.path,
          })(<Input type="hidden" />)}
        </FormItem>
        <FormItem style={{ marginBottom: '0px' }}>
          {getFieldDecorator('swfPath', {
            initialValue: item.swfPath,
          })(<Input type="hidden" />)}
        </FormItem>
        <Row>
          <Col span={12}>
            <FormItem label="预案名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '名称未填写' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写名称" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="预案类型:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('dicId', {
                initialValue: item.dicId ? item.dicId + '' : undefined,
                rules: [{ required: true, message: '请选择预案类型' }],
              })(
                <Select
                  showSearch
                  style={{ width: '100%' }}
                  placeholder="选择预案类型"
                  optionFilterProp="children"
                >
                  {loopOption(plansList.typeList)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="所属单位" {...formItemLayout}>
              {getFieldDecorator('type', {
                initialValue: item.type == null ? '4' : item.type,
              })(
                <RadioGroup style={{ width: '100%' }} onChange={changeRadio}>
                  <Radio value={'4'}>无</Radio>
                  <Radio value={'1'}>重点单位</Radio>
                  <Radio value={'2'}>消防队</Radio>
                  {showUnit(permissionList)}
                </RadioGroup>
              )}
            </FormItem>
          </Col>
          {plansList.type == 1 ? (
            <Col span={12}>
              <FormItem label="所属单位:" hasFeedback {...formItemLayout}>
                {getFieldDecorator('keyUnitId', {
                  initialValue: item.keyUnitId == null ? undefined : item.keyUnitId + '',
                  rules: [{ required: true, message: '请选择所属单位' }],
                })(
                  <Select showSearch optionFilterProp="children" placeholder="请选择所属单位">
                    {loopOption(plansList.keyUnitList)}
                  </Select>
                )}
              </FormItem>
            </Col>
          ) : plansList.type == 2 ? (
            <Col span={12}>
              <FormItem label="所属消防队" {...formItemLayout} hasFeedback>
                {getFieldDecorator('keyUnitId', {
                  initialValue:
                    item.keyUnitId != null ? item.keyUnitId + '' : undefined,
                    rules: [{ required: true, message: '请选择消防队' }],
                })(
                  <TreeSelect
                    showSearch
                    treeNodeFilterProp="title"
                    dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                    placeholder="请选择消防队"
                    notFoundContent="无匹配结果"
                    allowClear
                    treeData={treeData(plansList.fireBrigadeTree)}
                    treeDefaultExpandAll
                  />
                )}
              </FormItem>
            </Col>
          ) : plansList.type == 3 ?(
            <Col span={12}>
              <FormItem label="重点部位" {...formItemLayout} hasFeedback>
                {getFieldDecorator('keyUnitId', {
                  initialValue: item.keyUnitId != null ? item.keyUnitId + '' : undefined,
                  rules: [{ required: true, message: '请选择重点部位' }],
                })(
                  <Select placeholder="请选择重点部位">{loopOption(plansList.keyPartsList)}</Select>
                )}
              </FormItem>
            </Col>
          ):""}
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="编写人:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('writer', {
                initialValue: item.writer,
                rules: [
                  { required: true, message: '编写人未填写' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写编写人" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="编写单位" {...formItemLayout} hasFeedback>
              {getFieldDecorator('writingUnit', {
                initialValue:
                  item.writingUnit != null ? item.writingUnit + '' : plansList.brigadeId + '',
              })(
                <TreeSelect
                  showSearch
                  treeNodeFilterProp="title"
                  dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                  placeholder="请选择编写单位"
                  notFoundContent="无匹配结果"
                  allowClear
                  treeData={treeData(plansList.fireBrigadeTree)}
                  treeDefaultExpandAll
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="编写时间:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('writingTime', {
                initialValue: item.writingTime ? moment(item.writingTime) : undefined,
                rules: [{ required: true, message: '请选择编写时间' }],
              })(
                <DatePicker
                  style={{ width: '100%' }}
                  placeholder="请选择编写时间"
                  format={'YYYY-MM-DD'}
                />
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="预案级别:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('level', {
                initialValue: item.level ? item.level + '' : undefined,
                rules: [{ required: true, message: '请选择预案级别' }],
              })(
                <Select
                  style={{ width: '100%' }}
                  placeholder="选择预案级别"
                  optionFilterProp="children"
                >
                  <Option value="1">Ⅰ级</Option>
                  <Option value="2">Ⅱ级</Option>
                  <Option value="3">Ⅲ级</Option>
                  <Option value="4">Ⅳ级</Option>
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="预案编号:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('code', {
                initialValue: item.code,
                rules: [
                  { required: true, message: '请填写预案编号' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写预案编号" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="预案状态:" {...formItemLayout}>
              {getFieldDecorator('status', {
                initialValue: item.status == null ? '1' : item.status + '',
                rules: [{ required: true, message: '请选择预案状态' }],
              })(
                <RadioGroup style={{ width: '100%' }}>
                  <Radio value={'1'}>可用</Radio>
                  <Radio value={'2'}>不可用</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem {...formItemLayout} label="总体预案">
              {getFieldDecorator('fileList', {
                initialValue: plansList.fileList.length > 0 ? plansList.fileList : undefined,
                rules: [{ required: true, message: '请上传预案文件' }],
              })(
                <Upload {...uploadProps} fileList={plansList.fileList}>
                  <Button>
                    <Icon type="upload" /> 上传总体预案文件
                  </Button>
                </Upload>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem {...formItemLayout} label="文件名称">
              {getFieldDecorator('fileName', {
                initialValue: item.fileName,
              })(<Input placeholder="请上传预案文件" />)}
            </FormItem>
          </Col>
        </Row>
        {initCatalogs(plansList.catalogIds)}
        <Row>
          <Col span={12}>&nbsp;</Col>
          <Col span={12} style={{ textAlign: 'right' }}>
            <Button icon="plus" type="primary" onClick={onAdd}>
              新增
            </Button>
          </Col>
        </Row>
        <br />
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { plansList: state.plansList };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
