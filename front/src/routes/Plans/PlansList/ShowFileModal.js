import React from 'react';
import {
  Form,
  Input,
  Modal,
  Button,
  Radio,
  Row,
  Col,
  Select,
  InputNumber,
  TreeSelect,
  DatePicker,
} from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import { baseUrl } from '../../../config/system';
const RadioGroup = Radio.Group;
const Option = Select.Option;
const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

let ShowFileModal = ({ plansList, item = {}, form, dispatch }) => {
  const { getFieldDecorator, validateFields, getFieldsValue, resetFields } = form;
  const formItemLayout1 = {
    labelCol: {
      span: 10,
    },
    wrapperCol: {
      span: 14,
    },
  };
  const modalOpts = {
    title: '查看信息',
    visible: plansList.findModalVisible,
    maskClosable: false,
    width: 600,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
    ],
  };
  if (!plansList.findModalVisible) {
    resetFields();
  }

  if (!plansList.modalVisible) {
    resetFields();
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = getFieldsValue();
      data.id = plansList.modalType === 'create' ? '' : item.id;
      dispatch({
        type: `plansList/${plansList.modalType}`,
        payload: data,
        search: { pageNum: plansList.current, pageSize: plansList.pageSize },
      });
    });
  }

  function handleCansel() {
    dispatch({
      type: 'plansList/updateState',
      payload: {
        findModalVisible: false,
        fileList: [],
      },
    });
  }

  const uploadProps = {
    action: baseUrl + '/common/uploadFile.jhtml',
    withCredentials: true,
    listType: 'text',
  };

  function initCatalogs(values) {
    return values != null && values.length > 0
      ? values.map((k, index) => {
          return (
            <Row key={'page' + k}>
              <Col span={11}>
                <FormItem {...formItemLayout1} label="目录名称" hasFeedback>
                  {getFieldDecorator(`name_${k}`, {
                    initialValue: plansList.catalogNames['name_' + k],
                    rules: [
                      { required: true, whitespace: true, message: '请输入目录名称' },
                      { max: 50, message: '最长不超过50个字符' },
                    ],
                  })(<Input />)}
                </FormItem>
              </Col>
              <Col span={11}>
                <FormItem {...formItemLayout1} label="起始页码" hasFeedback>
                  {getFieldDecorator(`page_${k}`, {
                    initialValue: plansList.catalogNames['page_' + k],
                    rules: [{ required: true, message: '请输入起始页码' }],
                  })(<InputNumber min={1} max={99999999} style={{ width: '80%' }} />)}
                </FormItem>
              </Col>
            </Row>
          );
        })
      : null;
  }

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  const treeData = i => {
    return i.map(d => {
      d.label = d.name;
      d.key = d.id_ + '';
      d.value = d.id_ + '';
      d.children = d.children && d.children.length > 0 ? treeData(d.children) : [];
      return d;
    });
  };

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <FormItem style={{ marginBottom: '0px' }}>
          {getFieldDecorator('fileName', {
            initialValue: item.fileName,
          })(<Input type="hidden" />)}
        </FormItem>
        <FormItem style={{ marginBottom: '0px' }}>
          {getFieldDecorator('path', {
            initialValue: item.path,
          })(<Input type="hidden" />)}
        </FormItem>

        <Row>
          <Col span={24}>
            <FormItem label="预案名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
              })(<Input type="text" readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="所属单位" hasFeedback {...formItemLayout}>
              {getFieldDecorator('type', {
                initialValue: item.type == null ? '1' : item.type + '',
              })(
                <RadioGroup style={{ width: '322px' }}>
                  <Radio value={'1'}>重点单位</Radio>
                  <Radio value={'2'}>消防队</Radio>
                  <Radio value={'3'}>无</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
        </Row>

        {plansList.type == 1 || plansList.type == 2 ? (
          <Row>
            <Col span={24}>
              <FormItem label="所属单位:" hasFeedback {...formItemLayout}>
                {getFieldDecorator('keyUnitName', {
                  initialValue: item.keyUnitName == null ? undefined : item.keyUnitName + '',
                })(<Input type="text" readOnly />)}
              </FormItem>
            </Col>
          </Row>
        ) : (
          ''
        )}

        <Row>
          <Col span={24}>
            <FormItem label="预案类型:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('typeName', {
                initialValue: item.typeName ? item.typeName + '' : undefined,
              })(<Input type="text" readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Col span={24}>
          <FormItem label="预案编号:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('code', {
              initialValue: item.code,
            })(<Input type="text" readOnly />)}
          </FormItem>
        </Col>
        <Row>
          <Col span={24}>
            <FormItem label="编写人:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('writer', {
                initialValue: item.writer,
              })(<Input type="text" readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="编写单位:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('writingUnit', {
                initialValue: item.writingUnit,
              })(<Input type="text" readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="编写时间:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('writingTime', {
                initialValue: item.writingTime ? moment(item.writingTime) : undefined,
              })(
                <DatePicker
                  readOnly
                  style={{ width: '100%' }}
                  placeholder="请选择编写时间"
                  format={'YYYY-MM-DD'}
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="预案级别:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('levelName', {
                initialValue:
                  item.level == 1
                    ? 'Ⅰ级'
                    : item.level == 2 ? 'Ⅱ级' : item.level == 3 ? 'Ⅲ级' : 'Ⅳ级',
              })(<Input type="text" readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem {...formItemLayout} label="文件名称">
              {getFieldDecorator('fileName', {
                initialValue: item.fileName,
              })(<Input />)}
            </FormItem>
          </Col>
        </Row>
        {initCatalogs(plansList.catalogIds)}
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { plansList: state.plansList };
}

ShowFileModal = Form.create()(ShowFileModal);

export default connect(mapStateToProps)(ShowFileModal);
