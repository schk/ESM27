import React from 'react';
import { Form, Input, Modal, Button, Row, Col, TreeSelect } from 'antd';
import { connect } from 'dva';
const { TextArea } = Input;
const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

let AddEditModal = ({ plansType, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: plansType.modalType == 'create' ? '新建部门' : '修改部门',
    visible: plansType.modalVisible,
    maskClosable: false,
    width: 600,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={plansType.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!plansType.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'plansType/updateState',
      payload: {
        modalVisible: false,
      },
    });
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = getFieldsValue();
      data.id = plansType.modalType === 'create' ? '' : item.id;
      data.type = 3;
      dispatch({
        type: `plansType/${plansType.modalType}`,
        payload: data,
      });
    });
  }

  const loop = data =>
    data.map(d => {
      let children = null;
      if (d.children != null && d.children.length > 0) {
        children = loop(d.children);
      }
      return { label: d.name, value: d.id_, key: d.id_, children, disabled: item.id_ === d.id_ };
    });

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={24}>
            <FormItem label="名称：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '请输入预案名称' },
                  { max: 50, message: '最长不超过50个字符' },
                ],
              })(<Input type="text" placeholder="请输入预案名称" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="编码：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('code', {
                initialValue: item.code,
                rules: [
                  { required: true, message: '请输入编码' },
                  { max: 20, message: '最长不超过20个字符' },
                ],
              })(<Input type="text" placeholder="请输入编码" />)}
            </FormItem>
          </Col>
        </Row>

        <Row>
          <Col span={24}>
            <FormItem label="描述：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('remark', {
                initialValue: item.remark,
                rules: [{ max: 100, message: '最长不超过100个字符' }],
              })(<TextArea autosize={{ minRows: 4 }} />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { plansType: state.plansType };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
