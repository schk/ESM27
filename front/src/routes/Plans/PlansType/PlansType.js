import { Table, Form, Button, Popconfirm, Card, Divider } from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import PageHeader from '../../../layouts/PageHeaderLayout';

function PlansType({ location, plansType, form, dispatch, loading }) {
  const AddEditModalProps = {
    item: plansType.item,
  };

  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    { title: '名称', dataIndex: 'name', key: 'name', width: 100 },
    { title: '编码', dataIndex: 'code', key: 'code', width: 100 },
    { title: '描述', dataIndex: 'remark', key: 'remark', width: 100 },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 60,
      render: (text, record) => (
        <span>
          <a onClick={() => onUpdate(record)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </span>
      ),
    },
  ];

  function onUpdate(record) {
    dispatch({
      type: 'plansType/info',
      payload: record,
    });
  }

  function onAdd() {
    dispatch({
      type: 'plansType/updateState',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'plansType/del',
      payload: id,
    });
  }

  return (
    <PageHeader>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={onAdd}>
              新建
            </Button>
          </div>
          <Table
            columns={columns}
            dataSource={plansType.list}
            rowKey={record => record.id}
            loading={loading}
            pagination={false}
          />
        </div>
      </Card>
      <AddEditModal {...AddEditModalProps} />
    </PageHeader>
  );
}

function mapStateToProps(state) {
  return {
    plansType: state.plansType,
    loading: state.loading.models.plansType,
  };
}

PlansType = Form.create()(PlansType);

export default connect(mapStateToProps)(PlansType);
