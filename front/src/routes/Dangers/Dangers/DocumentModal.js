import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, Form, Input, Radio, TreeSelect, Select, Button } from 'antd';
const TreeNode = TreeSelect.TreeNode;
const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const Option = Select.Option;
import { baseFileUrl } from '../../../config/system';
var filePath_;
class DocumentModal extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    setTimeout(function() {
      var fp = new FlexPaperViewer(baseFileUrl + '/upload/flexPaper/FlexPaperViewer', 'viewerPlaceHolder', {
        config: {
          SwfFile: escape(baseFileUrl + filePath_),
          Scale: 0.9,
          ZoomTransition: 'easeOut',
          ZoomTime: 0.5,
          ZoomInterval: 0.2,
          FitPageOnLoad: true,
          FitWidthOnLoad: false,
          PrintEnabled: true,
          FullScreenAsMaxWindow: false,
          ProgressiveLoading: true,
          MinZoomSize: 0.3,
          MaxZoomSize: 5,
          SearchMatchAll: false,
          InitViewMode: 'Portrait',
          ViewModeToolsVisible: true,
          ZoomToolsVisible: true,
          NavToolsVisible: true,
          CursorToolsVisible: true,
          SearchToolsVisible: true,
          localeChain: 'zh_CN',
        },
      });
    }, 100);
  }

  render() {
    filePath_ = this.props.filePath;
    console.log(filePath_);
    const { children, modalVisible, filePath } = this.props;
    const { getFieldDecorator, getFieldsValue } = this.props.form;
    var dispatch = this.props.dispatch;

    const onCancel = () => {
      dispatch({
        type: 'dangers/updateState',
        payload: {
          documnetModalVisible: false,
          filePath: null,
        },
      });
    };

    return (
      <span>
        <Modal
          maskClosable={this.props.maskClosable}
          confirmLoading={this.props.confirmLoading}
          width={1200}
          title={this.props.title}
          visible={this.props.visible}
          footer={[
            <Button key="close" type="primary" size="large" onClick={onCancel.bind(this)}>
              关闭
            </Button>,
          ]}
          onCancel={onCancel.bind(this)}
          key={this.props.key}
        >
          <div
            style={{
              width: 'auto',
              height: document.body.clientHeight - 280,
              position: 'relative',
              backgroundColor: '#fff',
            }}
          >
            <a id="viewerPlaceHolder" style={{ width: '1000px', height: '660px' }} />
          </div>
        </Modal>
      </span>
    );
  }
}

export default Form.create()(DocumentModal);
