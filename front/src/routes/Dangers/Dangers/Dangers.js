import React, { Fragment } from 'react';
import { connect } from 'dva';
import {
  Select,
  Form,
  Input,
  Button,
  Row,
  Col,
  Table,
  Popconfirm,
  Tree,
  Divider,
  Card,
} from 'antd';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import ShowFileModal from './ShowFileModal';
import DocumentModal from './DocumentModal';
const TreeNode = Tree.TreeNode;

function Dangers({ location, dispatch, dangers, form }) {
  const { getFieldDecorator, validateFields, getFieldsValue, resetFields } = form;
  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    {
      title: '中文名称',
      dataIndex: 'cName',
      key: 'cName',
      width: 90,
      render: (text, record, value) => (
        <Fragment>
          <a onClick={() => onFindInfo(record.id_)}>{record.cName}</a>
        </Fragment>
      ),
    },
    {
      title: '英文名',
      dataIndex: 'eName',
      key: 'eName',
      width: 100,
    },
    {
      title: '类别',
      dataIndex: 'typeName',
      key: 'typeName',
      width: 100,
    },
    {
      title: '项目',
      dataIndex: 'project',
      key: 'project',
      width: 120,
    },
    {
      title: 'CAS号',
      dataIndex: 'casNum',
      key: 'casNum',
      width: 100,
    },
    {
      title: 'UN号',
      dataIndex: 'unNum',
      key: 'unNum',
      width: 100,
    },
    {
      title: '操作',
      key: 'operation',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onEdit(record.id_)}>编辑</a>
          <Divider type="vertical" />
          <Popconfirm title="确定删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
          {record.swfPath ? (
            <span>
              <Divider type="vertical" />
              <a onClick={() => onShowDocument(record.swfPath)}>预览</a>
            </span>
          ) : (
            ''
          )}
        </Fragment>
      ),
      width: 100,
    },
  ];
  const onFindInfo = id => {
    dispatch({
      type: 'dangers/findInfo',
      payload: id,
    });
  };
  const loop = data =>
    data.map(item => {
      if (item.children && item.children.length) {
        return (
          <TreeNode key={item.id_} title={item.name}>
            {loop(item.children)}
          </TreeNode>
        );
      }
      return <TreeNode key={item.id_} title={item.name} />;
    });

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'dangers/qryListByParams',
      payload: { pageNum: 1, pageSize: dangers.pageSize },
    });
  }

  const onAdd = () => {
    dispatch({
      type: 'dangers/showModal',
      payload: {
        modalType: 'create',
        fileList: [],
        item: {},
        catalogIds: [],
        catalogNames: {},
      },
    });
  };

  function onShowDocument(path) {
    dispatch({
      type: 'dangers/getDocumnet',
      payload: {
        documnetModalVisible: true,
        filePath: path,
        newKey: new Date().getTime() + '',
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'dangers/remove',
      payload: id,
      search: {
        typeId: dangers.typeId,
        pageNum: dangers.current,
        pageSize: dangers.pageSize,
        ...getFieldsValue(),
      },
    });
  }

  const onEdit = id => {
    dispatch({
      type: 'dangers/info',
      payload: id,
    });
  };

  const submitHandle = () => {
    dispatch({
      type: 'dangers/qryListByParams',
      payload: { ...getFieldsValue() },
    });
  };

  const pagination = {
    current: dangers.current,
    pageSize: dangers.pageSize,
    total: dangers.total,
    showSizeChanger: true,
    showTotal: total => '共' + dangers.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'dangers/qryListByParams',
        payload: { pageNum: current, pageSize: size, ...getFieldsValue() },
      });
    },
    onChange(current) {
      dispatch({
        type: 'dangers/qryListByParams',
        payload: { pageNum: current, pageSize: dangers.pageSize, ...getFieldsValue() },
      });
    },
  };
  const showModalProps = {
    item: dangers.currentItem,
    dangerTypeNoTopTree: dangers.dangerTypeNoTopTree,
    visible: dangers.findModalVisible,
    title: `查看详情`,
    activeKey: dangers.activeKey,
    typeId: dangers.typeId == 1 ? undefined : dangers.typeId,
    maskClosable: false,
    dispatch: dispatch,
    fileList: dangers.fileList,
    newKey: dangers.newKey,
    confirmLoading: dangers.buttomLoading,
    wrapClassName: 'vertical-center-modal',
    catalogIds: dangers.catalogIds,
    uuid: dangers.uuid,
    catalogNames: dangers.catalogNames,
    onCancel: handleCansel,
    imgPath:dangers.imgPath,
  };

  const modalProps = {
    buttomLoading: dangers.buttomLoading,
    item: dangers.modalType === 'create' ? {} : dangers.currentItem,
    dangerTypeNoTopTree: dangers.dangerTypeNoTopTree,
    visible: dangers.modalVisible,
    type: dangers.modalType,
    activeKey: dangers.activeKey,
    typeId: dangers.typeId == 1 ? undefined : dangers.typeId,
    maskClosable: false,
    dispatch: dispatch,
    fileList: dangers.fileList,
    newKey: dangers.newKey,
    confirmLoading: dangers.buttomLoading,
    title: `${dangers.modalType === 'create' ? '新增危化品库' : '更新危化品库'}`,
    wrapClassName: 'vertical-center-modal',
    catalogIds: dangers.catalogIds,
    uuid: dangers.uuid,
    catalogNames: dangers.catalogNames,
    imgPath:dangers.imgPath,
    onOk(data) {
      dispatch({
        type: `dangers/${dangers.modalType}`,
        payload: data,
        search: {
          typeId: dangers.typeId,
          pageNum: dangers.current,
          pageSize: dangers.pageSize,
          ...getFieldsValue(),
        },
      });
    },
    onCancel() {
      dispatch({
        type: 'dangers/hideModal',
        payload: {
          modalVisible: false,
        },
      });
      dispatch({
        type: 'dangers/updateState',
        payload: {
          fileList: [],
          catalogIds: [],
          catalogNames: {},
          uuid: 0,
          imgPath:'',
        },
      });
    },
  };

  const onSelect = info => {
    if (info.length > 0) {
      dispatch({
        type: 'dangers/qryListByParams',
        payload: { ...getFieldsValue(), ...{ typeId: info[0] } },
      });
    } else {
      dispatch({
        type: 'dangers/qryListByParams',
        payload: { ...getFieldsValue(), ...{ typeId: null } },
      });
    }
  };
  function handleCansel() {
    dispatch({
      type: 'dangers/updateState',
      payload: {
        findModalVisible: false,
        fileList: [],
      },
    });
  }
  function handleTableChange(pagination, filters, sorter) {
    let isChinese = ['account', 'name'].indexOf(sorter.field) >= 0;
    dispatch({
      type: 'dangers/qryListByParams',
      payload: {
        pageNum: pagination.current,
        pageSize: pagination.pageSize,
        ...getFieldsValue(),
        orderBy: sorter.field,
        order: sorter.order,
        isChinese,
      },
    });
  }

  const DocumentModallGen = () => <DocumentModal {...DocumentModalProps} />;
  const DocumentModalProps = {
    filePath: dangers.filePath,
    visible: dangers.documnetModalVisible,
    title: '查看文档',
    dispatch: dispatch,
  };

  return (
    <PageHeaderLayout>
      <div style={{ backgroundColor: '#fff', position: 'relative' }}>
        <div style={{ width: '275px', float: 'left', position: 'absolute', bottom: '0', top: '0' }}>
          <div
            style={{
              width: '100%',
              height: '40px',
              backgroundColor: '#232f40',
              lineHeight: '40px',
              color: '#fff',
              paddingLeft: '20px',
              fontSize: '14px',
              fontWeight: '400',
            }}
          >
            危化品类型树
          </div>
          <div
            style={{
              position: 'absolute',
              top: '40px',
              bottom: '0px',
              overflow: 'hidden',
              overflowY: 'auto',
              width: '100%',
            }}
          >
            <Tree defaultExpandedKeys={['1']} onSelect={onSelect}>
              {dangers.dangerTypeTree && loop(dangers.dangerTypeTree)}
            </Tree>
          </div>
        </div>
        <div
          style={{
            marginLeft: '275px',
            paddingTop: '0px',
            minHeight: '600px',
            borderLeft: '1px solid #e8e8e8',
          }}
        >
          <Card bordered={false}>
            <div className={styles.tableList}>
              <div className={styles.tableListForm}>
                <Form layout="inline">
                  <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                    <Col span={5} sm={6}>
                      <Form.Item label="危化品名称：">
                        {getFieldDecorator('cName')(<Input placeholder="输入危化品名称查找" />)}
                      </Form.Item>
                    </Col>
                    <Col span={4} sm={5}>
                      <Form.Item label="CAS号：">
                        {getFieldDecorator('casNum')(<Input placeholder="输入CAS号查找" />)}
                      </Form.Item>
                    </Col>
                    <Col span={4} sm={5}>
                      <Form.Item label="UN号：">
                        {getFieldDecorator('unNum')(<Input placeholder="输入UN号查找" />)}
                      </Form.Item>
                    </Col>
                    <Col md={4} sm={5}>
                      <span className={styles.submitButtons}>
                        <Button type="primary" htmlType="submit" onClick={submitHandle}>
                          查询
                        </Button>
                        <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                          重置
                        </Button>
                        <Button
                          style={{ marginLeft: 8 }}
                          icon="plus"
                          type="primary"
                          onClick={onAdd}
                        >
                          新增
                        </Button>
                      </span>
                    </Col>
                  </Row>
                </Form>
              </div>
              {/* <div className={styles.submitButtons}>
                <Button icon="plus" type="primary" onClick={onAdd}>
                  新增
                </Button>
              </div> */}
              <div style={{ width: '100%' }}>
                <Table
                  columns={columns}
                  dataSource={dangers.list}
                  style={{ marginTop: 10 }}
                  pagination={pagination}
                  loading={dangers.loading}
                  rowKey={record => record.id_}
                  onChange={handleTableChange}
                />
              </div>
            </div>
          </Card>
        </div>
      </div>
      <AddEditModal {...modalProps} />
      <ShowFileModal {...showModalProps} />
      <DocumentModallGen />
    </PageHeaderLayout>
  );
}

function mapStateToProps(state) {
  return {
    dangers: state.dangers,
  };
}

Dangers = Form.create()(Dangers);

export default connect(mapStateToProps)(Dangers);
