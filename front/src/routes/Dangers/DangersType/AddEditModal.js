import React from 'react';
import { Form, Input, Modal, Button, Row, Col, TreeSelect } from 'antd';
import { connect } from 'dva';
const { TextArea } = Input;
const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

let AddEditModal = ({ dangersType, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: dangersType.modalType == 'create' ? '新建危化品类型' : '修改危化品类型',
    visible: dangersType.modalVisible,
    maskClosable: false,
    width: 600,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={dangersType.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!dangersType.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'dangersType/updateState',
      payload: {
        modalVisible: false,
      },
    });
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = getFieldsValue();
      data.id = dangersType.modalType === 'create' ? '' : item.id;
      data.pid = data.pid ? data.pid : 1;
      dispatch({
        type: `dangersType/${dangersType.modalType}`,
        payload: data,
      });
    });
  }

  const loop = data =>
    data.map(d => {
      let children = null;
      if (d.children != null && d.children.length > 0) {
        children = loop(d.children);
      }
      return { label: d.name, value: d.id_, key: d.id_, children, disabled: item.id_ === d.id_ };
    });

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={24}>
            <FormItem label="名称：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '姓名未填写' },
                  { max: 50, message: '最长不超过50个字符' },
                ],
              })(<Input type="text" placeholder="请填写姓名" />)}
            </FormItem>
          </Col>
        </Row>

        <Row>
          <Col span={24}>
            <FormItem label="上级类型：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('pid', {
                initialValue: item.pid != null && item.pid != 1 ? item.pid + '' : undefined,
              })(
                <TreeSelect
                  allowClear
                  placeholder="请选择上级类型"
                  showCheckedStrategy={'SHOW_CHILD'}
                  treeData={loop(dangersType.list)}
                />
              )}
            </FormItem>
          </Col>
        </Row>

        <Row>
          <Col span={24}>
            <FormItem label="编码：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('code', {
                initialValue: item.code,
                rules: [
                  { required: true, message: '请输入编码' },
                  { max: 20, message: '最长不超过20个字符' },
                ],
              })(<Input type="text" placeholder="请输入编码" />)}
            </FormItem>
          </Col>
        </Row>

        <Row>
          <Col span={24}>
            <FormItem label="描述：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('remark', {
                initialValue: item.remark,
                rules: [{ max: 100, message: '最长不超过100个字符' }],
              })(<TextArea autosize={{ minRows: 4 }} />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { dangersType: state.dangersType };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
