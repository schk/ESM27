import React, { PureComponent, Fragment } from 'react';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Table,
  Popconfirm,
} from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from '../../common/common.less';
import LogModal from './LogModal';
const { Option } = Select;
const FormItem = Form.Item;
const RangePicker = DatePicker.RangePicker;

function Log({ location, log, form, dispatch, loading }) {
  const { getFieldDecorator, validateFields, getFieldsValue, setFieldsValue, resetFields } = form;

  const AddEditModalProps = {
    item: log.item,
  };

  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 100,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    { title: '操作用户', dataIndex: 'userName', key: 'userName', sorter: true },
    { title: '真实姓名', dataIndex: 'actName', key: 'actName', sorter: true },
    { title: '操作类型', dataIndex: 'operateType', key: 'operateType' },
    { title: '模块', dataIndex: 'module', key: 'module' },
    { title: '描述', dataIndex: 'description', key: 'description' },
    { title: 'IP地址', dataIndex: 'ip', key: 'ip' },
    {
      title: '操作时间',
      dataIndex: 'operateTime',
      key: 'operateTime',
      sorter: true,
      render: (value, row, index) => {
        return value ? new moment(value).format('YYYY-MM-DD HH:mm') : '';
      },
    },
  ];

  function onDelete(id) {
    dispatch({
      type: 'log/del',
      payload: id,
    });
  }

  const pagination = {
    current: log.current,
    pageSize: log.pageSize,
    total: log.total,
    showSizeChanger: true,
    showTotal: total => '共' + log.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'log/qryListByParam',
        payload: { pageNum: current, pageSize: size, ...getFieldsValue() },
      });
    },
    onChange(current) {
      dispatch({
        type: 'log/qryListByParam',
        payload: { pageNum: current, pageSize: log.pageSize, ...getFieldsValue() },
      });
    },
  };

  function handleSearch() {
    dispatch({
      type: 'log/qryListByParam',
      payload: { pageNum: 1, pageSize: log.pageSize, ...getFieldsValue() },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'log/qryListByParam',
      payload: { currentPage: 1, pageSize: log.pageSize },
    });
  }

  const modalProps = {
    item: log.currentItem,
    visible: log.modalVisible,
    title: '查看参数',
    newKey: log.newKey,
    type: log.modalType,
    onCancel() {
      dispatch({
        type: 'log/updateState',
        payload: {
          modalVisible: false,
        },
      });
    },
  };

  function handleTableChange(pagination, filters, sorter) {
    let isChinese = ['account', 'operateType', 'module'].indexOf(sorter.field) >= 0;
    dispatch({
      type: 'log/qryListByParam',
      payload: {
        pageNum: pagination.current,
        pageSize: pagination.pageSize,
        ...getFieldsValue(),
        orderBy: sorter.field,
        order: sorter.order,
        isChinese,
      },
    });
  }

  return (
    <PageHeaderLayout>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={6} sm={24}>
                  <FormItem label="操作用户">
                    {getFieldDecorator('userName')(<Input placeholder="请输入操作用户查找" />)}
                  </FormItem>
                </Col>
                <Col md={6} sm={24}>
                  <FormItem label="操作类型">
                    {getFieldDecorator('operateType')(
                      <Select allowClear placeholder="请选择操作类型查找">
                        <Option value="登录">登录</Option>
                        <Option value="添加">添加</Option>
                        <Option value="更新">更新</Option>
                        <Option value="删除">删除</Option>
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={6} sm={24}>
                  <FormItem label="操作时间">
                    {getFieldDecorator('operateTime')(<RangePicker />)}
                  </FormItem>
                </Col>
                <Col md={6} sm={24} style={{ display: 'flex', flexDirection: 'row-reverse' }}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                      重置
                    </Button>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <Table
            loading={loading}
            rowKey={record => record.id}
            dataSource={log.list}
            columns={columns}
            pagination={pagination}
            onChange={handleTableChange}
          />
        </div>
      </Card>

      <LogModal {...modalProps} />
    </PageHeaderLayout>
  );
}

function mapStateToProps(state) {
  return {
    log: state.log,
    loading: state.loading.models.log,
  };
}

Log = Form.create()(Log);

export default connect(mapStateToProps)(Log);
