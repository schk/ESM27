import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, Form, Input, Button } from 'antd';
const FormItem = Form.Item;
const { TextArea } = Input;

class LogModal extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { dispatch, item } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <span>
        <Modal
          maskClosable={this.props.maskClosable}
          confirmLoading={this.props.confirmLoading}
          title={this.props.title}
          visible={this.props.visible}
          onCancel={this.props.onCancel}
          key={this.props.newKey}
          footer={[
            <Button key="back" size="large" onClick={this.props.onCancel}>
              关闭
            </Button>,
          ]}
        >
          <Form layout="horizontal">
            <FormItem>
              {getFieldDecorator('result', {
                initialValue: item.args,
              })(<TextArea autosize={{ minRows: 4 }} />)}
            </FormItem>
          </Form>
        </Modal>
      </span>
    );
  }
}

export default Form.create()(LogModal);
