import React, { Fragment } from 'react';
import { Table, Form, Button, Popconfirm, Card, Divider } from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import PageHeader from '../../../layouts/PageHeaderLayout';

function Department({ location, department, form, dispatch, loading }) {
  const AddEditModalProps = {
    item: department.item,
  };

  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    { title: '名称', dataIndex: 'name', key: 'name', width: 100 },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 60,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  function onUpdate(id) {
    dispatch({
      type: 'department/info',
      payload: id,
    });
  }

  function onAdd() {
    dispatch({
      type: 'department/updateState',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'department/del',
      payload: id,
    });
  }

  return (
    <PageHeader>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={onAdd}>
              新建
            </Button>
          </div>
          <Table
            columns={columns}
            dataSource={department.list}
            rowKey={record => record.id}
            loading={loading}
            pagination={false}
            defaultExpandAllRows={true}
          />
        </div>
      </Card>
      <AddEditModal {...AddEditModalProps} />
    </PageHeader>
  );
}

function mapStateToProps(state) {
  return {
    department: state.department,
    loading: state.loading.models.department,
  };
}

Department = Form.create()(Department);

export default connect(mapStateToProps)(Department);
