import React from 'react';
import { Form, Input, Modal, Button, Row, Col, TreeSelect } from 'antd';
import { connect } from 'dva';

const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

let AddEditModal = ({ department, item = {}, form, dispatch }) => {
  const { getFieldDecorator, validateFields, getFieldsValue, resetFields } = form;

  const modalOpts = {
    title: department.modalType == 'create' ? '新建机构' : '修改机构',
    visible: department.modalVisible,
    maskClosable: false,
    width: 600,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={department.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!department.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'department/updateState',
      payload: {
        modalVisible: false,
      },
    });
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = getFieldsValue();
      data.id = department.modalType === 'create' ? '' : item.id;
      data.pid = data.pid ? data.pid : -1;
      dispatch({
        type: `department/${department.modalType}`,
        payload: data,
      });
    });
  }

  const loop = data =>
    data.map(d => {
      let children = null;
      if (d.children != null && d.children.length > 0) {
        children = loop(d.children);
      }
      return { label: d.name, value: d.id_, key: d.id_, children, disabled: item.id_ === d.id_ };
    });

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={24} style={{ height: '56px' }}>
            <FormItem label="名称：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '姓名未填写' },
                  { max: 50, message: '最长不超过50个字符' },
                ],
              })(<Input type="text" placeholder="请填写姓名" />)}
            </FormItem>
          </Col>
        </Row>

        <Row>
          <Col span={24} style={{ height: '56px' }}>
            <FormItem label="上级机构：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('pid', {
                initialValue: item.pid != null && item.pid != -1 ? item.pid + '' : undefined,
              })(
                <TreeSelect
                  allowClear
                  placeholder="请选择上级机构"
                  showCheckedStrategy={'SHOW_CHILD'}
                  treeData={loop(department.list)}
                />
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { department: state.department };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
