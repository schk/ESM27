import React, { PropTypes } from 'react';
import { Form, Input, Modal, Select, Tree, TreeSelect } from 'antd';
import { connect } from 'dva';
const FormItem = Form.Item;
const TreeNode = Tree.TreeNode;
const Option = Select.Option;

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};

var RolePermission = ({
  role,
  roleName,
  dispatch,
  form,
  allPermission,
  rolePermissions,
  type,
  item = {},
  onOk,
  onCancel,
}) => {
  const { getFieldDecorator, validateFields, getFieldsValue } = form;
  const modalOpts = {
    title: '权限设置',
    key: role.newKey,
    visible: role.permissionVisible,
    maskClosable: false,
    confirmLoading: role.buttomLoading,
    onOk: handleOk,
    onCancel: handleCancel,
  };

  function handleCancel() {
    form.resetFields();
    onCancel();
  }

  function handleOk() {
    form.validateFields(errors => {
      if (errors) {
        return;
      }
      const data = { ...form.getFieldsValue(), id: role.item.id };
      onOk(data, form);
    });
  }

  const loop = data =>
    data.map(item => {
      if (item.children) {
        return (
          <TreeNode title={item.name} key={item.id_+""}>
            {loop(item.children)}
          </TreeNode>
        );
      }
      return <TreeNode title={item.name} key={item.id_+""} />;
    });

  //选择权限
  const handleSelect = (checkedKeys,info) => {
	let checkedKeysResult = [...checkedKeys, ...info.halfCheckedKeys];
    dispatch({
      type: 'role/updateState',
      payload: { selectPermissions: checkedKeysResult},
    });
  };

  return (
    <Modal {...modalOpts}>
      <div>
        <Form layout="horizontal">
          <FormItem label="角色名称：" hasFeedback {...formItemLayout}>
            {getFieldDecorator('roleName', { initialValue: role.item.name })(
              <Input disabled={true} type="text" />
            )}
          </FormItem>
          <FormItem label="权限：" {...formItemLayout}>
            <div style={{ height: 500, overflow: 'auto' }}>
              {getFieldDecorator('rolepermissions', { initialValue: role.rolePermissions })(
                <Tree
                  checkable
                  onCheck={handleSelect.bind(this)}
                  defaultCheckedKeys={role.rolePermissionsNoP}
                  defaultExpandedKeys={role.rolePermissions}
                >
                  {loop(role.allPermission)}
                </Tree>
              )}
            </div>
          </FormItem>
        </Form>
      </div>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { role: state.role };
}

RolePermission = Form.create()(RolePermission);

export default connect(mapStateToProps)(RolePermission);
