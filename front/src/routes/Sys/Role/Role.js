import React, { PureComponent, Fragment } from 'react';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Table,
  Popconfirm,
} from 'antd';
import { connect } from 'dva';
import AddEditModal from './AddEditModal';
import RolePermission from './RolePermission';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import styles from '../../../common/common.less';
const FormItem = Form.Item;

function Role({ location, role, form, dispatch, loading }) {
  const { getFieldDecorator, validateFields, getFieldsValue, setFieldsValue, resetFields } = form;

  const AddEditModalProps = {
    item: role.item,
  };

  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 100,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    {
      title: '角色',
      dataIndex: 'name',
      key: 'name',
      render: (text, record) => (
        <div style={{ wordWrap: 'break-word', wordBreak: 'break-all' }}>{text}</div>
      ),
    },
    {
      title: '权限',
      dataIndex: 'permissionName',
      key: 'permissionName',
      render: (text, record) => (
        <div style={{ wordWrap: 'break-word', wordBreak: 'break-all' }}>{text}</div>
      ),
    },
    {
      title: '备注',
      dataIndex: 'remark',
      key: 'remark',
      render: (text, record) => (
        <div style={{ wordWrap: 'break-word', wordBreak: 'break-all' }}>{text}</div>
      ),
    },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 200,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => setPermission(record.id_, record)}>权限设置</a>
          <Divider type="vertical" />
          <a onClick={() => onUpdate(record.id_)}>编辑</a>
          <Divider type="vertical" />
          <Popconfirm title="是否要删除该角色？" onConfirm={onDelete.bind(this, record.id)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  function onAdd() {
    dispatch({
      type: 'role/updateState',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
      },
    });
  }

  function onUpdate(id) {
    dispatch({
      type: 'role/info',
      payload: id,
      search: { pageNum: role.current, pageSize: role.pageSize, ...getFieldsValue() },
    });
  }
  function onDelete(id) {
    dispatch({
      type: 'role/del',
      payload: id,
      search: { pageNum: role.current, pageSize: role.pageSize, ...getFieldsValue() },
    });
  }

  function setPermission(id, item) {
    dispatch({
      type: 'role/qryRolePermission',
      payload: { id: id, item: item },
    });
   
  }

  function handleSearch() {
    dispatch({
      type: 'role/qryListByParams',
      payload: { pageNum: 1, pageSize: role.pageSize, ...getFieldsValue() },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'role/qryListByParams',
      payload: { pageNum: 1, pageSize: role.pageSize },
    });
  }

  const pagination = {
    current: role.current,
    pageSize: role.pageSize,
    total: role.total,
    showSizeChanger: true,
    showTotal: total => '共' + role.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'role/qryListByParams',
        payload: { pageNum: current, pageSize: size, ...getFieldsValue() },
      });
    },
    onChange(current) {
      dispatch({
        type: 'role/qryListByParams',
        payload: { pageNum: current, pageSize: role.pageSize, ...getFieldsValue() },
      });
    },
  };

  const rolePermissionProps = {
    item: role.modalType === 'create' ? {} : role.currentItem,
    visible: role.permissionVisible,
    permissions: role.allPermission,
    onOk(data, form) {
      dispatch({
        type: `role/setPermission`,
        payload: { permission: role.selectPermissions, id: data.id },
        form: form,
        search: { pageNum: role.current, pageSize: role.pageSize, ...getFieldsValue() },
      });
    },
    onCancel() {
      dispatch({
        type: 'role/hidePermissionModal',
      });
    },
  };

  function handleTableChange(pagination, filters, sorter) {
    let isChinese = ['roleName'].indexOf(sorter.field) >= 0;
    dispatch({
      type: 'role/qryListByParams',
      payload: {
        pageNum: pagination.current,
        pageSize: pagination.pageSize,
        ...getFieldsValue(),
        orderBy: sorter.field,
        order: sorter.order,
        isChinese,
      },
    });
  }

  return (
    <PageHeaderLayout>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={8} sm={24}>
                  <FormItem label="角色">
                    {getFieldDecorator('name')(<Input placeholder="请输入" />)}
                  </FormItem>
                </Col>
                <Col md={8} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                      重置
                    </Button>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={onAdd}>
              新建
            </Button>
          </div>
          <Table
            loading={loading}
            rowKey={record => record.id}
            dataSource={role.list}
            columns={columns}
            pagination={pagination}
            onChange={handleTableChange}
          />
        </div>
      </Card>
      <AddEditModal {...AddEditModalProps} />
      <RolePermission {...rolePermissionProps} />
    </PageHeaderLayout>
  );
}

function mapStateToProps(state) {
  return {
    role: state.role,
    loading: state.loading.models.role,
  };
}

Role = Form.create()(Role);

export default connect(mapStateToProps)(Role);
