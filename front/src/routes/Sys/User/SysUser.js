import React, { Fragment } from 'react';
import { message } from 'antd';
import { connect } from 'dva';
import {
  Select,
  Form,
  Upload,
  Input,
  Button,
  Row,
  Col,
  Table,
  Popconfirm,
  Tree,
  Tag,
  Divider,
  Card,
  TreeSelect,
  Modal,
} from 'antd';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import styles from '../../../common/common.less';
import ShowFileModal from './ShowFileModal';
import UserModal from './UserModal';
import { baseUrl } from '../../../config/system';
const FormItem = Form.Item;
const TreeNode = Tree.TreeNode;

function SysUser({ location, dispatch, sysUser, form }) {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    setFieldsValue,
    resetFields,
    getFieldProps,
    getFieldValue,
  } = form;
  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 60,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    {
      title: '姓名',
      dataIndex: 'name',
      key: 'name',
      width: 120,
      render: (text, record, value) => (
        <Fragment>
          <a onClick={() => onFindInfo(record.id_)}>{record.name}</a>
        </Fragment>
      ),
    },
    {
      title: '用户名',
      dataIndex: 'account',
      key: 'account',
      width: 120,
    },
    {
      title: '职位',
      dataIndex: 'position',
      key: 'position',
      width: 100,
    },
    {
      title: '手机号',
      dataIndex: 'phone',
      key: 'phone',
      width: 120,
    },
    {
      title: '员工号',
      dataIndex: 'employeeNo',
      key: 'employeeNo',
      width: 120,
    },
    {
      title: '所属部门',
      dataIndex: 'deptName',
      key: 'deptName',
      width: 150,
    },
    
    {
      title: '性别',
      dataIndex: 'sex',
      key: 'sex',
      width: 80,
      render: (text, record) => {
        if (record.sex == 1) {
          return <Tag color="#2db7f5">男</Tag>;
        }
        if (record.sex == 2) {
          return <Tag color="#f50">女</Tag>;
        }
      },
    },
    {
      title: '角色',
      dataIndex: 'roleNames',
      key: 'roleNames',
      width: 200,
    },
    {
      title: '邮箱',
      dataIndex: 'email',
      key: 'email',
      width: 150,
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      width: 80,
      render: (text, record) => {
        if (record.status == 1) {
          return <Tag color="green">正常</Tag>;
        }
        if (record.status == 2) {
          return <Tag color="blue">停用</Tag>;
        }
        if (record.status == 3) {
          return <Tag color="red">禁用</Tag>;
        }
      },
    },
    {
      title: '备注',
      dataIndex: 'remarks',
      key: 'remarks',
      width: 200,
    },
    {
      title: '操作',
      width: 100,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onEdit(record.id_)}>编辑</a>
          <Divider type="vertical" />
          <Popconfirm title="确定删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];
  const onFindInfo = id => {
    dispatch({
      type: 'sysUser/findInfo',
      payload: id,
    });
  };
  const loop = data =>
    data.map(item => {
      if (item.children && item.children.length) {
        return (
          <TreeNode key={item.id_} title={item.name}>
            {loop(item.children)}
          </TreeNode>
        );
      }
      return <TreeNode key={item.id_} title={item.name} />;
    });

  function onSearch() {
    dispatch({
      type: 'sysUser/qryUserByParams',
      payload: { pageNum: 1, pageSize: user.pageSize, ...getFieldsValue() },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'sysUser/qryUserByParams',
      payload: { pageNum: 1, pageSize: sysUser.pageSize },
    });
  }

  function downloadTemplate() {
    window.open(baseUrl + '/execl/downLoadTemplate.jhtml?type=user');
  }

  const onAdd = () => {
    resetFields();
    dispatch({
      type: 'sysUser/showCreateModal',
      payload: {
        modalType: 'create',
      },
    });
  };

  function onDelete(id) {
    dispatch({
      type: 'sysUser/remove',
      payload: id,
      search: { pageNum: sysUser.current, pageSize: sysUser.pageSize, ...getFieldsValue() },
    });
  }

  const onEdit = id => {
    dispatch({
      type: 'sysUser/qryUserById',
      payload: id,
    });
  };

  const submitHandle = () => {
    dispatch({
      type: 'sysUser/qryUserByParams',
      payload: { ...getFieldsValue(), ...{ fireBrigadeId: sysUser.fireBrigadeId } },
    });
  };

  const pagination = {
    current: sysUser.current,
    pageSize: sysUser.pageSize,
    total: sysUser.total,
    showSizeChanger: true,
    showTotal: total => '共' + sysUser.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'sysUser/qryUserByParams',
        payload: {
          pageNum: current,
          pageSize: size,
          ...getFieldsValue(),
          ...{ fireBrigadeId: sysUser.fireBrigadeId },
        },
      });
    },
    onChange(current) {
      dispatch({
        type: 'sysUser/qryUserByParams',
        payload: {
          pageNum: current,
          pageSize: sysUser.pageSize,
          ...getFieldsValue(),
          ...{ fireBrigadeId: sysUser.fireBrigadeId },
        },
      });
    },
  };

  const modalProps = {
    item: sysUser.modalType === 'create' ? {} : sysUser.currentItem,
    deptNoTopTree: sysUser.deptNoTopTree,
    roles: sysUser.roleList,
    visible: sysUser.modalVisible,
    type: sysUser.modalType,
    maskClosable: false,
    confirmLoading: sysUser.buttonLoading,
    title: `${sysUser.modalType === 'create' ? '新增用户' : '更新用户'}`,
    wrapClassName: 'vertical-center-modal',
    newKey: sysUser.newKey,
    onOk(data) {
      dispatch({
        type: `sysUser/${sysUser.modalType}`,
        payload: data,
      });
    },
    onCancel() {
      dispatch({
        type: 'sysUser/hideModal',
      });
    },
  };
  const ShowModalProps = {
    item: sysUser.currentItem,
    deptNoTopTree: sysUser.deptNoTopTree,
    roles: sysUser.roleList,
    visible: sysUser.findModalVisible,
    type: sysUser.modalType,
    maskClosable: false,
    confirmLoading: sysUser.buttonLoading,
    title: `查看信息`,
    wrapClassName: 'vertical-center-modal',
    newKey: sysUser.newKey,
    onCancel() {
      dispatch({
        type: 'sysUser/updateState',
        payload: { findModalVisible: false },
      });
    },
  };

  const onSelect = info => {
    if (info.length > 0) {
      dispatch({
        type: 'sysUser/qryUserByParams',
        payload: { ...getFieldsValue(), ...{ fireBrigadeId: info[0] } },
      });
    } else {
      dispatch({
        type: 'sysUser/qryUserByParams',
        payload: { ...getFieldsValue(), ...{ fireBrigadeId: null } },
      });
    }
  };

  function handleTableChange(pagination, filters, sorter) {
    let isChinese = ['account', 'name'].indexOf(sorter.field) >= 0;
    dispatch({
      type: 'sysUser/qryUserByParams',
      payload: {
        pageNum: pagination.current,
        pageSize: pagination.pageSize,
        ...getFieldsValue(),
        orderBy: sorter.field,
        order: sorter.order,
        isChinese,
      },
    });
  }

  const treeData = i => {
    return i.map(d => {
      d.title = d.name;
      d.key = d.id_ + '';
      d.value = d.id_ + '';
      d.children = d.children && d.children.length > 0 ? treeData(d.children) : [];
      return d;
    });
  };

  function initLoopMsg(values) {
    return values != null && values.length > 0
      ? values.map((k, index) => {
          return <div key={index}>{k}</div>;
        })
      : null;
  }

  const props = {
    name: 'file',
    action: baseUrl + '/user/importExpert.jhtml',
    beforeUpload(file) {
      // const isJPG = file.type === 'application/vnd.ms-excel' || file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
      // if (!isJPG) {
      //   message.error('只能上传EXCEL文件！');
      // }
      // return isJPG;
    },
    onChange(info) {
      let fileList = info.fileList;
      if (info.file.status === 'done') {
        if (info.file.response.httpCode == 200) {
          if (info.file.response.errorCode == 2) {
            if (info.file.response.data && info.file.response.data.length > 0) {
              Modal.error({
                title: '导入提示',
                content: initLoopMsg(info.file.response.data),
              });
            }
          } else {
            message.success(`${info.file.name} 上传成功`);
            dispatch({
              type: 'sysUser/qryUserByParams',
              payload: { pageNum: 1, pageSize: sysUser.pageSize, ...getFieldsValue() },
            });
          }
        } else {
          Modal.error({
            title: '导入提示',
            content: info.file.response.msg,
          });
        }
        fileList = fileList.filter(file => {
          return file.response && file.response.fid;
        });
        //只保留最后一条记录
        fileList = fileList.slice(-1);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`);
      }
      let fildsValue = {};
      fildsValue['file'] = fileList.map(file => {
        return file;
      });
      setFieldsValue(fildsValue);
    },
  };

  return (
    <PageHeaderLayout>
      <div style={{ backgroundColor: '#fff', position: 'relative' }}>
        <div style={{ width: '230px', float: 'left', position: 'absolute', bottom: '0', top: '0' }}>
          <div
            style={{
              width: '100%',
              height: '40px',
              backgroundColor: '#232f40',
              lineHeight: '40px',
              color: '#fff',
              paddingLeft: '20px',
              fontSize: '14px',
              fontWeight: '400',
            }}
          >
            所属消防队
          </div>
          <div
            style={{
              position: 'absolute',
              top: '40px',
              bottom: '0px',
              overflow: 'hidden',
              overflowY: 'auto',
              width: '100%',
            }}
          >
            <Tree defaultExpandedKeys={['1']} onSelect={onSelect}>
              {sysUser.deptTree && loop(sysUser.deptTree)}
            </Tree>
          </div>
        </div>
        <div
          style={{
            marginLeft: '230px',
            paddingTop: '0px',
            minHeight: '600px',
            borderLeft: '1px solid #e8e8e8',
          }}
        >
          <Card bordered={false}>
            <div className={styles.tableList}>
              <div className={styles.tableListForm}>
                <Form layout="inline">
                  <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                    <Col md={6} sm={24}>
                      <FormItem label="用户名：">
                        {getFieldDecorator('account')(<Input placeholder="输入用户名查找" />)}
                      </FormItem>
                    </Col>
                    <Col md={6} sm={24}>
                      <FormItem label="姓名：">
                        {getFieldDecorator('name')(<Input placeholder="输入姓名查找" />)}
                      </FormItem>
                    </Col>
                    <Col md={6} sm={24}>
                      <FormItem label="手机号：">
                        {getFieldDecorator('phone')(<Input placeholder="输入手机号查找" />)}
                      </FormItem>
                    </Col>
                    <Col md={6} sm={5}>
                      <FormItem label="所属部门	">
                        {getFieldDecorator('deptId')(
                          <TreeSelect
                            showSearch
                            treeNodeFilterProp="title"
                            dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                            placeholder="选择所属部门查找"
                            notFoundContent="无匹配结果"
                            allowClear
                            treeData={treeData(sysUser.deptNoTopTree)}
                            treeDefaultExpandAll
                          />
                        )}
                      </FormItem>
                    </Col>
                  </Row>
                  <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                    <Col md={6} sm={24}>
                      <FormItem label="员工号">
                        {getFieldDecorator('employeeNo')(<Input placeholder="输入员工号查找" />)}
                      </FormItem>
                    </Col>
                    <Col md={6} sm={24}>
                      <FormItem label="邮箱：">
                        {getFieldDecorator('email')(<Input placeholder="输入邮箱查找" />)}
                      </FormItem>
                    </Col>
                    <Col md={6} sm={24}>
                      <span className={styles.submitButtons}>
                        <Button type="primary" htmlType="submit" onClick={submitHandle}>
                          查询
                        </Button>
                        <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                          重置
                        </Button>

                        <Button
                          style={{ marginLeft: 80 }}
                          type="primary"
                          icon="download"
                          onClick={downloadTemplate}
                        >
                          下载模板
                        </Button>
                        <Upload
                          {...getFieldProps(
                            'file',
                            {
                              validate: [
                                {
                                  rules: [
                                    { type: 'array', required: true, message: '请添加数据文件' },
                                  ],
                                  trigger: 'onBlur',
                                },
                              ],
                            },
                            { valuePropName: 'fileIds' }
                          )}
                          {...props}
                          fileList={getFieldValue('file')}
                        >
                          <Button style={{ marginLeft: 8 }} type="primary" icon="upload">
                            导入
                          </Button>
                        </Upload>
                      </span>
                    </Col>
                  </Row>
                </Form>
              </div>
              <div className={styles.submitButtons}>
                <Button icon="plus" type="primary" onClick={onAdd}>
                  新增
                </Button>
              </div>
              <div style={{ width: '100%' }}>
                <Table
                  columns={columns}
                  dataSource={sysUser.userList}
                  style={{ marginTop: 10 }}
                  pagination={pagination}
                  loading={sysUser.loading}
                  rowKey={record => record.id_}
                  onChange={handleTableChange}
                  scroll={{ x: 1600 }}
                />
              </div>
            </div>
          </Card>
        </div>
      </div>
      <UserModal {...modalProps} />
      <ShowFileModal {...ShowModalProps} />
    </PageHeaderLayout>
  );
}

function mapStateToProps(state) {
  return {
    sysUser: state.sysUser,
  };
}

SysUser = Form.create()(SysUser);

export default connect(mapStateToProps)(SysUser);
