import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, Form, Input, Radio, TreeSelect, Select, Row, Col } from 'antd';
const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const Option = Select.Option;

class UserModal extends Component {
  constructor(props) {
    super(props);
  }

  okHandler = () => {
    const { onOk } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        onOk(values);
      }
    });
  };

  checkPassword = (rule, value, callback) => {
    if (this.props.type == 'create') {
      if (!value && $.trim(value) == '') {
        callback('请输入密码');
      } else {
        callback();
      }
    } else {
      callback();
    }
  };

  checkConfirm = (rule, value, callback) => {
    const form = this.props.form;
    if (this.props.type == 'create') {
      if (!value && $.trim(value) == '') {
        callback('请再次输入密码');
      }
    }
    if (value && value !== form.getFieldValue('password')) {
      callback('两次密码输入不一致，请重新输入');
    } else {
      callback();
    }
  };

  render() {
    const { children, sysUser, item, deptNoTopTree, roles } = this.props;
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 14 },
    };

    const treeData = i => {
      return i.map(d => {
        d.title = d.name;
        d.key = d.id_ + '';
        d.value = d.id_ + '';
        d.children = d.children && d.children.length > 0 ? treeData(d.children) : [];
        return d;
      });
    };

    const loopRoles = data =>
      data.map(item => {
        return <Option key={item.id}>{item.name}</Option>;
      });
    function chechPhone(rule, value, callback) {
      var reg = /^(13\d|14[57]|15[^4,\D]|17[678]|18\d)\d{8}|170[059]\d{7}$/;
      if (value != null && value != '' && !reg.test(value)) {
        callback(new Error('请输入正确格式的联系方式'));
      } else {
        callback();
      }
    }

    return (
      <span>
        <Modal
          maskClosable={this.props.maskClosable}
          confirmLoading={this.props.confirmLoading}
          title={this.props.title}
          visible={this.props.visible}
          onOk={this.okHandler}
          onCancel={this.props.onCancel}
          key={this.props.newKey}
          width={800}
        >
          <Form layout="horizontal" onSubmit={this.okHandler}>
            <FormItem style={{ marginBottom: '0px' }}>
              {getFieldDecorator('id', { initialValue: item.id_ })(<Input type="hidden" />)}
            </FormItem>
            <Row>
              <Col span={12}>
                <FormItem {...formItemLayout} label="用户名" hasFeedback>
                  {this.props.type == 'create'
                    ? getFieldDecorator('account', {
                        initialValue: item.account,
                        rules: [
                          {
                            required: true,
                            whitespace: true,
                            message: '请输入用户名',
                          },
                        ],
                      })(<Input />)
                    : getFieldDecorator('account', {
                        initialValue: item.account,
                        rules: [
                          {
                            required: true,
                            whitespace: true,
                            message: '请输入用户名',
                          },
                        ],
                      })(<Input readOnly="readonly" />)}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem {...formItemLayout} label="姓名" hasFeedback>
                  {getFieldDecorator('name', {
                    initialValue: item.name,
                    rules: [
                      {
                        required: true,
                        whitespace: true,
                        message: '请输入姓名',
                      },
                    ],
                  })(<Input />)}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem {...formItemLayout} label="密码" required hasFeedback>
                  {getFieldDecorator('password', {
                    rules: [
                      {
                        validator: this.checkPassword,
                      },
                    ],
                  })(<Input type="password" />)}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem {...formItemLayout} required label="确认密码" hasFeedback>
                  {getFieldDecorator('confirm', {
                    rules: [
                      {
                        validator: this.checkConfirm,
                      },
                    ],
                  })(<Input type="password" />)}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem {...formItemLayout} label="所属机构" hasFeedback>
                  {getFieldDecorator('deptId', {
                    initialValue: item.deptId_ ? item.deptId_ + '' : undefined,
                    rules: [
                      {
                        required: true,
                        whitespace: true,
                        message: '请选择所属机构',
                      },
                    ],
                  })(
                    <TreeSelect
                      treeNodeFilterProp="title"
                      dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                      placeholder="请选择所属机构"
                      notFoundContent="无匹配结果"
                      allowClear
                      treeData={treeData(deptNoTopTree)}
                      treeDefaultExpandAll
                    />
                  )}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem {...formItemLayout} label="所属消防队" hasFeedback>
                  {getFieldDecorator('fireBrigadeId', {
                    initialValue: item.fireBrigadeId
                      ? item.fireBrigadeId + ''
                      : sysUser.fireBrigadeId ? sysUser.fireBrigadeId + '' : undefined,
                    rules: [{ required: true, whitespace: true, message: '请选择所属消防队' }],
                  })(
                    <TreeSelect
                      showSearch
                      treeNodeFilterProp="title"
                      dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                      placeholder="请选择消防队"
                      notFoundContent="无匹配结果"
                      allowClear
                      treeData={treeData(sysUser.deptTree)}
                      treeDefaultExpandAll
                    />
                  )}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem {...formItemLayout} label="角色" hasFeedback>
                  {getFieldDecorator('roles', {
                    initialValue: item.roles_ ? item.roles_ : [],
                    rules: [{ required: true, message: '请选择角色' }],
                  })(
                    <Select mode="multiple" placeholder="请选择角色">
                      {loopRoles(roles)}
                    </Select>
                  )}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem {...formItemLayout} label="员工号" hasFeedback>
                  {getFieldDecorator('employeeNo', {
                    initialValue: item.employeeNo,
                    
                  })(<Input />)}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem {...formItemLayout} label="职务" hasFeedback>
                  {getFieldDecorator('position', {
                    initialValue: item.position,
                    
                  })(<Input />)}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem {...formItemLayout} label="电话" hasFeedback>
                  {getFieldDecorator('phone', {
                    initialValue: item.phone,
                    rules: [
                      
                      { validator: chechPhone },
                    ],
                  })(<Input />)}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem {...formItemLayout} label="邮箱" hasFeedback>
                  {getFieldDecorator('email', {
                    initialValue: item.email,
                    rules: [
                      {
                        type: 'email',
                        message: '请输入有效的邮箱!',
                      },
                      
                    ],
                  })(<Input />)}
                </FormItem>
              </Col>

              <Col span={12}>
                <FormItem {...formItemLayout} label="性别">
                  {getFieldDecorator('sex', {
                    initialValue: item.sex == null ? 1 : item.sex,
                  })(
                    <RadioGroup>
                      <Radio value={1}>男</Radio>
                      <Radio value={2}>女</Radio>
                    </RadioGroup>
                  )}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem {...formItemLayout} label="状态">
                  {getFieldDecorator('status', {
                    initialValue: item.status == null ? 1 : item.status,
                  })(
                    <RadioGroup>
                      <Radio value={1}>正常</Radio>
                      {/* <Radio value={2}>锁定</Radio> */}
                      <Radio value={3}>禁用</Radio>
                    </RadioGroup>
                  )}
                </FormItem>
              </Col>

                <Col span={12}>
                  <FormItem {...formItemLayout} label="备注" hasFeedback>
                    {getFieldDecorator('remarks', {
                      initialValue: item.remarks,
                    })(<Input />)}
                  </FormItem>
                </Col>
            </Row>
          </Form>
        </Modal>
      </span>
    );
  }
}

function mapStateToProps(state) {
  return {
    sysUser: state.sysUser,
    loading: state.loading.models.sysUser,
  };
}

UserModal = Form.create()(UserModal);

export default connect(mapStateToProps)(UserModal);
