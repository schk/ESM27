import React, { Fragment } from 'react';
import {
  Table,
  Form,
  Button,
  Popconfirm,
  Card,
  Input,
  Divider,
  Row,
  Col,
  Modal,
  Upload,
  message,
} from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import LocationModal from './LocationModal';
import ShowFileModal from './ShowFileModal';
import PageHeader from '../../../layouts/PageHeaderLayout';
import { baseUrl } from '../../../config/system';
const FormItem = Form.Item;

function Teams({ location, teams, form, dispatch, loading }) {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    setFieldsValue,
    getFieldValue,
    getFieldProps,
    resetFields,
  } = form;
  const AddEditModalProps = {
    item: teams.item,
  };
  const LocationModalProps = {};
  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    {
      title: '队伍名称',
      dataIndex: 'name',
      key: 'name',
      width: 180,
      render: (text, record, value) => (
        <Fragment>
          <a onClick={() => onFindKeyUnit(record.id_)}>{record.name}</a>
        </Fragment>
      ),
    },
    { title: '队伍位置', dataIndex: 'position', key: 'position', width: 180 },
    {
      title: '经纬度',
      dataIndex: 'lon',
      key: 'lon',
      width: 150,
      render: (text, record) => {
        if(record.lon && record.lat){
          return record.lon + ',' + record.lat;
        }
        return '';
      },
    },
    { title: '队伍性质', dataIndex: 'properties', key: 'properties', width: 100 },
    { title: '队伍规模', dataIndex: 'scale', key: 'scale', width: 100 },
    { title: '队伍责任人', dataIndex: 'charge', key: 'charge', width: 100 },
    { title: '责任人联系方式', dataIndex: 'phone', key: 'phone', width: 100 },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 100,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_)}>修改</a>
          <Divider type="vertical" />
          {/* <a onClick={() => onFindKeyUnit(record.id_)}>查看</a>
          <Divider type="vertical" /> */}
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];
  const onFindKeyUnit = id => {
    dispatch({
      type: 'teams/findTeams',
      payload: id,
    });
  };
  function onUpdate(id) {
    dispatch({
      type: 'teams/info',
      payload: id,
    });
  }

  function onAdd() {
    resetFields();
    dispatch({
      type: 'teams/updateState',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'teams/del',
      payload: id,
      search: { pageNum: teams.current, pageSize: teams.pageSize, ...getFieldsValue() },
    });
  }

  function handleSearch() {
    dispatch({
      type: 'teams/qryListByParams',
      payload: { pageNum: 1, pageSize: teams.pageSize, ...getFieldsValue() },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'teams/qryListByParams',
      payload: { pageNum: 1, pageSize: teams.pageSize },
    });
  }

  const pagination = {
    current: teams.current,
    pageSize: teams.pageSize,
    total: teams.total,
    showSizeChanger: true,
    showTotal: total => '共' + teams.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'teams/qryListByParams',
        payload: { pageNum: current, pageSize: size, ...getFieldsValue() },
      });
    },
    onChange(current) {
      dispatch({
        type: 'teams/qryListByParams',
        payload: { pageNum: current, pageSize: teams.pageSize, ...getFieldsValue() },
      });
    },
  };
  const LocationModalGen = () => <LocationModal {...LocationModalProps} />;

  function downloadTemplate() {
    window.open(baseUrl + '/execl/downLoadTemplate.jhtml?type=teamsTemplate');
  }

  function initLoopMsg(values) {
    return values != null && values.length > 0
      ? values.map((k, index) => {
          return <div key={index}>{k}</div>;
        })
      : null;
  }

  const props = {
    name: 'file',
    action: baseUrl + '/teams/excel/importExcel.jhtml',
    beforeUpload(file) {
      // const isJPG = file.type === 'application/vnd.ms-excel' || file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
      // if (!isJPG) {
      //   message.error('只能上传EXCEL文件！');
      // }
      // return isJPG;
    },
    onChange(info) {
      let fileList = info.fileList;
      if (info.file.status === 'done') {
        if (info.file.response.httpCode == 200) {
          if (info.file.response.errorCode == 1) {
            //弹出确认取消框
          } else if (info.file.response.errorCode == 2) {
            if (info.file.response.data && info.file.response.data.length > 0) {
              Modal.error({
                title: '导入提示',
                content: initLoopMsg(info.file.response.data),
              });
            }
          } else {
            message.success(`${info.file.name} 上传成功`);
            dispatch({
              type: 'teams/qryListByParams',
              payload: {},
            });
          }
        } else {
          Modal.error({
            title: '导入提示',
            content: info.file.response.msg,
          });
        }
        fileList = fileList.filter(file => {
          return file.response && file.response.fid;
        });
        //只保留最后一条记录
        fileList = fileList.slice(-1);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`);
      }
      let fildsValue = {};
      fildsValue['file'] = fileList.map(file => {
        return file;
      });
      setFieldsValue(fildsValue);
    },
  };

  return (
    <PageHeader>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={6} sm={24}>
                  <FormItem label="名称">
                    {getFieldDecorator('name')(<Input placeholder="请输入名称" />)}
                  </FormItem>
                </Col>
                <Col md={8} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                      重置
                    </Button>

                    <Button
                      style={{ marginLeft: 80 }}
                      type="primary"
                      icon="download"
                      onClick={downloadTemplate}
                    >
                      下载模板
                    </Button>
                    <Upload
                      {...getFieldProps(
                        'file',
                        {
                          validate: [
                            {
                              rules: [{ type: 'array', required: true, message: '请添加数据文件' }],
                              trigger: 'onBlur',
                            },
                          ],
                        },
                        { valuePropName: 'fileIds' }
                      )}
                      {...props}
                      fileList={getFieldValue('file')}
                    >
                      <Button style={{ marginLeft: 8 }} type="primary" icon="upload">
                        导入
                      </Button>
                    </Upload>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={onAdd}>
              新建
            </Button>
          </div>
          <Table
            columns={columns}
            dataSource={teams.list}
            rowKey={record => record.id}
            loading={loading}
            pagination={pagination}
          />
        </div>
      </Card>
      <AddEditModal {...AddEditModalProps} />
      <ShowFileModal {...AddEditModalProps} />
      <LocationModalGen />
    </PageHeader>
  );
}

function mapStateToProps(state) {
  return {
    teams: state.teams,
    loading: state.loading.models.teams,
  };
}

Teams = Form.create()(Teams);

export default connect(mapStateToProps)(Teams);
