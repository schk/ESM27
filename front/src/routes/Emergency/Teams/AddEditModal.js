import React from 'react';
import { Form, Input, Modal, Button, upload, Icon, Radio, Row, Col,message, } from 'antd';
import { connect } from 'dva';
import { baseFileUrl } from '../../../config/system';

const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const { TextArea } = Input;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

let AddEditModal = ({ teams, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
    getFieldProps,
  } = form;

  const modalOpts = {
    title: teams.modalType == 'create' ? '新建应急队伍' : '修改应急队伍',
    visible: teams.modalVisible,
    maskClosable: false,
    width: 600,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={teams.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  const onGetLocation = () => {
    const data = getFieldsValue();
    dispatch({
      type: 'teams/updateState',
      payload: {
        locationModalVisible: true,
        newKey1: new Date().getTime() + '',
        locationItem: { lonLat: data.lonLat, dtPath: data.dtPath },
      },
    });
  };

  if (!teams.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'teams/updateState',
      payload: {
        modalVisible: false,
        locationItem: {},
      },
    });
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      var lonLat = getFieldsValue(["lonLat"]);
          if(lonLat.lonLat==undefined){
          message.error("坐标不能为空");
          return;
      }
      const data = getFieldsValue();
      data.id = teams.modalType === 'create' ? '' : item.id;
      dispatch({
        type: `teams/${teams.modalType}`,
        payload: data,
        search: { pageNum: teams.current, pageSize: teams.pageSize },
      });
    });
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={24}>
            <FormItem label="队伍名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '请填写应急队伍名称' },
                  { max: 30, message: '最长不超过30个字' },
                ],
              })(<Input type="text" placeholder="请填写应急队伍名称" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="队伍位置:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('position', {
                initialValue: item.position,
                rules: [
                  { required: true, message: '请填写应急队伍位置' },
                  { max: 100, message: '最长不超过100个字' },
                ],
              })(
                <TextArea autosize={{ minRows: 2, maxRows: 4 }} placeholder="请填写应急队伍位置" />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="获取坐标点:" hasFeedback {...formItemLayout}>
              <div
                style={{
                  marginRight: '10px',
                  marginTop: '5px',
                  width: '28px',
                  height: '28px',
                  background: 'url(images/iconAdd.png)',
                  display: 'inlineBlock',
                  float: 'left',
                  cursor: 'pointer',
                }}
                onClick={onGetLocation}
              />
              {getFieldDecorator('lonLat', {
                initialValue: teams.locationItem.lonLat
                  ? teams.locationItem.lonLat + ''
                  : undefined,
              })(
                <Input type="text" readOnly style={{ width: '88%' }} placeholder="请选择经纬度" />
              )}
            </FormItem>
          </Col>
        </Row>
        {/* <Row>
          <Col span={24}>
             <FormItem label="地图图标:" {...formItemLayout}>
                <Input type="hidden" {...getFieldProps('dtPath', { initialValue: teams.locationItem.dtPath })} />
                {teams.locationItem.dtPath?
                  <div style={{borderRadius: '4px',border: '1px solid #d9d9d9',height: '64px',position: 'relative',clear: 'both',overflow: 'hidden',}}>
                  <div style={{width: '60px',height: '60px',textAlign: 'center',border: '1px solid #d9d9d9', position: 'relative', borderRadius: '5px'}}>
                    <img style={{position: 'absolute',left: '0',right: '0',bottom: '0',top: '0',margin: 'auto', width: '20px', height: '20px',}}
                      src={baseFileUrl + teams.locationItem.dtPath} alt=""
                    />
                  </div>
                </div> :""}                    
            </FormItem>
          </Col>
        </Row> */}
        <Row>
          <Col span={24}>
            <FormItem label="队伍性质:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('properties', {
                initialValue: item.properties,
                rules: [
                  { required: true, message: '请填写队伍性质' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写队伍性质" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="队伍责任人:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('charge', {
                initialValue: item.charge,
                rules: [
                  { required: true, message: '请填写队伍责任人' },
                  { max: 20, message: '最长不超过20个字' },
                ],
              })(<Input type="text" placeholder="请填写队伍责任人" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="责任人联系方式:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('phone', {
                initialValue: item.phone,
                rules: [
                  { required: true, message: '请输入责任人联系方式' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写责任人联系方式" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="队伍规模:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('scale', {
                initialValue: item.scale,
                rules: [{ max: 100, message: '最长不超过100个字' }],
              })(<TextArea autosize={{ minRows: 2 }} placeholder="请应急队伍规模" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="描述:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('remark', {
                initialValue: item.remark,
                rules: [{ max: 100, message: '最长不超过100个字' }],
              })(<TextArea autosize={{ minRows: 2 }} placeholder="请应急队伍描述" />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { teams: state.teams };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
