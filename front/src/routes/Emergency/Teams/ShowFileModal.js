import React from 'react';
import { Form, Input, Modal, Button, Radio, Row, Col } from 'antd';
import { connect } from 'dva';
import { baseFileUrl } from '../../../config/system';
const FormItem = Form.Item;
const { TextArea } = Input;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

let AddEditModal = ({ teams, item = {}, form, dispatch }) => {
  const { getFieldDecorator, resetFields } = form;

  const modalOpts = {
    title: '查看信息',
    visible: teams.findModalVisible,
    maskClosable: false,
    width: 600,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
    ],
  };
  if (!teams.findModalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'teams/updateState',
      payload: {
        findModalVisible: false,
        locationItem: {},
      },
    });
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={24}>
            <FormItem label="队伍名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
              })(<Input type="text" readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="队伍位置:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('position', {
                initialValue: item.position,
              })(<TextArea autosize={{ minRows: 2, maxRows: 4 }} readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="经纬度:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('lonLat', {
                initialValue: teams.locationItem.lonLat
                  ? teams.locationItem.lonLat + ''
                  : undefined,
              })(<Input type="text" readOnly />)}
            </FormItem>
          </Col>
        </Row>
        {/* <Row>
           <Col span={24}>
            <FormItem label="地图图标:" {...formItemLayout}>
              {teams.locationItem.dtPath?
                <div style={{borderRadius: '4px',border: '1px solid #d9d9d9',height: '64px',position: 'relative',clear: 'both',overflow: 'hidden',}}>
                <div style={{width: '60px',height: '60px',textAlign: 'center',border: '1px solid #d9d9d9', position: 'relative', borderRadius: '5px'}}>
                  <img style={{position: 'absolute',left: '0',right: '0',bottom: '0',top: '0',margin: 'auto', width: '20px', height: '20px',}}
                    src={baseFileUrl + teams.locationItem.dtPath} alt=""
                  />
                </div>
              </div> :""}                    
            </FormItem>
           </Col>
        </Row> */}
        <Row>
          <Col span={24}>
            <FormItem label="队伍性质:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('properties', {
                initialValue: item.properties,
              })(<Input type="text" readOnly />)}
            </FormItem>
          </Col>
        </Row>

        <Row>
          <Col span={24}>
            <FormItem label="队伍责任人:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('charge', {
                initialValue: item.charge,
              })(<Input type="text" readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="责任人联系方式:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('phone', {
                initialValue: item.phone,
              })(<Input type="text" readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="队伍规模:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('scale', {
                initialValue: item.scale,
              })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="描述:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('remark', {
                initialValue: item.remark,
              })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { teams: state.teams };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
