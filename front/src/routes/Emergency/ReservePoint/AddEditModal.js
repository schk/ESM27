import React from 'react';
import { Form, Input, Modal, Button, Radio, Row, Col, Select, TreeSelect,message, } from 'antd';
import { connect } from 'dva';
import { baseFileUrl } from '../../../config/system';
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const Option = Select.Option;
const { TextArea } = Input;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

let AddEditModal = ({ reservePoint, item = {}, form, dispatch }) => {
  const { getFieldDecorator, validateFields, getFieldsValue, resetFields, getFieldProps } = form;

  const modalOpts = {
    title: reservePoint.modalType == 'create' ? '新建储备点' : '修改储备点',
    visible: reservePoint.modalVisible,
    maskClosable: false,
    width: 600,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={reservePoint.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!reservePoint.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'reservePoint/updateState',
      payload: {
        modalVisible: false,
        locationItem: {},
      },
    });
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      var lonLat = getFieldsValue(["lonLat"]);
          if(lonLat.lonLat==undefined){
          message.error("坐标不能为空");
          return;
      }
      const data = getFieldsValue();
      data.id = reservePoint.modalType === 'create' ? '' : item.id;
      dispatch({
        type: `reservePoint/${reservePoint.modalType}`,
        payload: data,
        search: reservePoint.selectObj,
      });
    });
  }

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  //根据储备点取重点单位名称
  const changeRadio = e => {
    form.setFieldsValue({ keyUnitId: undefined });
    form.setFieldsValue({ fireBrigadeId: undefined });
    dispatch({
      type: 'reservePoint/updateState',
      payload: { type: e.target.value },
    });
  };

  const onGetLocation = () => {
    const data = getFieldsValue();
    dispatch({
      type: 'reservePoint/updateState',
      payload: {
        locationModalVisible: true,
        newKey1: new Date().getTime() + '',
        locationItem: { lonLat: data.lonLat, dtPath: data.dtPath },
      },
    });
  };

  const treeData = i => {
    return i.map(d => {
      d.title = d.name;
      d.key = d.id_ + '';
      d.value = d.id_ + '';
      d.children = d.children && d.children.length > 0 ? treeData(d.children) : [];
      return d;
    });
  };

  console.log(reservePoint.brigadeId);

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={24}>
            <FormItem label="储备点名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '请填写储备点名称' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写储备点名称" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem {...formItemLayout} label="所属单位">
              {getFieldDecorator('type', {
                initialValue: item.type == null ? 1 : item.type,
              })(
                <RadioGroup onChange={changeRadio}>
                  <Radio value={1}>重点单位</Radio>
                  <Radio value={2}>消防队</Radio>
                  <Radio value={3}>无</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
        </Row>

        {reservePoint.type == 1 ? (
          <Row>
            <Col span={24}>
              <FormItem label="所属单位:" hasFeedback {...formItemLayout}>
                {getFieldDecorator('keyUnitId', {
                  initialValue: item.keyUnitId == null ? undefined : item.keyUnitId + '',
                  rules: [{ required: true, message: '请选择所属单位' }],
                })(
                  <Select showSearch optionFilterProp="children" placeholder="请选择所属单位">
                    {loopOption(reservePoint.keyUnitList)}
                  </Select>
                )}
              </FormItem>
            </Col>
          </Row>
        ) : reservePoint.type == 2 ? (
          <Row>
            <Col span={24}>
              <FormItem {...formItemLayout} label="所属消防队" hasFeedback>
                {getFieldDecorator('keyUnitId', {
                  initialValue: item.keyUnitId
                    ? item.keyUnitId + ''
                    : reservePoint.brigadeId ? reservePoint.brigadeId + '' : undefined,
                  rules: [{ required: true, whitespace: true, message: '请选择所属消防队' }],
                })(
                  <TreeSelect
                    showSearch
                    treeNodeFilterProp="title"
                    dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                    placeholder="请选择消防队"
                    notFoundContent="无匹配结果"
                    allowClear
                    treeData={treeData(reservePoint.fireBrigadeTree)}
                    treeDefaultExpandAll
                  />
                )}
              </FormItem>
            </Col>
          </Row>
        ) : (
          ''
        )}

        <Row>
          <Col span={24}>
            <FormItem label="储备点性质:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('properties', {
                initialValue: item.properties,
                rules: [
                  { required: true, message: '请填写储备点性质' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input tyep="text" placeholder="请填写储备点性质" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="负责人:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('charge', {
                initialValue: item.charge,
                rules: [
                  { required: true, message: '请填写负责人' },
                  { max: 20, message: '最长不超过20个字' },
                ],
              })(<Input tyep="text" placeholder="请填写负责人" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="负责人联系方式:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('chargePhone', {
                initialValue: item.chargePhone,
                rules: [
                  { required: true, message: '请填写负责人联系方式' },
                  { max: 20, message: '最长不超过20个字' },
                ],
              })(<Input tyep="text" placeholder="请填写负责人联系方式" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="获取坐标点:" hasFeedback {...formItemLayout}>
              <div
                style={{
                  marginRight: '10px',
                  marginTop: '5px',
                  width: '28px',
                  height: '28px',
                  background: 'url(images/iconAdd.png)',
                  display: 'inlineBlock',
                  float: 'left',
                  cursor: 'pointer',
                }}
                onClick={onGetLocation}
              />
              {getFieldDecorator('lonLat', {
                initialValue: reservePoint.locationItem.lonLat
                  ? reservePoint.locationItem.lonLat + ''
                  : undefined,
              })(
                <Input type="text" readOnly style={{ width: '88%' }} placeholder="请选择经纬度" />
              )}
            </FormItem>
          </Col>
        </Row>
        {/* <Row>
          <Col span={24}>
             <FormItem label="地图图标:" {...formItemLayout}>
                <Input type="hidden" {...getFieldProps('dtPath', { initialValue: reservePoint.locationItem.dtPath })} />
                {reservePoint.locationItem.dtPath?
                  <div style={{borderRadius: '4px',border: '1px solid #d9d9d9',height: '64px',position: 'relative',clear: 'both',overflow: 'hidden',}}>
                  <div style={{width: '60px',height: '60px',textAlign: 'center',border: '1px solid #d9d9d9', position: 'relative', borderRadius: '5px'}}>
                    <img style={{position: 'absolute',left: '0',right: '0',bottom: '0',top: '0',margin: 'auto', width: '20px', height: '20px',}}
                      src={baseFileUrl + reservePoint.locationItem.dtPath} alt=""
                    />
                  </div>
                </div> :""}                    
            </FormItem>
          </Col>
        </Row> */}
        <Row>
          <Col span={24}>
            <FormItem label="储备点地址:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('address', {
                initialValue: item.address,
                rules: [
                  { required: true, message: '请填写储备点地址' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<TextArea autosize={{ minRows: 2, maxRows: 2 }} placeholder="请填写储备点地址" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="储备点描述:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('remark', {
                initialValue: item.remark,
                rules: [{ max: 100, message: '最长不超过100个字符' }],
              })(<TextArea autosize={{ minRows: 2 }} placeholder="请输入储备点描述" />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { reservePoint: state.reservePoint };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
