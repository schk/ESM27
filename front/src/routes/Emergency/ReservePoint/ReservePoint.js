import React, { Fragment } from 'react';
import {
  Table,
  Form,
  Button,
  Popconfirm,
  Card,
  Input,
  Divider,
  Row,
  Col,
  Select,
  TreeSelect,
  Radio,
  Upload,
  message,
  Modal,
} from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import LocationModal from './LocationModal';
import ShowFileModal from './ShowFileModal';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import { baseUrl } from '../../../config/system';
const Option = Select.Option;
const FormItem = Form.Item;

function ReservePoint({ location, reservePoint, form, dispatch, loading }) {
  const {
    getFieldDecorator,
    validateFields,
    getFieldValue,
    getFieldProps,
    getFieldsValue,
    setFieldsValue,
    resetFields,
  } = form;
  const AddEditModalProps = {
    item: reservePoint.item,
  };
  const LocationModalProps = {};
  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    {
      title: '储备点名称',
      dataIndex: 'name',
      key: 'name',
      width: 120,
      render: (text, record, value) => (
        <Fragment>
          <a onClick={() => onFindInfo(record.id_, record.type)}>{record.name}</a>
        </Fragment>
      ),
    },
    { title: '所属单位', dataIndex: 'keyUnitName', key: 'keyUnitName', width: 120 },
    { title: '责任人', dataIndex: 'charge', key: 'charge', width: 100 },
    { title: '责任人联系方式', dataIndex: 'chargePhone', key: 'chargePhone', width: 100 },
    { title: '性质', dataIndex: 'properties', key: 'properties', width: 100 },
    { title: '地址', dataIndex: 'address', key: 'address', width: 150 },
    {
      title: '经纬度',
      dataIndex: 'lon',
      width: 120,
      render: (text, record) => {
        if(record.lon && record.lat){
          return record.lon + ',' + record.lat;
        }
        return '';
      },
    },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 100,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_, record.type)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];
  function onFindInfo(id, type) {
    dispatch({
      type: 'reservePoint/findInfo',
      payload: { id: id, type: type },
    });
  }

  function onUpdate(id, type) {
    dispatch({
      type: 'reservePoint/info',
      payload: { id: id, type: type },
    });
  }

  function onAdd() {
    dispatch({
      type: 'reservePoint/showCreateModal',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'reservePoint/del',
      payload: id,
      search: {
        pageNum: reservePoint.current,
        pageSize: reservePoint.pageSize,
        ...getFieldsValue(),
        type: reservePoint.type,
      },
    });
  }

  function handleSearch() {
    dispatch({
      type: 'reservePoint/qryListByParams',
      payload: {
        pageNum: 1,
        pageSize: reservePoint.pageSize,
        ...getFieldsValue(),
        type: reservePoint.type,
      },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'reservePoint/qryListByParams',
      payload: {
        pageNum: 1,
        pageSize: reservePoint.pageSize,
        type: '',
      },
    });
  }

  function callback(key) {
    console.log(key);
  }

  // const loop = data =>
  //   data.map(item => {
  //     if (item.children && item.children.length) {
  //       return (
  //         <TreeNode key={item.id_} title={item.name}>
  //           {loop(item.children)}
  //         </TreeNode>
  //       );
  //     }
  //     return <TreeNode key={item.id_} title={item.name} />;
  //   });

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  //切换组
  function handleTypeChange(event) {
    dispatch({
      type: 'reservePoint/qryListByParams',
      payload: {
        pageNum: 1,
        pageSize: reservePoint.pageSize,
        ...getFieldsValue(),
        type: event.target.value,
      },
    });
  }

  const pagination = {
    current: reservePoint.current,
    pageSize: reservePoint.pageSize,
    total: reservePoint.total,
    showSizeChanger: true,
    showTotal: total => '共' + reservePoint.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'reservePoint/qryListByParams',
        payload: {
          pageNum: current,
          pageSize: size,
          ...getFieldsValue(),
          type: reservePoint.type,
        },
      });
    },
    onChange(current) {
      dispatch({
        type: 'reservePoint/qryListByParams',
        payload: {
          pageNum: current,
          pageSize: reservePoint.pageSize,
          ...getFieldsValue(),
          type: reservePoint.type,
        },
      });
    },
  };

  function handleTableChange(pagination, filters, sorter) {
    let isChinese = ['account', 'name'].indexOf(sorter.field) >= 0;
    dispatch({
      type: 'facilities/qryListByParams',
      payload: {
        pageNum: pagination.current,
        pageSize: pagination.pageSize,
        ...getFieldsValue(),
        orderBy: sorter.field,
        order: sorter.order,
        isChinese,
      },
    });
  }
  const LocationModalGen = () => <LocationModal {...LocationModalProps} />;

  const loop = data =>
    data.map(d => {
      let children = null;
      if (d.children != null && d.children.length > 0) {
        children = loop(d.children);
      }
      return { title: d.name, value: d.id_, key: d.id_, children };
    });

  function downloadTemplate() {
    window.open(baseUrl + '/execl/downLoadTemplate.jhtml?type=reservePointTemplate');
  }

  function initLoopMsg(values) {
    return values != null && values.length > 0
      ? values.map((k, index) => {
          return <div key={index}>{k}</div>;
        })
      : null;
  }

  const props = {
    name: 'file',
    action: baseUrl + '/reservePoint/excel/importExcel.jhtml',
    beforeUpload(file) {
      // const isJPG = file.type === 'application/vnd.ms-excel' || file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
      // if (!isJPG) {
      //   message.error('只能上传EXCEL文件！');
      // }
      // return isJPG;
    },
    onChange(info) {
      let fileList = info.fileList;
      if (info.file.status === 'done') {
        if (info.file.response.httpCode == 200) {
          if (info.file.response.errorCode == 1) {
            //弹出确认取消框
          } else if (info.file.response.errorCode == 2) {
            Modal.error({
              title: '导入提示',
              content: initLoopMsg(info.file.response.data),
            });
          } else {
            message.success(`${info.file.name} 上传成功`);
            dispatch({
              type: 'reservePoint/qryListByParams',
              payload: { type: '' },
            });
          }
        } else {
          Modal.error({
            title: '导入提示',
            content: info.file.response.msg,
          });
        }
        fileList = fileList.filter(file => {
          return file.response && file.response.fid;
        });
        //只保留最后一条记录
        fileList = fileList.slice(-1);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`);
      }
      let fildsValue = {};
      fildsValue['file'] = fileList.map(file => {
        return file;
      });
      setFieldsValue(fildsValue);
    },
  };

  return (
    <PageHeaderLayout>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={6} sm={24}>
                  <FormItem label="储备点名称">
                    {getFieldDecorator('name')(<Input placeholder="请输入名称" />)}
                  </FormItem>
                </Col>
                <Col md={5}>
                  <Radio.Group
                    defaultValue={reservePoint.type}
                    onChange={handleTypeChange.bind(this)}
                  >
                    <Radio.Button value="1">重点单位</Radio.Button>
                    <Radio.Button value="2">消防队</Radio.Button>
                    <Radio.Button value="3">其它</Radio.Button>
                  </Radio.Group>
                </Col>
                {reservePoint.type == 1 ? (
                  <Col span={6} sm={6}>
                    <Form.Item>
                      {getFieldDecorator('keyUnitId')(
                        <Select allowClear placeholder="请选择所属单位">
                          <Option value="">全部</Option>
                          {loopOption(reservePoint.keyUnitList)}
                        </Select>
                      )}
                    </Form.Item>
                  </Col>
                ) : reservePoint.type == 2 ? (
                  <Col span={6} sm={6}>
                    <Form.Item>
                      {getFieldDecorator('fireBrigadeId')(
                        <TreeSelect
                          showSearch
                          treeNodeFilterProp="title"
                          dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                          placeholder="请选择所属消防队"
                          notFoundContent="无匹配结果"
                          allowClear
                          treeData={loop(reservePoint.fireBrigadeTree)}
                          treeDefaultExpandAll
                        />
                      )}
                    </Form.Item>
                  </Col>
                ) : (
                  ''
                )}
                <Col md={6} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                      重置
                    </Button>

                    <Button
                      style={{ marginLeft: 80 }}
                      type="primary"
                      icon="download"
                      onClick={downloadTemplate}
                    >
                      下载模板
                    </Button>
                    <Upload
                      {...getFieldProps(
                        'file',
                        {
                          validate: [
                            {
                              rules: [{ type: 'array', required: true, message: '请添加数据文件' }],
                              trigger: 'onBlur',
                            },
                          ],
                        },
                        { valuePropName: 'fileIds' }
                      )}
                      {...props}
                      fileList={getFieldValue('file')}
                    >
                      <Button style={{ marginLeft: 8 }} type="primary" icon="upload">
                        导入
                      </Button>
                    </Upload>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={onAdd}>
              新建
            </Button>
          </div>
          <Table
            columns={columns}
            dataSource={reservePoint.list}
            rowKey={record => record.id}
            loading={loading}
            pagination={pagination}
          />
        </div>
      </Card>
      <AddEditModal {...AddEditModalProps} />
      <ShowFileModal {...AddEditModalProps} />
      <LocationModalGen />
    </PageHeaderLayout>
  );
}

function mapStateToProps(state) {
  return {
    reservePoint: state.reservePoint,
    loading: state.loading.models.reservePoint,
  };
}

ReservePoint = Form.create()(ReservePoint);

export default connect(mapStateToProps)(ReservePoint);
