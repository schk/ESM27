import React from 'react';
import {
  Form,
  Input,
  Modal,
  Button,
  Radio,
  Row,
  upload,
  Icon,
  Col,
  Select,
  TreeSelect,
} from 'antd';
import { connect } from 'dva';
import { baseFileUrl } from '../../../config/system';
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const Option = Select.Option;
const { TextArea } = Input;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

let AddEditModal = ({ reservePoint, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: '查看物资储备点详情信息',
    visible: reservePoint.findModalVisible,
    maskClosable: false,
    width: 600,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
    ],
  };
  if (!reservePoint.findModalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'reservePoint/updateState',
      payload: {
        findModalVisible: false,
        locationItem: {},
      },
    });
  }
  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={24}>
            <FormItem label="储备点名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
              })(<Input type="text" readOnly placeholder="储备点名称" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem {...formItemLayout} label="所属单位">
              {getFieldDecorator('type', {
                initialValue: item.type == null ? 1 : item.type,
              })(
                <RadioGroup>
                  <Radio value={1}>重点单位</Radio>
                  <Radio value={2}>消防战队</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
        </Row>

        {reservePoint.type == 1 ? (
          <Row>
            <Col span={24}>
              <FormItem label="所属单位:" hasFeedback {...formItemLayout}>
                {getFieldDecorator('keyUnitName', {
                  initialValue: item.keyUnitName == null ? undefined : item.keyUnitName + '',
                })(<Input type="text" readOnly placeholder="所属单位" />)}
              </FormItem>
            </Col>
          </Row>
        ) : (
          <Row>
            <Col span={24}>
              <FormItem {...formItemLayout} label="所属消防队" hasFeedback>
                {getFieldDecorator('fireBrigadeName', {
                  initialValue:
                    item.fireBrigadeName == null
                      ? reservePoint.fireBrigadeId
                      : item.fireBrigadeName + '',
                })(<Input type="text" readOnly placeholder="所属消防队" />)}
              </FormItem>
            </Col>
          </Row>
        )}
        <Row>
          <Col span={24}>
            <FormItem label="储备点性质:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('properties', {
                initialValue: item.properties,
              })(<Input tyep="text" readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="负责人:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('charge', {
                initialValue: item.charge,
              })(<Input tyep="text" readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="负责人联系方式:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('chargePhone', {
                initialValue: item.chargePhone,
              })(<Input tyep="text" readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="经纬度:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('lonLat', {
                initialValue: reservePoint.locationItem.lonLat
                  ? reservePoint.locationItem.lonLat + ''
                  : undefined,
              })(<Input type="text" readOnly />)}
            </FormItem>
          </Col>
        </Row>
        {/* <Row>
           <Col span={24}>
            <FormItem label="地图图标:" {...formItemLayout}>
              {reservePoint.locationItem.dtPath?
                <div style={{borderRadius: '4px',border: '1px solid #d9d9d9',height: '64px',position: 'relative',clear: 'both',overflow: 'hidden',}}>
                <div style={{width: '60px',height: '60px',textAlign: 'center',border: '1px solid #d9d9d9', position: 'relative', borderRadius: '5px'}}>
                  <img style={{position: 'absolute',left: '0',right: '0',bottom: '0',top: '0',margin: 'auto', width: '20px', height: '20px',}}
                    src={baseFileUrl + reservePoint.locationItem.dtPath} alt=""
                  />
                </div>
              </div> :""}                    
            </FormItem>
           </Col>
        </Row> */}
        <Row>
          <Col span={24}>
            <FormItem label="储备点地址:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('address', {
                initialValue: item.address,
              })(<TextArea autosize={{ minRows: 2, maxRows: 2 }} readOnly placeholder="无" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="储备点描述:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('remark', {
                initialValue: item.remark,
              })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { reservePoint: state.reservePoint };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
