import React from 'react';
import {
  Form,
  Input,
  Modal,
  Button,
  Radio,
  Row,
  DatePicker,
  Col,
  Select,
  TreeSelect,
  InputNumber,
} from 'antd';
import { connect } from 'dva';
const Option = Select.Option;
const FormItem = Form.Item;
import moment from 'moment';
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

let AddEditModal = ({ extinguisher, item = {}, form, dispatch }) => {
  const { getFieldDecorator, validateFields, getFieldsValue, resetFields } = form;

  const modalOpts = {
    title: extinguisher.modalType == 'create' ? '新建灭火剂' : '修改灭火剂',
    visible: extinguisher.modalVisible,
    maskClosable: false,
    width: 600,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={extinguisher.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!extinguisher.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'extinguisher/updateState',
      payload: {
        modalVisible: false,
      },
    });
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = getFieldsValue();
      data.id = extinguisher.modalType === 'create' ? '' : item.id;
      dispatch({
        type: `extinguisher/${extinguisher.modalType}`,
        payload: data,
        search: extinguisher.selectObj,
      });
    });
  }

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  const loop = data =>
    data.map(d => {
      let children = null;
      if (d.children != null && d.children.length > 0) {
        children = loop(d.children);
      }
      return { title: d.name, value: d.id_, key: d.id_, children, disabled: item.id_ === d.id_ };
    });

  const checkConfirm = (rule, value, callback) => {
    if (!value && $.trim(value) == '') {
      callback('请填写灭火剂配备量');
    }
    if (value > 99999999) {
      callback('灭火剂配备量不能超过99999999KG');
    } else {
      callback();
    }
  };

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={24}>
            <FormItem label="灭火剂类型:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('typeId', {
                initialValue: item.typeId ? item.typeId + '' : undefined,
                rules: [{ required: true, message: '请选择灭火剂类型' }],
              })(
                <Select placeholder="请选择灭火剂类型">{loopOption(extinguisher.dicList)}</Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="所属消防队:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('fireBrigadeId', {
                initialValue: item.fireBrigadeId
                  ? item.fireBrigadeId + ''
                  : extinguisher.brigadeId + '',
                rules: [{ required: true, message: '请填写所属消防队' }],
              })(
                <TreeSelect
                  showSearch
                  treeNodeFilterProp="title"
                  dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                  placeholder="请选择所属消防队"
                  notFoundContent="无匹配结果"
                  allowClear
                  treeData={loop(extinguisher.fireBrigadeTree)}
                  treeDefaultExpandAll
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="配备数量:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('quantity', {
                initialValue: item.quantity ? item.quantity + '' : undefined,
                rules: [
                  {
                    required: true,
                    validator: checkConfirm,
                  },
                ],
              })(<InputNumber style={{ width: '200px' }} placeholder="请填写配备数量" />)}
            </FormItem>
            <span style={{ position: 'absolute', right: '180px', top: '8px' }}>(KG)</span>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="生产日期:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('productDate', {
                initialValue: item.productDate ? new moment(item.productDate) : undefined,
                rules: [{ required: true, message: '请选择生产日期' }],
              })(<DatePicker placeholder="请选择生产日期" style={{ width: 250 }} />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="有效日期:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('effectiveDate', {
                initialValue: item.effectiveDate ? new moment(item.effectiveDate) : undefined,
                rules: [{ required: true, message: '请选择有效日期' }],
              })(<DatePicker placeholder="请选择有效日期" style={{ width: 250 }} />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { extinguisher: state.extinguisher };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
