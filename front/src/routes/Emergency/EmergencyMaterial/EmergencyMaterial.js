import React, { Fragment } from 'react';
import {
  Table,
  Form,
  Button,
  Upload,
  Popconfirm,
  Card,
  Input,
  Divider,
  Row,
  Col,
  Select,
  Tree,
  Tag,
  Icon,
  Modal,
} from 'antd';
import { message } from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import ShowFileModal from './ShowFileModal';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import { baseUrl } from '../../../config/system';
const confirm = Modal.confirm;
const Option = Select.Option;
const TreeNode = Tree.TreeNode;

function EmergencyMaterial({ location, emergencyMaterial, form, dispatch, loading }) {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldProps,
    getFieldValue,
    setFieldsValue,
    resetFields,
  } = form;
  const AddEditModalProps = {
    item: emergencyMaterial.item,
  };

  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 80,
      fixed: 'left',
      render: (value, row, index) => {
        return index + 1;
      },
    },
    {
      title: '物资名称',
      dataIndex: 'name',
      key: 'name',
      width: 150,
      fixed: 'left',
      render: (text, record, value) => (
        <Fragment>
          <a onClick={() => onFindInfo(record.id_)}>{record.name}</a>
        </Fragment>
      ),
    },
    { title: '所属单位', dataIndex: 'keyUnitName', key: 'keyUnitName', width: 200 },
    { title: '物资储备点', dataIndex: 'reservePointName', key: 'reservePointName ', width: 200 },
    { title: '所属分类', dataIndex: 'typeName', key: 'typeName', width: 200 },
    { title: '规格', dataIndex: 'specifications', key: 'specifications', width: 150 },
    { title: '型号', dataIndex: 'model', key: 'model', width: 150 },
    {
      title: '存储数量',
      dataIndex: 'storageQuantity',
      key: 'storageQuantity',
      width: 100,
      render: (value, text, record) => {
        return value ? <Tag color="geekblue">{value}</Tag> : '';
      },
    },
    { title: '计量单位', dataIndex: 'storageUnit', key: 'storageUnit', width: 100 },
    { title: '负责人', dataIndex: 'chargePerson', key: 'chargePerson', width: 100 },
    { title: '尺寸大小', dataIndex: 'size', key: 'size', width: 150 },
    { title: '电话', dataIndex: 'phone', key: 'phone', width: 150 },
    {
      title: '是否可用',
      dataIndex: 'available',
      key: 'available',
      width: 100,
      render: (text, record) => {
        if (record.available == 1) {
          return <Tag color="green">可用</Tag>;
        }
        if (record.available == 0) {
          return <Tag color="red">不可用</Tag>;
        }
      },
    },
    {
      title: '智能推荐',
      dataIndex: 'intelligentRecommendation',
      key: 'intelligentRecommendation',
      width: 150,
      render: (text, record) => (
        <Fragment>
          {record.intelligentRecommendation==1?<span><Tag color="pink">荐</Tag>{record.disasterType}</span>:""}
        </Fragment>
      ),
    },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 150,
      fixed: 'right',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_)}>修改</a>
          <Divider type="vertical" />
          {/* <a onClick={() => onFindInfo(record.id_)}>查看</a>
          <Divider type="vertical" /> */}
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];
  const onFindInfo = id => {
    dispatch({
      type: 'emergencyMaterial/EmergencyMaterialInfo',
      payload: id,
    });
  };
  function onUpdate(id) {
    dispatch({
      type: 'emergencyMaterial/info',
      payload: id,
    });
  }

  function onAdd() {
    dispatch({
      type: 'emergencyMaterial/updateState',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'emergencyMaterial/del',
      payload: id,
      search: {
        departmentId: emergencyMaterial.departmentId,
        pageNum: emergencyMaterial.current,
        pageSize: emergencyMaterial.pageSize,
        ...getFieldsValue(),
      },
    });
  }

  function handleSearch() {
    dispatch({
      type: 'emergencyMaterial/qryListByParams',
      payload: {
        pageNum: 1,
        pageSize: emergencyMaterial.pageSize,
        ...getFieldsValue(),
        departmentId: emergencyMaterial.departmentId,
      },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'emergencyMaterial/qryListByParams',
      payload: {
        reservePoint: 1,
        pageSize: emergencyMaterial.pageSize,
        departmentId: emergencyMaterial.departmentId,
      },
    });
  }

  function downloadTemplate() {
    window.open(baseUrl + '/execl/downLoadTemplate.jhtml?type=emergencyMaterial');
  }

  function callback(key) {
    console.log(key);
  }

  const loop = data =>
    data.map(item => {
      if (item.children && item.children.length) {
        return (
          <TreeNode
            key={item.id_}
            title={item.name}
            icon={
              <Icon
                style={{
                  width: '10px',
                  height: '10px',
                  background: 'url(images/liBgIcon.png)',
                  display: 'inlineBlock',
                  verticalAlign: '1px',
                }}
              />
            }
          >
            {loop(item.children)}
          </TreeNode>
        );
      }
      return (
        <TreeNode
          key={item.id_}
          title={item.name}
          icon={
            <Icon
              style={{
                width: '10px',
                height: '10px',
                background: 'url(images/liBgIcon.png)',
                display: 'inlineBlock',
                verticalAlign: '1px',
              }}
            />
          }
        />
      );
    });

  const onSelect = info => {
    if (info.length > 0) {
      dispatch({
        type: 'emergencyMaterial/qryListByParams',
        payload: { ...getFieldsValue(), ...{ reservePointId: info[0] } },
      });
    } else {
      dispatch({
        type: 'emergencyMaterial/qryListByParams',
        payload: { ...getFieldsValue(), ...{ reservePointId: undefined } },
      });
    }
  };

  const pagination = {
    current: emergencyMaterial.current,
    pageSize: emergencyMaterial.pageSize,
    total: emergencyMaterial.total,
    showSizeChanger: true,
    showTotal: total => '共' + emergencyMaterial.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'emergencyMaterial/qryListByParams',
        payload: {
          pageNum: current,
          pageSize: size,
          ...getFieldsValue(),
          reservePointId: emergencyMaterial.reservePointId,
        },
      });
    },
    onChange(current) {
      dispatch({
        type: 'emergencyMaterial/qryListByParams',
        payload: {
          pageNum: current,
          pageSize: emergencyMaterial.pageSize,
          ...getFieldsValue(),
          reservePointId: emergencyMaterial.reservePointId,
        },
      });
    },
  };

  function handleTableChange(pagination, filters, sorter) {
    let isChinese = ['account', 'name'].indexOf(sorter.field) >= 0;
    dispatch({
      type: 'emergencyMaterial/qryListByParams',
      payload: {
        pageNum: pagination.current,
        pageSize: pagination.pageSize,
        ...getFieldsValue(),
        orderBy: sorter.field,
        order: sorter.order,
        isChinese,
      },
    });
  }

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  function initLoopMsg(values) {
    return values != null && values.length > 0
      ? values.map((k, index) => {
          return <div key={index}>{k}</div>;
        })
      : null;
  }

  const props = {
    name: 'file',
    action: baseUrl + '/execl/importExcelData.jhtml?type=EmergencyMaterial', //对应后台VLDEmergencyMaterialExcel 方法
    beforeUpload(file) {
      // const isJPG = file.type === 'application/vnd.ms-excel' || file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
      // if (!isJPG) {
      //   message.error('只能上传EXCEL文件！');
      // }
      // return isJPG;
    },
    onChange(info) {
      let fileList = info.fileList;
      if (info.file.status === 'done') {
        if (info.file.response.httpCode == 200) {
          if (info.file.response.errorCode == 1) {
            //弹出确认取消框
            confirm({
              title: '信息提示框',
              content: initLoopMsg(info.file.response.data),
              okText: '继续导入',
              cancelText: '取消导入',
              onOk() {
                dispatch({
                  type: 'emergencyMaterial/importConfirm',
                  payload: info.file.response.times,
                });
              },
              onCancel() {
                dispatch({
                  type: 'emergencyMaterial/delTemp',
                  payload: info.file.response.times,
                });
              },
            });
          } else if (info.file.response.errorCode == 2) {
            Modal.error({
              title: '导入提示',
              content: initLoopMsg(info.file.response.data),
            });
          } else {
            message.success(`${info.file.name} 上传成功`);
            dispatch({
              type: 'emergencyMaterial/qryListByParams',
              payload: { pageNum: 1, pageSize: emergencyMaterial.pageSize, ...getFieldsValue() },
            });
          }
        } else {
          Modal.error({
            title: '导入提示',
            content: info.file.response.msg,
          });
        }
        fileList = fileList.filter(file => {
          return file.response && file.response.fid;
        });
        //只保留最后一条记录
        fileList = fileList.slice(-1);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`);
      }
      let fildsValue = {};
      fildsValue['file'] = fileList.map(file => {
        return file;
      });
      setFieldsValue(fildsValue);
    },
  };
  return (
    <PageHeaderLayout>
      <div style={{ backgroundColor: '#fff', position: 'relative' }}>
        <div style={{ width: '250px', float: 'left', position: 'absolute', bottom: '0', top: '0' }}>
          <div
            style={{
              width: '100%',
              height: '40px',
              backgroundColor: '#232f40',
              paddingLeft: '20px',
              lineHeight: '40px',
              color: '#fff',
              fontSize: '14px',
              fontWeight: '400',
            }}
          >
            物资储备点
          </div>
          <div
            style={{
              position: 'absolute',
              top: '40px',
              bottom: '0px',
              overflow: 'hidden',
              overflowY: 'auto',
              width: '100%',
            }}
          >
            <Tree defaultExpandedKeys={['1']} onSelect={onSelect} showIcon>
              {loop(emergencyMaterial.reservePointList)}
            </Tree>
          </div>
        </div>
        <div
          style={{
            marginLeft: '250px',
            paddingTop: '0px',
            minHeight: '600px',
            borderLeft: '1px solid #e8e8e8',
          }}
        >
          <Card bordered={false}>
            <div className={styles.tableList}>
              <div className={styles.tableListForm}>
                <Form layout="inline">
                  <Row gutter={{ md: 8, lg: 24, xl: 24 }}>
                    <Col span={5} sm={5}>
                      <Form.Item label="物资名称：">
                        {getFieldDecorator('name')(<Input placeholder="输入名称查找" />)}
                      </Form.Item>
                    </Col>
                    <Col span={7} sm={6}>
                      <Form.Item label="所属分类：">
                        {getFieldDecorator('typeId')(
                          <Select allowClear placeholder="请选择所属分类">
                            <Option value="">全部</Option>
                            {loopOption(emergencyMaterial.typeList)}
                          </Select>
                        )}
                      </Form.Item>
                    </Col>

                    <Col md={5} sm={4}>
                      <span className={styles.submitButtons}>
                        <Button type="primary" htmlType="submit" onClick={handleSearch}>
                          查询
                        </Button>
                        <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                          重置
                        </Button>
                        <Button
                          style={{ marginLeft: 80 }}
                          type="primary"
                          icon="download"
                          onClick={downloadTemplate}
                        >
                          下载模板
                        </Button>
                        {/* <a href={"template/应急专家.xls"} > <Button style={{ marginLeft: 80 }} type="primary" icon="download"  >下载模板</Button></a> */}
                        <Upload
                          {...getFieldProps(
                            'file',
                            {
                              validate: [
                                {
                                  rules: [
                                    { type: 'array', required: true, message: '请添加数据文件' },
                                  ],
                                  trigger: 'onBlur',
                                },
                              ],
                            },
                            { valuePropName: 'fileIds' }
                          )}
                          {...props}
                          fileList={getFieldValue('file')}
                        >
                          <Button style={{ marginLeft: 8 }} type="primary" icon="upload">
                            导入
                          </Button>
                        </Upload>
                      </span>
                    </Col>
                  </Row>
                </Form>
              </div>
              <div className={styles.submitButtons}>
                <Button icon="plus" type="primary" onClick={onAdd}>
                  新增
                </Button>
              </div>
              <div style={{ width: '100%', paddingRight: '20px' }}>
                <Table
                  columns={columns}
                  dataSource={emergencyMaterial.list}
                  style={{ marginTop: 10 }}
                  pagination={pagination}
                  loading={emergencyMaterial.loading}
                  rowKey={record => record.id_}
                  scroll={{ x: 2250 }}
                />
              </div>
            </div>
          </Card>
        </div>
      </div>
      <AddEditModal {...AddEditModalProps} />
      <ShowFileModal {...AddEditModalProps} />
    </PageHeaderLayout>
  );
}

function mapStateToProps(state) {
  return {
    emergencyMaterial: state.emergencyMaterial,
    loading: state.loading.models.emergencyMaterial,
  };
}

EmergencyMaterial = Form.create()(EmergencyMaterial);

export default connect(mapStateToProps)(EmergencyMaterial);
