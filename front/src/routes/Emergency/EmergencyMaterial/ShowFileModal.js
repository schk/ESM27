import React from 'react';
import { Form, Input, Modal, Button, Radio, Row, Col, Tag } from 'antd';
import { connect } from 'dva';
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const { TextArea } = Input;

const formItemLayout = {
  labelCol: { span: 5 },
  wrapperCol: { span: 17 },
};

let AddEditModal = ({ emergencyMaterial, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: '查看信息',
    visible: emergencyMaterial.findModalVisible,
    maskClosable: false,
    width: 1000,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
    ],
  };

  if (!emergencyMaterial.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'emergencyMaterial/updateState',
      payload: {
        findModalVisible: false,
      },
    });
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <FormItem style={{ marginBottom: '0px' }}>
          {getFieldDecorator('keyUnitId', { initialValue: item.keyUnitId })(
            <Input type="hidden" />
          )}
        </FormItem>
        <Row>
          <Col span={12}>
            <FormItem label="物资名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '请填写物资名称' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" readOnly placeholder="请填写物资名称" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="物资储备点:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('reservePointName', {
                initialValue: item.reservePointName == null ? undefined : item.reservePointName,
                rules: [{ required: true, message: '请选择物资储备点' }],
              })(<Input type="text" readOnly placeholder="" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="所属单位:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('keyUnitName', {
                initialValue: emergencyMaterial.keyUnitName,
                rules: [{ required: true, message: '请选择所属单位' }],
              })(<Input type="text" readOnly placeholder="" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="物资类型：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('typeName', {
                initialValue: item.typeName,
                rules: [{ required: true, message: '请选择物资类型' }],
              })(<Input type="text" readOnly placeholder="" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label={<Tag color="geekblue">储存数量</Tag>} hasFeedback {...formItemLayout}>
              {getFieldDecorator('storageQuantity', {
                initialValue: item.storageQuantity,
                rules: [
                  { required: true, message: '请填写储存数量' },
                  { max: 10, message: '最长不超过10个字符' },
                ],
              })(<Input type="text" readOnly placeholder="请填写储存数量" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="储存单位：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('storageUnit', {
                initialValue: item.storageUnit,
                rules: [{ max: 10, message: '最长不超过10个字符' }],
              })(<Input type="text" readOnly placeholder="请填写储存单位" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="规格：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('specifications', {
                initialValue: item.specifications,
                rules: [{ max: 50, message: '最长不超过50个字符' }],
              })(<Input type="text" readOnly placeholder="请填写规格" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="型号：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('model', {
                initialValue: item.model,
                rules: [{ max: 50, message: '最长不超过50个字符' }],
              })(<Input type="text" readOnly placeholder="请填写型号" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="负责人：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('chargePerson', {
                initialValue: item.chargePerson,
                rules: [{ max: 20, message: '最长不超过20个字符' }],
              })(<Input type="text" readOnly placeholder="请填写负责人" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="电话：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('phone', {
                initialValue: item.phone,
                rules: [{ max: 20, message: '最长不超过20个字符' }],
              })(<Input type="text" readOnly placeholder="请填写电话" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="尺寸大小：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('size', {
                initialValue: item.size,
                rules: [{ max: 100, message: '最长不超过100个字符' }],
              })(<Input type="text" readOnly placeholder="请填写尺寸大小" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="是否可用：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('available', {
                initialValue: item.available == null ? 1 : item.available,
              })(
                <RadioGroup style={{ width: '200px' }}>
                  <Radio value={1}>可用</Radio>
                  <Radio value={0}>不可用</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="是否智能推荐：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('intelligentRecommendation', {
                initialValue: item.intelligentRecommendation == null ? 0 : item.intelligentRecommendation,
              })(
                <RadioGroup style={{ width: '100%' }}>
                  <Radio value={1}>是</Radio>
                  <Radio value={0}>否</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
          {getFieldValue('intelligentRecommendation')===1?
          <Col span={12}>
            <FormItem label="灾害类型：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('disasterType', {
                initialValue: item.disasterType ? item.disasterType: undefined,
              })(
                <Input type="text" readOnly />
              )}
            </FormItem>
          </Col>
          :""}
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="物质用途：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('purpose', {
                initialValue: item.purpose,
              })(<TextArea autosize={{ minRows: 2, maxRows: 4 }} readOnly />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="物资描述：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('remark', {
                initialValue: item.remark,
              })(<TextArea autosize={{ minRows: 2, maxRows: 4 }} readOnly />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { emergencyMaterial: state.emergencyMaterial };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
