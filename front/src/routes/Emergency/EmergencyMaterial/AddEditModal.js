import React from 'react';
import {
  Form,
  Input,
  Modal,
  Button,
  Radio,
  Row,
  Col,
  Select,
  TreeSelect,
  Tag,
  InputNumber,
} from 'antd';
import { connect } from 'dva';
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const Option = Select.Option;
const { TextArea } = Input;

const formItemLayout = {
  labelCol: { span: 5 },
  wrapperCol: { span: 17 },
};

let AddEditModal = ({ emergencyMaterial, item = {}, form, dispatch }) => {
  const { getFieldDecorator, validateFields, getFieldsValue,getFieldValue, resetFields } = form;

  const modalOpts = {
    title: emergencyMaterial.modalType == 'create' ? '新建应急物资' : '修改应急物资',
    visible: emergencyMaterial.modalVisible,
    maskClosable: false,
    width: 1000,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={emergencyMaterial.buttomLoading}
      >
        保存
      </Button>,
    ],
  };
  if (!emergencyMaterial.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'emergencyMaterial/updateState',
      payload: {
        modalVisible: false,
      },
    });
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = getFieldsValue();
      data.id = emergencyMaterial.modalType === 'create' ? '' : item.id;
      dispatch({
        type: `emergencyMaterial/${emergencyMaterial.modalType}`,
        payload: data,
        search: emergencyMaterial.selectObj,
      });
    });
  }

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  const treeData = i => {
    return i.map(d => {
      d.title = d.name;
      d.key = d.id_ + '';
      d.value = d.id_ + '';
      d.children = d.children && d.children.length > 0 ? treeData(d.children) : [];
      return d;
    });
  };
  const handleFireBrigadeChange = value => {
    form.setFieldsValue({ reservePointId: undefined });
    dispatch({
      type: 'emergencyMaterial/qryReserveByType',
      payload: { keyUnitId: value, type: emergencyMaterial.type },
    });
  };

  //根据储备点取重点单位名称
  const changeRadio = e => {
    form.setFieldsValue({ reservePointId: undefined });
    form.setFieldsValue({ keyUnitId: undefined });
    dispatch({
      type: 'emergencyMaterial/updateState',
      payload: { type: e.target.value },
    });
  };

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={12}>
            <FormItem label="物资名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '请填写物资名称' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写物资名称" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="物资类型：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('typeId', {
                initialValue: item.typeId,
                rules: [{ required: true, message: '请选择物资类型' }],
              })(
                <Select placeholder="请选择物资类型">
                  {loopOption(emergencyMaterial.typeList)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem required label="是否可用：" {...formItemLayout}>
              {getFieldDecorator('available', {
                initialValue: item.available == null ? 1 : item.available,
              })(
                <RadioGroup style={{ width: '100%' }}>
                  <Radio value={1}>可用</Radio>
                  <Radio value={0}>不可用</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem {...formItemLayout} label="所属单位">
              {getFieldDecorator('type', {
                initialValue: item.type == null ? 1 : item.type,
              })(
                <RadioGroup onChange={changeRadio}>
                  <Radio value={1}>重点单位</Radio>
                  <Radio value={2}>消防队</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          {emergencyMaterial.type == 1 ? (
            <Col span={12}>
              <FormItem label="所属单位:" hasFeedback {...formItemLayout}>
                {getFieldDecorator('keyUnitId', {
                  initialValue: item.keyUnitId ? item.keyUnitId + '' : undefined,
                  rules: [{ required: true, message: '请选择所属单位' }],
                })(
                  <Select
                    showSearch
                    optionFilterProp="children"
                    placeholder="请选择所属单位"
                    onChange={handleFireBrigadeChange}
                  >
                    {loopOption(emergencyMaterial.keyUnitList)}
                  </Select>
                )}
              </FormItem>
            </Col>
          ) : (
            <Col span={12}>
              <FormItem {...formItemLayout} label="所属消防队" hasFeedback>
                {getFieldDecorator('keyUnitId', {
                  initialValue: item.keyUnitId ? item.keyUnitId + '' : undefined,
                  rules: [{ required: true, whitespace: true, message: '请选择所属消防队' }],
                })(
                  <TreeSelect
                    showSearch
                    treeNodeFilterProp="title"
                    dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                    placeholder="请选择消防队"
                    notFoundContent="无匹配结果"
                    allowClear
                    treeData={treeData(emergencyMaterial.fireBrigadeTree)}
                    onChange={handleFireBrigadeChange}
                    treeDefaultExpandAll
                  />
                )}
              </FormItem>
            </Col>
          )}
          <Col span={12}>
            <FormItem label="物资储备点:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('reservePointId', {
                initialValue: item.reservePointId ? item.reservePointId + '' : undefined,
                rules: [{ required: true, message: '请选择物资储备点' }],
              })(
                <Select placeholder="请选择物资储备点">
                  {loopOption(emergencyMaterial.reservePointKeyIdList)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label={<Tag color="geekblue">储存数量</Tag>} hasFeedback {...formItemLayout}>
              {getFieldDecorator('storageQuantity', {
                initialValue: item.storageQuantity,
                rules: [{ required: true, message: '请填写储存数量' }],
              })(
                <InputNumber
                  min={1}
                  max={99999999}
                  style={{ width: '100%' }}
                  placeholder="请填写储存数量"
                />
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="储存单位：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('storageUnit', {
                initialValue: item.storageUnit,
                rules: [{ max: 10, message: '最长不超过10个字符' }],
              })(<Input type="text" placeholder="请填写储存单位" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="规格：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('specifications', {
                initialValue: item.specifications,
                rules: [{ max: 50, message: '最长不超过50个字符' }],
              })(<Input type="text" placeholder="请填写规格" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="型号：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('model', {
                initialValue: item.model,
                rules: [{ max: 50, message: '最长不超过50个字符' }],
              })(<Input type="text" placeholder="请填写型号" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="负责人：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('chargePerson', {
                initialValue: item.chargePerson,
                rules: [{ max: 20, message: '最长不超过20个字符' }],
              })(<Input type="text" placeholder="请填写负责人" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="电话：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('phone', {
                initialValue: item.phone,
                rules: [{ max: 20, message: '最长不超过20个字符' }],
              })(<Input type="text" placeholder="请填写电话" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="尺寸大小：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('size', {
                initialValue: item.size,
                rules: [{ max: 100, message: '最长不超过100个字符' }],
              })(<Input type="text" placeholder="请填写尺寸大小" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="物资用途：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('purpose', {
                initialValue: item.purpose,
                rules: [{ max: 50, message: '最长不超过50个字符' }],
              })(<Input type="text" placeholder="请填写物资用途" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="是否智能推荐：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('intelligentRecommendation', {
                initialValue: item.intelligentRecommendation == null ? 0 : item.intelligentRecommendation,
              })(
                <RadioGroup style={{ width: '100%' }}>
                  <Radio value={1}>是</Radio>
                  <Radio value={0}>否</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
          {getFieldValue('intelligentRecommendation')===1?
          <Col span={12}>
            <FormItem label="灾害类型：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('disasterType', {
                initialValue: item.disasterType ? JSON.parse(item.disasterType): undefined,
                rules: [{ required: true, message: '请选择灾害类型' }],
              })(
                <Select placeholder="请选择灾害类型" mode="multiple" allowClear>
                  <Option value="废油池流淌火">废油池流淌火</Option>
                  <Option value="油罐火灾">油罐火灾</Option>
                  <Option value="气体泄漏">气体泄漏</Option>
                  <Option value="爆炸">爆炸</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          :""}
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="物资描述：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('remark', {
                initialValue: item.remark,
                rules: [{ max: 50, message: '最长不超过50个字符' }],
              })(<TextArea autosize={{ minRows: 2, maxRows: 4 }} placeholder="请填写物资描述" />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { emergencyMaterial: state.emergencyMaterial };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
