import React from 'react';
import { Form, Input, Modal, Button, upload, Icon, Radio, Row, Col } from 'antd';
import { connect } from 'dva';
import { baseFileUrl } from '../../../config/system';

const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const { TextArea } = Input;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 16,
  },
};
const formItemLayout1 = {
  labelCol: {
    span: 3,
  },
  wrapperCol: {
    span: 20,
  },
};

let AddEditModal = ({ fireBrigade, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
    getFieldProps,
  } = form;

  const modalOpts = {
    title: '查看消防队信息',
    visible: fireBrigade.findModalVisible,
    maskClosable: false,
    width: 800,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
    ],
  };

  if (!fireBrigade.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'fireBrigade/updateState',
      payload: {
        findModalVisible: false,
        locationItem: {},
      },
    });
  }
  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={12}>
            <FormItem label="消防队名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
              })(<Input type="text" readOnly placeholder="消防队名称" />)}
            </FormItem>
          </Col>
          <Col span={12} style={{ height: '56px' }}>
            <FormItem label="上级消防队：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('parentName', {
                initialValue: item.parentName ? item.parentName : '无上级消防队',
              })(<Input type="text" readOnly placeholder="上级消防队" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="责任人:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('charge', {
                initialValue: item.charge,
              })(<Input type="text" readOnly placeholder="消防队责任人" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="联系方式:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('phone', {
                initialValue: item.phone,
              })(<Input type="text" readOnly placeholder="责任人联系方式" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="消防队位置:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('position', {
                initialValue: item.position,
              })(<Input type="text" readOnly placeholder="消防队位置" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="消防队性质:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('properties', {
                initialValue: item.properties,
              })(<Input type="text" readOnly placeholder="消防队性质" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="经纬度:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('lonLat', {
                initialValue: fireBrigade.locationItem.lonLat
                  ? fireBrigade.locationItem.lonLat + ''
                  : undefined,
              })(<Input type="text" readOnly placeholder="经纬度" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="站级别:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('stationLevel', {
                initialValue: item.stationLevel,
                rules: [
                  { required: true, message: '请填写站级别' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写站级别" />)}
            </FormItem>
          </Col>
          {/* <Col span={12}>
              <FormItem label="地图图标:" {...formItemLayout}>
                <Input type="hidden" {...getFieldProps('dtPath', { initialValue: fireBrigade.locationItem.dtPath })} />
                {fireBrigade.locationItem.dtPath?
                  <div style={{borderRadius: '4px',border: '1px solid #d9d9d9',height: '64px',position: 'relative',clear: 'both',overflow: 'hidden',}}>
                  <div style={{width: '60px',height: '60px',textAlign: 'center',border: '1px solid #d9d9d9', position: 'relative', borderRadius: '5px'}}>
                    <img style={{position: 'absolute',left: '0',right: '0',bottom: '0',top: '0',margin: 'auto', width: '20px', height: '20px',}}
                      src={baseFileUrl + fireBrigade.locationItem.dtPath} alt=""
                    />
                  </div>
                </div> :""}                    
              </FormItem>
          </Col> */}
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="救援能力:" hasFeedback {...formItemLayout1}>
              {getFieldDecorator('rescueAbility', {
                initialValue: item.rescueAbility,
              })(<TextArea autosize={{ minRows: 2, maxRows: 4 }} readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="消防队规模:" hasFeedback {...formItemLayout1}>
              {getFieldDecorator('scale', {
                initialValue: item.scale,
              })(<TextArea autosize={{ minRows: 2, maxRows: 4 }} readOnly />)}
            </FormItem>
          </Col>
        </Row>

        <Row>
          <Col span={24}>
            <FormItem label="辖区概况:" hasFeedback {...formItemLayout1}>
              {getFieldDecorator('generalSituation', {
                initialValue: item.generalSituation,
              })(<TextArea autosize={{ minRows: 2, maxRows: 4 }} readOnly />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="消防队描述:" hasFeedback {...formItemLayout1}>
              {getFieldDecorator('remark', {
                initialValue: item.remark,
              })(<TextArea autosize={{ minRows: 2, maxRows: 4 }} readOnly />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { fireBrigade: state.fireBrigade };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
