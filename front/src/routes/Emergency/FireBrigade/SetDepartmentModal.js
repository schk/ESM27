import React, { PropTypes } from 'react';
import { Form, Input, Modal, Select, Tree, TreeSelect } from 'antd';
import { connect } from 'dva';
const FormItem = Form.Item;
const TreeNode = Tree.TreeNode;
const Option = Select.Option;

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};

let SetDepartmentModal = ({ fireBrigade, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: '组织机构设置',
    visible: fireBrigade.setDepartmentModalVisible,
    maskClosable: false,
    confirmLoading: fireBrigade.setDepartmentLoading,
    onOk: handleOk,
    onCancel: handleCancel,
  };

  function handleCancel() {
    dispatch({
      type: 'fireBrigade/updateState',
      payload: {
        setDepartmentModalVisible: false,
      },
    });
  }

  if (!fireBrigade.setDepartmentModalVisible) {
    resetFields();
  }
  console.log(item);

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = getFieldsValue();
      data.fireBrigadeId = item.fireBrigadeId;
      dispatch({
        type: `fireBrigade/setDepartment`,
        payload: data,
        search: { pageNum: fireBrigade.current, pageSize: fireBrigade.pageSize },
      });
    });
  }

  const treeData = i => {
    return i.map(d => {
      d.label = d.name;
      d.key = d.id_ + '';
      d.value = d.id + '';
      d.children = d.children && d.children.length > 0 ? treeData(d.children) : [];
      return d;
    });
  };
  return (
    <Modal {...modalOpts}>
      <div>
        <Form layout="horizontal">
          <FormItem label="战队名称：" hasFeedback {...formItemLayout}>
            {getFieldDecorator('fireBrigadeName', { initialValue: item.fireBrigadeName })(
              <Input disabled={true} type="text" />
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="所属机构" hasFeedback>
            {getFieldDecorator('departmentId', {
              initialValue: item.departmentId ? item.departmentId + '' : undefined,
              rules: [
                {
                  required: true,
                  whitespace: true,
                  message: '请选择组织机构',
                },
              ],
            })(
              <TreeSelect
                showSearch
                treeNodeFilterProp="title"
                dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                placeholder="请选择组织机构"
                notFoundContent="无匹配结果"
                allowClear
                treeData={treeData(fireBrigade.deptNoTopTree)}
                treeDefaultExpandAll
              />
            )}
          </FormItem>
        </Form>
      </div>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { fireBrigade: state.fireBrigade };
}

SetDepartmentModal = Form.create()(SetDepartmentModal);

export default connect(mapStateToProps)(SetDepartmentModal);
