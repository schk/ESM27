import React from 'react'
import {
    Table,
    Row,
    Form,
    Button,
    Popconfirm,
    Card,
    Input,
    Divider,
    message,
    Upload,
    Col,
    Select,
    Tag,
    DatePicker,
    Modal,
  } from 'antd';

import { connect } from 'dva';
import styles from '../../../common/common.less';
const Option = Select.Option;
const FormItem = Form.Item;
const RangePicker = DatePicker.RangePicker;
import moment from 'moment';
const confirm = Modal.confirm;


const columns = [
    {
      title: '预案名称',
      dataIndex: 'name',
      key: 'name',
      width: 120,
    },
    { title: '预案类型', dataIndex: 'typeName', key: 'typeName', width: 100 },
    {
      title: '预案级别',
      dataIndex: 'level',
      key: 'level',
      width: 80,
      render: (value, row, index) => {
        return value == 1 ? 'Ⅰ级' : value == 2 ? 'Ⅱ级' : value == 3 ? 'Ⅲ级' : 'Ⅳ级';
      },
    },
    {
      title: '预案编号',
      dataIndex: 'code',
      key: 'code',
      width: 120,
    },
    {
      title: '预案状态',
      dataIndex: 'status',
      key: 'status',
      width: 60,
      render: (value, row, index) => {
        if (row.status == 1) {
          return <Tag color="green">可用</Tag>;
        }
        if (row.status == 2) {
          return <Tag color="red">不可用</Tag>;
        }
      },
    },
    { title: '编写人', dataIndex: 'writer', key: 'writer', width: 100 },
    { title: '编写单位', dataIndex: 'fireBrigadeName', key: 'fireBrigadeName', width: 100 },
    {
      title: '编写日期',
      dataIndex: 'writingTime',
      key: 'writingTime',
      width: 100,
      render: (value, row, index) => {
        return value ? new moment(value).format('YYYY-MM-DD') : '';
      },
    },
];

class ChoosePlanModal extends React.Component{

    state = {
        selectedRowKeys: [], // Check here to configure the default column
    };


    onSelectChange = (selectedRowKeys)=> {
        this.setState({ selectedRowKeys });
      };


    render(){
        const {fireBrigade,dispatch} = this.props
        const {selectedRowKeys } = this.state;
        const { getFieldDecorator,getFieldsValue,resetFields } = this.props.form;
        
        const loopOption = data =>
            data.map(item => {
            return <Option key={item.id_}>{item.name}</Option>;
        });

        
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
            getCheckboxProps:record => ({
                defaultChecked: selectedRowKeys.includes(`${record.id_}`),
              }),
        };
        const modalOpts = {
            title: fireBrigade.modalType == 'createPlans' ? '选择预案' : '修改预案',
            visible: fireBrigade.planItemModalVisible,
            maskClosable: false,
            centered:true,
            width: 1400,
            onCancel: handleCansel,
            footer: [
              <Button key="back" type="ghost" size="large" onClick={handleCansel}>
                取消
              </Button>,
              <Button
                key="submit"
                type="primary"
                size="large"
                onClick={() => handleOk()}
                loading={fireBrigade.buttomLoading}
              >
                确定
              </Button>,
            ],
          };
    
          const pagination = {
            current: fireBrigade.current,
            pageSize: fireBrigade.pageSize,
            total: fireBrigade.total,
            showSizeChanger: true,
            showTotal: total => '共' + fireBrigade.total + '条',
            onShowSizeChange(current, size) {
              dispatch({
                type: 'fireBrigade/qryPlanListByParams',
                payload: { keyUnitId:fireBrigade.keyUnitId, pageNum: current, pageSize: size, ...getFieldsValue()  },
              });
            },
            onChange(current) {
              dispatch({
                type: 'fireBrigade/qryPlanListByParams',
                payload: { keyUnitId:fireBrigade.keyUnitId,pageNum: current, pageSize: fireBrigade.pageSize, ...getFieldsValue() },
              });
            },
          };
    
        function handleOk() {
            dispatch({
                type: 'fireBrigade/updateKeyUnit',
                payload: {
                    keyUnitId:fireBrigade.keyUnitId,
                    planIdList:selectedRowKeys,
                    type:2
                  },
                });
            
        }

        function handleSearch() {
            dispatch({
              type: 'fireBrigade/qryPlanListByParams',
              payload: { keyUnitId:fireBrigade.keyUnitId,pageNum: 1, pageSize: fireBrigade.pageSize, ...getFieldsValue() },
            });
          }
    
        function handleCansel() {
            dispatch({
            type: 'fireBrigade/updateState',
            payload: {
                planItemModalVisible: false,
                },
            });
        }

        function handleFormReset() {
            resetFields();
            dispatch({
              type: 'fireBrigade/qryPlanListByParams',
              payload: { keyUnitId:fireBrigade.keyUnitId,pageNum: 1, pageSize: fireBrigade.pageSize },
            });
          }

        return(
            <Modal {...modalOpts}>
                <div className={styles.tableList}>

                <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={6} sm={24}>
                  <FormItem label="预案名称">
                    {getFieldDecorator('name')(<Input placeholder="请输入预案名称" />)}
                  </FormItem>
                </Col>
                <Col md={6} sm={24}>
                  <FormItem label="预案类型">
                    {getFieldDecorator('dicId')(
                      <Select
                        allowClear
                        style={{ width: '100%' }}
                        placeholder="选择预案类型"
                        optionFilterProp="children"
                      >
                        <Option value="">全部</Option>
                        {loopOption(fireBrigade.typeList)}
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={6} sm={24}>
                  <FormItem label="预案级别">
                    {getFieldDecorator('level')(
                      <Select
                        allowClear
                        style={{ width: '100%' }}
                        placeholder="选择级别"
                        optionFilterProp="children"
                      >
                        <Option value="">全部</Option>
                        <Option value="1">Ⅰ级</Option>
                        <Option value="2">Ⅱ级</Option>
                        <Option value="3">Ⅲ级</Option>
                        <Option value="4">Ⅳ级</Option>
                      </Select>
                    )}
                  </FormItem>
                </Col>

                <Col md={6} sm={5}>
                  <FormItem label="编写人 ">
                    {getFieldDecorator('writer')(<Input placeholder="请输入编写人查询" />)}
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={6} sm={24}>
                  <FormItem label="预案状态">
                    {getFieldDecorator('status')(
                      <Select
                        allowClear
                        style={{ width: '100%' }}
                        placeholder="选择预案状态"
                        optionFilterProp="children"
                      >
                        <Option value="">全部</Option>
                        <Option value="1">可用</Option>
                        <Option value="2">不可用</Option>
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={6} sm={24}>
                  <FormItem label="操作时间">
                    {getFieldDecorator('createTime')(<RangePicker />)}
                  </FormItem>
                </Col>
                <Col md={6} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                      重置
                    </Button>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>

                  <Table
                    columns={columns}
                    dataSource={fireBrigade.allPlansList}
                    rowKey={record => record.id_}
                    // loading={loading}
                    rowSelection={rowSelection} 
                    pagination={pagination}
                    scroll={{ y: 600 }}
                  />
                </div>
            </Modal>
            );
    }

}

function mapStateToProps(state) {
    return { fireBrigade: state.fireBrigade };
}
  
ChoosePlanModal = Form.create()(ChoosePlanModal);
  
export default connect(mapStateToProps)(ChoosePlanModal);
