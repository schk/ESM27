import React, { Fragment } from 'react';
import { message } from 'antd';
import {
  Table,
  Form,
  Upload,
  Button,
  Popconfirm,
  Card,
  Input,
  Divider,
  Row,
  Col,
  Modal,
} from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import LocationModal from './LocationModal';
import ShowFileModal from './ShowFileModal';
import PlanEditModal from './PlanEditModal';
import ShowPlans from './ShowPlans';
import DocumentModal from './DocumentModal';
import PageHeader from '../../../layouts/PageHeaderLayout';
import { baseUrl } from '../../../config/system';
import ChoosePlanModal from './ChoosePlanModal';
const FormItem = Form.Item;

function FireBrigade({ location, fireBrigade, form, dispatch, loading }) {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    setFieldsValue,
    resetFields,
    getFieldProps,
    getFieldValue,
  } = form;
  const AddEditModalProps = {
    item: fireBrigade.item,
  };
  const LocationModalProps = {};

  const ShowPlansModalProps = {};

  const ShowPlansItemMadalProps = {
    item: fireBrigade.planItem,
  };

  const DocumentModalProps = {
    filePath: fireBrigade.filePath,
    visible: fireBrigade.documnetModalVisible,
    title: '查看文档',
    dispatch: dispatch,
  };

  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 80,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    {
      title: '消防队名称',
      dataIndex: 'name',
      key: 'name',
      width: 250,
      render: (text, record, value) => (
        <Fragment>
          <a onClick={() => onFindFireBrigade(record.id_)}>{record.name}</a>
        </Fragment>
      ),
    },
    { title: '消防队位置', dataIndex: 'position', key: 'position', width: 250 },
    {
      title: '经纬度',
      dataIndex: 'lon',
      key: 'lon',
      width: 200,
      render: (text, record) => {
        if(record.lon && record.lat){
          return record.lon + ',' + record.lat;
        }
        return '';
      },
    },
    { title: '消防队规模', dataIndex: 'scale', key: 'scale', width: 200 },
    { title: '消防队责任人', dataIndex: 'charge', key: 'charge', width: 150 },
    { title: '责任人联系方式', dataIndex: 'phone', key: 'phone', width: 150 },
    { title: '消防队性质', dataIndex: 'properties', key: 'properties', width: 150 },
    { title: '救援能力', dataIndex: 'rescueAbility', key: 'rescueAbility', width: 200 },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      fixed: 'right',
      width: 200,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_)}>修改</a>
          <Divider type="vertical" />
          <a onClick={() => onShowPlans(record.id_)}>增加预案</a>
          <Divider type="vertical" />
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];
  const onFindFireBrigade = id => {
    dispatch({
      type: 'fireBrigade/findFireBrigade',
      payload: id,
    });
  };

  function onShowPlans(id) {
    dispatch({
      type: 'fireBrigade/getPlans',
      payload: id,
    });
  }

  function onUpdate(id) {
    dispatch({
      type: 'fireBrigade/info',
      payload: id,
    });
  }

  function onAdd() {
    resetFields();
    dispatch({
      type: 'fireBrigade/updateState',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'fireBrigade/del',
      payload: id,
      search: { pageNum: fireBrigade.current, pageSize: fireBrigade.pageSize, ...getFieldsValue() },
    });
  }

  function handleSearch() {
    dispatch({
      type: 'fireBrigade/qryListByParams',
      payload: { pageNum: 1, pageSize: fireBrigade.pageSize, ...getFieldsValue() },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'fireBrigade/qryListByParams',
      payload: { pageNum: 1, pageSize: fireBrigade.pageSize },
    });
  }

  function downloadTemplate() {
    window.open(baseUrl + '/execl/downLoadTemplate.jhtml?type=fireBrigade');
  }

  function initLoopMsg(values) {
    return values != null && values.length > 0
      ? values.map((k, index) => {
          return <div key={index}>{k}</div>;
        })
      : null;
  }

  const props = {
    name: 'file',
    action: baseUrl + '/fireBrigade/excel/importExpert.jhtml',
    beforeUpload(file) {
      // const isJPG = file.type === 'application/vnd.ms-excel' || file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
      // if (!isJPG) {
      //   message.error('只能上传EXCEL文件！');
      // }
      // return isJPG;
    },
    onChange(info) {
      let fileList = info.fileList;
      if (info.file.status === 'done') {
        if (info.file.response.httpCode == 200) {
          if (info.file.response.errorCode == 1) {
            //弹出确认取消框
          } else if (info.file.response.errorCode == 2) {
            Modal.error({
              title: '导入提示',
              content: initLoopMsg(info.file.response.data),
            });
          } else {
            message.success(`${info.file.name} 上传成功`);
            dispatch({
              type: 'fireBrigade/qryListByParams',
              payload: { pageNum: 1, pageSize: fireBrigade.pageSize },
            });
          }
        } else {
          Modal.error({
            title: '导入提示',
            content: info.file.response.msg,
          });
        }
        fileList = fileList.filter(file => {
          return file.response && file.response.fid;
        });
        //只保留最后一条记录
        fileList = fileList.slice(-1);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`);
      }
      let fildsValue = {};
      fildsValue['file'] = fileList.map(file => {
        return file;
      });
      setFieldsValue(fildsValue);
    },
  };

  const pagination = {
    current: fireBrigade.current,
    pageSize: fireBrigade.pageSize,
    total: fireBrigade.total,
    showSizeChanger: true,
    showTotal: total => '共' + fireBrigade.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'fireBrigade/qryListByParams',
        payload: { pageNum: current, pageSize: size, ...getFieldsValue() },
      });
    },
    onChange(current) {
      dispatch({
        type: 'fireBrigade/qryListByParams',
        payload: { pageNum: current, pageSize: fireBrigade.pageSize, ...getFieldsValue() },
      });
    },
  };
  const LocationModalGen = () => <LocationModal {...LocationModalProps} />;

  const DocumentModallGen = () => <DocumentModal {...DocumentModalProps} />;

  return (
    <PageHeader>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={6} sm={24}>
                  <FormItem label="名称">
                    {getFieldDecorator('name')(<Input placeholder="请输入名称" />)}
                  </FormItem>
                </Col>
                <Col md={8} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                      重置
                    </Button>

                    <Button
                      style={{ marginLeft: 80 }}
                      type="primary"
                      icon="download"
                      onClick={downloadTemplate}
                    >
                      下载模板
                    </Button>
                    <Upload
                      {...getFieldProps(
                        'file',
                        {
                          validate: [
                            {
                              rules: [{ type: 'array', required: true, message: '请添加数据文件' }],
                              trigger: 'onBlur',
                            },
                          ],
                        },
                        { valuePropName: 'fileIds' }
                      )}
                      {...props}
                      fileList={getFieldValue('file')}
                    >
                      <Button style={{ marginLeft: 8 }} type="primary" icon="upload">
                        导入
                      </Button>
                    </Upload>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={onAdd}>
              新建
            </Button>
          </div>
          <Table
            columns={columns}
            dataSource={fireBrigade.list}
            rowKey={record => record.id_}
            loading={loading}
            pagination={pagination}
            scroll={{ x: 1900 }}
          />
        </div>
      </Card>
      <AddEditModal {...AddEditModalProps} />
      <LocationModalGen />
      <ShowFileModal {...AddEditModalProps} />
      <ShowPlans {...ShowPlansModalProps} />
      <ChoosePlanModal {...ShowPlansItemMadalProps} />
      <DocumentModallGen />
      {/* <SetDepartmentModal {...SetDepartmentModalProps} /> */}
    </PageHeader>
  );
}

function mapStateToProps(state) {
  return {
    fireBrigade: state.fireBrigade,
    loading: state.loading.models.fireBrigade,
  };
}

FireBrigade = Form.create()(FireBrigade);

export default connect(mapStateToProps)(FireBrigade);
