import React, { Fragment } from 'react';
import { Form, Modal, Divider, Popconfirm, Button, Row, Col, Table, Tag } from 'antd';
import { connect } from 'dva';
import moment from 'moment';

let ShowPlans = ({ fireBrigade, keyParts, item = {}, form, dispatch }) => {
  const { resetFields } = form;

  const columns = [
    {
      title: '预案名称',
      dataIndex: 'name',
      key: 'name',
      width: 180,
      render: (text, record, value) => (
        <Fragment>
          <a onClick={() => onFindInfo(record.id_, record.type)}>{record.name}</a>
        </Fragment>
      ),
    },
    { title: '预案类型', dataIndex: 'typeName', key: 'typeName', width: 100 },
    {
      title: '预案级别',
      dataIndex: 'level',
      key: 'level',
      width: 80,
      render: (value, row, index) => {
        return value == 1 ? 'Ⅰ级' : value == 2 ? 'Ⅱ级' : value == 3 ? 'Ⅲ级' : 'Ⅳ级';
      },
    },
    {
      title: '预案编号',
      dataIndex: 'code',
      key: 'code',
      width: 120,
    },
    {
      title: '预案状态',
      dataIndex: 'status',
      key: 'status',
      width: 80,
      render: (value, row, index) => {
        if (row.status == 1) {
          return <Tag color="green">可用</Tag>;
        }
        if (row.status == 2) {
          return <Tag color="red">不可用</Tag>;
        }
      },
    },
    { title: '编写人', dataIndex: 'writer', key: 'writer', width: 100 },
    { title: '编写单位', dataIndex: 'fireBrigadeName', key: 'fireBrigadeName', width: 200 },
    {
      title: '编写日期',
      dataIndex: 'writingTime',
      key: 'writingTime',
      width: 150,
      render: (value, row, index) => {
        return value ? new moment(value).format('YYYY-MM-DD') : '';
      },
    },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 130,
      render: (text, record) => (
        <Fragment>
          {/* <a onClick={() => edit(record.id_, record.type)}>修改</a> */}
          <Divider type="vertical" />
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
          {record.swfPath ? (
            <span>
              <Divider type="vertical" />
              <a onClick={() => onShowDocument(record.id, record.swfPath)}>预览</a>
            </span>
          ) : (
            ''
          )}
        </Fragment>
      ),
    },
  ];

  const modalOpts = {
    title: '查看预案列表',
    visible: fireBrigade.plansModalVisible,
    showVisible: false,
    width: 1400,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="primary" size="large" onClick={handleCansel}>
        关闭
      </Button>,
    ],
  };

  if (!fireBrigade.plansModalVisible) {
    resetFields();
  }

  function onShowDocument(id, path) {
    dispatch({
      type: 'fireBrigade/getDocumnet',
      payload: {
        documnetModalVisible: true,
        id: id,
        filePath: path,
        newKey: new Date().getTime() + '',
      },
    });
  }

  function onFindInfo(id, type) {
    dispatch({
      type: 'plansList/findInfo',
      payload: { id: id, type: type },
    });
  }

  function handleCansel() {
    dispatch({
      type: 'fireBrigade/updateState',
      payload: {
        plansModalVisible: false,
        plansList: [],
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'fireBrigade/delatePlan',
      payload: id,
    });
  }

  function edit(id, type) {
    dispatch({
      type: 'fireBrigade/getPlanItem',
      payload: {
        id: id,
        type: type,
      },
    });
    dispatch({
      type: 'fireBrigade/qryTypes',
      payload: 3,
    });
  }

  function onShow() {
    dispatch({
      type: 'fireBrigade/updateState',
      payload: {
        planItemModalVisible: true,
        modalType: 'createPlans',
        planItem: {},
      },
    });
    dispatch({
      type: 'fireBrigade/qryTypes',
      payload: 3,
    });
  }

  function onShowChoosePlan() {
    dispatch({
      type: 'fireBrigade/qryPlanListByParams',
      payload: {
        keyUnitId:fireBrigade.keyUnitId,
        // planItemModalVisible: true,
        // modalType: 'createPlans',
        // planItem: {},
      },
    });
    dispatch({
      type: 'fireBrigade/qryTypes',
      payload: 3,
    });
  }

  const loop = data =>
    data.map(d => {
      let children = null;
      if (d.children != null && d.children.length > 0) {
        children = loop(d.children);
      }
      return { title: d.name, value: d.id_, key: d.id_, children, disabled: item.id_ === d.id_ };
    });

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col style={{ top: '-12px' }} span={24}>
            <Button type="primary" htmlType="submit" onClick={() => onShowChoosePlan()}>
              添加预案
            </Button>
          </Col>
        </Row>
        <Table
          columns={columns}
          dataSource={fireBrigade.planList}
          rowKey={record => record.id}
          pagination={false}
          scroll={{ y: 1200 }}
        />
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return {
    fireBrigade: state.fireBrigade,
    plansList: state.plansList,
    keyParts: state.keyParts,
  };
}

ShowPlans = Form.create()(ShowPlans);

export default connect(mapStateToProps)(ShowPlans);
