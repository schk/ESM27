import React from 'react';
import { Form, Input, Modal, Button, upload, Icon, Radio, Row, Col, TreeSelect, message, } from 'antd';
import { connect } from 'dva';
import { baseFileUrl } from '../../../config/system';
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const { TextArea } = Input;

const formItemLayout = {
  labelCol: {
    span: 7,
  },
  wrapperCol: {
    span: 16,
  },
};

const formItemLayout1 = {
  labelCol: {
    span: 3,
  },
  wrapperCol: {
    span: 20,
  },
};

let AddEditModal = ({ fireBrigade, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
    getFieldProps,
  } = form;

  const currentUser = JSON.parse(localStorage.getItem('user'));
  
  const onGetLocation = () => {
    const data = getFieldsValue();
    dispatch({
      type: 'fireBrigade/updateState',
      payload: {
        locationModalVisible: true,
        newKey1: new Date().getTime() + '',
        locationItem: { lonLat: data.lonLat, dtPath: data.dtPath },
      },
    });
  };
  const modalOpts = {
    title: fireBrigade.modalType == 'create' ? '新建消防队' : '修改消防队信息',
    visible: fireBrigade.modalVisible,
    maskClosable: false,
    width: 800,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={fireBrigade.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!fireBrigade.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'fireBrigade/updateState',
      payload: {
        modalVisible: false,
        locationItem: {},
      },
    });
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      var lonLat = getFieldsValue(["lonLat"]);
        if(lonLat.lonLat==undefined){
        message.error("坐标不能为空");
        return;
      }
      const data = getFieldsValue();
      data.id = fireBrigade.modalType === 'create' ? '' : item.id;
      if (data.pid == undefined) {
        data.pid = -1;
      }
      dispatch({
        type: `fireBrigade/${fireBrigade.modalType}`,
        payload: data,
        search: { pageNum: fireBrigade.current, pageSize: fireBrigade.pageSize },
      });
    });
  }

  const loop = data =>
    data.map(d => {
      let children = null;
      if (d.children != null && d.children.length > 0) {
        children = loop(d.children);
      }
      return { title: d.name, value: d.id_, key: d.id_, children, disabled: item.id_ === d.id_ };
    });

    function chechPhone(rule, value, callback) {
      var reg = /^(13\d|14[57]|15[^4,\D]|17[678]|18\d)\d{8}|170[059]\d{7}$/;
      if (value != null && value != '' && !reg.test(value)) {
        callback(new Error('请输入正确格式的联系方式'));
      } else {
        callback();
      }
    }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={12}>
            <FormItem label="消防队名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '请填写消防队名称' },
                  { max: 30, message: '最长不超过30个字' },
                ],
              })(<Input type="text" placeholder="请填写消防队名称" />)}
            </FormItem>
          </Col>
          <Col span={12} style={{ height: '56px' }}>
            <FormItem label="上级消防队：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('pid', {
                initialValue: item.pid != null && item.pid != -1 ? item.pid + '' : undefined,
                rules:[
                  currentUser.data.admin?{}:{ required:true, message: '请选择上级消防队' },
                ]
              })(
                <TreeSelect
                  allowClear
                  placeholder="请选择上级消防队"
                  showCheckedStrategy={'SHOW_CHILD'}
                  treeData={loop(fireBrigade.fireBrigadeTree)}
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="责任人:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('charge', {
                initialValue: item.charge,
                rules: [
                  { required: true, message: '请填写消防队责任人' },
                  { max: 20, message: '最长不超过20个字' },
                ],
              })(<Input type="text" placeholder="请填写消防队责任人" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="联系方式:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('phone', {
                initialValue: item.phone,
                rules: [
                  { required: true, message: '请输入责任人联系方式' },{ validator: chechPhone },
                  { max: 20, message: '最长不超过20个字' },
                ],
              })(<Input type="text" placeholder="请填写责任人联系方式" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="消防队位置:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('position', {
                initialValue: item.position,
                rules: [
                  { required: true, message: '请填写消防队位置' },
                  { max: 100, message: '最长不超过100个字' },
                ],
              })(<Input type="text" placeholder="请填写消防队位置" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="消防队性质:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('properties', {
                initialValue: item.properties,
                rules: [{ max: 50, message: '最长不超过50个字' }],
              })(<Input type="text" placeholder="请填写消防队性质" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="获取坐标点:" hasFeedback {...formItemLayout}>
              <div
                style={{
                  marginRight: '10px',
                  marginTop: '5px',
                  width: '28px',
                  height: '28px',
                  background: 'url(images/iconAdd.png)',
                  display: 'inlineBlock',
                  float: 'left',
                  cursor: 'pointer',
                }}
                onClick={onGetLocation}
              />
              {getFieldDecorator('lonLat', {
                initialValue: fireBrigade.locationItem.lonLat
                  ? fireBrigade.locationItem.lonLat + ''
                  : undefined,
              })(
                <Input type="text" readOnly style={{ width: '211px' }} placeholder="请选择经纬度" />
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="站级别:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('stationLevel', {
                initialValue: item.stationLevel,
                rules: [
                  { required: true, message: '请填写站级别' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写站级别" />)}
            </FormItem>
          </Col>
          {/* <Col span={12}>
              <FormItem label="地图图标:" {...formItemLayout}>
                <Input type="hidden" {...getFieldProps('dtPath', { initialValue: fireBrigade.locationItem.dtPath })} />
                {fireBrigade.locationItem.dtPath?
                  <div style={{borderRadius: '4px',border: '1px solid #d9d9d9',height: '64px',position: 'relative',clear: 'both',overflow: 'hidden',}}>
                  <div style={{width: '60px',height: '60px',textAlign: 'center',border: '1px solid #d9d9d9', position: 'relative', borderRadius: '5px'}}>
                    <img style={{position: 'absolute',left: '0',right: '0',bottom: '0',top: '0',margin: 'auto', width: '20px', height: '20px',}}
                      src={baseFileUrl + fireBrigade.locationItem.dtPath} alt=""
                    />
                  </div>
                </div> :""}                    
              </FormItem>
            </Col> */}
        </Row>
        <Row style={{ left: '15px' }}>
          <Col span={24}>
            <FormItem label="救援能力:" hasFeedback {...formItemLayout1}>
              {getFieldDecorator('rescueAbility', {
                initialValue: item.rescueAbility,
                rules: [{ max: 100, message: '最长不超过100个字' }],
              })(
                <TextArea
                  autosize={{ minRows: 2, maxRows: 4 }}
                  placeholder="请填写消防队救援能力"
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row style={{ left: '15px' }}>
          <Col span={24}>
            <FormItem label="消防队规模:" hasFeedback {...formItemLayout1}>
              {getFieldDecorator('scale', {
                initialValue: item.scale,
                rules: [
                  { required: true, message: '请填写消防队规模' },
                  { max: 100, message: '最长不超过100个字' },
                ],
              })(<TextArea autosize={{ minRows: 2, maxRows: 4 }} placeholder="请填写消防队规模" />)}
            </FormItem>
          </Col>
        </Row>
        <Row style={{ left: '15px' }}>
          <Col span={24}>
            <FormItem label="辖区概况:" hasFeedback {...formItemLayout1}>
              {getFieldDecorator('generalSituation', {
                initialValue: item.generalSituation,
                rules: [{ max: 100, message: '最长不超过100个字' }],
              })(<TextArea autosize={{ minRows: 2, maxRows: 4 }} placeholder="请填写辖区概况" />)}
            </FormItem>
          </Col>
        </Row>
        <Row style={{ left: '15px' }}>
          <Col span={24}>
            <FormItem label="消防队描述:" hasFeedback {...formItemLayout1}>
              {getFieldDecorator('remark', {
                initialValue: item.remark,
                rules: [{ max: 100, message: '最长不超过100个字' }],
              })(<TextArea autosize={{ minRows: 2, maxRows: 4 }} placeholder="请填写消防队描述" />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { fireBrigade: state.fireBrigade };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
