import React, { Fragment } from 'react';
import { Table, Form, Button, Popconfirm, Card, Input, Divider, Row, Col, Select } from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import PageHeader from '../../../layouts/PageHeaderLayout';
const Option = Select.Option;
const FormItem = Form.Item;

function SuppliesType({ location, suppliesType, form, dispatch, loading }) {
  const { getFieldDecorator, validateFields, getFieldsValue, setFieldsValue, resetFields } = form;
  const AddEditModalProps = {
    item: suppliesType.item,
  };

  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    { title: '名称', dataIndex: 'name', key: 'name', width: 100 },
    { title: '描述', dataIndex: 'remark', key: 'remark', width: 100 },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 100,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  function onUpdate(id) {
    dispatch({
      type: 'suppliesType/info',
      payload: id,
    });
  }

  function onAdd() {
    resetFields();
    dispatch({
      type: 'suppliesType/updateState',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'suppliesType/del',
      payload: id,
      search: {
        ...getFieldsValue(),
      },
    });
  }

  function handleSearch() {
    dispatch({
      type: 'suppliesType/qryListByParams',
      payload: { ...getFieldsValue() },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'suppliesType/qryListByParams',
      payload: {},
    });
  }

  return (
    <PageHeader>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={onAdd}>
              新建
            </Button>
          </div>
          <Table
            columns={columns}
            dataSource={suppliesType.list}
            rowKey={record => record.id}
            loading={loading}
            pagination={false}
          />
        </div>
      </Card>
      <AddEditModal {...AddEditModalProps} />
    </PageHeader>
  );
}

function mapStateToProps(state) {
  return {
    suppliesType: state.suppliesType,
    loading: state.loading.models.suppliesType,
  };
}

SuppliesType = Form.create()(SuppliesType);

export default connect(mapStateToProps)(SuppliesType);
