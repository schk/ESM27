import React, { Fragment } from 'react';
import { Table, Form, Button, Popconfirm, Card, Input, Divider, Row, Col, Select } from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import PageHeader from '../../../layouts/PageHeaderLayout';
const Option = Select.Option;
const FormItem = Form.Item;

function ExtinguisherType({ location, extinguisherType, form, dispatch, loading }) {
  const { getFieldDecorator, validateFields, getFieldsValue, setFieldsValue, resetFields } = form;
  const AddEditModalProps = {
    item: extinguisherType.item,
  };

  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    { title: '灭火剂类型名称', dataIndex: 'name', key: 'name', width: 100 },
    { title: '灭火剂类型描述', dataIndex: 'remark', key: 'remark', width: 200 },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 100,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  function onUpdate(id) {
    dispatch({
      type: 'extinguisherType/info',
      payload: id,
    });
  }

  function onAdd() {
    resetFields();
    dispatch({
      type: 'extinguisherType/updateState',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'extinguisherType/del',
      payload: id,
      search: extinguisherType.selectObj,
    });
  }

  function handleSearch() {
    dispatch({
      type: 'extinguisherType/qryByType',
      payload: extinguisherType.selectObj,
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'extinguisherType/qryByType',
      payload: extinguisherType.selectObj,
    });
  }

  return (
    <PageHeader>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={onAdd}>
              新建
            </Button>
          </div>
          <Table
            columns={columns}
            dataSource={extinguisherType.list}
            rowKey={record => record.id}
            loading={loading}
            pagination={false}
          />
        </div>
      </Card>
      <AddEditModal {...AddEditModalProps} />
    </PageHeader>
  );
}

function mapStateToProps(state) {
  return {
    extinguisherType: state.extinguisherType,
    loading: state.loading.models.extinguisherType,
  };
}

ExtinguisherType = Form.create()(ExtinguisherType);

export default connect(mapStateToProps)(ExtinguisherType);
