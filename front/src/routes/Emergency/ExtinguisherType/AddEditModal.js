import React from 'react';
import { Form, Input, Modal, Button, Radio, Row, Col } from 'antd';
import { connect } from 'dva';
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const { TextArea } = Input;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

let AddEditModal = ({ extinguisherType, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: extinguisherType.modalType == 'create' ? '新建车辆信息类型' : '修改车辆信息类型',
    visible: extinguisherType.modalVisible,
    maskClosable: false,
    width: 600,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={extinguisherType.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!extinguisherType.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'extinguisherType/updateState',
      payload: {
        modalVisible: false,
      },
    });
  }
  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = getFieldsValue();
      data.id = extinguisherType.modalType === 'create' ? '' : item.id;
      data.type = 5;
      dispatch({
        type: `extinguisherType/${extinguisherType.modalType}`,
        payload: data,
        search: extinguisherType.selectObj,
      });
    });
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={24}>
            <FormItem label="名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name ? item.name + '' : undefined,
                rules: [
                  { required: true, message: '名称未填写' },
                  { max: 20, message: '最长不超过20个字' },
                ],
              })(<Input type="text" placeholder="请填写名称" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="描述：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('remark', {
                initialValue: item.remark ? item.remark + '' : undefined,
                rules: [{ max: 100, message: '最长不超过100个字符' }],
              })(<TextArea autosize={{ minRows: 4 }} />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { extinguisherType: state.extinguisherType };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
