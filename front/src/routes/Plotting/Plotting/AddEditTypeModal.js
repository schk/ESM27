import React from 'react';
import { Form, Input, Modal, Button, Radio, Row, Col } from 'antd';
import { connect } from 'dva';
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const { TextArea } = Input;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

let AddEditTypeModal = ({ plotting, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: plotting.typeModalType == 'createType' ? '新建绘标类型' : '修改绘标类型',
    visible: plotting.typeModalVisible,
    maskClosable: false,
    width: 600,
    key: plotting.newKey,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={plotting.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!plotting.typeModalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'plotting/updateState',
      payload: {
        typeModalVisible: false,
        newKey: Math.random(),
      },
    });
  }
  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = getFieldsValue();
      data.id = plotting.typeModalType === 'createType' ? '' : item.id;
      data.type = 4;
      dispatch({
        type: `plotting/${plotting.typeModalType}`,
        payload: data,
      });
      dispatch({
        type: `plotting/updateState`,
        payload: {
          typeModalVisible: false,
          newKey: Math.random(),
        },
      });
    });
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={24}>
            <FormItem label="类型名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '请填写类型名称' },
                  { max: 20, message: '最长不超过20个字' },
                ],
              })(<Input type="text" placeholder="请填写类型名称" />)}
            </FormItem>
          </Col>
        </Row>

        <Row>
          <Col span={24}>
            <FormItem label="编码:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('code', {
                initialValue: item.code,
              })(<Input type="text" placeholder="请填写编码" />)}
            </FormItem>
          </Col>
        </Row>

        <Row>
          <Col span={24}>
            <FormItem label="描述：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('remark', {
                initialValue: item.remark,
                rules: [{ max: 100, message: '最长不超过100个字' }],
              })(<TextArea autosize={{ minRows: 4 }} />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { plotting: state.plotting };
}

AddEditTypeModal = Form.create()(AddEditTypeModal);

export default connect(mapStateToProps)(AddEditTypeModal);
