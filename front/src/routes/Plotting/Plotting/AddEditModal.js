import React from 'react';
import { Form, Input, Modal, Button, Radio, Row, Col, Select, Upload, Icon, message } from 'antd';
import { connect } from 'dva';
import { baseUrl } from '../../../config/system';
const RadioGroup = Radio.Group;
const Option = Select.Option;
const FormItem = Form.Item;
const { TextArea } = Input;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

let AddEditModal = ({ plotting, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: plotting.modalType == 'create' ? '新建标会' : '修改标会',
    visible: plotting.modalVisible,
    maskClosable: false,
    width: 600,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={plotting.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!plotting.modalVisible) {
    resetFields();
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = getFieldsValue();
      data.id = plotting.modalType === 'create' ? '' : item.id;
      dispatch({
        type: `plotting/${plotting.modalType}`,
        payload: data,
        search: { pageNum: plotting.current, pageSize: plotting.pageSize },
      });
    });
  }
  function handleCansel() {
    dispatch({
      type: 'plotting/updateState',
      payload: {
        modalVisible: false,
        fileList: [],
        jhFileList: [],
        dtFileList: [],
      },
    });
  }

  const uploadProps = {
    action: baseUrl + '/common/uploadIcon.jhtml',
    withCredentials: true,
    listType: 'picture',
    onChange: handleChange,
    onRemove: handleRemove,
  };

  function handleRemove() {
    dispatch({
      type: 'plotting/updateState',
      payload: {
        fileList: [],
      },
    });
  }

  function handleChange(info) {
    let fileList = info.fileList;
    if (info.file.status === 'done') {
      console.log(info);
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
        setFieldsValue({ path: info.file.response.data.fid });
        //只保留最后一条记录
        fileList = fileList.slice(-1);
      } else {
        message.error(info.file.response.msg);
        fileList = fileList.filter(file => {
          if (file.response) {
            if (file.response.httpCode === 200) {
              return true;
            } else {
              return false;
            }
          }
          return true;
        });
      }
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }
    // fileList = fileList.map((file) => {
    //    if (file.response&&file.response.data) {
    //      file.url = file.response.data.url;
    //    }
    //    return file;
    //  });
    // fileList = fileList.filter((file) => {
    //  if (file.response) {
    //    if( file.response.httpCode === 200){
    //      setFieldsValue({ path: file.response.data.fid });
    //
    //      return true;
    //    }else{
    //      message.error(file.response.msg);
    //      return false;
    //    }
    //  }
    //  return true;
    // });
    console.log(fileList);
    if (fileList != null && fileList.length > 1) {
      //只保留最后一条记录
      fileList = fileList.slice(-1);
    }
    dispatch({
      type: 'plotting/updateState',
      payload: {
        fileList: fileList,
      },
    });
  }

  const uploadJhProps = {
    action: baseUrl + '/common/uploadIcon.jhtml',
    withCredentials: true,
    listType: 'picture',
    onChange: handleJhChange,
    onRemove: handleJhRemove,
  };

  function handleJhRemove() {
    dispatch({
      type: 'plotting/updateState',
      payload: {
        jhFileList: [],
      },
    });
  }

  function handleJhChange(info) {
    let fileList = info.fileList;
    if (info.file.status === 'done') {
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
        setFieldsValue({ jhPath: info.file.response.data.fid });
      } else {
        message.error(info.file.response.msg);
      }
      fileList = fileList.filter(file => {
        return true;
      });
      //只保留最后一条记录
      fileList = fileList.slice(-1);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }

    dispatch({
      type: 'plotting/updateState',
      payload: {
        jhFileList: fileList,
      },
    });
  }

  const uploadDtProps = {
    action: baseUrl + '/common/uploadIcon.jhtml',
    withCredentials: true,
    listType: 'picture',
    onChange: handleDtChange,
    onRemove: handleDtRemove,
  };

  function handleDtRemove() {
    dispatch({
      type: 'plotting/updateState',
      payload: {
        dtFileList: [],
      },
    });
  }

  function handleDtChange(info) {
    let fileLists = info.fileList;
    if (info.file.status === 'done') {
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
        setFieldsValue({ dtPath: info.file.response.data.fid });
        fileLists = fileLists.filter(file => {
          return true;
        });
      } else {
        message.error(info.file.response.msg);
        dispatch({
          type: 'plotting/updateState',
          payload: {
            dtFileList: [],
          },
        });
      }

      //只保留最后一条记录
      fileLists = fileLists.slice(-1);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }

    dispatch({
      type: 'plotting/updateState',
      payload: {
        dtFileList: fileLists,
      },
    });
  }

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <FormItem style={{ marginBottom: '0px' }}>
          {getFieldDecorator('path', {
            initialValue: item.path,
          })(<Input type="hidden" />)}
        </FormItem>
        <FormItem style={{ marginBottom: '0px' }}>
          {getFieldDecorator('jhPath', {
            initialValue: item.jhPath,
          })(<Input type="hidden" />)}
        </FormItem>
        <FormItem style={{ marginBottom: '0px' }}>
          {getFieldDecorator('dtPath', {
            initialValue: item.dtPath,
          })(<Input type="hidden" />)}
        </FormItem>

        <Row>
          <Col span={24}>
            <FormItem label="标绘名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '名称未填写' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写名称" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="标绘类型:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('dicId', {
                initialValue: item.dicId ? item.dicId + '' : undefined,
                rules: [{ required: true, message: '请选择标绘类型' }],
              })(
                <Select
                  showSearch
                  style={{ width: '100%' }}
                  placeholder="选择标绘类型"
                  optionFilterProp="children"
                >
                  {loopOption(plotting.typeList)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem {...formItemLayout} label="是否面板显示">
              {getFieldDecorator('isDisplay', {
                initialValue: item.isDisplay == null ? 1 : item.isDisplay,
                rules: [{ required: true, message: '请选择' }],
              })(
                <RadioGroup>
                  <Radio value={1}>显示</Radio>
                  <Radio value={2}>不显示</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="标绘描述:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('remark', {
                initialValue: item.remark ? item.remark + '' : undefined,
                rules: [{ required: true, message: '请输入标绘描述' }],
              })(<TextArea autosize={{ minRows: 3 }} />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem {...formItemLayout} label="面板图标" hasFeedback>
              {getFieldDecorator('fileList', {
                initialValue: plotting.fileList.length > 0 ? plotting.fileList : undefined,
                rules: [{ required: true, message: '请上传面板图标' }],
              })(
                <Upload {...uploadProps} fileList={plotting.fileList}>
                  <Button>
                    <Icon type="upload" /> 上传面板图标
                  </Button>
                </Upload>
              )}
              <div
                style={{
                  color: '#f5222d',
                  fontSize: '12px',
                  lineHeight: '14px',
                }}
              >
                图片大小36*36且不超过5kb
              </div>
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem {...formItemLayout} label="上传聚合图片" hasFeedback>
              {getFieldDecorator('fileList1', {
                initialValue: plotting.jhFileList.length > 0 ? plotting.jhFileList : undefined,
              })(
                <Upload {...uploadJhProps} fileList={plotting.jhFileList}>
                  <Button>
                    <Icon type="upload" /> 上传聚合图片
                  </Button>
                </Upload>
              )}
              <div
                style={{
                  color: '#f5222d',
                  fontSize: '12px',
                  lineHeight: '14px',
                }}
              >
                图片大小55*30且不超过5kb
              </div>
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem {...formItemLayout} label="地图图片" hasFeedback>
              {getFieldDecorator('fileList2', {
                initialValue: plotting.dtFileList.length > 0 ? plotting.dtFileList : undefined,
              })(
                <Upload {...uploadDtProps} fileList={plotting.dtFileList}>
                  <Button>
                    <Icon type="upload" /> 上传地图图片
                  </Button>
                </Upload>
              )}
              <div
                style={{
                  color: '#f5222d',
                  fontSize: '12px',
                  lineHeight: '14px',
                }}
              >
                图片大小25*25且不超过5kb
              </div>
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { plotting: state.plotting };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
