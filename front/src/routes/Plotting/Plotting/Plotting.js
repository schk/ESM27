import React, { Fragment } from 'react';
import {
  Table,
  Form,
  Button,
  Popconfirm,
  Card,
  Input,
  Divider,
  Row,
  Col,
  Select,
  Tabs,
  Tag,
} from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import AddEditTypeModal from './AddEditTypeModal';
import PageHeader from '../../../layouts/PageHeaderLayout';
import { baseFileUrl } from '../../../config/system';
const Option = Select.Option;
const FormItem = Form.Item;
const TabPane = Tabs.TabPane;

function Plotting({ location, plotting, form, dispatch, loading }) {
  const { getFieldDecorator, validateFields, getFieldsValue, setFieldsValue, resetFields } = form;
  const AddEditModalProps = {
    item: plotting.item,
  };
  const AddEditTypeModalProps = {
    item: plotting.typeItem,
  };
  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    { title: '标绘名称', dataIndex: 'name', key: 'name', width: 100 },
    { title: '标绘类型', dataIndex: 'dicName', key: 'dicName', width: 100 },
    { title: '标绘描述', dataIndex: 'remark', key: 'remark', width: 100 },
    {
      title: '是否面板显示',
      dataIndex: 'isDisplay',
      key: 'isDisplay',
      width: 100,
      render: (value, row, index) => {
        if (row.isDisplay == 1) {
          return <Tag color="green">显示</Tag>;
        }
        if (row.isDisplay == 2) {
          return <Tag color="blue">不显示</Tag>;
        }
      },
    },
    {
      title: '小图标',
      dataIndex: 'path',
      key: 'path',
      width: 100,
      render: (value, row, index) => {
        if (row.path) {
          return <img src={baseFileUrl + row.path} style={{ width: '46px' }} />;
        }
      },
    },
    {
      title: '聚合图',
      dataIndex: 'jhPath',
      key: 'jhPath',
      width: 100,
      render: (value, row, index) => {
        if (row.jhPath) {
          return <img src={baseFileUrl + row.jhPath} style={{ width: '55px' }} />;
        }
      },
    },
    {
      title: '地图',
      dataIndex: 'dtPath',
      key: 'dtPath',
      width: 100,
      render: (value, row, index) => {
        if (row.dtPath) {
          return <img src={baseFileUrl + row.dtPath} style={{ width: '25px' }} />;
        }
      },
    },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 100,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  const typeColumns = [
    { title: '名称', dataIndex: 'name', key: 'name', width: 100 },
    { title: '编码', dataIndex: 'code', key: 'code', width: 100 },
    { title: '描述', dataIndex: 'remark', key: 'remark', width: 100 },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 100,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdateType(record.id_)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm title="确定要删除吗？" onConfirm={onDeleteType.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  function onUpdate(id) {
    dispatch({
      type: 'plotting/info',
      payload: id,
    });
  }

  function onAdd() {
    dispatch({
      type: 'plotting/updateState',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'plotting/del',
      payload: id,
      search: { pageNum: plotting.current, pageSize: plotting.pageSize, ...getFieldsValue() },
    });
  }

  function handleSearch() {
    dispatch({
      type: 'plotting/qryListByParams',
      payload: { pageNum: 1, pageSize: plotting.pageSize, ...getFieldsValue() },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'plotting/qryListByParams',
      payload: { pageNum: 1, pageSize: plotting.pageSize },
    });
  }

  function onAddType() {
    dispatch({
      type: 'plotting/updateState',
      payload: {
        typeModalVisible: true,
        typeModalType: 'createType',
        typeItem: {},
      },
    });
  }
  function onUpdateType(id) {
    dispatch({
      type: 'plotting/typeInfo',
      payload: id,
    });
  }

  function onDeleteType(id) {
    dispatch({
      type: 'plotting/delType',
      payload: id,
    });
  }

  function callback(key) {
    console.log(key);
  }

  const pagination = {
    current: plotting.current,
    pageSize: plotting.pageSize,
    total: plotting.total,
    showSizeChanger: true,
    showTotal: total => '共' + plotting.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'plotting/qryListByParams',
        payload: { pageNum: current, pageSize: size, ...getFieldsValue() },
      });
    },
    onChange(current) {
      dispatch({
        type: 'plotting/qryListByParams',
        payload: { pageNum: current, pageSize: plotting.pageSize, ...getFieldsValue() },
      });
    },
  };

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id}>{item.name}</Option>;
    });

  return (
    <PageHeader>
      <Tabs onChange={callback} type="card">
        <TabPane tab="标绘管理" key="1">
          <Card bordered={false}>
            <div className={styles.tableList}>
              <div className={styles.tableListForm}>
                <Form layout="inline">
                  <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                    <Col md={5} sm={24}>
                      <FormItem label="名称">
                        {getFieldDecorator('name')(<Input placeholder="请输入名称" />)}
                      </FormItem>
                    </Col>
                    <Col md={5} sm={24}>
                      <FormItem label="标绘类型">
                        {getFieldDecorator('dicId')(
                          <Select
                            showSearch
                            style={{ width: '100%' }}
                            placeholder="选择类型"
                            optionFilterProp="children"
                          >
                            <Option value="">全部</Option>
                            {loopOption(plotting.typeList)}
                          </Select>
                        )}
                      </FormItem>
                    </Col>
                    <Col md={8} sm={24}>
                      <span className={styles.submitButtons}>
                        <Button type="primary" htmlType="submit" onClick={handleSearch}>
                          查询
                        </Button>
                        <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                          重置
                        </Button>
                      </span>
                    </Col>
                  </Row>
                </Form>
              </div>
              <div className={styles.tableListOperator}>
                <Button icon="plus" type="primary" onClick={onAdd}>
                  新建
                </Button>
              </div>
              <Table
                columns={columns}
                dataSource={plotting.list}
                rowKey={record => record.id}
                loading={loading}
                pagination={pagination}
              />
            </div>
          </Card>
          <AddEditModal {...AddEditModalProps} />
        </TabPane>
        <TabPane tab="标绘类型" key="2">
          <Card bordered={false}>
            <div className={styles.tableList}>
              <div className={styles.tableListOperator}>
                <Button icon="plus" type="primary" onClick={onAddType}>
                  新建
                </Button>
              </div>
              <Table
                columns={typeColumns}
                dataSource={plotting.typeList}
                rowKey={record => record.id}
                loading={loading}
                pagination={false}
              />
            </div>
          </Card>
          <AddEditTypeModal {...AddEditTypeModalProps} />
        </TabPane>
      </Tabs>
    </PageHeader>
  );
}

function mapStateToProps(state) {
  return {
    plotting: state.plotting,
    loading: state.loading.models.plotting,
  };
}

Plotting = Form.create()(Plotting);

export default connect(mapStateToProps)(Plotting);
