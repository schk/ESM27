import React from 'react';
import {
  Form,
  Input,
  Modal,
  Button,
  Radio,
  Row,
  Col,
  Select,
  Upload,
  Icon,
  message,
  TreeSelect,
  InputNumber,
  DatePicker
} from 'antd';
import { connect } from 'dva';
import { baseUrl } from '../../../config/system';
import moment from 'moment';
const RadioGroup = Radio.Group;
const Option = Select.Option;
const FormItem = Form.Item;

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};

let AddEditModal = ({ specialDutyEquip, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: specialDutyEquip.modalType == 'create' ? '新建器材' : '修改器材',
    visible: specialDutyEquip.modalVisible,
    maskClosable: false,
    width: 900,
    onCancel: handleCansel,
    key: specialDutyEquip.newKey,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={specialDutyEquip.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!specialDutyEquip.modalVisible) {
    resetFields();
  }
  function handleCansel() {
    dispatch({
      type: 'specialDutyEquip/updateState',
      payload: {
        modalVisible: false,
        pictureFileList: [],
        technicalFileList: [],
        systemFileList: [],
        compute: 1,
      },
    });
  }
  //图片上传
  const pictureUploadProps = {
    action: baseUrl + '/common/uploadImg.jhtml',
    withCredentials: true,
    listType: 'picture',
    onChange: pictureHandleChange,
    onRemove: pictureHandleRemove,
  };
  //技术参数文件上传
  const technicalUploadProps = {
    action: baseUrl + '/common/uploadFile.jhtml',
    withCredentials: true,
    listType: 'text',
    onChange: technicalHandleChange,
    onRemove: technicalHandleRemove,
  };
  //制度规程文件上传
  const systemUploadProps = {
    action: baseUrl + '/common/uploadFile.jhtml',
    withCredentials: true,
    listType: 'text',
    onChange: systemHandleChange,
    onRemove: systemHandleRemove,
  };

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = getFieldsValue();
      data.id = specialDutyEquip.modalType === 'create' ? '' : item.id;
      dispatch({
        type: `specialDutyEquip/${specialDutyEquip.modalType}`,
        payload: data,
        search: {
          pageNum: specialDutyEquip.current,
          pageSize: specialDutyEquip.pageSize,
          equipType: 1,
        },
      });
    });
  }
  //删除图片
  function pictureHandleRemove() {
    dispatch({
      type: 'specialDutyEquip/updateState',
      payload: {
        pictureFileList: [],
      },
    });
  }
  //删除技术参数文件
  function technicalHandleRemove() {
    dispatch({
      type: 'specialDutyEquip/updateState',
      payload: {
        technicalFileList: [],
      },
    });
  }
  //删除制度规程文件
  function systemHandleRemove() {
    dispatch({
      type: 'specialDutyEquip/updateState',
      payload: {
        systemFileList: [],
      },
    });
  }

  //图片上传
  function pictureHandleChange(info) {
    let fileList = info.fileList;
    if (info.file.status === 'done') {
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
        setFieldsValue({ picture: info.file.response.data.fid });
      } else {
        message.error(info.file.response.msg);
      }
      fileList = fileList.filter(file => {
        return true;
      });
      //只保留最后一条记录
      fileList = fileList.slice(-1);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }

    dispatch({
      type: 'specialDutyEquip/updateState',
      payload: {
        pictureFileList: fileList,
      },
    });
  }
  //技术参数文件
  function technicalHandleChange(info) {
    let fileList = info.fileList;
    if (info.file.status === 'done') {
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
        setFieldsValue({ technicalParamFile: info.file.response.data.fid });
      } else {
        message.error(info.file.response.msg);
      }
      fileList = fileList.filter(file => {
        return true;
      });
      //只保留最后一条记录
      fileList = fileList.slice(-1);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }

    dispatch({
      type: 'specialDutyEquip/updateState',
      payload: {
        technicalFileList: fileList,
      },
    });
  }
  //制度规程文件上传
  function systemHandleChange(info) {
    let fileList = info.fileList;
    if (info.file.status === 'done') {
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
        setFieldsValue({ systemRules: info.file.response.data.fid });
      } else {
        message.error(info.file.response.msg);
      }
      fileList = fileList.filter(file => {
        return true;
      });
      //只保留最后一条记录
      fileList = fileList.slice(-1);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }

    dispatch({
      type: 'specialDutyEquip/updateState',
      payload: {
        systemFileList: fileList,
      },
    });
  }
  //-------------------------------------------------
  const loop = data =>
    data.map(d => {
      let children = null;
      if (d.children != null && d.children.length > 0) {
        children = loop(d.children);
      }
      return { title: d.name, value: d.id_, key: d.id_, children, disabled: item.id_ === d.id_ };
    });

  const foamIdOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  function checkFlow(rule, value, callback) {
    if (specialDutyEquip.compute == 1) {
      if (!value && $.trim(value) == '') {
        callback('请输入器材流量');
      } else if (value > 9999999) {
        callback('器材流量不能超过9999999');
      } else {
        callback();
      }
    }
  }

  const changeCompute = e => {
    dispatch({
      type: 'specialDutyEquip/updateState',
      payload: {
        compute: e.target.value,
      },
    });
  };

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <FormItem style={{ marginBottom: '0px' }}>
          {getFieldDecorator('technicalParamFile', {
            initialValue: item.technicalParamFile,
          })(<Input type="hidden" />)}
        </FormItem>
        <FormItem style={{ marginBottom: '0px' }}>
          {getFieldDecorator('systemRules', {
            initialValue: item.systemRules,
          })(<Input type="hidden" />)}
        </FormItem>
        <FormItem style={{ marginBottom: '0px' }}>
          {getFieldDecorator('equipType', {
            initialValue: 1,
          })(<Input type="hidden" />)}
        </FormItem>
        <Row>
          <Col span={12}>
            <FormItem label="器材名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '请填写器材名称' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写器材名称" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="所属消防队:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('fireBrigadeId', {
                initialValue: item.fireBrigadeId
                  ? item.fireBrigadeId + ''
                  : specialDutyEquip.brigadeId ? specialDutyEquip.brigadeId + '' : undefined,
                rules: [{ required: true, message: '请选择所属消防队' }],
              })(
                <TreeSelect
                  showSearch
                  treeNodeFilterProp="title"
                  dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                  placeholder="请选择所属消防队"
                  notFoundContent="无匹配结果"
                  allowClear
                  treeData={loop(specialDutyEquip.fireBrigadeTree)}
                  treeDefaultExpandAll
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="设备编码:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('code', {
                initialValue: item.code,
                rules: [
                  { required: true, message: '请填写设备编码' },
                  { max: 50, message: '设备编码最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写设备编码" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="资产编码:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('selfCode', {
                initialValue: item.selfCode,
                rules: [
                  { required: true, message: '请填写资产编码' },
                  { max: 50, message: '资产编码最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写资产编码" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="规格型号:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('specificationsModel', {
                initialValue: item.specificationsModel,
                rules: [
                  { required: true, message: '请填写规格型号' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写规格型号" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="器材类型:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('type', {
                initialValue: item.type,
                rules: [{ required: true, message: '请选择器材类型' }],
              })(
                <Select placeholder="请选择器材类型">
                  {foamIdOption(specialDutyEquip.typeList)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="库存:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('storageQuantity', {
                initialValue: item.storageQuantity,
                rules: [{ required: true, message: '请填写库存' }],
              })(
                <InputNumber
                  min={1}
                  max={99999999}
                  style={{ width: '100%' }}
                  placeholder="请填写库存"
                />
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="计量单位:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('unit', {
                initialValue: item.unit,
                rules: [
                  { required: true, message: '请填写计量单位' },
                  { max: 10, message: '计量单位最长不超过10个字' },
                ],
              })(<Input type="text" placeholder="请填写计量单位" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
              <Col span={12}>
                  <FormItem label="出厂日期" hasFeedback {...formItemLayout}>
                    {getFieldDecorator('productionDate', {
                      initialValue: item.productionDate ? new moment(item.productionDate) : undefined,
                    })(<DatePicker style={{ width: '100%' }} placeholder="请填写出厂日期" />)}
                  </FormItem>
                </Col>
                <Col span={12}>
                  <FormItem label="使用年限" hasFeedback {...formItemLayout}>
                    {getFieldDecorator('serviceLife', {
                      initialValue: item.serviceLife,
                    })(<Input placeholder="请填写使用年限" />)}
                  </FormItem>
                </Col>
          </Row>
        <Row>
          <Col span={12}>
            <FormItem label="存放位置:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('position', {
                initialValue: item.position,
                rules: [{ max: 100, message: '存放位置最长不超过100个字' }],
              })(<Input type="text" placeholder="请填写存放位置" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="是否可用泡沫:" {...formItemLayout}>
              {getFieldDecorator('foamAvailable', {
                initialValue: item.foamAvailable == null ? 1 : item.foamAvailable,
                rules: [{ required: true }],
              })(
                <RadioGroup style={{ width: '100%' }}>
                  <Radio value={1}>可用</Radio>
                  <Radio value={2}>不可用</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
        </Row>
        
        <Row>
          <Col span={12}>
            <FormItem required label="用于喷淋计算?:" {...formItemLayout}>
              {getFieldDecorator('compute', {
                initialValue: specialDutyEquip.compute,
              })(
                <RadioGroup style={{ width: '100%' }} onChange={changeCompute}>
                  <Radio value={1}>是</Radio>
                  <Radio value={2}>否</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
          {specialDutyEquip.compute == 1 ? (
            <Col span={12}>
              <FormItem required label="器材流量:" {...formItemLayout}>
                {getFieldDecorator('flow', {
                  initialValue: item.flow,
                  rules: [
                    {
                      validator: checkFlow,
                    },
                  ],
                })(<InputNumber style={{ width: '86%' }} placeholder="请输入器材流量" />)}
              </FormItem>
              <span style={{ position: 'absolute', right: '40px', top: '8px' }}>(L/S)</span>
            </Col>
          ) : (
            ''
          )}
        </Row>
        {/* <Row>
          <Col span={12}>
            <FormItem {...formItemLayout} label="技术参数文件" hasFeedback>
              {getFieldDecorator('technicalFileList')(
                <Upload {...technicalUploadProps} fileList={specialDutyEquip.technicalFileList}>
                  <Button>
                    <Icon type="upload" /> 上传
                  </Button>
                </Upload>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem {...formItemLayout} label="制度规程文件" hasFeedback>
              {getFieldDecorator('systemFileList')(
                <Upload {...systemUploadProps} fileList={specialDutyEquip.systemFileList}>
                  <Button>
                    <Icon type="upload" /> 上传
                  </Button>
                </Upload>
              )}
            </FormItem>
          </Col>
        </Row> */}
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { specialDutyEquip: state.specialDutyEquip };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
