import React from 'react';
import { Form, Modal, Button, Radio, Row, Col, Select, Upload, TreeSelect } from 'antd';
import { connect } from 'dva';
import { baseUrl } from '../../../config/system';
import moment from 'moment';
const FormItem = Form.Item;


const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};
let ShowFileModal = ({ specialDutyEquip, item, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: '查看信息',
    visible: specialDutyEquip.modalShowFile,
    maskClosable: false,
    width: 800,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
    ],
  };
  if (!specialDutyEquip.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'specialDutyEquip/updateState',
      payload: {
        modalShowFile: false,
        fileList: [],
      },
    });
  }

  //图片上传
  const pictureUploadProps = {
    action: baseUrl + '/common/uploadImg.jhtml',
    withCredentials: true,
    listType: 'picture',
  };

  //技术参数文件上传
  const technicalUploadProps = {
    action: baseUrl + '/common/uploadFile.jhtml',
    withCredentials: true,
    listType: 'text',
  };

  return (
    <Modal {...modalOpts}>
      <Row>
        <Col span={12}>
          <FormItem label="器材名称:" hasFeedback {...formItemLayout}>
            <label>{item.name}</label>
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="所属消防队:" hasFeedback {...formItemLayout}>
            <label>{item.fireBrigadeName}</label>
          </FormItem>
        </Col>
      </Row>
      <Row>
        <Col span={12}>
          <FormItem label="设备编码:" hasFeedback {...formItemLayout}>
            <label>{item.code}</label>
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="资产编码:" hasFeedback {...formItemLayout}>
            <label>{item.selfCode}</label>
          </FormItem>
        </Col>
      </Row>
      <Row>
        <Col span={12}>
          <FormItem label="规格型号:" hasFeedback {...formItemLayout}>
            <label>{item.specificationsModel}</label>
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="器材类型:" hasFeedback {...formItemLayout}>
            <label>{item.typeName}</label>
          </FormItem>
        </Col>
      </Row>
      <Row>
        <Col span={12}>
          <FormItem label="库存:" hasFeedback {...formItemLayout}>
            <label>{item.storageQuantity}</label>
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="计量单位:" hasFeedback {...formItemLayout}>
            <label>{item.unit}</label>
          </FormItem>
        </Col>
      </Row>
      <Row>
        <Col span={12}>
          <FormItem label="出厂日期:" hasFeedback {...formItemLayout}>
            <label>{item.productionDate ? new moment(item.productionDate).format('YYYY-MM-DD') : undefined}</label>
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="使用年限:" hasFeedback {...formItemLayout}>
            <label>{item.serviceLife}</label>
          </FormItem>
        </Col>
      </Row>
      <Row>
        <Col span={12}>
          <FormItem label="已随车数量:" hasFeedback {...formItemLayout}>
            <label>{item.numberOfCar}</label>
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="存放位置:" hasFeedback {...formItemLayout}>
            <label>{item.position}</label>
          </FormItem>
        </Col>
      </Row>
      <Row>
        <Col span={12}>
          <FormItem label="器材流量:" hasFeedback {...formItemLayout}>
            <label>{item.flow}</label>
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="是否可用泡沫:" hasFeedback {...formItemLayout}>
            <label>
              {item.foamAvailable == 1 ? '可用' : item.foamAvailable == 2 ? '不可用' : '无'}
            </label>
          </FormItem>
        </Col>
      </Row>
      <Row>
        {/* <Col span={12}>
          <FormItem label="图片:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('position', {
            })(
              <Upload {...pictureUploadProps}  fileList={specialDutyEquip.pictureFileList}>

              </Upload>
            )}
          </FormItem>
        </Col> */}
        {/* <Col span={12}>
          <FormItem label="技术参数文件:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('position', {})(
              <Upload {...technicalUploadProps} fileList={specialDutyEquip.technicalFileList} />
            )}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="制度规程文件:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('position', {})(
              <Upload {...technicalUploadProps} fileList={specialDutyEquip.systemFileList} />
            )}
          </FormItem>
        </Col> */}
      </Row>
    </Modal>
  );
};
function mapStateToProps(state) {
  return { specialDutyEquip: state.specialDutyEquip };
}

ShowFileModal = Form.create()(ShowFileModal);

export default connect(mapStateToProps)(ShowFileModal);
