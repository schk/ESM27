import React from 'react';
import {
  Form,
  Input,
  Modal,
  Button,
  Radio,
  Row,
  Col,
  Select,
  Upload,
  Icon,
  message,
  TreeSelect,
  InputNumber,
} from 'antd';
import { connect } from 'dva';
import { baseUrl } from '../../../config/system';
import moment from 'moment';

const RadioGroup = Radio.Group;
const Option = Select.Option;
const FormItem = Form.Item;
const TreeNode = TreeSelect.TreeNode;

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};
let ShowFileModal = ({ conventionalEquip, item, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: '查看信息',
    visible: conventionalEquip.modalShowFile,
    maskClosable: false,
    width: 800,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
    ],
  };
  if (!conventionalEquip.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'conventionalEquip/updateState',
      payload: {
        modalShowFile: false,
        fileList: [],
      },
    });
  }

  //图片上传
  const pictureUploadProps = {
    action: baseUrl + '/common/uploadImg.jhtml',
    withCredentials: true,
    listType: 'picture',
  };

  //技术参数文件上传
  const technicalUploadProps = {
    action: baseUrl + '/common/uploadFile.jhtml',
    withCredentials: true,
    listType: 'text',
  };

  const treeData = i => {
    return i.map(d => {
      d.label = d.name;
      d.key = d.id_ + '';
      d.value = d.id_ + '';
      d.children = d.children && d.children.length > 0 ? treeData(d.children) : [];
      return d;
    });
  };
  return (
    <Modal {...modalOpts}>
      <Row>
        <Col span={12}>
          <FormItem label="器材名称:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('name', {
              initialValue: item.name,
            })(<Input type="text" />)}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="所属消防队:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('fireBrigadeName', {
              initialValue: item.fireBrigadeName,
            })(<Input type="text" />)}
          </FormItem>
        </Col>
      </Row>
      <Row>
        <Col span={12}>
          <FormItem label="设备编码:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('code', {
              initialValue: item.code,
            })(<Input type="text" />)}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="规格型号:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('specificationsModel', {
              initialValue: item.specificationsModel,
            })(<Input type="text" />)}
          </FormItem>
        </Col>
      </Row>
      <Row>
        <Col span={12}>
          <FormItem label="类型:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('type', {
              initialValue: item.typeName,
            })(<Input type="text" />)}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="库存:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('storageQuantity', {
              initialValue: item.storageQuantity,
            })(<Input type="text" />)}
          </FormItem>
        </Col>
      </Row>
      <Row>
          <Col span={12}>
              <FormItem label="出厂日期" hasFeedback {...formItemLayout}>
                {getFieldDecorator('productionDate', {
                  initialValue: item.productionDate ? new moment(item.productionDate).format('YYYY-MM-DD') : undefined,
                })(<Input/>)}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="使用年限" hasFeedback {...formItemLayout}>
                {getFieldDecorator('serviceLife', {
                  initialValue: item.serviceLife,
                })(<Input  />)}
              </FormItem>
            </Col>
      </Row>
      <Row>
        <Col span={12}>
          <FormItem label="计量单位:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('unit', {
              initialValue: item.unit,
            })(<Input type="text" />)}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="已随车数量:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('numberOfCar', {
              initialValue: item.numberOfCar,
            })(
              <div
                style={{
                  display: 'inline-block',
                  padding: '4px 11px',
                  width: '100%',
                  height: '32px',
                  fontSize: '14px',
                  lineHeight: '1.5',
                  color: 'rgba(0, 0, 0, 0.65)',
                  backgroundColor: '#fff',
                  backgroundImage: 'none',
                }}
              />
            )}
          </FormItem>
        </Col>
      </Row>
      <Row>
        <Col span={12}>
          <FormItem label="器材流量:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('flow', {
              initialValue: item.flow,
            })(<InputNumber style={{ width: '86%' }} />)}
          </FormItem>
          <span style={{ position: 'absolute', right: '36px', top: '8px' }}>(L/S)</span>
        </Col>
        <Col span={12}>
          <FormItem label="是否可用泡沫:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('foamAvailable', {
              initialValue:
                item.foamAvailable == 1 ? '可用' : item.foamAvailable == 2 ? '不可用' : '无',
            })(<Input type="text" />)}
          </FormItem>
        </Col>
      </Row>
      <Row>
        {/* <Col span={12}>
          <FormItem label="图片:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('position', {})(
              <Upload {...pictureUploadProps} fileList={conventionalEquip.pictureFileList} />
            )}
          </FormItem>
        </Col> */}
        {/* <Col span={12}>
          <FormItem label="技术参数文件:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('position', {})(
              <Upload {...technicalUploadProps} fileList={conventionalEquip.technicalFileList} />
            )}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="制度规程文件:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('position', {})(
              <Upload {...technicalUploadProps} fileList={conventionalEquip.systemFileList} />
            )}
          </FormItem>
        </Col> */}
      </Row>
    </Modal>
  );
};
function mapStateToProps(state) {
  return { conventionalEquip: state.conventionalEquip };
}

ShowFileModal = Form.create()(ShowFileModal);

export default connect(mapStateToProps)(ShowFileModal);
