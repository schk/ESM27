import React from 'react';
import {
  Form,
  Input,
  Modal,
  Button,
  Row,
  Col,
  Select,
  Upload,
  Icon,
  message,
  TreeSelect,
  InputNumber,
  DatePicker
} from 'antd';
import { connect } from 'dva';
import { baseUrl } from '../../../config/system';
import moment from 'moment';
const Option = Select.Option;
const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 16,
  },
};

let AddEditModal = ({ specialDutyEquipTwo, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: specialDutyEquipTwo.modalType == 'create' ? '新建器材' : '修改器材',
    visible: specialDutyEquipTwo.modalVisible,
    maskClosable: false,
    width: 900,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={specialDutyEquipTwo.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!specialDutyEquipTwo.modalVisible) {
    resetFields();
  }
  function handleCansel() {
    dispatch({
      type: 'specialDutyEquipTwo/updateState',
      payload: {
        modalVisible: false,
        pictureFileList: [],
        technicalFileList: [],
        systemFileList: [],
      },
    });
  }
  //图片上传
  const pictureUploadProps = {
    action: baseUrl + '/common/uploadImg.jhtml',
    withCredentials: true,
    listType: 'picture',
    onChange: pictureHandleChange,
    onRemove: pictureHandleRemove,
  };
  //技术参数文件上传
  const technicalUploadProps = {
    action: baseUrl + '/common/uploadFile.jhtml',
    withCredentials: true,
    listType: 'text',
    onChange: technicalHandleChange,
    onRemove: technicalHandleRemove,
  };
  //制度规程文件上传
  const systemUploadProps = {
    action: baseUrl + '/common/uploadFile.jhtml',
    withCredentials: true,
    listType: 'text',
    onChange: systemHandleChange,
    onRemove: systemHandleRemove,
  };

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = getFieldsValue();
      data.id = specialDutyEquipTwo.modalType === 'create' ? '' : item.id;
      dispatch({
        type: `specialDutyEquipTwo/${specialDutyEquipTwo.modalType}`,
        payload: data,
        search: {
          pageNum: specialDutyEquipTwo.current,
          pageSize: specialDutyEquipTwo.pageSize,
          equipType: 2,
        },
      });
    });
  }
  //删除图片
  function pictureHandleRemove() {
    dispatch({
      type: 'specialDutyEquipTwo/updateState',
      payload: {
        pictureFileList: [],
      },
    });
  }
  //删除技术参数文件
  function technicalHandleRemove() {
    dispatch({
      type: 'specialDutyEquipTwo/updateState',
      payload: {
        technicalFileList: [],
      },
    });
  }
  //删除制度规程文件
  function systemHandleRemove() {
    dispatch({
      type: 'specialDutyEquipTwo/updateState',
      payload: {
        systemFileList: [],
      },
    });
  }

  //图片上传
  function pictureHandleChange(info) {
    let fileList = info.fileList;
    if (info.file.status === 'done') {
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
        setFieldsValue({ picture: info.file.response.data.fid });
      } else {
        message.error(info.file.response.msg);
      }
      fileList = fileList.filter(file => {
        return true;
      });
      //只保留最后一条记录
      fileList = fileList.slice(-1);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }

    dispatch({
      type: 'specialDutyEquipTwo/updateState',
      payload: {
        pictureFileList: fileList,
      },
    });
  }
  //技术参数文件
  function technicalHandleChange(info) {
    let fileList = info.fileList;
    if (info.file.status === 'done') {
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
        setFieldsValue({ technicalParamFile: info.file.response.data.fid });
      } else {
        message.error(info.file.response.msg);
      }
      fileList = fileList.filter(file => {
        return true;
      });
      //只保留最后一条记录
      fileList = fileList.slice(-1);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }

    dispatch({
      type: 'specialDutyEquipTwo/updateState',
      payload: {
        technicalFileList: fileList,
      },
    });
  }
  //制度规程文件上传
  function systemHandleChange(info) {
    let fileList = info.fileList;
    if (info.file.status === 'done') {
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
        setFieldsValue({ systemRules: info.file.response.data.fid });
      } else {
        message.error(info.file.response.msg);
      }
      fileList = fileList.filter(file => {
        return true;
      });
      //只保留最后一条记录
      fileList = fileList.slice(-1);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }

    dispatch({
      type: 'specialDutyEquipTwo/updateState',
      payload: {
        systemFileList: fileList,
      },
    });
  }
  //-------------------------------------------------
  const loop = data =>
    data.map(d => {
      let children = null;
      if (d.children != null && d.children.length > 0) {
        children = loop(d.children);
      }
      return { title: d.name, value: d.id_, key: d.id_, children, disabled: item.id_ === d.id_ };
    });

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });
  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <FormItem style={{ marginBottom: '0px' }}>
          {getFieldDecorator('technicalParamFile', {
            initialValue: item.technicalParamFile,
          })(<Input type="hidden" />)}
        </FormItem>
        <FormItem style={{ marginBottom: '0px' }}>
          {getFieldDecorator('systemRules', {
            initialValue: item.systemRules,
          })(<Input type="hidden" />)}
        </FormItem>
        <FormItem style={{ marginBottom: '0px' }}>
          {getFieldDecorator('equipType', {
            initialValue: 2,
          })(<Input type="hidden" />)}
        </FormItem>
        <FormItem style={{ marginBottom: '0px' }}>
          {getFieldDecorator('compute', {
            initialValue: 2,
          })(<Input type="hidden" />)}
        </FormItem>
        <Row>
          <Col span={12}>
            <FormItem label="器材名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '请填写器材名称' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写器材名称" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="设备编码:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('code', {
                initialValue: item.code,
                rules: [
                  { required: true, message: '请填写设备编码' },
                  { max: 50, message: '设备编码最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写设备编码" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="资产编码:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('selfCode', {
                initialValue: item.selfCode,
                rules: [
                  { required: true, message: '请填写资产编码' },
                  { max: 50, message: '资产编码最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写资产编码" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="所属消防队:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('fireBrigadeId', {
                initialValue: item.fireBrigadeId
                  ? item.fireBrigadeId + ''
                  : specialDutyEquipTwo.brigadeId + '' ? specialDutyEquipTwo.brigadeId : undefined,
                rules: [{ required: true, message: '请选择所属消防队' }],
              })(
                <TreeSelect
                  showSearch
                  treeNodeFilterProp="title"
                  dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                  placeholder="请选择所属消防队"
                  notFoundContent="无匹配结果"
                  allowClear
                  treeData={loop(specialDutyEquipTwo.fireBrigadeTree)}
                  treeDefaultExpandAll
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="规格型号:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('specificationsModel', {
                initialValue: item.specificationsModel,
                rules: [
                  { required: true, message: '请填写规格型号' },
                  { max: 50, message: '规格型号最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写规格型号" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="器材类型:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('type', {
                initialValue: item.type,
                rules: [{ required: true, message: '请选择器材类型' }],
              })(
                <Select placeholder="请选择器材类型">
                  {loopOption(specialDutyEquipTwo.equipTypeList)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="库存:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('storageQuantity', {
                initialValue: item.storageQuantity,
                rules: [{ required: true, message: '请填写库存' }],
              })(
                <InputNumber
                  min={1}
                  max={99999999}
                  style={{ width: '100%' }}
                  placeholder="请填写库存"
                />
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="计量单位:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('unit', {
                initialValue: item.unit,
                rules: [
                  { required: true, message: '请填写计量单位' },
                  { max: 10, message: '计量单位最长不超过10个字' },
                ],
              })(<Input type="text" placeholder="请填写计量单位" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
              <Col span={12}>
                  <FormItem label="出厂日期" hasFeedback {...formItemLayout}>
                    {getFieldDecorator('productionDate', {
                      initialValue: item.productionDate ? new moment(item.productionDate) : undefined,
                    })(<DatePicker style={{ width: '100%' }} placeholder="请填写出厂日期" />)}
                  </FormItem>
                </Col>
                <Col span={12}>
                  <FormItem label="使用年限" hasFeedback {...formItemLayout}>
                    {getFieldDecorator('serviceLife', {
                      initialValue: item.serviceLife,
                    })(<Input placeholder="请填写使用年限" />)}
                  </FormItem>
                </Col>
          </Row>
        <Row>
          {/* <Col span={12}>
            <FormItem label="已随车数量:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('numberOfCar', {
                initialValue: item.numberOfCar,
              })(<div style={{display: 'inline-block',
              padding: '4px 11px',
              width: '100%',
              height: '32px',
              fontSize: '14px',
              lineHeight: '1.5',
              color: 'rgba(0, 0, 0, 0.65)',
              backgroundColor: '#fff',
              backgroundImage: 'none',
              border: '1px solid #d9d9d9',
              borderRadius: '4px'}}>请填写随车数量</div>)}
            </FormItem>
          </Col> */}
          <Col span={12}>
            <FormItem label="存放位置:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('position', {
                initialValue: item.position,
                rules: [{ max: 100, message: '存放位置最长不超过100个字' }],
              })(<Input type="text" placeholder="请填写存放位置" />)}
            </FormItem>
          </Col>
        </Row>
        {/* <Row>
          <Col span={12}>
            <FormItem {...formItemLayout} label="技术参数文件" hasFeedback>
              {getFieldDecorator('technicalFileList')(
                <Upload {...technicalUploadProps} fileList={specialDutyEquipTwo.technicalFileList}>
                  <Button>
                    <Icon type="upload" /> 上传
                  </Button>
                </Upload>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem {...formItemLayout} label="制度规程文件" hasFeedback>
              {getFieldDecorator('systemFileList')(
                <Upload {...systemUploadProps} fileList={specialDutyEquipTwo.systemFileList}>
                  <Button>
                    <Icon type="upload" /> 上传
                  </Button>
                </Upload>
              )}
            </FormItem>
          </Col>
        </Row> */}
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { specialDutyEquipTwo: state.specialDutyEquipTwo };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
