import React, { Fragment } from 'react';
import { message } from 'antd';
import {
  Table,
  Form,
  Button,
  Popconfirm,
  Card,
  Input,
  Divider,
  Row,
  Col,
  Select,
  TreeSelect,
  Upload,
  Modal,
} from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import ShowFileModal from './ShowFileModal';
import PageHeader from '../../../layouts/PageHeaderLayout';
import { baseUrl } from '../../../config/system';
const Option = Select.Option;
const FormItem = Form.Item;
const confirm = Modal.confirm;

function specialDutyEquipTwo({ location, specialDutyEquipTwo, form, dispatch, loading }) {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    setFieldsValue,
    resetFields,
    getFieldProps,
    getFieldValue,
  } = form;
  const AddEditModalProps = {
    item: specialDutyEquipTwo.item,
  };

  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    {
      title: '器材名称',
      dataIndex: 'name',
      key: 'name',
      width: 100,
      render: (text, record, value) => (
        <Fragment>
          <a onClick={() => onShow(record.id_)}>{record.name}</a>
        </Fragment>
      ),
    },
    { title: '设备编码', dataIndex: 'code', key: 'code', width: 100 },
    { title: '资产编码', dataIndex: 'selfCode', key: 'selfCode', width: 100 },
    { title: '所属战队', dataIndex: 'fireBrigadeName', key: 'fireBrigadeName', width: 150 },
    { title: '规格型号', dataIndex: 'specificationsModel', key: 'specificationsModel', width: 100 },
    { title: '类型', dataIndex: 'typeName', key: 'typeName', width: 100 },
    {
      title: '库存',
      dataIndex: 'storageQuantity',
      key: 'storageQuantity',
      width: 80,
      render: (value, row, index) => {
        return value + '' + row.unit;
      },
    },
    { title: '已随车数量', dataIndex: 'numberOfCar', key: 'numberOfCar', width: 80 },
    { title: '存放位置', dataIndex: 'position', key: 'position', width: 100 },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 70,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  function onUpdate(id) {
    dispatch({
      type: 'specialDutyEquipTwo/info',
      payload: id,
    });
  }
  function onShow(id) {
    dispatch({
      type: 'specialDutyEquipTwo/infoFile',
      payload: id,
    });
  }

  function onAdd() {
    dispatch({
      type: 'specialDutyEquipTwo/showCreateModal',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
        pictureFileList: [],
        technicalFileList: [],
        systemFileList: [],
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'specialDutyEquipTwo/del',
      payload: id,
      search: {
        pageNum: specialDutyEquipTwo.current,
        pageSize: specialDutyEquipTwo.pageSize,
        equipType: 2,
        ...getFieldsValue(),
      },
    });
  }

  function handleSearch() {
    dispatch({
      type: 'specialDutyEquipTwo/qryListByParams',
      payload: {
        pageNum: 1,
        equipType: 2,
        pageSize: specialDutyEquipTwo.pageSize,
        ...getFieldsValue(),
      },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'specialDutyEquipTwo/qryListByParams',
      payload: { pageNum: 1, equipType: 2, pageSize: specialDutyEquipTwo.pageSize },
    });
  }

  function downloadTemplate() {
    window.open(baseUrl + '/execl/downLoadTemplate.jhtml?type=kqEquip');
  }

  function initLoopMsg(values) {
    return values != null && values.length > 0
      ? values.map((k, index) => {
          return <div key={index}>{k}</div>;
        })
      : null;
  }

  const props = {
    name: 'file',
    action: baseUrl + '/specialDutyEquip/importExpert.jhtml?equipType=2',
    beforeUpload(file) {
      // const isJPG = file.type === 'application/vnd.ms-excel' || file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
      // if (!isJPG) {
      //   message.error('只能上传EXCEL文件！');
      // }
      // return isJPG;
    },
    onChange(info) {
      let fileList = info.fileList;
      if (info.file.status === 'done') {
        if (info.file.response.httpCode == 200) {
          if (info.file.response.errorCode == 1) {
            //弹出确认取消框
            confirm({
              title: '信息提示框',
              content: initLoopMsg(info.file.response.data),
              okText: '继续导入',
              cancelText: '取消导入',
              onOk() {
                dispatch({
                  type: 'specialDutyEquipTwo/importConfirm',
                  payload: info.file.response.times,
                });
              },
              onCancel() {
                dispatch({
                  type: 'specialDutyEquipTwo/delTemp',
                  payload: info.file.response.times,
                });
              },
            });
          } else if (info.file.response.errorCode == 2) {
            Modal.error({
              title: '导入提示',
              content: info.file.response.data,
            });
          } else {
            message.success(`${info.file.name} 上传成功`);
            dispatch({
              type: 'specialDutyEquipTwo/qryListByParams',
              payload: { pageNum: 1, pageSize: specialDutyEquipTwo.pageSize, equipType: 2 },
            });
          }
        } else {
          Modal.error({
            title: '导入提示',
            content: info.file.response.msg,
          });
        }
        fileList = fileList.filter(file => {
          return file.response && file.response.fid;
        });
        //只保留最后一条记录
        fileList = fileList.slice(-1);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`);
      }
      let fildsValue = {};
      fildsValue['file'] = fileList.map(file => {
        return file;
      });
      setFieldsValue(fildsValue);
    },
  };

  const pagination = {
    current: specialDutyEquipTwo.current,
    pageSize: specialDutyEquipTwo.pageSize,
    total: specialDutyEquipTwo.total,
    showSizeChanger: true,
    showTotal: total => '共' + specialDutyEquipTwo.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'specialDutyEquipTwo/qryListByParams',
        payload: { pageNum: current, equipType: 2, pageSize: size, ...getFieldsValue() },
      });
    },
    onChange(current) {
      dispatch({
        type: 'specialDutyEquipTwo/qryListByParams',
        payload: {
          pageNum: current,
          equipType: 2,
          pageSize: specialDutyEquipTwo.pageSize,
          ...getFieldsValue(),
        },
      });
    },
  };

  const loop = data =>
    data.map(d => {
      let children = null;
      if (d.children != null && d.children.length > 0) {
        children = loop(d.children);
      }
      return { title: d.name, value: d.id_, key: d.id_, children };
    });
  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });
  return (
    <PageHeader>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={5} sm={24}>
                  <FormItem label="名称">
                    {getFieldDecorator('name')(<Input placeholder="请输入名称" />)}
                  </FormItem>
                </Col>
                <Col md={5} sm={24}>
                  <FormItem label="所属战队">
                    {getFieldDecorator('fireBrigadeId')(
                      <TreeSelect
                        showSearch
                        treeNodeFilterProp="title"
                        dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                        placeholder="请选择所属战队"
                        notFoundContent="无匹配结果"
                        allowClear
                        treeData={loop(specialDutyEquipTwo.fireBrigadeTree)}
                        treeDefaultExpandAll
                      />
                    )}
                  </FormItem>
                </Col>
                <Col md={5} sm={24}>
                  <FormItem label="类型">
                    {getFieldDecorator('type', {})(
                      <Select placeholder="请选择类型">
                        {loopOption(specialDutyEquipTwo.equipTypeList)}
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={4} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                      重置
                    </Button>

                    <Button
                      style={{ marginLeft: 80 }}
                      type="primary"
                      icon="download"
                      onClick={downloadTemplate}
                    >
                      下载模板
                    </Button>
                    <Upload
                      {...getFieldProps(
                        'file',
                        {
                          validate: [
                            {
                              rules: [{ type: 'array', required: true, message: '请添加数据文件' }],
                              trigger: 'onBlur',
                            },
                          ],
                        },
                        { valuePropName: 'fileIds' }
                      )}
                      {...props}
                      fileList={getFieldValue('file')}
                    >
                      <Button style={{ marginLeft: 8 }} type="primary" icon="upload">
                        导入
                      </Button>
                    </Upload>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={onAdd}>
              新建
            </Button>
          </div>
          <Table
            columns={columns}
            dataSource={specialDutyEquipTwo.list}
            rowKey={record => record.id}
            loading={loading}
            pagination={pagination}
          />
        </div>
      </Card>
      <AddEditModal {...AddEditModalProps} />
      <ShowFileModal {...AddEditModalProps} />
    </PageHeader>
  );
}

function mapStateToProps(state) {
  return {
    specialDutyEquipTwo: state.specialDutyEquipTwo,
    loading: state.loading.models.specialDutyEquipTwo,
  };
}

specialDutyEquipTwo = Form.create()(specialDutyEquipTwo);

export default connect(mapStateToProps)(specialDutyEquipTwo);
