import React from 'react';
import {
  Form,
  Input,
  Modal,
  Button,
  Radio,
  Row,
  Col,
  Select,
  Upload,
  Icon,
  message,
  TreeSelect,
} from 'antd';
import { connect } from 'dva';
import { baseUrl } from '../../../config/system';
import moment from 'moment';
const RadioGroup = Radio.Group;
const Option = Select.Option;
const FormItem = Form.Item;
const TreeNode = TreeSelect.TreeNode;

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};
let ShowFileModal = ({ specialDutyEquipTwo, item, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: '查看信息',
    visible: specialDutyEquipTwo.modalShowFile,
    maskClosable: false,
    width: 800,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
    ],
  };
  if (!specialDutyEquipTwo.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'specialDutyEquipTwo/updateState',
      payload: {
        modalShowFile: false,
        fileList: [],
      },
    });
  }

  const treeData = i => {
    return i.map(d => {
      d.label = d.name;
      d.key = d.id_ + '';
      d.value = d.id_ + '';
      d.children = d.children && d.children.length > 0 ? treeData(d.children) : [];
      return d;
    });
  };
  //技术参数文件上传
  const technicalUploadProps = {
    action: baseUrl + '/common/uploadFile.jhtml',
    withCredentials: true,
    listType: 'text',
  };

  return (
    <Modal {...modalOpts}>
      <Row>
        <Col span={12}>
          <FormItem label="器材名称:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('name', {
              initialValue: item.name,
            })(<Input type="text" placeholder="请填写名称" />)}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="设备编码:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('code', {
              initialValue: item.code,
            })(<Input type="text" placeholder="请填写设备编码" />)}
          </FormItem>
        </Col>
      </Row>
      <Row>
        <Col span={12}>
          <FormItem label="资产编码:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('selfCode', {
              initialValue: item.selfCode,
            })(<Input type="text" placeholder="请填写资产编码" />)}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="所属消防队:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('fireBrigadeName', {
              initialValue: item.fireBrigadeName,
            })(<Input type="text" placeholder="请选择所属消防队" />)}
          </FormItem>
        </Col>
      </Row>
      <Row>
        <Col span={12}>
          <FormItem label="规格型号:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('specificationsModel', {
              initialValue: item.specificationsModel,
            })(<Input type="text" placeholder="请填写规格型号" />)}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="类型:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('type', {
              initialValue: item.typeName,
            })(<Input type="text" placeholder="请填写类型" />)}
          </FormItem>
        </Col>
      </Row>
      <Row>
        <Col span={12}>
          <FormItem label="库存:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('storageQuantity', {
              initialValue: item.storageQuantity,
            })(<Input type="text" placeholder="请填写库存" />)}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="计量单位:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('unit', {
              initialValue: item.unit,
            })(<Input type="text" placeholder="请填写计量单位" />)}
          </FormItem>
        </Col>
      </Row>
      <Row>
          <Col span={12}>
              <FormItem label="出厂日期" hasFeedback {...formItemLayout}>
                {getFieldDecorator('productionDate', {
                  initialValue: item.productionDate ? new moment(item.productionDate).format('YYYY-MM-DD') : undefined,
                })(<Input/>)}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="使用年限" hasFeedback {...formItemLayout}>
                {getFieldDecorator('serviceLife', {
                  initialValue: item.serviceLife,
                })(<Input  />)}
              </FormItem>
            </Col>
      </Row>
      <Row>
        <Col span={12}>
          <FormItem label="已随车数量:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('numberOfCar', {
              initialValue: item.numberOfCar,
            })(
              <div
                style={{
                  display: 'inline-block',
                  padding: '4px 11px',
                  width: '100%',
                  height: '32px',
                  fontSize: '14px',
                  lineHeight: '1.5',
                  color: 'rgba(0, 0, 0, 0.65)',
                  backgroundColor: '#fff',
                  backgroundImage: 'none',
                  borderRadius: '4px',
                }}
              >
                请填写随车数量
              </div>
            )}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="存放位置:" hasFeedback {...formItemLayout}>
            {getFieldDecorator('position', {
              initialValue: item.position,
            })(<Input type="text" placeholder="请填写存放位置" />)}
          </FormItem>
        </Col>
      </Row>
      {/* <Row>
        <Col span={12}>
          <FormItem label="技术参数文件:" {...formItemLayout}>
            {getFieldDecorator('position', {})(
              <Upload {...technicalUploadProps} fileList={specialDutyEquipTwo.technicalFileList} />
            )}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="制度规程文件:" {...formItemLayout}>
            {getFieldDecorator('position', {})(
              <Upload {...technicalUploadProps} fileList={specialDutyEquipTwo.systemFileList} />
            )}
          </FormItem>
        </Col>
      </Row> */}
    </Modal>
  );
};
function mapStateToProps(state) {
  return { specialDutyEquipTwo: state.specialDutyEquipTwo };
}

ShowFileModal = Form.create()(ShowFileModal);

export default connect(mapStateToProps)(ShowFileModal);
