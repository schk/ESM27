import React from 'react';
import {
  Form,
  Input,
  Modal,
  Button,
  Row,
  Col,
  Icon,
  Select,
  Upload,
  message,
  Popconfirm,
  DatePicker,
} from 'antd';
import { connect } from 'dva';
import { baseUrl, baseFileUrl } from '../../../config/system';
import moment from 'moment';
const { TextArea } = Input;
const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 16,
  },
};

let AddEditModal = ({ accidentCase, item = {}, form, dispatch }) => {
  const { getFieldDecorator, validateFields, getFieldsValue, resetFields } = form;

  const modalOpts = {
    title: accidentCase.modalType == 'create' ? '新建事故案例' : '修改事故案例',
    visible: accidentCase.modalVisible,
    width: 700,
    maskClosable: false,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={accidentCase.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  if (!accidentCase.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'accidentCase/updateState',
      payload: {
        modalVisible: false,
        fileList: [],
      },
    });
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = getFieldsValue();
      var fileList = [];
      if (accidentCase.fileList != null && accidentCase.fileList.length > 0) {
        for (var i = 0; i < accidentCase.fileList.length; i++) {
          var fileId = accidentCase.fileList[i].uid;
          var doc = {};
          var names = accidentCase.fileList[i].name.split('.');
          doc.name = data['name_' + fileId] + '.' + names[1];
          if (accidentCase.fileList[i].response != null) {
            doc.url = accidentCase.fileList[i].response.data.url;
            doc.swfUrl = accidentCase.fileList[i].response.data.swfUrl;
          } else {
            doc.url = accidentCase.fileList[i].url;
            doc.swfUrl = accidentCase.fileList[i].swfPath;
          }
          fileList.push(doc);
        }
      }
      data.fileList = fileList;
      //文档数据
      //文档目录数据
      data.id = accidentCase.modalType === 'create' ? '' : item.id;
      dispatch({
        type: `accidentCase/${accidentCase.modalType}`,
        payload: data,
        search: accidentCase.selectObj,
      });
    });
  }

  const uploadProps = {
    action: baseUrl + '/common/uploadFileSwf.jhtml',
    withCredentials: true,
    multiple: true,
    listType: 'text',
    onChange: handleChange,
    onRemove: file => {
      const index = accidentCase.fileList.indexOf(file);
      const newFileList = accidentCase.fileList.slice();
      newFileList.splice(index, 1);
      dispatch({
        type: 'accidentCase/updateState',
        payload: {
          fileList: newFileList,
        },
      });
    },
  };

  function handleChange(info) {
    let fileList = info.fileList;
    if (info.file.status === 'done') {
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
      } else {
        message.error(info.file.response.msg);
      }
      fileList = fileList.filter(file => {
        return true;
      });
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }
    dispatch({
      type: 'accidentCase/updateState',
      payload: {
        fileList: fileList,
      },
    });
  }

  function fileName(fileName, index) {
    let names = fileName.split('.');
    return names[index];
  }
  function onRemoveFile(fileId, oldFileId) {
    var formValue = getFieldsValue();
    //更新fileList
    var fileList = accidentCase.fileList;
    if (fileList != null && fileList.length > 0) {
      for (var i = 0; i < fileList.length; i++) {
        if (fileList[i].uid == fileId) {
          fileList.splice(i, 1);
          break;
        }
      }
      //更新fileCatalogs
      dispatch({
        type: 'accidentCase/updateState',
        payload: {
          fileList: fileList,
        },
      });
    }
  }

  function handleDownload(url, index) {
    let file = accidentCase.fileList[index];
    if (!!!url) {
      if(!file.response)return false;
      url = baseFileUrl + file.response.data.url;
    }else{
      url = baseFileUrl+url;
    };
    window.open(url);
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={24}>
            <FormItem label="事故案例名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '请填写事故案例名称' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写事故案例名称" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="案例类型:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('typeId', {
                initialValue: item.typeId ? item.typeId + '' : undefined,
                rules: [{ required: true, message: '请选择预案类型' }],
              })(
                <Select
                  showSearch
                  style={{ width: '100%' }}
                  placeholder="选择案例类型"
                  optionFilterProp="children"
                >
                  {loopOption(accidentCase.typeList)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="发生时间:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('time', {
                initialValue: item.time ? moment(item.time) : undefined,
                rules: [{ required: true, message: '请选择发生时间' }],
              })(
                <DatePicker
                  style={{ width: '100%' }}
                  placeholder="请选择发生时间"
                  format={'YYYY-MM-DD'}
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="描述：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('desc', {
                initialValue: item.desc,
                rules: [{ max: 100, message: '最长不超过100个字符' }],
              })(<TextArea placeholder="请填写事故案例描述" autosize={{ minRows: 3 }} />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem {...formItemLayout} label="上传文件">
              {getFieldDecorator('fileList', {
                initialValue: accidentCase.fileList.length > 0 ? accidentCase.fileList : undefined,
                rules: [{ required: true, message: '请上传事故案例文件' }],
              })(
                <Upload {...uploadProps}  fileList={accidentCase.fileList}>
                  <Button>
                    <Icon type="upload" /> 上传事故案例文件
                  </Button>
                </Upload>
              )}
            </FormItem>
          </Col>
        </Row>

        {accidentCase.fileList != null && accidentCase.fileList.length > 0 ? (
          <Row>
            {/* <Col span={2}>&nbsp;&nbsp;</Col> */}
            <Col span={13} style={{ textAlign: 'center' }}>
              <span>文件名称</span>
            </Col>
            <Col span={5} style={{ textAlign: 'center' }}>
              <span>文件类型</span>
            </Col>
            <Col span={5} style={{ textAlign: 'center' }}>
              <span>操作</span>
            </Col>
          </Row>
        ) : (
          ''
        )}
        {accidentCase.fileList != null && accidentCase.fileList.length > 0
          ? accidentCase.fileList.map((k, index) => {
              return (
                <Row key={'page' + index}>
                  {/* <Col span={2}>&nbsp;&nbsp;</Col>          */}
                  <Col span={13} style={{ textAlign: 'center' }}>
                    <FormItem>
                      {getFieldDecorator(`name_${k.uid}`, {
                        initialValue: fileName(k.name, 0),
                        rules: [
                          { required: true, whitespace: true, message: '请输入文件名称' },
                          { max: 50, message: '最长不超过50个字符' },
                        ],
                      })(<Input />)}
                    </FormItem>
                  </Col>
                  <Col span={5} style={{ textAlign: 'center' }}>
                    <FormItem>
                      <span>{fileName(k.name, 1)}</span>
                    </FormItem>
                  </Col>
                  <Col span={5} style={{ textAlign: 'center', lineHeight: '42px' }}>
                    <Popconfirm
                      title="确定要删除吗？"
                      onConfirm={() => onRemoveFile(k.uid, accidentCase.curFileId)}
                    >
                      <a>删除</a>
                    </Popconfirm>
                    &nbsp;
                    <a href="javascript:void(0)" onClick={() => handleDownload(k.url, index)}>
                      下载
                    </a>
                  </Col>
                </Row>
              );
            })
          : ''}
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { accidentCase: state.accidentCase };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
