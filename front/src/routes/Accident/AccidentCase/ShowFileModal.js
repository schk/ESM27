import React from 'react';
import {
  Form,
  Input,
  Modal,
  Popover,
  Button,
  Radio,
  Row,
  Col,
  InputNumber,
  Icon,
  Select,
  Upload,
  message,
  Popconfirm,
} from 'antd';
import { connect } from 'dva';
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const Option = Select.Option;
const { TextArea } = Input;
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

let AddEditModal = ({ accidentCase, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: '查看信息',
    visible: accidentCase.findModalVisible,
    maskClosable: false,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
    ],
  };

  if (!accidentCase.findModalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'accidentCase/updateState',
      payload: {
        findModalVisible: false,
        fileList: [],
      },
    });
  }

  function fileName(fileName, index) {
    let names = fileName.split('.');
    return names[index];
  }
  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={24}>
            <FormItem label="事故案例名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '请填写事故案例名称' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写事故案例名称" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="描述：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('desc', {
                initialValue: item.desc,
                rules: [{ max: 100, message: '最长不超过100个字符' }],
              })(<TextArea placeholder="请填写事故案例描述" autosize={{ minRows: 3 }} />)}
            </FormItem>
          </Col>
        </Row>
        {accidentCase.fileList != null && accidentCase.fileList.length > 0 ? (
          <Row>
            <Col span={2}>&nbsp;&nbsp;</Col>
            <Col span={14} style={{ textAlign: 'center' }}>
              <span>文件名称</span>
            </Col>
            <Col span={6} style={{ textAlign: 'center' }}>
              <span>文件类型</span>
            </Col>
          </Row>
        ) : (
          ''
        )}
        {accidentCase.fileList != null && accidentCase.fileList.length > 0
          ? accidentCase.fileList.map((k, index) => {
              return (
                <Row key={'page' + index}>
                  <Col span={2}>&nbsp;&nbsp;</Col>
                  <Col span={14} style={{ textAlign: 'center' }}>
                    <FormItem hasFeedback>
                      {getFieldDecorator(`name_${k.uid}`, {
                        initialValue: fileName(k.name, 0),
                        rules: [{ required: true, whitespace: true, message: '目录名称' }],
                      })(<Input readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={6} style={{ textAlign: 'center' }}>
                    <FormItem>
                      <span>{fileName(k.name, 1)}</span>
                    </FormItem>
                  </Col>
                </Row>
              );
            })
          : ''}
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { accidentCase: state.accidentCase };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
