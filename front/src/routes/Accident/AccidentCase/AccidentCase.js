import React, { Fragment } from 'react';
import {
  Table,
  Form,
  Button,
  Popconfirm,
  Card,
  Input,
  Divider,
  Row,
  Col,
  Select,
  Tabs,
  Tree,
  Modal,
  Icon,
  Upload,
  message,
  DatePicker,
} from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import ShowFileModal from './ShowFileModal';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import moment from 'moment';
const RangePicker = DatePicker.RangePicker;
const FormItem = Form.Item;
function AccidentCase({ location, accidentCase, form, dispatch, loading }) {
  const { getFieldDecorator, getFieldsValue, resetFields } = form;
  const AddEditModalProps = {
    item: accidentCase.item,
  };
  const findModalProps = {
    item: accidentCase.item,
  };

  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    {
      title: '火灾案例名称',
      dataIndex: 'name',
      key: 'name',
      width: 100,
      render: (text, record, value) => (
        <Fragment>
          <a onClick={() => onFindInfo(record.id_)}>{record.name}</a>
        </Fragment>
      ),
    },
    { title: '描述', dataIndex: 'desc', key: 'desc', width: 150 },
    {
      title: '时间',
      dataIndex: 'time',
      key: 'time',
      width: 100,
      render: (value, row, index) => {
        return value ? new moment(value).format('YYYY-MM-DD') : '';
      },
    },
    { title: '类型', dataIndex: 'typeName', key: 'typeName', width: 100 },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 100,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];
  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  const onFindInfo = id => {
    dispatch({
      type: 'accidentCase/findInfo',
      payload: id,
    });
  };

  function onUpdate(id) {
    dispatch({
      type: 'accidentCase/info',
      payload: id,
    });
  }

  function onAdd() {
    dispatch({
      type: 'accidentCase/updateState',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'accidentCase/del',
      payload: id,
      search: {
        pageNum: accidentCase.current,
        pageSize: accidentCase.pageSize,
        ...getFieldsValue(),
      },
    });
  }

  function handleSearch() {
    dispatch({
      type: 'accidentCase/qryListByParams',
      payload: {
        pageNum: 1,
        pageSize: accidentCase.pageSize,
        ...getFieldsValue(),
      },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'accidentCase/qryListByParams',
      payload: { pageNum: 1, pageSize: accidentCase.pageSize },
    });
  }

  function callback(key) {
    console.log(key);
  }

  const pagination = {
    current: accidentCase.current,
    pageSize: accidentCase.pageSize,
    total: accidentCase.total,
    showSizeChanger: true,
    showTotal: total => '共' + accidentCase.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'accidentCase/qryListByParams',
        payload: {
          pageNum: current,
          pageSize: size,
          ...getFieldsValue(),
        },
      });
    },
    onChange(current) {
      dispatch({
        type: 'accidentCase/qryListByParams',
        payload: {
          pageNum: current,
          pageSize: accidentCase.pageSize,
          ...getFieldsValue(),
        },
      });
    },
  };

  function handleTableChange(pagination, filters, sorter) {
    let isChinese = ['account', 'name'].indexOf(sorter.field) >= 0;
    dispatch({
      type: 'accidentCase/qryListByParams',
      payload: {
        pageNum: pagination.current,
        pageSize: pagination.pageSize,
        ...getFieldsValue(),
        orderBy: sorter.field,
        order: sorter.order,
        isChinese,
      },
    });
  }

  const LocationModalGen = () => <LocationModal {...LocationModalProps} />;
  return (
    <PageHeaderLayout>
      <div style={{ backgroundColor: '#fff' }}>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>
              <Form layout="inline">
                <Row gutter={{ md: 8, lg: 24, xl: 24 }}>
                  <Col span={5} sm={5}>
                    <Form.Item label="名称：">
                      {getFieldDecorator('name')(<Input placeholder="输入名称查找" />)}
                    </Form.Item>
                  </Col>
                  <Col md={6} sm={24}>
                    <FormItem label="预案类型">
                      {getFieldDecorator('typeId')(
                        <Select
                          allowClear
                          style={{ width: '100%' }}
                          placeholder="选择案例类型"
                          optionFilterProp="children"
                        >
                          <Option value="">全部</Option>
                          {loopOption(accidentCase.typeList)}
                        </Select>
                      )}
                    </FormItem>
                  </Col>
                  <Col md={6} sm={24}>
                    <FormItem label="发生时间">
                      {getFieldDecorator('time')(
                        <DatePicker
                          style={{ width: '100%' }}
                          placeholder="请选择发生时间"
                          format={'YYYY-MM-DD'}
                        />
                      )}
                    </FormItem>
                  </Col>
                  <Col md={5} sm={4}>
                    <span className={styles.submitButtons}>
                      <Button type="primary" htmlType="submit" onClick={handleSearch}>
                        查询
                      </Button>
                      <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                        重置
                      </Button>
                      <Button style={{ marginLeft: 80 }} icon="plus" type="primary" onClick={onAdd}>
                        新增
                      </Button>
                    </span>
                  </Col>
                </Row>
              </Form>
            </div>
            <div style={{ width: '100%' }}>
              <Table
                columns={columns}
                dataSource={accidentCase.list}
                style={{ marginTop: 10 }}
                pagination={pagination}
                loading={accidentCase.loading}
                rowKey={record => record.id_}
              />
            </div>
          </div>
        </Card>
      </div>
      <AddEditModal {...AddEditModalProps} />
      <ShowFileModal {...findModalProps} />
    </PageHeaderLayout>
  );
}

function mapStateToProps(state) {
  return {
    accidentCase: state.accidentCase,
    loading: state.loading.models.accidentCase,
  };
}

AccidentCase = Form.create()(AccidentCase);

export default connect(mapStateToProps)(AccidentCase);
