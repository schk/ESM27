import React from 'react';
import { Form,
  Input,
  Modal,
  Button,
  Row,
  Col,
  Table,
  Tag,
  Popconfirm,
  Divider,
  Tabs,
  DatePicker,
  Card,
  message, } from 'antd';
import { connect } from 'dva';
import { baseUrl } from '../../../config/system';
import styles from '../../../common/common.less';
const TabPane = Tabs.TabPane;
const RangePicker = DatePicker.RangePicker;
import moment from 'moment';
const FormItem = Form.Item;

function ShowData ({ accident, form, dispatch }) {  
  
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: '查看监测数据',
    visible: accident.dataVisable,
    maskClosable: false,
    width: 1000,
    onCancel: handleCansel,
    footer: null,
  };

  function handleCansel() {
    resetFields();
    dispatch({
      type: 'accident/updateState',
      payload: {
        dataVisable: false,
        currentWeather: 1,
        totalWeather: 0,
        historyWeatherData: [],
        currentGas: 1,
        totalGas: 0,
        historyGasData: [],
        historyGasTitle:[],
        activityKey:'tab1',
      },
    });
  }

  function onChangeTab(targetKey) {
    dispatch({
      type: 'accident/updateState',
      payload: { activityKey: targetKey },
    });
  }

  const columns = [
    { title: '时间', dataIndex: 'timeString', key: 'timeString', width: 200 },
    { title: '风速', dataIndex: 'windSpeed', key: 'windSpeed', width: 100 },
    {
      title: '风向',
      dataIndex: 'windDirection',
      key: 'windDirection',
      width: 80,
    },
    {
      title: 'PM2.5',
      dataIndex: 'pmValue',
      key: 'pmValue',
      width: 80,
    },
    { title: '温度', dataIndex: 'temperature', key: 'temperature', width: 100 },
    { title: '湿度', dataIndex: 'humidity', key: 'humidity', width: 100 },
    { title: '气压', dataIndex: 'pressure', key: 'pressure', width: 100 },
    { title: '噪音', dataIndex: 'noise', key: 'noise', width: 100 },
  ];

  const loop = data =>
    data.map(d => {
      return { title: d.name, dataIndex: d.titleKey, key: d.titleKey, width: 100 };
    });
  const initColumns =  [
    { title: '时间', dataIndex: 'timeString', key: 'timeString', width: 200 },
    { title: '设备编号', dataIndex: 'serial', key: 'serial', width: 100 },
  ];
  const columnsGas = initColumns.concat(loop(accident.historyGasTitle)) ;
  function handleFormReset() {
    resetFields();
  }

  const pagination = {
    current: accident.currentWeather,
    pageSize: accident.pageSizeWeather,
    total: accident.totalWeather,
    showSizeChanger: true,
    showTotal: total => '共' + accident.totalWeather + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'accident/getHistoryWeatherDataByRoom',
        payload: {
          pageNum: current,
          pageSize: size,
          ...getFieldsValue(),
        },
      });
    },
    onChange(current) {
      dispatch({
        type: 'accident/getHistoryWeatherDataByRoom',
        payload: {
          pageNum: current,
          pageSize: accident.pageSizeWeather,
          ...getFieldsValue(),
        },
      });
    },
  };

  const paginationGas = {
    current: accident.currentGas,
    pageSize: accident.pageSizeGas,
    total: accident.totalGas,
    showSizeChanger: true,
    showTotal: total => '共' + accident.totalGas + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'accident/getHistoryGasDataByRoom',
        payload: {
          pageNum: current,
          pageSize: size,
          ...getFieldsValue(),
        },
      });
    },
    onChange(current) {
      dispatch({
        type: 'accident/getHistoryGasDataByRoom',
        payload: {
          pageNum: current,
          pageSize: accident.pageSizeGas,
          ...getFieldsValue(),
        },
      });
    },
  };

  function handleSearch() {
    dispatch({
      type: 'accident/getHistoryWeatherDataByRoom',
      payload: { pageNum: 1, pageSize: accident.pageSizeWeather, ...getFieldsValue() },
    });
    dispatch({
      type: 'accident/getHistoryGasDataByRoom',
      payload: { pageNum: 1, pageSize: accident.pageSizeGas, ...getFieldsValue() },
    });
  }

  return (
    <Modal {...modalOpts} style={{ top: 5 }}>
      <Card bordered={false}>
        <div className={styles.tableList} style={{ marginTop: '-30px' }}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <FormItem style={{ marginBottom: '0px' }}>
                {getFieldDecorator('room', {
                  initialValue: accident.room,
                })(<Input type="hidden" />)}
              </FormItem>
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={8} sm={24}>
                  <FormItem >
                    {getFieldDecorator('createTime')(<RangePicker 
                      showTime={{
                        hideDisabledOptions: true,
                        defaultValue: [moment('00:00:00', 'HH:mm:ss'), moment('11:59:59', 'HH:mm:ss')],
                      }}
                      format="YYYY-MM-DD HH:mm:ss"/>)}
                  </FormItem>
                </Col>
                <Col md={8} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                      重置
                    </Button>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <Tabs type="card" activeKey={accident.activityKey} onChange={onChangeTab}>
            <TabPane tab="气象数据" key="tab1">
              <Table
                columns={columns}
                dataSource={accident.historyWeatherData}
                rowKey={record => record.id}
                pagination={pagination}
              />
            </TabPane>
            <TabPane tab="气体数据" key="tab2">
              <Table
                columns={columnsGas}
                dataSource={accident.historyGasData}
                rowKey={record => record.id}
                pagination={paginationGas}
              />
            </TabPane>
        </Tabs>
        </div>
      </Card>
    </Modal>



  
  );
};

function mapStateToProps(state) {
  return { accident: state.accident };
}

ShowData = Form.create()(ShowData);

export default connect(mapStateToProps)(ShowData);
