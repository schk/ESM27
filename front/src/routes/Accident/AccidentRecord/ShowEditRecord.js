import React, { Fragment } from 'react';
import { Table, Form, Button, Card, Divider, Input, Popconfirm, Row, Col, Modal } from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
const FormItem = Form.Item;

function ShowEditRecord({ location, accident, form, dispatch, loading }) {
  const { getFieldDecorator, getFieldsValue, setFieldsValue, resetFields } = form;
  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    {
      title: '命令类型',
      dataIndex: 'type',
      key: 'type',
      width: 80,
      render: value => {
        return value == 7 ? '命令' : value == 13 ? '任务' : '';
      },
    },
    { title: '发布者', dataIndex: 'publisher', key: 'publisher', width: 80 },
    { title: '内容', dataIndex: 'actionDesc', key: 'actionDesc', width: 150 },
    { title: '反馈者', dataIndex: 'feedback', key: 'feedback', width: 80 },
    { title: '反馈内容', dataIndex: 'feedbackDesc', key: 'feedbackDesc', width: 150 },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      width: 60,
      render: value => {
        return value == 1 ? '未反馈' : value == 2 ? '已反馈' : '';
      },
    },
    {
      title: '操作',
      key: 'operation',
      width: 80,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onEditRecord(record.id)}>编辑</a>
          <Divider type="vertical" />
          <Popconfirm title="确定删除吗？" onConfirm={onDelete.bind(this, record.id)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  const modalOpts = {
    title: '编辑记录',
    visible: accident.editRecordVisable,
    maskClosable: false,
    width: 1400,
    zIndex: 20,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={accident.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  //点击保存以后，回显
  function handleOk() {
    dispatch({
      type: `accident/updateState`,
      payload: {
        editRecordVisable: false,
        editCurrent: 1,
        editTotal: 0,
        editList: [],
      },
    });
  }
  function onDelete(id) {
    dispatch({
      type: 'accident/delRecord',
      payload: id,
      search: {
        pageNum: accident.editCurrent,
        pageSize: accident.editPageSize,
        ...getFieldsValue(),
        id: accident.roomId,
      },
    });
  }

  function onEditRecord(id) {
    dispatch({
      type: 'accident/getEditRecordById',
      payload: id,
    });
  }

  function handleSearch() {
    dispatch({
      type: 'accident/getEditRecord',
      payload: {
        pageNum: 1,
        pageSize: accident.editPageSize,
        ...getFieldsValue(),
        id: accident.roomId,
      },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'accident/getEditRecord',
      payload: { pageNum: 1, pageSize: accident.editPageSize, id: accident.roomId },
    });
  }

  function handleCansel() {
    dispatch({
      type: 'accident/updateState',
      payload: { editRecordVisable: false },
    });
  }

  const pagination = {
    current: accident.editCurrent,
    pageSize: accident.editPageSize,
    total: accident.editTotal,
    showSizeChanger: true,
    showTotal: total => '共' + accident.editTotal + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'accident/getEditRecord',
        payload: { id: accident.roomId, pageNum: current, pageSize: size, ...getFieldsValue() },
      });
    },
    onChange(current) {
      dispatch({
        type: 'accident/getEditRecord',
        payload: {
          id: accident.roomId,
          pageNum: current,
          pageSize: accident.editPageSize,
          ...getFieldsValue(),
        },
      });
    },
  };

  return (
    <Modal {...modalOpts} style={{ top: 5 }}>
      <Card bordered={false}>
        <div className={styles.tableList} style={{ marginTop: '-30px' }}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={8} sm={24}>
                  <FormItem label="关键字">
                    {getFieldDecorator('keyWord')(<Input placeholder="请输入关键字搜索" />)}
                  </FormItem>
                </Col>
                <Col md={8} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                      重置
                    </Button>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <Table
            columns={columns}
            dataSource={accident.editList}
            rowKey={record => record.id}
            loading={loading}
            pagination={pagination}
          />
        </div>
      </Card>
    </Modal>
  );
}

function mapStateToProps(state) {
  return {
    accident: state.accident,
    loading: state.loading.models.accident,
  };
}

ShowEditRecord = Form.create()(ShowEditRecord);

export default connect(mapStateToProps)(ShowEditRecord);
