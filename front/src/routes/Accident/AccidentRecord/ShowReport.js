import React from 'react';
import { Form, Modal } from 'antd';
import { connect } from 'dva';
import { baseUrl } from '../../../config/system';

let ShowReport = ({ accident, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: '查看报告',
    visible: accident.reportVisable,
    maskClosable: false,
    width: 1000,
    onCancel: handleCansel,
    footer: null,
  };

  if (!accident.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'accident/updateState',
      payload: {
        reportVisable: false,
      },
    });
  }

  function handleOk() {
    myFrame.contentWindow.print();
  }

  return (
    <Modal {...modalOpts}>
      <iframe
        name="myFrameName"
        src={baseUrl + '/accident/report.jhtml?accidentId=' + accident.accidentId}
        style={{ width: '950px', height: '800px', border: '0' }}
      />
    </Modal>
  );
};

function mapStateToProps(state) {
  return { accident: state.accident };
}

ShowReport = Form.create()(ShowReport);

export default connect(mapStateToProps)(ShowReport);
