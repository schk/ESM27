import React, { Fragment } from 'react';
import { Table, Form, Button, Card, Input, Divider, Row, Col, Select, Tabs, Tree,Menu,
  Dropdown,
  Icon, } from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import { baseUrl } from '../../../config/system';
import ShowReport from './ShowReport';
import ShowData from './ShowData';
import ShowEditRecord from './ShowEditRecord';
import ShowEditRecordItem from './ShowEditRecordItem';

import moment from 'moment';

function Accident({ location, accident, form, dispatch, loading }) {
  const { getFieldDecorator, getFieldsValue, resetFields } = form;

  const ShowEditRecordModalProps = {
    item: accident.currentRecordItem,
  };

  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    {
      title: '事故开始时间',
      dataIndex: 'createTime',
      key: 'createTime',
      width: 150,
      render: (value, row, index) => {
        return value ? new moment(value).format('YYYY-MM-DD hh:MM:ss') : '';
      },
    },
    { title: '房间号', dataIndex: 'room', key: 'room', width: 100 },
    { title: '事故点名称', dataIndex: 'accidentName', key: 'accidentName', width: 100 },
    { title: '事故等级', dataIndex: 'accidentLevel', key: 'accidentLevel', width: 70 },
    { title: '事故类型', dataIndex: 'accidentType', key: 'accidentType', width: 100 },
    { title: '事故指挥人', dataIndex: 'accidentConductor', key: 'accidentConductor', width: 70 },
    { title: '事故描述', dataIndex: 'accidentRemark', key: 'accidentRemark', width: 150 },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 100,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onInfo(record.id_)}>回放</a>
          <Divider type="vertical" />
          <Dropdown
            overlay={
              <Menu>
                <Menu.Item>
                  <a onClick={() => onShowEditRecord(record.id_)}>编辑记录</a>
                </Menu.Item>
                <Menu.Item>
                  <a onClick={() => onReport(record.id_)}>报告</a>
                </Menu.Item>
                <Menu.Item>
                  <a onClick={() => onData(record.room)}>监测数据</a>
                </Menu.Item>
              </Menu>
            }
          >
            <a className="ant-dropdown-link">
              操作<Icon type="down" />
            </a>
          </Dropdown>
        </Fragment>
      ),
    },
  ];

  function onInfo(id) {
    dispatch({
      type: 'accident/findInfo',
      payload: id,
    });
  }

  /**
   * 查看事故历史记录，只查询命令和任务得数据
   * @param {*} id
   */
  function onShowEditRecord(id) {
    dispatch({
      type: 'accident/getEditRecord',
      payload: {
        id: id,
        pageNum: 1,
        pageSize: accident.editPageSize,
      },
    });
  }

  function onReport(id) {
    dispatch({
      type: 'accident/findReport',
      payload: id,
    });
  }

  function onData(room) {
    dispatch({
      type: 'accident/findData',
      payload: {room:room,},
    });
  }

  function handleSearch() {
    dispatch({
      type: 'accident/qryListByParams',
      payload: {
        pageNum: 1,
        pageSize: accident.pageSize,
        ...getFieldsValue(),
      },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'accident/qryListByParams',
      payload: { pageNum: 1, pageSize: accident.pageSize },
    });
  }

  function onBack() {
    dispatch({
      type: 'accident/updateState',
      payload: { type: 1 },
    });
  }

  const pagination = {
    current: accident.current,
    pageSize: accident.pageSize,
    total: accident.total,
    showSizeChanger: true,
    showTotal: total => '共' + accident.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'accident/qryListByParams',
        payload: {
          pageNum: current,
          pageSize: size,
          ...getFieldsValue(),
        },
      });
    },
    onChange(current) {
      dispatch({
        type: 'accident/qryListByParams',
        payload: {
          pageNum: current,
          pageSize: accident.pageSize,
          ...getFieldsValue(),
        },
      });
    },
  };

  return (
    <PageHeaderLayout>
      {accident.type == 1 ? (
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>
              <Form layout="inline">
                <Row gutter={{ md: 8, lg: 24, xl: 24 }}>
                  <Col span={5} sm={5}>
                    <Form.Item label="房间号：">
                      {getFieldDecorator('room')(<Input placeholder="输入房间号查找" />)}
                    </Form.Item>
                  </Col>
                  <Col span={5} sm={5}>
                    <Form.Item label="事故点名称：">
                      {getFieldDecorator('accidentName')(
                        <Input placeholder="输入事故点名称查找" />
                      )}
                    </Form.Item>
                  </Col>
                  <Col span={7} sm={6}>
                    <Form.Item label="所属类型：">
                      {getFieldDecorator('accidentType')(<Input placeholder="输入类型查找" />)}
                    </Form.Item>
                  </Col>
                  <Col md={5} sm={4}>
                    <span className={styles.submitButtons}>
                      <Button type="primary" htmlType="submit" onClick={handleSearch}>
                        查询
                      </Button>
                      <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                        重置
                      </Button>
                    </span>
                  </Col>
                </Row>
              </Form>
            </div>
            <div style={{ width: '100%', paddingRight: '20px' }}>
              <Table
                columns={columns}
                dataSource={accident.list}
                style={{ marginTop: 10 }}
                pagination={pagination}
                loading={accident.loading}
                rowKey={record => record.id_}
              />
            </div>
          </div>
        </Card>
      ) : (
        <Card bordered={false}>
          <div
            style={{
              width: '30px',
              height: '30px',
              display: 'block',
              background: 'url(images/leftArr.png)no-repeat',
              position: 'absolute',
              left: '35px',
              top: '29px',
              cursor: 'pointer',
            }}
            onClick={() => onBack()}
          />
          <iframe
            src={baseUrl + '/accident/accidentHistory.jhtml?accidentId=' + accident.accidentId}
            style={{ width: '100%', height: '710px', border: '0' }}
          />
        </Card>
      )}
      <ShowReport />
      <ShowData />
      <ShowEditRecord />
      <ShowEditRecordItem {...ShowEditRecordModalProps} />
    </PageHeaderLayout>
  );
}

function mapStateToProps(state) {
  return {
    accident: state.accident,
    loading: state.loading.models.accident,
  };
}

Accident = Form.create()(Accident);

export default connect(mapStateToProps)(Accident);
