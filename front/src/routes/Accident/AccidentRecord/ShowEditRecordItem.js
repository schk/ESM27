import React from 'react';
import { Form, Input, Modal, Button, Row, Col, Icon, Upload, message, Popconfirm } from 'antd';
import { connect } from 'dva';
import { baseUrl, baseFileUrl } from '../../../config/system';
const { TextArea } = Input;
const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

let ShowEditRecordItem = ({ accident, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
    getFieldProps,
  } = form;

  const modalOpts = {
    title: accident.modalType == 'create' ? '新建事故案例' : '修改事故案例',
    visible: accident.currentRecordModalVisible,
    maskClosable: false,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={accident.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!accident.currentRecordModalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'accident/updateState',
      payload: {
        currentRecordModalVisible: false,
        fileList: [],
      },
    });
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = getFieldsValue();
      var fileList = [];
      if (accident.fileList != null && accident.fileList.length > 0) {
        for (var i = 0; i < accident.fileList.length; i++) {
          var fileId = accident.fileList[i].uid;
          var doc = {};
          var names = accident.fileList[i].name.split('.');
          doc.name = data['name_' + fileId] + '.' + names[1];
          if (accident.fileList[i].response != null) {
            doc.url = accident.fileList[i].response.data.url;
            doc.swfPath = accident.fileList[i].response.data.swfUrl;
          } else {
            doc.url = accident.fileList[i].url;
            doc.swfPath = accident.fileList[i].swfPath;
          }
          fileList.push(doc);
        }
      }
      data.fileList = fileList;
      //文档数据
      //文档目录数据
      data.id = accident.modalType === 'create' ? '' : item.id;
      dispatch({
        type: `accident/${accident.modalType}`,
        payload: data,
      });
    });
  }

  const uploadProps = {
    action: baseUrl + '/common/uploadFile.jhtml',
    withCredentials: true,
    multiple: true,
    listType: 'text',
    onChange: handleChange,
    onRemove: file => {
      const index = accident.fileList.indexOf(file);
      const newFileList = accident.fileList.slice();
      newFileList.splice(index, 1);
      dispatch({
        type: 'accident/updateState',
        payload: {
          fileList: newFileList,
        },
      });
    },
  };
  function handleChange(info) {
    let fileList = info.fileList;
    if (info.file.status === 'done') {
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
      } else {
        message.error(info.file.response.msg);
      }
      fileList = fileList.filter(file => {
        return true;
      });
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }
    dispatch({
      type: 'accident/updateState',
      payload: {
        fileList: fileList,
      },
    });
  }

  function fileName(fileName, index) {
    let names = fileName.split('.');
    return names[index];
  }
  function onRemoveFile(fileId, oldFileId) {
    var formValue = getFieldsValue();
    //更新fileList
    var fileList = accident.fileList;
    if (fileList != null && fileList.length > 0) {
      for (var i = 0; i < fileList.length; i++) {
        if (fileList[i].uid == fileId) {
          fileList.splice(i, 1);
          break;
        }
      }
      //更新fileCatalogs
      dispatch({
        type: 'accident/updateState',
        payload: {
          fileList: fileList,
        },
      });
    }
  }

  function handleDownload(url, index) {
    let file = accident.fileList[index];
    if (!!!url) url = baseFileUrl + file.response.data.url;
    window.open(url);
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={24}>
            <FormItem label="发布者:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('publisher', {
                initialValue: item.publisher,
                rules: [
                  { required: true, message: '请填写发布者名称' },
                  { max: 20, message: '最长不超过20个字' },
                ],
              })(<Input type="text" placeholder="请填写发布者名称" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="任务内容:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('actionDesc', {
                initialValue: item.actionDesc,
                rules: [
                  { required: true, message: '请填写任务内容' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<TextArea placeholder="请填写任务内容" autosize={{ minRows: 3 }} />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="反馈者:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('feedback', {
                initialValue: item.feedback,
                rules: [
                  { required: true, message: '请填写反馈者名称' },
                  { max: 20, message: '最长不超过20个字' },
                ],
              })(<Input type="text" placeholder="请填写反馈者名称" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="反馈情况：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('feedbackDesc', {
                initialValue: item.feedbackDesc,
                rules: [{ max: 100, message: '最长不超过100个字符' }],
              })(<TextArea placeholder="请填写事故案例描述" autosize={{ minRows: 3 }} />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem {...formItemLayout} label="上传文件">
              {getFieldDecorator('fileList', {
                initialValue: accident.fileList.length > 0 ? accident.fileList : undefined,
                rules: [{ required: true, message: '请上传文件' }],
              })(
                <Upload {...uploadProps} showUploadList={false} fileList={accident.fileList}>
                  <Button>
                    <Icon type="upload" /> 上传文件
                  </Button>
                </Upload>
              )}
            </FormItem>
          </Col>
        </Row>

        {accident.fileList != null && accident.fileList.length > 0 ? (
          <Row>
            {/* <Col span={2}>&nbsp;&nbsp;</Col> */}
            <Col span={11} style={{ textAlign: 'center' }}>
              <span>文件名称</span>
            </Col>
            <Col span={6} style={{ textAlign: 'center' }}>
              <span>文件类型</span>
            </Col>
            <Col span={5} style={{ textAlign: 'center' }}>
              <span>操作</span>
            </Col>
          </Row>
        ) : (
          ''
        )}
        {accident.fileList != null && accident.fileList.length > 0
          ? accident.fileList.map((k, index) => {
              return (
                <Row key={'page' + index}>
                  {/* <Col span={2}>&nbsp;&nbsp;</Col>          */}
                  <Col span={11} style={{ textAlign: 'center' }}>
                    <FormItem>
                      {getFieldDecorator(`name_${k.uid}`, {
                        initialValue: fileName(k.name, 0),
                        rules: [
                          { required: true, whitespace: true, message: '请输入文件名称' },
                          { max: 50, message: '最长不超过50个字符' },
                        ],
                      })(<Input />)}
                    </FormItem>
                  </Col>
                  <Col span={6} style={{ textAlign: 'center' }}>
                    <FormItem>
                      <span>{fileName(k.name, 1)}</span>
                    </FormItem>
                  </Col>
                  <Col span={5} style={{ textAlign: 'center', lineHeight: '42px' }}>
                    <Popconfirm
                      title="确定要删除吗？"
                      onConfirm={() => onRemoveFile(k.uid, accident.curFileId)}
                    >
                      <a>删除</a>
                    </Popconfirm>
                    &nbsp;
                    <a href="javascript:void(0)" onClick={() => handleDownload(k.url, index)}>
                      下载
                    </a>
                  </Col>
                </Row>
              );
            })
          : ''}
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { accident: state.accident };
}

ShowEditRecordItem = Form.create()(ShowEditRecordItem);

export default connect(mapStateToProps)(ShowEditRecordItem);
