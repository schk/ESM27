import React from 'react';
import { connect } from 'dva';
import { Form, Button, Row, Col } from 'antd';
var this_;

class GeographicAccident extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    setTimeout(function() {
      map = new ol.Map({
        target: 'map',
        layers: [
          new ol.layer.Tile({
            source: new ol.source.XYZ({
              url:
                'https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}',
            }),
          }),
          layer,
        ],
        view: new ol.View({
          // 设置成都为地图中心
          center: ol.proj.transform([104.094752, 30.667451], 'EPSG:4326', 'EPSG:3857'),
          zoom: 8,
          minZoom: 3,
          maxZoom: 18,
        }),
        interactions: ol.interaction.defaults().extend([new app.Drag()]),
      });

      map.on('click', function(e) {
        var lonlat = map.getCoordinateFromPixel(e.pixel);
        var lon = ol.proj.transform(lonlat, 'EPSG:3857', 'EPSG:4326')[0].toFixed(6);
        var lat = ol.proj.transform(lonlat, 'EPSG:3857', 'EPSG:4326')[1].toFixed(6);
        var cLonLat = lon + ',' + lat;
        this_.props.form.setFieldsValue({ lonLat: cLonLat });
      });
    }, 100);
  }

  render() {
    const { children, dispatch } = this.props;
    const {
      getFieldDecorator,
      validateFields,
      getFieldsValue,
      getFieldValue,
      resetFields,
      setFieldsValue,
    } = this.props.form;
    this_ = this;
    const accident = this.props.accident;
    function onBack() {
      dispatch({
        type: 'accident/updateState',
        payload: {
          type: 1,
        },
      });
    }

    return (
      <span>
        <Row>
          <Col span={2}>
            <Button icon="left" onClick={() => onBack()}>
              返回
            </Button>
          </Col>
        </Row>
        <iframe
          src="accidentHistory.html"
          style={{ width: '1500px', height: '800px', border: '0' }}
        />
      </span>
    );
  }
}

function mapStateToProps(state) {
  return {
    sysUser: state.sysUser,
  };
}

GeographicAccident = Form.create()(GeographicAccident);

export default connect(mapStateToProps)(GeographicAccident);
