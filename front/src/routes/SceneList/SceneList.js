import React, { Component, PropTypes, Fragment } from 'react';
import { connect } from 'dva';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { baseUrl } from '../../config/system';

class SceneList extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <PageHeaderLayout>
        <iframe
          src={baseUrl + '/scene/qryList.jhtml'}
          style={{ width: '100%', height: '760px', border: '0' }}
        />
      </PageHeaderLayout>
    );
  }
}
function mapStateToProps(state) {
  return {
    scene: state.scene,
  };
}

export default connect(mapStateToProps)(SceneList);
