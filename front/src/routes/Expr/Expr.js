import React, { Fragment } from 'react';
import { connect } from 'dva';
import { Form, Card, Row, Col, Button, Input, Table, Divider, Popconfirm } from 'antd';
import { baseUrl } from '../../config/system';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from '../../common/common.less';
import ExprModal from '../../components/Expr/ExprModal';
const FormItem = Form.Item;

function Expr({ location, dispatch, expr, form, loading }) {
  const { getFieldDecorator, validateFields, getFieldsValue, resetFields } = form;

  const AddEditModalProps = {
    item: expr.item,
  };

  function onAdd() {
    dispatch({
      type: 'expr/updateState',
      payload: {
        modalVisible: true,
        modalType: 'create',
        dressingMethod: '',
        item: {},
      },
    });
  }

  function onUpdate(id) {
    dispatch({
      type: 'expr/qryInfo',
      payload: id,
    });
  }

  function handleSearch() {
    dispatch({
      type: 'expr/qryListByParams',
      payload: { pageNum: 1, pageSize: expr.pageSize, ...getFieldsValue() },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'expr/qryListByParams',
      payload: { pageNum: 1, pageSize: expr.pageSize },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'expr/del',
      payload: id,
      search: { pageNum: expr.current, pageSize: expr.pageSize, ...getFieldsValue() },
    });
  }

  const columns = [
    { title: '名称', dataIndex: 'name', key: 'name', width: 300 },
    { title: '公式', dataIndex: 'expr', key: 'expr' },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 200,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_)}>编辑</a>
          <Divider type="vertical" />
          <Popconfirm title="是否要删除该公式？" onConfirm={onDelete.bind(this, record.id)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  const pagination = {
    current: expr.current,
    pageSize: expr.pageSize,
    total: expr.total,
    showSizeChanger: true,
    showTotal: total => '共' + expr.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'expr/qryListByParams',
        payload: { pageNum: current, pageSize: size, ...getFieldsValue() },
      });
    },
    onChange(current) {
      dispatch({
        type: 'expr/qryListByParams',
        payload: { pageNum: current, pageSize: expr.pageSize, ...getFieldsValue() },
      });
    },
  };

  const handleDownLoadExl = () => {
    let param = JSON.stringify({ ...getFieldsValue() });
    param = encodeURIComponent(param);
    window.open(baseUrl + '/execl/downLoadExprEXL?param=' + param);
  };

  return (
    <PageHeaderLayout>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={8} sm={24}>
                  <FormItem label="公式名称">
                    {getFieldDecorator('name')(<Input placeholder="请输入公式名称" />)}
                  </FormItem>
                </Col>
                <Col md={8} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                      重置
                    </Button>
                    <Button
                      icon="download"
                      type="primary"
                      style={{ marginLeft: 8 }}
                      onClick={handleDownLoadExl}
                    >
                      导出
                    </Button>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={onAdd}>
              新建
            </Button>
          </div>
        </div>
        <Table
          loading={loading}
          rowKey={record => record.id}
          dataSource={expr.list}
          columns={columns}
          pagination={pagination}
        />
      </Card>
      <ExprModal {...AddEditModalProps} />
    </PageHeaderLayout>
  );
}

function mapStateToProps(state) {
  return {
    expr: state.expr,
    loading: state.loading.models.expr,
  };
}

Expr = Form.create()(Expr);

export default connect(mapStateToProps)(Expr);
