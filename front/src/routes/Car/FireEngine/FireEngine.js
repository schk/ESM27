import React from 'react';
import {
  Form,
  Button,
  Popconfirm,
  Card,
  Input,
  Row,
  Col,
  Select,
  List,
  Icon,
  TreeSelect,
  Upload,
  message,
  Modal,
} from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import SelectEqipmnet from './SelectEqipmnet';
import SelectPlotting from './SelectPlotting';
import ShowEquipment from './ShowEquipment';
import PageHeader from '../../../layouts/PageHeaderLayout';
import { baseUrl, baseFileUrl } from '../../../config/system';
import ShowMessage from '../CarMassage/ShowMessage';
const Option = Select.Option;
const FormItem = Form.Item;
const { Meta } = Card;
import { routerRedux } from 'dva/router';
const confirm = Modal.confirm;

function FireEngine({ fireEngine, carMassage, form, dispatch, loading }) {
  const {
    getFieldDecorator,
    getFieldsValue,
    getFieldValue,
    getFieldProps,
    setFieldsValue,
    resetFields,
  } = form;

  const SelectPlottingModalProps = {
    item: fireEngine.item,
    type: fireEngine.modalType,
    maskClosable: false,
    equipmentList: fireEngine.equipmentList,
    currentSelectList: fireEngine.currentSelectList,
    confirmLoading: loading,
    wrapClassName: 'vertical-center-modal',
    newKey: fireEngine.newKey,
  };

  const SelectEqipmnetModalProps = {
    item: fireEngine.item,
    visible: fireEngine.modalVisible,
    type: fireEngine.modalType,
    maskClosable: false,
    equipmentList: fireEngine.equipmentList,
    dispatch: dispatch,
    currentSelectList: fireEngine.currentSelectList,
    confirmLoading: loading,
    wrapClassName: 'vertical-center-modal',
    newKey: fireEngine.newKey,
  };
  const ShowEquipmentProps = {
    item: fireEngine.editEquipItem,
  };
  const AddEditModalProps = {
    item: fireEngine.item,
  };
  //-------------------------------------
  function onUpdate(id) {
    dispatch({
      type: 'fireEngine/info',
      payload: id,
    });
    dispatch({
      type: 'fireEngine/qryFoamList',
    });
  }
  //-----------------------------------
  const ShowMessageProps = {
    item: carMassage.item,
  };

  function onAdd() {
    dispatch({
      type: 'fireEngine/showCreateModal',
      payload: {
        fireBrigadeTree: fireEngine.fireBrigadeTree,
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'fireEngine/del',
      payload: id,
      search: {
        pageNum: fireEngine.current,
        pageSize: fireEngine.pageSize,
        ...getFieldsValue(),
      },
    });
  }

  function handleSearch() {
    dispatch({
      type: 'fireEngine/qryListByParams',
      payload: {
        pageNum: 1,
        pageSize: fireEngine.pageSize,
        ...getFieldsValue(),
      },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'fireEngine/qryListByParams',
      payload: { pageNum: 1, pageSize: fireEngine.pageSize },
    });
  }
  const dicIdOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });
  const pagination = {
    current: fireEngine.current,
    pageSize: fireEngine.pageSize,
    total: fireEngine.total,
    pageSizeOptions: ['8', '16', '24', '32', '40'],
    showSizeChanger: true,
    showTotal: total => '共' + fireEngine.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'fireEngine/qryListByParams',
        payload: {
          pageNum: current,
          pageSize: size,
          ...getFieldsValue(),
        },
      });
    },
    onChange(current) {
      dispatch({
        type: 'fireEngine/qryListByParams',
        payload: {
          pageNum: current,
          pageSize: fireEngine.pageSize,
          ...getFieldsValue(),
          departmentId: fireEngine.departmentId,
        },
      });
    },
  };
  const treeData = i => {
    return i.map(d => {
      d.title = d.name;
      d.key = d.id_ + '';
      d.value = d.id_ + '';
      d.children = d.children && d.children.length > 0 ? treeData(d.children) : [];
      return d;
    });
  };
  function goToList() {
    dispatch(
      routerRedux.push({
        pathname: '/conventionalEquip/carMassage',
      })
    );
  }
  function showMessage(id) {
    dispatch({
      type: 'carMassage/info',
      payload: id,
    });
    dispatch({
      type: 'carMassage/qryListByEngineId',
      payload: {
        pageNum: 1,
        pageSize: 10,
        fireEngineId: id,
      },
    });
  }

  function downloadTemplate() {
    window.open(baseUrl + '/execl/downLoadTemplate.jhtml?type=fireEngineTemplate');
  }

  function initLoopMsg(values) {
    return values != null && values.length > 0
      ? values.map((k, index) => {
          return <div key={index}>{k}</div>;
        })
      : null;
  }

  const props = {
    name: 'file',
    action: baseUrl + '/fireEngine/excel/importExcel.jhtml',
    beforeUpload(file) {
      // const isJPG = file.type === 'application/vnd.ms-excel' || file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
      // if (!isJPG) {
      //   message.error('只能上传EXCEL文件！');
      // }
      // return isJPG;
    },
    onChange(info) {
      let fileList = info.fileList;
      if (info.file.status === 'done') {
        if (info.file.response.httpCode == 200) {
          if (info.file.response.errorCode == 1) {
            //弹出确认取消框
            confirm({
              title: '信息提示框',
              content: initLoopMsg(info.file.response.data),
              okText: '继续导入',
              cancelText: '取消导入',
              onOk() {
                dispatch({
                  type: 'fireEngine/importExcelConfirm',
                  payload: info.file.response.times,
                });
              },
              onCancel() {
                dispatch({
                  type: 'fireEngine/delExcelTemp',
                  payload: info.file.response.times,
                });
              },
            });
          } else if (info.file.response.errorCode == 2) {
            if (info.file.response.data && info.file.response.data.length > 0) {
              Modal.error({
                title: '导入提示',
                content: initLoopMsg(info.file.response.data),
              });
            }
          } else {
            message.success(`${info.file.name} 上传成功`);
            dispatch({
              type: 'fireEngine/qryListByParams',
              payload: { pageNum: 1, pageSize: fireEngine.pageSize },
            });
          }
        } else {
          Modal.error({
            title: '导入提示',
            content: info.file.response.msg,
          });
        }
        fileList = fileList.filter(file => {
          return file.response && file.response.fid;
        });
        //只保留最后一条记录
        fileList = fileList.slice(-1);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`);
      }
      let fildsValue = {};
      fildsValue['file'] = fileList.map(file => {
        return file;
      });
      setFieldsValue(fildsValue);
    },
  };

  return (
    <PageHeader>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={5} sm={24}>
                  <FormItem label="名称">
                    {getFieldDecorator('name')(<Input placeholder="请输入名称" />)}
                  </FormItem>
                </Col>
                <Col md={6} sm={24}>
                  <FormItem label="所属消防队">
                    {getFieldDecorator('fbIds')(
                      <TreeSelect
                        showSearch
                        treeNodeFilterProp="title"
                        optionFilterProp="children"
                        multiple={true}
                        treeCheckable={true}
                        dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                        placeholder="请选择所属消防队"
                        notFoundContent="无匹配结果"
                        allowClear
                        treeData={treeData(fireEngine.fireBrigadeTree)}
                        treeDefaultExpandAll
                      />
                    )}
                  </FormItem>
                </Col>
                <Col span={5}>
                  <FormItem label="车辆类型:">
                    {getFieldDecorator('dicId')(
                      <Select placeholder="请选择车辆类型" allowClear>
                        {dicIdOption(fireEngine.dicList)}
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={8} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                      重置
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={goToList}>
                      列表数据
                    </Button>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={onAdd}>
              新建
            </Button>

            <Button
              style={{ marginLeft: 80 }}
              type="primary"
              icon="download"
              onClick={downloadTemplate}
            >
              下载模板
            </Button>
            <Upload
              {...getFieldProps(
                'file',
                {
                  validate: [
                    {
                      rules: [{ type: 'array', required: true, message: '请添加数据文件' }],
                      trigger: 'onBlur',
                    },
                  ],
                },
                { valuePropName: 'fileIds' }
              )}
              {...props}
              fileList={getFieldValue('file')}
            >
              <Button style={{ marginLeft: 8 }} type="primary" icon="upload">
                导入
              </Button>
            </Upload>
          </div>
        </div>
        <List
          grid={{ gutter: 16, column: 4 }}
          dataSource={fireEngine.list}
          pagination={pagination}
          renderItem={item => (
            <List.Item
              key={item.id}
              //onClick={onUpdate.bind(item.id)}
            >
              <Card
                style={{ width: 300, cursor: 'pointer' }}
                cover={
                  <img
                    onClick={() => onUpdate(item.id)}
                    style={{ width: '298px', height: '180px' }}
                    alt={item.name}
                    src={item.path ? baseFileUrl + item.path : 'images/timg.jpg'}
                    //src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                  />
                }
                actions={[
                  <Icon type="edit" onClick={() => onUpdate(item.id)} />,
                  <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, item.id)}>
                    <Icon type="delete" />
                  </Popconfirm>,
                ]}
              >
                <Meta title={item.name} />
              </Card>
            </List.Item>
          )}
        />
      </Card>
      <SelectEqipmnet {...SelectEqipmnetModalProps} />
      <SelectPlotting {...SelectPlottingModalProps} />
      <AddEditModal {...AddEditModalProps} />
      <ShowEquipment {...ShowEquipmentProps} />
      <ShowMessage {...ShowMessageProps} />
    </PageHeader>
  );
}

function mapStateToProps(state) {
  return {
    carMassage: state.carMassage,
    fireEngine: state.fireEngine,
    loading: state.loading.models.fireEngine,
  };
}

FireEngine = Form.create()(FireEngine);

export default connect(mapStateToProps)(FireEngine);
