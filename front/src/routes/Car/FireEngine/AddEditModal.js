import React, { Fragment } from 'react';
import {
  Form,
  Input,
  Modal,
  Divider,
  Popconfirm,
  Button,
  Radio,
  Row,
  Col,
  Select,
  Upload,
  Icon,
  message,
  Table,
  TreeSelect,
  DatePicker,
  InputNumber,
  Tabs,
  Tag,
} from 'antd';
import { baseUrl, baseFileUrl } from '../../../config/system';
import { connect } from 'dva';
import moment from 'moment';
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const Option = Select.Option;
const TabPane = Tabs.TabPane;
const formItemLayout = {
  labelCol: { span: 7 },
  wrapperCol: { span: 15 },
};

const formItemLayout1 = {
  labelCol: { span: 2 },
  wrapperCol: { span: 15 },
};

let AddEditModal = ({ fireEngine, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    getFieldProps,
    resetFields,
    setFieldsValue,
  } = form;
  const columns = [
    { title: '名称', dataIndex: 'name', key: 'name', width: 180 },
    { title: '型号', dataIndex: 'code', key: 'code', width: 100 },
    { title: '类型', dataIndex: 'typeName', key: 'typeName', width: 100 },
    { title: '器材数量', dataIndex: 'count', key: 'count', width: 100 },
    { title: '用途说明', dataIndex: 'useDesc', key: 'useDesc', width: 100 },
    {
      title: '随车日期',
      dataIndex: 'followDate',
      key: 'followDate',
      width: 130,
      render: (value, row, index) => {
        return value ? new moment(value).format('YYYY-MM-DD') : '';
      },
    },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 100,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => edit(record)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];
  const modalOpts = {
    title: fireEngine.modalType == 'create' ? '新建车辆信息' : '修改车辆信息',
    visible: fireEngine.showVisible,
    key: fireEngine.equipmentKey,
    showVisible: false,
    width: 1300,
    zIndex: 10,
    top: 5,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={fireEngine.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!fireEngine.showVisible) {
    resetFields();
  }
  //
  const changeRadio = e => {
    dispatch({
      type: 'fireEngine/updateState',
      payload: {
        type: e.target.value,
      },
    });

    if (e.target.value == 2) {
      //展示会标图标
      dispatch({
        type: 'fireEngine/qryPlottingList',
        payload: {
          equipType: '',
          equipmentPageNum: 1,
          equipmentPageSize: 10,
          type: e.target.value,
        },
      });
    }
  };
  const changeRadio1 = function() {
    //展示会标图标
    dispatch({
      type: 'fireEngine/qryPlottingList',
      payload: {
        equipType: '',
        equipmentPageNum: 1,
        equipmentPageSize: 10,
        type: 2,
      },
    });
  };

  const changeVehicularArtillery = e => {
    dispatch({
      type: 'fireEngine/updateState',
      payload: {
        isvehicularArtillery: e.target.value,
      },
    });
  };

  //----------------
  //删除
  //-*--------------------------------------

  function onDelete(key) {
    dispatch({
      type: 'fireEngine/updateState',
      payload: {
        currentSelectList: fireEngine.currentSelectList.filter(item => item.id !== key),
      },
    });
  }
  //------------------------------------------------------------
  //修改
  function edit(record) {
    var editEquipItem = fireEngine.currentSelectList.filter(item => item.id === record.id)[0];
    dispatch({
      type: 'fireEngine/updateState',
      payload: {
        equipmentVisible: true,
        editEquipItem: editEquipItem,
        rowKey: new Date().getTime() + '',
        fileList: '',
        fileIconList: '',
      },
    });
  }
  //-------------
  function handleCansel() {
    dispatch({
      type: 'fireEngine/updateState',
      payload: {
        showVisible: false,
        currentSelectList: [],
        isvehicularArtillery: 1,
        activityKey: 'tab1',
        type: 1,
        path: '',
        currentDate: {},
        foamList: [],
        extinguisherQu: 0,
      },
    });
  }

  function handleOk() {
    validateFields((err, values) => {
      if (!err) {
        const data = getFieldsValue();
        data.id = fireEngine.modalType === 'create' ? '' : item.id;
        dispatch({
          type: `fireEngine/${fireEngine.modalType}`,
          payload: data,
          search: fireEngine.selectObj,
        });
      }
    });
  }
  //展示随车器材模态框
  function onShow() {
    dispatch({
      type: 'fireEngine/qryEquipmentList',
      payload: { equipType: '', equipmentPageNum: 1, equipmentPageSize: 10 },
    });
  }
  //展示会标图标
  function onShowPlotting() {
    dispatch({
      type: 'fireEngine/qryPlottingList',
      payload: { equipType: '', equipmentPageNum: 1, equipmentPageSize: 10 },
    });
  }
  //上传聚合图片  开始
  const uploadPropsJh = {
    action: baseUrl + '/common/uploadJHIcon.jhtml',
    withCredentials: true,
    listType: 'picture',
    onChange: handleChangeJh,
  };
  function handleChangeJh(info) {
    let fileList = info.fileList;
    if (info.file.status === 'done') {
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
        dispatch({
          type: 'fireEngine/updateState',
          payload: {
            jhPath: info.file.response.data.fid,
          },
        });
      } else {
        message.error(info.file.response.msg);
      }
      fileList = fileList.filter(file => {
        return true;
      });
      //只保留最后一条记录
      fileList = fileList.slice(-1);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }
  }
  //上传地图图片  开始
  const uploadPropsDt = {
    action: baseUrl + '/common/uploadDTIcon.jhtml',
    withCredentials: true,
    listType: 'picture',
    onChange: handleChangeDt,
  };
  function handleChangeDt(info) {
    let fileList = info.fileList;
    if (info.file.status === 'done') {
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
        dispatch({
          type: 'fireEngine/updateState',
          payload: {
            dtPath: info.file.response.data.fid,
          },
        });
      } else {
        message.error(info.file.response.msg);
      }
      fileList = fileList.filter(file => {
        return true;
      });
      //只保留最后一条记录
      fileList = fileList.slice(-1);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }
  }
  //上传车辆图片  开始
  const uploadCarProps = {
    action: baseUrl + '/common/uploadCarImg.jhtml',
    withCredentials: true,
    listType: 'picture',
    onChange: handleChange,
  };

  function handleChange(info) {
    let fileList = info.fileList;
    if (info.file.status === 'done') {
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
        dispatch({
          type: 'fireEngine/updateState',
          payload: {
            path: info.file.response.data.fid,
          },
        });
      } else {
        message.error(info.file.response.msg);
      }
      fileList = fileList.filter(file => {
        return true;
      });
      //只保留最后一条记录
      fileList = fileList.slice(-1);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }
  }
  //上传车辆图片  结束

  //上传车辆图标  开始
  const uploadIconProps = {
    action: baseUrl + '/common/uploadIcon.jhtml',
    withCredentials: true,
    listType: 'picture',
    onChange: handleIconChange,
  };

  function handleIconChange(info) {
    let fileList = info.fileList;
    if (info.file.status === 'done') {
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
        dispatch({
          type: 'fireEngine/updateState',
          payload: {
            path: info.file.response.data.fid,
          },
        });
      } else {
        message.error(info.file.response.msg);
      }
      fileList = fileList.filter(file => {
        return true;
      });
      //只保留最后一条记录
      fileList = fileList.slice(-1);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }
  }
  //上传车辆图标  结束

  const dicIdOption = data =>
    data.map(item => {
      return <Option key={item.id}>{item.name}</Option>;
    });
  const foamIdOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });

  const loop = data =>
    data.map(d => {
      let children = null;
      if (d.children != null && d.children.length > 0) {
        children = loop(d.children);
      }
      return { title: d.name, value: d.id_, key: d.id_, children, disabled: item.id_ === d.id_ };
    });

  function onChangeTab(targetKey) {
    dispatch({
      type: 'fireEngine/updateState',
      payload: { activityKey: targetKey },
    });
  }

  const checkSpraySpeed = (rule, value, callback) => {
    const capacity = getFieldValue('capacity');
    if (value && capacity) {
      var aa = (capacity / value).toFixed(2);
      setFieldsValue({ sprayTime: aa });
      callback();
    } else {
      callback();
    }
  };

  const checkCapacity = (rule, value, callback) => {
    if (value > fireEngine.extinguisherQu) {
      callback('干粉剩余量不足');
    } else {
      callback();
    }
    // const spraySpeed = getFieldValue('spraySpeed');
    // if (spraySpeed && value) {
    //   var aa = (value / spraySpeed).toFixed(2);
    //   setFieldsValue({ sprayTime: aa });
    //   callback();
    // } else {
    //   callback();
    // }
  };

  //验证干粉是否足量
  const checkPowderQuantity = (rule, value, callback) => {
    if (value > fireEngine.powderQuantityQu) {
      callback('泡沫剩余量不足');
    } else {
      callback();
    }
  };

  const checkVehicularArtillery = (rule, value, callback) => {
    let type = getFieldValue('isvehicularArtillery');
    if (type == 1 && (value == null || value == '')) {
      callback(new Error('请填写车载炮功率'));
    } else {
      callback();
    }
  };

  const handleFireBrigadeChange = value => {
    form.setFieldsValue({ foamTypeId: undefined });
    form.setFieldsValue({ capacity: undefined });
    dispatch({
      type: 'fireEngine/qryFoamList',
      payload: { fireBrigadeId: value },
    });
  };

  const handldCarTypeChange = value => {
    form.setFieldsValue({ typeId: undefined });
    dispatch({
      type: 'fireEngine/qryCarTypeChild',
      payload: value,
    });
  };

  const handleFoamTypeChange = value => {
    form.setFieldsValue({ capacity: undefined });
    dispatch({
      type: 'fireEngine/qryExtinguisherQu',
      payload: { id: value },
    });
  };

  const handlePaoMoChange = value => {
    form.setFieldsValue({ powderQuantity: undefined });
    dispatch({
      type: 'fireEngine/qryPowderQuantityQu',
      payload: { id: value },
    });
  };

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <FormItem style={{ marginBottom: '0px' }}>
          {getFieldDecorator('fireList', {
            initialValue: fireEngine.currentSelectList,
          })(<Input type="hidden" />)}
        </FormItem>
        <Tabs type="card" activeKey={fireEngine.activityKey} onChange={onChangeTab}>
          <TabPane tab="基本信息" key="tab1">
            <Row>
              <Col span={8}>
                <FormItem label="车辆名称:" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('name', {
                    initialValue: item.name,
                    rules: [
                      { required: true, message: '请填写车辆名称' },
                      { max: 50, message: '最长不超过50个字' },
                    ],
                  })(<Input type="text" placeholder="请填写车辆名称" />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="所属消防队:" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('fireBrigadeId', {
                    initialValue: item.fireBrigadeId
                      ? item.fireBrigadeId + ''
                      : fireEngine.brigadeId ? fireEngine.brigadeId + '' : undefined,
                    rules: [{ required: true, message: '请选择所属消防队' }],
                  })(
                    <TreeSelect
                      showSearch
                      treeNodeFilterProp="title"
                      dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                      placeholder="请选择所属消防队"
                      notFoundContent="无匹配结果"
                      allowClear
                      onChange={handleFireBrigadeChange}
                      treeData={loop(fireEngine.fireBrigadeTree)}
                      treeDefaultExpandAll
                    />
                  )}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="车辆型号：" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('model', {
                    initialValue: item.model,
                    rules: [
                      { required: true, message: '请填写车辆型号' },
                      { max: 50, message: '最长不超过50个字' },
                    ],
                  })(<Input type="text" placeholder="请填写车辆型号" />)}
                </FormItem>
              </Col>
            </Row>

            <Row>
              <Col span={8}>
                <FormItem label="车辆编号：" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('code', {
                    initialValue: item.code,
                    rules: [
                      { required: true, message: '请填写车辆编号' },
                      { max: 50, message: '最长不超过50个字' },
                    ],
                  })(<Input type="text" placeholder="请填写车辆编号" />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="车牌号：" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('plateNumber', {
                    initialValue: item.plateNumber,
                    rules: [
                      { required: true, message: '请填写车辆牌号' },
                      { max: 50, message: '最长不超过50个字' },
                    ],
                  })(<Input type="text" placeholder="请填写车牌号" />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="车辆状态：" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('status', {
                    initialValue: item.status ? item.status + '' : '1',
                    rules: [{ required: true, message: '请选择车辆状态' }],
                  })(
                    <Select placeholder="请选择车辆状态" style={{ width: '100%' }} allowClear>
                      <Option value="1">在用</Option>
                      <Option value="2">空闲</Option>
                    </Select>
                  )}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={8}>
                <FormItem label="消防车类别:" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('dicId', {
                    initialValue: item.dicId == null ? undefined : item.dicId + '',
                    rules: [{ required: true, message: '请选择车辆类型' }],
                  })(
                    <Select
                      placeholder="请选择车辆类型"
                      // onChange={handldCarTypeChange}
                    >
                      {dicIdOption(fireEngine.carTypeParentList)}
                    </Select>
                  )}
                </FormItem>
              </Col>
              {/* <Col span={8}>
                <FormItem label="载重类型:" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('typeId', {
                    initialValue: item.typeId == null ? undefined : item.typeId+'',
                    rules: [{ required: true, message: '请选择载重类型' }],
                  })(
                    <Select 
                     placeholder="请选择载重类型"
                    >
                      {dicIdOption(fireEngine.carTypeChildList)}
                    </Select>
                  )}
                </FormItem>
              </Col>  */}
              <Col span={8}>
                <FormItem label="车身长度" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('carSize', {
                    initialValue: item.carSize,
                    rules: [{ max: 50, message: '最长不超过50个字' }],
                  })(<Input placeholder="请填写车身长度" />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="投用日期" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('investmentDate', {
                    initialValue: item.investmentDate ? new moment(item.investmentDate) : undefined,
                  })(<DatePicker style={{ width: '100%' }} placeholder="请填写投用日期" />)}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={8}>
                <FormItem label={<Tag color="magenta">水量</Tag>} hasFeedback {...formItemLayout}>
                  {getFieldDecorator('water', {
                    initialValue: item.water,
                    rules: [{ required: true, message: '请填写载水量' }],
                  })(
                    <InputNumber style={{ width: '85%' }} min={0} placeholder="请填写载水总容量" />
                  )}
                </FormItem>
                <span style={{ position: 'absolute', right: '38px', top: '8px' }}>(KG)</span>
              </Col>
              <Col span={8}>
                <FormItem
                  required
                  label={<Tag color="magenta">所载干粉</Tag>}
                  hasFeedback
                  {...formItemLayout}
                >
                  {getFieldDecorator('foamTypeId', {
                    initialValue: item.foamTypeId ? item.foamTypeId + '' : undefined,
                    rules: [{ required: true, message: '请选择所载干粉' }],
                  })(
                    <Select
                      placeholder="请选择所载干粉"
                      //onChange={handleFoamTypeChange}
                    >
                      {foamIdOption(fireEngine.foamList)}
                    </Select>
                  )}
                </FormItem>
              </Col>
              <Col span={8}>
                {/* <div style={{ position: 'absolute',  top: '-20px',left:'29.16666667%' }}><span style={{width: '100%', display: 'inline-block',textAlign: 'right'}}>干粉剩余量<span style={{display: 'inline-block',textAlign: 'right'}}>{fireEngine.extinguisherQu}</span>KG</span></div> */}
                <FormItem
                  label={<Tag color="magenta">载干粉量</Tag>}
                  required
                  hasFeedback
                  {...formItemLayout}
                >
                  {getFieldDecorator('capacity', {
                    initialValue: item.capacity,
                    rules: [
                      {
                        required: true,
                        message: '请填写所载干粉量',
                        // validator: checkCapacity,
                      },
                    ],
                  })(
                    <InputNumber style={{ width: '85%' }} min={0} placeholder="请填写所载干粉量" />
                  )}
                </FormItem>
                <span style={{ position: 'absolute', right: '38px', top: '8px' }}>(KG)</span>
              </Col>
            </Row>
            <Row>
              <Col span={8}>
                <FormItem
                  required
                  label={<Tag color="magenta">所载泡沫</Tag>}
                  hasFeedback
                  {...formItemLayout}
                >
                  {getFieldDecorator('foamTypeIdO', {
                    initialValue: item.foamTypeIdO ? item.foamTypeIdO + '' : undefined,
                    rules: [{ required: true, message: '请选择所载泡沫' }],
                  })(
                    <Select
                      placeholder="请选择所载泡沫"
                      //onChange={handlePaoMoChange}
                    >
                      {foamIdOption(fireEngine.foamList)}
                    </Select>
                  )}
                </FormItem>
              </Col>
              <Col span={8}>
                {/* <div style={{ position: 'absolute',  top: '-20px',left:'29.16666667%' }}><span style={{width: '100%', display: 'inline-block',textAlign: 'right'}}>泡沫剩余量<span style={{display: 'inline-block',textAlign: 'right'}}>{fireEngine.powderQuantityQu}</span>KG</span></div> */}
                <FormItem
                  label={<Tag color="magenta">载泡沫量</Tag>}
                  required
                  hasFeedback
                  {...formItemLayout}
                >
                  {getFieldDecorator('powderQuantity', {
                    initialValue: item.powderQuantity,
                    rules: [
                      {
                        required: true,
                        message: '请填写载泡沫量',
                        //  validator: checkPowderQuantity,
                      },
                    ],
                  })(<InputNumber style={{ width: '85%' }} min={0} placeholder="请填写载泡沫量" />)}
                </FormItem>
                <span style={{ position: 'absolute', right: '38px', top: '8px' }}>(KG)</span>
              </Col>
              <Col span={8}>
                <FormItem
                  label={<Tag color="magenta">水泵功率</Tag>}
                  hasFeedback
                  {...formItemLayout}
                >
                  {getFieldDecorator('spraySpeed', {
                    initialValue: item.spraySpeed,
                    rules: [{ required: true, message: '请填写水泵功率' }],
                  })(<InputNumber style={{ width: '79%' }} placeholder="请填写水泵功率" />)}
                </FormItem>
                <span style={{ position: 'absolute', right: '38px', top: '8px' }}>(KG/H)</span>
              </Col>
            </Row>
            <Row>
              <Col span={8}>
                <FormItem label={<Tag color="magenta">炮射程</Tag>} hasFeedback {...formItemLayout}>
                  {getFieldDecorator('gunRange', {
                    initialValue: item.gunRange,
                    rules: [{ required: true, message: '请填写炮射程' }],
                  })(<InputNumber style={{ width: '79%' }} min={0} placeholder="请填写炮射程" />)}
                </FormItem>
                <span style={{ position: 'absolute', right: '52px', top: '8px' }}>(L/S)</span>
              </Col>
              <Col span={8}>
                <FormItem label="举升高度" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('liftingHeight', {
                    initialValue: item.liftingHeight,
                  })(<InputNumber style={{ width: '85%' }} min={0} placeholder="请填写举升高度" />)}
                </FormItem>
                <span style={{ position: 'absolute', right: '42px', top: '8px' }}>(米)</span>
              </Col>
              <Col span={8}>
                <FormItem label="泵浦流量" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('pumpFlow', {
                    initialValue: item.pumpFlow,
                  })(<InputNumber style={{ width: '79%' }} min={0} placeholder="请填写泵浦流量" />)}
                </FormItem>
                <span style={{ position: 'absolute', right: '52px', top: '8px' }}>(L/S)</span>
              </Col>
            </Row>
            <Row>
              <Col span={8}>
                <FormItem label="底盘型号" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('chassisModel', {
                    initialValue: item.chassisModel,
                  })(<Input placeholder="请填写底盘型号" />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="生产厂家" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('manufacturer', {
                    initialValue: item.manufacturer,
                  })(<Input placeholder="请填写生产厂家" />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="执勤状态：" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('stateOfDuty', {
                    initialValue: item.stateOfDuty ? item.stateOfDuty + '' : '1',
                    rules: [{ required: true, message: '请选择执勤状态' }],
                  })(
                    <Select placeholder="请选择执勤状态" style={{ width: '268px' }} allowClear>
                      <Option value="1">执勤</Option>
                      <Option value="2">备勤</Option>
                    </Select>
                  )}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={8}>
                  <FormItem label="出厂日期" hasFeedback {...formItemLayout}>
                    {getFieldDecorator('productionDate', {
                      initialValue: item.productionDate ? new moment(item.productionDate) : undefined,
                    })(<DatePicker style={{ width: '100%' }} placeholder="请填写出厂日期" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="使用年限" hasFeedback {...formItemLayout}>
                    {getFieldDecorator('serviceLife', {
                      initialValue: item.serviceLife,
                    })(<Input placeholder="请填写使用年限" />)}
                  </FormItem>
                </Col>
            </Row>
            <Row>
              <div
                style={{
                  width: '100%',
                  height: '1px',
                  borderBottom: '1px dashed #d3d3d3',
                  marginBottom: '24px',
                }}
              />
            </Row>
            <Row>
              <Col span={8}>
                <FormItem label="是否含有车载炮" {...formItemLayout}>
                  {getFieldDecorator('type', {
                    initialValue: fireEngine.isvehicularArtillery,
                  })(
                    <RadioGroup onChange={changeVehicularArtillery}>
                      <Radio value={1}>含</Radio>
                      <Radio value={2}>不含</Radio>
                    </RadioGroup>
                  )}
                </FormItem>
              </Col>
              {fireEngine.isvehicularArtillery == 1 ? (
                <Col span={8}>
                  <FormItem label="车载炮功率" {...formItemLayout}>
                    {getFieldDecorator('vehicularArtillery', {
                      initialValue: item.vehicularArtillery,
                      rules: [
                        {
                          validator: checkVehicularArtillery,
                        },
                      ],
                    })(
                      <InputNumber
                        style={{ width: '74%' }}
                        min={1}
                        max={99999999}
                        placeholder="请填写车载炮功率"
                      />
                    )}
                  </FormItem>
                  <span style={{ position: 'absolute', right: '52px', top: '8px' }}>(L/S)</span>
                </Col>
              ) : (
                ''
              )}
            </Row>
            <Row>
              <div
                style={{
                  width: '100%',
                  height: '1px',
                  borderBottom: '1px dashed #d3d3d3',
                  marginBottom: '24px',
                }}
              />
            </Row>
            <Row>
              <Col span={24}>
                <FormItem {...formItemLayout1} label="上传照片">
                  <Input
                    type="hidden"
                    {...getFieldProps('path', { initialValue: fireEngine.path })}
                    style={{ paddingRight: '8px' }}
                  />
                  <Upload style={{ width: '300px' }} showUploadList={false} {...uploadCarProps}>
                    <Button>
                      <Icon type="upload" />上传照片
                    </Button>
                  </Upload>
                  <div
                    style={{
                      borderRadius: '4px',
                      border: '1px solid #d9d9d9',
                      position: 'relative',
                      clear: 'both',
                      overflow: 'hidden',
                      margin: '10px 0',
                    }}
                  >
                    <div
                      style={{
                        width: '298px',
                        height: '180px',
                        textAlign: 'center',
                        border: '1px solid #d9d9d9',
                        position: 'relative',
                        overflow: 'hidden',
                      }}
                    >
                      <img
                        style={{
                          width: 'auto',
                          height: 'auto',
                          position: 'absolute',
                          left: '0',
                          right: '0',
                          bottom: '0',
                          top: '0',
                          margin: 'auto',
                          maxWidth: '100%',
                        }}
                        alt=""
                        src={
                          fireEngine.path != null && fireEngine.path != ''
                            ? baseFileUrl + fireEngine.path
                            : 'images/default.png'
                        }
                      />
                    </div>
                  </div>
                  <div
                    style={{
                      color: '#f5222d',
                      fontSize: '12px',
                      lineHeight: '14px',
                    }}
                  >
                    图片大小298*180且不超过30kb
                  </div>
                </FormItem>
              </Col>
            </Row>
            {/* <Row>
              <div style={{width:'100%',height:'1px', borderBottom:'1px dashed #d3d3d3', marginBottom: '14px'}}></div>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem label="图标类型" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('type', {
                    initialValue: item.type == null ? 1 : item.type,
                  })(
                    <RadioGroup style={{ width: '222px' }} onChange={changeRadio}>
                      <Radio value={1}>本地上传</Radio>
                      <Radio value={2}>会标图标</Radio>
                    </RadioGroup>
                  )}
                </FormItem>
              </Col>
            </Row>
            {getFieldValue('type') == 1 ? (
              <Row>
                <Col span={3}>&nbsp;&nbsp;</Col>
                <Col span={6}>
                  <FormItem>
                    <Input
                      type="hidden"
                      {...getFieldProps('path', { initialValue: fireEngine.path })}
                    />
                    <div
                      style={{
                        borderRadius: '4px',
                        border: '1px solid #d9d9d9',
                        height: '84px',
                        position: 'relative',
                        clear: 'both',
                        overflow: 'hidden',
                      }}
                    >
                      <div
                        style={{
                          width: '84px',
                          height: '84px',
                          textAlign: 'center',
                          border: '1px solid #d9d9d9',
                          position: 'relative',
                        }}
                      >
                        <img
                          style={{
                            width: 'auto',
                            height: 'auto',
                            position: 'absolute',
                            left: '0',
                            right: '0',
                            bottom: '0',
                            top: '0',
                            margin: 'auto',
                            maxWidth: '100%'
                          }}
                          alt=""
                          src={fireEngine.path!=null&&fireEngine.path!='' ? baseFileUrl + fireEngine.path: 'images/default.png'}
                        />
                      </div>
                    </div>
                    <Upload style={{ width: '300px' }} showUploadList={false} {...uploadIconProps}>
                      <Button>
                        <Icon type="upload" />上传图标
                      </Button>
                    </Upload>
                    <div
                      style={{
                        color: '#f5222d',
                        color: '#999',
                        fontSize: '12px',
                        lineHeight: '14px',
                      }}
                    >
                      图片大小36*36且不超过5kb
                    </div>
                  </FormItem>
                </Col>
                <Col span={6}>
                  <FormItem>
                    <Input
                      type="hidden"
                      {...getFieldProps('jhPath', { initialValue: fireEngine.jhPath })}
                    />

                    <div
                      style={{
                        borderRadius: '4px',
                        border: '1px solid #d9d9d9',
                        height: '84px',
                        position: 'relative',
                        clear: 'both',
                        overflow: 'hidden',
                      }}
                    >
                      <div
                        style={{
                          width: '84px',
                          height: '84px',
                          textAlign: 'center',
                          border: '1px solid #d9d9d9',
                          position: 'relative',
                        }}
                      >
                        <img
                          style={{
                            width: 'auto',
                            height: 'auto',
                            position: 'absolute',
                            left: '0',
                            right: '0',
                            bottom: '0',
                            top: '0',
                            margin: 'auto',
                            maxWidth: '100%'
                          }}
                          alt=""
                          src={fireEngine.jhPath!=null&&fireEngine.jhPath!='' ? baseFileUrl + fireEngine.jhPath: 'images/default.png'}
                        />
                      </div>
                    </div>
                    <Upload style={{ width: '300px' }} showUploadList={false} {...uploadPropsJh}>
                      <Button>
                        <Icon type="upload" />聚合图标
                      </Button>
                    </Upload>
                    <div
                      style={{
                        color: '#f5222d',
                        color: '#999',
                        fontSize: '12px',
                        lineHeight: '14px',
                      }}
                    >
                      图片大小55*30且不超过5kb
                    </div>
                  </FormItem>
                </Col>
                <Col span={6}>
                  <FormItem>
                    <Input
                      type="hidden"
                      {...getFieldProps('dtPath', { initialValue: fireEngine.dtPath })}
                    />
                    <div
                      style={{
                        borderRadius: '4px',
                        border: '1px solid #d9d9d9',
                        height: '84px',
                        position: 'relative',
                        clear: 'both',
                        overflow: 'hidden',
                      }}
                    >
                      <div
                        style={{
                          width: '84px',
                          height: '84px',
                          textAlign: 'center',
                          border: '1px solid #d9d9d9',
                          position: 'relative',
                        }}
                      >
                        <img
                          style={{
                            width: 'auto',
                            height: 'auto',
                            position: 'absolute',
                            left: '0',
                            right: '0',
                            bottom: '0',
                            top: '0',
                            margin: 'auto',
                            maxWidth: '100%'
                          }}
                          alt=""
                          src={fireEngine.dtPath!=null&&fireEngine.dtPath!='' ? baseFileUrl + fireEngine.dtPath: 'images/default.png'}
                        />
                      </div>
                    </div>
                    <Upload style={{ width: '300px' }} showUploadList={false} {...uploadPropsDt}>
                      <Button>
                        <Icon type="upload" />地图图标
                      </Button>
                    </Upload>
                    <div
                      style={{
                        color: '#f5222d',
                        color: '#999',
                        fontSize: '12px',
                        lineHeight: '14px',
                      }}
                    >
                      图片大小20*20且不超过5kb
                    </div>
                  </FormItem>
                </Col>
                <Col span={3}>&nbsp;&nbsp;</Col>
              </Row>
            ) : (
              <Row>
                <Col span={3}>&nbsp;&nbsp;</Col>
                <Col span={6}>
                  <FormItem>
                    <Input
                      type="hidden"
                      {...getFieldProps('path', { initialValue: fireEngine.currentDate.path })}
                    />

                    <div
                      style={{
                        borderRadius: '4px',
                        border: '1px solid #d9d9d9',
                        height: '84px',
                        position: 'relative',
                        clear: 'both',
                        overflow: 'hidden',
                      }}
                    >
                      <div
                        style={{
                          width: '84px',
                          height: '84px',
                          textAlign: 'center',
                          border: '1px solid #d9d9d9',
                          position: 'relative',
                        }}
                      >
                        <img
                          style={{
                            width: 'auto',
                            height: 'auto',
                            position: 'absolute',
                            left: '0',
                            right: '0',
                            bottom: '0',
                            top: '0',
                            margin: 'auto',
                            maxWidth: '100%'
                          }}
                          alt=""
                          src={fireEngine.path!=null&&fireEngine.path!='' ? baseFileUrl + fireEngine.path: 'images/default.png'}
                        />
                      </div>
                    </div>
                    <div style={{ paddingLeft: '10px' }}>
                      <span style={{ cursor: 'pointer' }} onClick={() => changeRadio1()}>
                        上传图标
                      </span>
                    </div>
                  </FormItem>
                </Col>
                <Col span={6}>
                  <FormItem>
                    <Input
                      type="hidden"
                      {...getFieldProps('jhPath', {
                        initialValue: fireEngine.currentDate.jhPath,
                      })}
                    />

                    <div
                      style={{
                        borderRadius: '4px',
                        border: '1px solid #d9d9d9',
                        height: '84px',
                        position: 'relative',
                        clear: 'both',
                        overflow: 'hidden',
                      }}
                    >
                      <div
                        style={{
                          width: '84px',
                          height: '84px',
                          textAlign: 'center',
                          border: '1px solid #d9d9d9',
                          position:'relative'
                        }}
                      >
                        <img

                          style={{
                            width: 'auto',
                            height: 'auto',
                            position: 'absolute',
                            left: '0',
                            right: '0',
                            bottom: '0',
                            top: '0',
                            margin: 'auto',
                            maxWidth: '100%'
                          }}
                          alt=""
                          src={fireEngine.jhPath!=null&&fireEngine.jhPath!='' ? baseFileUrl + fireEngine.path: 'images/default.png'}
                        />
                      </div>
                    </div>
                    <div style={{ paddingLeft: '10px' }}>
                      <span style={{ cursor: 'pointer' }} onClick={() => changeRadio1()}>
                        聚合图标
                      </span>
                    </div>
                  </FormItem>
                </Col>
                <Col span={6}>
                  <FormItem>
                    <Input
                      type="hidden"
                      {...getFieldProps('dtPath', {
                        initialValue: fireEngine.currentDate.dtPath,
                      })}
                    />

                    <div
                      style={{
                        borderRadius: '4px',
                        border: '1px solid #d9d9d9',
                        height: '84px',
                        position: 'relative',
                        clear: 'both',
                        overflow: 'hidden',
                      }}
                    >
                      <div
                        style={{
                          width: '84px',
                          height: '84px',
                          textAlign: 'center',
                          border: '1px solid #d9d9d9',
                          position: 'relative',
                        }}
                      >
                        <img
                          style={{
                            width: 'auto',
                            height: 'auto',
                            position: 'absolute',
                            left: '0',
                            right: '0',
                            bottom: '0',
                            top: '0',
                            margin: 'auto',
                            maxWidth: '100%'
                          }}
                          alt=""
                          src={fireEngine.dtPath!=null&&fireEngine.dtPath!='' ? baseFileUrl + fireEngine.dtPath: 'images/default.png'}
                        />
                      </div>
                    </div>
                    <div style={{ paddingLeft: '10px' }}>
                      <span style={{ cursor: 'pointer' }} onClick={() => changeRadio1()}>
                        地图图标
                      </span>
                    </div>
                  </FormItem>
                </Col>
                <Col span={3}>&nbsp;&nbsp;</Col>
              </Row>
            )} */}
          </TabPane>
          <TabPane tab="随车器材" key="tab2">
            <Row>
              <Col span={24}>
                <Button type="primary" htmlType="submit" onClick={() => onShow()}>
                  随车器材
                </Button>
              </Col>
            </Row>
            <Table
              columns={columns}
              dataSource={fireEngine.currentSelectList}
              rowKey={record => record.id}
              pagination={false}
              key={fireEngine.equipmentKey}
              scroll={{ y: 700 }}
            />
          </TabPane>
        </Tabs>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { fireEngine: state.fireEngine };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
