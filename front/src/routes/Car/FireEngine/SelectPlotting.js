import React from 'react';
import { Table, Form, Button, Card, Input, Row, Col, Select, Modal } from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import { baseFileUrl } from '../../../config/system';
const Option = Select.Option;
const FormItem = Form.Item;

function SelectPlotting({ location, fireEngine, form, dispatch, loading }) {
  const { getFieldDecorator, getFieldsValue, setFieldsValue, resetFields } = form;

  var selectedRows3 = {},
    _dispatch;

  const columns = [
    { title: '标绘名称', dataIndex: 'name', key: 'name', width: 100 },
    { title: '标绘类型', dataIndex: 'dicName', key: 'dicName', width: 100 },
    { title: '标绘描述', dataIndex: 'remark', key: 'remark', width: 100 },
    {
      title: '小图标',
      dataIndex: 'path',
      key: 'path',
      width: 100,
      render: (value, row, index) => {
        return <img src={baseFileUrl + row.path} style={{ width: '46px', height: '46px' }} />;
      },
    },
    {
      title: '聚合图',
      dataIndex: 'jhPath',
      key: 'jhPath',
      width: 100,
      render: (value, row, index) => {
        return <img src={baseFileUrl + row.jhPath} />;
      },
    },
    {
      title: '地图',
      dataIndex: 'dtPath',
      key: 'dtPath',
      width: 100,
      render: (value, row, index) => {
        return <img src={baseFileUrl + row.dtPath} />;
      },
    },
  ];

  const rowSelection = {
    onSelect(record, selected, selectedRows) {
      selectedRows3 = selectedRows[0];
    },

    type: 'radio',
  };
  const modalOpts = {
    title: '会标图片',
    visible: fireEngine.plottingModalVisible,
    maskClosable: false,
    width: 900,
    zIndex: 20,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={fireEngine.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  //点击保存以后，回显
  function handleOk() {
    dispatch({
      type: `fireEngine/updateState`,
      payload: {
        plottingModalVisible: false,
        currentDate: selectedRows3,
        equipmentCurrent: 1,
        equipmentTotal: 0,
        equipmentList: [],
      },
    });
  }
  function onDelete(id) {
    dispatch({
      type: 'fireEngine/del',
      payload: id,
      search: { pageNum: fireEngine.current, pageSize: fireEngine.pageSize, ...getFieldsValue() },
    });
  }

  function handleSearch() {
    dispatch({
      type: 'fireEngine/qryPlottingList',
      payload: { pageNum: 1, pageSize: fireEngine.pageSize, ...getFieldsValue() },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'fireEngine/qryPlottingList',
      payload: { pageNum: 1, pageSize: fireEngine.pageSize },
    });
  }

  function handleCansel() {
    dispatch({
      type: 'fireEngine/updateState',
      payload: { plottingModalVisible: false },
    });
  }

  const pagination = {
    current: fireEngine.current,
    pageSize: fireEngine.pageSize,
    total: fireEngine.total,
    showSizeChanger: true,
    showTotal: total => '共' + fireEngine.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'fireEngine/qryPlottingList',
        payload: { pageNum: current, pageSize: size, ...getFieldsValue() },
      });
    },
    onChange(current) {
      dispatch({
        type: 'fireEngine/qryPlottingList',
        payload: { pageNum: current, pageSize: fireEngine.pageSize, ...getFieldsValue() },
      });
    },
  };

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id}>{item.name}</Option>;
    });

  return (
    <Modal {...modalOpts} width={900} style={{ top: 5 }}>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={8} sm={24}>
                  <FormItem label="名称">
                    {getFieldDecorator('name')(<Input placeholder="请输入名称" />)}
                  </FormItem>
                </Col>
                <Col md={8} sm={24}>
                  <FormItem label="标绘类型">
                    {getFieldDecorator('dicId')(
                      <Select
                        showSearch
                        style={{ width: '100%' }}
                        placeholder="选择类型"
                        optionFilterProp="children"
                      >
                        <Option value="">全部</Option>
                        {loopOption(fireEngine.typeList)}
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={8} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                      重置
                    </Button>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <Table
            rowSelection={rowSelection}
            columns={columns}
            dataSource={fireEngine.plottingList}
            rowKey={record => record.id}
            loading={loading}
            pagination={pagination}
          />
        </div>
      </Card>
    </Modal>
  );
}

function mapStateToProps(state) {
  return {
    fireEngine: state.fireEngine,
    loading: state.loading.models.fireEngine,
  };
}

SelectPlotting = Form.create()(SelectPlotting);

export default connect(mapStateToProps)(SelectPlotting);
