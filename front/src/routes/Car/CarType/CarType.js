import React, { Fragment } from 'react';
import { Table, Form, Button, Popconfirm, Card, Divider } from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import AddEditModal from './AddEditModal';
import SelectPlotting from './SelectPlotting';
import PageHeader from '../../../layouts/PageHeaderLayout';

function CarType({ carType, form, dispatch, loading }) {
  const { resetFields } = form;
  const AddEditModalProps = {
    item: carType.item,
  };

  const SelectPlottingModalProps = {
    item: carType.item,
    type: carType.modalType,
    maskClosable: false,
    confirmLoading: loading,
    wrapClassName: 'vertical-center-modal',
    newKey: carType.newKey,
  };

  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    { title: '名称', dataIndex: 'name', key: 'name', width: 100 },
    { title: '描述', dataIndex: 'remark', key: 'remark', width: 100 },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 100,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  function onUpdate(id) {
    dispatch({
      type: 'carType/info',
      payload: id,
    });
  }

  function onAdd() {
    resetFields();
    dispatch({
      type: 'carType/updateState',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
      },
    });
  }

  function onDelete(id) {
    dispatch({
      type: 'carType/del',
      payload: id,
      search: carType.selectObj,
    });
  }

  function handleSearch() {
    dispatch({
      type: 'carType/qryByType',
      payload: carType.selectObj,
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'carType/qryByType',
      payload: carType.selectObj,
    });
  }

  return (
    <PageHeader>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={onAdd}>
              新建
            </Button>
          </div>
          <Table
            columns={columns}
            dataSource={carType.list}
            rowKey={record => record.id}
            loading={loading}
            pagination={false}
          />
        </div>
      </Card>
      <AddEditModal {...AddEditModalProps} />
      <SelectPlotting {...SelectPlottingModalProps} />
    </PageHeader>
  );
}

function mapStateToProps(state) {
  return {
    carType: state.carType,
    loading: state.loading.models.carType,
  };
}

CarType = Form.create()(CarType);

export default connect(mapStateToProps)(CarType);
