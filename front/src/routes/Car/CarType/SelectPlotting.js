import React, { Fragment } from 'react';
import {
  Table,
  Form,
  Button,
  Popconfirm,
  Card,
  Input,
  Divider,
  Row,
  Col,
  Select,
  Modal,
  Tabs,
} from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import PageHeader from '../../../layouts/PageHeaderLayout';
import { baseFileUrl } from '../../../config/system';
const Option = Select.Option;
const FormItem = Form.Item;
const TabPane = Tabs.TabPane;

function SelectPlotting({ location, carType, form, dispatch, loading }) {
  const { getFieldDecorator, validateFields, getFieldsValue, setFieldsValue, resetFields } = form;

  var selectedRows3 = {},
    _dispatch;

  const columns = [
    { title: '标绘名称', dataIndex: 'name', key: 'name', width: 100 },
    { title: '标绘类型', dataIndex: 'dicName', key: 'dicName', width: 100 },
    { title: '标绘描述', dataIndex: 'remark', key: 'remark', width: 100 },
    {
      title: '小图标',
      dataIndex: 'path',
      key: 'path',
      width: 100,
      render: (value, row, index) => {
        return <img src={baseFileUrl + row.path} style={{ width: '46px', height: '46px' }} />;
      },
    },
    {
      title: '聚合图',
      dataIndex: 'jhPath',
      key: 'jhPath',
      width: 100,
      render: (value, row, index) => {
        return <img src={baseFileUrl + row.jhPath} />;
      },
    },
    {
      title: '地图',
      dataIndex: 'dtPath',
      key: 'dtPath',
      width: 100,
      render: (value, row, index) => {
        return <img src={baseFileUrl + row.dtPath} />;
      },
    },
  ];

  const rowSelection = {
    onSelect(record, selected, selectedRows) {
      selectedRows3 = selectedRows[0];
    },

    type: 'radio',
  };
  const modalOpts = {
    title: '会标图片',
    visible: carType.plottingModalVisible,
    maskClosable: false,
    width: 900,
    zIndex: 20,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={carType.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  //点击保存以后，回显
  function handleOk() {
    dispatch({
      type: `carType/updateState`,
      payload: {
        plottingModalVisible: false,
        path: selectedRows3.path,
        jhPath: selectedRows3.jhPath,
        dtPath: selectedRows3.dtPath,
        plottingCurrent: 1,
        plottingTotal: 0,
        plottingList: [],
        newKey: new Date().getTime + '',
      },
    });
  }

  function handleSearch() {
    dispatch({
      type: 'carType/qryPlottingList',
      payload: { pageNum: 1, pageSize: carType.pageSize, ...getFieldsValue() },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'carType/qryPlottingList',
      payload: { pageNum: 1, pageSize: carType.pageSize },
    });
  }

  function handleCansel() {
    dispatch({
      type: 'carType/updateState',
      payload: { plottingModalVisible: false, newKey: new Date().getTime + '' },
    });
  }

  const pagination = {
    current: carType.plottingCurrent,
    pageSize: carType.plottingPageSize,
    total: carType.plottingTotal,
    showSizeChanger: true,
    showTotal: total => '共' + carType.plottingTotal + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'carType/qryPlottingList',
        payload: { pageNum: current, pageSize: size, ...getFieldsValue() },
      });
    },
    onChange(current) {
      dispatch({
        type: 'carType/qryPlottingList',
        payload: { pageNum: current, pageSize: carType.plottingPageSize, ...getFieldsValue() },
      });
    },
  };

  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id}>{item.name}</Option>;
    });

  return (
    <Modal {...modalOpts} width={900} style={{ top: 5 }} zIndex={100}>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={8} sm={24}>
                  <FormItem label="名称">
                    {getFieldDecorator('name')(<Input placeholder="请输入名称" />)}
                  </FormItem>
                </Col>
                <Col md={8} sm={24}>
                  <FormItem label="标绘类型">
                    {getFieldDecorator('dicId')(
                      <Select
                        showSearch
                        style={{ width: '100%' }}
                        placeholder="选择类型"
                        optionFilterProp="children"
                      >
                        <Option value="">全部</Option>
                        {loopOption(carType.typeList)}
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={8} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                      重置
                    </Button>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <Table
            rowSelection={rowSelection}
            columns={columns}
            dataSource={carType.plottingList}
            rowKey={record => record.id}
            loading={loading}
            pagination={pagination}
          />
        </div>
      </Card>
    </Modal>
  );
}

function mapStateToProps(state) {
  return {
    carType: state.carType,
    loading: state.loading.models.carType,
  };
}

SelectPlotting = Form.create()(SelectPlotting);

export default connect(mapStateToProps)(SelectPlotting);
