import React from 'react';
import { Form, Input, Modal, Button, Row, Col, TreeSelect } from 'antd';
import { connect } from 'dva';
const FormItem = Form.Item;
const { TextArea } = Input;
import { baseFileUrl } from '../../../config/system';

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
};

let AddEditModal = ({ carType, item = {}, form, dispatch }) => {
  const { getFieldDecorator, validateFields, getFieldsValue, resetFields, getFieldProps } = form;

  const modalOpts = {
    title: carType.modalType == 'create' ? '新建车辆信息类型' : '修改车辆信息类型',
    visible: carType.modalVisible,
    maskClosable: false,
    width: 600,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={carType.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!carType.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'carType/updateState',
      payload: {
        modalVisible: false,
        path: '',
        jhPath: '',
        dtPath: '',
        newKey: new Date().getTime + '',
      },
    });
  }
  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = getFieldsValue();
      data.pid = data.pid ? data.pid : -1;
      data.id = carType.modalType === 'create' ? '' : item.id;
      data.type = 1;
      dispatch({
        type: `carType/${carType.modalType}`,
        payload: data,
        search: carType.selectObj,
      });
    });
  }

  const loop = data =>
    data.map(d => {
      let children = null;
      if (d.children != null && d.children.length > 0) {
        children = loop(d.children);
      }
      return { title: d.name, value: d.id, key: d.id, children, disabled: item.id === d.id };
    });

  const changeRadio1 = function() {
    //展示会标图标
    dispatch({
      type: 'carType/qryPlottingList',
      payload: {
        equipType: '',
        equipmentPageNum: 1,
        equipmentPageSize: 10,
        type: 2,
      },
    });
  };

  return (
    <Modal {...modalOpts} zIndex={10}>
      <Form layout="horizontal">
        <Row>
          <Col span={24}>
            <FormItem label="名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '名称未填写' },
                  { max: 20, message: '最长不超过20个字' },
                ],
              })(<Input type="text" placeholder="请填写名称" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{ height: '56px' }}>
            <FormItem label="上级分类：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('pid', {
                initialValue: item.pid != null && item.pid != -1 ? item.pid + '' : undefined,
              })(
                <TreeSelect
                  allowClear
                  placeholder="请选择上级分类"
                  showCheckedStrategy={'SHOW_CHILD'}
                  treeData={loop(carType.list)}
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="描述：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('remark', {
                initialValue: item.remark,
                rules: [{ max: 100, message: '最长不超过100个字符' }],
              })(<TextArea autosize={{ minRows: 4 }} />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <div
            style={{
              width: '100%',
              height: '1px',
              borderBottom: '1px dashed #d3d3d3',
              marginBottom: '14px',
            }}
          />
        </Row>
        <Row>
          <Col span={3}>&nbsp;&nbsp;</Col>
          <Col span={3}>图标类型:</Col>
          <Button type="primary" htmlType="submit" onClick={() => changeRadio1()}>
            请选择图标
          </Button>
        </Row>
        <Row style={{ top: '10px' }}>
          <Col span={3}>&nbsp;&nbsp;</Col>
          <Col span={3}>&nbsp;&nbsp;</Col>
          <Col span={6}>
            <FormItem>
              <Input type="hidden" {...getFieldProps('path', { initialValue: carType.path })} />
              <div
                style={{
                  borderRadius: '4px',
                  height: '84px',
                  position: 'relative',
                  clear: 'both',
                  overflow: 'hidden',
                }}
              >
                <div
                  style={{
                    width: '84px',
                    height: '84px',
                    textAlign: 'center',
                    border: '1px solid #d9d9d9',
                    position: 'relative',
                  }}
                >
                  <img
                    style={{
                      width: 'auto',
                      height: 'auto',
                      position: 'absolute',
                      left: '0',
                      right: '0',
                      bottom: '0',
                      top: '0',
                      margin: 'auto',
                      maxWidth: '100%',
                    }}
                    alt=""
                    src={
                      carType.path != null && carType.path != ''
                        ? baseFileUrl + carType.path
                        : 'images/default.png'
                    }
                  />
                </div>
              </div>
              <div style={{ paddingLeft: '10px' }}>
                <span style={{ cursor: 'pointer' }} onClick={() => changeRadio1()}>
                  上传图标
                </span>
              </div>
            </FormItem>
          </Col>
          <Col span={6}>
            <FormItem>
              <Input
                type="hidden"
                {...getFieldProps('jhPath', {
                  initialValue: carType.jhPath,
                })}
              />
              <div
                style={{
                  borderRadius: '4px',
                  height: '84px',
                  position: 'relative',
                  clear: 'both',
                  overflow: 'hidden',
                }}
              >
                <div
                  style={{
                    width: '84px',
                    height: '84px',
                    textAlign: 'center',
                    border: '1px solid #d9d9d9',
                    position: 'relative',
                  }}
                >
                  <img
                    style={{
                      width: 'auto',
                      height: 'auto',
                      position: 'absolute',
                      left: '0',
                      right: '0',
                      bottom: '0',
                      top: '0',
                      margin: 'auto',
                      maxWidth: '100%',
                    }}
                    alt=""
                    src={
                      carType.jhPath != null && carType.jhPath != ''
                        ? baseFileUrl + carType.jhPath
                        : 'images/default.png'
                    }
                  />
                </div>
              </div>
              <div style={{ paddingLeft: '10px' }}>
                <span style={{ cursor: 'pointer' }} onClick={() => changeRadio1()}>
                  聚合图标
                </span>
              </div>
            </FormItem>
          </Col>
          <Col span={6}>
            <FormItem>
              <Input
                type="hidden"
                {...getFieldProps('dtPath', {
                  initialValue: carType.dtPath,
                })}
              />

              <div
                style={{
                  borderRadius: '4px',
                  height: '84px',
                  position: 'relative',
                  clear: 'both',
                  overflow: 'hidden',
                }}
              >
                <div
                  style={{
                    width: '84px',
                    height: '84px',
                    textAlign: 'center',
                    border: '1px solid #d9d9d9',
                    position: 'relative',
                  }}
                >
                  <img
                    style={{
                      width: 'auto',
                      height: 'auto',
                      position: 'absolute',
                      left: '0',
                      right: '0',
                      bottom: '0',
                      top: '0',
                      margin: 'auto',
                      maxWidth: '100%',
                    }}
                    alt=""
                    src={
                      carType.dtPath != null && carType.dtPath != ''
                        ? baseFileUrl + carType.dtPath
                        : 'images/default.png'
                    }
                  />
                </div>
              </div>
              <div style={{ paddingLeft: '10px' }}>
                <span style={{ cursor: 'pointer' }} onClick={() => changeRadio1()}>
                  地图图标
                </span>
              </div>
            </FormItem>
          </Col>
          <Col span={3}>&nbsp;&nbsp;</Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { carType: state.carType };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
