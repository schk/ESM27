import React, { Fragment } from 'react';
import { routerRedux } from 'dva/router';
import { Table, Form, Button, Popconfirm, Card, Input, Row, Col, Select, TreeSelect } from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import ShowMessage from './ShowMessage';
import PageHeader from '../../../layouts/PageHeaderLayout';
import AddEditModal from './AddEditModal';
import SelectEqipmnet from './SelectEqipmnet';
import ShowEquipment from './ShowEquipment';

const Option = Select.Option;
const FormItem = Form.Item;

function CarMassage({ location, carMassage, fireEngine, form, dispatch, loading }) {
  const { getFieldDecorator, validateFields, getFieldsValue, setFieldsValue, resetFields } = form;

  const ShowMessageProps = {
    item: carMassage.item,
  };

  const columns = [
    {
      title: '名称',
      dataIndex: 'name',
      key: 'name',
      width: 100,
      render: (text, record, value) => (
        <Fragment>
          <a onClick={() => showMessage(record.id_)}>{record.name}</a>
        </Fragment>
      ),
    },
    { title: '所属消防队', dataIndex: 'fireBrigadeName', key: 'fireBrigadeName', width: 100 },
    { title: '车辆型号', dataIndex: 'model', key: 'model', width: 100 },
    { title: '车辆编号', dataIndex: 'code', key: 'code', width: 100 },
    { title: '车牌号', dataIndex: 'plateNumber', key: 'plateNumber', width: 100 },
    {
      title: '使用状态',
      dataIndex: 'status',
      key: 'status',
      width: 100,
      render: (value, row, index) => {
        return value == 1 ? '在用' : '空闲';
      },
    },
    {
      title: '车载炮量',
      dataIndex: 'vehicularArtillery',
      key: 'vehicularArtillery',
      width: 100,
    },
    {
      title: '灭火剂',
      width: 600,
      children: [
        {
          title: '水',
          dataIndex: 'water',
          key: 'water',
          width: 100,
        },
        {
          title: '泡沫',
          dataIndex: 'foamTypeNameO',
          key: 'foamTypeNameO',
          width: 100,
        },
        {
          title: '泡沫总容量',
          dataIndex: 'powderQuantity',
          key: 'powderQuantity',
          width: 100,
        },
        {
          title: '干粉',
          dataIndex: 'foamTypeName',
          key: 'foamTypeName',
          width: 100,
        },
        {
          title: '干粉总容量',
          dataIndex: 'capacity',
          key: 'capacity',
          width: 100,
        },
      ],
    },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 100,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_)}>修改</a>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];
  //-------------------------------------
  function onUpdate(id) {
    dispatch({
      type: 'carMassage/info',
      payload: id,
    });
    dispatch({
      type: 'carMassage/qryFoamList',
      payload: {},
    });
  }
  function onDelete(id) {}
  function showMessage(id) {
    dispatch({
      type: 'carMassage/getinfo',
      payload: id,
    });
    dispatch({
      type: 'carMassage/qryListByEngineId',
      payload: {
        pageNum: 1,
        pageSize: 10,
        fireEngineId: id,
      },
    });
  }
  //-----------------------------------
  function onDelete(id) {
    dispatch({
      type: 'carMassage/del',
      payload: id,
      search: {
        pageNum: carMassage.current,
        pageSize: carMassage.pageSize,
        ...getFieldsValue(),
      },
    });
  }

  function handleSearch() {
    dispatch({
      type: 'carMassage/qryListByParams',
      payload: {
        pageNum: 1,
        pageSize: carMassage.pageSize,
        ...getFieldsValue(),
      },
    });
  }

  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'carMassage/qryListByParams',
      payload: { pageNum: 1, pageSize: carMassage.pageSize },
    });
  }

  const pagination = {
    current: carMassage.current,
    pageSize: carMassage.pageSize,
    total: carMassage.total,
    showSizeChanger: true,
    showTotal: total => '共' + carMassage.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'carMassage/qryListByParams',
        payload: {
          pageNum: current,
          pageSize: size,
          ...getFieldsValue(),
        },
      });
    },
    onChange(current) {
      dispatch({
        type: 'carMassage/qryListByParams',
        payload: {
          pageNum: current,
          pageSize: carMassage.pageSize,
          ...getFieldsValue(),
          departmentId: carMassage.departmentId,
        },
      });
    },
  };

  const loop = data =>
    data.map(d => {
      let children = null;
      if (d.children != null && d.children.length > 0) {
        children = loop(d.children);
      }
      return { title: d.name, value: d.id_, key: d.id_, children };
    });
  function goToList() {
    dispatch(
      routerRedux.push({
        pathname: '/conventionalEquip/fireEngine',
      })
    );
  }
  const AddEditModalProps = {
    item: carMassage.item,
  };

  const ShowEquipmentProps = {
    item: carMassage.editEquipItem,
  };

  const SelectEqipmnetModalProps = {
    item: carMassage.item,
    visible: carMassage.modalVisible,
    type: carMassage.modalType,
    maskClosable: false,
    equipmentList: carMassage.equipmentList,
    dispatch: dispatch,
    currentSelectList: carMassage.currentSelectList,
    confirmLoading: loading,
    wrapClassName: 'vertical-center-modal',
    newKey: carMassage.newKey,
  };
  function onAdd() {
    resetFields();
    dispatch({
      type: 'carMassage/showCreateModal',
      payload: {
        fireBrigadeTree: carMassage.fireBrigadeTree,
      },
    });
  }
  const dicIdOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });
  return (
    <PageHeader>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={5} sm={24}>
                  <FormItem label="名称">
                    {getFieldDecorator('name')(<Input placeholder="请输入名称" />)}
                  </FormItem>
                </Col>
                <Col md={6} sm={24}>
                  <FormItem label="所属消防队">
                    {getFieldDecorator('fbIds')(
                      <TreeSelect
                        showSearch
                        treeNodeFilterProp="title"
                        optionFilterProp="children"
                        multiple={true}
                        treeCheckable={true}
                        dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                        placeholder="请选择所属消防队"
                        notFoundContent="无匹配结果"
                        allowClear
                        treeData={loop(carMassage.fireBrigadeTree)}
                        treeDefaultExpandAll
                      />
                    )}
                  </FormItem>
                </Col>
                <Col span={5}>
                  <FormItem label="车辆类型:">
                    {getFieldDecorator('dicId')(
                      <Select placeholder="请选择车辆类型" allowClear>
                        {dicIdOption(fireEngine.dicList)}
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={8} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                      重置
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={goToList}>
                      图表数据
                    </Button>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={onAdd}>
              新建
            </Button>
          </div>
        </div>
        <Table
          columns={columns}
          dataSource={carMassage.list}
          rowKey={record => record.id}
          loading={loading}
          pagination={pagination}
          bordered={true}
        />
      </Card>
      <ShowMessage {...ShowMessageProps} />
      <AddEditModal {...AddEditModalProps} />
      <SelectEqipmnet {...SelectEqipmnetModalProps} />
      <ShowEquipment {...ShowEquipmentProps} />
    </PageHeader>
  );
}

function mapStateToProps(state) {
  return {
    carMassage: state.carMassage,
    loading: state.loading.models.carMassage,
    fireEngine: state.fireEngine,
  };
}

CarMassage = Form.create()(CarMassage);

export default connect(mapStateToProps)(CarMassage);
