import React from 'react';
import { Form, Input, Modal, DatePicker, Button, InputNumber, Row, Col } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
const { TextArea } = Input;
const FormItem = Form.Item;
const formItemLayout = {
  labelCol: { span: 7 },
  wrapperCol: { span: 15 },
};

let ShowEquipment = ({ carMassage, item = {}, form, dispatch }) => {
  const { getFieldDecorator, resetFields } = form;
  const modalOpts = {
    title: '修改随车器材',
    visible: carMassage.equipmentVisible,
    width: 600,
    key: carMassage.key2,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={carMassage.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!carMassage.showVisible) {
    resetFields();
  }
  //-------------
  function handleCansel() {
    dispatch({
      type: 'carMassage/updateState',
      payload: {
        equipmentVisible: false,
        key2: new Date().getTime() + '',
      },
    });
  }
  function handleOk() {
    form.validateFields((err, values) => {
      if (!err) {
        for (var i = 0; i < carMassage.currentSelectList.length; i++) {
          var currntItem = carMassage.currentSelectList[i];
          if (values.equipmentId == currntItem.id) {
            currntItem.count = values.count;
            currntItem.useDesc = values.useDesc;
            currntItem.followDate = values.followDate;
          }
        }
        dispatch({
          type: 'carMassage/updateState',
          payload: {
            currentSelectList: carMassage.currentSelectList,
            equipmentVisible: false,
            key2: new Date().getTime() + '',
          },
        });
      }
    });
  }
  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <FormItem style={{ marginBottom: '0px' }}>
          {getFieldDecorator('equipmentId', { initialValue: item.id })(<Input type="hidden" />)}
        </FormItem>

        <Row>
          <Col span={24}>
            <FormItem label="名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
              })(<Input type="text" readOnly placeholder="请填写名称" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="型号:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('code', {
                initialValue: item.code,
              })(<Input type="text" readOnly placeholder="请填写型号" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            {item.equipType == 3 ? (
              <FormItem label="器材数量:" hasFeedback {...formItemLayout}>
                {getFieldDecorator('count', {
                  initialValue: item.count,
                })(<InputNumber style={{ width: '200px' }} placeholder="请填写器材数量" />)}
              </FormItem>
            ) : (
              <FormItem label="器材数量:" hasFeedback {...formItemLayout}>
                {getFieldDecorator('count', {
                  initialValue: 1,
                })(
                  <InputNumber style={{ width: '200px' }} readOnly placeholder="请填写器材数量" />
                )}
              </FormItem>
            )}
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="随车日期:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('followDate', {
                initialValue: item.followDate ? new moment(item.followDate) : undefined,
              })(
                <DatePicker
                  style={{ width: '200px' }}
                  placeholder="请选择随车日期"
                  format={'YYYY-MM-DD'}
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="用途说明:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('useDesc', {
                initialValue: item.useDesc,
              })(<TextArea placeholder="请填写用途说明" autosize={{ minRows: 3 }} />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { carMassage: state.carMassage };
}

ShowEquipment = Form.create()(ShowEquipment);

export default connect(mapStateToProps)(ShowEquipment);
