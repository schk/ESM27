import React from 'react';
import {
  Form,
  Input,
  Modal,
  Button,
  Select,
  Radio,
  Row,
  Col,
  DatePicker,
  Tabs,
  Table,
  Tag,
  InputNumber,
} from 'antd';
import { baseFileUrl } from '../../../config/system';
import { connect } from 'dva';
import moment from 'moment';
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const Option = Select.Option;
const TabPane = Tabs.TabPane;
const formItemLayout = {
  labelCol: {
    span: 7,
  },
  wrapperCol: {
    span: 15,
  },
};

const formItemLayout1 = {
  labelCol: { span: 3 },
  wrapperCol: { span: 15 },
};

let ShowMessage = ({ carMassage, item = {}, form, dispatch }) => {
  const { getFieldDecorator, resetFields } = form;

  const columns = [
    { title: '名称', dataIndex: 'name', key: 'name', width: 180 },
    { title: '型号', dataIndex: 'specificationsModel', key: 'specificationsModel', width: 100 },
    { title: '器材数量', dataIndex: 'count', key: 'count', width: 100 },
    { title: '用途说明', dataIndex: 'useDesc', key: 'useDesc', width: 100 },
    {
      title: '随车日期',
      dataIndex: 'followDate',
      key: 'followDate',
      width: 100,
      render: (value, row, index) => {
        return value ? new moment(value).format('YYYY-MM-DD') : '';
      },
    },
  ];
  const modalOpts = {
    title: '查看车辆信息',
    visible: carMassage.showInfoVisible,
    width: 1300,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        关闭
      </Button>,
    ],
  };

  if (!carMassage.showInfoVisible) {
    resetFields();
  }

  //-------------
  function handleCansel() {
    dispatch({
      type: 'carMassage/updateState',
      payload: {
        showInfoVisible: false,
        fileList: [],
        selectedRows: [],
        key: Math.random(),
        equipmentKey: Math.random(),
      },
    });
  }

  function onChangeTab(targetKey) {
    dispatch({
      type: 'carMassage/updateState',
      payload: { activityKey: targetKey },
    });
  }
  const loop = data =>
    data.map(d => {
      let children = null;
      if (d.children != null && d.children.length > 0) {
        children = loop(d.children);
      }
      return { label: d.name, value: d.id_, key: d.id_, children };
    });

  return (
    <Modal {...modalOpts} style={{ height: 222, top: 5 }}>
      <Form layout="horizontal">
        <FormItem style={{ marginBottom: '0px' }}>
          {getFieldDecorator('idList', {
            initialValue: carMassage.idList,
          })(<Input type="hidden" />)}
        </FormItem>
        <Tabs type="card" activeKey={carMassage.activityKey} onChange={onChangeTab}>
          <TabPane tab="基本信息" key="tab1">
            <Row>
              <Col span={8}>
                <FormItem label="车辆名称:" hasFeedback {...formItemLayout}>
                  {/* <label>{item.name}</label> */}
                  {getFieldDecorator('name', {
                    initialValue: item.name,
                  })(<Input type="text" readOnly />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="所属消防队:" hasFeedback {...formItemLayout}>
                  {/* <label>{item.fireBrigadeName}</label> */}
                  {getFieldDecorator('fireBrigadeName', {
                    initialValue: item.fireBrigadeName,
                  })(<Input type="text" readOnly />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="车辆型号：" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('model', {
                    initialValue: item.model,
                  })(<Input type="text" readOnly />)}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={8}>
                <FormItem label="车辆编号：" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('code', {
                    initialValue: item.code,
                  })(<Input type="text" readOnly />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="车牌号：" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('plateNumber', {
                    initialValue: item.plateNumber,
                  })(<Input type="text" readOnly />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="车辆状态：" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('status', {
                    initialValue: item.status == 1 ? '在用' : '空闲',
                  })(<Input type="text" readOnly />)}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={8}>
                <FormItem label="消防车类别:" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('dicName', {
                    initialValue: item.dicName,
                  })(<Input type="text" readOnly />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="车身长度" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('carSize', {
                    initialValue: item.carSize,
                  })(<Input readOnly />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="投用日期" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('investmentDate', {
                    initialValue: item.investmentDate ? new moment(item.investmentDate) : undefined,
                  })(<DatePicker style={{ width: '100%' }} readOnly />)}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={8}>
                <FormItem label={<Tag color="magenta">水量</Tag>} hasFeedback {...formItemLayout}>
                  {getFieldDecorator('water', {
                    initialValue: item.water,
                  })(<InputNumber style={{ width: '85%' }} readOnly />)}
                </FormItem>
                <span style={{ position: 'absolute', right: '38px', top: '8px' }}>(KG)</span>
              </Col>
              <Col span={8}>
                <FormItem
                  required
                  label={<Tag color="magenta">所载干粉</Tag>}
                  hasFeedback
                  {...formItemLayout}
                >
                  {getFieldDecorator('foamTypeName', {
                    initialValue: item.foamTypeName,
                  })(<Input readOnly />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem
                  label={<Tag color="magenta">载干粉量</Tag>}
                  required
                  hasFeedback
                  {...formItemLayout}
                >
                  {getFieldDecorator('capacity', {
                    initialValue: item.capacity,
                  })(<InputNumber style={{ width: '85%' }} readOnly />)}
                </FormItem>
                <span style={{ position: 'absolute', right: '38px', top: '8px' }}>(KG)</span>
              </Col>
            </Row>

            <Row>
              <Col span={8}>
                <FormItem
                  required
                  label={<Tag color="magenta">所载泡沫</Tag>}
                  hasFeedback
                  {...formItemLayout}
                >
                  {getFieldDecorator('foamTypeNameO', {
                    initialValue: item.foamTypeNameO,
                  })(<Input readOnly />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem
                  label={<Tag color="magenta">载泡沫量</Tag>}
                  required
                  hasFeedback
                  {...formItemLayout}
                >
                  {getFieldDecorator('powderQuantity', {
                    initialValue: item.powderQuantity,
                  })(<InputNumber readOnly style={{ width: '85%' }} />)}
                </FormItem>
                <span style={{ position: 'absolute', right: '38px', top: '8px' }}>(KG)</span>
              </Col>
              <Col span={8}>
                <FormItem
                  label={<Tag color="magenta">水泵功率</Tag>}
                  hasFeedback
                  {...formItemLayout}
                >
                  {getFieldDecorator('spraySpeed', {
                    initialValue: item.spraySpeed,
                  })(<InputNumber readOnly style={{ width: '79%' }} />)}
                </FormItem>
                <span style={{ position: 'absolute', right: '38px', top: '8px' }}>(KG/H)</span>
              </Col>
            </Row>
            <Row>
              <Col span={8}>
                <FormItem label={<Tag color="magenta">炮射程</Tag>} hasFeedback {...formItemLayout}>
                  {getFieldDecorator('gunRange', {
                    initialValue: item.gunRange,
                  })(<InputNumber readOnly style={{ width: '79%' }} />)}
                </FormItem>
                <span style={{ position: 'absolute', right: '52px', top: '8px' }}>(L/S)</span>
              </Col>
              <Col span={8}>
                <FormItem label="举升高度" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('liftingHeight', {
                    initialValue: item.liftingHeight,
                  })(<InputNumber readOnly style={{ width: '85%' }} />)}
                </FormItem>
                <span style={{ position: 'absolute', right: '42px', top: '8px' }}>(米)</span>
              </Col>
              <Col span={8}>
                <FormItem label="泵浦流量" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('pumpFlow', {
                    initialValue: item.pumpFlow,
                  })(<InputNumber readOnly style={{ width: '79%' }} />)}
                </FormItem>
                <span style={{ position: 'absolute', right: '52px', top: '8px' }}>(L/S)</span>
              </Col>
            </Row>
            <Row>
              <Col span={8}>
                <FormItem label="底盘型号" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('chassisModel', {
                    initialValue: item.chassisModel,
                  })(<Input readOnly placeholder="请填写底盘型号" />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="生产厂家" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('manufacturer', {
                    initialValue: item.manufacturer,
                  })(<Input readOnly placeholder="请填写生产厂家" />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="执勤状态：" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('stateOfDuty', {
                    initialValue: item.stateOfDuty == 1 ? '执勤' : '备勤',
                  })(<Input type="text" readOnly />)}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <div
                style={{
                  width: '100%',
                  height: '1px',
                  borderBottom: '1px dashed #d3d3d3',
                  marginBottom: '24px',
                }}
              />
            </Row>
            <Row>
              <Col span={8}>
                <FormItem label="是否含有车载炮" {...formItemLayout}>
                  {getFieldDecorator('type', {
                    initialValue: carMassage.isvehicularArtillery,
                  })(
                    <RadioGroup readOnly>
                      <Radio value={1}>含</Radio>
                      <Radio value={2}>不含</Radio>
                    </RadioGroup>
                  )}
                </FormItem>
              </Col>
              {carMassage.isvehicularArtillery == 1 ? (
                <Col span={8}>
                  <FormItem label="车载炮功率" {...formItemLayout}>
                    {getFieldDecorator('vehicularArtillery', {
                      initialValue: item.vehicularArtillery,
                    })(<InputNumber readOnly style={{ width: '77%' }} />)}
                  </FormItem>
                  <span style={{ position: 'absolute', right: '52px', top: '8px' }}>(L/S)</span>
                </Col>
              ) : (
                ''
              )}
            </Row>
            <Row>
              <div
                style={{
                  width: '100%',
                  height: '1px',
                  borderBottom: '1px dashed #d3d3d3',
                  marginBottom: '24px',
                }}
              />
            </Row>
            <Row>
              <Col span={24}>
                <FormItem {...formItemLayout1} label="上传照片">
                  <div
                    style={{
                      borderRadius: '4px',
                      border: '1px solid #d9d9d9',
                      position: 'relative',
                      clear: 'both',
                      overflow: 'hidden',
                      margin: '10px 0',
                    }}
                  >
                    <div
                      style={{
                        width: '298px',
                        height: '180px',
                        textAlign: 'center',
                        border: '1px solid #d9d9d9',
                        position: 'relative',
                        overflow: 'hidden',
                      }}
                    >
                      <img
                        style={{
                          width: 'auto',
                          height: 'auto',
                          position: 'absolute',
                          left: '0',
                          right: '0',
                          bottom: '0',
                          top: '0',
                          margin: 'auto',
                          maxWidth: '100%',
                        }}
                        alt=""
                        src={
                          carMassage.path != null && carMassage.path != ''
                            ? baseFileUrl + carMassage.path
                            : 'images/default.png'
                        }
                      />
                    </div>
                  </div>
                  <div
                    style={{
                      color: '#f5222d',
                      fontSize: '12px',
                      lineHeight: '14px',
                    }}
                  >
                    图片大小298*180且不超过30kb
                  </div>
                </FormItem>
              </Col>
            </Row>
          </TabPane>
          <TabPane tab="随车器材" key="tab2" style={{ height: 648 }}>
            <Table
              columns={columns}
              dataSource={carMassage.currentSelectList}
              rowKey={record => record.id}
              pagination={false}
              key={carMassage.equipmentKey}
              scroll={{ y: 240 }}
            />
          </TabPane>
        </Tabs>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { carMassage: state.carMassage };
}

ShowMessage = Form.create()(ShowMessage);

export default connect(mapStateToProps)(ShowMessage);
