import React, { Fragment, Component } from 'react';
import { Form, Modal, Button, Select, Row, Col, Table } from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
const FormItem = Form.Item;
const Option = Select.Option;

var selectList = [],
  selectedRows = [];
function addSelectlist(obj) {
  var ind = selectList.indexOf(obj.id_);
  if (ind == -1) {
    selectedRows.push(obj);
    selectList.push(obj.id_);
  }
}
function delSelectlist(obj) {
  var ind = selectList.indexOf(obj.id_);
  if (ind !== -1) {
    selectList.splice(ind, 1);
    selectedRows.splice(ind, 1);
  }
}
function retSelectlist(list) {
  selectList = [];
  selectedRows = [];

  for (var i = 0; i < list.length; i++) {
    selectedRows.push(list[i]);
    selectList.push(list[i].id_);
  }
}

class SelectEqipmnet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      selectList: [],
    };
  }

  componentWillReceiveProps(props) {
    if (!this.props.visible && props.visible) {
      retSelectlist(props.currentSelectList);
      this.setState({ selectList });
    }
  }

  //选择单个
  onSelect = (record, selected, selectedRows) => {
    if (selected) {
      addSelectlist(record);
    } else {
      delSelectlist(record);
    }
    this.setState({ selectList });
  };

  //全选
  onSelectAll = (selected, selectedRows, changeRows) => {
    if (selected) {
      for (var i = 0; i < changeRows.length; i++) {
        addSelectlist(changeRows[i]);
      }
    } else {
      for (var i = 0; i < changeRows.length; i++) {
        delSelectlist(changeRows[i]);
      }
    }
    this.setState({ selectList });
  };

  //反选
  onSelectInvert = (selectedRows, dsd3, dsaf3) => {
    var i = 0,
      k = 0;
    for (i = 0; i < this.props.equipmentList.length; i++) {
      var obj = false;
      for (k = 0; k < selectedRows.length; k++) {
        if (this.props.equipmentList[i].id_ == selectedRows[k]) {
          obj = true;
        }
      }
      if (obj) {
        addSelectlist(this.props.equipmentList[i]);
      } else {
        delSelectlist(this.props.equipmentList[i]);
      }
    }
    this.setState({ selectList });
  };

  render() {
    const { dispatch, carMassage } = this.props;
    const { getFieldDecorator, getFieldsValue, resetFields } = this.props.form;
    const columns = [
      { title: '名称', dataIndex: 'name', key: 'name', width: 180 },
      { title: '型号', dataIndex: 'specificationsModel', key: 'specificationsModel', width: 100 },
      { title: '类型', dataIndex: 'typeName', key: 'typeName', width: 100 },
      { title: '库存', dataIndex: 'storageQuantity', key: 'storageQuantity', width: 80 },
      { title: '计量单位', dataIndex: 'unit', key: 'unit', width: 60 },
      { title: '存放位置', dataIndex: 'position', key: 'position', width: 100 },
    ];

    //--------------------------------------
    //分页
    const pagination = {
      current: carMassage.equipmentCurrent,
      pageSize: carMassage.equipmentPageSize,
      total: carMassage.equipmentTotal,
      showSizeChanger: true,
      showTotal: total => '共' + carMassage.equipmentTotal + '条',
      onShowSizeChange(current, size) {
        dispatch({
          type: 'carMassage/qryEquipmentList',
          payload: { pageNum: current, pageSize: size, ...getFieldsValue() },
        });
      },
      onChange(current) {
        dispatch({
          type: 'carMassage/qryEquipmentList',
          payload: {
            pageNum: current,
            pageSize: carMassage.equipmentPageSize,
            ...getFieldsValue(),
          },
        });
      },
    };

    const rowSelection = {
      selections: true,
      selectedRowKeys: this.state.selectList,
      onSelect: this.onSelect.bind(this),
      onSelectAll: this.onSelectAll.bind(this),
      onSelectInvert: this.onSelectInvert.bind(this),
    };

    //-----------------------------------------------------
    const modalOpts = {
      title: '随车器材1',
      visible: carMassage.modalVisible,
      maskClosable: false,
      width: 1100,
      zIndex: 20,
      onCancel: handleCansel,
      footer: [
        <Button key="back" type="ghost" size="large" onClick={handleCansel}>
          取消
        </Button>,
        <Button
          key="submit"
          type="primary"
          size="large"
          onClick={() => handleOk()}
          loading={carMassage.buttomLoading}
        >
          保存
        </Button>,
      ],
    };

    if (!carMassage.modalVisible) {
      resetFields();
    }

    function handleCansel() {
      dispatch({
        type: 'carMassage/updateState',
        payload: { modalVisible: false },
      });
    }

    //点击保存以后，回显
    function handleOk() {
      var arr = [];
      for (var i = 0; i < selectedRows.length; i++) {
        arr.push(selectedRows[i]);
      }
      dispatch({
        type: `carMassage/updateState`,
        payload: {
          modalVisible: false,
          currentSelectList: arr,
          equipmentCurrent: 1,
          equipmentTotal: 0,
          equipmentList: [],
        },
      });
    }

    function handleSearch() {
      dispatch({
        type: 'carMassage/qryEquipmentList',
        payload: {
          pageNum: 1,
          pageSize: carMassage.pageSize,
          ...getFieldsValue(),
        },
      });
    }
    function handleFormReset() {
      resetFields();
      dispatch({
        type: 'carMassage/qryEquipmentList',
        payload: { pageNum: 1, pageSize: carMassage.pageSize },
      });
    }
    return (
      <Modal {...modalOpts}>
        <div className={styles.tableListForm}>
          <Form layout="inline">
            <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
              <Col md={10} sm={24}>
                <FormItem label="器材类型">
                  {getFieldDecorator('equipType')(
                    <Select placeholder="选择器材类型">
                      <Option value="">全部</Option>
                      <Option value="1">专勤器材</Option>
                      <Option value="2">空气呼气器</Option>
                      <Option value="3">常规器材</Option>
                    </Select>
                  )}
                </FormItem>
              </Col>
              <Col md={8} sm={24}>
                <span className={styles.submitButtons}>
                  <Button type="primary" htmlType="submit" onClick={handleSearch}>
                    查询
                  </Button>
                  <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                    重置
                  </Button>
                </span>
              </Col>
            </Row>
          </Form>
        </div>
        <Table
          columns={columns}
          dataSource={carMassage.equipmentList}
          rowKey={record => record.id}
          pagination={pagination}
          rowSelection={rowSelection}
          key={carMassage.key}
        />
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return { carMassage: state.carMassage };
}

SelectEqipmnet = Form.create()(SelectEqipmnet);

export default connect(mapStateToProps)(SelectEqipmnet);
