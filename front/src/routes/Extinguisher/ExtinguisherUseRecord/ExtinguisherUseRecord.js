import React, { Fragment, Component } from 'react';
import {
  Table,
  Form,
  Button,
  Popconfirm,
  Card,
  Divider,
  Select,
  Row,
  Col,
  TreeSelect,
  Tabs,
} from 'antd';
import { connect } from 'dva';
import echarts from 'echarts';
import { Stat, Frame, G2 } from 'g2';
import createG2 from 'g2-react';
import styles from '../../../common/common.less';
import PageHeader from '../../../layouts/PageHeaderLayout';
import PieModal from './pieModal.js';
const FormItem = Form.Item;
const Option = Select.Option;
const TabPane = Tabs.TabPane;

function ExtinguisherUseRecord({ location, extinguisherUseRecord, form, dispatch, loading }) {
  const { getFieldDecorator, validateFields, getFieldsValue, setFieldsValue, resetFields } = form;

  function handleSearch() {
    dispatch({
      type: 'extinguisherUseRecord/qryListByParams',
      payload: { ...getFieldsValue() },
    });
  }
  function handleFormReset() {}
  const PieModalProps = {
    pieData: extinguisherUseRecord.pieData,
  };
  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });
  function callback(key) {
    dispatch({
      type: 'extinguisherUseRecord/qryListByParams',
      payload: { ...getFieldsValue() },
    });
  }
  const columns = data =>
    data.map(d => {
      return {
        title: d.title,
        dataIndex: d.key,
        key: d.key,
        fixed: d.cancolSpan ? 'left' : '',
        width: d.width,
      };
    });
  return (
    <PageHeader>
      <div>
        <Card bordered={false}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={6} sm={24}>
                  <Form.Item label="灭火剂类型：">
                    {getFieldDecorator('dictionaryIds')(
                      <Select allowClear placeholder="请选择灭火剂类型" mode="multiple">
                        {loopOption(extinguisherUseRecord.dictionaryTypes)}
                      </Select>
                    )}
                  </Form.Item>
                </Col>
                <Col md={6} sm={24}>
                  <Form.Item label="消防队：">
                    {getFieldDecorator('fireBrigadeIds')(
                      <TreeSelect
                        optionFilterProp="children"
                        showSearch
                        allowClear
                        multiple={true}
                        treeCheckable={true}
                        placeholder="请选择消防队"
                        showCheckedStrategy={'SHOW_ALL'}
                        treeData={extinguisherUseRecord.fireBrigades}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col md={8} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    &nbsp;&nbsp;
                    <Button type="primary" htmlType="submit">
                      导出
                    </Button>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <br />
          <div className={styles.tableList}>
            <Tabs onChange={callback} type="card">
              <TabPane tab="列表数据" key="1">
                <Table
                  columns={columns(extinguisherUseRecord.columns)}
                  dataSource={extinguisherUseRecord.list}
                  rowKey={record => record.id}
                  loading={loading}
                  pagination={false}
                  bordered={true}
                  scroll={{ x: extinguisherUseRecord.width, y: 580 }}
                />
              </TabPane>
              <TabPane tab="图形数据" key="2">
                <div>
                  <PieModal {...PieModalProps} />
                </div>
              </TabPane>
            </Tabs>
          </div>
        </Card>
      </div>
    </PageHeader>
  );
}

function mapStateToProps(state) {
  return {
    extinguisherUseRecord: state.extinguisherUseRecord,
    loading: state.loading.models.extinguisherUseRecord,
  };
}

ExtinguisherUseRecord = Form.create()(ExtinguisherUseRecord);
export default connect(mapStateToProps)(ExtinguisherUseRecord);
