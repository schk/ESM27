import React, { Component, PropTypes } from 'react';
import { connect } from 'dva';
import { hashHistory } from 'dva/router';
import styles from './index.less';
import { Icon, Select, Button, DatePicker, Table, Input } from 'antd';
import G2 from 'g2';
import createG2 from 'g2-react';
var _state, _props, _this;

export default class pieModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
    };
  }
  componentWillReceiveProps(newProps) {
    if (this.state.data === newProps.pieData) {
    } else {
      this.setState({ data: newProps.pieData });
    }
  }
  componentDidMount() {}
  render() {
    const Line = createG2(chart => {
      chart
        .intervalDodge()
        .position('label*value')
        .color('name');
      //3、定义别名
      chart.cols({
        label: {
          alias: '灭火剂类型', // 设置属性的别名
        },
        value: {
          alias: '消耗值',
        },
      });
      //图例
      chart.legend({
        mode: false,
        position: 'top', // 图例的显示位置，有 'top','left','right','bottom'四种位置，默认是'right'。
        dy: 1,
      });
      //最后，绘制组件
      chart.render();
    });

    return (
      <div>
        <Line
          data={this.state.data}
          forceFit={true}
          height={550}
          plotCfg={{
            margin: [80, 80, 90, 80],
          }}
        />
      </div>
    );
  }
}
