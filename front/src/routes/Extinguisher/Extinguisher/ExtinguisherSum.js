import React, { Fragment } from 'react';
import {
  Table,
  Form,
  Button,
  Tabs,
  Popconfirm,
  Card,
  Divider,
  Select,
  TreeSelect,
  Row,
  Col,
} from 'antd';
import { connect } from 'dva';
import styles from '../../../common/common.less';
import PageHeader from '../../../layouts/PageHeaderLayout';
import Lnterval from './Lnterval';

function ExtinguisherSum({ location, extinguisherSum, form, dispatch, loading }) {
  const { getFieldDecorator, validateFields, getFieldsValue, setFieldsValue, resetFields } = form;
  const AddEditModalProps = {
    item: extinguisherSum.item,
  };
  const TreeNode = TreeSelect.TreeNode;
  const FormItem = Form.Item;
  const Option = Select.Option;
  const TabPane = Tabs.TabPane;

  function handleSearch() {
    dispatch({
      type: 'extinguisherSum/qrySumByKeyId',
      payload: { ...getFieldsValue() },
    });
  }
  function handleFormReset() {
    resetFields();
    dispatch({
      type: 'extinguisherSum/qrySumByKeyId',
      payload: {},
    });
  }
  const treeData = i => {
    return i.map(d => {
      d.label = d.name;
      d.key = d.id_ + '';
      d.value = d.id_ + '';
      d.children = d.children && d.children.length > 0 ? treeData(d.children) : [];
      return d;
    });
  };
  const loopOption = data =>
    data.map(item => {
      return <Option key={item.id_}>{item.name}</Option>;
    });
  function callback(key) {
    dispatch({
      type: 'extinguisherSum/qrySumByKeyId',
      payload: { ...getFieldsValue() },
    });
  }
  const LntervalLineProps = {
    chartData: extinguisherSum.charDataList,
  };

  const columns = data =>
    data.map(d => {
      return {
        title: d.title,
        dataIndex: d.key,
        key: d.key,
        fixed: d.cancolSpan ? 'left' : '',
        width: d.width,
      };
    });
  return (
    <PageHeader>
      <Card bordered={false}>
        <div className={styles.tableListForm}>
          <Form layout="inline">
            <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
              <Col md={6} sm={24}>
                <Form.Item label="灭火剂类型：">
                  {getFieldDecorator('dictionaryIds')(
                    <Select allowClear placeholder="请选择灭火剂类型" mode="multiple">
                      {loopOption(extinguisherSum.dictionaryTypes)}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col md={6} sm={24}>
                <Form.Item label="消防队：">
                  {getFieldDecorator('fireBrigadeIds')(
                    <TreeSelect
                      optionFilterProp="children"
                      showSearch
                      allowClear
                      multiple={true}
                      treeCheckable={true}
                      placeholder="请选择消防队"
                      showCheckedStrategy={'SHOW_ALL'}
                      treeData={extinguisherSum.fireBrigades}
                    />
                  )}
                </Form.Item>
              </Col>
              <Col md={8} sm={24}>
                <span className={styles.submitButtons}>
                  <Button type="primary" htmlType="submit" onClick={handleSearch}>
                    查询
                  </Button>
                  <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                    重置
                  </Button>
                </span>
              </Col>
            </Row>
          </Form>
        </div>
        <div className={styles.gutterExample}>
          <Row gutter={16}>
            <Col className={styles.gutterRow} span={8}>
              <div
                className={styles.gutterBox}
                style={{
                  background: '#668eff url(well-overlay.png) center bottom no-repeat',
                  border: 'solid 1px #dfe4ea',
                  borderRadius: '4px',
                  overflow: 'hidden',
                  position: 'relative',
                  display: 'block',
                  marginBottom: '20px',
                  boxShadow: '0 1px 1px rgba(0, 0, 0, 0.1)',
                  padding: '10px 10px 12px',
                }}
              >
                <p style={{ color: '#fff', marginBottom: '10px', fontSize: '12px' }}>随车</p>
                <h3
                  style={{
                    fontSize: '26px',
                    color: '#fff',
                    lineHeight: '36px',
                    fontweight: 'bold',
                    margin: '0',
                  }}
                >
                  {extinguisherSum.carQuantity}kG
                </h3>
              </div>
            </Col>
            <Col className={styles.gutterRow} span={8}>
              <div
                className={styles.gutterBox}
                style={{
                  background: '#668eff url(well-overlay.png) center bottom no-repeat',
                  border: 'solid 1px #dfe4ea',
                  borderRadius: '4px',
                  overflow: 'hidden',
                  position: 'relative',
                  display: 'block',
                  marginBottom: '20px',
                  boxShadow: '0 1px 1px rgba(0, 0, 0, 0.1)',
                  padding: '10px 10px 12px',
                }}
              >
                <p style={{ color: '#fff', marginBottom: '10px', fontSize: '12px' }}>在库</p>
                <h3
                  style={{
                    fontSize: '26px',
                    color: '#fff',
                    lineHeight: '36px',
                    fontweight: 'bold',
                    margin: '0',
                  }}
                >
                  {extinguisherSum.totalUsedAmount}kG
                </h3>
              </div>
            </Col>
            <Col className={styles.gutterRow} span={8}>
              <div
                className={styles.gutterBox}
                style={{
                  background: '#668eff url(well-overlay.png) center bottom no-repeat',
                  border: 'solid 1px #dfe4ea',
                  borderRadius: '4px',
                  overflow: 'hidden',
                  position: 'relative',
                  display: 'block',
                  marginBottom: '20px',
                  boxShadow: '0 1px 1px rgba(0, 0, 0, 0.1)',
                  padding: '10px 10px 12px',
                }}
              >
                <p style={{ color: '#fff', marginBottom: '10px', fontSize: '12px' }}>已消耗</p>
                <h3
                  style={{
                    fontSize: '26px',
                    color: '#fff',
                    lineHeight: '36px',
                    fontweight: 'bold',
                    margin: '0',
                  }}
                >
                  {extinguisherSum.quantity}kG
                </h3>
              </div>
            </Col>
          </Row>
        </div>
        <div className={styles.tableList}>
          <Tabs onChange={callback} type="card">
            <TabPane tab="列表数据" key="1">
              <Table
                columns={columns(extinguisherSum.colums)}
                dataSource={extinguisherSum.sumList}
                loading={loading}
                pagination={false}
                bordered={true}
                size="middle"
                scroll={{ x: extinguisherSum.width, y: 800 }}
              />
            </TabPane>
            <TabPane tab="图形数据" key="2">
              <div>
                <Lnterval {...LntervalLineProps} />
              </div>
            </TabPane>
          </Tabs>
        </div>
      </Card>
    </PageHeader>
  );
}

function mapStateToProps(state) {
  return {
    extinguisherSum: state.extinguisherSum,
    loading: state.loading.models.extinguisherSum,
  };
}

ExtinguisherSum = Form.create()(ExtinguisherSum);

export default connect(mapStateToProps)(ExtinguisherSum);
