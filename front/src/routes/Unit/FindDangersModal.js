import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Modal,
  Form,
  Input,
  Radio,
  TreeSelect,
  Select,
  Upload,
  Tabs,
  Button,
  Icon,
  message,
  Row,
  Col,
  InputNumber,
  Popconfirm,
} from 'antd';
import { baseUrl, baseFileUrl } from '../../config/system';
const TreeNode = TreeSelect.TreeNode;
const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const RadioGroup = Radio.Group;
const Option = Select.Option;
const { TextArea } = Input;

class AddDangersModal extends Component {
  constructor(props) {
    super(props);
  }

  okHandler = () => {
    const { onOk, catalogIds } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        var keyUnitDangersCatalogs = [];
        // if (catalogIds == null || catalogIds.length == 0) {
        //   message.error('请添加MSDS文档目录信息');
        // } else {
        //   for (var i = 0; i < catalogIds.length; i++) {
        //     var catalog = {};
        //     catalog.name = values['name_' + (i + 1)];
        //     catalog.page = values['page_' + (i + 1)];
        //     catalog.sort = i + 1;
        //     keyUnitDangersCatalogs.push(catalog);
        //   }
        // }
        if (catalogIds != null && catalogIds.length > 0) {
          for (var i = 0; i < catalogIds.length; i++) {
            let name = values['name_' + (i + 1)];
            for (var j = i + 1; j < catalogIds.length; j++) {
              if (name == values['name_' + (j + 1)]) {
                message.error('目录名称不能重复');
                return;
              }
            }
          }
          for (var i = 0; i < catalogIds.length; i++) {
            var catalog = {};
            catalog.name = values['name_' + (i + 1)];
            catalog.page = values['page_' + (i + 1)];
            catalog.sort = i + 1;
            keyUnitDangersCatalogs.push(catalog);
          }
        }
        values.keyUnitDangersCatalogs = keyUnitDangersCatalogs;
        onOk(values);
      }
    });
  };

  render() {
    const {
      children,
      dangers,
      dispatch,
      item,
      dangerTypeNoTopTree,
      typeId,
      msdsPath,
      imgPath,
      detailActiveKey,
      swfPath,
      catalogIds,
      catalogNames,
      uuid,
    } = this.props;
    const { getFieldDecorator, getFieldProps, getFieldValue, setFieldsValue } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 14 },
    };

    function onChangeTab(targetKey) {
      dispatch({
        type: 'keyUnit/updateState',
        payload: { detailActiveKey: targetKey },
      });
    }

    function onShowDocument(path) {
      dispatch({
        type: 'keyUnit/getDocumnet',
        payload: {
          documnetModalVisible: true,
          viweSwfPath: path,
          newKey: new Date().getTime() + '',
        },
      });
    }

    const treeData = i => {
      return i.map(d => {
        d.title = d.name;
        d.key = d.id_ + '';
        d.value = d.id_ + '';
        d.children = d.children && d.children.length > 0 ? treeData(d.children) : [];
        return d;
      });
    };

    //图片上传
    const pictureUploadProps = {
      action: baseUrl + '/common/uploadImg.jhtml',
      withCredentials: true,
      listType: 'picture',
      onChange: handleChange,
      onRemove: pictureHandleRemove,
    };

    function pictureHandleRemove() {
      dispatch({
        type: 'keyUnit/updateState',
        payload: {
          imgPath: '',
        },
      });
    }
    function handleChange(info) {
      let fileList = info.fileList;
      if (info.file.status === 'done') {
        if (info.file.response.httpCode == 200) {
          message.success(`${info.file.name} 上传成功`);
          dispatch({
            type: 'keyUnit/updateState',
            payload: {
              imgPath: info.file.response.data.fid,
            },
          });
        } else {
          message.error(info.file.response.msg);
        }
        fileList = fileList.filter(file => {
          return true;
        });
        //只保留最后一条记录
        fileList = fileList.slice(-1);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`);
      }
    }

    //文件上传
    const systemUploadProps = {
      action: baseUrl + '/common/uploadImg.jhtml',
      withCredentials: true,
      listType: 'text',
      onChange: systemHandleChange,
      onRemove: systemHandleRemove,
    };

    //文件上传事件切换
    function systemHandleChange(info) {
      let fileList = info.fileList;
      if (info.file.status === 'done') {
        if (info.file.response.httpCode == 200) {
          message.success(`${info.file.name} 上传成功`);
          dispatch({
            type: 'keyUnit/updateState',
            payload: {
              msdsPath: info.file.response.data.fid,
              swfPath: info.file.response.data.pdfFid,
            },
          });
        } else {
          message.error(info.file.response.msg);
        }
        fileList = fileList.filter(file => {
          return true;
        });
        //只保留最后一条记录
        fileList = fileList.slice(-1);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`);
      }
      setFieldsValue({ fileName: fileList[0].name });
    }
    //删除制度规程文件
    function systemHandleRemove() {
      dispatch({
        type: 'keyUnit/updateState',
        payload: {
          msdsPath: '',
          swfPath: '',
        },
      });
    }
    function initCatalogs(values) {
      return values != null && values.length > 0
        ? values.map((k, index) => {
            return (
              <Row key={'page' + k}>
                <Col span={11}>
                  <FormItem {...formItemLayout} label="目录名称" hasFeedback>
                    {getFieldDecorator(`name_${k}`, {
                      initialValue: catalogNames['name_' + k],
                      rules: [
                        { required: true, whitespace: true, message: '请输入目录名称' },
                        { max: 50, message: '最长不超过50个字符' },
                      ],
                    })(<Input readOnly />)}
                  </FormItem>
                </Col>
                <Col span={11}>
                  <FormItem {...formItemLayout} label="起始页码" hasFeedback>
                    {getFieldDecorator(`page_${k}`, {
                      initialValue: catalogNames['page_' + k],
                      rules: [{ required: true, message: '请输入起始页码' }],
                    })(<InputNumber min={1} max={99999999} readOnly style={{ width: '80%' }} />)}
                  </FormItem>
                </Col>
              </Row>
            );
          })
        : null;
    }
    function onAdd() {
      var caseMore = catalogIds;
      var newUuid = uuid;
      newUuid++;
      caseMore.push(newUuid);
      dispatch({
        type: 'keyUnit/updateState',
        payload: {
          catalogIds: catalogIds,
          uuid: newUuid,
        },
      });
    }
    function onRemoveClg(index) {
      var caseMore = catalogIds;
      caseMore.splice(index, 1);
      dispatch({
        type: 'keyUnit/updateState',
        payload: {
          catalogIds: catalogIds,
        },
      });
    }
    function handleCansel() {
      dispatch({
        type: 'keyUnit/updateState',
        payload: {
          FindModalDangersVisible: false,
          activityKey: 'tab1',
        },
      });
    }

    return (
      <span>
        <Modal
          maskClosable={this.props.maskClosable}
          confirmLoading={this.props.confirmLoading}
          title={this.props.title}
          visible={this.props.visible}
          width={800}
          onOk={this.okHandler}
          onCancel={this.props.onCancel}
          footer={[
            <Button key="close" type="primary" size="large" onClick={this.props.onCancel}>
              关闭
            </Button>,
          ]}
          key={this.props.newKey}
        >
          <Form layout="horizontal" onSubmit={this.okHandler}>
            <FormItem style={{ marginBottom: '0px' }}>
              {getFieldDecorator('id', { initialValue: item.id })(<Input type="hidden" />)}
            </FormItem>
            <FormItem style={{ marginBottom: '0px' }}>
              {getFieldDecorator('swfPath', {
                initialValue: item.swfPath,
              })(<Input type="hidden" />)}
            </FormItem>
            <Row>
              <Col span={12}>
                <FormItem {...formItemLayout} label="中文名称" hasFeedback>
                  {getFieldDecorator('cName', {
                    initialValue: item.cName,
                  })(<Input readOnly />)}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem {...formItemLayout} label="英文名" hasFeedback>
                  {getFieldDecorator('eName', {
                    initialValue: item.eName,
                  })(<Input readOnly />)}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem {...formItemLayout} label="所属分类" hasFeedback>
                  {getFieldDecorator('typeId', {
                    initialValue: item.typeName,
                  })(<Input readOnly />)}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem {...formItemLayout} label="UN号" hasFeedback>
                  {getFieldDecorator('unNum', {
                    initialValue: item.unNum,
                  })(<Input readOnly />)}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem {...formItemLayout} label="CAS号" hasFeedback>
                  {getFieldDecorator('casNum', {
                    initialValue: item.casNum,
                  })(<Input readOnly />)}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem {...formItemLayout} label="文档查看" hasFeedback>
                  <Button
                    type="primary"
                    htmlType="submit"
                    onClick={() => onShowDocument(item.swfPath)}
                  >
                    预览
                  </Button>
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem {...formItemLayout} label="危化品图标">
                  <Upload
                    style={{ width: '300px' }}
                    showUploadList={false}
                    {...pictureUploadProps}
                  />
                  {imgPath === null ||
                  imgPath === '' ||
                  imgPath === 'undefined' ||
                  imgPath === undefined ? (
                    ''
                  ) : (
                    <div
                      style={{
                        paddingLeft: '8px',
                        borderRadius: '4px',
                        border: '1px solid #d9d9d9',
                        height: '66px',
                        position: 'relative',
                        clear: 'both',
                        overflow: 'hidden',
                      }}
                    >
                      <div
                        style={{
                          width: '60px',
                          height: '60px',
                          textAlign: 'center',
                          border: '1px solid #d9d9d9',
                        }}
                      >
                        <img
                          style={{ width: '60px', height: '60px' }}
                          src={baseFileUrl + imgPath}
                          alt=""
                        />
                      </div>
                    </div>
                  )}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem {...formItemLayout} label="文件名称">
                  {getFieldDecorator('fileName', {
                    initialValue: item.fileName,
                  })(<Input readOnly />)}
                </FormItem>
              </Col>
            </Row>
            {initCatalogs(catalogIds)}

            <br />
            <Tabs type="card" activeKey={detailActiveKey} onChange={onChangeTab}>
              <TabPane tab="化学品名称" key="tab1">
                <Row>
                  <Col span={12}>
                    <FormItem label="中文名称2:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('cName2', {
                        initialValue: item.cName2,
                      })(<Input type="text" readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="英文名称2:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('eName2', {
                        initialValue: item.eName2,
                      })(<Input type="text" readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="分子量:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('molecularWeight', {
                        initialValue: item.molecularWeight,
                      })(<Input type="text" readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="分子式:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('molecularFormula', {
                        initialValue: item.molecularFormula,
                      })(<Input type="text" readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="成分/组成信息" key="tab2">
                <Row>
                  <Col span={12}>
                    <FormItem label="有害物成分:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('harmfulIngredients', {
                        initialValue: item.harmfulIngredients,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="含量:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('content', {
                        initialValue: item.content,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="危险性概述" key="tab3">
                <Row>
                  <Col span={12}>
                    <FormItem label="健康危害:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('healthHazards', {
                        initialValue: item.healthHazards,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="环境危害" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('environmentalHazards', {
                        initialValue: item.environmentalHazards,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="爆炸危险:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('fireExplosionDanger', {
                        initialValue: item.fireExplosionDanger,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="急救措施" key="tab4">
                <Row>
                  <Col span={12}>
                    <FormItem label="皮肤接触:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('skinContact', {
                        initialValue: item.skinContact,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="眼睛接触" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('eyeContact', {
                        initialValue: item.eyeContact,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="吸入:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('inhalation', {
                        initialValue: item.inhalation,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="食入" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('feedInto', {
                        initialValue: item.feedInto,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="消防措施" key="tab5">
                <Row>
                  <Col span={12}>
                    <FormItem label="危险性:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('hazardCharacteristics', {
                        initialValue: item.hazardCharacteristics,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="有害燃烧产物" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('harmfulCombustionProducts', {
                        initialValue: item.harmfulCombustionProducts,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="灭火方法:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('fireExtinguishingMethod', {
                        initialValue: item.fireExtinguishingMethod,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="泄漏应急处理" key="tab6">
                <Row>
                  <Col span={12}>
                    <FormItem label="应急处理:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('emergencyManagement', {
                        initialValue: item.emergencyManagement,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="操作处置与储存" key="tab7">
                <Row>
                  <Col span={12}>
                    <FormItem label="操作处理:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('attentionOperation', {
                        initialValue: item.attentionOperation,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="储存注意事项:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('precautionsStorage', {
                        initialValue: item.precautionsStorage,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="接触控制/个体防护" key="tab8">
                <Row>
                  <Col span={12}>
                    <FormItem label="中国MAC(mg/m3):" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('chinaMac', {
                        initialValue: item.chinaMac,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="前苏联MAC:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('sovietUnionMac', {
                        initialValue: item.sovietUnionMac,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="监测方法:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('monitoringMethod', {
                        initialValue: item.monitoringMethod,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="工程控制:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('engineeringControl', {
                        initialValue: item.engineeringControl,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="呼吸系统:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('respiratorySystemProtection', {
                        initialValue: item.respiratorySystemProtection,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="眼睛防护:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('eyeProtection', {
                        initialValue: item.eyeProtection,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="身体防护:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('bodyProtection', {
                        initialValue: item.bodyProtection,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="手防护:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('handProtection', {
                        initialValue: item.handProtection,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="其他防护:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('otherProtection', {
                        initialValue: item.otherProtection,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="理化特性" key="tab9">
                <Row>
                  <Col span={12}>
                    <FormItem label="主要成分:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('mainComponents', {
                        initialValue: item.mainComponents,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="外观与性:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('appearanceCharacter', {
                        initialValue: item.appearanceCharacter,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="pH:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('ph', {
                        initialValue: item.ph,
                      })(<Input type="text" readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="熔点(℃):" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('meltingPoint', {
                        initialValue: item.meltingPoint,
                      })(<Input type="text" readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="沸点(℃):" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('expensePoint', {
                        initialValue: item.expensePoint,
                      })(<Input type="text" readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="相对密度:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('relativeDensity', {
                        initialValue: item.relativeDensity,
                      })(<Input type="text" readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="相对蒸汽压:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('relativeSteam', {
                        initialValue: item.relativeSteam,
                      })(<Input type="text" readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="饱和蒸汽压:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('saturatedVaporPressure', {
                        initialValue: item.saturatedVaporPressure,
                      })(<Input type="text" readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="燃烧热(kJ/mol):" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('burningHeat', {
                        initialValue: item.burningHeat,
                      })(<Input type="text" readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="临界温度:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('criticalTemperature', {
                        initialValue: item.criticalTemperature,
                      })(<Input type="text" readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="临界压力:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('criticalPressure', {
                        initialValue: item.criticalPressure,
                      })(<Input type="text" readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="辛醇/水分配数值:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('waterDistributionCoefficient', {
                        initialValue: item.waterDistributionCoefficient,
                      })(<Input type="text" readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="闪点(℃):" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('flashPoint', {
                        initialValue: item.flashPoint,
                      })(<Input type="text" readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="引燃温度(℃):" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('ignitionTemperature', {
                        initialValue: item.ignitionTemperature,
                      })(<Input type="text" readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="爆炸上限:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('upperLimitExplosion', {
                        initialValue: item.upperLimitExplosion,
                      })(<Input type="text" readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="爆炸下限:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('lowerLimitExplosion', {
                        initialValue: item.lowerLimitExplosion,
                      })(<Input type="text" readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="溶解性:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('solubility', {
                        initialValue: item.solubility,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="主要用途:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('mainUses', {
                        initialValue: item.mainUses,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="稳定性和反应活性" key="tab10">
                <Row>
                  <Col span={12}>
                    <FormItem label="稳定性:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('stability', {
                        initialValue: item.stability,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="禁配物:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('prohibition', {
                        initialValue: item.prohibition,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="避免接触:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('avoidContact', {
                        initialValue: item.avoidContact,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="急性毒素:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('acuteToxicity', {
                        initialValue: item.acuteToxicity,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="废弃处置" key="tab11">
                <Row>
                  <Col span={12}>
                    <FormItem label="废弃处理:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('discardedDisposalMethod', {
                        initialValue: item.discardedDisposalMethod,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="废弃注意事项:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('discardedNotices', {
                        initialValue: item.discardedNotices,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="运输信息" key="tab12">
                <Row>
                  <Col span={12}>
                    <FormItem {...formItemLayout} label="危险货物编号" hasFeedback>
                      {getFieldDecorator('dangerousNum', {
                        initialValue: item.dangerousNum,
                      })(<Input readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="包装类别:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('packingCategory', {
                        initialValue: item.packingCategory,
                      })(<Input readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="运输注意:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('attentionTransportation', {
                        initialValue: item.attentionTransportation,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="包装方法:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('packingMethod', {
                        initialValue: item.packingMethod,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row />
              </TabPane>
              <TabPane tab="法规信息" key="tab13">
                <Row>
                  <Col span={12}>
                    <FormItem label="法规信息:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('regulatoryInformation', {
                        initialValue: item.regulatoryInformation,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="其它信息" key="tab14">
                <Row>
                  <Col span={12}>
                    <FormItem label="其它信息:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('otherInformation', {
                        initialValue: item.otherInformation,
                      })(<TextArea autosize={{ minRows: 2 }} readOnly />)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
            </Tabs>
          </Form>
        </Modal>
      </span>
    );
  }
}

export default Form.create()(AddDangersModal);
