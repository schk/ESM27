import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Modal,
  Form,
  Input,
  Radio,
  TreeSelect,
  Select,
  Upload,
  Tabs,
  Button,
  Icon,
  message,
  Row,
  Col,
  InputNumber,
  Popconfirm,
} from 'antd';
import { baseUrl, baseFileUrl } from '../../config/system';
const TreeNode = TreeSelect.TreeNode;
const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const RadioGroup = Radio.Group;
const Option = Select.Option;
const { TextArea } = Input;

class AddDangersModal extends Component {
  constructor(props) {
    super(props);
  }

  okHandler = () => {
    const { onOk, catalogIds } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        var keyUnitDangersCatalogs = [];
        // if (catalogIds == null || catalogIds.length == 0) {
        //   message.error('请添加MSDS文档目录信息');
        // } else {
        //   for (var i = 0; i < catalogIds.length; i++) {
        //     var catalog = {};
        //     catalog.name = values['name_' + (i + 1)];
        //     catalog.page = values['page_' + (i + 1)];
        //     catalog.sort = i + 1;
        //     keyUnitDangersCatalogs.push(catalog);
        //   }
        // }
        if (catalogIds != null && catalogIds.length > 0) {
          for (var i = 0; i < catalogIds.length; i++) {
            let name = values['name_' + (i + 1)];
            for (var j = i + 1; j < catalogIds.length; j++) {
              if (name == values['name_' + (j + 1)]) {
                message.error('目录名称不能重复');
                return;
              }
            }
          }
          for (var i = 0; i < catalogIds.length; i++) {
            var catalog = {};
            catalog.name = values['name_' + (i + 1)];
            catalog.page = values['page_' + (i + 1)];
            catalog.sort = i + 1;
            keyUnitDangersCatalogs.push(catalog);
          }
        }
        values.keyUnitDangersCatalogs = keyUnitDangersCatalogs;
        console.log(values);
        onOk(values);
      }
    });
  };

  render() {
    const {
      children,
      dangers,
      dispatch,
      item,
      dangerTypeNoTopTree,
      typeId,
      msdsPath,
      imgPath,
      detailActiveKey,
      swfPath,
      catalogIds,
      catalogNames,
      uuid,
      fileName,
    } = this.props;
    const { getFieldDecorator, getFieldProps, getFieldValue, setFieldsValue } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 14 },
    };

    function onChangeTab(targetKey) {
      dispatch({
        type: 'keyUnit/updateState',
        payload: { detailActiveKey: targetKey },
      });
    }

    const treeData = i => {
      return i.map(d => {
        d.title = d.name;
        d.key = d.id_ + '';
        d.value = d.id_ + '';
        d.children = d.children && d.children.length > 0 ? treeData(d.children) : [];
        return d;
      });
    };

    //图片上传
    const pictureUploadProps = {
      action: baseUrl + '/common/uploadImg.jhtml',
      withCredentials: true,
      listType: 'picture',
      onChange: handleChange,
      onRemove: pictureHandleRemove,
    };

    function pictureHandleRemove() {
      dispatch({
        type: 'keyUnit/updateState',
        payload: {
          imgPath: '',
        },
      });
    }
    function handleChange(info) {
      let fileList = info.fileList;
      if (info.file.status === 'done') {
        if (info.file.response.httpCode == 200) {
          message.success(`${info.file.name} 上传成功`);
          dispatch({
            type: 'keyUnit/updateState',
            payload: {
              imgPath: info.file.response.data.fid,
            },
          });
        } else {
          message.error(info.file.response.msg);
        }
        fileList = fileList.filter(file => {
          return true;
        });
        //只保留最后一条记录
        fileList = fileList.slice(-1);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`);
      }
    }

    //文件上传
    const systemUploadProps = {
      action: baseUrl + '/common/uploadSwf.jhtml',
      withCredentials: true,
      listType: 'text',
      // accept: "application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.wordprocessingml.document,'application/msword,application/pdf",
      onChange: systemHandleChange,
      onRemove: systemHandleRemove,
    };

    //文件上传事件切换
    function systemHandleChange(info) {
      let fileList = info.fileList;
      if (info.file.status === 'done') {
        if (info.file.response.httpCode == 200) {
          message.success(`${info.file.name} 上传成功`);
          dispatch({
            type: 'keyUnit/updateState',
            payload: {
              msdsPath: info.file.response.data.fid,
              swfPath: info.file.response.data.swfUrl,
            },
          });
        } else {
          message.error(info.file.response.msg);
        }
        fileList = fileList.filter(file => {
          return true;
        });
        //只保留最后一条记录
        fileList = fileList.slice(-1);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`);
      }
      setFieldsValue({ fileName: fileList[0].name });
    }
    //删除制度规程文件
    function systemHandleRemove() {
      dispatch({
        type: 'keyUnit/updateState',
        payload: {
          msdsPath: '',
          swfPath: '',
        },
      });
    }
    function initCatalogs(values) {
      return values != null && values.length > 0
        ? values.map((k, index) => {
            return (
              <Row key={'page' + k}>
                <Col span={11}>
                  <FormItem {...formItemLayout} label="目录名称" hasFeedback>
                    {getFieldDecorator(`name_${k}`, {
                      initialValue: catalogNames['name_' + k],
                      rules: [
                        { required: true, whitespace: true, message: '请输入目录名称' },
                        { max: 50, message: '最长不超过50个字符' },
                      ],
                    })(<Input />)}
                  </FormItem>
                </Col>
                <Col span={11}>
                  <FormItem {...formItemLayout} label="起始页码" hasFeedback>
                    {getFieldDecorator(`page_${k}`, {
                      initialValue: catalogNames['page_' + k],
                      rules: [{ required: true, message: '请输入起始页码' }],
                    })(<InputNumber min={1} max={99999999} style={{ width: '80%' }} />)}
                  </FormItem>
                </Col>
                <Col span={2}>
                  <Popconfirm title="确定要删除吗？" onConfirm={onRemoveClg.bind(this, index)}>
                    <Icon
                      className="dynamic-delete-button"
                      type="minus-circle-o"
                      style={{ cursor: 'pointer' }}
                    />
                  </Popconfirm>
                </Col>
              </Row>
            );
          })
        : null;
    }
    function onAdd() {
      var caseMore = catalogIds;
      var newUuid = uuid;
      newUuid++;
      caseMore.push(newUuid);
      dispatch({
        type: 'keyUnit/updateState',
        payload: {
          catalogIds: catalogIds,
          uuid: newUuid,
        },
      });
    }
    function onRemoveClg(index) {
      var caseMore = catalogIds;
      caseMore.splice(index, 1);
      dispatch({
        type: 'keyUnit/updateState',
        payload: {
          catalogIds: catalogIds,
        },
      });
    }
    return (
      <span>
        <Modal
          maskClosable={this.props.maskClosable}
          confirmLoading={this.props.confirmLoading}
          title={this.props.title}
          visible={this.props.visible}
          width={800}
          onOk={this.okHandler}
          onCancel={this.props.onCancel}
          key={this.props.newKey}
        >
          <Form layout="horizontal" onSubmit={this.okHandler}>
            <FormItem style={{ marginBottom: '0px' }}>
              {getFieldDecorator('id', { initialValue: item.id })(<Input type="hidden" />)}
            </FormItem>
            <FormItem style={{ marginBottom: '0px' }}>
              {getFieldDecorator('swfPath', {
                initialValue: item.swfPath,
              })(<Input type="hidden" />)}
            </FormItem>
            <Row>
              <Col span={12}>
                <FormItem {...formItemLayout} label="中文名称" hasFeedback>
                  {getFieldDecorator('cName', {
                    initialValue: item.cName,
                    rules: [
                      { required: true, whitespace: true, message: '化学品中文名称' },
                      { max: 50, message: '最长不超过50个字符' },
                    ],
                  })(<Input placeholder="请输入中文名称" />)}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem {...formItemLayout} label="英文名" hasFeedback>
                  {getFieldDecorator('eName', {
                    initialValue: item.eName,
                    rules: [
                      { required: true, whitespace: true, message: '请输入化学品英文名' },
                      { max: 100, message: '最长不超过100个字符' },
                    ],
                  })(<Input placeholder="请输入化学品英文名" />)}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem {...formItemLayout} label="所属分类" hasFeedback>
                  {getFieldDecorator('typeId', {
                    initialValue: item.typeId == null ? typeId : item.typeId,
                    rules: [{ required: true, whitespace: true, message: '请选择所属分类' }],
                  })(
                    <TreeSelect
                      showSearch
                      treeNodeFilterProp="title"
                      dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                      placeholder="请选择所属分类"
                      notFoundContent="无匹配结果"
                      treeData={treeData(dangerTypeNoTopTree)}
                      allowClear
                      treeDefaultExpandAll
                    />
                  )}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem {...formItemLayout} label="危险货物编号" hasFeedback>
                  {getFieldDecorator('dangerousNum', {
                    initialValue: item.dangerousNum,
                    rules: [{ required: true, whitespace: true, message: '请输入危险货物编号' }],
                  })(<Input placeholder="请输入危险货物编号" />)}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem {...formItemLayout} label="UN号" hasFeedback>
                  {getFieldDecorator('unNum', {
                    initialValue: item.unNum,
                    rules: [
                      {
                        required: true,
                        whitespace: true,
                        message: '请输入UN号',
                      },
                    ],
                  })(<Input placeholder="请输入UN号" />)}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem {...formItemLayout} label="CAS号" hasFeedback>
                  {getFieldDecorator('casNum', {
                    initialValue: item.casNum,
                    rules: [
                      {
                        required: true,
                        whitespace: true,
                        message: '请输入CAS号',
                      },
                    ],
                  })(<Input placeholder="请输入CAS号" />)}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem {...formItemLayout} label="危化品图标">
                  <Input type="hidden" {...getFieldProps('imgPath', { initialValue: imgPath })} />
                  <Upload style={{ width: '300px' }} showUploadList={false} {...pictureUploadProps}>
                    <Button>
                      <Icon type="upload" />上传图标
                    </Button>
                  </Upload>
                  {imgPath === null ||
                  imgPath === '' ||
                  imgPath === 'undefined' ||
                  imgPath === undefined ? (
                    ''
                  ) : (
                    <div
                      style={{
                        borderRadius: '4px',
                        border: '1px solid #d9d9d9',
                        height: '66px',
                        position: 'relative',
                        clear: 'both',
                        overflow: 'hidden',
                      }}
                    >
                      <div
                        style={{
                          width: '60px',
                          height: '60px',
                          textAlign: 'center',
                          border: '1px solid #d9d9d9',
                          borderRadius: '4px',
                        }}
                      >
                        <img
                          style={{ width: '60px', height: '60px' }}
                          src={baseFileUrl + imgPath}
                          alt=""
                        />
                      </div>
                    </div>
                  )}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem {...formItemLayout} label="MSDS文档">
                  <Input type="hidden" {...getFieldProps('msdsPath', { initialValue: msdsPath })} />
                  <Upload style={{ width: '300px' }} showUploadList={false} {...systemUploadProps}>
                    <Button>
                      <Icon type="upload" />MSDS文档上传
                    </Button>
                  </Upload>
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem {...formItemLayout} label="文件名称" hasFeedback>
                  {getFieldDecorator('fileName', {
                    initialValue: item.fileName,
                    rules: [
                      {
                        required: true,
                        whitespace: true,
                        message: '请上传MSDS文档',
                      },
                    ],
                  })(<Input placeholder="请上传MSDS文档" />)}
                </FormItem>
              </Col>
            </Row>
            {initCatalogs(catalogIds)}
            <Row>
              <Col span={12}>&nbsp;</Col>
              <Col span={12} style={{ textAlign: 'right' }}>
                <Button icon="plus" type="primary" onClick={onAdd}>
                  新增
                </Button>
              </Col>
            </Row>
            <br />
            <Tabs type="card" activeKey={detailActiveKey} onChange={onChangeTab}>
              <TabPane tab="化学品名称" key="tab1">
                <Row>
                  <Col span={12}>
                    <FormItem label="中文名称2:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('cName2', {
                        initialValue: item.cName2,
                        rules: [{ max: 50, message: '最长不超过50个字' }],
                      })(<Input type="text" placeholder="请填写中文名称2" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="英文名称2:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('eName2', {
                        initialValue: item.eName2,
                        rules: [{ max: 100, message: '最长不超过100个字' }],
                      })(<Input type="text" placeholder="请填写英文名称2" />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="分子量:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('molecularWeight', {
                        initialValue: item.molecularWeight,
                        rules: [{ max: 20, message: '最长不超过20个字' }],
                      })(<Input type="text" placeholder="请填写分子量" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="分子式:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('molecularFormula', {
                        initialValue: item.molecularFormula,
                        rules: [{ max: 50, message: '最长不超过50个字' }],
                      })(<Input type="text" placeholder="请填写分子式" />)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="成分/组成信息" key="tab2">
                <Row>
                  <Col span={12}>
                    <FormItem label="有害物成分:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('harmfulIngredients', {
                        initialValue: item.harmfulIngredients,
                        rules: [{ max: 150, message: '最长不超过150个字' }],
                      })(<Input type="text" placeholder="请填写有害物成分" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="含量:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('content', {
                        initialValue: item.content,
                        rules: [{ max: 100, message: '最长不超过100个字' }],
                      })(<Input type="text" placeholder="请填写含量" />)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="危险性概述" key="tab3">
                <Row>
                  <Col span={12}>
                    <FormItem label="健康危害:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('healthHazards', {
                        initialValue: item.healthHazards,
                        rules: [{ max: 500, message: '最长不超过500个字' }],
                      })(<Input type="text" placeholder="请填写健康危害" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="环境危害" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('environmentalHazards', {
                        initialValue: item.environmentalHazards,
                        rules: [{ max: 255, message: '最长不超过255个字' }],
                      })(<Input type="text" placeholder="请填写环境危害" />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="爆炸危险:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('fireExplosionDanger', {
                        initialValue: item.fireExplosionDanger,
                        rules: [{ max: 100, message: '最长不超过100个字' }],
                      })(<Input type="text" placeholder="请填写爆炸危险" />)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="急救措施" key="tab4">
                <Row>
                  <Col span={12}>
                    <FormItem label="皮肤接触:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('skinContact', {
                        initialValue: item.skinContact,
                        rules: [{ max: 150, message: '最长不超过150个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="眼睛接触" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('eyeContact', {
                        initialValue: item.eyeContact,
                        rules: [{ max: 150, message: '最长不超过150个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="吸入:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('inhalation', {
                        initialValue: item.inhalation,
                        rules: [{ max: 100, message: '最长不超过100个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="食入" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('feedInto', {
                        initialValue: item.feedInto,
                        rules: [{ max: 100, message: '最长不超过100个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="消防措施" key="tab5">
                <Row>
                  <Col span={12}>
                    <FormItem label="危险性:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('hazardCharacteristics', {
                        initialValue: item.hazardCharacteristics,
                        rules: [{ max: 255, message: '最长不超过255个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="有害燃烧产物" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('harmfulCombustionProducts', {
                        initialValue: item.harmfulCombustionProducts,
                        rules: [{ max: 100, message: '最长不超过100个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="灭火方法:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('fireExtinguishingMethod', {
                        initialValue: item.fireExtinguishingMethod,
                        rules: [{ max: 150, message: '最长不超过150个字' }],
                      })(
                        <TextArea
                          type="text"
                          style={{ width: '267px', height: '100px' }}
                          placeholder=""
                        />
                      )}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="泄漏应急处理" key="tab6">
                <Row>
                  <Col span={12}>
                    <FormItem label="应急处理:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('emergencyManagement', {
                        initialValue: item.emergencyManagement,
                        rules: [{ max: 255, message: '最长不超过255个字' }],
                      })(
                        <TextArea
                          type="text"
                          style={{ width: '610px', height: '100px' }}
                          placeholder=""
                        />
                      )}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="操作处置与储存" key="tab7">
                <Row>
                  <Col span={12}>
                    <FormItem label="操作注意事项:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('attentionOperation', {
                        initialValue: item.attentionOperation,
                        rules: [{ max: 500, message: '最长不超过500个字' }],
                      })(
                        <TextArea
                          type="text"
                          style={{ width: '240px', height: '100px' }}
                          placeholder=""
                        />
                      )}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="储存注意事项:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('precautionsStorage', {
                        initialValue: item.precautionsStorage,
                        rules: [{ max: 255, message: '最长不超过255个字' }],
                      })(
                        <TextArea
                          type="text"
                          style={{ width: '240px', height: '100px' }}
                          placeholder=""
                        />
                      )}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="接触控制/个体防护" key="tab8">
                <Row>
                  <Col span={12}>
                    <FormItem label="中国MAC(mg/m3):" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('chinaMac', {
                        initialValue: item.chinaMac,
                        rules: [{ max: 50, message: '最长不超过50个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="前苏联MAC:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('sovietUnionMac', {
                        initialValue: item.sovietUnionMac,
                        rules: [{ max: 50, message: '最长不超过50个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="监测方法:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('monitoringMethod', {
                        initialValue: item.monitoringMethod,
                        rules: [{ max: 100, message: '最长不超过100个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="工程控制:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('engineeringControl', {
                        initialValue: item.engineeringControl,
                        rules: [{ max: 150, message: '最长不超过150个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="呼吸系统:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('respiratorySystemProtection', {
                        initialValue: item.respiratorySystemProtection,
                        rules: [{ max: 255, message: '最长不超过255个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="眼睛防护:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('eyeProtection', {
                        initialValue: item.eyeProtection,
                        rules: [{ max: 150, message: '最长不超过150个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="身体防护:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('bodyProtection', {
                        initialValue: item.bodyProtection,
                        rules: [{ max: 100, message: '最长不超过100个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="手防护:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('handProtection', {
                        initialValue: item.handProtection,
                        rules: [{ max: 100, message: '最长不超过100个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="其它防护:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('otherProtection', {
                        initialValue: item.otherProtection,
                        rules: [{ max: 200, message: '最长不超过200个字' }],
                      })(
                        <TextArea
                          type="text"
                          style={{ width: '610px', height: '100px' }}
                          placeholder=""
                        />
                      )}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="理化特性" key="tab9">
                <Row>
                  <Col span={12}>
                    <FormItem label="主要成分:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('mainComponents', {
                        initialValue: item.mainComponents,
                        rules: [{ max: 150, message: '最长不超过150个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="外观与性:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('appearanceCharacter', {
                        initialValue: item.appearanceCharacter,
                        rules: [{ max: 100, message: '最长不超过100个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="pH:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('ph', {
                        initialValue: item.ph,
                        rules: [{ max: 50, message: '最长不超过50个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="熔点(℃):" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('meltingPoint', {
                        initialValue: item.meltingPoint,
                        rules: [{ max: 50, message: '最长不超过50个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="沸点(℃):" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('expensePoint', {
                        initialValue: item.expensePoint,
                        rules: [{ max: 50, message: '最长不超过50个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="相对密度:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('relativeDensity', {
                        initialValue: item.relativeDensity,
                        rules: [{ max: 50, message: '最长不超过50个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="相对蒸汽:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('relativeSteam', {
                        initialValue: item.relativeSteam,
                        rules: [{ max: 20, message: '最长不超过20个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="饱和蒸气压(kPa):" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('saturatedVaporPressure', {
                        initialValue: item.saturatedVaporPressure,
                        rules: [{ max: 20, message: '最长不超过20个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="燃烧热(kJ/mol):" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('burningHeat', {
                        initialValue: item.burningHeat,
                        rules: [{ max: 30, message: '最长不超过30个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="临界温度:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('criticalTemperature', {
                        initialValue: item.criticalTemperature,
                        rules: [{ max: 50, message: '最长不超过50个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="临界压力:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('criticalPressure', {
                        initialValue: item.criticalPressure,
                        rules: [{ max: 50, message: '最长不超过50个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="辛醇/水分配数值:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('waterDistributionCoefficient', {
                        initialValue: item.waterDistributionCoefficient,
                        rules: [{ max: 50, message: '最长不超过50个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="爆炸上限:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('upperLimitExplosion', {
                        initialValue: item.upperLimitExplosion,
                        rules: [{ max: 20, message: '最长不超过20个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="爆炸下限:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('lowerLimitExplosion', {
                        initialValue: item.lowerLimitExplosion,
                        rules: [{ max: 20, message: '最长不超过20个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="溶解性:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('solubility', {
                        initialValue: item.solubility,
                        rules: [{ max: 100, message: '最长不超过100个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="主要用途:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('mainUses', {
                        initialValue: item.mainUses,
                        rules: [{ max: 255, message: '最长不超过255个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="稳定性和反应活性" key="tab10">
                <Row>
                  <Col span={12}>
                    <FormItem label="稳定性:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('stability', {
                        initialValue: item.stability,
                        rules: [{ max: 255, message: '最长不超过255个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="禁配物:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('prohibition', {
                        initialValue: item.prohibition,
                        rules: [{ max: 255, message: '最长不超过255个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="避免接触:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('avoidContact', {
                        initialValue: item.avoidContact,
                        rules: [{ max: 100, message: '最长不超过100个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="急性毒素:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('acuteToxicity', {
                        initialValue: item.acuteToxicity,
                        rules: [{ max: 100, message: '最长不超过100个字' }],
                      })(<Input type="text" placeholder="" />)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="废弃处置" key="tab11">
                <Row>
                  <Col span={12}>
                    <FormItem label="废弃处理:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('discardedDisposalMethod', {
                        initialValue: item.discardedDisposalMethod,
                        rules: [{ max: 100, message: '最长不超过100个字' }],
                      })(
                        <TextArea
                          type="text"
                          style={{ width: '240px', height: '100px' }}
                          placeholder=""
                        />
                      )}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="废弃注意事项:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('discardedNotices', {
                        initialValue: item.discardedNotices,
                        rules: [{ max: 255, message: '最长不超过255个字' }],
                      })(
                        <TextArea
                          type="text"
                          style={{ width: '240px', height: '100px' }}
                          placeholder=""
                        />
                      )}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="运输信息" key="tab12">
                <Row>
                  <Col span={12}>
                    <FormItem label="包装类别:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('packingCategory', {
                        initialValue: item.packingCategory,
                        rules: [{ max: 50, message: '最长不超过50个字' }],
                      })(
                        <TextArea
                          type="text"
                          style={{ width: '232px', height: '100px' }}
                          placeholder=""
                        />
                      )}
                    </FormItem>
                  </Col>
                  <Col span={12}>
                    <FormItem label="运输注意:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('attentionTransportation', {
                        initialValue: item.attentionTransportation,
                        rules: [{ max: 500, message: '最长不超过500个字' }],
                      })(
                        <TextArea
                          type="text"
                          style={{ width: '224px', height: '100px' }}
                          placeholder=""
                        />
                      )}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <FormItem label="包装方法:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('packingMethod', {
                        initialValue: item.packingMethod,
                        rules: [{ max: 255, message: '最长不超过255个字' }],
                      })(
                        <TextArea
                          type="text"
                          style={{ width: '610px', height: '100px' }}
                          placeholder=""
                        />
                      )}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="法规信息" key="tab13">
                <Row>
                  <Col span={12}>
                    <FormItem label="法规信息:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('regulatoryInformation', {
                        initialValue: item.regulatoryInformation,
                        rules: [{ max: 500, message: '最长不超过500个字' }],
                      })(
                        <TextArea
                          type="text"
                          placeholder=""
                          style={{ width: '610px', height: '100px' }}
                        />
                      )}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="其它信息" key="tab14">
                <Row>
                  <Col span={12}>
                    <FormItem label="其它信息:" hasFeedback {...formItemLayout}>
                      {getFieldDecorator('otherInformation', {
                        initialValue: item.otherInformation,
                        rules: [{ max: 255, message: '最长不超过255个字' }],
                      })(
                        <TextArea
                          type="text"
                          style={{ width: '610px', height: '100px' }}
                          placeholder=""
                        />
                      )}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
            </Tabs>
          </Form>
        </Modal>
      </span>
    );
  }
}

export default Form.create()(AddDangersModal);
