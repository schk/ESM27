import React from 'react';
import {
  Form,
  Input,
  Modal,
  Popover,
  Button,
  Radio,
  Row,
  Col,
  InputNumber,
  Icon,
  Select,
  Upload,
  message,
  Popconfirm,
} from 'antd';
import { connect } from 'dva';
import { baseUrl, baseFileUrl } from '../../config/system';
const { TextArea } = Input;
const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

let ShowAccidentFileModal = ({ keyUnit, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
    getFieldProps,
  } = form;

  const modalOpts = {
    title: '查看重点单位的事故信息',
    visible: keyUnit.showAccidentVisible,
    maskClosable: false,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={keyUnit.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!keyUnit.showAccidentVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'keyUnit/updateState',
      payload: {
        showAccidentVisible: false,
        accidentFileList: [],
        accidentItem: {},
      },
    });
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      const data = getFieldsValue();
      var fileList = [];
      if (keyUnit.accidentFileList != null && keyUnit.accidentFileList.length > 0) {
        for (var i = 0; i < keyUnit.accidentFileList.length; i++) {
          var fileId = keyUnit.accidentFileList[i].uid;
          var doc = {};
          var names = keyUnit.accidentFileList[i].name.split('.');
          doc.name = data['name_' + fileId] + '.' + names[1];
          if (keyUnit.accidentFileList[i].response != null) {
            doc.url = keyUnit.accidentFileList[i].response.data.url;
            doc.swfPath = keyUnit.accidentFileList[i].response.data.swfUrl;
          } else {
            doc.url = keyUnit.accidentFileList[i].url;
            doc.swfPath = keyUnit.accidentFileList[i].swfPath;
          }
          fileList.push(doc);
        }
      }
      data.accidentFileList = fileList;
      //文档数据
      //文档目录数据
      data.id = item.id;
      dispatch({
        type: `keyUnit/${keyUnit.modalType}`,
        payload: data,
        search: keyUnit.selectObj,
      });
    });
  }

  const uploadProps = {
    action: baseUrl + '/common/uploadFileSwf.jhtml',
    withCredentials: true,
    multiple: true,
    listType: 'text',
    onChange: handleChange,
    onRemove: file => {
      const index = keyUnit.accidentFileList.indexOf(file);
      const newFileList = keyUnit.accidentFileList.slice();
      newFileList.splice(index, 1);
      dispatch({
        type: 'keyUnit/updateState',
        payload: {
          fileList: newFileList,
        },
      });
    },
  };
  function handleChange(info) {
    let fileList = info.fileList;

    if (info.file.status === 'done') {
      if (info.file.response.httpCode == 200) {
        message.success(`${info.file.name} 上传成功`);
      } else {
        message.error(info.file.response.msg);
      }
      fileList = fileList.filter(file => {
        return true;
      });
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }
    dispatch({
      type: 'keyUnit/updateState',
      payload: {
        accidentFileList: fileList,
      },
    });
  }

  function fileName(fileName, index) {
    let names = fileName.split('.');
    return names[index];
  }
  function onRemoveFile(fileId, oldFileId) {
    var formValue = getFieldsValue();
    //更新fileList
    var fileList = keyUnit.accidentFileList;
    if (fileList != null && fileList.length > 0) {
      for (var i = 0; i < fileList.length; i++) {
        if (fileList[i].uid == fileId) {
          fileList.splice(i, 1);
          break;
        }
      }
      //更新fileCatalogs
      dispatch({
        type: 'keyUnit/updateState',
        payload: {
          accidentFileList: fileList,
        },
      });
    }
  }

  function handleDownload(url, index) {
    let file = keyUnit.accidentFileList[index];
    var values = { filePath: file.url, fileName: file.name };
    var param = JSON.stringify(values);
    param = encodeURIComponent(param);
    window.open(baseFileUrl + '/common/downLoadFile.jhtml?param=' + param);
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={24}>
            <FormItem label="重点单位名称:" hasFeedback {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue: item.name,
              })(<Input type="text" readOnly />)}
            </FormItem>
          </Col>
        </Row>

        <Row>
          <Col span={24}>
            <FormItem {...formItemLayout} label="上传文件">
              {getFieldDecorator('fileList', {
                initialValue:
                  keyUnit.accidentFileList.length > 0 ? keyUnit.accidentFileList : undefined,
                rules: [{ required: true, message: '请上传事故案例文件' }],
              })(
                <Upload {...uploadProps} showUploadList={false} fileList={keyUnit.accidentFileList}>
                  <Button>
                    <Icon type="upload" /> 上传事故案例文件
                  </Button>
                </Upload>
              )}
            </FormItem>
          </Col>
        </Row>

        {keyUnit.accidentFileList != null && keyUnit.accidentFileList.length > 0 ? (
          <Row>
            {/* <Col span={2}>&nbsp;&nbsp;</Col> */}
            <Col span={11} style={{ textAlign: 'center' }}>
              <span>文件名称</span>
            </Col>
            <Col span={6} style={{ textAlign: 'center' }}>
              <span>文件类型</span>
            </Col>
            <Col span={5} style={{ textAlign: 'center' }}>
              <span>操作</span>
            </Col>
          </Row>
        ) : (
          ''
        )}
        {keyUnit.accidentFileList != null && keyUnit.accidentFileList.length > 0
          ? keyUnit.accidentFileList.map((k, index) => {
              return (
                <Row key={'page' + index}>
                  {/* <Col span={2}>&nbsp;&nbsp;</Col>          */}
                  <Col span={11} style={{ textAlign: 'center' }}>
                    <FormItem>
                      {getFieldDecorator(`name_${k.uid}`, {
                        initialValue: fileName(k.name, 0),
                        rules: [
                          { required: true, whitespace: true, message: '请输入文件名称' },
                          { max: 50, message: '最长不超过50个字符' },
                        ],
                      })(<Input />)}
                    </FormItem>
                  </Col>
                  <Col span={6} style={{ textAlign: 'center' }}>
                    <FormItem>
                      <span>{fileName(k.name, 1)}</span>
                    </FormItem>
                  </Col>
                  <Col span={5} style={{ textAlign: 'center', lineHeight: '42px' }}>
                    <Popconfirm
                      title="确定要删除吗？"
                      onConfirm={() => onRemoveFile(k.uid, keyUnit.curFileId)}
                    >
                      <a>删除</a>
                    </Popconfirm>
                    &nbsp;
                    <a href="javascript:void(0)" onClick={() => handleDownload(k.url, index)}>
                      下载
                    </a>
                  </Col>
                </Row>
              );
            })
          : ''}
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { keyUnit: state.keyUnit };
}

ShowAccidentFileModal = Form.create()(ShowAccidentFileModal);

export default connect(mapStateToProps)(ShowAccidentFileModal);
