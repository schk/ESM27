import React, { PureComponent, Fragment } from 'react';
import { Form, Modal, Button } from 'antd';
import { connect } from 'dva';
const FormItem = Form.Item;

var PreviewModal = ({ keyUnit, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: '平面图展示',
    visible: keyUnit.previewVisible,
    maskClosable: false,
    width: 300,
    top: 300,
    onCancel: handleCansel,
    footer: null,
  };

  if (!keyUnit.previewVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'keyUnit/updateState',
      payload: {
        previewVisible: false,
        previewImage: '',
      },
    });
  }

  return (
    <Modal {...modalOpts}>
      <img alt="example" style={{ width: '100%' }} src={keyUnit.previewImage} />
    </Modal>
  );
};

function mapStateToProps(state) {
  return { keyUnit: state.keyUnit };
}

PreviewModal = Form.create()(PreviewModal);

export default connect(mapStateToProps)(PreviewModal);
