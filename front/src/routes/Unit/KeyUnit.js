import React, { Fragment } from 'react';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Button,
  TreeSelect,
  message,
  Divider,
  Table,
  Popconfirm,
  Upload,
  Modal,
  Menu,
  Dropdown,
  Icon,
} from 'antd';
import { connect } from 'dva';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from '../../common/common.less';
import AddEditModal from './AddEditModal';
import LocationModal from './LocationModal';
import OldAddDangersModal from './OldAddDangersModal';
import FindDangersModal from './FindDangersModal';
import ShowFileModal from './ShowFileModal';
import ShowAccidentFileModal from './ShowAccidentFileModal';
import PreviewModal from './PreviewModal';
import DocumentModal from './DocumentModal';
import PlanEditModal from './PlanEditModal';
import ShowPlans from './ShowPlans';
import ShowDangersModal from './ShowDangersModal';
import AddDangersModal from './AddDangersModal';
import { baseUrl } from '../../config/system';
import ChoosePlanModal from './ChoosePlanModal';
const FormItem = Form.Item;

function KeyUnit({ keyUnit, form, dispatch, loading }) {
  const {
    getFieldDecorator,
    getFieldsValue,
    setFieldsValue,
    resetFields,
    getFieldProps,
    getFieldValue,
  } = form;

  const AddEditModalProps = {
    item: keyUnit.item,
  };
  const ShowEditModalProps = {
    item: keyUnit.findItem,
  };
  const ShowAccidentFileModalProps = {
    item: keyUnit.accidentItem,
  };

  const ShowPlansModalProps = {};

  const ShowPlansItemMadalProps = {
    item: keyUnit.planItem,
  };
  const FindDangersModalProps = {
    item: keyUnit.dangersItem,
    dispatch: dispatch,
    newKey: keyUnit.newKey,
    msdsPath: keyUnit.msdsPath,
    imgPath: keyUnit.imgPath,
    swfPath: keyUnit.swfPath,
    detailActiveKey: keyUnit.detailActiveKey,
    visible: keyUnit.FindModalDangersVisible,
    dangerTypeNoTopTree: keyUnit.dangerTypeNoTopTree,
    title: '查看详情',
    catalogIds: keyUnit.catalogIds,
    catalogNames: keyUnit.catalogNames,
    uuid: keyUnit.uuid,
    onCancel() {
      dispatch({
        type: 'keyUnit/updateState',
        payload: {
          FindModalDangersVisible: false,
          dangersItem: {},
          msdsPath: '',
          swfPath: '',
          imgPath: '',
          detailActiveKey: 'tab1',
          newKey: new Date().getTime() + '',
          catalogIds: [],
          catalogNames: {},
          uuid: 0,
        },
      });
    },
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
    ],
  };

  const OldAddDangersModalProps = {
    item: keyUnit.dangersItem,
    dispatch: dispatch,
    newKey: keyUnit.newKey,
    msdsPath: keyUnit.msdsPath,
    imgPath: keyUnit.imgPath,
    swfPath: keyUnit.swfPath,
    fileName: keyUnit.fileName,
    detailActiveKey: keyUnit.detailActiveKey,
    visible: keyUnit.modalDangersVisible,
    dangerTypeNoTopTree: keyUnit.dangerTypeNoTopTree,
    title: '编辑危化品信息',
    catalogIds: keyUnit.catalogIds,
    catalogNames: keyUnit.catalogNames,
    uuid: keyUnit.uuid,
    onCancel() {
      dispatch({
        type: 'keyUnit/updateState',
        payload: {
          modalDangersVisible: false,
          dangersItem: {},
          msdsPath: '',
          swfPath: '',
          imgPath: '',
          detailActiveKey: 'tab1',
          newKey: new Date().getTime() + '',
          catalogIds: [],
          catalogNames: {},
          uuid: 0,
        },
      });
    },
    onOk(data) {
      if (keyUnit.detailModalType == 'createDetail') {
        data.id = new Date().getTime() + '';
        keyUnit.dangersList.push(data);
      } else {
        for (var j = 0; j < keyUnit.dangersList.length; j++) {
          if (keyUnit.dangersList[j].id == data.id) {
            keyUnit.dangersList[j] = data;
            break;
          }
        }
      }
      dispatch({
        type: 'keyUnit/updateState',
        payload: {
          modalDangersVisible: false,
          dangersItem: {},
          dangersList: keyUnit.dangersList,
          detailActiveKey: 'tab1',
          newKey: new Date().getTime() + '',
        },
      });
    },
  };

  Array.prototype.removeServer = function(dx) {
    if (isNaN(dx) || dx > this.length) {
      return false;
    }
    for (var i = 0, n = 0; i < this.length; i++) {
      if (this[i] != this[dx]) {
        this[n++] = this[i];
      }
    }
    this.length -= 1;
  };
  function handleCansel() {
    dispatch({
      type: 'keyUnit/updateState',
      payload: {
        modalDangersVisible: false,
        activityKey: 'tab1',
      },
    });
  }
  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      width: 40,
      render: (value, row, index) => {
        return index + 1;
      },
    },
    {
      title: '重点单位名称',
      dataIndex: 'name',
      key: 'name',
      width: 120,
      render: (text, record, value) => (
        <Fragment>
          <a onClick={() => onFindKeyUnit(record.id_)}>{record.name}</a>
        </Fragment>
      ),
    },
    { title: '所属消防战队', dataIndex: 'fireBrigadeName', key: 'fireBrigadeName', width: 150 },
    { title: '消防管理人员', dataIndex: 'admin', key: 'admin', width: 100 },
    { title: '值班室电话', dataIndex: 'tel', key: 'tel', width: 100 },
    { title: '地理位置', dataIndex: 'addr', key: 'addr', width: 150 },
    { title: '供水能力', dataIndex: 'waterSupply', key: 'waterSupply', width: 100 },
    {
      title: '经纬度',
      dataIndex: 'lon',
      width: 130,
      render: (text, record) => {
        if(record.lon && record.lat){
          return record.lon + ',' + record.lat;
        }
        return '';
      },
    },
    {
      title: '操作',
      dataIndex: 'operation',
      key: 'operation',
      width: 150,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onUpdate(record.id_)}>编辑</a>
          <Divider type="vertical" />
          <Popconfirm title="是否要删除此行？" onConfirm={deleteHandler.bind(this, record.id)}>
            <a>删除</a>
          </Popconfirm>
          <Divider type="vertical" />
          <Dropdown
            overlay={
              <Menu>
                <Menu.Item>
                  <a onClick={() => onUploadAccident(record.id_)}>上传事故信息</a>
                </Menu.Item>
                <Menu.Item>
                  <a onClick={() => onShowPlans(record.id_)}>增加预案</a>
                </Menu.Item>
                <Menu.Item>
                  <a onClick={() => onShowDangers(record.id_)}>添加危化品</a>
                </Menu.Item>
              </Menu>
            }
          >
            <a className="ant-dropdown-link">
              操作<Icon type="down" />
            </a>
          </Dropdown>
        </Fragment>
      ),
    },
  ];
  const onFindKeyUnit = id => {
    dispatch({
      type: 'keyUnit/findKeyUnit',
      payload: id,
    });
  };

  function onShowDangers(id) {
    dispatch({
      type: 'keyUnit/onShowDangers',
      payload: id,
    });
  }

  function onShowPlans(id) {
    dispatch({
      type: 'keyUnit/getPlans',
      payload: id,
    });
  }

  const onUploadAccident = id => {
    dispatch({
      type: 'keyUnit/getUnitAccident',
      payload: id,
    });
  };

  const onUpdate = id => {
    dispatch({
      type: 'keyUnit/info',
      payload: id,
    });
  };

  const deleteHandler = id => {
    dispatch({
      type: 'keyUnit/remove',
      payload: id,
      search: { pageNum: keyUnit.current, pageSize: keyUnit.pageSize, ...getFieldsValue() },
    });
  };

  const pagination = {
    current: keyUnit.current,
    pageSize: keyUnit.pageSize,
    total: keyUnit.total,
    showSizeChanger: true,
    showTotal: total => '共' + keyUnit.total + '条',
    onShowSizeChange(current, size) {
      dispatch({
        type: 'keyUnit/qryListByParams',
        payload: { pageNum: current, pageSize: size, ...getFieldsValue() },
      });
    },
    onChange(current) {
      dispatch({
        type: 'keyUnit/qryListByParams',
        payload: { pageNum: current, pageSize: keyUnit.pageSize, ...getFieldsValue() },
      });
    },
  };

  const handleSearch = () => {
    dispatch({
      type: 'keyUnit/qryListByParams',
      payload: { pageNum: 1, pageSize: keyUnit.pageSize, ...getFieldsValue() },
    });
  };

  const handleFormReset = () => {
    resetFields();
    dispatch({
      type: 'keyUnit/qryListByParams',
      payload: { pageNum: 1, pageSize: keyUnit.pageSize },
    });
  };

  const onAdd = () => {
    dispatch({
      type: 'keyUnit/showCreateModal',
      payload: {
        modalVisible: true,
        modalType: 'create',
        item: {},
        newKey: new Date().getTime() + '',
      },
    });
  };

  const loop = data =>
    data.map(d => {
      let children = null;
      if (d.children != null && d.children.length > 0) {
        children = loop(d.children);
      }
      return { title: d.name, value: d.id_, key: d.id_, children };
    });

  function handleTableChange(pagination, filters, sorter) {
    let isChinese = ['roleName'].indexOf(sorter.field) >= 0;
    dispatch({
      type: 'keyUnit/qryListByParams',
      payload: {
        pageNum: pagination.current,
        pageSize: pagination.pageSize,
        ...getFieldsValue(),
        orderBy: sorter.field,
        order: sorter.order,
        isChinese,
      },
    });
  }

  //弹出地图选择
  const LocationModalGen = () => <LocationModal />;
  const DocumentModallGen = () => <DocumentModal {...DocumentModalProps} />;
  const DocumentModalProps = {
    viweSwfPath: keyUnit.viweSwfPath,
    visible: keyUnit.documnetModalVisible,
    title: '查看文档',
    dispatch: dispatch,
  };

  function downloadTemplate() {
    window.open(baseUrl + '/execl/downLoadTemplate.jhtml?type=keyUnit');
  }

  function initLoopMsg(values) {
    return values != null && values.length > 0
      ? values.map((k, index) => {
          return <div key={index}>{k}</div>;
        })
      : null;
  }

  const props = {
    name: 'file',
    action: baseUrl + '/keyUnit/excel/importExpert.jhtml',
    beforeUpload(file) {
      // const isJPG = file.type === 'application/vnd.ms-excel' || file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
      // if (!isJPG) {
      //   message.error('只能上传EXCEL文件！');
      // }
      // return isJPG;
    },
    onChange(info) {
      let fileList = info.fileList;
      if (info.file.status === 'done') {
        if (info.file.response.httpCode == 200) {
          if (info.file.response.errorCode == 1) {
            //弹出确认取消框
          } else if (info.file.response.errorCode == 2) {
            if (info.file.response.data && info.file.response.data.length > 0) {
              Modal.error({
                title: '导入提示',
                content: initLoopMsg(info.file.response.data),
              });
            }
          } else {
            message.success(`${info.file.name} 上传成功`);
            dispatch({
              type: 'keyUnit/qryListByParams',
              payload: { name: '' },
            });
          }
        } else {
          Modal.error({
            title: '导入提示',
            content: info.file.response.msg,
          });
        }
        fileList = fileList.filter(file => {
          return file.response && file.response.fid;
        });
        //只保留最后一条记录
        fileList = fileList.slice(-1);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`);
      }
      let fildsValue = {};
      fildsValue['file'] = fileList.map(file => {
        return file;
      });
      setFieldsValue(fildsValue);
    },
  };

  const AddDangersModalProps = {
    visible: keyUnit.addDangersVisible,
    existDangersList: keyUnit.existDangersList,
    allDangersList: keyUnit.allDangersList,
    currentDangers: keyUnit.currentDangers,
    pageSizeDangers: keyUnit.pageSizeDangers,
    totalDangers: keyUnit.totalDangers,
    maskClosable: false,
    dispatch: dispatch,
    title: '新增明细',
    wrapClassName: 'vertical-center-modal',
    onCancel() {
      dispatch({
        type: 'keyUnit/hideSecondModal',
      });
    },
  };

  return (
    <PageHeaderLayout>
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={5} sm={5}>
                  <FormItem label="名称">
                    {getFieldDecorator('name')(<Input placeholder="请输入名称" />)}
                  </FormItem>
                </Col>
                <Col md={6} sm={6}>
                  <FormItem label="所属消防队">
                    {getFieldDecorator('fireBrigadeId')(
                      <TreeSelect
                        showSearch
                        treeNodeFilterProp="title"
                        dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                        placeholder="请选择所属消防队"
                        notFoundContent="无匹配结果"
                        allowClear
                        treeData={loop(keyUnit.fireBrigadeTree)}
                        treeDefaultExpandAll
                      />
                    )}
                  </FormItem>
                </Col>
                <Col md={5} sm={4}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={handleFormReset}>
                      重置
                    </Button>

                    <Button
                      style={{ marginLeft: 80 }}
                      type="primary"
                      icon="download"
                      onClick={downloadTemplate}
                    >
                      下载模板
                    </Button>
                    <Upload
                      {...getFieldProps(
                        'file',
                        {
                          validate: [
                            {
                              rules: [{ type: 'array', required: true, message: '请添加数据文件' }],
                              trigger: 'onBlur',
                            },
                          ],
                        },
                        { valuePropName: 'fileIds' }
                      )}
                      {...props}
                      fileList={getFieldValue('file')}
                    >
                      <Button style={{ marginLeft: 8 }} type="primary" icon="upload">
                        导入
                      </Button>
                    </Upload>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={onAdd}>
              新建
            </Button>
          </div>
          <Table
            loading={loading}
            rowKey={record => record.id}
            dataSource={keyUnit.list}
            columns={columns}
            pagination={pagination}
            onChange={handleTableChange}
          />
        </div>
      </Card>
      <AddEditModal {...AddEditModalProps} />
      <LocationModalGen />
      <OldAddDangersModal {...OldAddDangersModalProps} />
      <FindDangersModal {...FindDangersModalProps} />
      <ShowFileModal {...ShowEditModalProps} />
      <ShowAccidentFileModal {...ShowAccidentFileModalProps} />
      <PreviewModal />
      <DocumentModallGen />
      <ShowPlans {...ShowPlansModalProps} />
      <ChoosePlanModal {...ShowPlansItemMadalProps} />
      <ShowDangersModal />
      <AddDangersModal {...AddDangersModalProps} />
    </PageHeaderLayout>
  );
}

function mapStateToProps(state) {
  return {
    keyUnit: state.keyUnit,
    loading: state.loading.models.keyUnit,
  };
}

KeyUnit = Form.create()(KeyUnit);

export default connect(mapStateToProps)(KeyUnit);
