import React, { Fragment } from 'react';
import {
  Form,
  Input,
  Modal,
  Button,
  Row,
  Col,
  Table,
  Tag,
  Popconfirm,
  Divider,
  message,
} from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import { baseUrl, baseFileUrl } from '../../config/system';
const FormItem = Form.Item;
const { TextArea } = Input;
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

let ShowDangersModal = ({ keyUnit, loading, item = {}, form, dispatch }) => {
  const { getFieldDecorator, resetFields, validateFields } = form;

  const modalOpts = {
    title: '查看重点部门危化品',
    visible: keyUnit.dangersVisible,
    maskClosable: false,
    width: 1000,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={keyUnit.buttomLoading}
      >
        保存
      </Button>,
    ],
  };
  if (!keyUnit.dangersVisible) {
    resetFields();
  }

  function handleOk() {
    if (keyUnit.existDangersList.length == 0) {
      message.error('您还没有选择危化品');
      return;
    }
    const data = {};
    data.id = keyUnit.keyUnitId;
    data.dList = keyUnit.existDangersList;
    dispatch({
      type: `keyUnit/saveDangersOfKeyUnit`,
      payload: data,
      search: keyUnit.selectObj,
    });
  }

  function handleCansel() {
    dispatch({
      type: 'keyUnit/updateState',
      payload: {
        dangersVisible: false,
        existDangersList: [],
      },
    });
  }
  const columns = [
    {
      title: '危化品名称',
      dataIndex: 'cName',
      key: 'cName',
      width: 180,
      render: (text, record, value) => (
        <Fragment>
          <a onClick={() => onFindInfo(record.id_)}>{record.cName}</a>
        </Fragment>
      ),
    },
    { title: '危化品类型', dataIndex: 'typeName', key: 'typeName', width: 100 },
    {
      title: 'CAS号',
      dataIndex: 'casNum',
      key: 'casNum',
      width: 80,
    },
    {
      title: 'UN号',
      dataIndex: 'unNum',
      key: 'unNum',
      width: 80,
    },
    { title: '分子式', dataIndex: 'molecularFormula', key: 'molecularFormula', width: 100 },
    { title: '分子量', dataIndex: 'molecularWeight', key: 'molecularWeight', width: 100 },
    {
      title: '操作',
      dataIndex: 'edit',
      key: 'edit',
      width: 60,
      render: (text, record) => (
        <Fragment>
          <Popconfirm title="确定要删除吗？" onConfirm={onDelete.bind(this, record.id_)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  function onDelete(id) {
    for (var k = 0; k < keyUnit.existDangersList.length; k++) {
      var curr = keyUnit.existDangersList[k];
      if (id == curr.id) {
        keyUnit.existDangersList.removeServer(k);
      }
    }
    dispatch({
      type: 'keyUnit/updateState',
      payload: { existDangersList: keyUnit.existDangersList },
    });
  }

  function onAddDangers() {
    dispatch({
      type: 'keyUnit/updateState',
      payload: {
        addDangersVisible: true,
      },
    });
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="inline">
        <Button style={{ bottom: '10px' }} icon="plus" type="primary" onClick={onAddDangers}>
          新增危化品
        </Button>
      </Form>
      <Table
        columns={columns}
        dataSource={keyUnit.existDangersList}
        rowKey={record => record.id}
        loading={loading}
        pagination={false}
      />
    </Modal>
  );
};

function mapStateToProps(state) {
  return { keyUnit: state.keyUnit };
}

ShowDangersModal = Form.create()(ShowDangersModal);

export default connect(mapStateToProps)(ShowDangersModal);
