import React, { Fragment } from 'react';
import {
  Form,
  Input,
  Modal,
  Button,
  Row,
  Col,
  Upload,
  Table,
  Icon,
  TreeSelect,
  InputNumber,
  Tabs,
  Divider,
  Popconfirm,
  message,
} from 'antd';
import { baseUrl, baseFileUrl } from '../../config/system';
import { connect } from 'dva';
const FormItem = Form.Item;
const { TextArea } = Input;
const TabPane = Tabs.TabPane;

const formItemLayout = {
  labelCol: { span: 7 },
  wrapperCol: { span: 16 },
};

const formItemLayout1 = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};
var AddEditModal = ({ keyUnit, item = {}, form, dispatch }) => {
  const { getFieldDecorator, validateFields, getFieldsValue, resetFields, getFieldProps } = form;

  const modalOpts = {
    title:
      keyUnit.modalType == 'create'
        ? '新建重点单位'
        : keyUnit.modalType == 'update'
          ? '编辑重点单位'
          : keyUnit.modalType == 'find' ? '查看' : '',
    visible: keyUnit.modalVisible,
    maskClosable: false,
    width: 800,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={keyUnit.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!keyUnit.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'keyUnit/updateState',
      payload: {
        modalVisible: false,
        activityKey: 'tab1',
        dangersList: [],
        fileList: [],
        lonLat: '',
        catalogIds: [],
        catalogNames: {},
        uuid: 0,
      },
    });
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }      
      var lonLat = getFieldsValue(["lonLat"]);
      if(lonLat.lonLat==undefined){
        message.error("坐标不能为空");
        return;
      }
      let list = keyUnit.fileList;
      let fileIds = [];
      for (var i = 0; i < list.length; i++) {
        if (list[i].response) {
          fileIds.push(list[i].response.data.fid);
        } else {
          fileIds.push(list[i].fid);
        }
      }

      const data = {
        ...getFieldsValue(),
        imgList: fileIds,
        id: keyUnit.modalType == 'create' ? '0' : item.id_,
      };

      dispatch({
        type: `keyUnit/${keyUnit.modalType}`,
        payload: data,
        search: keyUnit.selectObj,
      });
    });
  }
  //获取图标
  const changeRadio = e => {
    dispatch({
      type: 'fireEngine/updateState',
      payload: { type: e.target.value },
    });
  };
  //展示会标图片模态框
  function onShowPlotting() {
    dispatch({
      type: 'fireEngine/qryPlottingList',
      payload: { equipType: '', equipmentPageNum: 1, equipmentPageSize: 10 },
    });
  }
  //图片上传
  const pictureUploadProps = {
    action: baseUrl + '/common/uploadImg.jhtml',
    withCredentials: true,
    listType: 'picture-card',
    onChange: pictureHandleChange,
    onPreview: handlePreview,
  };

  //图片上传
  function pictureHandleChange(info) {
    debugger;
    let fileList = info.fileList;
    if (fileList) {
      fileList = fileList.filter(file => {
        if(file.type&&!/image\/\w+/.test(file.type)) {
          message.warning("请上传图片文件！")
          return false;
        }
        return true;
      });
      dispatch({
        type: 'keyUnit/updateState',
        payload: {
          fileList: fileList,
        },
      });
    }
  }

  function handlePreview(info) {
    dispatch({
      type: 'keyUnit/updateState',
      payload: {
        previewVisible: true,
        previewImage: info.url || info.thumbUrl,
      },
    });
  }

  function onChangeTab(targetKey) {
    dispatch({
      type: 'keyUnit/updateState',
      payload: { activityKey: targetKey },
    });
  }

  //展示随车器材模态框
  function onShow() {
    dispatch({
      type: 'keyUnit/updateState',
      payload: {
        modalDangersVisible: true,
        detailModalType: 'createDetail',
        newKey: new Date().getTime() + '',
        catalogIds: [],
        catalogNames: {},
        uuid: 0,
      },
    });
  }

  const onGetLocation = () => {
    const data = getFieldsValue();
    dispatch({
      type: 'keyUnit/updateState',
      payload: {
        locationModalVisible: true,
        newKey1: new Date().getTime() + '',
        locationItem: { lonLat: data.lonLat, dtPath: data.dtPath },
      },
    });
  };

  const deleteHandler = id => {
    for (var j = 0; j < keyUnit.dangersList.length; j++) {
      if (keyUnit.dangersList[j].id == id) {
        keyUnit.dangersList.removeServer(j);
        break;
      }
    }
    dispatch({
      type: 'keyUnit/updateState',
      payload: {
        dangersList: keyUnit.dangersList,
        catalogIds: [],
        catalogNames: {},
        uuid: 0,
      },
    });
  };

  Array.prototype.removeServer = function(dx) {
    if (isNaN(dx) || dx > this.length) {
      return false;
    }
    for (var i = 0, n = 0; i < this.length; i++) {
      if (this[i] != this[dx]) {
        this[n++] = this[i];
      }
    }
    this.length -= 1;
  };

  const loop = data =>
    data.map(d => {
      let children = null;
      if (d.children != null && d.children.length > 0) {
        children = loop(d.children);
      }
      return { title: d.name, value: d.id_, key: d.id_, children, disabled: item.id_ === d.id_ };
    });

  const onDetailUpdate = record => {
    var catalogIds = [];
    var catalogNames = {};
    var uuid = 0;
    if (record.keyUnitDangersCatalogs != null && record.keyUnitDangersCatalogs.length > 0) {
      for (var i = 0; i < record.keyUnitDangersCatalogs.length; i++) {
        catalogNames['name_' + (i + 1)] = record.keyUnitDangersCatalogs[i].name;
        catalogNames['page_' + (i + 1)] = record.keyUnitDangersCatalogs[i].page;
        catalogIds.push(i + 1);
        uuid++;
      }
    }

    dispatch({
      type: 'keyUnit/updateState',
      payload: {
        modalDangersVisible: true,
        dangersItem: record,
        msdsPath: record.msdsPath,
        imgPath: record.imgPath,
        swfPath: record.swfPath,
        detailModalType: 'editDetail',
        catalogIds: catalogIds,
        uuid: uuid,
        catalogNames: catalogNames,
      },
    });
  };

  const checkConfirm = (rule, value, callback) => {
    if (!value && $.trim(value) == '') {
      callback('请填写供水能力');
    }
    if (value > 99999999) {
      callback('供水能力不能超过99999999KG');
    } else {
      callback();
    }
  };

  const columns = [
    { title: '中文名称', dataIndex: 'cName', key: 'cName', width: 120 },
    { title: '英文名', dataIndex: 'eName', key: 'eName', width: 100 },
    { title: '危险货物编号', dataIndex: 'dangerousNum', key: 'dangerousNum', width: 100 },
    { title: 'UN号', dataIndex: 'unNum', key: 'unNum', width: 100 },
    { title: 'CAS号', dataIndex: 'casNum', key: 'casNum', width: 100 },
    {
      title: '操作',
      dataIndex: 'operation',
      key: 'operation',
      width: 100,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => onDetailUpdate(record)}>编辑</a>
          <Divider type="vertical" />
          <Popconfirm title="是否要删除此行？" onConfirm={deleteHandler.bind(this, record.id)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <FormItem style={{ marginBottom: '0px' }}>
          {getFieldDecorator('dangersList', {
            initialValue: keyUnit.dangersList,
          })(<Input type="hidden" />)}
        </FormItem>
        <Tabs type="card" activeKey={keyUnit.activityKey} onChange={onChangeTab}>
          <TabPane tab="基本信息" key="tab1">
            <Row>
              <Col span={12}>
                <FormItem label="单位名称:" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('name', {
                    initialValue: item.name,
                    rules: [
                      { required: true, message: '重点单位名称未填写' },
                      { max: 50, message: '最长不超过50个字' },
                    ],
                  })(<Input type="text" placeholder="请填写重点单位名称" />)}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem label="单位地址:" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('addr', {
                    initialValue: item.addr,
                    rules: [
                      { required: true, message: '重点单位地址未填写' },
                      { max: 100, message: '最长不超过100个字' },
                    ],
                  })(<Input type="text" placeholder="请填写重点单位地址" />)}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem label="所属消防队:" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('fireBrigadeId', {
                    initialValue: item.fireBrigadeId
                      ? item.fireBrigadeId + ''
                      : keyUnit.brigadeId + '',
                    rules: [{ required: true, message: '请选择消防队' }],
                  })(
                    <TreeSelect
                      showSearch
                      treeNodeFilterProp="title"
                      dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                      placeholder="请选择所属消防队"
                      notFoundContent="无匹配结果"
                      allowClear
                      treeData={loop(keyUnit.fireBrigadeTree)}
                      treeDefaultExpandAll
                    />
                  )}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem label="消防管理人员:" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('admin', {
                    initialValue: item.admin,
                    rules: [
                      { required: true, message: '消防管理人员名称未填写' },
                      { max: 20, message: '最长不超过20个字' },
                    ],
                  })(<Input type="text" placeholder="请填写消防管理人员" />)}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem label="值班室电话:" hasFeedback {...formItemLayout}>
                  {getFieldDecorator('tel', {
                    initialValue: item.tel,
                    rules: [
                      { required: true, message: '值班室电话未填写' },
                      { max: 20, message: '最长不超过20个字' },
                    ],
                  })(<Input type="text" placeholder="请填写值班室电话" />)}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem label="供水能力" required hasFeedback {...formItemLayout}>
                  {getFieldDecorator('waterSupply', {
                    initialValue: item.waterSupply,
                    rules: [
                      {
                        validator: checkConfirm,
                      },
                    ],
                  })(
                    <InputNumber style={{ width: '100%' }} min={0} placeholder="请填写供水能力" />
                  )}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem label="获取坐标点:" hasFeedback {...formItemLayout}>
                  <div
                    style={{
                      marginRight: '10px',
                      marginTop: '5px',
                      width: '28px',
                      height: '28px',
                      background: 'url(images/iconAdd.png)',
                      display: 'inlineBlock',
                      float: 'left',
                      cursor: 'pointer',
                    }}
                    onClick={onGetLocation}
                  />
                  {getFieldDecorator('lonLat', {
                    initialValue: keyUnit.locationItem.lonLat
                      ? keyUnit.locationItem.lonLat + ''
                      : undefined,
                  })(
                    <Input
                      type="text"
                      readOnly
                      style={{ width: '211px' }}
                      placeholder="请通过坐标点选择经纬度"
                    />
                  )}
                </FormItem>
              </Col>
              {/* <Col span={12}>
                <FormItem label="地图图标:" {...formItemLayout}>
                  <Input type="hidden" {...getFieldProps('dtPath', { initialValue:  keyUnit.locationItem.dtPath })} />
                  {keyUnit.locationItem.dtPath?
                    <div style={{borderRadius: '4px',border: '1px solid #d9d9d9',height: '64px',position: 'relative',clear: 'both',overflow: 'hidden',}}>
                    <div style={{width: '60px',height: '60px',textAlign: 'center',border: '1px solid #d9d9d9', position: 'relative', borderRadius: '5px'}}>
                      <img style={{position: 'absolute',left: '0',right: '0',bottom: '0',top: '0',margin: 'auto', width: '20px', height: '20px',}}
                        src={baseFileUrl + keyUnit.locationItem.dtPath} alt=""
                      />
                    </div>
                  </div> :""}                    
                </FormItem>
              </Col> */}
            </Row>
            <Row style={{ marginLeft: '-20px' }}>
              <FormItem label="平面图上传:" hasFeedback {...formItemLayout1}>
                {getFieldDecorator('fileList')(
                  <Upload {...pictureUploadProps} fileList={keyUnit.fileList}>
                    <Icon type="plus" />
                    <div className="ant-upload-text">平面图上传</div>
                  </Upload>
                )}
              </FormItem>
            </Row>
            <Row style={{ marginLeft: '-20px' }}>
              <Col span={24}>
                <FormItem label="重点单位简介:" hasFeedback {...formItemLayout1}>
                  {getFieldDecorator('desc', {
                    initialValue: item.desc,
                    rules: [{ max: 200, message: '最长不超过200个字' }],
                  })(<TextArea autosize={{ minRows: 3 }} placeholder="请填写重点单位简介" />)}
                </FormItem>
              </Col>
            </Row>
          </TabPane>
          {/*
          <TabPane tab="危化品信息" key="tab2">
            <Row>
              <Col span={24}>
                <Button type="primary" htmlType="submit" onClick={() => onShow()}>
                  添加危化品
                </Button>
              </Col>
            </Row>
            <Table
              rowKey={record => record.id}
              dataSource={keyUnit.dangersList}
              columns={columns}
              pagination={false}
            />
          </TabPane>
          */}
          
        </Tabs>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { keyUnit: state.keyUnit };
}

AddEditModal = Form.create()(AddEditModal);

export default connect(mapStateToProps)(AddEditModal);
