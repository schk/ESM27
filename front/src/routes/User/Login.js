import React, { Component } from 'react';
import { connect } from 'dva';
import { Link } from 'dva/router';
import { Form, Checkbox, Input, Alert, Icon } from 'antd';
import Login from 'components/Login';
import styles from './Login.less';
const FormItem = Form.Item;
const { Tab, UserName, Password, Mobile, Captcha, Submit } = Login;

@connect(({ login, loading }) => ({
  login,
  submitting: loading.effects['login/login'],
}))

class LoginPage extends Component {
  state = {
    type: 'account',
    autoLogin: true,
  };

 
  onKeyUp = (e) => {
      if(e.keyCode === 13){
        this.handleSubmit();
      }
  }

  onTabChange = type => {
    this.setState({ type });
  };

  // handleSubmit = (err, values) => {
  //   const { type } = this.state;
  //   console.log(type);
  //   console.log(values);
  //   if (!err) {
  //     this.props.dispatch({
  //       type: 'login/login',
  //       payload: {
  //         username: values.userName,
  //         password: values.password,
  //       },
  //     });
  //   }
  // };

  handleSubmit = () => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.dispatch({
          type: 'login/login',
          payload: values,
        });
      }
    });
  };

  changeAutoLogin = e => {
    this.setState({
      autoLogin: e.target.checked,
    });
  };

  renderMessage = content => {
    return <Alert style={{ marginBottom: 24 }} message={content} type="error" showIcon />;
  };

  render() {
    const { login, loginLoading } = this.props;
    const { getFieldDecorator, validateFieldsAndScroll } = this.props.form;
    const { type } = this.state;

    return (
      // <div className={styles.main}>
      //   <Login defaultActiveKey={type} onTabChange={this.onTabChange} onSubmit={this.handleSubmit}>
      //     <div style={{ width: '368px', textAlign: 'center', paddingBottom: '30px' }}>
      //       <span style={{ fontSize: '16px', lineHeight: '24px', width: '368px' }}>
      //         账户密码登录
      //       </span>
      //     </div>
      //     {login.status === 'error' &&
      //       login.type === 'account' &&
      //       !login.loginLoading &&
      //       this.renderMessage('账户或密码错误')}
      //     <UserName name="userName" placeholder="请输入用户名" />
      //     <Password name="password" placeholder="请输入密码" />
      //     <Submit loading={loginLoading}>登录</Submit>
      //   </Login>
      // </div>
      <div className={styles.main} onKeyUp={this.onKeyUp}>
        <div className={styles.box}>
          <div className={styles.system}>
            <div className={styles.systemLeft}>
              <div className={styles.logoPic}>
                <img src="images/logoPic.png" alt="" />
              </div>
            </div>
            <Form layout="horizontal" onSubmit={this.handleSubmit}>
              <div className={styles.systemRight}>
                <div className={styles.logoMain}>
                  <div className={styles.logoIn}>
                    <div className={styles.welcome}>
                      <span>欢迎登录</span>
                    </div>
                    <div className={styles.user}>
                      <div className={styles.userIn}>
                        <i
                          style={{
                            background: 'url(images/my.png) no-repeat',
                            top: '20px',
                            left: '0',
                          }}
                        />

                        <FormItem hasFeedback style={{ marginBottom: '0' }}>
                          {getFieldDecorator('username', {
                            rules: [
                              {
                                required: true,
                                message: '请填写用户名',
                              },
                            ],
                          })(<Input size="large" className={styles.inText} placeholder="用户名"/>)}
                        </FormItem>
                      </div>
                      <div className={styles.userIn}>
                        <i
                          style={{
                            background: 'url(images/password.png) no-repeat',
                            top: '20px',
                            left: '0',
                          }}
                        />
                        {/* <input type="password" placeholder="请输入密码" className={styles.inText}/> */}
                        <FormItem hasFeedback style={{ marginBottom: '0' }}>
                          {getFieldDecorator('password', {
                            rules: [
                              {
                                required: true,
                                message: '请填写密码',
                              },
                            ],
                          })(
                            <Input
                              size="large"
                              type="password"
                              className={styles.inText}
                              placeholder="密码"
                            />
                          )}
                        </FormItem>
                      </div>
                    </div>
                    <div className={styles.logOn} onClick={this.handleSubmit}>
                      <a href="javascript:void(0);">登录</a>
                    </div>
                  </div>
                </div>
              </div>
            </Form>
          </div>
        </div>
        <div className={styles.footer}>©ALIMAMA MUX, powered by alimama THX.法律声明</div>
      </div>
    );
  }
}
export default Form.create()(LoginPage);
