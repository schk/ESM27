import React, { Component, PropTypes, Fragment } from 'react';
import createG2 from 'g2-react';
import G2 from '@antv/g2';
import { Stat, Frame, Global, Shape } from 'g2';
import { connect } from 'dva';
import {
  Select,
  Form,
  Input,
  Button,
  Row,
  Col,
  Table,
  Popconfirm,
  Tree,
  Tag,
  Divider,
  Card,
  TreeSelect,
  Radio,
  DatePicker,
} from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from '../../common/common.less';
import MapModal from '../../components/datacount/Map';
import moment from 'moment';
import 'moment/locale/zh-cn';
moment.locale('zh-cn');
function MapCount({ location, dispatch, policeNumber, form }) {
  const { getFieldDecorator, validateFields, getFieldsValue, resetFields, getFieldValue } = form;
  const { list } = policeNumber;

  const MapModalProps = {
    charData: [{ name: '北京', value: 120 }, { name: '陕西', value: 90 }],
  };
  return (
    <PageHeaderLayout>
      <div>
        <Card bordered={false}>
          <Row>
            <Col span={24}>
              <MapModal {...MapModalProps} />
            </Col>
          </Row>
        </Card>
      </div>
    </PageHeaderLayout>
  );
}

function mapStateToProps(state) {
  return {
    policeNumber: state.policeNumber,
  };
}

MapCount = Form.create()(MapCount);

export default connect(mapStateToProps)(MapCount);
