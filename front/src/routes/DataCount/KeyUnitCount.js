import React, { Component, PropTypes, Fragment } from 'react';
import createG2 from 'g2-react';
import G2 from '@antv/g2';
import { Stat, Frame, Global, Shape } from 'g2';
import { connect } from 'dva';
import {
  Select,
  Form,
  Input,
  Button,
  Row,
  Col,
  Table,
  Popconfirm,
  Tree,
  Tag,
  Divider,
  Card,
  TreeSelect,
} from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from '../../common/common.less';
import MoerIntervalStack from '../../components/datacount/MoerIntervalStack';
const TreeNode = Tree.TreeNode;

function KeyUnitCount({ location, dispatch, keyunitcount, form }) {
  const { getFieldDecorator, validateFields, getFieldsValue, resetFields } = form;
  const { list } = keyunitcount;

  function handleSearch() {
    dispatch({
      type: 'keyunitcount/queryData',
      payload: { ...getFieldsValue() },
    });
  }
  const MoerIntervalStackProps = {
    charData: keyunitcount.charData,
    fields: keyunitcount.fields,
  };
  return (
    <PageHeaderLayout>
      <div>
        <Card bordered={false}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={6} sm={24}>
                  <Form.Item label="消防队：">
                    {getFieldDecorator('teamIds')(
                      <TreeSelect
                        optionFilterProp="children"
                        showSearch
                        allowClear
                        multiple={true}
                        treeCheckable={true}
                        placeholder="请选择消防队"
                        showCheckedStrategy={'SHOW_ALL'}
                        treeData={keyunitcount.fireBrigades}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col md={18} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <br />
          <Row>
            <Col span={24}>
              <div>
                <MoerIntervalStack {...MoerIntervalStackProps} />
              </div>
            </Col>
          </Row>
        </Card>
      </div>
    </PageHeaderLayout>
  );
}

function mapStateToProps(state) {
  return {
    keyunitcount: state.keyunitcount,
  };
}

KeyUnitCount = Form.create()(KeyUnitCount);

export default connect(mapStateToProps)(KeyUnitCount);
