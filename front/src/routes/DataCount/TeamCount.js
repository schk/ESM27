import React, { Component, PropTypes, Fragment } from 'react';
import createG2 from 'g2-react';
import G2 from '@antv/g2';
import { Stat, Frame, Global, Shape } from 'g2';
import { connect } from 'dva';
import {
  Select,
  Form,
  Input,
  Button,
  Row,
  Col,
  Table,
  Popconfirm,
  Tree,
  Tag,
  Divider,
  Card,
  TreeSelect,
} from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from '../../common/common.less';
import IntervalStack from '../../components/datacount/IntervalStack';
const TreeNode = Tree.TreeNode;

function TeamCount({ location, dispatch, teamCount, form }) {
  const { getFieldDecorator, validateFields, getFieldsValue, resetFields } = form;
  const { list } = TeamCount;

  let plotCfg = {
    margin: [80, 80, 90, 80],
  };
  function handleSearch() {
    dispatch({
      type: 'teamCount/queryData',
      payload: { ...getFieldsValue(), ...{ typeId: null } },
    });
  }
  let chartdata = [
    { label: '特勤大队', value: 12, percent: 0.1 },
    { label: '沙河口大队', value: 20, percent: 0.11 },
    { label: '保税区', value: 11, percent: 0.09 },
    { label: '石化特勤大队', value: 33, percent: 0.12 },
    { label: '旅顺大队', value: 25, percent: 0.2 },
    { label: '甘井子大队', value: 12, percent: 0.1 },
    { label: '开发区大队', value: 20, percent: 0.1 },
    { label: '金州大队', value: 11, percent: 0.1 },
    { label: '高新园大队', value: 33, percent: 0.1 },
    { label: '海兰典大队', value: 25, percent: 0.1 },
  ];
  const IntervalStackProps = {
    chartData: teamCount.data,
  };
  return (
    <PageHeaderLayout>
      <div>
        <Card bordered={false}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={6} sm={24}>
                  <Form.Item label="消防队：">
                    {getFieldDecorator('teamIds')(
                      <TreeSelect
                        optionFilterProp="children"
                        showSearch
                        allowClear
                        multiple={true}
                        treeCheckable={true}
                        placeholder="请选择消防队"
                        showCheckedStrategy={'SHOW_ALL'}
                        treeData={teamCount.fireBrigades}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col md={18} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <br />
          <Row>
            <Col span={24}>
              <div>
                <IntervalStack {...IntervalStackProps} />
              </div>
            </Col>
          </Row>
        </Card>
      </div>
    </PageHeaderLayout>
  );
}

function mapStateToProps(state) {
  return {
    teamCount: state.teamCount,
  };
}

TeamCount = Form.create()(TeamCount);

export default connect(mapStateToProps)(TeamCount);
