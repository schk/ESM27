import React, { Component, PropTypes, Fragment } from 'react';
import createG2 from 'g2-react';
import G2 from '@antv/g2';
import { Stat, Frame, Global, Shape } from 'g2';
import { connect } from 'dva';
import {
  Select,
  Form,
  Input,
  Button,
  Row,
  Col,
  Table,
  Popconfirm,
  Tree,
  Tag,
  Divider,
  Card,
  TreeSelect,
  Radio,
  DatePicker,
} from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from '../../common/common.less';
import LntervalLine from '../../components/datacount/LntervalLine';
import moment from 'moment';
import 'moment/locale/zh-cn';
moment.locale('zh-cn');
const FormItem = Form.Item;
const Option = Select.Option;
const TreeNode = Tree.TreeNode;
const MonthPicker = DatePicker.MonthPicker;
function AccidentCount({ location, dispatch, accidentCount, form }) {
  const { getFieldDecorator, validateFields, getFieldsValue, resetFields, getFieldValue } = form;
  const { list } = accidentCount;
  let plotCfg = {
    margin: [80, 80, 90, 80],
  };
  function handleSearch() {
    var formValue = getFieldsValue();
    if (formValue.dateType == 'month') {
      formValue.dateValue =
        formValue.month != null && formValue.month != ''
          ? new moment(formValue.month).format('YYYY-MM')
          : '';
    } else {
      formValue.dateValue =
        formValue.year != null && formValue.year != ''
          ? new moment(formValue.year).format('YYYY')
          : '';
    }
    dispatch({
      type: 'accidentCount/queryData',
      payload: formValue,
    });
  }
  const yearList = accidentCount.yearList
    ? accidentCount.yearList.map(d => <Option key={d.value}>{d.label}</Option>)
    : [];
  const LntervalLineProps = {
    chartData: accidentCount.data,
  };
  function radioChange(e) {
    var formValue = getFieldsValue();
    if (e.target.value == 'month') {
      formValue.dateValue =
        formValue.month != null && formValue.month != ''
          ? new moment(formValue.month).format('YYYY-MM')
          : '';
    } else {
      formValue.dateValue =
        formValue.year != null && formValue.year != ''
          ? new moment(formValue.year).format('YYYY')
          : '';
    }
    formValue.dateType = e.target.value;
    dispatch({
      type: 'accidentCount/queryData',
      payload: formValue,
    });
  }
  return (
    <PageHeaderLayout>
      <div>
        <Card bordered={false}>
          <div className={styles.tableListForm}>
            <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col md={6} sm={24}>
                  <Form.Item label="类型：">
                    {getFieldDecorator('dateType', { initialValue: 'month' })(
                      <Radio.Group onChange={radioChange.bind(this)}>
                        <Radio.Button value="month">月</Radio.Button>
                        <Radio.Button value="year">年</Radio.Button>
                      </Radio.Group>
                    )}
                  </Form.Item>
                </Col>
                <Col md={6} sm={24}>
                  {getFieldValue('dateType') == 'month' ? (
                    <Form.Item label="月份：">
                      {getFieldDecorator('month', {
                        initialValue: moment().subtract(0, 'months'),
                      })(<MonthPicker placeholder="请选择" />)}
                    </Form.Item>
                  ) : (
                    <Form.Item label="年份：">
                      {getFieldDecorator('year', { initialValue: new Date().getFullYear() + '' })(
                        <Select
                          optionFilterProp="children"
                          showSearch
                          allowClear
                          placeholder="请选择"
                        >
                          {yearList}
                        </Select>
                      )}
                    </Form.Item>
                  )}
                </Col>
                <Col md={12} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit" onClick={handleSearch}>
                      查询
                    </Button>
                  </span>
                </Col>
              </Row>
            </Form>
          </div>
          <br />
          <Row>
            <Col span={24}>
              <div>
                <LntervalLine {...LntervalLineProps} />
              </div>
            </Col>
          </Row>
        </Card>
      </div>
    </PageHeaderLayout>
  );
}

function mapStateToProps(state) {
  return {
    accidentCount: state.accidentCount,
  };
}

AccidentCount = Form.create()(AccidentCount);

export default connect(mapStateToProps)(AccidentCount);
