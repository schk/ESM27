import React from 'react';
import {
  Form,
  Input,
  Modal,
  Button,
  Row,
  Col,
  message,
  Checkbox,
  Icon,
  InputNumber,
  Select,
} from 'antd';
const FormItem = Form.Item;
const { TextArea } = Input;
const Option = Select.Option;
import { connect } from 'dva';
import styles from './ExprModal.less';

const formItemLayout = {
  labelCol: { span: 3 },
  wrapperCol: { span: 19 },
};

const formItemLayout1 = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};

const formItemLayout2 = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
};

var ExprModal = ({ expr, item = {}, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: expr.modalType == 'create' ? '新建公式' : '修改公式',
    visible: expr.modalVisible,
    maskClosable: false,
    width: 1000,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={() => handleOk()}
        loading={expr.buttomLoading}
      >
        保存
      </Button>,
    ],
  };

  if (!expr.modalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'expr/updateState',
      payload: {
        modalVisible: false,
        variables: [],
        index: 0,
      },
    });
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      let variables = expr.variables;
      if (variables == null || variables.length == 0) {
        message.error('请添加变量');
        return;
      }
      debugger;
      for(var i=0;i < variables.length; i++){
        if(variables[i].type==2){
          if(variables[i].childOption==null || variables[i].childOption==undefined || variables[i].childOption.length==0){
            message.error('请添加选择变量的属性和数值');
            return;
          }
        }
      }

      let fromValue = getFieldsValue();
      let exprParams = [];
      for (var i = 0; i < variables.length; i++) {
        var param = {};
        param.value = fromValue['value_' + i];
        param.name = fromValue['name_' + i];
        param.empty = variables[i].empty == true ? 1 : 2;
        param.realTime = variables[i].realTime == true ? 1 : 2;
        param.type = variables[i].type;
        var childOption = [];
        if (variables[i].childOption) {
          for (var j = 0; j < variables[i].childOption.length; j++) {
            var childOptionItem = {};
            (childOptionItem.defaultVal = variables[i].childOption[j].defaultVal == true ? 1 : 2),
              (childOptionItem.attr = fromValue['attr_' + j + '_DATA_' + i]);
            childOptionItem.optionVal = fromValue['optionVal_' + j + '_DATA_' + i];
            childOption.push(childOptionItem);
          }
        }
        param.childOption = childOption;
        exprParams.push(param);
      }
      fromValue.exprParams = exprParams;
      fromValue.id = expr.modalType == 'create' ? '0' : item.id_;
      dispatch({
        type: `expr/${expr.modalType}`,
        payload: fromValue,
        search: { pageNum: expr.current, pageSize: expr.pageSize },
      });
    });
  }

  function exprinsert(type) {
    switch (type) {
      case 1:
        insertText('+');
        break;
      case 2:
        insertText('-');
        break;
      case 3:
        insertText('*');
        break;
      case 3:
        insertText('/');
        break;
      case 4:
        insertText('/');
        break;
      case 5:
        insertText('^');
        break;
      case 6:
        insertText('abs()');
        break;
      case 7:
        insertText('sin()');
        break;
      case 8:
        insertText('cos()');
        break;
      case 9:
        insertText('tan()');
        break;
      case 9:
        insertText('asin()');
        break;
      case 10:
        insertText('asin()');
        break;
      case 11:
        insertText('acos()');
        break;
      case 12:
        insertText('atan()');
        break;
      case 13:
        insertText('ceil()');
        break;
      case 14:
        insertText('sqrt()');
        break;
      case 15:
        insertText('exp()');
        break;
      case 16:
        insertText('log()');
        break;
    }
  }

  function variable(type) {
    dispatch({
      type: 'expr/addv',
      payload: type,
    });
  }

  function ins(index) {
    let v = $('#value_' + index).val();
    insertText(v);
  }

  function insdel(index) {
    dispatch({
      type: 'expr/delv',
      payload: index,
    });
  }

  function addArray(index) {
    dispatch({
      type: 'expr/addArray',
      payload: index,
    });
  }

  //删除选项
  function delArray(index, indexArray) {
    dispatch({
      type: 'expr/delArray',
      payload: {
        index: index,
        indexArray: indexArray,
      },
    });
  }

  //勾选是否为空
  function insEmpty(index, event) {
    dispatch({
      type: 'expr/changeEmpty',
      payload: {
        index: index,
        checkd: event.target.checked,
      },
    });
  }

  //勾选是否实时数据
  function insReal(index, event) {
    dispatch({
      type: 'expr/changeReal',
      payload: {
        index: index,
        checkd: event.target.checked,
      },
    });
  }

  //勾选是否实时数据
  function insDefault(index, indexArray, event) {
    console.log(event);
    console.log(index);
    console.log(indexArray);
    dispatch({
      type: 'expr/changeDefault',
      payload: {
        index: index,
        indexArray: indexArray,
        checkd: event.target.checked,
      },
    });
  }

  function insertText(str) {
    var obj = $('#expr').get(0);
    if (document.selection) {
      obj.focus();
      var sel = document.selection.createRange();
      sel.text = str;
    } else if (typeof obj.selectionStart === 'number' && typeof obj.selectionEnd === 'number') {
      var startPos = obj.selectionStart;
      var endPos = obj.selectionEnd;
      var tmpStr = obj.value;
      let v = tmpStr.substring(0, startPos) + str + tmpStr.substring(endPos, tmpStr.length);
      setFieldsValue({ expr: v });
    } else {
      let v = obj.value + str;
      setFieldsValue({ expr: v });
    }
  }

  function totalCount(rule, value, callback) {
    let type = getFieldValue('dressingMethod');
    if (type == 1 && (value == null || value == '')) {
      callback(new Error('小数点位数不能为空'));
    } else {
      callback();
    }
  }

  const handldDressingMethod = value => {
    form.setFieldsValue({ scale: '' });
    dispatch({
      type: 'expr/changeDressingMethod',
      payload: value,
    });
  };

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={12}>
            <FormItem label="公式名称" hasFeedback {...formItemLayout2}>
              {getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  { required: true, message: '公式名称未填写' },
                  { max: 50, message: '最长不超过50个字' },
                ],
              })(<Input type="text" placeholder="请填写公式名称" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="单位" hasFeedback {...formItemLayout2}>
              {getFieldDecorator('unit', {
                initialValue: item.unit,
                rules: [{ max: 20, message: '最长不超过20个字' }],
              })(<Input type="text" placeholder="请填写单位" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="取整方式" hasFeedback {...formItemLayout2}>
              {getFieldDecorator('dressingMethod', {
                initialValue: item.dressingMethod ? item.dressingMethod + '' : undefined,
                rules: [{ required: true, message: '请选择取整方式' }],
              })(
                <Select
                  placeholder="请选择取整方式"
                  onChange={handldDressingMethod}
                  style={{ width: '100%' }}
                  allowClear
                >
                  <Option value="1">四舍五入</Option>
                  <Option value="2">向上取整</Option>
                  <Option value="3">向下取整</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          {expr.dressingMethod == 1 ? (
            <Col span={12}>
              <FormItem required label="小数点位数" hasFeedback {...formItemLayout2}>
                {getFieldDecorator('scale', {
                  initialValue: item.scale,
                  rules: [{ validator: totalCount }],
                })(
                  <InputNumber
                    min={1}
                    max={6}
                    style={{ width: '100%' }}
                    placeholder="请填写小数点位数"
                  />
                )}
              </FormItem>
            </Col>
          ) : (
            ''
          )}
        </Row>

        <Row style={{ marginLeft: '110px' }}>
          <Col className={styles.jianJu} span={2}>
            <Button className={styles.button} onClick={exprinsert.bind(this, 1)}>
              +
            </Button>
          </Col>
          <Col className={styles.jianJu} span={2}>
            <Button className={styles.button} onClick={exprinsert.bind(this, 2)}>
              -
            </Button>
          </Col>
          <Col className={styles.jianJu} span={2}>
            <Button className={styles.button} onClick={exprinsert.bind(this, 3)}>
              *
            </Button>
          </Col>
          <Col className={styles.jianJu} span={2}>
            <Button className={styles.button} onClick={exprinsert.bind(this, 4)}>
              /
            </Button>
          </Col>
          <Col className={styles.jianJu} span={2}>
            <Button className={styles.button} onClick={exprinsert.bind(this, 5)}>
              ^
            </Button>
          </Col>
          <Col className={styles.jianJu} span={2}>
            <Button className={styles.button} onClick={exprinsert.bind(this, 6)}>
              abs
            </Button>
          </Col>
          <Col className={styles.jianJu} span={2}>
            <Button className={styles.button} onClick={exprinsert.bind(this, 7)}>
              sin
            </Button>
          </Col>
          <Col className={styles.jianJu} span={2}>
            <Button className={styles.button} onClick={exprinsert.bind(this, 8)}>
              cos
            </Button>
          </Col>
        </Row>

        <Row style={{ marginLeft: '110px', marginTop: '10px' }}>
          <Col className={styles.jianJu} span={2}>
            <Button className={styles.button} onClick={exprinsert.bind(this, 9)}>
              tan
            </Button>
          </Col>
          <Col className={styles.jianJu} span={2}>
            <Button className={styles.button} onClick={exprinsert.bind(this, 10)}>
              asin
            </Button>
          </Col>
          <Col className={styles.jianJu} span={2}>
            <Button className={styles.button} onClick={exprinsert.bind(this, 11)}>
              acos
            </Button>
          </Col>
          <Col className={styles.jianJu} span={2}>
            <Button className={styles.button} onClick={exprinsert.bind(this, 12)}>
              atan
            </Button>
          </Col>
          <Col className={styles.jianJu} span={2}>
            <Button className={styles.button} onClick={exprinsert.bind(this, 13)}>
              ceil
            </Button>
          </Col>
          <Col className={styles.jianJu} span={2}>
            <Button className={styles.button} onClick={exprinsert.bind(this, 12)}>
              sqrt
            </Button>
          </Col>
          <Col className={styles.jianJu} span={2}>
            <Button className={styles.button} onClick={exprinsert.bind(this, 15)}>
              exp
            </Button>
          </Col>
          <Col className={styles.jianJu} span={2}>
            <Button className={styles.button} onClick={exprinsert.bind(this, 16)}>
              log
            </Button>
          </Col>
        </Row>

        <Row style={{ marginTop: '20px' }}>
          <Col span={24}>
            <FormItem label="公式" hasFeedback {...formItemLayout}>
              {getFieldDecorator('expr', {
                initialValue: item.expr,
                rules: [
                  { required: true, message: '公式未填写' },
                  { max: 1000, message: '最长不超过1000个字' },
                ],
              })(<TextArea placeholder="请填写公式" autosize={{ minRows: 4 }} id="expr" />)}
            </FormItem>
          </Col>
        </Row>
        <Row style={{ marginTop: '20px' }}>
          <Col span={24}>
            <FormItem label="描述" hasFeedback {...formItemLayout}>
              {getFieldDecorator('describtion', {
                initialValue: item.describtion,
                rules: [
                  { required: true, message: '描述未填写' },
                  { max: 1000, message: '最长不超过1000个字' },
                ],
              })(<TextArea placeholder="请填写描述" autosize={{ minRows: 4 }} />)}
            </FormItem>
          </Col>
        </Row>

        <Row style={{ marginLeft: '20px', marginBottom: '10px' }}>
          <Button type="primary" onClick={variable.bind(this, 1)}>
            增加普通变量
          </Button>
          <Button style={{ marginLeft: '20px' }} type="primary" onClick={variable.bind(this, 2)}>
            增加选择变量
          </Button>
        </Row>
        {expr.variables.map((d, i) => {
          return d.type == 1 ? (
            <Row key={i}>
              <div className="ant-row" style={{ backgroundColor: '#fff7d8' }}>
                <Col span={2}>
                  <span
                    type="primary"
                    style={{ lineHeight: '40px', display: 'block', textAlign: 'center' }}
                  >
                    普通变量
                  </span>
                </Col>
                <Col span={6}>
                  <FormItem
                    label="名称"
                    hasFeedback
                    {...formItemLayout1}
                    style={{ marginBottom: '0' }}
                  >
                    {getFieldDecorator('name_' + i, {
                      initialValue: d.name,
                      rules: [{ required: true, message: '名称未填写' }],
                    })(<Input type="text" />)}
                  </FormItem>
                </Col>
                <Col span={6}>
                  <FormItem
                    label="变量"
                    hasFeedback
                    {...formItemLayout1}
                    style={{ marginBottom: '0' }}
                  >
                    {getFieldDecorator('value_' + i, {
                      initialValue: d.value,
                      rules: [{ required: true, message: '变量未填写' }],
                    })(<Input type="text" />)}
                  </FormItem>
                </Col>
                <Col span={9} style={{ marginTop: '8px' }}>
                  <Button size="small" type="primary" onClick={ins.bind(this, i)}>
                    插入
                  </Button>
                  &nbsp;&nbsp;
                  <Button size="small" onClick={insdel.bind(this, i)}>
                    删除
                  </Button>
                  &nbsp;&nbsp;
                  <Checkbox checked={d.empty} onChange={insEmpty.bind(this, i)}>
                    是否为空
                  </Checkbox>
                  {/* <Checkbox checked={d.realTime} onChange={insReal.bind(this, i)}>
                    实时获取
                  </Checkbox> */}
                </Col>
              </div>
            </Row>
          ) : (
            <div style={{ backgroundColor: '#e6f7ff' }}>
              <Row key={i}>
                <Col span={2}>
                  <span
                    type="primary"
                    style={{ lineHeight: '40px', display: 'block', textAlign: 'center' }}
                  >
                    选择变量
                  </span>
                </Col>
                <Col span={6}>
                  <FormItem
                    label="名称"
                    hasFeedback
                    {...formItemLayout1}
                    style={{ marginBottom: '0' }}
                  >
                    {getFieldDecorator('name_' + i, {
                      initialValue: d.name,
                      rules: [{ required: true, message: '名称未填写' }],
                    })(<Input type="text" />)}
                  </FormItem>
                </Col>
                <Col span={6}>
                  <FormItem
                    label="变量"
                    hasFeedback
                    {...formItemLayout1}
                    style={{ marginBottom: '0' }}
                  >
                    {getFieldDecorator('value_' + i, {
                      initialValue: d.value,
                      rules: [{ required: true, message: '变量未填写' }],
                    })(<Input type="text" />)}
                  </FormItem>
                </Col>
                <Col span={9} style={{ marginTop: '8px' }}>
                  <Button size="small" type="primary" onClick={ins.bind(this, i)}>
                    插入
                  </Button>
                  &nbsp;&nbsp;
                  <Button size="small" onClick={insdel.bind(this, i)}>
                    删除
                  </Button>
                  &nbsp;&nbsp;
                  <Checkbox checked={d.empty} onChange={insEmpty.bind(this, i)}>
                    是否为空
                  </Checkbox>
                  {/* <Checkbox checked={d.realTime} onChange={insReal.bind(this, i)}>
                    实时获取
                  </Checkbox> */}
                  &nbsp;&nbsp;
                  <Button
                    type="primary"
                    size="small"
                    shape="circle"
                    onClick={addArray.bind(this, i)}
                  >
                    +
                  </Button>
                </Col>
              </Row>
              {d.childOption
                ? d.childOption.map((item, j) => {
                    return (
                      <Row key={j + '子选项'}>
                        <Col span={2} />
                        <Col span={6}>
                          <FormItem
                            label="属性"
                            hasFeedback
                            {...formItemLayout1}
                            style={{ marginBottom: '0' }}
                          >
                            {getFieldDecorator('attr_' + j + '_DATA_' + i, {
                              initialValue: item.attr,
                              rules: [{ required: true, message: '属性未填写' }],
                            })(<Input type="text" />)}
                          </FormItem>
                        </Col>
                        <Col span={6}>
                          <FormItem
                            label="数值"
                            hasFeedback
                            {...formItemLayout1}
                            style={{ marginBottom: '0' }}
                          >
                            {getFieldDecorator('optionVal_' + j + '_DATA_' + i, {
                              initialValue: item.optionVal,
                              rules: [{ required: true, message: '数值未填写' }],
                            })(<Input type="text" />)}
                          </FormItem>
                        </Col>
                        <Col span={6} style={{ marginTop: '8px' }}>
                          <Icon
                            className="dynamic-delete-button"
                            type="minus-circle-o"
                            onClick={delArray.bind(this, i, j)}
                          />
                          &nbsp;&nbsp;
                          <Checkbox
                            checked={item.defaultVal}
                            onChange={insDefault.bind(this, i, j)}
                          >
                            是否默认值
                          </Checkbox>
                        </Col>
                      </Row>
                    );
                  })
                : ''}
            </div>
          );
        })}
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { expr: state.expr };
}

ExprModal = Form.create()(ExprModal);

export default connect(mapStateToProps)(ExprModal);
