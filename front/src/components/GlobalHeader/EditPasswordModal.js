import React, { PropTypes } from 'react';
import { Form, Input, Modal, Button, Row, Col, Radio, Upload, Icon, Select } from 'antd';
const RadioGroup = Radio.Group;
const Option = Select.Option;
const FormItem = Form.Item;
import { connect } from 'dva';

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
};

var EditPasswordModal = ({ user, form, dispatch }) => {
  const {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    getFieldValue,
    resetFields,
    setFieldsValue,
  } = form;

  const modalOpts = {
    title: '修改密码',
    visible: user.editPwModalVisible,
    maskClosable: false,
    width: 500,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={handleOk}
        loading={user.buttomLoading}
      >
        确认
      </Button>,
    ],
  };

  if (!user.editPwModalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'user/updateState',
      payload: {
        editPwModalVisible: false,
        confirmDirty: false,
      },
    });
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }

      dispatch({
        type: 'user/updatePassword',
        payload: {
          password: getFieldValue('password'),
          id: user.currentUser.id,
        },
      });
    });
  }

  function checkOldPassword(rule, value, callback) {
    dispatch({
      type: 'user/checkOldPassword',
      payload: {
        oldPassword: value,
        username: user.currentUser.account,
      },
      callback: callback,
    });
  }

  function checkPassword(rule, value, callback) {
    var rega = /[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/;
    var regb = /^.{6,18}$/;
    if (value && (!rega.test(value) || !regb.test(value))) {
      callback('6-18位字母、数字或符号组成，必须包含字母和数字');
    } else {
      callback();
    }
    if (value && user.confirmDirty) {
      validateFields(['confirmPassword'], { force: true });
    }
  }

  function checkConfirm(rule, value, callback) {
    if (value && value !== getFieldValue('password')) {
      callback('两次输入密码不一致');
    } else {
      callback();
    }
  }

  function handleConfirmBlur(e) {
    const value = e.target.value;
    dispatch({
      type: 'user/updateState',
      payload: {
        confirmDirty: user.confirmDirty || !!value,
      },
    });
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={24} style={{ height: '56px' }}>
            <FormItem label="原密码：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('oldPassword', {
                initialValue: '',
                rules: [
                  { required: true, message: '原密码未填写' },
                  { validator: checkOldPassword },
                ],
                validateTrigger: 'onBlur',
                validateFirst: true,
              })(<Input type="password" autoComplete="off" placeholder="请输入原密码" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{ height: '56px' }}>
            <FormItem label="新密码：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: '新密码未填写' }, { validator: checkPassword }],
              })(<Input type="password" autoComplete="off" placeholder="请输入新密码" />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{ height: '56px' }}>
            <FormItem label="确认新密码：" hasFeedback {...formItemLayout}>
              {getFieldDecorator('confirmPassword', {
                rules: [{ required: true, message: '密码未确认' }, { validator: checkConfirm }],
              })(
                <Input
                  type="password"
                  autoComplete="off"
                  placeholder="请确认密码"
                  onBlur={handleConfirmBlur}
                />
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { user: state.user };
}

EditPasswordModal = Form.create()(EditPasswordModal);

export default connect(mapStateToProps)(EditPasswordModal);
