import React, { PropTypes } from 'react';
import {
  Form,
  Input,
  Modal,
  Select,
  TreeSelect,
  DatePicker,
  Row,
  Col,
  textarea,
  Button,
} from 'antd';
import moment from 'moment';
import 'moment/locale/zh-cn';
moment.locale('zh-cn');
import { connect } from 'dva';
const FormItem = Form.Item;
const Option = Select.Option;

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
};

var EditPersonDataModal = ({ user, dispatch, form, type }) => {
  const item = user.currentUser;
  const sexList = user.sexList ? user.sexList.map(d => <Option key={d.code}>{d.name}</Option>) : [];

  const { getFieldDecorator, validateFields, getFieldsValue, resetFields } = form;
  const modalOpts = {
    title: '修改个人资料',
    visible: user.editModalVisible,
    maskClosable: false,
    width: 600,
    onCancel: handleCansel,
    footer: [
      <Button key="back" type="ghost" size="large" onClick={handleCansel}>
        取消
      </Button>,
      <Button
        key="submit"
        type="primary"
        size="large"
        onClick={handleOk}
        loading={user.buttomLoading}
      >
        确认
      </Button>,
    ],
  };

  if (!user.editModalVisible) {
    resetFields();
  }

  function handleCansel() {
    dispatch({
      type: 'user/updateState',
      payload: {
        editModalVisible: false,
      },
    });
  }

  function handleOk() {
    validateFields(errors => {
      if (errors) {
        return;
      }
      dispatch({
        type: 'user/updatePersonData',
        payload: { ...getFieldsValue(), id: item.id, version: item.version },
      });
    });
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <Row>
          <Col span={12} key={1}>
            <FormItem id="name" label="姓名" {...formItemLayout} hasFeedback>
              {getFieldDecorator('name', {
                initialValue: item.name,
                validate: [
                  {
                    rules: [
                      { required: true, min: 1, max: 20, message: '请输入名称,不能超过20个字符' },
                    ],
                    trigger: 'onBlur',
                  },
                ],
              })(<Input type="text" placeholder="请输入姓名" />)}
            </FormItem>
          </Col>
          <Col span={12} key={2}>
            <FormItem id="name" label="用户名" {...formItemLayout} hasFeedback>
              {getFieldDecorator('username', {
                initialValue: item.username,
                validate: [
                  {
                    rules: [
                      { required: true, min: 1, max: 20, message: '请输入用户名,不能超过20个字符' },
                    ],
                    trigger: 'onBlur',
                  },
                ],
              })(<Input type="text" placeholder="请输入用户名" />)}
            </FormItem>
          </Col>
        </Row>

        <Row>
          <Col span={12} key={3}>
            <FormItem id="sexCode" label="性别" {...formItemLayout} required hasFeedback>
              {getFieldDecorator('sexCode', {
                initialValue: item.sexCode,
                validate: [
                  {
                    rules: [{ required: true, message: '请选择性别' }],
                    trigger: 'onBlur',
                  },
                ],
              })(
                <Select showSearch optionFilterProp="children" placeholder="请选择性别">
                  {sexList}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col span={12} key={4}>
            <FormItem id="phone" label="手机号" {...formItemLayout} required hasFeedback>
              {getFieldDecorator('mobilePhone', {
                initialValue: item.mobilePhone,
                validate: [
                  {
                    rules: [{ required: true, min: 11, max: 11, message: '请填写11位手机号' }],
                    trigger: 'onBlur',
                  },
                ],
              })(<Input type="text" placeholder="请输入手机号" />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

function mapStateToProps(state) {
  return { user: state.user };
}

EditPersonDataModal = Form.create()(EditPersonDataModal);

export default connect(mapStateToProps)(EditPersonDataModal);
