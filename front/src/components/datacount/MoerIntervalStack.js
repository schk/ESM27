import React, { Component, PropTypes } from 'react';
import { connect } from 'dva';
import { hashHistory } from 'dva/router';
import { Icon, Select, Button, DatePicker, Table, Input } from 'antd';
import G2 from '@antv/g2';
import { DataSet } from '@antv/data-set';
import { Stat, Frame, Global, Shape } from 'g2';
var _state, _props, _this;

class MoerIntervalStack extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      fields: [],
    };
  }
  componentWillReceiveProps(newProps) {
    console.log(newProps);
    if (this.state.data === newProps.charData) {
    } else {
      this.setState({ data: newProps.charData, fields: newProps.fields });
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.data === this.state.data) {
    } else {
      if (!(this.state.data === {})) this.upg2value(this.state.data, this.state.fields);
    }
  }

  upg2value(data, fields) {
    var ds = new DataSet();
    var dv = ds.createView().source(data);
    dv.transform({
      type: 'fold',
      fields: fields, // 展开字段集
      key: 'transkey', // key字段
      value: 'transvalue', // value字段
    });
    if (this.state.chart) {
      const chart = this.state.chart;
      chart.changeData(dv);
      this.setState({ chart: chart });
    } else {
      console.log(data);
      var chart = new G2.Chart({
        container: 'teamcountChart',
        forceFit: true,
        height: 600,
        animate: false,
        plotCfg: {
          margin: [80, 80, 90, 80],
        },
      });
      chart.source(dv);
      chart
        .intervalStack()
        .position('transkey*transvalue')
        .color('label');
      chart.render();
      this.setState({ chart: chart });
    }
  }
  render() {
    _this = this;
    _props = _this.props;
    _state = _this.state;
    console.log(_this.state.data);
    return (
      <div>
        <div id="teamcountChart" ref="chartbox" />
      </div>
    );
  }
}

export default MoerIntervalStack;
