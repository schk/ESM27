import React, { Component, PropTypes } from 'react';
import { connect } from 'dva';
import { hashHistory } from 'dva/router';
import { Icon, Select, Button, DatePicker, Table, Input } from 'antd';
import G2 from '@antv/g2';
import { Stat, Frame, Global, Shape } from 'g2';
var _state, _props, _this;

class LntervalLine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
    };
  }
  componentWillReceiveProps(newProps) {
    if (this.state.data === newProps.chartData) {
    } else {
      this.setState({ data: newProps.chartData });
    }
  }
  componentDidMount() {}
  componentDidUpdate(prevProps, prevState) {
    if (prevState.data === this.state.data) {
    } else {
      if (!(this.state.data === {})) this.upg2value(this.state.data);
    }
  }
  upg2value(data) {
    if (this.state.chart) {
      const chart = this.state.chart;
      chart.changeData(data);
      this.setState({ chart: chart });
    } else {
      var chart = new G2.Chart({
        container: 'c1',
        forceFit: true,
        height: 600,
        animate: false,
        padding: [20, 80, 80, 80],
      });
      chart.source(data, {
        type: {
          type: 'value', // 声明 type 字段为分类类型
          alias: '事故高发', // 设置属性的别名
        },
      });
      chart.scale('value', {
        min: 0,
        alias: '事故高发',
      });
      chart.scale('rate', {
        min: 0,
        alias: '救援效率',
      });
      chart.scale('label', {
        range: [0.05, 0.95],
      });
      chart
        .interval()
        .position('label*value')
        .color('#48D1CC');
      // chart.line().position('label*rate').color('#1890ff');
      // chart.point().position('label*rate').size(5).shape('hollowCircle').style({
      //   stroke: '#1890ff',
      //   lineWidth: 1
      // });
      chart.render();
      this.setState({ chart: chart });
    }
  }
  render() {
    _this = this;
    _props = _this.props;
    _state = _this.state;

    return (
      <div>
        <div id="c1" ref="chartbox" />
      </div>
    );
  }
}

export default LntervalLine;
