import React, { Component, PropTypes } from 'react';
import { connect } from 'dva';
import { hashHistory } from 'dva/router';
import { Icon, Select, Button, DatePicker, Table, Input } from 'antd';
import G2 from '@antv/g2';
import { Stat, Frame, Global, Shape } from 'g2';
var _state, _props, _this;

class IntervalStack extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      flag: 0,
    };
  }
  componentWillReceiveProps(newProps) {
    if (this.state.data === newProps.chartData) {
    } else {
      this.setState({ data: newProps.chartData });
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.data === this.state.data) {
    } else {
      if (!(this.state.data === [] || this.state.data.length == 0)) this.upg2value(this.state.data);
      else this.setState({ chart: null });
    }
  }
  upg2value(data) {
    if (this.state.chart) {
      const chart = this.state.chart;
      chart.changeData(data);
      this.setState({ chart: chart });
    } else {
      console.log(data);
      var chart = new G2.Chart({
        container: 'teamcountChart',
        forceFit: true,
        height: 650,
        animate: false,
        plotCfg: {
          margin: [50],
        },
      });
      chart.source(data);
      chart.coord('theta', {
        radius: 0.7,
        innerRadius: 0.3,
      });
      chart.legend(false);
      chart.tooltip({
        showTitle: false,
        itemTpl: '<li>{label}: {value}({percent})</li>',
      });
      chart
        .intervalStack()
        .position('percent')
        .color('label')
        .label('label*percent', function(label, percent) {
          return label;
        })
        .tooltip('label*value*percent', function(label, value, percent) {
          percent = (percent * 100).toFixed(2) + '%';
          return {
            label: label,
            value: value,
            percent: percent,
          };
        });
      chart.render();
      this.setState({ chart: chart, flag: 1 });
    }
  }
  render() {
    _this = this;
    _props = _this.props;
    _state = _this.state;
    console.log(_this.state.data);
    return (
      <div>
        {(_this.state.data != null && _this.state.data.length > 0) || _this.state.flag == 0 ? (
          <div id="teamcountChart" ref="chartbox" />
        ) : (
          <div
            style={{
              position: 'relative',
              padding: '16px 16px',
              background: '#fff',
              height: '600px',
              textAlign: 'center',
              fontSize: '14px',
              color: 'rgba(0, 0, 0, 0.45)',
              zIndex: 1,
            }}
          >
            暂无数据
          </div>
        )}
      </div>
    );
  }
}

export default IntervalStack;
