import { stringify } from 'qs';
import request from '../../utils/request';

export async function qryListByParams(params) {
  return request('/expr/qryExprList', {
    method: 'POST',
    body: params,
  });
}

export async function delExpr(params) {
  return request(`/expr/remove`, {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request(`/expr/create`, {
    method: 'POST',
    body: params,
  });
}

export async function update(params) {
  return request(`/expr/update`, {
    method: 'POST',
    body: params,
  });
}

export async function qryInfo(params) {
  return request(`/expr/qryInfo`, {
    method: 'POST',
    body: params,
  });
}
