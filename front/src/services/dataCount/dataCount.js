import request from '../../utils/request';

export async function qryKeyUnitCount(params) {
  return request(`/dataCount/qryKeyUnitCount`, {
    method: 'POST',
    body: params,
  });
}

export async function qryTeamCount(params) {
  return request(`/dataCount/qryTeamCount`, {
    method: 'POST',
    body: params,
  });
}

export async function qryAccidentCount(params) {
  return request(`/dataCount/qryAccidentCount`, {
    method: 'POST',
    body: params,
  });
}

export async function getYearList(params) {
  return request(`/dataCount/getYearList`, {
    method: 'POST',
  });
}

export async function qryPoliceNumber(params) {
  return request(`/dataCount/qryPoliceNumber`, {
    method: 'POST',
    body: params,
  });
}
