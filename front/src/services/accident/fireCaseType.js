import request from '../utils/request';

export async function qryListByParams(params) {
  return request(`/fireCaseType/qryByType`, {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request(`/fireCaseType/create`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/fireCaseType/qryById`, {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request(`/fireCaseType/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request(`/fireCaseType/remove`, {
    method: 'POST',
    body: params,
  });
}
