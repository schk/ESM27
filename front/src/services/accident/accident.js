import request from '../../utils/request';

export async function qryListByParams(params) {
  return request(`/accident/qryListByParams`, {
    method: 'POST',
    body: params,
  });
}
export async function getEditRecord(params) {
  return request(`/accident/getEditRecord`, {
    method: 'POST',
    body: params,
  });
}

export async function delRecord(params) {
  return request(`/accident/delRecord`, {
    method: 'POST',
    body: params,
  });
}

export async function getEditRecordById(params) {
  return request(`/accident/getEditRecordById`, {
    method: 'POST',
    body: params,
  });
}

export async function editRecord(params) {
  return request(`/accident/editRecord`, {
    method: 'POST',
    body: params,
  });
}

export async function getHistoryWeatherDataByRoom(params) {
  return request(`/accident/queryHistoryWeatherDataByRoom`, {
    method: 'POST',
    body: params,
  });
}

export async function getHistoryGasDataByRoom(params) {
  return request(`/accident/queryHistoryGasDataByRoom`, {
    method: 'POST',
    body: params,
  });
}

export async function getHistoryGasDataTitle(params) {
  return request(`/accident/getHistoryGasDataTitle`, {
    method: 'POST',
    body: params,
  });
}