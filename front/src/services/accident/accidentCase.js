import request from '../../utils/request';

export async function qryListByParams(params) {
  return request(`/accidentCase/qryListByParams`, {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request(`/accidentCase/create`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/accidentCase/qryById`, {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request(`/accidentCase/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request(`/accidentCase/remove`, {
    method: 'POST',
    body: params,
  });
}
