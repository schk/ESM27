import { stringify } from 'qs';
import request from '../../utils/request';

export async function qryListByParams(params) {
  return request(`/fireEngineEquipment/qryListByParams`, {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request('/fireEngineEquipment/create', {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request('/fireEngineEquipment/edit', {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request('/fireEngineEquipment/remove', {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request('/fireEngineEquipment/qryById', {
    method: 'POST',
    body: params,
  });
}
