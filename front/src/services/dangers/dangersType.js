import request from '../../utils/request';

export async function qryDangerTypeNoTop() {
  return request('/dangersType/qryDangerTypeNoTop');
}

export async function qryDangerType() {
  return request('/dangersType/qryDangerType');
}

export async function create(params) {
  return request(`/dangersType/create`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/dangersType/qryById`, {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request(`/dangersType/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request(`/dangersType/remove`, {
    method: 'POST',
    body: params,
  });
}
