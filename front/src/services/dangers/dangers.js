import { stringify } from 'qs';
import request from '../../utils/request';

export async function qryListByParams(params) {
  return request('/dangers/qryListByParams', {
    method: 'POST',
    body: params,
  });
}

export async function qryDangersByKeyParts(params) {
  return request('/dangers/qryDangersByKeyParts', {
    method: 'POST',
    body: params,
  });
}

export async function qryDangersByKeyUnit(params) {
  return request('/dangers/qryDangersByKeyUnit', {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request('/dangers/create', {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request('/dangers/edit', {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request('/dangers/remove', {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request('/dangers/qryById', {
    method: 'POST',
    body: params,
  });
}
