import request from '../../utils/request';

export async function qryDeptsNoTop() {
  return request('/sys/dept/qryDeptsNoTop');
}

export async function qryListByParams(params) {
  return request(`/sys/dept/qryDeptsNoTop`, {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request(`/sys/dept/create`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/sys/dept/qryById`, {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request(`/sys/dept/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request(`/sys/dept/remove`, {
    method: 'POST',
    body: params,
  });
}
