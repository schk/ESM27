import request from '../../utils/request';

export async function qryListByParam(params) {
  return request(`/system/log/qryListByParam`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/system/log/qryById`, {
    method: 'POST',
    body: params,
  });
}
