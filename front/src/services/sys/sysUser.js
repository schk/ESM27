import { stringify } from 'qs';
import request from '../../utils/request';

export async function qryUserByParams(params) {
  return request('/user/qryUserList', {
    method: 'POST',
    body: params,
  });
}

export async function qryUserByDept(params) {
  return request('/user/qryUserByDept', {
    method: 'POST',
    body: params,
  });
}

export async function qryAllUser() {
  return api.post('/user/qryAllUser');
}

export async function qryRoleOption() {
  return request('/system/role/qryRoleOption');
}

export async function create(params) {
  return request('/user/create', {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request('/user/edit', {
    method: 'POST',
    body: params,
  });
}

export async function remove(params) {
  return request('/user/remove', {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request('/user/qryById', {
    method: 'POST',
    body: params,
  });
}

export async function findInfo(params) {
  return request('/user/findInfo', {
    method: 'POST',
    body: params,
  });
}

export async function fakeAccountLogin(params) {
  return request('/user/login', {
    method: 'POST',
    body: params,
  });
}

export async function fakeAccountLoginOut() {
  return request('/user/logout', {
    method: 'POST',
  });
}
