import request from '../../utils/request';

export async function qryListByParams(params) {
  return request(`/system/role/qryRoleList`, {
    method: 'POST',
    body: params,
  });
}

export async function addUser(params) {
  return request(`/system/role/create`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/system/role/qryById`, {
    method: 'POST',
    body: params,
  });
}

export async function updateUser(params) {
  return request(`/system/role/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function delUser(params) {
  return request(`/system/role/remove`, {
    method: 'POST',
    body: params,
  });
}

export async function setRolePermission(params) {
  return request(`/system/permission/setRolePermission`, {
    method: 'POST',
    body: params,
  });
}
export async function getAllPermission(params) {
  return request(`/system/permission/qryPermissionList`, {
    method: 'POST',
    body: params,
  });
}

export async function qryRolePermission(params) {
  return request(`/system/permission/qryRolePermission`, {
    method: 'POST',
    body: params,
  });
}

export async function qryRolePermissionNoP(params) {
  return request(`/system/permission/qryRolePermissionNoP`, {
    method: 'POST',
    body: params,
  });
}
