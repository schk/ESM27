import { stringify } from 'qs';
import request from '../../utils/request';

export async function qryListByParams(params) {
  return request(`/extinguisherUseRecord/qryListByParams`, {
    method: 'POST',
    body: params,
  });
}

export async function qryByType(params) {
  return request(`/dictionary/qryByType`, {
    method: 'POST',
    body: 5,
  });
}

export async function qryFireBrigade(params) {
  return request(`/extinguisherUseRecord/qryFireBrigade`, {
    method: 'POST',
  });
}
