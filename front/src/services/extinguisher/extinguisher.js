import { stringify } from 'qs';
import request from '../../utils/request';

export async function qryListByParams(params) {
  return request('/extinguisher/qryListByParams', {
    method: 'POST',
    body: params,
  });
}

export async function qrySumByKeyId(params) {
  return request('/extinguisher/qrySumByKeyId', {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request('/extinguisher/create', {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request('/extinguisher/edit', {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request('/extinguisher/remove', {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request('/extinguisher/qryById', {
    method: 'POST',
    body: params,
  });
}
