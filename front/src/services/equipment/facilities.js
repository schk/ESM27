import request from '../../utils/request';

export async function qryListByParams(params) {
  return request(`/facilities/qryListByParams`, {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request(`/facilities/create`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/facilities/qryById`, {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request(`/facilities/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request(`/facilities/remove`, {
    method: 'POST',
    body: params,
  });
}

export async function importExcelConfirm(params) {
  return request('/facilities/excel/importExcelConfirm', {
    method: 'POST',
    body: params,
  });
}

export async function delExcelTemp(params) {
  return request('/facilities/excel/delExcelTemp', {
    method: 'POST',
    body: params,
  });
}
