import request from '../../utils/request';

export async function qryListByParams(params) {
  return request(`/keyParts/qryListByParams`, {
    method: 'POST',
    body: params,
  });
}

export async function qryKeyPartsList() {
  return request(`/keyParts/qryKeyPartsList`);
}

export async function create(params) {
  return request(`/keyParts/create`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/keyParts/qryById`, {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request(`/keyParts/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request(`/keyParts/remove`, {
    method: 'POST',
    body: params,
  });
}

export async function delDangers(params) {
  return request(`/keyParts/delDangers`, {
    method: 'POST',
    body: params,
  });
}

export async function saveDangersOfKeyParts(params) {
  return request(`/keyParts/saveDangersOfKeyParts`, {
    method: 'POST',
    body: params,
  });
}

export async function importExcelConfirm(params) {
  return request('/keyParts/excel/importExcelConfirm', {
    method: 'POST',
    body: params,
  });
}

export async function delExcelTemp(params) {
  return request('/keyParts/excel/delExcelTemp', {
    method: 'POST',
    body: params,
  });
}
