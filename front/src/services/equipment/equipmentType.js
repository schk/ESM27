import request from '../../utils/request';

export async function qryEquipmentTypeList(params) {
  return request(`/equipmentType/qryEquipmentTypeList`, {
    method: 'POST',
    body: params,
  });
}

export async function qryEquTypeList(params) {
  return request(`/equipmentType/qryEquTypeList`, {
    method: 'POST',
    body: params,
  });
}

export async function qryListByParams(params) {
  return request(`/equipmentType/qryListByParams`, {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request(`/equipmentType/create`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/equipmentType/qryById`, {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request(`/equipmentType/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request(`/equipmentType/remove`, {
    method: 'POST',
    body: params,
  });
}
