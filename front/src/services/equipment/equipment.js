import request from '../../utils/request';

export async function qryListByParams(params) {
  return request(`/equipment/qryListByParams`, {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request(`/equipment/create`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/equipment/qryById`, {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request(`/equipment/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request(`/equipment/remove`, {
    method: 'POST',
    body: params,
  });
}

export async function importExcelConfirm(params) {
  return request('/equipment/excel/importExcelConfirm', {
    method: 'POST',
    body: params,
  });
}

export async function delExcelTemp(params) {
  return request('/equipment/excel/delExcelTemp', {
    method: 'POST',
    body: params,
  });
}
