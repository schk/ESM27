import { stringify } from 'qs';
import request from '../../utils/request';

export async function queryList(params) {
  return request('/socialResource/qryList', {
    method: 'POST',
    body: params,
  });
}

export async function qryKeyUnitList() {
  return request('/socialResource/qryKeyUnitList');
}


export async function getInfo(params) {
  return request('/socialResource/qryById', {
    method: 'POST',
    body: params,
  });
}

export async function remove(params) {
  return request('/socialResource/remove', {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request('/socialResource/edit', {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request('/socialResource/create', {
    method: 'POST',
    body: params,
  });
}

export async function getkeyUnitName(params) {
  return request('/socialResource/getkeyUnitName', {
    method: 'POST',
    body: params,
  });
}

export async function getUnitAccident(params) {
  return request('/socialResource/qryAccidentByUnitId', {
    method: 'POST',
    body: params,
  });
}

export async function addKeyUnitAccident(params) {
  return request('/socialResource/addKeyUnitAccident', {
    method: 'POST',
    body: params,
  });
}

export async function saveDangersOfKeyUnit(params) {
  return request(`/socialResource/saveDangersOfKeyUnit`, {
    method: 'POST',
    body: params,
  });
}

export async function delDangers(params) {
  return request(`/socialResource/delDangers`, {
    method: 'POST',
    body: params,
  });
}