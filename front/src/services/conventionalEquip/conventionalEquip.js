import request from '../../utils/request';

export async function qryListByParams(params) {
  return request(`/conventionalEquip/qryListByParams`, {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request(`/conventionalEquip/create`, {
    method: 'POST',
    body: params,
  });
}

export async function getDeptName(params) {
  return request(`/conventionalEquip/getDeptNameById`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/conventionalEquip/qryById`, {
    method: 'POST',
    body: params,
  });
}
//修改
export async function edit(params) {
  return request(`/conventionalEquip/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request(`/conventionalEquip/remove`, {
    method: 'POST',
    body: params,
  });
}

export async function uploadMSDS(params) {
  return request(`/conventionalEquip/uploadMSDS`, {
    method: 'POST',
    body: params,
  });
}

export async function importConfirm(params) {
  return request('/expert/importConfirm', {
    method: 'POST',
    body: params,
  });
}

export async function delTemp(params) {
  return request('/expert/delTemp', {
    method: 'POST',
    body: params,
  });
}
