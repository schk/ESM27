import request from '../../utils/request';

export async function qryListByParams(params) {
  return request(`/specialDutyEquip/qryListByParams`, {
    method: 'POST',
    body: params,
  });
}

export async function getObject(params) {
  return request(`/specialDutyEquip/getObject`, {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request(`/specialDutyEquip/create`, {
    method: 'POST',
    body: params,
  });
}

export async function getDeptName(params) {
  return request(`/specialDutyEquip/getDeptNameById`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/specialDutyEquip/qryById`, {
    method: 'POST',
    body: params,
  });
}
//修改
export async function edit(params) {
  return request(`/specialDutyEquip/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request(`/specialDutyEquip/remove`, {
    method: 'POST',
    body: params,
  });
}

export async function uploadMSDS(params) {
  return request(`/specialDutyEquip/uploadMSDS`, {
    method: 'POST',
    body: params,
  });
}
export async function qryListByEngineId(params) {
  return request(`/specialDutyEquip/qryListByEngineId`, {
    method: 'POST',
    body: params,
  });
}

export async function qryType(params) {
  return request(`/dictionary/qryByType`, {
    method: 'POST',
    body: params,
  });
}
export async function importConfirm(params) {
  return request('/specialDutyEquip/importConfirm', {
    method: 'POST',
    body: params,
  });
}

export async function delTemp(params) {
  return request('/specialDutyEquip/delTemp', {
    method: 'POST',
    body: params,
  });
}
