import request from '../utils/request';

export async function query() {
  return request('/api/users');
}

export async function queryCurrent() {
  return request(`/user/queryCurrent`, {
    method: 'POST',
  });
}

export async function updatePassword(params) {
  return request(`/user/updatePassword`, {
    method: 'POST',
    body: params,
  });
}

export async function checkOldPassword(params) {
  return request(`/user/checkOldPassword`, {
    method: 'POST',
    body: params,
  });
}

export async function getUserBrigadeId() {
  return request(`/user/getUserBrigadeId`);
}
