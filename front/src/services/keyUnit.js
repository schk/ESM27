import { stringify } from 'qs';
import request from '../utils/request';

export async function queryList(params) {
  return request('/unit/qryList', {
    method: 'POST',
    body: params,
  });
}

export async function qryKeyUnitList() {
  return request('/unit/qryKeyUnitList');
}

export async function getkeyUnitInfo(params) {
  return request('/unit/getkeyUnitInfo', {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request('/unit/qryById', {
    method: 'POST',
    body: params,
  });
}

export async function remove(params) {
  return request('/unit/remove', {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request('/unit/edit', {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request('/unit/create', {
    method: 'POST',
    body: params,
  });
}

export async function getkeyUnitName(params) {
  return request('/unit/getkeyUnitName', {
    method: 'POST',
    body: params,
  });
}

export async function getUnitAccident(params) {
  return request('/unit/qryAccidentByUnitId', {
    method: 'POST',
    body: params,
  });
}

export async function addKeyUnitAccident(params) {
  return request('/unit/addKeyUnitAccident', {
    method: 'POST',
    body: params,
  });
}

export async function saveDangersOfKeyUnit(params) {
  return request(`/unit/saveDangersOfKeyUnit`, {
    method: 'POST',
    body: params,
  });
}

export async function delDangers(params) {
  return request(`/unit/delDangers`, {
    method: 'POST',
    body: params,
  });
}