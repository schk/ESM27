import request from '../../utils/request';

export async function qryListByParams(params) {
  return request(`/emergencyMaterial/qryListByParams`, {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request(`/emergencyMaterial/create`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/emergencyMaterial/qryById`, {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request(`/emergencyMaterial/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request(`/emergencyMaterial/remove`, {
    method: 'POST',
    body: params,
  });
}
export async function importConfirm(params) {
  return request('/emergencyMaterial/importConfirm', {
    method: 'POST',
    body: params,
  });
}

export async function delTemp(params) {
  return request('/emergencyMaterial/delTemp', {
    method: 'POST',
    body: params,
  });
}
