import request from '../../utils/request';

export async function qryListByParams(params) {
  return request(`/reservePoint/qryListByParams`, {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request(`/reservePoint/create`, {
    method: 'POST',
    body: params,
  });
}
export async function qryReservePointList() {
  return request('/reservePoint/qryReservePointList');
}

export async function qryReserveByType(params) {
  return request('/reservePoint/qryReserveByType', {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/reservePoint/qryById`, {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request(`/reservePoint/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request(`/reservePoint/remove`, {
    method: 'POST',
    body: params,
  });
}
