import request from '../../utils/request';

export async function qryListByParams(params) {
  return request('/extinguisher/qryListByParams', {
    method: 'POST',
    body: params,
  });
}
export async function create(params) {
  return request('/extinguisher/create', {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request('/extinguisher/edit', {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request('/extinguisher/remove', {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request('/extinguisher/qryById', {
    method: 'POST',
    body: params,
  });
}

export async function qryListByFireBrigade() {
  return request('/extinguisher/qryListAll');
}

export async function qryExtinguishere(params) {
  return request('/extinguisher/qryExtinguishere', {
    method: 'POST',
    body: params,
  });
}

export async function qryExtinguisherQu(params) {
  return request('/extinguisher/qryExtinguisherQu', {
    method: 'POST',
    body: params,
  });
}

export async function importExcelConfirm(params) {
  return request('/extinguisher/excel/importExcelConfirm', {
    method: 'POST',
    body: params,
  });
}

export async function delExcelTemp(params) {
  return request('/extinguisher/excel/delExcelTemp', {
    method: 'POST',
    body: params,
  });
}
