import request from '../../utils/request';

export async function qryListByParams(params) {
  return request(`/emergencySuppliesType/qryListByParams`, {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request(`/emergencySuppliesType/create`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/emergencySuppliesType/qryById`, {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request(`/emergencySuppliesType/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request(`/emergencySuppliesType/remove`, {
    method: 'POST',
    body: params,
  });
}
