import request from '../../utils/request';

export async function qryListByParams(params) {
  return request(`/teams/qryListByParams`, {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request(`/teams/create`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/teams/qryById`, {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request(`/teams/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request(`/teams/remove`, {
    method: 'POST',
    body: params,
  });
}
