import request from '../../utils/request';

export async function qryListByParams(params) {
  return request(`/fireBrigade/qryListByParams`, {
    method: 'POST',
    body: params,
  });
}

export async function qryFiBrigade() {
  return request('/fireBrigade/qryFiBrigade');
}

export async function qryFireBrigadeList() {
  return request(`/fireBrigade/qryList`);
}

export async function qryFireBrigadeTree() {
  return request(`/fireBrigade/qryFireBrigadeTree`);
}

export async function create(params) {
  return request(`/fireBrigade/create`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/fireBrigade/qryById`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfoDetail(params) {
  return request(`/fireBrigade/getInfoDetail`, {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request(`/fireBrigade/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request(`/fireBrigade/remove`, {
    method: 'POST',
    body: params,
  });
}

export async function getPlans(params) {
  return request(`/plan/qryPlansByBrigade`, {
    method: 'POST',
    body: params,
  });
}
