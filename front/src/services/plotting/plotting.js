import request from '../../utils/request';

export async function qryListByParams(params) {
  return request(`/plotting/qryListByParams`, {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request(`/plotting/create`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/plotting/qryById`, {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request(`/plotting/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request(`/plotting/remove`, {
    method: 'POST',
    body: params,
  });
}

export async function qryPlottingList(params) {
  return request(`/plotting/qryPlottingList`, {
    method: 'POST',
    body: params,
  });
}
export async function qryJhPlottingList(params) {
  return request(`/plotting/qryJhPlottingList`, {
    method: 'POST',
    body: params,
  });
}

