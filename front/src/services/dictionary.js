import request from '../utils/request';

export async function qryListByParams(params) {
  return request(`/dictionary/qryByType`, {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request(`/dictionary/create`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/dictionary/qryById`, {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request(`/dictionary/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request(`/dictionary/remove`, {
    method: 'POST',
    body: params,
  });
}
