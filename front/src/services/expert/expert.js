import { stringify } from 'qs';
import request from '../../utils/request';

export async function qryListByParams(params) {
  return request('/expert/qryListByParams', {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request('/expert/create', {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request('/expert/edit', {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request('/expert/remove', {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request('/expert/qryById', {
    method: 'POST',
    body: params,
  });
}

export async function importExpertConfirm(params) {
  return request('/expert/importExpertConfirm', {
    method: 'POST',
    body: params,
  });
}

export async function delExpertTemp(params) {
  return request('/expert/delExpertTemp', {
    method: 'POST',
    body: params,
  });
}
