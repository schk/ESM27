import request from '../../utils/request';

export async function qryListByParams() {
  return request(`/expertType/qryListByParams`);
}

export async function create(params) {
  return request(`/expertType/create`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/expertType/qryById`, {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request(`/expertType/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request(`/expertType/remove`, {
    method: 'POST',
    body: params,
  });
}
