import request from '../../utils/request';

export async function qryListByParams(params) {
  return request(`/plan/qryListByParams`, {
    method: 'POST',
    body: params,
  });
}

export async function qryListByKeyParts(params) {
  return request(`/plan/qryListByKeyParts`, {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request(`/plan/create`, {
    method: 'POST',
    body: params,
  });
}

export async function getPlansInfo(params) {
  return request(`/plan/getPlansInfo`, {
    method: 'POST',
    body: params,
  });
}
export async function getInfo(params) {
  return request(`/plan/qryById`, {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request(`/plan/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request(`/plan/remove`, {
    method: 'POST',
    body: params,
  });
}
export async function updateFreq(params) {
  return request(`/plan/updateFreq`, {
    method: 'POST',
    body: params,
  });
}

export async function importExcelConfirm(params) {
  return request('/plan/excel/importExcelConfirm', {
    method: 'POST',
    body: params,
  });
}

export async function delExcelTemp(params) {
  return request('/plan/excel/delExcelTemp', {
    method: 'POST',
    body: params,
  });
}

export async function getPlans(params) {
  return request(`/plan/qryPlansByKeyUnit`, {
    method: 'POST',
    body: params,
  });
}


export async function qryPlanByKeyParts(params) {
  return request(`/plan/qryPlansByKeyParts`, {
    method: 'POST',
    body: params,
  });
}

export async function updateKeyUnit(params) {
  return request(`/plan/updateKeyUnit`, {
    method: 'POST',
    body: params,
  });
}
export async function delKeyUnitByPlanId(params) {
  return request(`/plan/delKeyUnitByPlanId`, {
    method: 'POST',
    body: params,
  });
}

export async function qryNotAddedPlanList(params) {
  return request(`/plan/qryNotAddedPlanList`, {
    method: 'POST',
    body: params,
  });
}


