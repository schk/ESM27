import { stringify } from 'qs';
import request from '../../utils/request';

export async function qryListByParams(params) {
  return request('/fireEngine/qryListByParams', {
    method: 'POST',
    body: params,
  });
}
export async function create(params) {
  return request('/fireEngine/create', {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request('/fireEngine/edit', {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request('/fireEngine/remove', {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request('/fireEngine/qryById', {
    method: 'POST',
    body: params,
  });
}

export async function importExcelConfirm(params) {
  return request('/fireEngine/excel/importExcelConfirm', {
    method: 'POST',
    body: params,
  });
}

export async function delExcelTemp(params) {
  return request('/fireEngine/excel/delExcelTemp', {
    method: 'POST',
    body: params,
  });
}
