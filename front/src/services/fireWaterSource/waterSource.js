import request from '../../utils/request';

export async function qryByType(params) {
  return request('/dictionary/qryByType', {
    method: 'POST',
    body: params,
  });
}
export async function qryListByParams(params) {
  return request(`/fireWaterSource/qryListByParams`, {
    method: 'POST',
    body: params,
  });
}

export async function create(params) {
  return request(`/fireWaterSource/create`, {
    method: 'POST',
    body: params,
  });
}

export async function getInfo(params) {
  return request(`/fireWaterSource/qryById`, {
    method: 'POST',
    body: params,
  });
}

export async function edit(params) {
  return request(`/fireWaterSource/edit`, {
    method: 'POST',
    body: params,
  });
}

export async function del(params) {
  return request(`/fireWaterSource/remove`, {
    method: 'POST',
    body: params,
  });
}

export async function importExcelConfirm(params) {
  return request('/fireWaterSource/excel/importExcelConfirm', {
    method: 'POST',
    body: params,
  });
}

export async function delExcelTemp(params) {
  return request('/fireWaterSource/excel/delExcelTemp', {
    method: 'POST',
    body: params,
  });
}
