// use localStorage to store the authority info, which might be sent from server in actual project.
export function getAuthority() {
  if (localStorage.getItem('antd-pro-authority')) {
    return localStorage.getItem('antd-pro-authority');
  } else {
    return '';
  }
}

export function setAuthority(authority) {
  return localStorage.setItem('antd-pro-authority', authority);
}
