﻿
#ifndef _MathModelAPI_h_
#define _MathModelAPI_h_


/***************************************初始化接口*************************************************/
// 初始化
// 返回值: 1 成功, 0 失败(序列号错误)
extern "C" __declspec(dllexport) int __stdcall Init_API(void);




/*****************************************泄漏源反算***********************************************/

// 计算泄漏源的位置和泄漏率
// ws: 风速，单位m/s
// wd: 风向，单位度
// as: 大气稳定度，取值 1,2,3,4,5,6 与大气稳定度取值表的对应关系 1=A, 2=B, 3=C, 4=D, 5=E, 6=F
// data[]: 四组传感器数据，传值(q1,x1,y1,h1,......q4,x4,y4,h4) q浓度，xy传感器坐标，h传感器距离地面高度
// 返回值: true计算成功，false失败
extern "C" __declspec(dllexport) bool __stdcall LeakSource_API(const char* id, double ws, double wd, int as, const char* data);

// 获取泄漏率
// 返回值: 泄漏率，单位公斤每秒(Kg/s)
extern "C" __declspec(dllexport) double __stdcall GetLsQ_API(const char* id);

// 获取泄漏源坐标 x
// 返回值: x 坐标，单位米(m)
extern "C" __declspec(dllexport) double __stdcall GetLsX_API(const char* id);

// 获取泄漏源坐标 y
// 返回值: y 坐标，单位米(m)
extern "C" __declspec(dllexport) double __stdcall GetLsY_API(const char* id);

// 获取泄漏源高度 h
// 返回值: 高度，单位米(m)
extern "C" __declspec(dllexport) double __stdcall GetLsH_API(const char* id);




/*****************************************气体扩散高斯模型***********************************************/

// 传参数
// as: 大气稳定度，整型，取值 1,2,3,4,5,6 与大气稳定度取值表的对应关系 1=A, 2=B, 3=C, 4=D, 5=E, 6=F
// ws: 环境风速，一般取地面 10m 高处的平均风速，单位m/s
// wd: 风向，单位度
// gd: 污染气体在常温下的浓度，单位 kg/m3
// h: 泄漏点离地面的高度
// dq: 泄漏源强度，kg/s
// 返回值: true成功，false失败
extern "C" __declspec(dllexport) bool __stdcall SetGaussionModelParameters_API(const char* id, int as, double ws, double wd, double gd, double h, double dq);

// 设置积分时间间隔，也可以不设置此参数，程序内默认是 1 秒，间隔越短计算次数越多耗时越长，间隔越长计算次数越少耗时越少。
// dt: 积分时间间隔，单位秒s
// 返回值: true成功，false失败
extern "C" __declspec(dllexport) bool __stdcall SetDeltaT_API(const char* id, double dt);

// 开始模拟，计算 fCurrentTime 时间点的范围轮廓线
// fCurrentTime: 扩散时间
// 返回值 true 计算成功，false 计算失败
extern "C" __declspec(dllexport) bool __stdcall Simulate_API(const char* id, double fCurrentTime);

// 开始模拟，计算 fCurrentTime 时间点的范围轮廓线，沿着风向的轴线垂直面上的轮廓线
// fCurrentTime: 扩散时间
// 返回值 true 计算成功，false 计算失败
extern "C" __declspec(dllexport) bool __stdcall Simulate2_API(const char* id, double fCurrentTime);

// 设置浓度阈值，计算此浓度范围轮廓线，单位kg/m3
// t: 等值线浓度
extern "C" __declspec(dllexport) void __stdcall SetThreshold_API(const char* id, double t);

// 设置浓度阈值，计算此浓度范围轮廓线，单位kg/m3，沿着风向的轴线垂直面上的轮廓线
// t: 等值线浓度
extern "C" __declspec(dllexport) void __stdcall SetThreshold2_API(const char* id, double t);

// 获取轮廓线顶点坐标数组的长度
extern "C" __declspec(dllexport) int __stdcall GetVerticesLength_API(const char* id);

// 获取轮廓线顶点坐标数组[x1,y1,z1,x2,y2,z2,......]
extern "C" __declspec(dllexport) void __stdcall GetVertices_API(const char* id, double va[]);

// 获取等值线上的最远距离
extern "C" __declspec(dllexport) double __stdcall GetDistance_API(const char* id);




/*****************************************颗粒物沉降模型***********************************************/

// 传参数
// as: 大气稳定度，整型，取值 1,2,3,4,5,6 与大气稳定度取值表的对应关系 1=A, 2=B, 3=C, 4=D, 5=E, 6=F
// ws: 环境风速，一般取地面 10m 高处的平均风速，单位m/s
// wd: 风向，单位度
// h: 烟雾泄漏点离地面的高度
// dq: 颗粒物源强度，kg/s
// 返回值: true成功，false失败
extern "C" __declspec(dllexport) bool __stdcall SetPsParameters_API(const char* id, int as, double ws, double wd, double h, double dq);

// 设置积分时间间隔，也可以不设置此参数，程序内默认是 1 秒，间隔越短计算次数越多耗时越长，间隔越长计算次数越少耗时越少。
// dt: 积分时间间隔，单位秒s
// 返回值: true成功，false失败
extern "C" __declspec(dllexport) bool __stdcall SetPsDeltaT_API(const char* id, double dt);

// 开始模拟，计算 fCurrentTime 时间点的范围轮廓线
// fCurrentTime: 扩散时间
// 返回值 true 计算成功，false 计算失败
extern "C" __declspec(dllexport) bool __stdcall SimulatePs_API(const char* id, double fCurrentTime);

// 开始模拟，计算 fCurrentTime 时间点的范围轮廓线，沿着风向的轴线垂直面上的轮廓线（计算沉降率，而不是沉降质量）
// fCurrentTime: 扩散时间
// 返回值 true 计算成功，false 计算失败
extern "C" __declspec(dllexport) bool __stdcall SimulatePs2_API(const char* id, double fCurrentTime);

// 设置沉降质量阈值，计算此沉降质量范围轮廓线，单位kg/m3
// t: 等值线
extern "C" __declspec(dllexport) void __stdcall SetPsThreshold_API(const char* id, double t);

// 设置沉降质量阈值，计算此沉降质量范围轮廓线，单位kg/m3（沿着风向的轴线垂直面上的轮廓线）
// t: 等值线
extern "C" __declspec(dllexport) void __stdcall SetPsThreshold2_API(const char* id, double t);

// 获取轮廓线顶点坐标数组的长度
extern "C" __declspec(dllexport) int __stdcall GetPsVerticesLength_API(const char* id);

// 获取轮廓线顶点坐标数组[x1,y1,z1,x2,y2,z2......]
extern "C" __declspec(dllexport) void __stdcall GetPsVertices_API(const char* id, double va[]);




/***************************************爆炸模型*************************************************/

// 计算爆炸伤害
// type: 爆炸类型(1-3是物理爆炸, 4是化学爆炸), 1 压缩气体容器爆炸, 2 高压液体容器爆炸, 3 过热液体容器爆炸, 4 蒸气云爆炸
// idx: 爆炸物索引
//       1 压缩气体容器爆炸:空气(1),氮(2),氧(3),氢(4),甲烷(5),乙烷(6),乙烯(7),丙烷(8),一氧化碳(9),二氧化碳(10),一氧化氮(11),二氧化氮(12),氨气(13),氯气(14),过热蒸气(15),干饱和蒸气(16),氢氯酸(17)
//       2 高压液体容器爆炸:无
//       3 过热液体容器爆炸:高温饱和水(1)
//       4 蒸气云爆炸:氢气(1),氨气(2),苯(3),一氧化碳(4),硫化氢1(生成SO2)(5),硫化氢2(生成SO3)(6),甲烷(7),乙烷(8),乙烯(9),乙炔(10),丙烷(11),丙烯(12),正丁烷(13),异丁烷(14),丁烯(15)
// pressure: 爆炸物的压力, 单位兆帕(Mpa)
// volume: 爆炸物体积, 单位立方米(m³)
// r: 距离爆炸点的距离
// 返回值: 伤害值(查表获得伤害描述)
extern "C" __declspec(dllexport) double __stdcall CalcExplosionHurt_API(const char* id, int type, int idx, double pressure, double volume, double r);

// 计算爆炸伤害半径
// type: 爆炸类型(1-3是物理爆炸, 4是化学爆炸), 1 压缩气体容器爆炸, 2 高压液体容器爆炸, 3 过热液体容器爆炸, 4 蒸气云爆炸
// idx: 爆炸物索引
//       1 压缩气体容器爆炸:空气(1),氮(2),氧(3),氢(4),甲烷(5),乙烷(6),乙烯(7),丙烷(8),一氧化碳(9),二氧化碳(10),一氧化氮(11),二氧化氮(12),氨气(13),氯气(14),过热蒸气(15),干饱和蒸气(16),氢氯酸(17)
//       2 高压液体容器爆炸:无
//       3 过热液体容器爆炸:高温饱和水(1)
//       4 蒸气云爆炸:氢气(1),氨气(2),苯(3),一氧化碳(4),硫化氢1(生成SO2)(5),硫化氢2(生成SO3)(6),甲烷(7),乙烷(8),乙烯(9),乙炔(10),丙烷(11),丙烯(12),正丁烷(13),异丁烷(14),丁烯(15)
// pressure: 爆炸物的压力, 单位兆帕(Mpa)
// volume: 爆炸物体积, 单位立方米(m³)
// hurt: 伤害值
// 返回值: 能够造成此伤害值的半径范围
extern "C" __declspec(dllexport) double __stdcall CalcExplosionHurtR_API(const char* id, int type, int idx, double pressure, double volume, double hurt);




/*****************************************热辐射模型***********************************************/

// 传参数（计算热辐射伤害），后 4 个参数查数据库
// S: 火灾面积，单位平方米(m2)
// T0: 环境温度，单位开氏度(K)
// Hc: 物质的燃烧热，单位焦耳每公斤(J/Kg)
// Hv: 物质的蒸发热，单位焦耳每公斤(J/Kg)
// cp: 物质的定压比热容，单位焦耳(每公斤每华氏度)(J/(Kg*K))
// Tb: 物质的沸点，单位开氏度(K)
// 返回值: true成功，false失败
extern "C" __declspec(dllexport) bool __stdcall SetHeatHurtParameters_API(const char* id, double S, double T0, double Hc, double Hv, double cp, double Tb);

// 传参数（计算热辐射伤害），主要用来计算石油、煤油、汽油等混合物燃烧热辐射伤害
// S: 火灾面积，单位平方米(m2)
// Hc: 物质的燃烧热，单位焦耳每公斤(J/Kg)
// mf: 燃烧速率（kg•m-2•s-1），石油最大燃烧速率 0.0781
// 返回值: true成功，false失败
extern "C" __declspec(dllexport) bool __stdcall SetHeatHurtMf_API(const char* id, double S, double mf, double Hc);

// 计算热辐射伤害
// length: 距离着火中心点距离，单位米(m)
// time: 暴露在热辐射范围内的时间，单位秒(m)
// 返回值: 目标地点接收到的热通量密度，单位千瓦每平方米（kW/m2）
extern "C" __declspec(dllexport) double __stdcall CalcHeatHurt_API(const char* id, double length, double time);

// 获取死亡概率（有衣服保护，20%皮肤裸露）
// 返回值: 死亡概率，概率单位为5时对应伤亡百分数50%
extern "C" __declspec(dllexport) double __stdcall GetHeatHurtPr1_API(const char* id);

// 获取二级烧伤概率（有衣服保护，20%皮肤裸露）
// 返回值: 二级烧伤概率，概率单位为5时对应伤亡百分数50%
extern "C" __declspec(dllexport) double __stdcall GetHeatHurtPr2_API(const char* id);

// 获取一级烧伤概率（有衣服保护，20%皮肤裸露）
// 返回值: 一级烧伤概率，概率单位为5时对应伤亡百分数50%
extern "C" __declspec(dllexport) double __stdcall GetHeatHurtPr3_API(const char* id);

// 计算热辐射伤害半径
// time: 暴露在热辐射范围内的时间
extern "C" __declspec(dllexport) void __stdcall CalcHeatHurtR_API(const char* id, double time);

// 获取死亡半径（概率单位大于等于5）
// 返回值: 死亡半径，单位米(m)
extern "C" __declspec(dllexport) double __stdcall GetHeatHurtR1_API(const char* id);

// 获取二级烧伤半径（概率单位大于等于5）
// 返回值: 二级烧伤半径，单位米(m)
extern "C" __declspec(dllexport) double __stdcall GetHeatHurtR2_API(const char* id);

// 获取一级烧伤半径（概率单位大于等于5）
// 返回值: 一级烧伤半径，单位米(m)
extern "C" __declspec(dllexport) double __stdcall GetHeatHurtR3_API(const char* id);

// 获取有风的情况下的热辐射范围
// ws: 风速
// wd: 风向
// d: 罐直径
// 返回值: 范围线的数组长度
extern "C" __declspec(dllexport) int __stdcall CalcHrLine_API(const char* id, double ws, double wd, double d);

// 获取轮廓线顶点坐标数组[x1,y1,z1,x2,y2,z2,......]
extern "C" __declspec(dllexport) void __stdcall GetHrVertices1_API(const char* id, double va[]);
extern "C" __declspec(dllexport) void __stdcall GetHrVertices2_API(const char* id, double va[]);
extern "C" __declspec(dllexport) void __stdcall GetHrVertices3_API(const char* id, double va[]);



/*****************************************液体泄漏模型***********************************************/

// 计算液体泄漏率
// rou: 液体密度（kg/m3）
// s: 泄露孔面积（m2）
// p: 容器内压强（Pa）
// h: h液压高度（液体最高点与泄漏点之间的高度差）（m）
// 返回值: 泄漏速率（kg/s）
extern "C" __declspec(dllexport) double __stdcall CalcLiquidLeak_API(const char* id, double rou, double s, double p, double h);

// 计算液体泄漏（瞬时泄露）后形成的液池半径
// m: 泄漏总量（kg）
// t: 泄漏时间（s）
// rou: 液体密度（kg/m3）
// 返回值: 液池半径（m）
extern "C" __declspec(dllexport) double __stdcall CalcLiquidLeakArea1_API(const char* id, double m, double t, double rou);

// 计算液体泄漏（持续泄露）后形成的液池半径
// m: 泄漏总量（kg）
// t: 泄漏时间（s）
// rou: 液体密度（kg/m3）
// 返回值: 液池半径（m）
extern "C" __declspec(dllexport) double __stdcall CalcLiquidLeakArea2_API(const char* id, double m, double t, double rou);




/*****************************************蒸发模型***********************************************/

// 计算闪蒸，蒸发速率
// Cp:定压比热（J•kg-1•K-1）
// Hv: 蒸发热（J/kg）
// T0: 温度（开氏度K）
// Tb: 沸点（开氏度K）
// m: 泄漏质量（kg）
// t: 闪蒸时间（s）
// 返回值: 闪蒸速率（kg/s）
extern "C" __declspec(dllexport) double __stdcall CalcEvaporate1_API(const char* id, double Cp, double Hv, double T0, double Tb, double m, double t);

// 计算热量蒸发，蒸发速率
// A1: 液池面积（m2）
// T0: 环境温度（开氏度K）
// Tb: 液体沸点（开氏度K）
// Hv: 蒸发热（J/kg）
// L: 液池长度（m）
// K: 地面导热系数（J•m-1•K-1）		普通混凝土的导热系数是：1.28 瓦/米·度（W/(m·℃)）。
// a: 热扩散系数（m2/s）
// t: 蒸发时间（s）
// Nu: 努塞尔数
// 返回值: 蒸发速率（kg/s）
extern "C" __declspec(dllexport) double __stdcall CalcEvaporate2_API(const char* id, double A1, double T0, double Tb, double Hv, double L, double K, double a, double t, double Nu);

// 计算质量蒸发，蒸发速率
// alpha: 分子扩散系数（m2/s）
// Sh: 舍伍德数
// A: 液池面积（m2）
// L: 液池长度（m）
// rou: 液体密度（kg/m3）
// 返回值: 蒸发速率（kg/s）
extern "C" __declspec(dllexport) double __stdcall CalcEvaporate3_API(const char* id, double alpha, double Sh, double A, double L, double rou);




/*****************************************石油燃烧物分析***********************************************/

// 计算石油燃烧物中污染物的产生率
// speed: 风速 m/s
// mt: 油品燃烧速度 kg/s 或 g/s
// Cc: 石油中含碳的质量百分比
// Cs: 石油中含硫的质量百分比
// Cn: 石油中含氮的质量百分比
extern "C" __declspec(dllexport) void __stdcall CalcEmissionRate_API(const char* id, double speed, double mt, double Cc, double Cs, double Cn);

// 获取 CO 产生率
// 返回值: CO 产生率(单位与 CalcEmission_API 中 mt 的单位一致，同为 kg/s 或 g/s)
extern "C" __declspec(dllexport) double __stdcall GetCO_API(const char* id);

// 获取 CO2 产生率
// 返回值: CO2 产生率(单位与 CalcEmission_API 中 mt 的单位一致，同为 kg/s 或 g/s
extern "C" __declspec(dllexport) double __stdcall GetCO2_API(const char* id);

// 获取 SO2 产生率
// 返回值: SO2 产生率(单位与 CalcEmission_API 中 mt 的单位一致，同为 kg/s 或 g/s
extern "C" __declspec(dllexport) double __stdcall GetSO2_API(const char* id);

// 获取 NO 产生率
// 返回值: NO 产生率(单位与 CalcEmission_API 中 mt 的单位一致，同为 kg/s 或 g/s
extern "C" __declspec(dllexport) double __stdcall GetNO_API(const char* id);

// 获取 NO2 产生率
// 返回值: NO2 产生率(单位与 CalcEmission_API 中 mt 的单位一致，同为 kg/s 或 g/s
extern "C" __declspec(dllexport) double __stdcall GetNO2_API(const char* id);




/*****************************************重质油品喷溅范围***********************************************/

// 计算重质油品喷溅范围
// ws: 风速 m/s
// wd: 风向
// r: 油罐半径
// va[]: 轮廓线顶点坐标数组[x1,y1,x2,y2,......], 数组长度 80
extern "C" __declspec(dllexport) void __stdcall GetPJRegion_API(const char* id, double ws, double wd, double r, double va[]);




/*****************************************火灾蔓延范围和速度***********************************************/

// 计算火灾蔓延范围和速度
// ws: 风速 m/s
// wd: 风向
// t: 蔓延时间 min
// va[]: 轮廓线顶点坐标数组[x1,y1,x2,y2,......], 数组长度 252
// sa[]: 火灾蔓延速度, 数组长度
//extern "C" __declspec(dllexport) void __stdcall GetFireSpreadRegion_API(double ws, double wd, double t, double va[], double sa[]);
extern "C" __declspec(dllexport) void __stdcall GetFireSpreadRegion_API(const char* id, double ws, double wd, double t, double va[], double sa[]);





#endif _MathModelAPI_h_
