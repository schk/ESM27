package cn.stronglink.core.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cn.stronglink.core.util.WebUtil;
 
public class MobileInterceptor extends HandlerInterceptorAdapter {
	
	//在控制器执行前调用
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		String requestUrl =request.getContextPath(); 
	    if(WebUtil.getCurrentUser() == null) {
			response.sendRedirect(requestUrl+"/api/mobile/index.jhtml");
			return false;
		}
		return true;
	}
}
