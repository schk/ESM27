package cn.stronglink.core.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;


public class CrossInterceptor implements HandlerInterceptor {

	private static final Logger logger = LoggerFactory.getLogger(CrossInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
//		   logger.info(request.getHeader("Referer")+"%%%%%%%%%%%%%%%%%%%%%");
		   //logger.info(request.getHeader("Origin")+"**********************");
			String domain = request.getHeader("Origin");
        	response.setHeader("Access-Control-Allow-Origin", domain);
    		response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With,X_Requested_With, Content-Type, Accept,X-Auth-Token,authorization");
    		response.setHeader("Access-Control-Allow-Methods","OPTIONS,GET,POST");
    		response.setHeader("Access-Control-Allow-Credentials","true");
    		response.setHeader("Allow", "OPTIONS,GET,POST");
    		return true;
       
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		
		
	}

}
