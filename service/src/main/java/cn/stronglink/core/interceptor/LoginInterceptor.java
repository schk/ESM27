package cn.stronglink.core.interceptor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cn.stronglink.core.support.HttpCode;
import cn.stronglink.core.util.ContextUtils;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.SysSession;
import cn.stronglink.esm27.web.sysSession.service.SysSessionService;
 
public class LoginInterceptor extends HandlerInterceptorAdapter {
	
	private SysSessionService sysSessionService = (SysSessionService) ContextUtils.getBean("sysSessionService");
	
	//在控制器执行前调用
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
	
		String requestType = request.getHeader("X-Requested-With");
		if("XMLHttpRequest".equals(requestType)){
		    if(WebUtil.getCurrentUser() == null) {
		    	response.setStatus(401);
				response.getWriter().write("");
				return false;
			}
		    /*ServletContext application = request.getServletContext();
			List<String> loginUser=(List<String>)application.getAttribute("loginUser");
			if (loginUser!=null && loginUser.size()>0) {
				String sessionId = request.getSession().getId();
				int a = 0;
				List<String> newLoginUser = new ArrayList<String>();
				for (String ipuser : loginUser) {
					String[] result = ipuser.split("_");
					if (result[2].equals(sessionId)) {
						a++;
						Date d = new Date();
						long currtime = d.getTime();
						ipuser = result[0]+"_"+result[1]+"_"+sessionId+"_"+currtime;
						newLoginUser.add(ipuser);
					}else {
						newLoginUser.add(ipuser);
					}
				}
				if (a==0) {
					Subject currentUser = SecurityUtils.getSubject(); 
					currentUser.logout();
					response.setStatus(401);
					response.getWriter().write("");
					return false;
				}
				application.setAttribute("loginUser", newLoginUser);
			}*/
		}else{
		    String requestUrl =request.getContextPath(); 
		    if(WebUtil.getCurrentUser() == null) {
				response.sendRedirect(requestUrl+"/webApi/login.jhtml");
				return false;
			}
		}
		
		HttpSession session = request.getSession();
		if(session.getAttribute("isLoadSession")==null
				||"".equals(session.getAttribute("isLoadSession"))
				||"0".equals(session.getAttribute("isLoadSession").toString())) {
			SysSession sysSession=sysSessionService.selectSessionByUserId(WebUtil.getCurrentUser(),request.getSession().getId());
			if(sysSession!=null) {
				//删除上一条session数据
				sysSessionService.delUserBySessionId(sysSession.getSessionId());
			}		
			session.setAttribute("sysSession", sysSession);
			session.setAttribute("isLoadSession", "1");
		}
		return true;
	}
}
