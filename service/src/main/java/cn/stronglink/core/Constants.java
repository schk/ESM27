package cn.stronglink.core;

import java.util.HashMap;

/**
 * 常量类
 * @author lei_w
 *
 */
public interface Constants {
	/** 系统异常提示  **/
	public static final String Exception_Head = "SOME ERRORS OCCURED! AS FOLLOWS.";
	/** 操作名称 */
    public static final String OPERATION_NAME = "OPERATION_NAME";
    /** 客户端语言 */
    public static final String USERLANGUAGE = "userLanguage";
    /** 客户端主题 */
    public static final String WEBTHEME = "webTheme";
    /** 当前用户 */
    public static final String CURRENT_USER = "CURRENT_USER";
    /** 当前用户姓名 */
    public static final String CURRENT_USER_NAME = "CURRENT_USER_NAME";
    /** 在线用户数量 */
    public static final String ALLUSER_NUMBER = "ALLUSER_NUMBER";
    /** 登录用户数量 */
    public static final String USER_NUMBER = "USER_NUMBER";
    /** 上次请求地址 */
    public static final String PREREQUEST = "PREREQUEST";
    /** 上次请求时间 */
    public static final String PREREQUEST_TIME = "PREREQUEST_TIME";
    /** 登录地址 */
    public static final String LOGIN_URL = "/login.html";
    /** 非法请求次数 */
    public static final String MALICIOUS_REQUEST_TIMES = "MALICIOUS_REQUEST_TIMES";
    /** 缓存命名空间 */
    public static final String CACHE_NAMESPACE = "suite:";
    /** 缓存命名空间 */
    public static HashMap<String,Object> GAS_CONTENT =null;
    
    /** 日志表状态 */
    public interface JOBSTATE {
        /**
         * 日志表状态，初始状态，插入
         */
        public static final String INIT_STATS = "I";
        /**
         * 日志表状态，成功
         */
        public static final String SUCCESS_STATS = "S";
        /**
         * 日志表状态，失败
         */
        public static final String ERROR_STATS = "E";
        /**
         * 日志表状态，未执行
         */
        public static final String UN_STATS = "N";
    }
	
  
    
 public static enum SpecialDutyEquipType{
    	
 	    SPECIALIZED("1","专勤器材"),
 	    NORMAL("3","常规器材");

    	
    	private final String typeId;
    	private final String typeName;
    	
    	
    	
        private SpecialDutyEquipType(String typeId, String typeName) {
			this.typeId = typeId;
			this.typeName = typeName;
		}



		public String getTypeId() {
			return typeId;
		}



		public String getTypeName() {
			return typeName;
		}



		public static SpecialDutyEquipType codeOf(String code){
            for(SpecialDutyEquipType assetStatus : values()){
                if(assetStatus.getTypeId() .equals(code) ){
                    return assetStatus;
                }
            }
            throw new RuntimeException("么有找到对应的枚举");
        }
    }
}
