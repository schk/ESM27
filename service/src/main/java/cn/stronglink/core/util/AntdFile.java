package cn.stronglink.core.util;

public class AntdFile {
	private Long fid;
	private String url;
	private String name;
	private Long uid;
	private String swfUrl;
	
	public String getSwfUrl() {
		return swfUrl;
	}
	public void setSwfUrl(String swfUrl) {
		this.swfUrl = swfUrl;
	}
	public Long getFid() {
		return fid;
	}
	public void setFid(Long fid) {
		this.fid = fid;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getUid() {
		return uid;
	}
	public void setUid(Long uid) {
		this.uid = uid;
	}
}
