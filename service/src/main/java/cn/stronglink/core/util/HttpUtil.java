package cn.stronglink.core.util;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSON;

public class HttpUtil {

	public static String postHttp(String url, Map<String, Object> map) {
		CloseableHttpClient httpclient = null;
		CloseableHttpResponse response = null;
		try {
			httpclient = HttpClients.createDefault();
			RequestBuilder builder = RequestBuilder.post().setUri(new URI(url));
			// if (map != null) {
			// for (Map.Entry<String, String> entry : map.entrySet()) {
			// String key = entry.getKey();
			// builder.addParameter(key, entry.getValue());
			// }
			// }
			if (map != null) {
				builder.setEntity(new StringEntity(JSON.toJSONString(map)));
			}
			builder.setHeader("Content-Type", "application/json;charset=UTF-8");
			HttpUriRequest sync = builder.build();
			response = httpclient.execute(sync);
			HttpEntity result = response.getEntity();
			System.out.println("sync form get: " + response.getStatusLine());
			String msg = EntityUtils.toString(result);
			return msg;
		} catch (Exception ex) {
			return "";
		} finally {
			try {
				if (response != null) {
					response.close();
				}
				if (httpclient != null) {
					httpclient.close();
				}

			} catch (IOException e) {

			}
		}
	}

	public static void main(String[] args) throws Exception {
		CloseableHttpClient httpclient = HttpClients.createDefault();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userId", "1");
		map.put("roomId", "1");
		map.put("serialPortName", "COM4");
		map.put("autoPushSensorDataIntervalSeconds", 5);

		HttpUriRequest sync = RequestBuilder.post().setUri(new URI("http://127.0.0.1:8000/OpenSerialPort"))
				// .addParameter("userId","1")
				// .addParameter("roomId","1")
				.setHeader("Content-Type", "application/json;charset=UTF-8")
				.setEntity(new StringEntity(JSON.toJSONString(map))).build();
		CloseableHttpResponse response = httpclient.execute(sync);
		try {
			HttpEntity result = response.getEntity();
			System.out.println("sync form get: " + response.getStatusLine());
			// EntityUtils.consume(entity);
			String msg = EntityUtils.toString(result);
			System.out.println(msg);
		} finally {
			response.close();
			httpclient.close();
		}

	}

}
