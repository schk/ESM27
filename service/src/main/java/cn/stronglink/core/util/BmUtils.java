package cn.stronglink.core.util;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;

public class BmUtils {
	public static DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	public static DateFormat dft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	

	public static String getCellValue(Cell cell) {
		if (null == cell) {
			return "";
		}

		switch (cell.getCellType()) {
			   case HSSFCell.CELL_TYPE_STRING: {
			       return cell.getRichStringCellValue().toString().trim();
			   }
			   case HSSFCell.CELL_TYPE_NUMERIC: {
				   if(HSSFDateUtil.isCellDateFormatted(cell)){
					   SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd"); 
				          return sdf.format(HSSFDateUtil.getJavaDate(cell.getNumericCellValue()).getTime());
				       }else{
				          return new BigDecimal(cell.getNumericCellValue()).toString().trim();
				       }   
			   }
			   case HSSFCell.CELL_TYPE_BOOLEAN: {
			        return String.valueOf(cell.getBooleanCellValue()).trim();
			   }
			   case HSSFCell.CELL_TYPE_FORMULA: {
				   try {
					   return String.valueOf(cell.getNumericCellValue());
				   }catch (IllegalStateException e) {
					   return String.valueOf(cell.getRichStringCellValue());
				   }
			   }
			   
			   default: {
			        return cell.getRichStringCellValue().toString().trim();
			   }
	   }
	}
	
	public static int[] checkIp(String ip){
		if (ip==null||"".equals(ip))
			return null;
		String ipArr[] = ip.split("\\.");
		if (ipArr.length!=4)
			return null;
	    
		int[] ips = new int[4];
		for (int i=0;i<4;i++){
			int k = Integer.parseInt(ipArr[i]);
			if (k<0||k>255){
				return null;
			}
			if (i==0&&(k==255||k==0)){
				return null;
			}
			if (i==3&&k==0){
				return null;
			}
			ips[3-i]=k;
		}
		return ips;
	}
	
	public static boolean compareString(String s,String t){
		if ((s==null||"".equals(s))&&(t==null||"".equals(t))){
			return true;
		}
		if (s!=null&&s.equals(t)){
			return true;
		}
		return false;
	}
	
	public static boolean compareDate(Date s,Date t){
		if (s==null&&t==null){
			return true;
		}
		if (s.getTime()==t.getTime()){
			return true;
		}
		return false;
	}
}

