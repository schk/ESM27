package cn.stronglink.core.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import cn.stronglink.core.exception.BusinessException;

public class PathUtil {
	
	public static String getClassPath() {

		String xmlpath = "";
		try {
			xmlpath = URLDecoder.decode(PathUtil.class.getClassLoader().getResource("").getPath(),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new BusinessException("数据编码错误");
		}
		xmlpath = xmlpath.replaceFirst("/", "").replace("/", "\\");
		return xmlpath;
	}
}
