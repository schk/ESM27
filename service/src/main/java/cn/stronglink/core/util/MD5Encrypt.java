package cn.stronglink.core.util;

import java.util.concurrent.atomic.AtomicLong;


public class MD5Encrypt {
	private static final String salt = "@8)D*l#^fR?";
	
	static AtomicLong  id = new AtomicLong(0);
	
	public static String encrypt(String value) {
		return byte2hex(value);
	}

	private static String byte2hex(String value) {
		try {
			value += salt;
			java.security.MessageDigest alg = java.security.MessageDigest
					.getInstance("MD5");
			alg.update(value.getBytes());
			byte[] b = alg.digest();
			String hs = "";
			String stmp = "";
			for (int n = 0; n < b.length; n++) {
				stmp = (java.lang.Integer.toHexString(b[n] & 0XFF));
				if (stmp.length() == 1)
					hs = hs + "0" + stmp;
				else
					hs = hs + stmp;
			}
			return hs.toUpperCase();
		} catch (java.security.NoSuchAlgorithmException ex) {
			return "";
		}
	}

	public static void main(String[] args) {
		System.out.println(MD5Encrypt.encrypt("123456"));
	} 

}
