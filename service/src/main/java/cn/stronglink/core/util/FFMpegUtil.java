package cn.stronglink.core.util;

import java.io.IOException;
import java.util.List;

import cc.eguid.FFmpegCommandManager.FFmpegManager;
import cc.eguid.FFmpegCommandManager.FFmpegManagerImpl;
import cn.stronglink.esm27.entity.CameraSet;

public class FFMpegUtil {
	
	private static FFmpegManager manager=new FFmpegManagerImpl(10);

	public static void rtspPush(List<CameraSet> cameras, String steamserver) throws Exception {

//		Runtime.getRuntime().exec("TASKKILL /F /IM ffmpeg.exe");
		manager.stopAll();
		Thread.sleep(2000);
//		for(CameraSet cs : cameras) {
//			String cmd = "D:\\ffmpeg\\ffmpeg.exe -i rtsp://"+ cs.getIp() +"/h264/ch1/sub/av_stream -f flv -r 25 -s 640x480 -an rtmp://"+ "127.0.0.1:1935" +"/hls/" + Base64EncryptUtil.encrypt(cs.getIp());
//			 Thread thread = new FFMpegThread(cmd);
//		     thread.start();
//		}
//		return true;
		
		for(CameraSet cs : cameras) {
			manager.start(Base64EncryptUtil.encrypt(cs.getIp()), "d:/ffmpeg/ffmpeg -i rtsp://"+ cs.getIp() + "/h264/ch1/sub/av_stream -f flv -r 25 -s 640x480 -an rtmp://"+ steamserver +"/hls/" + Base64EncryptUtil.encrypt(cs.getIp()),true);
		}
	}
	
	public static void main(String[] args) throws Exception {   
		FFmpegManager manager=new FFmpegManagerImpl(10); 
		manager.start("1", "d:/ffmpeg/ffmpeg -i rtsp://192.168.2.188/h264/ch1/sub/av_stream -f flv -r 25 -s 640x480 -an rtmp://127.0.0.1:1935/hls/1",true);
		
	}

}


