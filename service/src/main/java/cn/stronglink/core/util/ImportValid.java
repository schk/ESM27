package cn.stronglink.core.util;

import java.text.SimpleDateFormat;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.web.multipart.MultipartFile;

public final class ImportValid {

	private ImportValid() {
		
	}
	
	/**
	 * 校验文件类型是否为excel
	 * @param file
	 * @return
	 */
	public static boolean validXls(MultipartFile file) {
		String fileType = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1, file.getOriginalFilename().length());
		if(!"xls".equals(fileType) && !"xlsx".equals(fileType)) {
			return false;
		}
		
		return true;
	}
	
	@SuppressWarnings("deprecation")
	public static boolean isRowEmpty(Row row) {
		if (row==null) {
			return true;
		}else {
			for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
			       Cell cell = row.getCell(c);
			       if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK)
			           return false;
			   }
			   return true;
		}
	}
	
	public static boolean isValidDate(String datePattern,String str) {
	      boolean convertSuccess=true;
	       SimpleDateFormat format = new SimpleDateFormat(datePattern);
	       try {
	          format.setLenient(false);
	          format.parse(str);
	       } catch (Exception e) {
	           convertSuccess=false;
	       } 
	       return convertSuccess;
	}
	
	public static boolean isNotEmpty(String obj) {
        return obj != null && !"".equals(obj.trim());
    }
	
	public static boolean isEmpty(String obj) {
        return obj == null || "".equals(obj);
    }
	
	public static boolean isInt(String obj) {
		try {
			Integer.parseInt(obj);
		} catch(Exception e) {
			return false;
		}
		
		return true;
	}
}
