package cn.stronglink.core.util;


public class UploadEntity {
	private String fid;
	private String swfUrl;
	private String url;
	private String name;
	private Long uid;
	private Boolean success;

	public String getFid() {
		return fid;
	}
	public void setFid(String fid) {
		this.fid = fid;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getUid() {
		return uid;
	}
	public void setUid(Long uid) {
		this.uid = uid;
	}
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	public String getSwfUrl() {
		return swfUrl;
	}
	public void setSwfUrl(String swfUrl) {
		this.swfUrl = swfUrl;
	}
	
	

}

