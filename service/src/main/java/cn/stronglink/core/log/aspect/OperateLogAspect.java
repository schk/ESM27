package cn.stronglink.core.log.aspect;

import java.lang.reflect.Method;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.SysOperateLog;
import cn.stronglink.esm27.entity.User;
import cn.stronglink.esm27.module.system.log.service.SysLogService;
import cn.stronglink.esm27.module.system.user.service.UserService;


@Aspect
@Component
public class OperateLogAspect {

	@Autowired
	private SysLogService sysLogService;
	@Autowired
	private UserService userService;

	private static final Logger logger = LoggerFactory.getLogger(OperateLogAspect.class);	   
	    /**
	     * 定义一个切入点.
	     * 解释下：
	     *
	     * ~ 第一个 * 代表任意修饰符及任意返回值.
	     * ~ 第二个 * 任意包名
	     * ~ 第三个 * 代表任意方法.
	     * ~ 第四个 * 定义在web包或者子包
	     * ~ 第五个 * 任意方法
	     * ~ .. 匹配任意数量的参数.
	     */
	   //  @Pointcut("execution(public * cn.stronglink.**.controller..*.*(..))")
	     @Pointcut(value = "@annotation(cn.stronglink.core.log.annotation.OperateLog)")  
	     public void handleLog(){}
	     
	     @Before("handleLog()")
	     public void deBefore(JoinPoint joinPoint){
	    	 // 接收到请求，记录请求内容
	         logger.info("WebLogAspect.doBefore()");
	     }
	     
	     @AfterReturning("handleLog()")
	     public void  doAfterReturning(JoinPoint joinPoint){
	       // 处理完请求，返回内容
	        logger.info("WebLogAspect.doAfterReturning()");
	        try {
    			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    			// 获得注解
    			OperateLog logger = giveController(joinPoint);
    			if (logger == null) {
    				return;
    			}
    			String signature = joinPoint.getSignature().toString(); // ��ȡĿ�귽��ǩ��
    			String methodName = signature.substring(signature.lastIndexOf(".") + 1, signature.indexOf("("));
    			String classType = joinPoint.getTarget().getClass().getName();
    			Class<?> clazz = Class.forName(classType);
    			Method[] methods = clazz.getDeclaredMethods();
    			for (Method method : methods) {
    				if (method.isAnnotationPresent(OperateLog.class) && method.getName().equals(methodName)) {
    					Long userId=WebUtil.getCurrentUser();
    					if(userId==null){
    						return;
    					}
    					String username = "",actName="";
    					User user = userService.qryUserById(userId);
    					actName=user.getName();
    					username=user.getAccount();
    					OpType type = logger.type();
    					SysOperateLog log=new SysOperateLog();
    					if("SEARCH".equals(type.toString())){
    						return;
    					}else if("ADD".equals(type.toString())) {
    						log.setOperateType("添加");
    					}else if("UPDATE".equals(type.toString())) {
    						log.setOperateType("修改");
    					}else if("DEL".equals(type.toString())) {
    						log.setOperateType("删除");
    					}
    					String module = logger.module();
    					String desc = logger.desc();
    					log.setId(IdWorker.getId());
    					log.setArgs(optionContent(joinPoint.getArgs(), methodName));
    					log.setActName(actName);
    					log.setUserName(username);
    					log.setModule(module);
    					log.setOperateTime(new Date());
    					
    					log.setDescription(desc);
    					log.setIp(WebUtil.getHost(request));
    					log.setActName(actName);
    					sysLogService.insertOperateLog(log);
    					
    				}
    			}
    		} catch (Exception exp) {  
    			logger.error("异常:{}", exp.getMessage());
    			exp.printStackTrace();
    		}
	     }
	     
	 	private static OperateLog giveController(JoinPoint joinPoint) throws Exception {
			Signature signature = joinPoint.getSignature();
			MethodSignature methodSignature = (MethodSignature) signature;
			Method method = methodSignature.getMethod();

			if (method != null) {
				return method.getAnnotation(OperateLog.class);
			}
			return null;
		}
	     
	 	/**
	 	 * 使用Java反射来获取被拦截方法(insert、update)的参数值， 将参数值拼接为操作内容
	 	 */
	 	public String optionContent(Object[] args, String mName) throws Exception {
	 		if (args == null) {
	 			return null;
	 		}
	 		StringBuffer rs = new StringBuffer();
	 		rs.append(mName);
	 		String className = null;
	 		int index = 1;
	 		for (Object info : args) {	
	 			className = info.getClass().getName();
	 			className = className.substring(className.lastIndexOf(".") + 1);
	 			if ("ShiroHttpServletRequest".equals(className) 
 					|| "ShiroHttpServletResponse".equals(className)
 					|| "BindingAwareModelMap".equals(className) 
 					|| "RequestFacade".equals(className) 
 					|| "ResponseFacade".equals(className)
 					||"CookieHttpSessionStrategy$MultiSessionHttpServletResponse".equals(className)
 					||"WebStatFilter$StatHttpServletResponseWrapper".equals(className)) {
	 				continue;
	 			}
	 			String arg = JSON.toJSONString(info);
	 			rs.append("[参数" + index + "，值：" + arg);
	 			
	 			rs.append("]");
	 			index++;
	 		}
	 		return rs.toString();
	 	}
}
