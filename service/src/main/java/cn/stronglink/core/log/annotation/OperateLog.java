package cn.stronglink.core.log.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD,ElementType.TYPE})  
@Retention(RetentionPolicy.RUNTIME)  
@Documented  
public @interface OperateLog {	
	String module() default ""; 
	String desc() default "";
	public enum OpType{ADD,UPDATE,DEL,SEARCH,LOGIN};  
    OpType type() default OpType.SEARCH; 
}
