package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;

import cn.stronglink.core.base.BaseModel;

public class EquipmentDocCatalog extends BaseModel{
	/**
	 * 
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "name_")
	private String name;
	
	@TableField(value = "equipment_doc_id")
	private Long	equipmentDocId;
	@TableField(value = "page_")
	private Integer	page;
	@TableField(value = "sort_")
	private Integer	sort;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Long getEquipmentDocId() {
		return equipmentDocId;
	}
	public void setEquipmentDocId(Long equipmentDocId) {
		this.equipmentDocId = equipmentDocId;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
}