package cn.stronglink.esm27.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_plans_temp")
public class PlanTemp extends BaseModel{
	
	/**
	 * 
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "name_")
	private String name;
	
	@TableField(value = "code_")
	private String code;
	
	@TableField(value = "dic_")
	private String dic;
	
	@TableField(value = "level_")
	private Integer level;
	
	@TableField(value = "writer_")
	private String writer;
	
	@TableField(value = "writing_unit")
	private String writingUnit;
	
	@TableField(value = "writing_time")
	private Date writingTime;
	
	@TableField(value = "status_")
	private Integer status;
	
	@TableField(value = "key_unit_id")
	private String keyUnitId;
	
	@TableField(value = "type_")
	private String type;

	@TableField(value="timestamp_")
	private Long timestamp;
	
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getWritingUnit() {
		return writingUnit;
	}

	public void setWritingUnit(String writingUnit) {
		this.writingUnit = writingUnit;
	}

	public Date getWritingTime() {
		return writingTime;
	}

	public void setWritingTime(Date writingTime) {
		this.writingTime = writingTime;
	}

	public String getDic() {
		return dic;
	}

	public void setDic(String dic) {
		this.dic = dic;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getKeyUnitId() {
		return keyUnitId;
	}

	public void setKeyUnitId(String keyUnitId) {
		this.keyUnitId = keyUnitId;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}


}
