package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_gas_detector_data")
public class GasDetectorData extends BaseModel {
	
	/**
	 *
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "gas_detector_id")
	private Long gasDetectorId;//检测仪ID
	
	@TableField(value = "detector_position")
	private Integer detectorPosition;//通道位置
	
	@TableField(value = "detector_status")
	private Boolean detectorStatus;//0 关   1开 
	
	@TableField(value = "gas_id")
	private Integer gasId;//气体的ID

	public Long getGasDetectorId() {
		return gasDetectorId;
	}

	public void setGasDetectorId(Long gasDetectorId) {
		this.gasDetectorId = gasDetectorId;
	}

	public Integer getDetectorPosition() {
		return detectorPosition;
	}

	public void setDetectorPosition(Integer detectorPosition) {
		this.detectorPosition = detectorPosition;
	}

	public Boolean getDetectorStatus() {
		return detectorStatus;
	}

	public void setDetectorStatus(Boolean detectorStatus) {
		this.detectorStatus = detectorStatus;
	}

	public Integer getGasId() {
		return gasId;
	}

	public void setGasId(Integer gasId) {
		this.gasId = gasId;
	}

}
