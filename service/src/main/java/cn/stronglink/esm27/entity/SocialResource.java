package cn.stronglink.esm27.entity;

import java.util.List;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;
import cn.stronglink.core.util.AntdFile;
import cn.stronglink.core.util.UploadEntity;
import cn.stronglink.esm27.module.plan.vo.PlanVo;
import cn.stronglink.esm27.module.unit.vo.KeyPartsVo;

@TableName("t_social_resource")
public class SocialResource extends BaseModel{
	
	/**
	 * 社会资源表
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "name_")
	private String name;
	
	@TableField(value = "fire_brigade")
	private String fireBrigade;
	
	@TableField(value = "addr_")
	private String addr;
	
	@TableField(value = "tel_")
	private String tel;
	
	@TableField(value = "admin_")
	private String admin;
		
	@TableField(value = "lon_")
	private Double lon;
	
	@TableField(value = "lat_")
	private Double lat;	
	
	@TableField(value = "baidu_lon")
	private Double baiduLon;
	
	@TableField(value = "baidu_lat")
	private Double baiduLat;	
	
	@TableField(value = "dt_path")
	private String dtPath;
		
	@TableField(value = "desc_")
	private String desc;
	
	@TableField(value = "type_id")
	private Long typeId;
	
	@TableField(value = "water_supply")
	private String waterSupply;
	
	@TableField(exist = false)
	private String fireBrigadeName;
	
	@TableField(exist = false)
	private String distances;
	
	@TableField(exist = false)
	private List<String> imgList;
	
	@TableField(exist = false)
	private List<UploadEntity> fileList;
	
	@TableField(exist = false)
	private String lonLat;

	@TableField(exist = false)
	private List<KeyUnitDangers> dangersList;
	
	@TableField(exist = false)
	private List<PlanVo> planList;
	
	@TableField(exist = false)
	private List<KeyPartsVo> keyPartsList;
	
//	@TableField(exist = false)
//	private List<KeyUnitAccidentFile> accidentFileList;
	
	@TableField(exist = false)
	private List<AntdFile> accidentFileList;
	
	@TableField(exist = false)
	private String typeName;

	public List<KeyPartsVo> getKeyPartsList() {
		return keyPartsList;
	}

	public void setKeyPartsList(List<KeyPartsVo> keyPartsList) {
		this.keyPartsList = keyPartsList;
	}

	public String getWaterSupply() {
		return waterSupply;
	}

	public void setWaterSupply(String waterSupply) {
		this.waterSupply = waterSupply;
	}

	public String getLonLat() {
		return lonLat;
	}

	public void setLonLat(String lonLat) {
		this.lonLat = lonLat;
	}
	   
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFireBrigade() {
		return fireBrigade;
	}

	public void setFireBrigade(String fireBrigade) {
		this.fireBrigade = fireBrigade;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getAdmin() {
		return admin;
	}

	public void setAdmin(String admin) {
		this.admin = admin;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getFireBrigadeName() {
		return fireBrigadeName;
	}

	public void setFireBrigadeName(String fireBrigadeName) {
		this.fireBrigadeName = fireBrigadeName;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getBaiduLon() {
		return baiduLon;
	}

	public void setBaiduLon(Double baiduLon) {
		this.baiduLon = baiduLon;
	}

	public Double getBaiduLat() {
		return baiduLat;
	}

	public void setBaiduLat(Double baiduLat) {
		this.baiduLat = baiduLat;
	}

	public String getDtPath() {
		return dtPath;
	}

	public void setDtPath(String dtPath) {
		this.dtPath = dtPath;
	}

	public List<KeyUnitDangers> getDangersList() {
		return dangersList;
	}

	public void setDangersList(List<KeyUnitDangers> dangersList) {
		this.dangersList = dangersList;
	}

	public List<String> getImgList() {
		return imgList;
	}

	public void setImgList(List<String> imgList) {
		this.imgList = imgList;
	}

	public List<UploadEntity> getFileList() {
		return fileList;
	}

	public void setFileList(List<UploadEntity> fileList) {
		this.fileList = fileList;
	}

	public String getDistances() {
		return distances;
	}

	public void setDistances(String distances) {
		this.distances = distances;
	}

	public List<PlanVo> getPlanList() {
		return planList;
	}

	public void setPlanList(List<PlanVo> planList) {
		this.planList = planList;
	}

	public List<AntdFile> getAccidentFileList() {
		return accidentFileList;
	}

	public void setAccidentFileList(List<AntdFile> accidentFileList) {
		this.accidentFileList = accidentFileList;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	
}
