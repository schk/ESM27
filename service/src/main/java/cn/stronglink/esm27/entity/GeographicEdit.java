package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import cn.stronglink.core.base.BaseModel;

@TableName("t_geographic_edit")
public class GeographicEdit extends BaseModel{
	/**
	 * 应急指挥记录
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1421301688225365334L;
	
	@TableId(value = "id_", type = IdType.INPUT)
	private Long id;
		
	@TableField(value = "name_")
	private String name;
	
	@TableField(value = "desc_")
	private String desc;
	
	@TableField(value = "content_")
	private String content;
	
	@TableField(exist = false)
	private int type;


	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	
}
