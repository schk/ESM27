package cn.stronglink.esm27.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.FieldStrategy;

import cn.stronglink.core.base.BaseModel;

@TableName("t_fire_water_source")
public class FireWaterSource extends BaseModel {

	/**
	 * 消防水源
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = -1300302109877857120L;
	
	@TableField(value="name_")
	private String name;
	
	@TableField(value="dic_id")
	private Long dicId;
	
	@TableField(value="key_unit_id")
	private Long keyUnitId;
	
	@TableField(value="addr_")
	private String addr;
	
	@TableField(value="flow_",strategy = FieldStrategy.IGNORED)
	private BigDecimal flow;
	
	@TableField(value="reserves_",strategy = FieldStrategy.IGNORED)
	private BigDecimal reserves;
	
	@TableField(value="good_use")
	private Boolean goodUse;
	
	@TableField(value = "lon_")
	private Double lon;
	
	@TableField(value = "lat_")
	private Double lat;	
	
	@TableField(value = "baidu_lon")
	private Double baiduLon;
	
	@TableField(value = "baidu_lat")
	private Double baiduLat;	
	
	@TableField(value="remark_",strategy = FieldStrategy.IGNORED)
	private String remark;
	
	@TableField(exist = false)
	private String lonLat;
	
	// 勘查时间
	@TableField(value = "survey_time")
	private Date surveyTime;
	// 勘查人
	@TableField(value = "surveyor_")
	private String surveyor;	
	// 核查人
	@TableField(value = "verifier_")
	private String verifier;
	// 水源点负责人
	@TableField(value = "water_source_point_head")
	private String waterSourcePointHead;	
	
	@TableField(value = "phone_")
	private String phone;	
	// 管辖单位
	@TableField(value = "jurisdiction_unit")
	private String jurisdictionUnit;	
	
	
	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public BigDecimal getReserves() {
		return reserves;
	}

	public void setReserves(BigDecimal reserves) {
		this.reserves = reserves;
	}

	public Boolean getGoodUse() {
		return goodUse;
	}

	public void setGoodUse(Boolean goodUse) {
		this.goodUse = goodUse;
	}

	public String getLonLat() {
		return lonLat;
	}

	public void setLonLat(String lonLat) {
		this.lonLat = lonLat;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getDicId() {
		return dicId;
	}

	public void setDicId(Long dicId) {
		this.dicId = dicId;
	}

	public Long getKeyUnitId() {
		return keyUnitId;
	}

	public void setKeyUnitId(Long keyUnitId) {
		this.keyUnitId = keyUnitId;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Double getBaiduLon() {
		return baiduLon;
	}

	public void setBaiduLon(Double baiduLon) {
		this.baiduLon = baiduLon;
	}

	public Double getBaiduLat() {
		return baiduLat;
	}

	public void setBaiduLat(Double baiduLat) {
		this.baiduLat = baiduLat;
	}

	public BigDecimal getFlow() {
		return flow;
	}

	public void setFlow(BigDecimal flow) {
		this.flow = flow;
	}

	public Date getSurveyTime() {
		return surveyTime;
	}

	public void setSurveyTime(Date surveyTime) {
		this.surveyTime = surveyTime;
	}

	public String getSurveyor() {
		return surveyor;
	}

	public void setSurveyor(String surveyor) {
		this.surveyor = surveyor;
	}

	public String getVerifier() {
		return verifier;
	}

	public void setVerifier(String verifier) {
		this.verifier = verifier;
	}

	public String getWaterSourcePointHead() {
		return waterSourcePointHead;
	}

	public void setWaterSourcePointHead(String waterSourcePointHead) {
		this.waterSourcePointHead = waterSourcePointHead;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getJurisdictionUnit() {
		return jurisdictionUnit;
	}

	public void setJurisdictionUnit(String jurisdictionUnit) {
		this.jurisdictionUnit = jurisdictionUnit;
	}
	
}
