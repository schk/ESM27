package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_expr_param_option")
public class ExprParamOption extends BaseModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8867253306313765897L;

	@TableField(value="expr_param_id")
	private Long exprParamId;
	
	@TableField(value="attr_")
	private String attr;
	
	@TableField(value="option_val")
	private String optionVal;
	
	@TableField(value="default_val")
	private String defaultVal;

	public Long getExprParamId() {
		return exprParamId;
	}

	public void setExprParamId(Long exprParamId) {
		this.exprParamId = exprParamId;
	}

	public String getAttr() {
		return attr;
	}

	public void setAttr(String attr) {
		this.attr = attr;
	}

	public String getOptionVal() {
		return optionVal;
	}

	public void setOptionVal(String optionVal) {
		this.optionVal = optionVal;
	}

	public String getDefaultVal() {
		return defaultVal;
	}

	public void setDefaultVal(String defaultVal) {
		this.defaultVal = defaultVal;
	}
	
	
	
}
