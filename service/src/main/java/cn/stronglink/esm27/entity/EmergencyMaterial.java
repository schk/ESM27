package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;
@TableName("t_emergency_material")
public class EmergencyMaterial extends BaseModel {

	/**
	 * 应急物资表
	 */
	@TableField(exist=false)
	private static final long serialVersionUID = -8337335635068779347L;
	
	@TableField(value="name_")
	private String name;
	
	@TableField(value="reserve_point_id")
	private Long reservePointId;
	
	@TableField(value="specifications_")
	private String specifications;
	
	@TableField(value="model_")
	private String model;
	
	@TableField(value="type_id")
	private String  typeId;
	
	@TableField(value="storage_quantity")
	private Integer storageQuantity;
	
	@TableField(value="storage_unit")
	private String  storageUnit;
	
	@TableField(value="charge_person")
	private String chargePerson;
	
	@TableField(value="size_")
	private String size;
	
	@TableField(value="phone_")
	private String 	phone;
	
	@TableField(value="purpose_")
	private String purpose;
	
	@TableField(value="remark_")
	private String remark;
	
	@TableField(value="available_")
	private Integer available;
	
	@TableField(value="intelligent_recommendation")
	private Integer intelligentRecommendation;
	
	@TableField(value="disaster_type")
	private String disasterType;
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Long getReservePointId() {
		return reservePointId;
	}

	public void setReservePointId(Long reservePointId) {
		this.reservePointId = reservePointId;
	}

	public String getSpecifications() {
		return specifications;
	}

	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public Integer getStorageQuantity() {
		return storageQuantity;
	}

	public void setStorageQuantity(Integer storageQuantity) {
		this.storageQuantity = storageQuantity;
	}

	public String getStorageUnit() {
		return storageUnit;
	}

	public void setStorageUnit(String storageUnit) {
		this.storageUnit = storageUnit;
	}

	public String getChargePerson() {
		return chargePerson;
	}

	public void setChargePerson(String chargePerson) {
		this.chargePerson = chargePerson;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getAvailable() {
		return available;
	}

	public void setAvailable(Integer available) {
		this.available = available;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getIntelligentRecommendation() {
		return intelligentRecommendation;
	}

	public void setIntelligentRecommendation(Integer intelligentRecommendation) {
		this.intelligentRecommendation = intelligentRecommendation;
	}

	public String getDisasterType() {
		return disasterType;
	}

	public void setDisasterType(String disasterType) {
		this.disasterType = disasterType;
	}

	
}
