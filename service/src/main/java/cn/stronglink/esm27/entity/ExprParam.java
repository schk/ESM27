package cn.stronglink.esm27.entity;

import java.util.List;

import com.baomidou.mybatisplus.annotations.TableField;

import cn.stronglink.core.base.BaseModel;

public class ExprParam extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3582060099191492103L;

	@TableField(value="expr_id")
	private Long exprId;
	
	@TableField(value="name_")
	private String name;
	
	@TableField(value="value_")
	private String value;
	
	@TableField(value="empty_")
	private Integer empty;
	
	@TableField(value="type_")
	private Integer type;
	
	@TableField(value="real_time")
	private Integer realTime;
	
	
	@TableField(exist = false)
	private List<ExprParamOption> childOption;
	
	public Long getExprId() {
		return exprId;
	}

	public void setExprId(Long exprId) {
		this.exprId = exprId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getEmpty() {
		return empty;
	}

	public void setEmpty(Integer empty) {
		this.empty = empty;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getRealTime() {
		return realTime;
	}

	public void setRealTime(Integer realTime) {
		this.realTime = realTime;
	}

	public List<ExprParamOption> getChildOption() {
		return childOption;
	}

	public void setChildOption(List<ExprParamOption> childOption) {
		this.childOption = childOption;
	}


	
	
}
