package cn.stronglink.esm27.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_extinguisher_temp")
public class ExtinguisherTemp extends BaseModel {

	/**
	 * 灭火剂表
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value="type_")
	private String type;
	
	@TableField(value="fire_brigade_id")
	private Long fireBrigadeId;
	
	@TableField(value="quantity_")
	private Double quantity;
	
	@TableField(value="service_life")
	private Integer serviceLife;
	
	@TableField(value="product_date")
	private Date productDate;
	
	@TableField(value="effective_date")
	private Date effectiveDate;

	@TableField(value="timestamp_")
	private Long timestamp;
	
	
	
	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getFireBrigadeId() {
		return fireBrigadeId;
	}

	public void setFireBrigadeId(Long fireBrigadeId) {
		this.fireBrigadeId = fireBrigadeId;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Integer getServiceLife() {
		return serviceLife;
	}

	public void setServiceLife(Integer serviceLife) {
		this.serviceLife = serviceLife;
	}

	public Date getProductDate() {
		return productDate;
	}

	public void setProductDate(Date productDate) {
		this.productDate = productDate;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

}
