package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_gas_history_data_result")
public class GasHistoryDataResult  extends BaseModel {
	
	/**
	 *
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "gas_detector_id")
	private Long gasDetectorId;
	
	@TableField(value = "data_json")
	private String dataJson;
	
	@TableField(value = "room_id")
	private String roomId;
	
	@TableField(exist = false)
	private String month;
	
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	public Long getGasDetectorId() {
		return gasDetectorId;
	}
	public void setGasDetectorId(Long gasDetectorId) {
		this.gasDetectorId = gasDetectorId;
	}
	public String getDataJson() {
		return dataJson;
	}
	public void setDataJson(String dataJson) {
		this.dataJson = dataJson;
	}
	

	
}
