package cn.stronglink.esm27.entity;

public class ExtinguisheSum  {
	
	private Long id;
	
	private String name;
	
	private Long deptId;
	
	private Double commonProteinfoam;
	
	private Double clearWaterFoam;
	
	private Double highExpansionFoam;
	
	private Double f500Foam;
	
	private Double antiSolubleFoam;
	
	private Double waterFilmWater;
	
	private Double dryPowder;
	
	private Double water;

	private Double cSum;
	
	private Double kDryPowder;
	
	private Double kF500Foam;
	
	private Double kAntiSolubleFoam;
	
	private Double kCommonProteinFoam;
	
	private Double kWater;
	
	private Double kClearWaterFoam;
	
	private Double kHighExpansionFoam;
	
	private Double kWaterFilmWater;
	
	private Double kQuantity;
	
	private Double quantity;
	
	private String fireBrigadeName;

	
	public String getFireBrigadeName() {
		return fireBrigadeName;
	}

	public void setFireBrigadeName(String fireBrigadeName) {
		this.fireBrigadeName = fireBrigadeName;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getkWaterFilmWater() {
		return kWaterFilmWater;
	}

	public void setkWaterFilmWater(Double kWaterFilmWater) {
		this.kWaterFilmWater = kWaterFilmWater;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getDeptId() {
		return deptId;
	}

	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

	public Double getCommonProteinfoam() {
		return commonProteinfoam;
	}

	public void setCommonProteinfoam(Double commonProteinfoam) {
		this.commonProteinfoam = commonProteinfoam;
	}

	public Double getClearWaterFoam() {
		return clearWaterFoam;
	}

	public void setClearWaterFoam(Double clearWaterFoam) {
		this.clearWaterFoam = clearWaterFoam;
	}

	public Double getHighExpansionFoam() {
		return highExpansionFoam;
	}

	public void setHighExpansionFoam(Double highExpansionFoam) {
		this.highExpansionFoam = highExpansionFoam;
	}

	

	public Double getF500Foam() {
		return f500Foam;
	}

	public void setF500Foam(Double f500Foam) {
		this.f500Foam = f500Foam;
	}

	public Double getAntiSolubleFoam() {
		return antiSolubleFoam;
	}

	public void setAntiSolubleFoam(Double antiSolubleFoam) {
		this.antiSolubleFoam = antiSolubleFoam;
	}

	public Double getWaterFilmWater() {
		return waterFilmWater;
	}

	public void setWaterFilmWater(Double waterFilmWater) {
		this.waterFilmWater = waterFilmWater;
	}

	public Double getDryPowder() {
		return dryPowder;
	}

	public void setDryPowder(Double dryPowder) {
		this.dryPowder = dryPowder;
	}

	public Double getWater() {
		return water;
	}

	public void setWater(Double water) {
		this.water = water;
	}

	public Double getcSum() {
		return cSum;
	}

	public void setcSum(Double cSum) {
		this.cSum = cSum;
	}

	public Double getkDryPowder() {
		return kDryPowder;
	}

	public void setkDryPowder(Double kDryPowder) {
		this.kDryPowder = kDryPowder;
	}

	public Double getkF500Foam() {
		return kF500Foam;
	}

	public void setkF500Foam(Double kF500Foam) {
		this.kF500Foam = kF500Foam;
	}

	public Double getkAntiSolubleFoam() {
		return kAntiSolubleFoam;
	}

	public void setkAntiSolubleFoam(Double kAntiSolubleFoam) {
		this.kAntiSolubleFoam = kAntiSolubleFoam;
	}

	public Double getkCommonProteinFoam() {
		return kCommonProteinFoam;
	}

	public void setkCommonProteinFoam(Double kCommonProteinFoam) {
		this.kCommonProteinFoam = kCommonProteinFoam;
	}

	public Double getkWater() {
		return kWater;
	}

	public void setkWater(Double kWater) {
		this.kWater = kWater;
	}

	public Double getkClearWaterFoam() {
		return kClearWaterFoam;
	}

	public void setkClearWaterFoam(Double kClearWaterFoam) {
		this.kClearWaterFoam = kClearWaterFoam;
	}

	public Double getkHighExpansionFoam() {
		return kHighExpansionFoam;
	}

	public void setkHighExpansionFoam(Double kHighExpansionFoam) {
		this.kHighExpansionFoam = kHighExpansionFoam;
	}


	public Double getkQuantity() {
		return kQuantity;
	}

	public void setkQuantity(Double kQuantity) {
		this.kQuantity = kQuantity;
	}

	@Override
	public String toString() {
		return "ExtinguisheSum [name=" + name + ", deptId=" + deptId + ", commonProteinfoam=" + commonProteinfoam
				+ ", clearWaterFoam=" + clearWaterFoam + ", highExpansionFoam=" + highExpansionFoam + ", f500Foam="
				+ f500Foam + ", antiSolubleFoam=" + antiSolubleFoam + ", waterFilmWater=" + waterFilmWater
				+ ", dryPowder=" + dryPowder + ", water=" + water + ", cSum=" + cSum + ", kDryPowder=" + kDryPowder
				+ ", kF500Foam=" + kF500Foam + ", kAntiSolubleFoam=" + kAntiSolubleFoam + ", kCommonProteinFoam="
				+ kCommonProteinFoam + ", kWater=" + kWater + ", kClearWaterFoam=" + kClearWaterFoam
				+ ", kHighExpansionFoam=" + kHighExpansionFoam + ", kWaterFilmWater=" + kWaterFilmWater + ", kQuantity="
				+ kQuantity + "]";
	}

	
	

}
