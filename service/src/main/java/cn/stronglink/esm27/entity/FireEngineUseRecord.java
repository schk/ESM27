package cn.stronglink.esm27.entity;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_fire_engine_use_record")
public class FireEngineUseRecord extends BaseModel {
	/**
	 * 消防车使用记录
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1421301688225365334L;
	
	@TableField(value = "fire_engine_id")
	private Long fireEngineId;
	
	@TableField(value = "room_id")
	private Long roomId;
	
	@TableField(value = "count_")
	private Integer count;

	public Long getFireEngineId() {
		return fireEngineId;
	}

	public void setFireEngineId(Long fireEngineId) {
		this.fireEngineId = fireEngineId;
	}

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}


}
