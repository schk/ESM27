package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_room")
public class Room extends BaseModel {
	
	@TableField(exist = false)
	private static final long serialVersionUID = 1421301688225365334L;
	
	
	@TableField(value = "room_")
	private String room;
	
	@TableField(value = "accident_name")
    private String accidentName;
	
	@TableField(value = "accident_remark")
    private String accidentRemark;
	
	@TableField(value = "accident_level")
    private String accidentLevel;
	
	@TableField(value = "accident_type")
    private String accidentType;
	
	@TableField(value = "accident_conductor")
	private String accidentConductor;
	
	@TableField(value = "status_")
	private Integer status;
	
	@TableField(value = "accident_loss")
    private String accidentLoss;
	
	@TableField(value = "accident_address")
    private String accidentAddress;
	
	@TableField(value = "accident_location")
    private String accidentLocation;
	
	@TableField(value = "accident_people")
    private String accidentPeople;
	
	@TableField(value = "accident_afterwar")
    private String accidentAfterwar;
    
	@TableField(value = "weather_condition_l")
    private String weatherConditionL;
	
	@TableField(value = "weather_condition_x")
    private String weatherConditionX;
	
	@TableField(value = "combustion_area")
    private String combustionArea;
	
	@TableField(value = "adjacent_conditions")
    private String adjacentConditions;
	
	@TableField(value = "burning_material")
	private String burningMaterial;
	
	@TableField(value = "other_desc")
	private String otherDesc;
	
	@TableField(value = "alarm_time")
    private String alarmTime;
	
	@TableField(value = "attack_time")
    private String attackTime;
	
	@TableField(value = "attendance_time")
    private String attendanceTime;
	
	@TableField(value = "control_time")
    private String controlTime;
	
	@TableField(value = "extinguish_time")
    private String extinguishTime;
	
	@TableField(value = "evacuation_time")
    private String evacuationTime;
	
	@TableField(value = "rescue_desc")
    private String rescueDesc;
	
	@TableField(value = "scene_state")
    private String sceneState;
	
	@TableField(value = "transfer_matters")
    private String transferMatters;
	
	@TableField(value = "desc_")
    private String desc;

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getRescueDesc() {
		return rescueDesc;
	}

	public void setRescueDesc(String rescueDesc) {
		this.rescueDesc = rescueDesc;
	}

	public String getSceneState() {
		return sceneState;
	}

	public void setSceneState(String sceneState) {
		this.sceneState = sceneState;
	}

	public String getTransferMatters() {
		return transferMatters;
	}

	public void setTransferMatters(String transferMatters) {
		this.transferMatters = transferMatters;
	}

	public String getWeatherConditionL() {
		return weatherConditionL;
	}

	public void setWeatherConditionL(String weatherConditionL) {
		this.weatherConditionL = weatherConditionL;
	}

	public String getWeatherConditionX() {
		return weatherConditionX;
	}

	public void setWeatherConditionX(String weatherConditionX) {
		this.weatherConditionX = weatherConditionX;
	}

	public String getCombustionArea() {
		return combustionArea;
	}

	public void setCombustionArea(String combustionArea) {
		this.combustionArea = combustionArea;
	}

	public String getAdjacentConditions() {
		return adjacentConditions;
	}

	public void setAdjacentConditions(String adjacentConditions) {
		this.adjacentConditions = adjacentConditions;
	}

	public String getBurningMaterial() {
		return burningMaterial;
	}

	public void setBurningMaterial(String burningMaterial) {
		this.burningMaterial = burningMaterial;
	}

	public String getOtherDesc() {
		return otherDesc;
	}

	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}

	public String getAlarmTime() {
		return alarmTime;
	}

	public void setAlarmTime(String alarmTime) {
		this.alarmTime = alarmTime;
	}

	public String getAttackTime() {
		return attackTime;
	}

	public void setAttackTime(String attackTime) {
		this.attackTime = attackTime;
	}

	public String getAttendanceTime() {
		return attendanceTime;
	}

	public void setAttendanceTime(String attendanceTime) {
		this.attendanceTime = attendanceTime;
	}

	public String getControlTime() {
		return controlTime;
	}

	public void setControlTime(String controlTime) {
		this.controlTime = controlTime;
	}

	public String getExtinguishTime() {
		return extinguishTime;
	}

	public void setExtinguishTime(String extinguishTime) {
		this.extinguishTime = extinguishTime;
	}

	public String getEvacuationTime() {
		return evacuationTime;
	}

	public void setEvacuationTime(String evacuationTime) {
		this.evacuationTime = evacuationTime;
	}
	
	@TableField(exist = false)
	private String isCreate;
	
	@TableField(exist = false)
	private String createByName;
	
	public String getCreateByName() {
		return createByName;
	}
	public void setCreateByName(String createByName) {
		this.createByName = createByName;
	}
	public String getIsCreate() {
		return isCreate;
	}
	public void setIsCreate(String isCreate) {
		this.isCreate = isCreate;
	}
	public String getRoom() {
		return room;
	}
	public void setRoom(String room) {
		this.room = room;
	}
	public String getAccidentName() {
		return accidentName;
	}
	public void setAccidentName(String accidentName) {
		this.accidentName = accidentName;
	}
	public String getAccidentRemark() {
		return accidentRemark;
	}
	public void setAccidentRemark(String accidentRemark) {
		this.accidentRemark = accidentRemark;
	}
	public String getAccidentLevel() {
		return accidentLevel;
	}
	public void setAccidentLevel(String accidentLevel) {
		this.accidentLevel = accidentLevel;
	}
	public String getAccidentType() {
		return accidentType;
	}
	public void setAccidentType(String accidentType) {
		this.accidentType = accidentType;
	}
	
	public String getAccidentConductor() {
		return accidentConductor;
	}
	public void setAccidentConductor(String accidentConductor) {
		this.accidentConductor = accidentConductor;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getAccidentLoss() {
		return accidentLoss;
	}
	public void setAccidentLoss(String accidentLoss) {
		this.accidentLoss = accidentLoss;
	}
	public String getAccidentAddress() {
		return accidentAddress;
	}
	public void setAccidentAddress(String accidentAddress) {
		this.accidentAddress = accidentAddress;
	}
	public String getAccidentLocation() {
		return accidentLocation;
	}
	public void setAccidentLocation(String accidentLocation) {
		this.accidentLocation = accidentLocation;
	}
	public String getAccidentPeople() {
		return accidentPeople;
	}
	public void setAccidentPeople(String accidentPeople) {
		this.accidentPeople = accidentPeople;
	}
	public String getAccidentAfterwar() {
		return accidentAfterwar;
	}
	public void setAccidentAfterwar(String accidentAfterwar) {
		this.accidentAfterwar = accidentAfterwar;
	}
	
}
