package cn.stronglink.esm27.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_special_duty_equip")
public class SpecialDutyEquip extends BaseModel {

	/**
	 * 专勤器材表
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = -7906387870059333287L;
	

	@TableField(value="name_")
	private String name;
	
	@TableField(value="code_")
	private String code;
	
	@TableField(value="self_code")
	private String selfCode;
	
	@TableField(value="fire_brigade_id")
	private Long fireBrigadeId;
	
	@TableField(value="specifications_model")
	private String specificationsModel;
	
	@TableField(value="type_")
	private String type;
	
	@TableField(value="storage_quantity")
	private Integer storageQuantity;
	
	@TableField(value="unit_")
	private String unit;
	
	@TableField(value="number_of_car")
	private Integer numberOfCar;
	
	@TableField(value="picture_")
	private String picture;
	
	@TableField(value="position_")
	private String position;
	
	@TableField(value="technical_param_file")
	private String technicalParamFile;
	
	@TableField(value="system_rules")
	private String systemRules;
	
	@TableField(value="equip_type")
	private Integer  equipType;
	
	@TableField(value="temp_use")
	private Integer tempUse;
	
	@TableField(value="flow_")
	private Double flow;
	
	@TableField(value="compute_")
	private Integer compute;
	
	@TableField(value="foam_available")
	private Integer foamAvailable;

	@TableField(value = "production_data")
	private Date productionDate;
	@TableField(value = "service_life")
	private String serviceLife;

	public Integer getTempUse() {
		return tempUse;
	}

	public void setTempUse(Integer tempUse) {
		this.tempUse = tempUse;
	}

	public Integer getEquipType() {
		return equipType;
	}

	public void setEquipType(Integer equipType) {
		this.equipType = equipType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelfCode() {
		return selfCode;
	}

	public void setSelfCode(String selfCode) {
		this.selfCode = selfCode;
	}

	public Long getFireBrigadeId() {
		return fireBrigadeId;
	}

	public void setFireBrigadeId(Long fireBrigadeId) {
		this.fireBrigadeId = fireBrigadeId;
	}

	public String getSpecificationsModel() {
		return specificationsModel;
	}

	public void setSpecificationsModel(String specificationsModel) {
		this.specificationsModel = specificationsModel;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getStorageQuantity() {
		return storageQuantity;
	}

	public void setStorageQuantity(Integer storageQuantity) {
		this.storageQuantity = storageQuantity;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Integer getNumberOfCar() {
		return numberOfCar;
	}

	public void setNumberOfCar(Integer numberOfCar) {
		this.numberOfCar = numberOfCar;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getTechnicalParamFile() {
		return technicalParamFile;
	}

	public void setTechnicalParamFile(String technicalParamFile) {
		this.technicalParamFile = technicalParamFile;
	}

	public String getSystemRules() {
		return systemRules;
	}

	public void setSystemRules(String systemRules) {
		this.systemRules = systemRules;
	}

	public Double getFlow() {
		return flow;
	}

	public void setFlow(Double flow) {
		this.flow = flow;
	}

	public Integer getFoamAvailable() {
		return foamAvailable;
	}

	public void setFoamAvailable(Integer foamAvailable) {
		this.foamAvailable = foamAvailable;
	}

	public Integer getCompute() {
		return compute;
	}

	public void setCompute(Integer compute) {
		this.compute = compute;
	}

	public Date getProductionDate() {
		return productionDate;
	}

	public void setProductionDate(Date productionDate) {
		this.productionDate = productionDate;
	}

	public String getServiceLife() {
		return serviceLife;
	}

	public void setServiceLife(String serviceLife) {
		this.serviceLife = serviceLife;
	}
 
}
