package cn.stronglink.esm27.entity;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_emergency_material_use_record")
public class MaterialUseRecord extends BaseModel {
	/**
	 * 应急物资使用记录
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1421301688225365334L;
	
	@TableField(value = "material_id")
	private Long materialId;
	
	@TableField(value = "room_id")
	private Long roomId;
	
	@TableField(value = "count_")
	private Integer count;

	public Long getMaterialId() {
		return materialId;
	}

	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}


}
