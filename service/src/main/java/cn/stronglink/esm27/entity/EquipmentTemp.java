package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_equipment_temp")
public class EquipmentTemp extends BaseModel{
	
	/**
	 * 生产装置
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "name_")
	private String name;
	
	@TableField(value = "key_unit_id")
	private String keyUnitId;
	
	@TableField(value = "type_")
	private String type;
	
	@TableField(value = "code_")
	private String code;
	
	@TableField(value = "medium_")
	private String medium;
	
	@TableField(value = "lon_")
	private Double lon;
	
	@TableField(value = "lat_")
	private Double lat;	
	
	@TableField(value = "baidu_lon")
	private Double baiduLon;
	
	@TableField(value = "baidu_lat")
	private Double baiduLat;	
	
	@TableField(value = "vessel_diameter")
	private Double vesselDiameter;
	
	@TableField(value = "fire_level")
	private String fireLevel;
	
	@TableField(value = "volume_")
	private Double volume;
	
	@TableField(value = "dt_path")
	private String dtPath;

	@TableField(value = "timestamp_")
	private Long timestamp;
	
	
	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getKeyUnitId() {
		return keyUnitId;
	}

	public void setKeyUnitId(String keyUnitId) {
		this.keyUnitId = keyUnitId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMedium() {
		return medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getBaiduLon() {
		return baiduLon;
	}

	public void setBaiduLon(Double baiduLon) {
		this.baiduLon = baiduLon;
	}

	public Double getBaiduLat() {
		return baiduLat;
	}

	public void setBaiduLat(Double baiduLat) {
		this.baiduLat = baiduLat;
	}

	public String getDtPath() {
		return dtPath;
	}

	public void setDtPath(String dtPath) {
		this.dtPath = dtPath;
	}

	public Double getVesselDiameter() {
		return vesselDiameter;
	}

	public void setVesselDiameter(Double vesselDiameter) {
		this.vesselDiameter = vesselDiameter;
	}

	public String getFireLevel() {
		return fireLevel;
	}

	public void setFireLevel(String fireLevel) {
		this.fireLevel = fireLevel;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
