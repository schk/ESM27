package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_equipment_type")
public class EquipmentType extends BaseModel{
	
	/**
	 * 生产装置类型
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "name_")
	private String name;
	
	@TableField(value = "type_")
	private Long type;
	
	@TableField(value = "code_")
	private String code;
	
	@TableField(value="jh_path")
	private String jhPath;
	
	@TableField(value="dt_path")
	private String dtPath;
	
	@TableField(value = "remark_")
	private String remark;
	
	@TableField(exist = false)
	private String typeName;
	
	public String getDtPath() {
		return dtPath;
	}

	public void setDtPath(String dtPath) {
		this.dtPath = dtPath;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getJhPath() {
		return jhPath;
	}

	public void setJhPath(String jhPath) {
		this.jhPath = jhPath;
	}
	
	
}
