package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_emergency_material_type")
public class EmergencyMaterialType  extends BaseModel{

	/**
	 * 应急物资类型表
	 */
	@TableField(exist=false)
	private static final long serialVersionUID = 1834297379813834397L;
	
	@TableField(value="name_")
	private String name;
	
	@TableField(value="remark_")
	private String remark;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "EmergencyMaterialType [name=" + name + ", remark=" + remark + "]";
	}
	
	
}
