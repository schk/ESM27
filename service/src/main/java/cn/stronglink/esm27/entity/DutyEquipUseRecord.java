package cn.stronglink.esm27.entity;

import java.util.Date;

import cn.stronglink.core.base.BaseModel;


public class DutyEquipUseRecord  extends BaseModel {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1913919697787209892L;

	private Long id;

    private Long dutyEquipId;

    private Long roomId;

    private Integer count;

    private Date createTime;

    private Long createBy;

    private Date updateTime;

    private Long updateBy;

    public DutyEquipUseRecord(Long id, Long dutyEquipId, Long roomId, Integer count, Date createTime, Long createBy, Date updateTime, Long updateBy) {
        this.id = id;
        this.dutyEquipId = dutyEquipId;
        this.roomId = roomId;
        this.count = count;
        this.createTime = createTime;
        this.createBy = createBy;
        this.updateTime = updateTime;
        this.updateBy = updateBy;
    }

    public DutyEquipUseRecord() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDutyEquipId() {
        return dutyEquipId;
    }

    public void setDutyEquipId(Long dutyEquipId) {
        this.dutyEquipId = dutyEquipId;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }
}