package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_incident_record_file_ref")
public class IncidentRecordFileRef extends BaseModel{
	
	/**
	 * 操作记录文件表
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "file_name")
	private String fileName;
	
	@TableField(value = "path_")
	private String path;
	
	@TableField(value = "incident_record_id")
	private Long incidentRecordId;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Long getIncidentRecordId() {
		return incidentRecordId;
	}

	public void setIncidentRecordId(Long incidentRecordId) {
		this.incidentRecordId = incidentRecordId;
	}

	
}
