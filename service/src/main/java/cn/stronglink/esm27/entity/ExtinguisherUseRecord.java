package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_extinguisher_use_record")
public class ExtinguisherUseRecord extends BaseModel{

	/**
	 * 灭火剂使用记录表
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = -7398516212365479088L;
	
	@TableField(value="room_id")
	private Long roomId;
	
	@TableField(value="foam_type_id")
	private Long foamTypeId;
	
	@TableField(value="fire_engine_id")
	private Long fireEngineId;
	
	@TableField(value="used_amount")
	private Double usedAmount;

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public Long getFoamTypeId() {
		return foamTypeId;
	}

	public void setFoamTypeId(Long foamTypeId) {
		this.foamTypeId = foamTypeId;
	}

	public Long getFireEngineId() {
		return fireEngineId;
	}

	public void setFireEngineId(Long fireEngineId) {
		this.fireEngineId = fireEngineId;
	}

	public Double getUsedAmount() {
		return usedAmount;
	}

	public void setUsedAmount(Double usedAmount) {
		this.usedAmount = usedAmount;
	}
	
	
	
	
}
