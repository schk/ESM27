package cn.stronglink.esm27.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_user")
public class User extends BaseModel{
	
	/**
	 * 用户表
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "account_")
	private String account;
	
	@TableField(value = "password_")
	private String password;
	
	@TableField(value = "name_")
	private String name;
	
	@TableField(value = "sex_")
	private Integer sex;
	
	@TableField(value = "phone_")
	private String phone;
	
	@TableField(value = "employee_no")
	private String employeeNo;
	
	@TableField(value = "position_")
	private String position;
	
	@TableField(value = "email_")
	private String email;
	
	@TableField(value = "status_")
	private Integer status;
	
	@TableField(value = "is_sys")
	private boolean isSys;
	
	@TableField(value = "admin_")
	private boolean admin;

	@TableField(value = "dept_id")
    private Long deptId;
	
	@TableField(value = "remarks_")
	private String remarks;
	
	@TableField(exist = false)
    private Long fireBrigadeId;
	
    @TableField(exist = false)
    private List<Long> roles;
    
    @TableField(value = "salt_")
    private String salt;
    
    @TableField(exist = false)
	private String deptName;
    
    @TableField(exist = false)
	private String roleName;
    
    @TableField(exist = false)
	private String fireBrigadeIdName;
    

	@TableField(exist = false)
    private Set<String> roleset = new HashSet<>();    //用户所有角色值，用于shiro做角色权限的判断
    
    @TableField(exist = false)
    private Set<String> permset = new HashSet<>();    //用户所有权限值，用于shiro做资源权限的判断

	public Long getFireBrigadeId() {
		return fireBrigadeId;
	}

	public void setFireBrigadeId(Long fireBrigadeId) {
		this.fireBrigadeId = fireBrigadeId;
	}
	
	public String getFireBrigadeIdName() {
		return fireBrigadeIdName;
	}

	public void setFireBrigadeIdName(String fireBrigadeIdName) {
		this.fireBrigadeIdName = fireBrigadeIdName;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}
	 public String getDeptName() {
			return deptName;
		}

		public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

		public void setDeptName(String deptName) {
			this.deptName = deptName;
		}
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmployeeNo() {
		return employeeNo;
	}

	public void setEmployeeNo(String employeeNo) {
		this.employeeNo = employeeNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public boolean isSys() {
		return isSys;
	}

	public void setSys(boolean isSys) {
		this.isSys = isSys;
	}

	

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public Long getDeptId() {
		return deptId;
	}

	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	public String getDeptId_() {
		return deptId == null ? "" : deptId.toString();
	}
	public List<Long> getRoles() {
		return roles;
	}

	public void setRoles(List<Long> roles) {
		this.roles = roles;
	}	

	public List<String> getRoles_() {
		if (this.roles!= null && this.roles.size() > 0) {
			List<String> list = new ArrayList<String>(this.roles.size());
			for (int i = 0, j = this.roles.size(); i < j; i++) {
				list.add(this.roles.get(i).toString());
			}
			return list;
		} else {
			return null;
		}
	}
	
	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public Set<String> getRoleset() {
		return roleset;
	}

	public void setRoleset(Set<String> roleset) {
		this.roleset = roleset;
	}

	public Set<String> getPermset() {
		return permset;
	}

	public void setPermset(Set<String> permset) {
		this.permset = permset;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
}
