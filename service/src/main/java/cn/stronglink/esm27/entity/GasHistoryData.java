package cn.stronglink.esm27.entity;

public class GasHistoryData{

	private Long gasDateId;//设备数据ID
	
	private Double value;//值
	
	private Integer isAlert;//是否报警（1未报警，2报警）
	
	private Integer gasId;
	
	public Integer getGasId() {
		return gasId;
	}

	public void setGasId(Integer gasId) {
		this.gasId = gasId;
	}

	public Long getGasDateId() {
		return gasDateId;
	}

	public void setGasDateId(Long gasDateId) {
		this.gasDateId = gasDateId;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public Integer getIsAlert() {
		return isAlert;
	}

	public void setIsAlert(Integer isAlert) {
		this.isAlert = isAlert;
	}
	
	
}
