package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_permission")
public class Permission extends BaseModel{

	/**
	 * 
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "name_")
	private String name;
	
	@TableField(value = "code_")
	private String code;
	
	@TableField(value = "pid_")
	private Long pid;
	
	@TableField(value = "path_")
	private String path;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	

}
