package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_dictionary")
public class Dictionary  extends BaseModel{

	/**
	 * 所有类型表
	 */
	@TableField(exist=false)
	private static final long serialVersionUID = -5850940994391594515L;
	
	@TableField(value="name_")
	private String name;
	
	@TableField(value="code_")
	private String code;
	
	@TableField(value="type_")
	private Integer type;
	
	@TableField(value="p_code")
	private String pCode;
	
	@TableField(value="pid_")
	private Long pid;
	
	@TableField(value="sort_")
	private int sort;
	
	@TableField(value="dt_path")
	private String dtPath;
	
	@TableField(value="path_")
	private String path;
	
	@TableField(value="jh_path")
	private String jhPath;
	
	@TableField(value="remark_")
	private String  remark;

	public String getDtPath() {
		return dtPath;
	}

	public void setDtPath(String dtPath) {
		this.dtPath = dtPath;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getpCode() {
		return pCode;
	}

	public void setpCode(String pCode) {
		this.pCode = pCode;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getJhPath() {
		return jhPath;
	}

	public void setJhPath(String jhPath) {
		this.jhPath = jhPath;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}
	
	

}
