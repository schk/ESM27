package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_accident_case_file")
public class AccidentCaseFile extends BaseModel{
	
	/**
	 * 重点单位图片
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "file_name")
	private String fileName;
	
	@TableField(value = "path_")
	private String path;
	
	@TableField(value = "swf_path")
	private String swfPath;
	
	@TableField(value = "accident_case_id")
	private Long accidentCaseId;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getSwfPath() {
		return swfPath;
	}

	public void setSwfPath(String swfPath) {
		this.swfPath = swfPath;
	}

	public Long getAccidentCaseId() {
		return accidentCaseId;
	}

	public void setAccidentCaseId(Long accidentCaseId) {
		this.accidentCaseId = accidentCaseId;
	}



		
	
}
