package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_camera_set")
public class CameraSet extends BaseModel{
	
	/**
	 *
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	@TableField(exist = false)
	private String oaccount;
	@TableField(exist = false)
	private String opassword;
	@TableField(exist = false)
	private String oip;
	
	public String getOaccount() {
		return oaccount;
	}

	public void setOaccount(String oaccount) {
		this.oaccount = oaccount;
	}

	public String getOpassword() {
		return opassword;
	}

	public void setOpassword(String opassword) {
		this.opassword = opassword;
	}

	public String getOip() {
		return oip;
	}

	public void setOip(String oip) {
		this.oip = oip;
	}

	@TableField(value = "serial_")
	private String serial;
	
	@TableField(value = "ip_")
	private String ip;
	
	@TableField(value = "port_")
	private String port;
	
	@TableField(value = "account_")
	private String account;
	
	@TableField(value = "password_")
	private String password;
	
	@TableField(value = "jsession_id")
	private String jsessionId;

	@TableField(value="device_no")
	private String deviceNo;
	
	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getJsessionId() {
		return jsessionId;
	}

	public void setJsessionId(String jsessionId) {
		this.jsessionId = jsessionId;
	}

	public String getDeviceNo() {
		return deviceNo;
	}

	public void setDeviceNo(String deviceNo) {
		this.deviceNo = deviceNo;
	}

	
	
	
	
}
