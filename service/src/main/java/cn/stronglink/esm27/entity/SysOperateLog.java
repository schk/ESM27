package cn.stronglink.esm27.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import cn.stronglink.core.base.BaseModel;

@TableName("t_sys_operate_log")
public class SysOperateLog extends BaseModel{

	/**
	 * 
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;

	@TableField(value = "user_name")
	private String userName;
	
	@TableField(value = "act_name")
	private String actName;
	
	@TableField(value = "operate_type")
	private String operateType;
	
	@TableField(value = "module_")
	private String module;
	
	@TableField(value = "args_")
	private String args;
	
	@TableField(value = "description_")
	private String description;
	
	@TableField(value = "operate_time")
	private Date operateTime;
	
	@TableField(value = "ip_")
	private String ip;


	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getActName() {
		return actName;
	}

	public void setActName(String actName) {
		this.actName = actName;
	}

	public String getOperateType() {
		return operateType;
	}

	public void setOperateType(String operateType) {
		this.operateType = operateType;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getArgs() {
		return args;
	}

	public void setArgs(String args) {
		this.args = args;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date getOperateTime() {
		return operateTime;
	}

	public void setOperateTime(Date operateTime) {
		this.operateTime = operateTime;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

}
