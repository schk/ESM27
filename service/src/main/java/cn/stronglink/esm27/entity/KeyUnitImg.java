package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_key_unit_img")
public class KeyUnitImg extends BaseModel{
	
	/**
	 * 重点单位图片
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "path_")
	private String path;
	
	@TableField(value = "key_unit_id")
	private Long keyUnitId;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Long getKeyUnitId() {
		return keyUnitId;
	}

	public void setKeyUnitId(Long keyUnitId) {
		this.keyUnitId = keyUnitId;
	}

		
	
}
