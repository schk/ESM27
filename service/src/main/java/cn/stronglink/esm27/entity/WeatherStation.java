package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_weather_station")
public class WeatherStation extends BaseModel {

	/**
	 *
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "station_port")
	private String stationPort;//气象站编号

	public String getStationPort() {
		return stationPort;
	}

	public void setStationPort(String stationPort) {
		this.stationPort = stationPort;
	}
	
}
