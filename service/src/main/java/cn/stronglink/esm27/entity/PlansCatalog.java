package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;

import cn.stronglink.core.base.BaseModel;

public class PlansCatalog  extends BaseModel{
	/**
	 * 
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "name_")
	private String name;
	
	@TableField(value = "plans_id")
	private Long	plansId;
	@TableField(value = "page_")
	private Integer	page;
	@TableField(value = "sort_")
	private Integer	sort;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Long getPlansId() {
		return plansId;
	}
	public void setPlansId(Long plansId) {
		this.plansId = plansId;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
}