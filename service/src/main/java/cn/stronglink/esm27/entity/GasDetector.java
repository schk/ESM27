package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_gas_detector")
public class GasDetector extends BaseModel {
	
	/**
	 *
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "serial_")
	private String serial;//检测仪编号
	
	@TableField(value = "fire_brigede_id")
	private Long fireBrigedeId;//战队ID
	
	@TableField(value = "is_del")
	private Boolean shiDel;//是否删除

	public Long getFireBrigedeId() {
		return fireBrigedeId;
	}

	public void setFireBrigedeId(Long fireBrigedeId) {
		this.fireBrigedeId = fireBrigedeId;
	}

	public Boolean getShiDel() {
		return shiDel;
	}

	public void setShiDel(Boolean shiDel) {
		this.shiDel = shiDel;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}
	
}
