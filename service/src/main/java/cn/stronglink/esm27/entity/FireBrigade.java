package cn.stronglink.esm27.entity;

import java.util.List;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;
import cn.stronglink.esm27.module.extinguisher.vo.ExtinguisherVo;
import cn.stronglink.esm27.module.fireEngine.vo.FireEngineVo;
import cn.stronglink.esm27.module.plan.vo.PlanVo;

@TableName("t_fire_brigade")
public class FireBrigade extends BaseModel{
	
	/**
	 * 消防队
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "name_")
	private String name;
	
	@TableField(value = "rescue_ability")
	private String rescueAbility;
	
	@TableField(value = "position_")
	private String position;
	
	@TableField(value = "station_level")
	private String stationLevel;
	
	@TableField(value = "general_situation")
	private String generalSituation;
	
	@TableField(value = "lon_")
	private Double lon;
	
	@TableField(value = "lat_")
	private Double lat;
	
	@TableField(value = "baidu_lon")
	private Double baiduLon;
	
	@TableField(value = "baidu_lat")
	private Double baiduLat;	
	
	@TableField(value = "properties_")
	private String properties;
	
	@TableField(value = "scale_")
	private String scale;
	
	@TableField(value = "charge_")
	private String charge;
	
	@TableField(value = "dt_path")
	private String dtPath;
	
	@TableField(value = "phone_")
	private String phone;

	@TableField(value = "remark_")
	private String remark;
	
	@TableField(value = "pid_")
	private String pid;
	
	@TableField(value = "sort_")
	private Integer sort;
	
	@TableField(exist = false)
	private String distances;
	
	@TableField(exist = false)
	private String lonLat;
	
	@TableField(exist = false)
	private String parentName;
	
	@TableField(exist = false)
	private FireEngineVo carCount;
	
	@TableField(exist = false)
	private List<PlanVo> planList;
	
	@TableField(exist = false)
	private List<ExtinguisherVo> extinguisherList;
	
	@TableField(exist = false)
	private List<FireEngineVo> miehuojList;
	
	
	public List<FireEngineVo> getMiehuojList() {
		return miehuojList;
	}

	public void setMiehuojList(List<FireEngineVo> miehuojList) {
		this.miehuojList = miehuojList;
	}

	public String getStationLevel() {
		return stationLevel;
	}

	public void setStationLevel(String stationLevel) {
		this.stationLevel = stationLevel;
	}

	public String getGeneralSituation() {
		return generalSituation;
	}

	public void setGeneralSituation(String generalSituation) {
		this.generalSituation = generalSituation;
	}

	public List<ExtinguisherVo> getExtinguisherList() {
		return extinguisherList;
	}

	public void setExtinguisherList(List<ExtinguisherVo> extinguisherList) {
		this.extinguisherList = extinguisherList;
	}

	public List<PlanVo> getPlanList() {
		return planList;
	}

	public void setPlanList(List<PlanVo> planList) {
		this.planList = planList;
	}

	public FireEngineVo getCarCount() {
		return carCount;
	}

	public void setCarCount(FireEngineVo carCount) {
		this.carCount = carCount;
	}

	public String getDistances() {
		return distances;
	}

	public void setDistances(String distances) {
		this.distances = distances;
	}

	public String getLonLat() {
		return lonLat;
	}

	public void setLonLat(String lonLat) {
		this.lonLat = lonLat;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getProperties() {
		return properties;
	}

	public void setProperties(String properties) {
		this.properties = properties;
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public String getCharge() {
		return charge;
	}

	public void setCharge(String charge) {
		this.charge = charge;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getBaiduLon() {
		return baiduLon;
	}

	public void setBaiduLon(Double baiduLon) {
		this.baiduLon = baiduLon;
	}

	public Double getBaiduLat() {
		return baiduLat;
	}

	public void setBaiduLat(Double baiduLat) {
		this.baiduLat = baiduLat;
	}

	public String getRescueAbility() {
		return rescueAbility;
	}

	public void setRescueAbility(String rescueAbility) {
		this.rescueAbility = rescueAbility;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getDtPath() {
		return dtPath;
	}

	public void setDtPath(String dtPath) {
		this.dtPath = dtPath;
	}

	
	
}
