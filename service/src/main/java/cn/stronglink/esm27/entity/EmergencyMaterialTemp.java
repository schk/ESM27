package cn.stronglink.esm27.entity;


import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;
@TableName("t_emergency_material_temp")
public class EmergencyMaterialTemp extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5186013474885381665L;
	/** 物资名称 */
	@TableField(value = "name_")
	private String name; 
	/** 物资用途 */
	@TableField(value = "purpose_")
	private String purpose; 
	/** 物资描述 */
	@TableField(value = "remark_")
	private String remark; 
	
	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	/** 得到物资名称 */
	public String getName() {
		return name;
	}

	/** 设置物资名称 */
	public void setName(String name) {
		this.name = name;
	}
    

	/** 物资储备点 */
	@TableField(value = "reserve_point_id")
	private String reservePointId; 
	
	/** 得到物资储备点 */
	public String getReservePointId() {
		return reservePointId;
	}

	/** 设置物资储备点 */
	public void setReservePointId(String reservePointId) {
		this.reservePointId = reservePointId;
	}
    

	/** 规格 */
	@TableField(value = "specifications_")
	private String specifications; 
	
	/** 得到规格 */
	public String getSpecifications() {
		return specifications;
	}

	/** 设置规格 */
	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}
    

	/** 型号 */
	@TableField(value = "model_")
	private String model; 
	
	/** 得到型号 */
	public String getModel() {
		return model;
	}

	/** 设置型号 */
	public void setModel(String model) {
		this.model = model;
	}
    

	/** 种类 */
	@TableField(value = "type_id")
	private String typeId; 
	
	/** 得到种类 */
	public String getTypeId() {
		return typeId;
	}

	/** 设置种类 */
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
    

	/** 存储数量 */
	@TableField(value = "storage_quantity")
	private String storageQuantity; 
	
	/** 得到存储数量 */
	public String getStorageQuantity() {
		return storageQuantity;
	}

	/** 设置存储数量 */
	public void setStorageQuantity(String storageQuantity) {
		this.storageQuantity = storageQuantity;
	}
    

	/** 存储单位 */
	@TableField(value = "storage_unit")
	private String storageUnit; 
	
	/** 得到存储单位 */
	public String getStorageUnit() {
		return storageUnit;
	}

	/** 设置存储单位 */
	public void setStorageUnit(String storageUnit) {
		this.storageUnit = storageUnit;
	}
    

	/** 负责人 */
	@TableField(value = "charge_person")
	private String chargePerson; 
	
	/** 得到负责人 */
	public String getChargePerson() {
		return chargePerson;
	}

	/** 设置负责人 */
	public void setChargePerson(String chargePerson) {
		this.chargePerson = chargePerson;
	}
    

	/** 尺寸大小 */
	@TableField(value = "size_")
	private String size; 
	
	/** 得到尺寸大小 */
	public String getSize() {
		return size;
	}

	/** 设置尺寸大小 */
	public void setSize(String size) {
		this.size = size;
	}
    

	/** 电话 */
	@TableField(value = "phone_")
	private String phone; 
	
	/** 得到电话 */
	public String getPhone() {
		return phone;
	}

	/** 设置电话 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
    

	/** 0不可用    1可用 */
	@TableField(value = "available_")
	private String available; 
	
	/** 得到0不可用    1可用 */
	public String getAvailable() {
		return available;
	}

	/** 设置0不可用    1可用 */
	public void setAvailable(String available) {
		this.available = available;
	}
	
	
	@TableField(value = "timestamp_")
	private Long timestamp;

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	} 

	@TableField(value="intelligent_recommendation")
	private Integer intelligentRecommendation;
	
	@TableField(value="disaster_type")
	private String disasterType;

	public Integer getIntelligentRecommendation() {
		return intelligentRecommendation;
	}

	public void setIntelligentRecommendation(Integer intelligentRecommendation) {
		this.intelligentRecommendation = intelligentRecommendation;
	}

	public String getDisasterType() {
		return disasterType;
	}

	public void setDisasterType(String disasterType) {
		this.disasterType = disasterType;
	}
	
	

}
