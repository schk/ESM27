package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_key_parts_dangers")
public class KeyPartsDangers extends BaseModel{
	
	/**
	 * 消防队
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "key_parts_id")
	private Long keyPartsId;
	
	@TableField(value = "dangers_id")
	private Long dangersId;

	public Long getKeyPartsId() {
		return keyPartsId;
	}

	public void setKeyPartsId(Long keyPartsId) {
		this.keyPartsId = keyPartsId;
	}

	public Long getDangersId() {
		return dangersId;
	}

	public void setDangersId(Long dangersId) {
		this.dangersId = dangersId;
	}
	
	
}
