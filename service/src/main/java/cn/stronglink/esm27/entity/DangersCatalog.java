package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_dangers_catalog")
public class DangersCatalog extends BaseModel{
	
	/**
	 *危化品文档目录
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "name_")
	private String name;
	
	@TableField(value = "dangers_id")
	private Long dangersId;
	
	@TableField(value = "page_")
	private String page;
	
	@TableField(value = "sort_")
	private String sort;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}
	public Long getDangersId() {
		return dangersId;
	}

	public void setDangersId(Long dangersId) {
		this.dangersId = dangersId;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}
	
}
