package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_teams")
public class Teams extends BaseModel{
	
	/**
	 * 应急队伍
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "name_")
	private String name;
	
	@TableField(value = "position_")
	private String position;
	
	@TableField(value = "lon_")
	private Double lon;
	
	@TableField(value = "lat_")
	private Double lat;
	
	@TableField(value = "baidu_lon")
	private Double baiduLon;
	
	@TableField(value = "baidu_lat")
	private Double baiduLat;
	
	@TableField(value = "properties_")
	private String properties;
	
	@TableField(value = "scale_")
	private String scale;
	
	@TableField(value = "charge_")
	private String charge;
	
	@TableField(value = "phone_")
	private String phone;
	
	@TableField(value = "dt_path")
	private String dtPath;
	
	@TableField(value = "remark_")
	private String remark;
	
	@TableField(exist = false)
	private String distances;
	
	@TableField(exist = false)
	private String lonLat;
	
	
	public String getDistances() {
		return distances;
	}

	public void setDistances(String distances) {
		this.distances = distances;
	}

	public String getDtPath() {
		return dtPath;
	}

	public void setDtPath(String dtPath) {
		this.dtPath = dtPath;
	}

	public String getLonLat() {
		return lonLat;
	}

	public void setLonLat(String lonLat) {
		this.lonLat = lonLat;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getProperties() {
		return properties;
	}

	public void setProperties(String properties) {
		this.properties = properties;
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public String getCharge() {
		return charge;
	}

	public void setCharge(String charge) {
		this.charge = charge;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Double getBaiduLon() {
		return baiduLon;
	}

	public void setBaiduLon(Double baiduLon) {
		this.baiduLon = baiduLon;
	}

	public Double getBaiduLat() {
		return baiduLat;
	}

	public void setBaiduLat(Double baiduLat) {
		this.baiduLat = baiduLat;
	}
	
}
