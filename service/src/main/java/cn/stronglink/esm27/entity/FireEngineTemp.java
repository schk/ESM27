package cn.stronglink.esm27.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_fire_engine_temp")
public class FireEngineTemp extends BaseModel {

	/**
	 * 消防车表
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 6546817864878815166L;
	
	@TableField(value = "name_")
	private String name;
	
	@TableField(value = "fire_brigade_id")
	private Long fireBrigadeId;
	
	@TableField(value = "dic_id")
	private String dicId;
	
	@TableField(value = "model_")
	private String model;
	
	@TableField(value = "code_")
	private String code;
	
	@TableField(value = "plate_number")
	private String plateNumber;
	
	@TableField(value = "full_weight")
	private BigDecimal fullWeight;
	
	@TableField(value = "car_size")
	private String carSize;
	
	@TableField(value = "status_")
	private Integer status;
	
	@TableField(value = "vehicular_artillery")
	private Double vehicularArtillery;
	
	@TableField(value = "foam_type_id")
	private Long foamTypeId;
	
	@TableField(value = "spray_speed")
	private Double spraySpeed;
	
	@TableField(value = "capacity_")
	private Double capacity;
	
	@TableField(value = "water_")
	private Double water;
	
	@TableField(value = "gun_range")
	private Double gunRange;
	
	@TableField(value = "type_id")
	private String typeId;
	@TableField(value = "lifting_height")
	private Double liftingHeight;
	@TableField(value = "pump_flow")
	private Double pumpFlow;
	@TableField(value = "chassis_model")
	private String chassisModel;
	@TableField(value = "investment_date")
	private Date investmentDate;
	@TableField(value = "manufacturer_")
	private String manufacturer;
	@TableField(value = "state_of_duty")
	private Integer stateOfDuty;
	@TableField(value = "foam_type_id_o")
	private Long foamTypeIdO;
	@TableField(value = "powder_quantity")
	private Double powderQuantity;
	@TableField(value="timestamp_")
	private Long timestamp;

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public BigDecimal getFullWeight() {
		return fullWeight;
	}

	public void setFullWeight(BigDecimal fullWeight) {
		this.fullWeight = fullWeight;
	}

	public String getCarSize() {
		return carSize;
	}

	public void setCarSize(String carSize) {
		this.carSize = carSize;
	}

	public Double getPowderQuantity() {
		return powderQuantity;
	}

	public void setPowderQuantity(Double powderQuantity) {
		this.powderQuantity = powderQuantity;
	}

	public Double getVehicularArtillery() {
		return vehicularArtillery;
	}

	public void setVehicularArtillery(Double vehicularArtillery) {
		this.vehicularArtillery = vehicularArtillery;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getFireBrigadeId() {
		return fireBrigadeId;
	}

	public void setFireBrigadeId(Long fireBrigadeId) {
		this.fireBrigadeId = fireBrigadeId;
	}

	public String getDicId() {
		return dicId;
	}

	public void setDicId(String returnStr) {
		this.dicId = returnStr;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPlateNumber() {
		return plateNumber;
	}

	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getFoamTypeId() {
		return foamTypeId;
	}

	public void setFoamTypeId(Long foamTypeId) {
		this.foamTypeId = foamTypeId;
	}

	public Double getSpraySpeed() {
		return spraySpeed;
	}

	public void setSpraySpeed(Double spraySpeed) {
		this.spraySpeed = spraySpeed;
	}

	public Double getCapacity() {
		return capacity;
	}

	public void setCapacity(Double capacity) {
		this.capacity = capacity;
	}

	public Double getWater() {
		return water;
	}

	public void setWater(Double water) {
		this.water = water;
	}

	public Double getGunRange() {
		return gunRange;
	}

	public void setGunRange(Double gunRange) {
		this.gunRange = gunRange;
	}

	public Double getLiftingHeight() {
		return liftingHeight;
	}

	public void setLiftingHeight(Double liftingHeight) {
		this.liftingHeight = liftingHeight;
	}

	public Double getPumpFlow() {
		return pumpFlow;
	}

	public void setPumpFlow(Double pumpFlow) {
		this.pumpFlow = pumpFlow;
	}

	public String getChassisModel() {
		return chassisModel;
	}

	public void setChassisModel(String chassisModel) {
		this.chassisModel = chassisModel;
	}

	public Date getInvestmentDate() {
		return investmentDate;
	}

	public void setInvestmentDate(Date investmentDate) {
		this.investmentDate = investmentDate;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Integer getStateOfDuty() {
		return stateOfDuty;
	}

	public void setStateOfDuty(Integer stateOfDuty) {
		this.stateOfDuty = stateOfDuty;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public Long getFoamTypeIdO() {
		return foamTypeIdO;
	}

	public void setFoamTypeIdO(Long foamTypeIdO) {
		this.foamTypeIdO = foamTypeIdO;
	}

}
