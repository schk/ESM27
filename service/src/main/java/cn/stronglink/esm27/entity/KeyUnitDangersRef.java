package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_key_unit_dangers_ref")
public class KeyUnitDangersRef extends BaseModel{
	
	/**
	 * 消防队
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "key_unit_id")
	private Long keyUnitId;
	
	@TableField(value = "dangers_id")
	private Long dangersId;

	public Long getKeyUnitId() {
		return keyUnitId;
	}

	public void setKeyUnitId(Long keyUnitId) {
		this.keyUnitId = keyUnitId;
	}

	public Long getDangersId() {
		return dangersId;
	}

	public void setDangersId(Long dangersId) {
		this.dangersId = dangersId;
	}
	
	
}
