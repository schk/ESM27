package cn.stronglink.esm27.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

@TableName("t_accident")
public class Accident {
	
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableId(value = "id_", type = IdType.INPUT)
	private Long id;
	
	@TableField(value = "name_")
	private String name;
	
	@TableField(value = "room_id")
	private String roomId;
	
	@TableField(value = "desc_")
	private String desc;
	
	@TableField(value = "ground_height")
	private String groundHeight;
	
	@TableField(value = "type_")
	private String type;
	
	@TableField(value = "lon_")
	private Double lon;
	
	@TableField(value = "lat_")
	private Double lat;
	
	@TableField(value = "create_time")
	private Date createTime;

	@TableField(value = "content_")
	private String content;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getGroundHeight() {
		return groundHeight;
	}

	public void setGroundHeight(String groundHeight) {
		this.groundHeight = groundHeight;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	} 
	
	
	
}
