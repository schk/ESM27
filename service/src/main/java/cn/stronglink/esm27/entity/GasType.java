package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_gas_type")
public class GasType  extends BaseModel{

	/**
	 *
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "unit_")
	private String unit;//单位
	
	@TableField(value = "low_")
	private Double low;//下限
	
	@TableField(value = "up_")
	private Double up;//上限
	
	@TableField(value = "name_")
	private String name;//名字

	@TableField(value = "del_")
	private Boolean del;//是否删除
	
	@TableField(exist = false)
	private String titleKey;
	
	@TableField(value = "p_Low")
	private Double pLow;
	
	@TableField(value = "p_up")
	private Double pUp;
	
	public Double getpLow() {
		return pLow;
	}

	public void setpLow(Double pLow) {
		this.pLow = pLow;
	}

	public Double getpUp() {
		return pUp;
	}

	public void setpUp(Double pUp) {
		this.pUp = pUp;
	}

	public String getTitleKey() {
		return titleKey;
	}

	public void setTitleKey(String titleKey) {
		this.titleKey = titleKey;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getLow() {
		return low;
	}

	public void setLow(Double low) {
		this.low = low;
	}

	public Double getUp() {
		return up;
	}

	public void setUp(Double up) {
		this.up = up;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getDel() {
		return del;
	}
	
	public String getDel_() {
		return del==null?"0":del?"1":"0";
	}

	public void setDel(Boolean del) {
		this.del = del;
	}
	
}
