package cn.stronglink.esm27.entity;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;
import cn.stronglink.core.util.AntdFile;

@TableName("t_accident_case")
public class AccidentCase extends BaseModel{
	
	/**
	 * 事故案例
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "name_")
	private String name;
	
	@TableField(value = "desc_")
	private String desc;
	
	@TableField(value = "type_id")
	private Long typeId;
	
	@TableField(value="time_")
	private Date time;
	
	@TableField(exist = false)
	private List<AntdFile> fileList;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public List<AntdFile> getFileList() {
		return fileList;
	}

	public void setFileList(List<AntdFile> fileList) {
		this.fileList = fileList;
	}

}
