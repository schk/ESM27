package cn.stronglink.esm27.entity;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import cn.stronglink.core.util.AntdFile;

@TableName("t_incident_record")
public class IncidentRecord {
	/**
	 * 应急指挥记录
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1421301688225365334L;
	
	@TableId(value = "id_", type = IdType.INPUT)
	private Long id;
		
	@TableField(value = "director_")
	private String director;
	
	@TableField(value = "type_")
	private Integer type;
	
	@TableField(value = "publisher_")
	private String publisher;
	@TableField(value = "status_")
	private Integer status;
	@TableField(value = "feedback_")
	private String feedback;
	@TableField(value = "feedback_desc")
	private String feedbackDesc;
	
	@TableField(value = "room_id")
	private Long roomId;
	
	@TableField(value = "action_")
	private String action;
	
	@TableField(value = "action_desc")
	private String actionDesc;
	
	@TableField(value = "content_")
	private String content;
	
	@TableField(value = "create_time")
	private Date createTime;
	
	@TableField(value = "create_by")
	private Long createBy;
	
	@TableField(value = "time_difference")
    private String timeDifference;

	@TableField(value = "is_delete")
    private int isDelete;
	
	
	@TableField(exist = false)
	private List<AntdFile> fileList;
	
	public List<AntdFile> getFileList() {
		return fileList;
	}

	public void setFileList(List<AntdFile> fileList) {
		this.fileList = fileList;
	}

	public String getFeedbackDesc() {
		return feedbackDesc;
	}

	public void setFeedbackDesc(String feedbackDesc) {
		this.feedbackDesc = feedbackDesc;
	}

	public int getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	public String getTimeDifference() {
		return timeDifference;
	}

	public void setTimeDifference(String timeDifference) {
		this.timeDifference = timeDifference;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Long createBy) {
		this.createBy = createBy;
	}

	public String getActionDesc() {
		return actionDesc;
	}

	public void setActionDesc(String actionDesc) {
		this.actionDesc = actionDesc;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	
}
