package cn.stronglink.esm27.entity;

import java.util.List;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_key_unit_dangers")
public class KeyUnitDangers extends BaseModel{

	/**
	 *企业危化品管理
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(exist = false)
	private String typeName;
	
	@TableField(value = "type_id")
	private String typeId;
	
	@TableField(value = "project_")
	private String project;
	
	@TableField(value = "key_unit_id")
	private Long keyUnitId;
	
	@TableField(value = "img_path")
	private String imgPath;
	
	@TableField(value = "c_name")
	private String cName;
	
	@TableField(value = "e_name")
	private String eName;
	
	@TableField(value = "c_name2")
	private String cName2;
	
	@TableField(value = "e_name2")
	private String eName2;
	
	@TableField(value = "cas_num")
	private String casNum;
	
	@TableField(value = "un_num")
	private String unNum;
	
	@TableField(value = "msds_path")
	private String msdsPath;
	
	@TableField(value = "swf_path")
	private String swfPath;
	
	@TableField(value = "molecular_formula")
	private String molecularFormula;
	@TableField(value = "molecular_weight")
	private String molecularWeight;
	@TableField(value = "harmful_ingredients")
	private String harmfulIngredients;
	@TableField(value = "content_")
	private String content;
	@TableField(value = "health_hazards")
	private String healthHazards;
	@TableField(value = "environmental_hazards")
	private String environmentalHazards;
	@TableField(value = "fire_explosion_danger")
	private String fireExplosionDanger;
	@TableField(value = "skin_contact")
	private String skinContact;
	@TableField(value = "eye_contact")
	private String eyeContact;
	@TableField(value = "inhalation_")
	private String inhalation;
	@TableField(value = "feed_into")
	private String feedInto;
	@TableField(value = "hazard_characteristics")
	private String hazardCharacteristics;
	@TableField(value = "harmful_combustion_products")
	private String harmfulCombustionProducts;
	@TableField(value = "fire_extinguishing_method")
	private String fireExtinguishingMethod;
	@TableField(value = "emergency_management")
	private String emergencyManagement;
	@TableField(value = "attention_operation")
	private String attentionOperation;
	@TableField(value = "precautions_storage")
	private String precautionsStorage;
	@TableField(value = "china_mac")
	private String chinaMac;
	@TableField(value = "soviet_union_mac")
	private String sovietUnionMac;
	@TableField(value = "tlvtn_")
	private String tlvtn;
	@TableField(value = "tlvwn_")
	private String tlvwn;	
	@TableField(value = "monitoring_method")
	private String monitoringMethod;
	@TableField(value = "engineering_control")
	private String engineeringControl;
	@TableField(value = "respiratory_system_protection")
	private String respiratorySystemProtection;
	@TableField(value = "eye_protection")
	private String eyeProtection;
	@TableField(value = "body_protection")
	private String bodyProtection;
	@TableField(value = "hand_protection")
	private String handProtection;
	@TableField(value = "other_protection")
	private String otherProtection;
	@TableField(value = "main_components")
	private String mainComponents;
	@TableField(value = "appearance_character")
	private String appearanceCharacter;
	@TableField(value = "ph")
	private String ph;
	@TableField(value = "melting_point")
	private String meltingPoint;
	@TableField(value = "expense_point")
	private String expensePoint;
	@TableField(value = "relative_density")
	private String relativeDensity;
	@TableField(value = "relative_steam")
	private String relativeSteam;
	@TableField(value = "saturated_vapor_pressure")
	private String saturatedVaporPressure;
	@TableField(value = "burning_heat")
	private String burningHeat;
	@TableField(value = "critical_temperature")
	private String criticalTemperature;
	@TableField(value = "critical_pressure")
	private String criticalPressure;
	@TableField(value = "water_distribution_coefficient")
	private String waterDistributionCoefficient;
	@TableField(value = "flash_point")
	private String flashPoint;
	@TableField(value = "ignition_temperature")
	private String ignitionTemperature;
	@TableField(value = "upper_limit_explosion")
	private String upperLimitExplosion;
	@TableField(value = "lower_limit_explosion")
	private String lowerLimitEplosion;
	@TableField(value = "solubility_")
	private String solubility;
	@TableField(value = "main_uses")
	private String mainUses;
	@TableField(value = "stability_")
	private String stability;
	@TableField(value = "prohibition_")
	private String prohibition;
	@TableField(value = "avoid_contact")
	private String avoidContact;
	@TableField(value = "acute_toxicity")
	private String acuteToxicity;
	@TableField(value = "discarded_disposal_method")
	private String discardedDisposalMethod;
	@TableField(value = "discarded_notices")
	private String discardedNotices;
	@TableField(value = "dangerous_num")
	private String dangerousNum;
	@TableField(value = "packing_category")
	private String packingCategory;
	@TableField(value = "packing_method")
	private String packingMethod;
	@TableField(value = "attention_transportation")
	private String attentionTransportation;
	@TableField(value = "regulatory_information")
	private String regulatoryInformation;
	@TableField(value = "other_information")
	private String otherInformation;
	@TableField(value = "lower_limit_explosion")
	private String lowerLimitExplosion;
	
	@TableField(exist = false)
	private List<KeyUnitDangersCatalog> keyUnitDangersCatalogs;
	
	public String getOtherInformation() {
		return otherInformation;
	}

	public void setOtherInformation(String otherInformation) {
		this.otherInformation = otherInformation;
	}

	@TableField(value = "file_name")
	private String fileName;

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public String geteName() {
		return eName;
	}

	public void seteName(String eName) {
		this.eName = eName;
	}

	public String getcName2() {
		return cName2;
	}

	public void setcName2(String cName2) {
		this.cName2 = cName2;
	}

	public String geteName2() {
		return eName2;
	}

	public void seteName2(String eName2) {
		this.eName2 = eName2;
	}

	public String getCasNum() {
		return casNum;
	}

	public void setCasNum(String casNum) {
		this.casNum = casNum;
	}

	public String getUnNum() {
		return unNum;
	}

	public void setUnNum(String unNum) {
		this.unNum = unNum;
	}

	public String getMsdsPath() {
		return msdsPath;
	}

	public void setMsdsPath(String msdsPath) {
		this.msdsPath = msdsPath;
	}

	
	public String getMolecularFormula() {
		return molecularFormula;
	}

	public void setMolecularFormula(String molecularFormula) {
		this.molecularFormula = molecularFormula;
	}

	public String getMolecularWeight() {
		return molecularWeight;
	}

	public void setMolecularWeight(String molecularWeight) {
		this.molecularWeight = molecularWeight;
	}

	public String getHarmfulIngredients() {
		return harmfulIngredients;
	}

	public void setHarmfulIngredients(String harmfulIngredients) {
		this.harmfulIngredients = harmfulIngredients;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getHealthHazards() {
		return healthHazards;
	}

	public void setHealthHazards(String healthHazards) {
		this.healthHazards = healthHazards;
	}

	public String getEnvironmentalHazards() {
		return environmentalHazards;
	}

	public void setEnvironmentalHazards(String environmentalHazards) {
		this.environmentalHazards = environmentalHazards;
	}

	public String getFireExplosionDanger() {
		return fireExplosionDanger;
	}

	public void setFireExplosionDanger(String fireExplosionDanger) {
		this.fireExplosionDanger = fireExplosionDanger;
	}

	public String getSkinContact() {
		return skinContact;
	}

	public void setSkinContact(String skinContact) {
		this.skinContact = skinContact;
	}

	public String getEyeContact() {
		return eyeContact;
	}

	public void setEyeContact(String eyeContact) {
		this.eyeContact = eyeContact;
	}

	public String getInhalation() {
		return inhalation;
	}

	public void setInhalation(String inhalation) {
		this.inhalation = inhalation;
	}

	public String getFeedInto() {
		return feedInto;
	}

	public void setFeedInto(String feedInto) {
		this.feedInto = feedInto;
	}

	public String getHazardCharacteristics() {
		return hazardCharacteristics;
	}

	public void setHazardCharacteristics(String hazardCharacteristics) {
		this.hazardCharacteristics = hazardCharacteristics;
	}

	public String getHarmfulCombustionProducts() {
		return harmfulCombustionProducts;
	}

	public void setHarmfulCombustionProducts(String harmfulCombustionProducts) {
		this.harmfulCombustionProducts = harmfulCombustionProducts;
	}

	public String getFireExtinguishingMethod() {
		return fireExtinguishingMethod;
	}

	public void setFireExtinguishingMethod(String fireExtinguishingMethod) {
		this.fireExtinguishingMethod = fireExtinguishingMethod;
	}

	public String getEmergencyManagement() {
		return emergencyManagement;
	}

	public void setEmergencyManagement(String emergencyManagement) {
		this.emergencyManagement = emergencyManagement;
	}

	public String getAttentionOperation() {
		return attentionOperation;
	}

	public void setAttentionOperation(String attentionOperation) {
		this.attentionOperation = attentionOperation;
	}

	public String getPrecautionsStorage() {
		return precautionsStorage;
	}

	public void setPrecautionsStorage(String precautionsStorage) {
		this.precautionsStorage = precautionsStorage;
	}

	public String getChinaMac() {
		return chinaMac;
	}

	public void setChinaMac(String chinaMac) {
		this.chinaMac = chinaMac;
	}

	public String getSovietUnionMac() {
		return sovietUnionMac;
	}

	public void setSovietUnionMac(String sovietUnionMac) {
		this.sovietUnionMac = sovietUnionMac;
	}

	public String getTlvtn() {
		return tlvtn;
	}

	public void setTlvtn(String tlvtn) {
		this.tlvtn = tlvtn;
	}

	public String getTlvwn() {
		return tlvwn;
	}

	public void setTlvwn(String tlvwn) {
		this.tlvwn = tlvwn;
	}

	public String getMonitoringMethod() {
		return monitoringMethod;
	}

	public void setMonitoringMethod(String monitoringMethod) {
		this.monitoringMethod = monitoringMethod;
	}

	public String getEngineeringControl() {
		return engineeringControl;
	}

	public void setEngineeringControl(String engineeringControl) {
		this.engineeringControl = engineeringControl;
	}

	public String getRespiratorySystemProtection() {
		return respiratorySystemProtection;
	}

	public void setRespiratorySystemProtection(String respiratorySystemProtection) {
		this.respiratorySystemProtection = respiratorySystemProtection;
	}

	public String getEyeProtection() {
		return eyeProtection;
	}

	public void setEyeProtection(String eyeProtection) {
		this.eyeProtection = eyeProtection;
	}

	public String getBodyProtection() {
		return bodyProtection;
	}

	public void setBodyProtection(String bodyProtection) {
		this.bodyProtection = bodyProtection;
	}

	public String getHandProtection() {
		return handProtection;
	}

	public void setHandProtection(String handProtection) {
		this.handProtection = handProtection;
	}

	public String getOtherProtection() {
		return otherProtection;
	}

	public void setOtherProtection(String otherProtection) {
		this.otherProtection = otherProtection;
	}

	public String getMainComponents() {
		return mainComponents;
	}

	public void setMainComponents(String mainComponents) {
		this.mainComponents = mainComponents;
	}

	public String getAppearanceCharacter() {
		return appearanceCharacter;
	}

	public void setAppearanceCharacter(String appearanceCharacter) {
		this.appearanceCharacter = appearanceCharacter;
	}

	public String getPh() {
		return ph;
	}

	public void setPh(String ph) {
		this.ph = ph;
	}

	public String getMeltingPoint() {
		return meltingPoint;
	}

	public void setMeltingPoint(String meltingPoint) {
		this.meltingPoint = meltingPoint;
	}

	public String getExpensePoint() {
		return expensePoint;
	}

	public void setExpensePoint(String expensePoint) {
		this.expensePoint = expensePoint;
	}

	public String getRelativeDensity() {
		return relativeDensity;
	}

	public void setRelativeDensity(String relativeDensity) {
		this.relativeDensity = relativeDensity;
	}

	public String getRelativeSteam() {
		return relativeSteam;
	}

	public void setRelativeSteam(String relativeSteam) {
		this.relativeSteam = relativeSteam;
	}

	public String getSaturatedVaporPressure() {
		return saturatedVaporPressure;
	}

	public void setSaturatedVaporPressure(String saturatedVaporPressure) {
		this.saturatedVaporPressure = saturatedVaporPressure;
	}

	public String getBurningHeat() {
		return burningHeat;
	}

	public void setBurningHeat(String burningHeat) {
		this.burningHeat = burningHeat;
	}

	public String getCriticalTemperature() {
		return criticalTemperature;
	}

	public void setCriticalTemperature(String criticalTemperature) {
		this.criticalTemperature = criticalTemperature;
	}

	public String getCriticalPressure() {
		return criticalPressure;
	}

	public void setCriticalPressure(String criticalPressure) {
		this.criticalPressure = criticalPressure;
	}

	public String getWaterDistributionCoefficient() {
		return waterDistributionCoefficient;
	}

	public void setWaterDistributionCoefficient(String waterDistributionCoefficient) {
		this.waterDistributionCoefficient = waterDistributionCoefficient;
	}

	public String getFlashPoint() {
		return flashPoint;
	}

	public void setFlashPoint(String flashPoint) {
		this.flashPoint = flashPoint;
	}

	public String getIgnitionTemperature() {
		return ignitionTemperature;
	}

	public void setIgnitionTemperature(String ignitionTemperature) {
		this.ignitionTemperature = ignitionTemperature;
	}

	public String getUpperLimitExplosion() {
		return upperLimitExplosion;
	}

	public void setUpperLimitExplosion(String upperLimitExplosion) {
		this.upperLimitExplosion = upperLimitExplosion;
	}

	public String getLowerLimitEplosion() {
		return lowerLimitEplosion;
	}

	public void setLowerLimitEplosion(String lowerLimitEplosion) {
		this.lowerLimitEplosion = lowerLimitEplosion;
	}

	public String getSolubility() {
		return solubility;
	}

	public void setSolubility(String solubility) {
		this.solubility = solubility;
	}

	public String getMainUses() {
		return mainUses;
	}

	public void setMainUses(String mainUses) {
		this.mainUses = mainUses;
	}

	public String getStability() {
		return stability;
	}

	public void setStability(String stability) {
		this.stability = stability;
	}

	public String getProhibition() {
		return prohibition;
	}

	public void setProhibition(String prohibition) {
		this.prohibition = prohibition;
	}

	public String getAvoidContact() {
		return avoidContact;
	}

	public void setAvoidContact(String avoidContact) {
		this.avoidContact = avoidContact;
	}

	public String getAcuteToxicity() {
		return acuteToxicity;
	}

	public void setAcuteToxicity(String acuteToxicity) {
		this.acuteToxicity = acuteToxicity;
	}

	public String getDiscardedDisposalMethod() {
		return discardedDisposalMethod;
	}

	public void setDiscardedDisposalMethod(String discardedDisposalMethod) {
		this.discardedDisposalMethod = discardedDisposalMethod;
	}

	public String getDiscardedNotices() {
		return discardedNotices;
	}

	public void setDiscardedNotices(String discardedNotices) {
		this.discardedNotices = discardedNotices;
	}

	public String getDangerousNum() {
		return dangerousNum;
	}

	public void setDangerousNum(String dangerousNum) {
		this.dangerousNum = dangerousNum;
	}

	public String getPackingCategory() {
		return packingCategory;
	}

	public void setPackingCategory(String packingCategory) {
		this.packingCategory = packingCategory;
	}

	public String getPackingMethod() {
		return packingMethod;
	}

	public void setPackingMethod(String packingMethod) {
		this.packingMethod = packingMethod;
	}

	public String getAttentionTransportation() {
		return attentionTransportation;
	}

	public void setAttentionTransportation(String attentionTransportation) {
		this.attentionTransportation = attentionTransportation;
	}

	public String getRegulatoryInformation() {
		return regulatoryInformation;
	}

	public void setRegulatoryInformation(String regulatoryInformation) {
		this.regulatoryInformation = regulatoryInformation;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getSwfPath() {
		return swfPath;
	}

	public void setSwfPath(String swfPath) {
		this.swfPath = swfPath;
	}

	public Long getKeyUnitId() {
		return keyUnitId;
	}

	public void setKeyUnitId(Long keyUnitId) {
		this.keyUnitId = keyUnitId;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public List<KeyUnitDangersCatalog> getKeyUnitDangersCatalogs() {
		return keyUnitDangersCatalogs;
	}

	public void setKeyUnitDangersCatalogs(List<KeyUnitDangersCatalog> keyUnitDangersCatalogs) {
		this.keyUnitDangersCatalogs = keyUnitDangersCatalogs;
	}

	public String getLowerLimitExplosion() {
		return lowerLimitExplosion;
	}

	public void setLowerLimitExplosion(String lowerLimitExplosion) {
		this.lowerLimitExplosion = lowerLimitExplosion;
	}
	


	
}
