package cn.stronglink.esm27.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_expert_temp")
public class ExpertTemp extends BaseModel {

	/**
	 * 专家表
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 5570946317877475957L;

	@TableField(value="name_")
	private String name;
	
	@TableField(value="type_")
	private String type;
	
	@TableField(value="major_")
	private String major;
	
	@TableField(value="speciality_")
	private String speciality;
	
	@TableField(value="phone_")
	private String phone;
	
	@TableField(value="unit_")
	private String unit;
	
	@TableField(value="major_subject")
	private String majorSubject;
	
	@TableField(value="birth_")
	private Date birth;
	
	@TableField(value="address_")
	private String address;
	
	@TableField(value="sex_")
	private String sex;
	
	@TableField(value="timestamp_")
	private Long timestamp;

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getSpeciality() {
		return speciality;
	}

	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getMajorSubject() {
		return majorSubject;
	}

	public void setMajorSubject(String majorSubject) {
		this.majorSubject = majorSubject;
	}
	
	
}
