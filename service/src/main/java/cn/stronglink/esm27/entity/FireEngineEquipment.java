package cn.stronglink.esm27.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_fire_engine_equipment")
public class FireEngineEquipment extends BaseModel {

	/**
	 * 
	 */
	@TableField(exist=false)
	private static final long serialVersionUID = -4463672297202153252L;
	
	@TableField(value="fire_engine_id")
	private Long fireEngineId;
	
	@TableField(value="duty_equip_id")
	private Long dutyEquipId;
	
	@TableField(value="count_")
	private Integer count;
	
	@TableField(value="use_desc")
	private String useDesc;
	
	@TableField(value="follow_date")
	private Date followDate;

	public Long getFireEngineId() {
		return fireEngineId;
	}

	public void setFireEngineId(Long fireEngineId) {
		this.fireEngineId = fireEngineId;
	}

	public Long getDutyEquipId() {
		return dutyEquipId;
	}

	public void setDutyEquipId(Long dutyEquipId) {
		this.dutyEquipId = dutyEquipId;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getUseDesc() {
		return useDesc;
	}

	public void setUseDesc(String useDesc) {
		this.useDesc = useDesc;
	}

	public Date getFollowDate() {
		return followDate;
	}

	public void setFollowDate(Date followDate) {
		this.followDate = followDate;
	}

	@Override
	public String toString() {
		return "FireEngineEquipment [fireEngineId=" + fireEngineId + ", dutyEquipId=" + dutyEquipId + ", count=" + count
				+ ", useDesc=" + useDesc + ", followDate=" + followDate + "]";
	}
	
	

}
