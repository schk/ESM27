package cn.stronglink.esm27.entity;


import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_facilities_temp")
public class FacilitiesTemp extends BaseModel {
	
	/**
	 * 消防设施类型
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 6543713376086914788L;
	
	@TableField(value = "name_")
	private String name; 
	
	@TableField(value = "key_unit_id")
	private Long keyUnitId;
	
	@TableField(value = "type_")
	private String type;
	
	@TableField(value = "code_")
	private String code;
	
	@TableField(value = "lon_")
	private Double lon;
	
	@TableField(value = "lat_")
	private Double lat;
	
	@TableField(value = "baidu_lon")
	private Double baiduLon;
	
	@TableField(value = "baidu_lat")
	private Double baiduLat;	
	
	@TableField(value = "fire_radius")
	private Double fireRadius;
	
	@TableField(value = "flow_")
	private Double flow;

	@TableField(value = "reserves_")
	private Double reserves;
	@TableField(value = "pipe_network")
	private String pipeNetwork;
	@TableField(value = "check_time")
	private Date checkTime;
	@TableField(value = "check_desc")
	private String checkDesc; 
	@TableField(value="timestamp_")
	private Long timestamp;
	

	public Double getReserves() {
		return reserves;
	}

	public void setReserves(Double reserves) {
		this.reserves = reserves;
	}

	public String getPipeNetwork() {
		return pipeNetwork;
	}

	public void setPipeNetwork(String pipeNetwork) {
		this.pipeNetwork = pipeNetwork;
	}

	public Date getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(Date checkTime) {
		this.checkTime = checkTime;
	}

	public String getCheckDesc() {
		return checkDesc;
	}

	public void setCheckDesc(String checkDesc) {
		this.checkDesc = checkDesc;
	}

	public Long getKeyUnitId() {
		return keyUnitId;
	}

	public void setKeyUnitId(Long keyUnitId) {
		this.keyUnitId = keyUnitId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getFireRadius() {
		return fireRadius;
	}

	public void setFireRadius(Double fireRadius) {
		this.fireRadius = fireRadius;
	}

	public Double getBaiduLon() {
		return baiduLon;
	}

	public void setBaiduLon(Double baiduLon) {
		this.baiduLon = baiduLon;
	}

	public Double getBaiduLat() {
		return baiduLat;
	}

	public void setBaiduLat(Double baiduLat) {
		this.baiduLat = baiduLat;
	}

	public Double getFlow() {
		return flow;
	}

	public void setFlow(Double flow) {
		this.flow = flow;
	}
	

}
