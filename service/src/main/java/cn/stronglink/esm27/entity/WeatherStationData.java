package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_weather_station_data")
public class WeatherStationData extends BaseModel{

	/**
	 *
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "weather_station_id")
	private Long weatherStationId;//气象站id
	
	@TableField(value = "wind_speed")
	private String windSpeed;//风速
	
	@TableField(value = "wind_direction")
	private String windDirection;//风向
	
	@TableField(value = "pm_value")
	private String pmValue;//pm2.5
	
	@TableField(value = "temperature_")
	private String temperature;//温度
	
	@TableField(value = "humidity_")
	private String humidity;//湿度
	
	@TableField(value = "pressure_")
	private String pressure;//气压
	
	@TableField(value = "noise_")
	private String noise;//噪音
	
	@TableField(exist = false)
	private String windDirectionValue;//风向数值
	
	@TableField(exist = false)
	private String userId;
	
	@TableField(value = "room_id")
	private String roomId;
	
	public String getWindDirectionValue() {
		return windDirectionValue;
	}

	public void setWindDirectionValue(String windDirectionValue) {
		this.windDirectionValue = windDirectionValue;
	}

	public Long getWeatherStationId() {
		return weatherStationId;
	}

	public void setWeatherStationId(Long weatherStationId) {
		this.weatherStationId = weatherStationId;
	}

	public String getWindSpeed() {
		return windSpeed;
	}

	public void setWindSpeed(String windSpeed) {
		this.windSpeed = windSpeed;
	}

	public String getWindDirection() {
		return windDirection;
	}

	public void setWindDirection(String windDirection) {
		this.windDirection = windDirection;
	}

	public String getPmValue() {
		return pmValue;
	}

	public void setPmValue(String pmValue) {
		this.pmValue = pmValue;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getHumidity() {
		return humidity;
	}

	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}

	public String getPressure() {
		return pressure;
	}

	public void setPressure(String pressure) {
		this.pressure = pressure;
	}

	public String getNoise() {
		return noise;
	}

	public void setNoise(String noise) {
		this.noise = noise;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	
}
