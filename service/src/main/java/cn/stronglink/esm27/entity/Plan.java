package cn.stronglink.esm27.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.FieldStrategy;

import cn.stronglink.core.base.BaseModel;

@TableName("t_plans")
public class Plan extends BaseModel{
	
	/**
	 * 预案管理
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "name_")
	private String name;
	
	@TableField(value = "code_")
	private String code;
	
	@TableField(value = "dic_id")
	private Long dicId;
	
	@TableField(value = "level_")
	private Integer level;
	
	@TableField(value = "writer_")
	private String writer;
	
	@TableField(value = "writing_unit")
	private Long writingUnit;
	
	@TableField(value = "writing_time")
	private Date writingTime;
	
	@TableField(value = "status_")
	private Integer status;
	
	@TableField(value = "freq_")
	private Integer freq;
	
	@TableField(value = "path_")
	private String path;
	
	@TableField(value = "file_name")
	private String fileName;
	
	@TableField(value = "key_unit_id",strategy=FieldStrategy.IGNORED)
	private Long keyUnitId;
	
	@TableField(value = "type_")
	private String type;
	
	@TableField(value = "swf_path")
	private String swfPath;

	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getWritingTime() {
		return writingTime;
	}

	public void setWritingTime(Date writingTime) {
		this.writingTime = writingTime;
	}

	public String getSwfPath() {
		return swfPath;
	}

	public void setSwfPath(String swfPath) {
		this.swfPath = swfPath;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getKeyUnitId() {
		return keyUnitId;
	}

	public void setKeyUnitId(Long keyUnitId) {
		this.keyUnitId = keyUnitId;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getFreq() {
		return freq;
	}

	public void setFreq(Integer freq) {
		this.freq = freq;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Long getDicId() {
		return dicId;
	}

	public void setDicId(Long dicId) {
		this.dicId = dicId;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public Long getWritingUnit() {
		return writingUnit;
	}

	public void setWritingUnit(Long writingUnit) {
		this.writingUnit = writingUnit;
	}

}
