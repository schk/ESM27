package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_fire_limit")
public class FireLimit extends BaseModel {

	/**
	 * 灭火极限
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "material_type")
	private String materialType;
	
	@TableField(value = "pid_")
	private Long pid;
	
	@TableField(value = "is_combustion")
	private Integer isCombustion;
	
	@TableField(value = "fire_resistance_h")
	private Double fireResistanceH;
	
	@TableField(value = "material_name")
	private String materialName;
	
	@TableField(value = "remark_")
	private String remark;

	public String getMaterialType() {
		return materialType;
	}

	public void setMaterialType(String materialType) {
		this.materialType = materialType;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public Integer getIsCombustion() {
		return isCombustion;
	}

	public void setIsCombustion(Integer isCombustion) {
		this.isCombustion = isCombustion;
	}

	public Double getFireResistanceH() {
		return fireResistanceH;
	}

	public void setFireResistanceH(Double fireResistanceH) {
		this.fireResistanceH = fireResistanceH;
	}

	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	
	
}
