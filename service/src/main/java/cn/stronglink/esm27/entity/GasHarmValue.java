package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_gas_harm_value")
public class GasHarmValue  extends BaseModel{

	/**
	 *
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	@TableField(value = "chemical_")
	private String chemical;
	
	@TableField(value = "gas_name")
	private String gasName;
	
	@TableField(value = "hight_value")
	private Double hightValue;
	
	@TableField(value = "middle_value")
	private Double middleValue;
	
	@TableField(value = "low_value")
	private Double lowValue;
	
	@TableField(value = "unit_")
	private String unit;
	
	@TableField(value = "quality_")
	private Integer quality;

	
	public Integer getQuality() {
		return quality;
	}

	public void setQuality(Integer quality) {
		this.quality = quality;
	}

	public String getChemical() {
		return chemical;
	}

	public void setChemical(String chemical) {
		this.chemical = chemical;
	}

	public String getGasName() {
		return gasName;
	}

	public void setGasName(String gasName) {
		this.gasName = gasName;
	}

	public Double getHightValue() {
		return hightValue;
	}

	public void setHightValue(Double hightValue) {
		this.hightValue = hightValue;
	}

	public Double getMiddleValue() {
		return middleValue;
	}

	public void setMiddleValue(Double middleValue) {
		this.middleValue = middleValue;
	}

	public Double getLowValue() {
		return lowValue;
	}

	public void setLowValue(Double lowValue) {
		this.lowValue = lowValue;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	
}
