package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_expr")
public class Expr extends BaseModel{

	/**
	 * 公式表
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 290182506052411624L;
	
	@TableField(value="name_")
	private String name;
	
	@TableField(value="expr_")
	private String expr;
	
	@TableField(value="unit_")
	private String unit;
	
	@TableField(value="describtion_")
	private String describtion;
	
	@TableField(value="dressing_method")
	private Integer dressingMethod;
	
	@TableField(value="scale_")
	private Integer scale;
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExpr() {
		return expr;
	}

	public void setExpr(String expr) {
		this.expr = expr;
	}

	public String getDescribtion() {
		return describtion;
	}

	public void setDescribtion(String describtion) {
		this.describtion = describtion;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Integer getDressingMethod() {
		return dressingMethod;
	}

	public void setDressingMethod(Integer dressingMethod) {
		this.dressingMethod = dressingMethod;
	}

	public Integer getScale() {
		return scale;
	}

	public void setScale(Integer scale) {
		this.scale = scale;
	}
	
}
