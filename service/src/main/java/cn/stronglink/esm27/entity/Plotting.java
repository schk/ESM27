package cn.stronglink.esm27.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.stronglink.core.base.BaseModel;

@TableName("t_plotting")
public class Plotting extends BaseModel{

	/**
	 * 标绘管理表
	 */
	@TableField(exist=false)
	private static final long serialVersionUID = -6662991661172366861L;

	@TableField(value="name_")
	private String name;
	
	@TableField(value="dic_id")
	private Long dicId;
	
	@TableField(value="path_")
	private String path;
	
	@TableField(value="type_")
	private Integer type;
	
	@TableField(value="jh_path")
	private String jhPath;
	
	@TableField(value="dt_path")
	private String dtPath;
	
	@TableField(value="is_display")
	private int isDisplay;
	
	@TableField(value="remark_")
	private String remark;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getDicId() {
		return dicId;
	}

	public void setDicId(Long dicId) {
		this.dicId = dicId;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getJhPath() {
		return jhPath;
	}

	public void setJhPath(String jhPath) {
		this.jhPath = jhPath;
	}

	public String getDtPath() {
		return dtPath;
	}

	public void setDtPath(String dtPath) {
		this.dtPath = dtPath;
	}

	public int getIsDisplay() {
		return isDisplay;
	}

	public void setIsDisplay(int isDisplay) {
		this.isDisplay = isDisplay;
	}


	
}
