package cn.stronglink.esm27.shiro;

import java.util.concurrent.atomic.AtomicInteger;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.crypto.hash.Md5Hash;


public class RetryLimitHashedCredentialsMatcher extends HashedCredentialsMatcher {
	
	private Cache<String, AtomicInteger> passwordRetryCache;
	 
	public RetryLimitHashedCredentialsMatcher(CacheManager cacheManager) {
		this.setHashAlgorithmName(Md5Hash.ALGORITHM_NAME);
		this.setStoredCredentialsHexEncoded(true);
		this.setHashIterations(2);
		passwordRetryCache = cacheManager.getCache("passwordRetryCache");
	}
	
	 @Override
	    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) throws ExcessiveAttemptsException {
		 	String username = (String) token.getPrincipal();
			// retry count + 1
		 	//System.out.println("进入验证");
			AtomicInteger retryCount = passwordRetryCache.get(username);

	        if(retryCount==null){
	        	retryCount=new AtomicInteger(0);
	        	passwordRetryCache.put(username, retryCount);
	        }
	        if(retryCount.incrementAndGet()>5){
	        	//System.out.println("您的账号已被锁定，请10分钟之后在重试");
	        	throw new LockedAccountException("您的账号密码错误次数过多已被锁定，请30分钟之后在重试");//BusinessException("您的账号已被锁定，请10分钟之后在重试");
	        }
	        boolean matches = super.doCredentialsMatch(token, info);
	        if(matches){
	        	passwordRetryCache.remove(username);
	        }
	        return matches;
	    }

}
