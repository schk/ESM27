package cn.stronglink.esm27.shiro;

import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

import cn.stronglink.esm27.entity.User;

public class PasswordSaltUtil {

	// 随机数生成器
	private static RandomNumberGenerator randomNumberGenerator = new SecureRandomNumberGenerator();

	// 指定散列算法为md5
	private static String algorithmName = "MD5";
	// 散列迭代次数
	private final static int hashIterations = 2;

	/**
     * 生成随机盐�?�对密码进行加密
     * @param userLogin  登录识别串（用户名）
     * @return
     */
    public static User encrypt(User user) {
        user.setSalt(randomNumberGenerator.nextBytes().
        toHex());
        String newPassword = 
        new SimpleHash(algorithmName,user.getPassword(),
            ByteSource.Util.bytes(user.getSalt()),hashIterations).toHex();
        user.setPassword(newPassword);
        return user;
    }
}
