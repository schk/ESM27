package cn.stronglink.esm27.shiro;

import java.util.Set;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import cn.stronglink.esm27.entity.User;
import cn.stronglink.esm27.module.system.permission.service.PermissionService;
import cn.stronglink.esm27.module.system.user.service.UserService;

public class CustomRealm extends AuthorizingRealm {
	
	@Autowired
	private PermissionService permissionService;
	@Autowired
	private UserService userService;
	public CustomRealm() {
		
	}

	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

		if (principals == null) {
			throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
		}
		User user = (User) getAvailablePrincipal(principals);
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		info.setRoles(user.getRoleset());
		info.setStringPermissions(user.getPermset());
		return info;
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		UsernamePasswordToken upToken = (UsernamePasswordToken) token;
		String username = upToken.getUsername();
        User user = userService.findUserByName(username);
        if(user != null) {
        	if(user.getStatus() == 2 ) {
            	throw new LockedAccountException();
            }
            
            if(user.getStatus() == 3 ) {
            	throw new DisabledAccountException();
            }
            //查询用户的角色和权限存到SimpleAuthenticationInfo中，这样在其它地方
            //SecurityUtils.getSubject().getPrincipal()就能拿出用户的所有信息，包括角色和权限
            Set<String> perms = permissionService.getPermsByUserId(user.getId());
            user.getPermset().addAll(perms);
            SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(user, user.getPassword(),ByteSource.Util.bytes(user.getSalt()),getName());
            return info;
        }else {
        	return null;
        }
        

	}

}
