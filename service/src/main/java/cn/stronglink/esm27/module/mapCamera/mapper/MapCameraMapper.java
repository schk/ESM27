package cn.stronglink.esm27.module.mapCamera.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.MapCamera;

public interface MapCameraMapper extends BaseMapper<MapCamera> {

	List<String> selectList(String roomId);

	
}
