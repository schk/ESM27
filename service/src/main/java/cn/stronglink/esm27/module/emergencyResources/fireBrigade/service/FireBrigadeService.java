package cn.stronglink.esm27.module.emergencyResources.fireBrigade.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.FireBrigade;
import cn.stronglink.esm27.module.emergencyResources.fireBrigade.mapper.FireBrigadeMapper;
import cn.stronglink.esm27.module.emergencyResources.fireBrigade.vo.FireBrigadeTreeNodeVo;
import cn.stronglink.esm27.module.fireEngine.mapper.FireEngineMapper;
import cn.stronglink.esm27.module.fireEngine.vo.FireEngineVo;
import cn.stronglink.esm27.module.plan.mapper.PlanMapper;
import cn.stronglink.esm27.module.plan.vo.PlanVo;
import cn.stronglink.esm27.module.system.user.mapper.UserMapper;


@Service
@Transactional(rollbackFor=Exception.class)
public class FireBrigadeService {
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private FireBrigadeMapper fireBrigadeMapper;
	@Autowired
	private FireEngineMapper fireEngineMapper;
	@Autowired
	private PlanMapper planMapper;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<FireBrigade> qryListByParams(Page<FireBrigade> page, Map<String, Object> params) {
		page.setRecords(fireBrigadeMapper.qryListByParams(page,params));	
		return page;
	}
	
	/**
	 * 查询消防队下的车辆、预案、灭火剂种类
	 * @param page
	 * @param params
	 * @return
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<FireBrigade> qryListByParamsForWeb(Page<FireBrigade> page, Map<String, Object> params) {
		List<FireBrigade> list =fireBrigadeMapper.qryListByParamsForWeb(page,params);
		if(list.size()>0){
			for(FireBrigade fireBrigade:list){
				FireEngineVo carCount = fireEngineMapper.qryCarCountByBrigade(fireBrigade.getId());
				fireBrigade.setCarCount(carCount);
				List<PlanVo> planList =planMapper.qryPlansByBrigade(fireBrigade.getId());
				fireBrigade.setPlanList(planList);
				Map<String,Object> p = new HashMap<String,Object>();
				p.put("fireBrigadeId", fireBrigade.getId());
				List<FireEngineVo> mieH =fireEngineMapper.qryMieHuoJ(p);
				fireBrigade.setMiehuojList(mieH);
			}
		}
		page.setRecords(list);	
		return page;
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public FireBrigade qryById(Long id) {
		FireBrigade selectById = fireBrigadeMapper.selectById(id);
		if(selectById.getLon()!=null&&selectById.getLat()!=null) {
			selectById.setLonLat(selectById.getLon()+","+selectById.getLat());
		}
		return selectById;	
	}

	public void insert(FireBrigade entity) {
		if(!"".equals(entity.getLonLat())) {
			String [] lonLat=entity.getLonLat().split(",");
			entity.setLon(Double.valueOf(lonLat[0]));
			entity.setLat(Double.valueOf(lonLat[1]));
		}
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		fireBrigadeMapper.insert(entity);
		
	}

	public void update(FireBrigade entity) {
		if(!"".equals(entity.getLonLat())) {
			String [] lonLat=entity.getLonLat().split(",");
			entity.setLon(Double.valueOf(lonLat[0]));
			entity.setLat(Double.valueOf(lonLat[1]));
		}
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if(fireBrigadeMapper.updateById(entity)==0){
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
	}

	public void remove(Long id) {
		fireBrigadeMapper.deleteById(id);
		// 删除应急预案的关联关系
		planMapper.updateKeyUnitById(id);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<FireBrigade> qryList() {
		return fireBrigadeMapper.qryList();
		
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public FireBrigade getInfoDetail(Long id) {
		FireBrigade selectById = fireBrigadeMapper.getInfoDetail(id);
		return selectById;
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<FireBrigadeTreeNodeVo> qryFireBrigadeTree(HttpServletRequest request) {		
		if(request.getSession().getAttribute("admin")!=null){
			boolean isAdmin = (boolean) request.getSession().getAttribute("admin");
			if(!isAdmin) {
				Long brigadeId =userMapper.selectUserBrigade(WebUtil.getCurrentUser());
				List<FireBrigadeTreeNodeVo> rootDept = fireBrigadeMapper.qryTopById(brigadeId);	
				List<FireBrigadeTreeNodeVo> departmentList = fireBrigadeMapper.qryAllFireBrigade();
				if(rootDept!=null&&rootDept.size()>0){
					for(FireBrigadeTreeNodeVo vo:rootDept){
						recursion(departmentList, vo);  
					}
				}
		        return rootDept;  
			}
		}
		List<FireBrigadeTreeNodeVo> rootDept = fireBrigadeMapper.qryTop();	
		List<FireBrigadeTreeNodeVo> departmentList = fireBrigadeMapper.qryAllFireBrigade();
		if(rootDept!=null&&rootDept.size()>0){
			for(FireBrigadeTreeNodeVo vo:rootDept){
				recursion(departmentList, vo);  
			}
		}
        return rootDept;  
		
	}

	private void recursion(List<FireBrigadeTreeNodeVo> list, FireBrigadeTreeNodeVo node) {
		List<FireBrigadeTreeNodeVo> childList = getChildList(list, node);// 得到子节点列表
		if (!CollectionUtils.isEmpty(childList)) {
			node.setChildren(childList);
			Iterator<FireBrigadeTreeNodeVo> it = childList.iterator();
			while (it.hasNext()) {
				FireBrigadeTreeNodeVo n = (FireBrigadeTreeNodeVo) it.next();
				recursion(list, n);
			}
		} else {
			node.setChildren(null);
		}
	}
	
	private List<FireBrigadeTreeNodeVo> getChildList(List<FireBrigadeTreeNodeVo> list, FireBrigadeTreeNodeVo node) {  
        List<FireBrigadeTreeNodeVo> nodeList = new ArrayList<FireBrigadeTreeNodeVo>();  
        Iterator<FireBrigadeTreeNodeVo> it = list.iterator();  
        while (it.hasNext()) {  
        	FireBrigadeTreeNodeVo n = (FireBrigadeTreeNodeVo) it.next();
            if (n.getPid().equals(node.getId().toString()) ) {  
                nodeList.add(n);  
            }  
        }  
        return nodeList;  
    }
	public Integer getCountByUsername(FireBrigade entity) {
		return fireBrigadeMapper.getCountByUsername(entity);
	}

	public Long findBrigadeIdByName(String brigadeName){
		return fireBrigadeMapper.findBrigadeIdByName(brigadeName);
	}
	
	public void batchInsertObj(List<FireBrigade> list) {
		fireBrigadeMapper.batchInsertObj(list);
	}
	
}
