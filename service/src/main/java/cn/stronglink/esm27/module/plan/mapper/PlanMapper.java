package cn.stronglink.esm27.module.plan.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.esm27.entity.Plan;
import cn.stronglink.esm27.entity.PlanTemp;
import cn.stronglink.esm27.entity.PlansCatalog;
import cn.stronglink.esm27.module.plan.vo.PlanVo;


public interface PlanMapper extends BaseMapper<Plan>{

	List<PlanVo> qryListByParams(Pagination page, @Param("params")  Map<String, Object> params);

	List<PlanVo> qryListByKeyParts(Page<PlanVo> page, Map<String, Object> params);

	PlanVo qryById(@Param("planId")Long planId);

	List<PlansCatalog> getPlansCatalogs(@Param("planId")Long planId);

	void createPlan(PlanVo entity);

	void createPlanCatalogs(PlanVo entity);

	void deletePlanCatalogs(@Param("planId")Long planId);

	void updateFreq(Long id);

	int batchInsertEntity(@Param("interimList") List<Plan> interimList);

	int batchInsertTemp(@Param("interimList") List<PlanTemp> interimList);

	List<PlanTemp> qryExcelTemp(Long timestamp);

	void delExcelTemp(Long timestamp);

	List<PlanVo> qryPlansByBrigade(Long fireBrigadeId);

	List<PlanVo> qryPlansByUnit(Long keyUnitId);

	PlanVo qryWithKeyUnit(Long planId);

	PlanVo qryWithFireBrigade(Long planId);

	PlanVo qryWithKeyParts(Long planId);

	List<PlanVo> qryPlanByKeyParts(Long keyPartsId);

	PlanVo qryPlanById(Long id);

	int updateKeyUnitById(Long keyUnitId);

	void updateKeyUnit(@Param("planId")Long planId,@Param("keyUnitId") Long keyUnitId, @Param("type")String type);

	void delKeyUnitByPlanId(Long id);

	List<PlanVo> qryNotAddedPlanList(Pagination page, @Param("params")Map<String, Object> params);

}
