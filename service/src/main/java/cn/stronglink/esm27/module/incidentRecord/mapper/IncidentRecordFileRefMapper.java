package cn.stronglink.esm27.module.incidentRecord.mapper;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.stronglink.core.util.AntdFile;
import cn.stronglink.esm27.entity.IncidentRecordFileRef;


public interface IncidentRecordFileRefMapper extends BaseMapper<IncidentRecordFileRef>{

	void deleteFileByRecordId(Long incidentRecordId);

	void inserFiles(Map<String, Object> map);

	List<AntdFile> selectFileById(Long recordId);

	
}
