package cn.stronglink.esm27.module.extinguisher.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Dictionary;
import cn.stronglink.esm27.entity.Extinguisher;
import cn.stronglink.esm27.entity.ExtinguisherTemp;
import cn.stronglink.esm27.module.dictionary.mapper.DictionaryMapper;
import cn.stronglink.esm27.module.emergencyResources.fireBrigade.mapper.FireBrigadeMapper;
import cn.stronglink.esm27.module.emergencyResources.fireBrigade.vo.FireBrigadeTreeNodeVo;
import cn.stronglink.esm27.module.extinguisher.mapper.ExtinguisherMapper;
import cn.stronglink.esm27.module.extinguisher.vo.ExtinguisherClo;
import cn.stronglink.esm27.module.extinguisher.vo.ExtinguisherVo;
import cn.stronglink.esm27.module.extinguisherUseRecord.param.SearchParam;
import cn.stronglink.esm27.module.extinguisherUseRecord.vo.FireBrigadeVo;

@Service
@Transactional(rollbackFor=Exception.class)
public class ExtinguisherService {
	
	@Autowired
	private ExtinguisherMapper extinguisherMapper;
	@Autowired
	private FireBrigadeMapper fireBrigadeMapper;
	@Autowired
	private DictionaryMapper dictionaryMapper;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<ExtinguisherVo> qryListByParams(Page<ExtinguisherVo> page, Map<String, Object> params) {
		page.setRecords(extinguisherMapper.qryListByParams(page,params));	
		return page;
	}
	
	public List<FireBrigadeTreeNodeVo> recrusiveChildren(FireBrigadeTreeNodeVo p, List<FireBrigadeTreeNodeVo> rtnLst) {
		List<FireBrigadeTreeNodeVo> lstChildren = null;
		lstChildren = p.getChildren();

		if (lstChildren == null || lstChildren.size() < 1) {
			return lstChildren;
		}
		rtnLst.addAll(lstChildren);

		for (FireBrigadeTreeNodeVo obj : lstChildren) {
			List<FireBrigadeTreeNodeVo> lstTmp = recrusiveChildren(obj, rtnLst);

			if (lstTmp == null || lstTmp.size() < 1) {
				continue;
			}
			rtnLst.addAll(lstTmp);
		}

		return lstChildren;
	}
	
	
	/*
	 * 统计灭火剂总量
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Map<String,Object> qrySumByKeyId(SearchParam params) {
		List<ExtinguisherClo> columns=new ArrayList<ExtinguisherClo>();
		int width=0;
		//序号
		ExtinguisherClo xhC=new ExtinguisherClo();
		xhC.setTitle("序号");
		xhC.setDataIndex("xh");
		xhC.setKey("xh");
		xhC.setKey("xh");
		xhC.setFixed("left");
		xhC.setWidth("100px");
		width+=100;
		columns.add(xhC);
		ExtinguisherClo types=new ExtinguisherClo();
		types.setTitle("消防队");
		types.setDataIndex("teamName");
		types.setKey("teamName");
		types.setFixed("left");
		types.setWidth("150px");
		width+=150;
		columns.add(types);
		
		List<Long> brigadeIds=new ArrayList<Long>();
		if(params.getFireBrigadeIds()!=null&&params.getFireBrigadeIds().size()>0){
			brigadeIds.addAll(params.getFireBrigadeIds());
			brigadeIds=getParentId(brigadeIds,brigadeIds);
		}
		
		Map<String,Object> conditionMap=new HashMap<String,Object>();
		conditionMap.put("pid", -1L);
		conditionMap.put("fileBrigadeIds", brigadeIds);
		
		List<FireBrigadeVo> fireBrigadeVoList=fireBrigadeMapper.getAllFileBrigade(conditionMap);
//		if(fireBrigadeVoList!=null&&fireBrigadeVoList.size()>0){
//			for(FireBrigadeVo vo:fireBrigadeVoList){
//				getChildren(vo,brigadeIds);
//			}
//		}
		
//		if(fireBrigadeVoList!=null&&fireBrigadeVoList.size()>0){
//			for(FireBrigadeVo vo:fireBrigadeVoList){
//				ExtinguisherClo column=new ExtinguisherClo();
//				column.setTitle(vo.getName());
//				if(vo.getChildren()!=null&&vo.getChildren().size()>0){
//					width=getFileColumns(vo.getChildren(),column,width);
//				}else{
//					column.setDataIndex(vo.getId()+"");
//					column.setKey(vo.getId()+"");
//					column.setWidth(150);
//					width+=150;
//				}
//				columns.add(column);
//			}
//		}		
		
		
		
		List<Extinguisher> recordCountList=extinguisherMapper.getExtinguisherStock(params);
		//获取灭火剂类型
		Map<String,Object> conditionMap1=new HashMap<String,Object>();
		conditionMap1.put("type", 5);
		conditionMap1.put("dictionaryIds", params.getDictionaryIds());
		List<Dictionary> dictionaryList=dictionaryMapper.qryDictionary(conditionMap1);
		for(Dictionary dic:dictionaryList){
			ExtinguisherClo dics=new ExtinguisherClo();
			dics.setTitle(dic.getName());
			dics.setDataIndex(dic.getId_());
			dics.setKey(dic.getId_());
			dics.setWidth("75px");
			width+=75;
			columns.add(dics);
		}
		
		ExtinguisherClo totle=new ExtinguisherClo();
		totle.setTitle("合计");
		totle.setDataIndex("countNums");
		totle.setKey("countNums");
		totle.setWidth("75px");
		width+=75;
		columns.add(totle);
		
		//柱状图数据
		List<Map<String,Object>> charDataList=new ArrayList<Map<String,Object>>();
		//获取所有不存在子消防队的消防队数据
		//List<FireBrigade> fireBrigades=extinguisherUseRecordMapper.getFireBrigade(params);
		BigDecimal totalUsedAmount=BigDecimal.ZERO;
		List<Map<String,Object>> resultDataList=new ArrayList<Map<String,Object>>();
		if(fireBrigadeVoList!=null&&fireBrigadeVoList.size()>0){
			int flag=1;
			for(FireBrigadeVo fileBrigade:fireBrigadeVoList){
				Map<String,Object> map=new HashMap<String,Object>();
				BigDecimal usedAmount=BigDecimal.ZERO;
				map.put("id", fileBrigade.getId());
				map.put("xh", flag);
				map.put("teamName", fileBrigade.getName());
				for(Dictionary dictionary:dictionaryList){
					Map<String,Object> charData=new HashMap<String,Object>();
					charData.put("label", fileBrigade.getName());
					charData.put("name", dictionary.getName());
					BigDecimal amount=BigDecimal.ZERO;
					for(Extinguisher recordCount:recordCountList){
						if(recordCount.getTypeId().equals(dictionary.getId())
								&&fileBrigade.getId().equals(recordCount.getFireBrigadeId())){
							map.put(dictionary.getId_(), recordCount.getQuantity());
							BigDecimal b1 = new BigDecimal(recordCount.getQuantity());
							amount=b1;
							usedAmount=usedAmount.add(b1);
						}
					}
					charData.put("value", amount);
					charDataList.add(charData);
				}
				totalUsedAmount=totalUsedAmount.add(usedAmount);
				map.put("countNums", usedAmount);
				resultDataList.add(map);
				flag++;
			}
			
			/*for(Dictionary dictionary:dictionaryList){
				Map<String,Object> map=new HashMap<String,Object>();
				BigDecimal usedAmount=BigDecimal.ZERO;
				map.put("id", dictionary.getId());
				map.put("xh", flag);
				map.put("foamTypeName", dictionary.getName());
				if(fireBrigades!=null&&fireBrigades.size()>0){
					for(FireBrigade fileBrigade:fireBrigades){
						for(Extinguisher recordCount:recordCountList){
							if(recordCount.getTypeId().equals(dictionary.getId())
									&&fileBrigade.getId().equals(recordCount.getFireBrigadeId())){
								map.put(recordCount.getFireBrigadeId()+"", recordCount.getQuantity());
								BigDecimal b1 = new BigDecimal(recordCount.getQuantity());
								usedAmount=usedAmount.add(b1);
							}
						}
					}
				}
				map.put("countNums", usedAmount);
				resultDataList.add(map);
				flag++;
			}*/
		}
		
		Map<String,Object> map =new HashMap<String,Object>();
		map.put("columns", columns);
		map.put("dataList", resultDataList);
		map.put("charDataList", charDataList);
		
		map.put("width", width);
		//获取库存、随车、使用记录数据
		//使用记录
		BigDecimal quantity=extinguisherMapper.getExtinguisherUseRecord(params);
		map.put("quantity",quantity);
		//随车
		BigDecimal carQuantity=extinguisherMapper.getFireEngine(params);
		map.put("carQuantity",carQuantity);
		//库存
		map.put("totalUsedAmount",totalUsedAmount);
		return map;
	}
	
	public int getFileColumns(List<FireBrigadeVo> fireBrigadeVoList,
			ExtinguisherClo column,int width){
		List<ExtinguisherClo> fileColumns=new ArrayList<ExtinguisherClo>();
		if(fireBrigadeVoList!=null&&fireBrigadeVoList.size()>0){
			for(FireBrigadeVo vo:fireBrigadeVoList){
				ExtinguisherClo chdColumn=new ExtinguisherClo();
				chdColumn.setTitle(vo.getName());
				if(vo.getChildren()!=null&&vo.getChildren().size()>0){
					width=getFileColumns(vo.getChildren(),column,width);
				}else{
					chdColumn.setDataIndex(vo.getId()+"");
					chdColumn.setKey(vo.getId()+"");
					chdColumn.setWidth("100");
					width+=100;
				}
				fileColumns.add(chdColumn);
			}
			column.setChildren(fileColumns);
		}
		return width;
	}
	
	public List<Long> getParentId(List<Long> brigadeIds,List<Long> childrenIds){
		Map<String,Object> conditionMap=new HashMap<String,Object>();
		conditionMap.put("fileBrigadeIds", childrenIds);
		List<Long> parentIds=fireBrigadeMapper.getParentIds(conditionMap);
		if(parentIds!=null&&parentIds.size()>0){
			brigadeIds.addAll(parentIds);
			List<Long> childrens=new ArrayList<Long>();
			for(Long pid:parentIds){
				if(!pid.equals(-1L)){
					childrens.add(pid);
				}
			}
			if(childrens!=null&&childrens.size()>0){
				return getParentId(brigadeIds,childrens);
			}
		}
		return brigadeIds;
	}
	
	public void getChildren(FireBrigadeVo vo,List<Long> fireBrigadeIds){
		Map<String,Object> conditionMap=new HashMap<String,Object>();
		conditionMap.put("pid", vo.getId());
		conditionMap.put("fileBrigadeIds", fireBrigadeIds);
		List<FireBrigadeVo> fireBrigadeVoList=fireBrigadeMapper.getFileBrigade(conditionMap);
		if(fireBrigadeVoList!=null&&fireBrigadeVoList.size()>0){
			for(FireBrigadeVo child:fireBrigadeVoList){
				getChildren(child,fireBrigadeIds);
			}
			vo.setChildren(fireBrigadeVoList);
		}
	}
	/*
	 * 根据id查询信息
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public ExtinguisherVo selectById(Long id){
		return extinguisherMapper.qryById(id);
	}
	
	/*
	 * 根据id删除信息
	 */
	public void  remove(Long id){
		extinguisherMapper.deleteById(id);
	}
	/*
	 * 添加信息
	 */
	public void insert(Extinguisher entity) {		
		if(entity.getFireBrigadeId()!=null) {
			Extinguisher obj =extinguisherMapper.getQuantity(entity.getFireBrigadeId(),entity.getTypeId());
			if(obj!=null) {
				 BigDecimal bd1 = new BigDecimal(Double.toString(obj.getQuantity())); 
			     BigDecimal bd2 = new BigDecimal(Double.toString(entity.getQuantity())); 
			     entity.setQuantity(bd1.add(bd2).doubleValue());
			     entity.setUpdateTime(new Date());
			     entity.setId(obj.getId());
			     entity.setUpdateBy(WebUtil.getCurrentUser());
			     if(extinguisherMapper.updateById(entity)==0){
					throw new BusinessException("更新失败，记录不存在或记录已被更改!");
				 }
			}else {
				entity.setId(IdWorker.getId());		
				entity.setCreateBy(WebUtil.getCurrentUser());
				entity.setCreateTime(new Date());
				entity.setUpdateTime(entity.getCreateTime());
				extinguisherMapper.insert(entity);
			}
		}
		
	}
	//根据id修改信息
	public void update(Extinguisher entity) {
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if(extinguisherMapper.updateById(entity)==0){
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<ExtinguisherVo> qryListByFireBrigade(Map<String, Object> params) {
		return extinguisherMapper.qryListByFireBrigade(params);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Extinguisher getQuantity(Map<String, Object> params) {
		return extinguisherMapper.getQuantity(Long.parseLong(params.get("fireBrigadeId").toString()),Long.parseLong(params.get("foamTypeId").toString()));
	}

	public BigDecimal qryExtinguishere(Map<String, Object> params) {
		
		return extinguisherMapper.qryExtinguishere(params);
	}

	public void saveExcelData(List<Extinguisher> entityList) {
		if (entityList != null && entityList.size() > 0) {
//			List<Extinguisher> interimList = new ArrayList<Extinguisher>();
//			int j = (int) Math.ceil(entityList.size() / (double) 50);
//			int num = 0;
//			for (int i = 1; i <= j; i++) {
//				if (i * 50 >= entityList.size()) {
//					for (int k = (i - 1) * 50; k < entityList.size(); k++) {
//						interimList.add(entityList.get(k));
//					}
//					num = num + this.batchInsert(interimList);
//				} else {
//					interimList = entityList.subList((i - 1) * 50, i * 50);
//					num = num + this.batchInsert(interimList);
//				}
//			}
//			if (num != entityList.size()) {
//				throw new BusinessException("导入数据异常!");
//			}
			
//			
			List<Extinguisher> interimList = new ArrayList<Extinguisher>();
			int num = 0;
			for(int i=0;i<entityList.size();i++){
				interimList.add(entityList.get(i));
				if(i!=0&&i%50==0){
					num = num + this.batchInsert(interimList);
					interimList.clear();
				}
			}
			if(interimList.size()>0){
				num = num + this.batchInsert(interimList);
				interimList.clear();
			}
			if (num != entityList.size()) {
				throw new BusinessException("导入数据异常!");
			}
			
			
		}
	}

	private int batchInsert(List<Extinguisher> interimList) {
		return extinguisherMapper.batchInsert(interimList);
	}

	public void saveExcelTempData(List<ExtinguisherTemp> tempList) {
		if (tempList != null && tempList.size() > 0) {
			List<ExtinguisherTemp> interimList = new ArrayList<ExtinguisherTemp>();
			int num = 0;
			for(int i=0;i<tempList.size();i++){
				interimList.add(tempList.get(i));
				if(i!=0&&i%50==0){
					num = num + this.batchInsertTemp(interimList);
					interimList.clear();
				}
			}
			if(interimList.size()>0){
				num = num + this.batchInsertTemp(interimList);
				interimList.clear();
			}
			if (num != tempList.size()) {
				throw new BusinessException("导入数据异常!");
			}
		}
	}

	private int batchInsertTemp(List<ExtinguisherTemp> interimList) {
		return extinguisherMapper.batchInsertTemp(interimList);
	}
	
	public List<ExtinguisherTemp> qryExcelTemp(Long timestamp) {
		return extinguisherMapper.qryExcelTemp(timestamp);
	}
	
	public void delExcelTemp(Long timestamp) {
		extinguisherMapper.delExcelTemp(timestamp);
	}

	public List<ExtinguisherVo> qryListAll() {
		return extinguisherMapper.qryListAll();
	}

}
