package cn.stronglink.esm27.module.emergencyResources.emergencyMaterialTemp.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.EmergencyMaterialTemp;
import cn.stronglink.esm27.module.emergencyResources.emergencyMaterialTemp.service.EmergencyMaterialTempService;
import cn.stronglink.esm27.module.emergencyResources.emergencyMaterialTemp.vo.EmergencyMaterialTempVo;

@Controller
@RequestMapping("emergencyMaterialTemp")
public class EmergencyMaterialTempController extends AbstractController{
	
	@Autowired
	private EmergencyMaterialTempService emergencyMaterialTempService;

}
