package cn.stronglink.esm27.module.extinguisherUseRecord.vo;

import java.math.BigDecimal;

public class ExtinguisherUseRecordCount {

	private BigDecimal	usedAmount;
	private Long		foamTypeId;
	private Long		fireBrigadeId;
	private String		name;
	public BigDecimal getUsedAmount() {
		return usedAmount;
	}
	public void setUsedAmount(BigDecimal usedAmount) {
		this.usedAmount = usedAmount;
	}
	public Long getFoamTypeId() {
		return foamTypeId;
	}
	public void setFoamTypeId(Long foamTypeId) {
		this.foamTypeId = foamTypeId;
	}
	public Long getFireBrigadeId() {
		return fireBrigadeId;
	}
	public void setFireBrigadeId(Long fireBrigadeId) {
		this.fireBrigadeId = fireBrigadeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
