package cn.stronglink.esm27.module.fireWaterSource.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Extinguisher;
import cn.stronglink.esm27.entity.FireWaterSource;
import cn.stronglink.esm27.entity.FireWaterSourceTemp;
import cn.stronglink.esm27.module.fireWaterSource.mapper.FireWaterSourceMapper;
import cn.stronglink.esm27.module.fireWaterSource.vo.FireWaterSourceVo;

@Service
@Transactional(rollbackFor=Exception.class)
public class FireWaterSourceService {
	
	@Autowired
	private FireWaterSourceMapper fireWaterSourceMapper;
	/**
	 * 查询消防水源分页
	 * @param page
	 * @param params
	 * @return
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<FireWaterSourceVo> qryListByParams(Page<FireWaterSourceVo> page, Map<String, Object> params) {
		page.setRecords(fireWaterSourceMapper.qryListByParams(page,params));	
		return page;
	}
	
	/**
	 * 查询消防水源不带分页
	 * @return
	 */
	public List<FireWaterSourceVo> qryList() {
		return fireWaterSourceMapper.qryList();
	}
	
	/**
	 * 查询消防水源对象
	 * @param id
	 * @return
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public FireWaterSourceVo selectById(Long id){
		FireWaterSourceVo selectById = fireWaterSourceMapper.qryById(id);
		if(selectById.getLon()!=null&&selectById.getLat()!=null) {
			selectById.setLonLat(selectById.getLon()+","+selectById.getLat());
		}
		return selectById;
	}
	
	/**
	 * 根据id删除信息
	 * @param id
	 */
	public void  remove(Long id){
		fireWaterSourceMapper.deleteById(id);
	}
	
	/**
	 * 添加信息
	 * @param entity
	 */
	public void insert(FireWaterSource entity) {
		if(!"".equals(entity.getLonLat())) {
			String [] lonLat=entity.getLonLat().split(",");
			Double lonBd = new Double(lonLat[0]);
			Double latBd = new Double(lonLat[1]);
			entity.setLon(lonBd);
			entity.setLat(latBd);
		}
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		fireWaterSourceMapper.insert(entity);
	}
	//根据id修改信息
	public void update(FireWaterSource entity) {
		entity.setUpdateBy(WebUtil.getCurrentUser());
		if(!"".equals(entity.getLonLat())) {
			String [] lonLat=entity.getLonLat().split(",");
			Double lonBd = new Double(lonLat[0]);
			Double latBd = new Double(lonLat[1]);
			entity.setLon(lonBd);
			entity.setLat(latBd);
		}
		entity.setUpdateTime(new Date());
		entity.setUpdateBy(WebUtil.getCurrentUser());
		if(fireWaterSourceMapper.updateById(entity)==0){
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
	}
	
	public Integer findCountByName(String name) {
		return fireWaterSourceMapper.findCountByName(name);
	}
	
	public Integer findCountByNameAndId(String name, Long id) {
		return fireWaterSourceMapper.findCountByNameAndId(name,id);
	}
	
	public void saveExcelData(List<FireWaterSource> entityList) {
		if (entityList != null && entityList.size() > 0) {
			List<FireWaterSource> interimList = new ArrayList<FireWaterSource>();
			int num = 0;
			for(int i=0;i<entityList.size();i++){
				interimList.add(entityList.get(i));
				if(i!=0&&i%50==0){
					num = num + this.batchInsert(interimList);
					interimList.clear();
				}
			}
			if(interimList.size()>0){
				num = num + this.batchInsert(interimList);
				interimList.clear();
			}
			if (num != entityList.size()) {
				throw new BusinessException("导入数据异常!");
			}
		}
	}
	
	private int batchInsert(List<FireWaterSource> interimList) {
		return fireWaterSourceMapper.batchInsert(interimList);
	}
	
	public void saveExcelTempData(List<FireWaterSourceTemp> tempList) {
		if (tempList != null && tempList.size() > 0) {
			List<FireWaterSourceTemp> interimList = new ArrayList<FireWaterSourceTemp>();
			int num = 0;
			for(int i=0;i<tempList.size();i++){
				interimList.add(tempList.get(i));
				if(i!=0&&i%50==0){
					num = num + this.batchInsertTemp(interimList);
					interimList.clear();
				}
			}
			if(interimList.size()>0){
				num = num + this.batchInsertTemp(interimList);
				interimList.clear();
			}
			if (num != tempList.size()) {
				throw new BusinessException("导入数据异常!");
			}
		}
	}
	
	private int batchInsertTemp(List<FireWaterSourceTemp> interimList) {
		return fireWaterSourceMapper.batchInsertTemp(interimList);
	}
	
	public List<FireWaterSourceTemp> qryExcelTemp(Long timestamp) {
		return fireWaterSourceMapper.qryExcelTemp(timestamp);
	}
	
	public void delExcelTemp(Long timestamp) {
		fireWaterSourceMapper.delExcelTemp(timestamp);
	}
	

	

}
