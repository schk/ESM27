package cn.stronglink.esm27.module.danger.danger.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Dangers;
import cn.stronglink.esm27.entity.DangersCatalog;
import cn.stronglink.esm27.entity.KeyUnitDangers;
import cn.stronglink.esm27.module.danger.danger.mapper.DangersMapper;
import cn.stronglink.esm27.module.danger.danger.vo.DangersVo;
import cn.stronglink.esm27.module.danger.danger.vo.DangersWithTypeVo;

@Service
@Transactional(rollbackFor=Exception.class) 
public class DangersService {

	@Autowired
	private DangersMapper dangerMapper;

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<DangersVo> qryListByParams(Page<DangersVo> page, Map<String, Object> params) {
		page.setRecords(dangerMapper.qryListByParams(page,params));	
		return page;
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<KeyUnitDangers> qryDangersByKeyParts(Map<String, Object> params) {
		return dangerMapper.qryDangersByKeyParts(params);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<KeyUnitDangers> qryDangersByKeyUnit(Map<String, Object> params) {
		return dangerMapper.qryDangersByKeyUnit(params);
	}
	
	public void insert(Dangers entity){
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		dangerMapper.insert(entity);	
	}
	
	public void update(Dangers entity) {
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		dangerMapper.updateById(entity);	
	}
	
	public void remove(Long id){
		dangerMapper.deleteById(id);	
	}
	
	public DangersVo selectById(Long id) {
		DangersVo vo=dangerMapper.queryDangersById(id);
		List<DangersCatalog> dangersCatalogList=dangerMapper.getDangersCatalogByDangersId(id);
		if(dangersCatalogList!=null&&dangersCatalogList.size()>0){
			vo.setDangersCatalogs(dangersCatalogList);
		}
		return vo;
	}
	
	public List<DangersCatalog> qryDangerDoc(Long id) {
		return dangerMapper.selectDangersCatalogByDangersId(id);
	}

	public void createDangersVo(DangersVo entity) {
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		dangerMapper.createDangers(entity);
		if(entity.getDangersCatalogs()!=null&&entity.getDangersCatalogs().size()>0){
			for(DangersCatalog catalog:entity.getDangersCatalogs()){
				catalog.setId(IdWorker.getId());
				catalog.setDangersId(entity.getId());
				catalog.setCreateBy(WebUtil.getCurrentUser());
				catalog.setCreateTime(new Date());
			}
			dangerMapper.createDangersCatalogs(entity);
		}
	}
	
	public void updateDangersVo(DangersVo entity) {
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		dangerMapper.updateById(entity);
		dangerMapper.deleteDangersCatalogsById(entity.getId());
		if(entity.getDangersCatalogs()!=null&&entity.getDangersCatalogs().size()>0){
			for(DangersCatalog catalog:entity.getDangersCatalogs()){
				catalog.setId(IdWorker.getId());
				catalog.setDangersId(entity.getId());
				catalog.setCreateBy(WebUtil.getCurrentUser());
				catalog.setCreateTime(new Date());
			}
			dangerMapper.createDangersCatalogs(entity);
		}
	}
	public void deleteDangersVo(Long id) {
		dangerMapper.deleteById(id);
		dangerMapper.deleteDangersCatalogsById(id);
	}
	
	public Page<DangersWithTypeVo>  selectAll(Page<DangersWithTypeVo> page ){
		List<DangersWithTypeVo> selectAll = dangerMapper.selectAll(page);
		page.setRecords(selectAll);
		return page;
	}
	
	public DangersVo queryDangersById(Long dangersId) {
		DangersVo dangersWithTypeVo = dangerMapper.queryDangersById(dangersId);
		return dangersWithTypeVo;
	}

	

}
