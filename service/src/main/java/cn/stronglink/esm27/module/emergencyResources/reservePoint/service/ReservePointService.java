package cn.stronglink.esm27.module.emergencyResources.reservePoint.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.ReservePoint;
import cn.stronglink.esm27.module.emergencyResources.reservePoint.mapper.ReservePointMapper;
import cn.stronglink.esm27.module.emergencyResources.reservePoint.vo.ReservePointVo;

@Service
@Transactional(rollbackFor=Exception.class)
public class ReservePointService {
	
	@Autowired
	private ReservePointMapper reservePointMapper;
	
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<ReservePointVo> qryListByParams(Page<ReservePointVo> page, Map<String, Object> params) {
		if(params.get("type")!=null&&"1".equals(params.get("type"))) {
			page.setRecords(reservePointMapper.qryListForUnit(page,params));
		}else if(params.get("type")!=null&&"2".equals(params.get("type"))) {
			page.setRecords(reservePointMapper.qryListForBrigade(page,params));
		}else if(params.get("type")!=null&&"3".equals(params.get("type"))) {
			page.setRecords(reservePointMapper.qryListForWu(page,params));
		}else {
			page.setRecords(reservePointMapper.qryListByParams(page,params));	
		}
		return page;
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<ReservePointVo> qryListByQueryParams(Map<String, Object> params) {
		List<ReservePointVo> list = reservePointMapper.qryListByQueryParams(params);
		return list;
	}
	/*
	 * 根据id查询信息
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public ReservePointVo selectById(Map<String, Object> params){
		ReservePointVo vo =null;
		String type = params.get("type").toString();
		Long reservePointId = Long.parseLong(params.get("id").toString());
		switch(type){
			case "1":
				//查询与重点单位的关联
			    vo = reservePointMapper.qryWithKeyUnit(reservePointId);
			    break;
			case "2":
				//查询与消防队的关联
				vo = reservePointMapper.qryWithFireBrigade(reservePointId);
			    break;
			case "3":
				//查询与重点部位的关联
				vo = reservePointMapper.qryById(reservePointId);
			    break;
		}
		vo.setLonLat(vo.getLon()+","+vo.getLat());
		return vo;
	}
	
	/*
	 * 根据id删除信息
	 */
	public void  remove(Long id){
		reservePointMapper.deleteById(id);
	}
	/*
	 * 添加信息
	 */
	public void insert(ReservePoint entity) {
		if(!"".equals(entity.getLonLat())) {
			String [] lonLat=entity.getLonLat().split(",");
			Double lonBd = new Double(lonLat[0]);
			Double latBd = new Double(lonLat[1]);
			entity.setLon(lonBd);
			entity.setLat(latBd);
		}
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		reservePointMapper.insert(entity);
	}
	//根据id修改信息
	public void update(ReservePoint entity) {
		if(!"".equals(entity.getLonLat())) {
			String [] lonLat=entity.getLonLat().split(",");
			Double lonBd = new Double(lonLat[0]);
			Double latBd = new Double(lonLat[1]);
			entity.setLon(lonBd);
			entity.setLat(latBd);
		}
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if(reservePointMapper.updateById(entity)==0){
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
	}
	
	public List<ReservePoint> qryReservePointList( ) {
		return reservePointMapper.qryReservePointList();
	}
	
	public List<ReservePoint> qryReserveByType(Map<String, Object> params) {
		return reservePointMapper.qryReserveByType(params);
	}

	public void saveExcelData(List<ReservePoint> entityList) {
		if (entityList != null && entityList.size() > 0) {
			List<ReservePoint> interimList = new ArrayList<ReservePoint>();
			int j = (int) Math.ceil(entityList.size() / (double) 50);
			int num = 0;
			for (int i = 1; i <= j; i++) {
				if (i*50>=entityList.size()) {
					interimList = new ArrayList<ReservePoint>(entityList.subList((i - 1) * 50, entityList.size()));
					num =num+this.batchInsert(interimList);
				}else {
					interimList = new ArrayList<ReservePoint>(entityList.subList((i - 1) * 50, i * 50));
					num =num+this.batchInsert(interimList);
				}
			}
			if (num != entityList.size()) {
				throw new BusinessException("导入数据异常!");
			}
		}
	}

	private int batchInsert(List<ReservePoint> interimList) {
		return reservePointMapper.batchInsert(interimList);
	}

}
