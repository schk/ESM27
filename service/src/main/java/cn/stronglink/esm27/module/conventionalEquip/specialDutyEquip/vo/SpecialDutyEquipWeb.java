package cn.stronglink.esm27.module.conventionalEquip.specialDutyEquip.vo;

public class SpecialDutyEquipWeb {
	
	
	private String name;
	private Double flow;
	private Integer foamAvailable;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getFlow() {
		return flow;
	}
	public void setFlow(Double flow) {
		this.flow = flow;
	}
	public Integer getFoamAvailable() {
		return foamAvailable;
	}
	public void setFoamAvailable(Integer foamAvailable) {
		this.foamAvailable = foamAvailable;
	}
	
	
}
