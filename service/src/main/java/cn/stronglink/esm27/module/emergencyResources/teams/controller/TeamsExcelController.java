package cn.stronglink.esm27.module.emergencyResources.teams.controller;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.core.util.BmUtils;
import cn.stronglink.core.util.ImportValid;
import cn.stronglink.core.util.MapLonLatUtil;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Teams;
import cn.stronglink.esm27.module.emergencyResources.teams.service.TeamsService;

@Controller
@RequestMapping(value = "teams/excel")
public class TeamsExcelController extends AbstractController {

	@Autowired
	private TeamsService entityService;

	/**
	 * 导入EXCEL文件
	 * 
	 * @param file
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "importExcel")
	public ResponseEntity<ModelMap> importExcel(@RequestParam(value = "file", required = true) MultipartFile file,
			HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) throws Exception {
		List<String> dataList = new ArrayList<String>();
		System.out.println(WebUtil.getCurrentUser());
		// 导入Excel操作
		// 导入Excel文件不为空时，将文件写入流，然后转化为工作薄
		if (file != null) {
			try {
				Workbook workBook = null;
				if (!ImportValid.validXls(file)) {
					return setModelMap(modelMap, HttpCode.CONFLICT, "请选择excel格式文件！");
				}
				int header = 1;
				workBook = WorkbookFactory.create(file.getInputStream());
				if (workBook.getNumberOfSheets() > 0) {
					// 获取第一个单元薄
					Sheet sheet = workBook.getSheetAt(0);
					// 获取单元薄中表头信息,并将表头中每个单元格中信息存入数组headNames中
					String allTitle = "*队伍名称$*队伍位置$*坐标类型(百度坐标/无偏移坐标)$经纬度$*队伍性质$*队伍规模$*队伍负责人$*责任人联系方式$描述";
					Row rowHead = sheet.getRow(header);
					if (rowHead != null) {
						String[] headNames = new String[rowHead.getLastCellNum()];
						// 获取该行中总共有多少列数据row.getLastCellNum()
						for (int j = 0; j < rowHead.getLastCellNum(); j++) {
							Cell cell = rowHead.getCell(j);
							// 获取表头各个字段名称
							String returnStr = BmUtils.getCellValue(cell);
							headNames[j] = returnStr;
						}
						if (headNames != null && headNames.length > 0) {
							for (String name : headNames) {
								if (allTitle.indexOf(name) == -1) {
									throw new BusinessException("属性：" + name + "在系统中不存在,请使用模板文件！");
								}
							}
						}
						Date date = new Date();
						// 遍历单元簿中每一行，获取每一列字段，并根据表头对应赋值
						int flag = 3;
						List<Teams> entityList = new ArrayList<Teams>();

						int dataFormat = 0;
						modelMap.put("errorCode", 0);
						DecimalFormat df = new DecimalFormat("#.000000");
						for (int i = header; i < sheet.getPhysicalNumberOfRows(); i++) {
							Row row = sheet.getRow(i + 1);
							Boolean isRow = ImportValid.isRowEmpty(row);
							Teams entity = null;
							if (!isRow) {
								String rowError = "错误： 第" + flag + "行   ";
								String data = "";
								entity = new Teams();
								entity.setId(IdWorker.getId());
								entity.setCreateBy(WebUtil.getCurrentUser());
								entity.setCreateTime(date);
								entity.setUpdateTime(date);

								String lonLatType = "";

								// 获取该行中总共有多少列数据row.getLastCellNum()
								for (int j = 0; j < row.getLastCellNum(); j++) {
									if(j>headNames.length-1) {
										continue ;
									}
									Cell cell = row.getCell(j);
									if (StringUtils.hasText(headNames[j])) {
										// 获取单元格内容，并根据单元格类型，进行转化
										String returnStr = BmUtils.getCellValue(cell);
										// 根据给定的Excel格式，进行导入，一下是对应列对应字段
										if (headNames[j].equals("*队伍名称")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setName(returnStr);
											} else {
												dataFormat++;
												data += "【队伍名称】不能为空;";
											}
										} else if (headNames[j].equals("*队伍位置")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setPosition(returnStr);
											} else {
												dataFormat++;
												data += "【队伍位置】不能为空;";
											}
										} else if (headNames[j].equals("*坐标类型(百度坐标/无偏移坐标)")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												lonLatType = returnStr;
											} else {
												dataFormat++;
												data += "【坐标类型：" + returnStr + "】不能为空;";
											}
										} else if (headNames[j].equals("经纬度")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												String[] coord = returnStr.split(",");
												if (coord != null && coord.length == 2) {
													coord[0] = df.format(Double.valueOf(coord[0]));
													coord[1] = df.format(Double.valueOf(coord[1]));
													if ("百度坐标".equals(lonLatType)) {
														double[] pointwgs = MapLonLatUtil.bd09py(
																Double.valueOf(coord[0]), Double.valueOf(coord[1]));
														entity.setBaiduLon(Double.valueOf(coord[0]));
														entity.setBaiduLat(Double.valueOf(coord[1]));
														entity.setLon(Double.valueOf(pointwgs[0]));
														entity.setLat(Double.valueOf(pointwgs[1]));
													} else if ("无偏移坐标".equals(lonLatType)) {
														entity.setLon(Double.valueOf(coord[0]));
														entity.setLat(Double.valueOf(coord[1]));
													} else {
														dataFormat++;
														data += "【坐标类型】只能是百度坐标/无偏移坐标;";
													}
												} else {
													data += "【经纬度】格式错误;";
												}
											}
										} else if (headNames[j].equals("*队伍性质")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setProperties(returnStr);
											} else {
												dataFormat++;
												data += "【队伍性质】不能为空;";
											}
										} else if (headNames[j].equals("*队伍规模")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setScale(returnStr);
											} else {
												dataFormat++;
												data += "【队伍规模】不能为空;";
											}
										} else if (headNames[j].equals("*队伍负责人")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setCharge(returnStr);
											} else {
												dataFormat++;
												data += "【队伍负责人】不能为空;";
											}
										} else if (headNames[j].equals("*责任人联系方式")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setPhone(returnStr);
											} else {
												dataFormat++;
												data += "【责任人联系方式】不能为空;";
											}
										} else if (headNames[j].equals("描述")) {
											entity.setRemark(returnStr);
										}
									}
								}
								flag++;
								entityList.add(entity);
								if (data != null && !"".equals(data)) {
									dataList.add(rowError + data);
								}
							}
						}
						if (dataList != null && dataList.size() > 0) {
							if (dataFormat > 0) {
								modelMap.put("errorCode", 2);
								return setSuccessModelMap(modelMap, dataList);
							}
						} else {
							// 存数据
							if (entityList != null && entityList.size() > 0) {
								// 数据存储
								entityService.saveExcelData(entityList);
								return setSuccessModelMap(modelMap);
							} else {
								throw new BusinessException("文件数据为空!");
							}
						}
					}
				}
			} catch (IOException e) {
				throw new BusinessException("未知异常!");
			}
		}
		throw new BusinessException("无文件!");
	}

}
