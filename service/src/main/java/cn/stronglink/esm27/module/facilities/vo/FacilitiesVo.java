package cn.stronglink.esm27.module.facilities.vo;

import cn.stronglink.esm27.entity.Facilities;

public class FacilitiesVo extends Facilities{

	/**
	 * 重点单位
	 */
	private static final long serialVersionUID = 2009889022936377291L;
	
	private String keyUnitName;
	private String equipmentTypeName;
	private String typeName;
	private String distances;
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getKeyUnitName() {
		return keyUnitName;
	}
	public void setKeyUnitName(String keyUnitName) {
		this.keyUnitName = keyUnitName;
	}
	public String getEquipmentTypeName() {
		return equipmentTypeName;
	}
	public void setEquipmentTypeName(String equipmentTypeName) {
		this.equipmentTypeName = equipmentTypeName;
	}
	public String getDistances() {
		return distances;
	}
	public void setDistances(String distances) {
		this.distances = distances;
	}
	
	
	
	

}
