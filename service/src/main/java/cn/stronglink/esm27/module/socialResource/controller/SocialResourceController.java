package cn.stronglink.esm27.module.socialResource.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.SocialResource;
import cn.stronglink.esm27.module.socialResource.service.SocialResourceService;

@Controller
@RequestMapping(value = "socialResource")
public class SocialResourceController extends AbstractController {
	@Autowired
	private SocialResourceService socialResourceService;

	/**
	 * 查询社会资源列表
	 * 
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "qryList")
	public ResponseEntity<ModelMap> qryList(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, Object> params) {
		Page<SocialResource> page = (Page<SocialResource>) super.getPage(params);
		Page<SocialResource> data = socialResourceService.getListByParams(page, params);
		return setSuccessModelMap(modelMap, data);
	}

	@RequestMapping(value = "create")
	@OperateLog(module = "社会资源", desc = "添加社会资源", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			@RequestBody SocialResource entity) {
		// 查询当前单位名称是否存在
		int count = socialResourceService.getCountByName(entity);
		if (count > 0) {
			throw new BusinessException("此名称已存在");
		}
		socialResourceService.insert(entity);
		return setSuccessModelMap(modelMap, null);
	}

	@RequestMapping(value = "edit")
	@OperateLog(module = "社会资源", desc = "修改社会资源", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			@RequestBody SocialResource entity) {
		// 查询当前单位名称是否存在
		int count = socialResourceService.getCountByName(entity);
		if (count > 0) {
			throw new BusinessException("此名称已存在");
		}
		socialResourceService.update(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 查询某个对象
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> getSocialResource(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		SocialResource entity = socialResourceService.qryById(id);
		return setSuccessModelMap(modelMap, entity);
	}
	
	@RequestMapping(value = "remove")
	@OperateLog(module = "社会资源",desc="删除社会资源", type = OpType.DEL)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		socialResourceService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}

}
