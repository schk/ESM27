package cn.stronglink.esm27.module.system.user.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.MD5Encrypt;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Department;
import cn.stronglink.esm27.entity.Role;
import cn.stronglink.esm27.entity.User;
import cn.stronglink.esm27.entity.UserRole;
import cn.stronglink.esm27.module.emergencyResources.fireBrigade.mapper.FireBrigadeMapper;
import cn.stronglink.esm27.module.emergencyResources.fireBrigade.vo.FireBrigadeTreeNodeVo;
import cn.stronglink.esm27.module.system.department.mapper.DepartmentMapper;
import cn.stronglink.esm27.module.system.department.vo.DepartmentTreeNodeVo;
import cn.stronglink.esm27.module.system.role.mapper.RoleMapper;
import cn.stronglink.esm27.module.system.user.mapper.UserMapper;
import cn.stronglink.esm27.module.system.user.vo.UserVo;
import cn.stronglink.esm27.shiro.PasswordSaltUtil;

@Service
@Transactional(rollbackFor=Exception.class)
public class UserService {

	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private DepartmentMapper departmentMapper;
	
	@Autowired
	private RoleMapper roleMapper;
	
	@Autowired
	private FireBrigadeMapper fireBrigadeMapper;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<UserVo> getUserByParams(Page<UserVo> page, Map<String, Object> params) {
		page.setRecords(userMapper.getUserByParams(page,params));	
		return page;
	}

	public User qryUserById(Long id) {
		User user = userMapper.selectUserAndBrigade(id);
		List<Long> roles = userMapper.getRoleIds(id);
		user.setRoles(roles);
		return user;
	}
	
	public User findInfo(Long id) {
		//User user = userMapper.selectById(id);
		User user = userMapper.selectUserAndBrigade(id);
		String roleName="";
		List<Long> roles = userMapper.getRoleIds(id);
		for (int i = 0; i < roles.size(); i++) {
			Role selectById = roleMapper.selectById(roles.get(i));
			roleName+=selectById.getName()+",";
		}
		user.setRoleName(roleName);
		user.setRoles(roles);
		Department selectById = departmentMapper.selectById(user.getDeptId());
		if(selectById!=null){
		user.setDeptName(selectById.getName());
		}
		return user;
	}

	public void insertUser(User entity) {
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		Long userId = IdWorker.getId();
		entity.setId(userId);
		entity.setPassword(entity.getPassword());
		entity = PasswordSaltUtil.encrypt(entity);
		userMapper.insert(entity);
		Long userBrigadeRefId = IdWorker.getId();
		userMapper.insertUserBrigadeRef(userBrigadeRefId,entity.getId(),entity.getFireBrigadeId());
		//保存角色员工关系信息
		List<Long> roles = entity.getRoles();
		if(roles!=null&&roles.size()>0){
			for (Long roleId : roles) {
				UserRole userRole = new UserRole();
				userRole.setId(IdWorker.getId());
				userRole.setUserId(userId);
				userRole.setRoleId(roleId);
				userRole.setCreateTime(new Date());
				userRole.setCreateBy(WebUtil.getCurrentUser());
				userMapper.insertUserRole(userRole);
			}
		}
	}

	public User checkUser(String name,String password){
		System.out.println(MD5Encrypt.encrypt(password));
		User user=userMapper.selectUserByNamePass(name,MD5Encrypt.encrypt(password));
		return user;
		
	}
	
	public void updateUser(User entity) {
		User user = userMapper.selectById(entity.getId());
		entity.setAccount(user.getAccount());
		if(!StringUtils.isEmpty(entity.getPassword())){
			entity = PasswordSaltUtil.encrypt(entity);
		}else {
			entity.setPassword(user.getPassword());
			entity.setSalt(user.getSalt());
		}
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if(userMapper.updateAllColumnById(entity)==0){
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
		
		userMapper.updateUserBrigadeRef(entity.getId(),entity.getFireBrigadeId());
		userMapper.deleteUserRole(entity.getId());
		//保存新的角色员工关系信息
		List<Long> roles = entity.getRoles();
		if(roles!=null&&roles.size()>0){
			for (Long roleId : roles) {
				UserRole userRole = new UserRole();
				userRole.setId(IdWorker.getId());
				userRole.setUserId(entity.getId());
				userRole.setRoleId(roleId);
				userRole.setCreateTime(new Date());
				userRole.setCreateBy(WebUtil.getCurrentUser());
				userMapper.insertUserRole(userRole);
			}
		}
	}

	public void removeUser(Long id) {
		userMapper.deleteById(id);
		userMapper.deleteUserRole(id);
		userMapper.deleteBrigadeRef(id);
	}

	public Integer getCountByUsername(User entity) {
		return userMapper.getCountByUsername(entity);
	}

	public User selectByid(Long id) {
		return userMapper.selectById(id);
	}

	public void resetUserPW(User entity) {
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		entity = PasswordSaltUtil.encrypt(entity);
		userMapper.resetUserPW(entity);
	}
	
	public User findUserByName(String username) {
		return userMapper.findUserByName(username);
	}

	public List<String> getSysPermissionByUsername(String username) {
		return userMapper.getSysPermissionByUsername(username);
	}

	public List<Long> selectDeptId(Long id) {	
		List<Long> userdeptIds =new ArrayList<Long>();
		Long userdeptId =userMapper.selectDeptId(id);
		if(userdeptId!=null){
			userdeptIds.add(userdeptId);
			List<DepartmentTreeNodeVo> brigadeAll = departmentMapper.qryRootDeptNoTop();
			recursionDeptId(brigadeAll, userdeptId,userdeptIds);  
		}
		
		return userdeptIds;
	}
	
	private void recursionDeptId(List<DepartmentTreeNodeVo> list, Long pid,List<Long> brigadeIds) {
		List<Long> childList = getChildListDeptId(list, pid);// 得到子节点列表
		if (!CollectionUtils.isEmpty(childList)) {
			brigadeIds.addAll(childList);
			Iterator<Long> it = childList.iterator();
			while (it.hasNext()) {
				Long n = (Long) it.next();
				recursionDeptId(list, n,brigadeIds);
			}
		} 
	}
	
	private List<Long> getChildListDeptId(List<DepartmentTreeNodeVo> list, Long pid) {  
        List<Long> nodeList = new ArrayList<Long>();  
        Iterator<DepartmentTreeNodeVo> it = list.iterator();  
        while (it.hasNext()) {  
        	DepartmentTreeNodeVo n = (DepartmentTreeNodeVo) it.next();  
            if (String.valueOf(n.getPid()).equals(String.valueOf(pid))) {  
                nodeList.add(n.getId());  
            }  
        }  
        return nodeList;  
    }
	
	public Long selectUserBrigade(Long id) {	
		return userMapper.selectUserBrigade(id);
		
	}
	
	public List<Long> selectUserBrigadeChild(Long brigadeId) {	
		List<Long> brigadeIds=new ArrayList<Long>();
		if(brigadeId!=null){
			brigadeIds.add(brigadeId);
			List<FireBrigadeTreeNodeVo> brigadeAll = fireBrigadeMapper.qryAllFireBrigade();
			recursion(brigadeAll, brigadeId,brigadeIds);  
		}
		
		return brigadeIds;
	}
	
	private void recursion(List<FireBrigadeTreeNodeVo> list, Long pid,List<Long> brigadeIds) {
		List<Long> childList = getChildList(list, pid);// 得到子节点列表
		if (!CollectionUtils.isEmpty(childList)) {
			brigadeIds.addAll(childList);
			Iterator<Long> it = childList.iterator();
			while (it.hasNext()) {
				Long n = (Long) it.next();
				recursion(list, n,brigadeIds);
			}
		} 
	}
	
	
	private List<Long> getChildList(List<FireBrigadeTreeNodeVo> list, Long pid) {  
        List<Long> nodeList = new ArrayList<Long>();  
        Iterator<FireBrigadeTreeNodeVo> it = list.iterator();  
        while (it.hasNext()) {  
        	FireBrigadeTreeNodeVo n = (FireBrigadeTreeNodeVo) it.next();  
            if (n.getPid().equals(String.valueOf(pid))) {  
                nodeList.add(n.getId());  
            }  
        }  
        return nodeList;  
    }

	public Long findDeptIdByName(String deptName) {
		return userMapper.findDeptIdByName(deptName);
	}
	
	public Long findBrigadeIdByName(String brigadeName) {
		return userMapper.findBrigadeIdByName(brigadeName);
	}
	
	public void batchInsertUser(List<User> userList) {
		userMapper.batchInsertUser(userList);
		userMapper.batchInsertUserBrigadeRef(userList);
	}
}
