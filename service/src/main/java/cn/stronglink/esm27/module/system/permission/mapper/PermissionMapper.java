package cn.stronglink.esm27.module.system.permission.mapper;

import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.Permission;
import cn.stronglink.esm27.module.system.permission.vo.PermissionVo;
import cn.stronglink.esm27.module.system.role.vo.RoleVo;

public interface PermissionMapper extends BaseMapper<Permission>{

	List<PermissionVo> getPermissionList(@Param("pid") Long pid);

	List<Long> getRolePermission(@Param("id") Long id);

	void delRolePermissionById(@Param("id")Long id);

	void createRolePermissionById(RoleVo vo);
	
	Set<String> getPermsByUserId(@Param("userId")Long userId);

	List<Long> getRolePermissionNoP(@Param("id") Long id);

}
