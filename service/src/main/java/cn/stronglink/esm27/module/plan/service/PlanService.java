package cn.stronglink.esm27.module.plan.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Plan;
import cn.stronglink.esm27.entity.PlanTemp;
import cn.stronglink.esm27.entity.PlansCatalog;
import cn.stronglink.esm27.module.plan.mapper.PlanMapper;
import cn.stronglink.esm27.module.plan.vo.PlanVo;

@Service
@Transactional(rollbackFor = Exception.class)
public class PlanService {

	@Autowired
	private PlanMapper planMapper;

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Page<PlanVo> qryListByParams(Page<PlanVo> page, Map<String, Object> params) {
		page.setRecords(planMapper.qryListByParams(page, params));
		return page;
	}

	public Page<PlanVo> qryListByKeyParts(Page<PlanVo> page, Map<String, Object> params) {
		page.setRecords(planMapper.qryListByKeyParts(page, params));
		return page;
	}
	
	/* 根据id查询 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public PlanVo qryById(Map<String, Object> params) {
		PlanVo vo =null;
		String type = params.get("type").toString();
		Long planId = Long.parseLong(params.get("id").toString());
		switch(type){
			case "1":
				//查询与重点单位的关联
			    vo = planMapper.qryWithKeyUnit(planId);
			    break;
			case "2":
				//查询与消防队的关联
				vo = planMapper.qryWithFireBrigade(planId);
			    break;
			case "3":
				//查询与重点部位的关联
				vo = planMapper.qryWithKeyParts(planId);
			    break;
			case "4":
				//查询与重点部位的关联
				vo = planMapper.qryPlanById(planId);
			    break;    
		}
		// 获取文件目录数据
		List<PlansCatalog> plansCatalogs = planMapper.getPlansCatalogs(planId);
		if (plansCatalogs != null && plansCatalogs.size() > 0) {
			vo.setPlansCatalogs(plansCatalogs);
		}
		return vo;
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public PlanVo getPlansInfo(Long id) {
		PlanVo vo = planMapper.qryById(id);
		if (vo.getLevel() == 1) {
			vo.setLevelName("Ⅰ级");
		} else if (vo.getLevel() == 2) {
			vo.setLevelName("Ⅱ级");
		} else if (vo.getLevel() == 3) {
			vo.setLevelName("Ⅲ级");
		} else if (vo.getLevel() == 4) {
			vo.setLevelName("Ⅳ级");
		}
		// 获取文件目录数据
		List<PlansCatalog> plansCatalogs = planMapper.getPlansCatalogs(id);
		if (plansCatalogs != null && plansCatalogs.size() > 0) {
			vo.setPlansCatalogs(plansCatalogs);
		}
		return vo;
	}

	public void insert(Plan entity) {
		entity.setId(IdWorker.getId());
		entity.setStatus(1);
		entity.setFreq(0);
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		planMapper.insert(entity);

	}

	public void update(Plan entity) {
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if (planMapper.updateById(entity) == 0) {
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
	}

	public void remove(Long id) {
		planMapper.deleteById(id);
	}

	public void updateFreq(Long id) {
		planMapper.updateFreq(id);
	}

	public void createPlan(PlanVo entity) {
		entity.setId(IdWorker.getId());
		entity.setStatus(1);
		entity.setFreq(0);
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(new Date());
		if (entity.getPath() != null) {
			String swfPath = entity.getPath().split("\\.")[0] + ".swf";
			entity.setSwfPath(swfPath);
		}
		planMapper.createPlan(entity);
		if (entity.getPlansCatalogs() != null && entity.getPlansCatalogs().size() > 0) {
			for (PlansCatalog catalog : entity.getPlansCatalogs()) {
				catalog.setId(IdWorker.getId());
				catalog.setPlansId(entity.getId());
				catalog.setCreateBy(WebUtil.getCurrentUser());
				catalog.setCreateTime(new Date());
			}
			planMapper.createPlanCatalogs(entity);
		}
	}

	public void updatePlans(PlanVo entity) {
		planMapper.deletePlanCatalogs(entity.getId());
		if (entity.getPath() != null) {
			String swfPath = entity.getPath().split("\\.")[0] + ".swf";
			entity.setSwfPath(swfPath);
		}
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		planMapper.updateById(entity);
		if (entity.getPlansCatalogs() != null && entity.getPlansCatalogs().size() > 0) {
			for (PlansCatalog catalog : entity.getPlansCatalogs()) {
				catalog.setId(IdWorker.getId());
				catalog.setPlansId(entity.getId());
				catalog.setCreateBy(WebUtil.getCurrentUser());
				catalog.setCreateTime(new Date());
			}
			planMapper.createPlanCatalogs(entity);
		}
	}

	public void removePlans(Long id) {
		planMapper.deleteById(id);
		planMapper.deletePlanCatalogs(id);
	}

	public void saveExcelData(List<Plan> entityList) {
		if (entityList != null && entityList.size() > 0) {
			List<Plan> interimList = new ArrayList<Plan>();
			int j = (int) Math.ceil(entityList.size() / (double) 50);
			int num = 0;
			for (int i = 1; i <= j; i++) {
				if (i*50>=entityList.size()) {
					interimList = new ArrayList<Plan>(entityList.subList((i - 1) * 50, entityList.size()));
					num =num+this.batchInsertEntity(interimList);
				}else {
					interimList = new ArrayList<Plan>(entityList.subList((i - 1) * 50, i * 50));
					num =num+this.batchInsertEntity(interimList);
				}
			}
			if (num != entityList.size()) {
				throw new BusinessException("导入数据异常!");
			}
		}
	}

	private int batchInsertEntity(List<Plan> interimList) {
		return planMapper.batchInsertEntity(interimList);
	}

	public void saveExcelTempData(List<PlanTemp> tempList) {
		if (tempList != null && tempList.size() > 0) {
			List<PlanTemp> interimList = new ArrayList<PlanTemp>();
			int j = (int) Math.ceil(tempList.size() / (double) 50);
			int num = 0;
			for (int i = 1; i <= j; i++) {
				if (i*50>=tempList.size()) {
					interimList = new ArrayList<PlanTemp>(tempList.subList((i - 1) * 50, tempList.size()));
					num =num+this.batchInsertTemp(interimList);
				}else {
					interimList = new ArrayList<PlanTemp>(tempList.subList((i - 1) * 50, i * 50));
					num =num+this.batchInsertTemp(interimList);
				}
			}
			if (num != tempList.size()) {
				throw new BusinessException("导入数据异常!");
			}
		}
	}

	private int batchInsertTemp(List<PlanTemp> interimList) {
		return planMapper.batchInsertTemp(interimList);
	}

	public List<PlanTemp> qryExcelTemp(Long timestamp) {
		return planMapper.qryExcelTemp(timestamp);
	}

	public void delExcelTemp(Long timestamp) {
		planMapper.delExcelTemp(timestamp);
	}

	public List<PlanVo> qryPlansByBrigade(Long keyUnitId) {
		return planMapper.qryPlansByBrigade(keyUnitId);
	}

	public List<PlanVo> qryPlansByKeyUnit(Long keyUnitId) {
		return planMapper.qryPlansByUnit(keyUnitId);
	}

	public List<PlanVo> qryPlansByKeyParts(Long keyUnitId) {
		return planMapper.qryPlanByKeyParts(keyUnitId);
	}

	public PlanVo qryPlanById(Long id) {
		return planMapper.qryPlanById(id);
	}

	public int updateKeyUnitById(Long keyUnitId) {
		return planMapper.updateKeyUnitById(keyUnitId);
	}

	public void updateKeyUnit(PlanVo planVo) {
		for (Long planId : planVo.getPlanIdList()) {
			planMapper.updateKeyUnit(planId,planVo.getKeyUnitId(),planVo.getType());
		}
	}

	public void delKeyUnitByPlanId(Long id) {
		planMapper.delKeyUnitByPlanId(id);
	}
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Page<PlanVo> qryNotAddedPlanList(Page<PlanVo> page, Map<String, Object> params) {
		List<PlanVo> list = planMapper.qryNotAddedPlanList(page, params);
		page.setRecords(list);
		return page;
	}
	

}
