package cn.stronglink.esm27.module.expert.expertType.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.ExpertType;
import cn.stronglink.esm27.module.expert.expertType.mapper.ExpertTypeMapper;

@Service
@Transactional(rollbackFor=Exception.class)
public class ExpertTypeService {
	
	@Autowired
	private ExpertTypeMapper expertTypeMapper;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<ExpertType> qryListByParams(){
		return expertTypeMapper.qryListByParams();
	}


	public void insert(ExpertType entity) {
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		expertTypeMapper.insert(entity);
	}


	public void update(ExpertType entity) {
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		expertTypeMapper.updateById(entity);
		
	}


	public void remove(Long id) {
		expertTypeMapper.deleteById(id);
		
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public ExpertType qryById(Long id) {
		return expertTypeMapper.selectById(id);
	}

}
