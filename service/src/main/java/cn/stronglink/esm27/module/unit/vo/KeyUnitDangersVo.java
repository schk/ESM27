package cn.stronglink.esm27.module.unit.vo;

import java.util.List;

import cn.stronglink.core.base.BaseModel;
import cn.stronglink.esm27.entity.KeyUnitDangers;

public class KeyUnitDangersVo extends BaseModel{

	/**
	 * 重点单位扩展属性
	 */
	private static final long serialVersionUID = 4366608878209861297L;
	
    private List<KeyUnitDangers> dList ;
    
	public List<KeyUnitDangers> getdList() {
		return dList;
	}
	public void setdList(List<KeyUnitDangers> dList) {
		this.dList = dList;
	}
    
}
