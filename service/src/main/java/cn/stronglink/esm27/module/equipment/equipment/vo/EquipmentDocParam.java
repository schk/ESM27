package cn.stronglink.esm27.module.equipment.equipment.vo;

import java.util.List;

import cn.stronglink.esm27.entity.EquipmentDoc;
import cn.stronglink.esm27.entity.EquipmentDocCatalog;

public class EquipmentDocParam extends EquipmentDoc {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String docName;

	private String equipmentTypeName;
	public String getEquipmentTypeName() {
		return equipmentTypeName;
	}
	public void setEquipmentTypeName(String equipmentTypeName) {
		this.equipmentTypeName = equipmentTypeName;
	}
	private List<EquipmentDocCatalog> equipmentDocCatalogs;
	public List<EquipmentDocCatalog> getEquipmentDocCatalogs() {
		return equipmentDocCatalogs;
	}
	public void setEquipmentDocCatalogs(List<EquipmentDocCatalog> equipmentDocCatalogs) {
		this.equipmentDocCatalogs = equipmentDocCatalogs;
	}
	public String getDocName() {
		return docName;
	}
	public void setDocName(String docName) {
		this.docName = docName;
	}
	
	
}
