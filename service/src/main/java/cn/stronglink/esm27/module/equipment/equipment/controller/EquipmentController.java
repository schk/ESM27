package cn.stronglink.esm27.module.equipment.equipment.controller;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.module.equipment.equipment.service.EquipmentService;
import cn.stronglink.esm27.module.equipment.equipment.vo.EquipmentDocParam;
import cn.stronglink.esm27.module.equipment.equipment.vo.EquipmentVo;


@Controller
@RequestMapping(value = "equipment")
public class EquipmentController extends AbstractController  {
	
	@Autowired
	private EquipmentService equipmentService;
	
	/**
	 * 查询所有的生产装置
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<EquipmentVo> page = (Page<EquipmentVo>) super.getPage(params);
		Page<EquipmentVo> data = equipmentService.qryListByParams(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	
	/**
	 * 查询所有的生产装置不带分页
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryList")
	public ResponseEntity<ModelMap> qryList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<EquipmentVo> data = equipmentService.qryList();
		return setSuccessModelMap(modelMap, data);
	}
	
	
	/**
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param id
	 * @return
	 */
	@RequestMapping("qryById")
	public ResponseEntity<ModelMap> qryById(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody Long id){
		EquipmentVo vo =  equipmentService.qryById(id);
		List<EquipmentDocParam> list =  equipmentService.getDocById(id);
		modelMap.put("docList", list);
		return setSuccessModelMap(modelMap,vo);
	}
	
	/*
	 * 根据id查询设备文档
	 */
	@RequestMapping("qryDocById")
	public ResponseEntity<ModelMap> getDocById(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody Long id){
		
		List<EquipmentDocParam> list =  equipmentService.getDocById(id);
		System.out.println(list);
		return setSuccessModelMap(modelMap,list);
	}
	/*
	 * 根据路径删除文件及数据库信息
	 */
	@RequestMapping("deleteDocById")
	public ResponseEntity<ModelMap> deleteDocById(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody Map<String, Object> params){
		
		equipmentService.deleteDocById(params);
		
		String path = (String) params.get("path");
		File file = new File(path);
		file.delete();
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 新建人员
	 */
	@RequestMapping(value = "create")
	@OperateLog(module = "生产装置管理",desc="添加生产装置", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody EquipmentVo entity) {
		equipmentService.insert(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 修改装置
	 */
	@RequestMapping(value = "edit")
	@OperateLog(module = "生产装置管理",desc="修改生产装置", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody EquipmentVo entity) {
		equipmentService.update(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	
	/**
	 * 修改装置
	 */
	@RequestMapping(value = "editLocation")
	@OperateLog(module = "生产装置管理",desc="修改生产装置坐标点", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> editLocation(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody EquipmentVo entity) {
		equipmentService.editLocation(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 删除用户
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "remove")
	@OperateLog(module = "生产装置管理",desc="删除生产装置", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		equipmentService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}
	
	

}
