package cn.stronglink.esm27.module.danger.danger.vo;

import java.util.List;

import cn.stronglink.esm27.entity.Dangers;
import cn.stronglink.esm27.entity.DangersCatalog;

public class DangersVo extends Dangers{

	private static final long serialVersionUID = 1L;
	
	private String keyPartsDangersId;
	private String typeName;	
	private String typeIdName;
	
	private List<DangersCatalog> dangersCatalogs;	

	public String getTypeIdName() {
		return typeIdName;
	}

	public void setTypeIdName(String typeIdName) {
		this.typeIdName = typeIdName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public List<DangersCatalog> getDangersCatalogs() {
		return dangersCatalogs;
	}

	public void setDangersCatalogs(List<DangersCatalog> dangersCatalogs) {
		this.dangersCatalogs = dangersCatalogs;
	}

	public String getKeyPartsDangersId() {
		return keyPartsDangersId;
	}

	public void setKeyPartsDangersId(String keyPartsDangersId) {
		this.keyPartsDangersId = keyPartsDangersId;
	}
	
}
