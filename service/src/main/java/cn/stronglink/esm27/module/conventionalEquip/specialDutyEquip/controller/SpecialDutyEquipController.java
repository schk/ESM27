package cn.stronglink.esm27.module.conventionalEquip.specialDutyEquip.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.core.util.BmUtils;
import cn.stronglink.core.util.ImportValid;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Dictionary;
import cn.stronglink.esm27.entity.SpecialDutyEquip;
import cn.stronglink.esm27.entity.SpecialDutyEquipTemp;
import cn.stronglink.esm27.entity.User;
import cn.stronglink.esm27.module.conventionalEquip.specialDutyEquip.service.SpecialDutyEquipService;
import cn.stronglink.esm27.module.conventionalEquip.specialDutyEquip.vo.SpecialDutyEquipVo;
import cn.stronglink.esm27.module.dictionary.service.DictionaryService;
import cn.stronglink.esm27.module.system.user.service.UserService;

@Controller
@RequestMapping("specialDutyEquip")
public class SpecialDutyEquipController extends AbstractController {
	
	@Autowired
	private SpecialDutyEquipService specialDutyEquipService; 
	
	@Autowired
	private DictionaryService dictionaryService;
	
	@Autowired
	private UserService userService;
	/**
	 * 查询列表
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		if(request.getSession().getAttribute("admin")!=null){
			boolean isAdmin = (boolean) request.getSession().getAttribute("admin");
			if(!isAdmin) { 
//				if(request.getSession().getAttribute("userBrigade")!=null){
//					List<Long> fireBrigadeIds = (List<Long>) request.getSession().getAttribute("userBrigade");
//					params.put("fireBrigadeIds",fireBrigadeIds);
//				}
				User u =  (User) request.getSession().getAttribute("currentUser");
				if(u!=null) {
					Long brigadeId =userService.selectUserBrigade(u.getId());
					List<Long> fireBrigadeIds = userService.selectUserBrigadeChild(brigadeId);	
					params.put("fireBrigadeIds",fireBrigadeIds);
				}
			}
		}
		Page<SpecialDutyEquipVo> page = (Page<SpecialDutyEquipVo>) super.getPage(params);
		Page<SpecialDutyEquipVo> data = specialDutyEquipService.qryListByParams(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	
	@RequestMapping(value = "getObject")
	public ResponseEntity<ModelMap> getObject(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<SpecialDutyEquip> page = (Page<SpecialDutyEquip>) super.getPage(params);
		Page<SpecialDutyEquip> data = specialDutyEquipService.getObject(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	@RequestMapping(value = "getDeptNameById")
	public ResponseEntity<ModelMap> getDeptNameById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		SpecialDutyEquipVo entity =specialDutyEquipService.getDeptNameById(id);
		return setSuccessModelMap(modelMap, entity);
	}
	
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		SpecialDutyEquip entity =specialDutyEquipService.selectById(id);
		return setSuccessModelMap(modelMap, entity);
	}
	
	@OperateLog(module = "器材管理",desc="添加器材", type = OpType.ADD)
	@RequestMapping(value = "create")
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody SpecialDutyEquip entity) {
		specialDutyEquipService.insert(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	@OperateLog(module = "器材管理",desc="修改器材", type = OpType.UPDATE)
	@RequestMapping(value = "edit")
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody SpecialDutyEquip entity) {
		specialDutyEquipService.update(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	@OperateLog(module = "器材管理",desc="删除器材", type = OpType.DEL)
	@RequestMapping(value = "remove")
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		specialDutyEquipService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}
	
	@RequestMapping(value = "getSpecialDutyEquipType")
	public ResponseEntity<ModelMap> getSpecialDutyEquipType(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<Map<String,Object>> list=specialDutyEquipService.getSpecialDutyEquipType();
		return setSuccessModelMap(modelMap, list);
	}
	
	@RequestMapping(value = "importExpert")
	public ResponseEntity<ModelMap> importExpert(@RequestParam(value = "file", required = true) MultipartFile file,HttpServletRequest request,
			HttpServletResponse response,ModelMap modelMap) throws Exception{
		String equipType = request.getParameter("equipType");
		List<String> dataList=new ArrayList<String>();
		//导入Excel操作
		//导入Excel文件不为空时，将文件写入流，然后转化为工作薄
		if (file!=null){
			try {	
				Workbook workBook = null;
				if(!ImportValid.validXls(file)){
					return setModelMap(modelMap, HttpCode.CONFLICT, "请选择excel格式文件！");
				}
				
				int header = 1;
				workBook = WorkbookFactory.create(file.getInputStream());
				if (workBook.getNumberOfSheets() > 0){
					//获取第一个单元薄
					Sheet sheet = workBook.getSheetAt(0);
					//获取单元薄中表头信息,并将表头中每个单元格中信息存入数组headNames中
					//1.专勤器材    2.空气呼吸器  3.常规器材
					String allTitle = "";
					if("3".equals(equipType)){
						allTitle = "*器材名称$*所属消防队$*器材类型$*设备编码$*规格型号$*库存$*计量单位$存放位置$*是否可用泡沫$*是否用于喷淋计算$器材流量-(若用于喷淋计算,则必填)";
					}else if("1".equals(equipType)){
						allTitle = "*器材名称$*所属消防队$*器材类型$*资产编码$*设备编码$*规格型号$*库存$*计量单位$存放位置$*是否可用泡沫$*是否用于喷淋计算$器材流量-(若用于喷淋计算,则必填)";
					}else if("2".equals(equipType)){
						allTitle = "*器材名称$*所属消防队$*器材类型$*资产编码$*设备编码$*规格型号$*库存$*计量单位$存放位置";
					}
					
					Row rowHead = sheet.getRow(header); 
					if (rowHead != null) {
						String[] headNames = new String[rowHead.getLastCellNum()];
						// 获取该行中总共有多少列数据row.getLastCellNum()
						for (int j = 0; j < rowHead.getLastCellNum(); j++) {
							Cell cell = rowHead.getCell(j);
							// 获取表头各个字段名称
							String returnStr = BmUtils.getCellValue(cell);
							headNames[j] = returnStr;
							if (!allTitle.contains(returnStr)) {
								throw new BusinessException("属性："+returnStr+"在系统中不存在,请使用模板文件！");
							}
						}
						
						Date date = new Date();
						// 遍历单元簿中每一行，获取每一列字段，并根据表头对应赋值
						int flag = 3;
						List<SpecialDutyEquip> objList = new ArrayList<SpecialDutyEquip>();
						List<SpecialDutyEquipTemp> objTempList = new ArrayList<SpecialDutyEquipTemp>();
						List<Map<String, Object>> typeList = specialDutyEquipService.getSpecialDutyEquipType();
						Boolean typeNoExist = false;
						int dataFormat = 0;
						modelMap.put("errorCode", 0);
						
						//插入execl中得数据到临时表
						long timestamp=System.currentTimeMillis();
						for (int i = header; i < sheet.getPhysicalNumberOfRows(); i++) {
							Row row = sheet.getRow(i+1);
							Boolean isRow = ImportValid.isRowEmpty(row);
							SpecialDutyEquip obj = null;
							SpecialDutyEquipTemp objTemp = null;
							if (!isRow) {
								String rowError = "错误：第"+ flag +"行  ";
								String data="";
								obj = new SpecialDutyEquip();
								obj.setId(IdWorker.getId());
								obj.setCreateBy(WebUtil.getCurrentUser());
								obj.setCreateTime(date);
								obj.setUpdateTime(date);
								obj.setEquipType(Integer.parseInt(equipType));
								
								objTemp = new SpecialDutyEquipTemp();
								objTemp.setId(IdWorker.getId()); 
								objTemp.setCreateBy(WebUtil.getCurrentUser());
								objTemp.setTimestamp(timestamp);
								objTemp.setCreateTime(date);
								objTemp.setEquipType(Integer.parseInt(equipType));
								
								// 获取该行中总共有多少列数据row.getLastCellNum()
								for (int j = 0; j < rowHead.getLastCellNum(); j++) {
									if(j>headNames.length-1) {
										continue ;
									}
									Cell cell = row.getCell(j);
							        if (StringUtils.hasText(headNames[j])) {
										// 获取单元格内容，并根据单元格类型，进行转化
										String returnStr = BmUtils.getCellValue(cell);
										// 根据给定的Excel格式，进行导入，一下是对应列对应字段
									    if (headNames[j].equals("*器材名称")) {
											if(ImportValid.isNotEmpty(returnStr)){
												obj.setName(returnStr);
												objTemp.setName(returnStr);
											}else{
												dataFormat++;
												data += "【器材名称】不能为空;";
											}
										}else if (headNames[j].equals("*设备编码")) {				
											if(ImportValid.isNotEmpty(returnStr)){
												obj.setCode(returnStr);
												objTemp.setCode(returnStr);
											}else{
												dataFormat++;
												data+="【设备编码】不能为空;";
											}
										}else if (headNames[j].equals("*资产编码")) {				
											if(ImportValid.isNotEmpty(returnStr)){
												obj.setSelfCode(returnStr);
												objTemp.setSelfCode(returnStr);
											}else{
												dataFormat++;
												data+="【资产编码】不能为空;";
											}
										}else if (headNames[j].equals("*所属消防队")) {				
											if(ImportValid.isNotEmpty(returnStr)){
												Long brigadeId = specialDutyEquipService.findBrigadeIdByName(returnStr);
												if (brigadeId == null) {
													dataFormat++;
													data+="【所属消防队："+returnStr+"】不存在,请先添加;";
												} else {
													obj.setFireBrigadeId(brigadeId);
													objTemp.setFireBrigadeId(brigadeId);
												}
											}else{
												dataFormat++;
												data += "【所属消防队】不能为空;";
											}
										}else if (headNames[j].equals("*规格型号")) {											
											if(ImportValid.isNotEmpty(returnStr)){
												obj.setSpecificationsModel(returnStr);
												objTemp.setSpecificationsModel(returnStr);
											}else{
												dataFormat++;
												data += "【规格型号】不能为空;";
											}
										}else if (headNames[j].equals("*器材类型")) {				
											if(ImportValid.isNotEmpty(returnStr)){
												objTemp.setType(returnStr);
												if(typeList!=null && typeList.size()>0){
													boolean isExit = false;
													for(Map<String, Object> type : typeList){
														if((returnStr.replace(" ", "")).equals(String.valueOf(type.get("name_")).replace(" ", ""))){
															isExit = true;
															obj.setType(String.valueOf(type.get("id_")));
															break;
														}
													}
													if(!isExit){
														typeNoExist = true;
														data+="【器材类型："+ returnStr +"】不存在;";
													}
												}else{
													typeNoExist = true;
													data+="【器材类型："+ returnStr +"】不存在;";
												}
											}else{
												dataFormat++;
												data+="【器材类型】不能为空;";
											}
										}else if (headNames[j].equals("*库存")) {
											if(ImportValid.isNotEmpty(returnStr)){
												if (ImportValid.isInt(returnStr)) {
													obj.setStorageQuantity(Integer.parseInt(returnStr));
													objTemp.setStorageQuantity(Integer.parseInt(returnStr));
												} else {
													dataFormat++;
													data += "【库存：" + returnStr + "】，格式不正确不是有效数字;";
												}
											}else{
												dataFormat++;
												data+="【库存】不能为空;";
											}
										}else if (headNames[j].equals("*计量单位")) {				
											if(ImportValid.isNotEmpty(returnStr)){
												obj.setUnit(returnStr);
												objTemp.setUnit(returnStr);
											}else{
												dataFormat++;
												data+="【计量单位】不能为空;";
											}
										}else if (headNames[j].equals("存放位置")) {				
											obj.setPosition(returnStr);
											objTemp.setPosition(returnStr);
										}else if (headNames[j].equals("*是否可用泡沫")) {				
											if(ImportValid.isNotEmpty(returnStr)){
												if("是".equals(returnStr)){
													obj.setFoamAvailable(1);
													objTemp.setFoamAvailable(1);
												}else if("否".equals(returnStr)){
													obj.setFoamAvailable(2);
													objTemp.setFoamAvailable(2);
												}else{
													dataFormat++;
													data+="【是否可用泡沫】只能为'是'或者'否';";
												}
											
											}else{
												dataFormat++;
												data+="【是否可用泡沫】不能为空;";
											}
										}else if (headNames[j].equals("*是否用于喷淋计算")) {				
											if(ImportValid.isNotEmpty(returnStr)){
												if("是".equals(returnStr)){
													obj.setCompute(1);
													objTemp.setCompute(1);
												}else if("否".equals(returnStr)){
													obj.setCompute(2);
													objTemp.setCompute(2);
												}else{
													dataFormat++;
													data+="【是否用于喷淋计算】只能为'是'或者'否';";
												}
											
											}else{
												dataFormat++;
												data+="【是否用于喷淋计算】不能为空;";
											}
										}else if (headNames[j].equals("器材流量-(若用于喷淋计算,则必填)")) {				
											if (obj.getCompute()==1) {
												if(returnStr!=null&&!returnStr.equals("")){
													obj.setFlow(Double.valueOf(returnStr));
													objTemp.setFlow(Double.valueOf(returnStr));
												}else{
													dataFormat++;
													data+="【器材流量】不能为空;";
												}
											}
										}
							        }
								}
								flag++;
								
								if(ImportValid.isNotEmpty(data)){
									dataList.add(rowError+data);
								} else {
									objList.add(obj);
								}
								objTempList.add(objTemp);
							}
						}
						if(dataList!=null&&!dataList.isEmpty()){							
							if(dataFormat>0){
								modelMap.put("errorCode",2);
								return setSuccessModelMap(modelMap, dataList);
							}
							if(typeNoExist) {
								modelMap.put("errorCode", 1);
								modelMap.put("times", timestamp);
								//存临时表数据
								if(objTempList != null && !objTempList.isEmpty()) {
									specialDutyEquipService.batchInsertObjTemp(objTempList);//TODO 数据存储
									return setSuccessModelMap(modelMap, dataList);
								} else {
									throw new BusinessException("文件数据为空!");
								}
							}
						}else{
							//存数据
							if(objList != null && !objList.isEmpty()) {
								//TODO 数据存储
								specialDutyEquipService.batchInsertObj(objList);;
								return setSuccessModelMap(modelMap);
							} else {
								throw new BusinessException("文件数据为空!");
							}
						}						
					}
				}
			}catch (IOException e) {
				throw new BusinessException("未知异常!");
			}
		}
		throw new BusinessException("无文件!");
	}

	@RequestMapping(value = "importConfirm")
	public ResponseEntity<ModelMap> importConfirm(HttpServletRequest request,
			HttpServletResponse response,ModelMap modelMap, @RequestBody Long timestamp) throws Exception{
		Date date = new Date();
		// 遍历单元簿中每一行，获取每一列字段，并根据表头对应赋值
		List<SpecialDutyEquip> objList = new ArrayList<SpecialDutyEquip>();
		List<SpecialDutyEquipTemp> objTempList = specialDutyEquipService.qryObjTemp(timestamp);
		List<Map<String, Object>> typeList = specialDutyEquipService.getSpecialDutyEquipType();
		
		List<Dictionary> addTypeList = new ArrayList<Dictionary>();
		//插入execl中得数据到临时表
		for (int j = 0; j < objTempList.size(); j++) {
			SpecialDutyEquipTemp row = objTempList.get(j);
			SpecialDutyEquip obj = null;
			if (row!=null) {
				obj = new SpecialDutyEquip();
				if(typeList!=null &&typeList.size()>0){
					boolean isExit=false;
					for(Map<String, Object> type : typeList){
						if(row.getType().equals(String.valueOf(type.get("name_")))){
							isExit = true;
							obj.setType(String.valueOf(type.get("id_")));
							break;
						}
					}
					
					if(!isExit){
						Dictionary entity = new Dictionary();
						entity.setId(IdWorker.getId());
						entity.setName(row.getType());
						entity.setRemark(row.getType());
						entity.setCreateTime(new Date());
						entity.setCreateBy(WebUtil.getCurrentUser());
						entity.setType(2);
						
						addTypeList.add(entity);
						obj.setType(String.valueOf(entity.getId()));
						
						Map<String, Object> type = new HashMap<String, Object>();
						type.put("id_", entity.getId());
						type.put("name_", row.getType());
						typeList.add(type);
					}
				}
				obj.setId(IdWorker.getId());
				obj.setName(row.getName());
				obj.setCode(row.getCode());
				obj.setSelfCode(row.getSelfCode());
				obj.setFireBrigadeId(row.getFireBrigadeId());
				obj.setSpecificationsModel(row.getSpecificationsModel());
				obj.setEquipType(row.getEquipType());
				obj.setStorageQuantity(row.getStorageQuantity());
				obj.setUnit(row.getUnit());
				obj.setPosition(row.getPosition());
				obj.setFoamAvailable(row.getFoamAvailable());
				obj.setFlow(row.getFlow());
				obj.setCompute(row.getCompute());
				obj.setCreateBy(WebUtil.getCurrentUser());
				obj.setCreateTime(date);
				obj.setUpdateTime(date);
				objList.add(obj);
			}
		}
		
		if (!addTypeList.isEmpty()) {
			dictionaryService.batchInsertType(addTypeList);
		}
		
		if(objList != null && !objList.isEmpty()) {
			//TODO 数据存储
			specialDutyEquipService.batchInsertObj(objList);;
			//删除临时表数据
			specialDutyEquipService.delObjTemp(timestamp);
			return setSuccessModelMap(modelMap);
		} else {
			throw new BusinessException("文件数据为空!");
		}
		
	}
	
	@RequestMapping(value = "delTemp")
	public ResponseEntity<ModelMap> delObjTemp(HttpServletRequest request,
			HttpServletResponse response,ModelMap modelMap, @RequestBody Long timestamp){
		//删除临时表数据
		specialDutyEquipService.delObjTemp(timestamp);
		return setSuccessModelMap(modelMap);
		
	}
}
