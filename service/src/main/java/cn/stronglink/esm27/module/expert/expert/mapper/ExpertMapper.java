package cn.stronglink.esm27.module.expert.expert.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.esm27.entity.Expert;
import cn.stronglink.esm27.entity.ExpertTemp;
import cn.stronglink.esm27.module.expert.expert.vo.ExpertVo;


public interface ExpertMapper extends BaseMapper<Expert> {
	
	List<ExpertVo> qryListByParams(Pagination page, Map<String, Object> params);

	ExpertVo qryById(@Param("id")Long id);

	int batchInsertExpert(@Param("interimList") List<Expert> interimList);

	int batchInsertExpertTemp(@Param("interimList") List<ExpertTemp> interimList);

	List<ExpertTemp> qryExpertTemp(Long timestamp);

	void delExpertTemp(Long timestamp);
	
}
