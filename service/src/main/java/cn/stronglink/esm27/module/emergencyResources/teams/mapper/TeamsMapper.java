package cn.stronglink.esm27.module.emergencyResources.teams.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.esm27.entity.Teams;


public interface TeamsMapper extends BaseMapper<Teams>{

	List<Teams> qryListByParams(Pagination page, Map<String, Object> params);

	int batchInsert(@Param("interimList") List<Teams> interimList);

	List<Teams> qryList();

}
