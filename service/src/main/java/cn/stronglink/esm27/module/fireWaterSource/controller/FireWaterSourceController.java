package cn.stronglink.esm27.module.fireWaterSource.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.FireWaterSource;
import cn.stronglink.esm27.module.fireWaterSource.service.FireWaterSourceService;
import cn.stronglink.esm27.module.fireWaterSource.vo.FireWaterSourceVo;

@Controller
@RequestMapping("fireWaterSource")
public class FireWaterSourceController extends AbstractController {
	
	@Autowired
	private FireWaterSourceService fireWaterSourceService;
	

	/*
	 * 查询所有信息
	 */
	@RequestMapping("qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<FireWaterSourceVo> page = (Page<FireWaterSourceVo>) super.getPage(params);
		Page<FireWaterSourceVo> data = fireWaterSourceService.qryListByParams(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询列表不带分页
	 * @param modelMap
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("qryList")
	public ResponseEntity<ModelMap> qryList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<FireWaterSourceVo> data = fireWaterSourceService.qryList();
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 根据id查询信息
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param id
	 * @return
	 */
	@RequestMapping("qryById")
	public ResponseEntity<ModelMap> selectById(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody Long id){
		FireWaterSourceVo vo =  fireWaterSourceService.selectById(id);
		return setSuccessModelMap(modelMap,vo);
	}
	
	/*
	 * 根据id修改信息
	 */
	@RequestMapping(value = "edit")
	@OperateLog(module = "消防水源管理",desc="修改消防水源", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody FireWaterSource entity){
		Integer count = fireWaterSourceService.findCountByNameAndId(entity.getName(),entity.getId());
		if (count>0) {
			throw new BusinessException("水源名称已存在，请修改水源名称!");
		}else {
			fireWaterSourceService.update(entity);
			return setSuccessModelMap(modelMap, null);
		}
		
	}
	
	
	/*
	 * 根据id修改信息
	 */
	@RequestMapping(value = "editLocation")
	@OperateLog(module = "消防水源管理",desc="修改消防水源", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> editLocation(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody FireWaterSource entity){
		fireWaterSourceService.update(entity);
		return setSuccessModelMap(modelMap, null);
		
	}
	
	/*
	 * 根据id删除信息
	 */
	@RequestMapping(value = "remove")
	@OperateLog(module = "消防水源管理",desc="删除消防水源", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		fireWaterSourceService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}
	/**
	 * 新建信息
	 */
	@RequestMapping(value = "create")
	@OperateLog(module = "消防水源管理",desc="添加消防水源", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody FireWaterSource entity) {
		Integer count = fireWaterSourceService.findCountByName(entity.getName());
		if (count>0) {
			throw new BusinessException("水源名称已存在，请修改水源名称!");
		}else {
			fireWaterSourceService.insert(entity);
			return setSuccessModelMap(modelMap, null);
		}
		
	}
	


}
