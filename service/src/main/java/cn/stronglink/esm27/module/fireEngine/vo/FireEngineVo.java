package cn.stronglink.esm27.module.fireEngine.vo;


import cn.stronglink.esm27.entity.FireEngine;

public class FireEngineVo extends FireEngine {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7652152462385544917L;
	
	private String foamTypeNameO;  //泡沫名称
	private String fireBrigadeName;
	private String dicName;
	private String foamTypeName;  //干粉名称
	private String iconPath;
	private String jhPath;
	private String dtPath;
	private String secondTypeName;  //车辆二级分类名称
	private Integer carCount;      //统计消防队下的消防车数量
	
	public Integer getCarCount() {
		return carCount;
	}

	public void setCarCount(Integer carCount) {
		this.carCount = carCount;
	}

	public String getIconPath() {
		return iconPath;
	}

	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}

	public String getJhPath() {
		return jhPath;
	}

	public void setJhPath(String jhPath) {
		this.jhPath = jhPath;
	}

	public String getDtPath() {
		return dtPath;
	}

	public void setDtPath(String dtPath) {
		this.dtPath = dtPath;
	}

	public String getFoamTypeName() {
		return foamTypeName;
	}

	public void setFoamTypeName(String foamTypeName) {
		this.foamTypeName = foamTypeName;
	}

	public String getDicName() {
		return dicName;
	}

	public void setDicName(String dicName) {
		this.dicName = dicName;
	}
	
	public String getFireBrigadeName() {
		return fireBrigadeName;
	}

	public void setFireBrigadeName(String fireBrigadeName) {
		this.fireBrigadeName = fireBrigadeName;
	}

	public String getFoamTypeNameO() {
		return foamTypeNameO;
	}

	public void setFoamTypeNameO(String foamTypeNameO) {
		this.foamTypeNameO = foamTypeNameO;
	}

	public String getSecondTypeName() {
		return secondTypeName;
	}

	public void setSecondTypeName(String secondTypeName) {
		this.secondTypeName = secondTypeName;
	}

	
	

}
