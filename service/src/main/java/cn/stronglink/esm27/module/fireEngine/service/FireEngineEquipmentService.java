package cn.stronglink.esm27.module.fireEngine.service;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.FireEngineEquipment;
import cn.stronglink.esm27.module.fireEngine.mapper.FireEngineEquipmentMapper;
import cn.stronglink.esm27.module.fireEngine.vo.FireEngineEquipmentVo;

@Service
@Transactional(rollbackFor=Exception.class)
public class FireEngineEquipmentService {
	
	@Autowired
	private FireEngineEquipmentMapper  fireEngineEquipmentMapper;
	
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<FireEngineEquipmentVo> qryListByParams(Page<FireEngineEquipmentVo> page, Map<String, Object> params) {
		page.setRecords(fireEngineEquipmentMapper.qryListByParams(page,params));	
		return page;
	}
	
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public FireEngineEquipment qryById(Long id) {
		return fireEngineEquipmentMapper.selectById(id);
	}
	
	/*
	 * 增加信息
	 */
	public void insert(FireEngineEquipment entity) {
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		fireEngineEquipmentMapper.insert(entity);
		
		
	}
	/*
	 * 修改信息
	 */
	public void update(FireEngineEquipment entity) {
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if(fireEngineEquipmentMapper.updateById(entity)==0){
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
	}
	/*
	 * 删除对象
	 */
	public void remove(Long id) {
		fireEngineEquipmentMapper.deleteById(id);
	}
	


	

}
