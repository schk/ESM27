package cn.stronglink.esm27.module.equipment.equipment.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Equipment;
import cn.stronglink.esm27.entity.EquipmentDocCatalog;
import cn.stronglink.esm27.entity.EquipmentTemp;
import cn.stronglink.esm27.module.equipment.equipment.mapper.EquipmentMapper;
import cn.stronglink.esm27.module.equipment.equipment.vo.EquipmentDocParam;
import cn.stronglink.esm27.module.equipment.equipment.vo.EquipmentVo;

@Service
@Transactional(rollbackFor=Exception.class)
public class EquipmentService {

	@Autowired
	private EquipmentMapper equipmentMapper;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<EquipmentVo> qryListByParams(Page<EquipmentVo> page, Map<String, Object> params) {
		page.setRecords(equipmentMapper.qryListByParams(page,params));	
		return page;
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<EquipmentVo> qryList() {
		return equipmentMapper.qryList();
	}
	
	
	/*
	 * 根据id查看设备文档信息
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<EquipmentDocParam> getDocById(Long id){
		List<EquipmentDocParam> equipmentDocParams=equipmentMapper.getDocById(id);
		if(equipmentDocParams!=null&&equipmentDocParams.size()>0){
			for(EquipmentDocParam param:equipmentDocParams){
				List<EquipmentDocCatalog> equipmentDocCatalogs=equipmentMapper.getEquipmentDocCatalogs(param.getId());
				param.setEquipmentDocCatalogs(equipmentDocCatalogs);
			}
		}
		return equipmentDocParams;
	}
	/*
	 * 根据id删除设备文档信息
	 */
	public void deleteDocById(Map<String, Object> params){
		equipmentMapper.deleteDocById(params);
	}
	/*
	 * 增加信息
	 */
	public void insert(EquipmentVo entity) {
		Date date=new Date();
		if(!"".equals(entity.getLonLat())) {
			String [] lonLat=entity.getLonLat().split(",");
			Double lonBd = new Double(lonLat[0]);
			Double latBd = new Double(lonLat[1]);
			entity.setLon(lonBd);
			entity.setLat(latBd);
		}
		entity.setId(IdWorker.getId());
		List<EquipmentDocCatalog> equipmentDocCatalogs=new ArrayList<EquipmentDocCatalog>();
		if(entity.getEquipmentDocs()!=null&&entity.getEquipmentDocs().size()>0) {
			for (EquipmentDocParam doc:entity.getEquipmentDocs()) {
	            doc.setId(IdWorker.getId());
	            doc.setEquipmentId(entity.getId());
	            doc.setCreateTime(date);
	            doc.setCreateBy(WebUtil.getCurrentUser());
	            doc.setUpdateTime(date);
	            doc.setUpdateBy(WebUtil.getCurrentUser());
	            if (!"".equals(doc.getPath())&&doc.getPath()!=null) {
	            	 String swfPath = doc.getPath().split("\\.")[0]+".swf";
	 	            doc.setSwfPath(swfPath);
	 	           if(doc.getEquipmentDocCatalogs()!=null&&doc.getEquipmentDocCatalogs().size()>0){
		            	for(EquipmentDocCatalog catalog:doc.getEquipmentDocCatalogs()){
		            		catalog.setId(IdWorker.getId());
		            		catalog.setEquipmentDocId(doc.getId());
		            		catalog.setCreateTime(date);
		            		catalog.setCreateBy(WebUtil.getCurrentUser());
		            		equipmentDocCatalogs.add(catalog);
		            	}
		            }
	    		}
	        }
			Map<String, Object> map=new HashMap<String, Object>();
			map.put("equipmentDocs", entity.getEquipmentDocs());
			map.put("equipmentDocCatalogs", equipmentDocCatalogs);
			equipmentMapper.inserEquipmentDocs(map);
			if(equipmentDocCatalogs!=null&&equipmentDocCatalogs.size()>0){
				equipmentMapper.insertDocCatalogs(map);
			}
		}
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		equipmentMapper.insert(entity);
	}
	
	public void editLocation(EquipmentVo entity) {
		if(!"".equals(entity.getLonLat())) {
			String [] lonLat=entity.getLonLat().split(",");
			Double lonBd = new Double(lonLat[0]);
			Double latBd = new Double(lonLat[1]);
			entity.setLon(lonBd);
			entity.setLat(latBd);
		}
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if(equipmentMapper.updateById(entity)==0){
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
		
	}
	
	
	/*
	 * 修改信息
	 */
	public void update(EquipmentVo entity) {
		Date date=new Date();
		if(!"".equals(entity.getLonLat())) {
			String [] lonLat=entity.getLonLat().split(",");
			Double lonBd = new Double(lonLat[0]);
			Double latBd = new Double(lonLat[1]);
			entity.setLon(lonBd);
			entity.setLat(latBd);
		}
		equipmentMapper.deleteEquipmentDocCatalog(entity.getId());
		equipmentMapper.deleteDocByEquipId(entity.getId());
		//
		List<EquipmentDocCatalog> equipmentDocCatalogs=new ArrayList<EquipmentDocCatalog>();
		if(entity.getEquipmentDocs()!=null&&entity.getEquipmentDocs().size()>0) {
			for (EquipmentDocParam doc:entity.getEquipmentDocs()) {
	            doc.setId(IdWorker.getId());
	            doc.setEquipmentId(entity.getId());
	            doc.setCreateTime(date);
	            doc.setUpdateTime(date);
	            doc.setUpdateBy(WebUtil.getCurrentUser());
	            if (!"".equals(doc.getPath())&&doc.getPath()!=null) {
	               String swfPath = doc.getPath().split("\\.")[0]+".swf";
	 	           doc.setSwfPath(swfPath);
	 	           if(doc.getEquipmentDocCatalogs()!=null&&doc.getEquipmentDocCatalogs().size()>0){
		            	for(EquipmentDocCatalog catalog:doc.getEquipmentDocCatalogs()){
		            		catalog.setId(IdWorker.getId());
		            		catalog.setEquipmentDocId(doc.getId());
		            		catalog.setCreateTime(date);
		            		catalog.setCreateBy(WebUtil.getCurrentUser());
		            		equipmentDocCatalogs.add(catalog);
		            	}
		            }
	    		}
	        }
			Map<String, Object> map=new HashMap<String, Object>();
			map.put("equipmentDocs", entity.getEquipmentDocs());
			map.put("equipmentDocCatalogs", equipmentDocCatalogs);
			equipmentMapper.inserEquipmentDocs(map);
			if(equipmentDocCatalogs!=null&&equipmentDocCatalogs.size()>0){
				equipmentMapper.insertDocCatalogs(map);
			}
		}
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if(equipmentMapper.updateById(entity)==0){
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
	}
	/*
	 * 删除对象   
	 */
	public void remove(Long id) {
		equipmentMapper.deleteById(id);
		equipmentMapper.deleteEquipmentDocCatalog(id);
		equipmentMapper.deleteDocByEquipId(id);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public EquipmentVo qryById(Long id) {
		EquipmentVo selectById = equipmentMapper.qryById(id);
		if(selectById.getLon()!=null&&selectById.getLat()!=null) {
		selectById.setLonLat((selectById.getLon()!=null?selectById.getLon()+",":"")
				+(selectById.getLat()!=null?selectById.getLat():""));
		}
		return selectById;
	
	}

	public void saveExcelData(List<Equipment> entityList) {
		if (entityList != null && entityList.size() > 0) {
			List<Equipment> interimList = new ArrayList<Equipment>();
			int j = (int) Math.ceil(entityList.size() / (double) 50);
			int num = 0;
			for (int i = 1; i <= j; i++) {
				if (i*50>=entityList.size()) {
					interimList = new ArrayList<Equipment>(entityList.subList((i - 1) * 50, entityList.size()));
					num =num+this.batchInsert(interimList);
				}else {
					interimList = new ArrayList<Equipment>(entityList.subList((i - 1) * 50, i * 50));
					num =num+this.batchInsert(interimList);
				}
			}
			if (num != entityList.size()) {
				throw new BusinessException("导入数据异常!");
			}
		}
	}
	
	private int batchInsert(List<Equipment> interimList) {
		return equipmentMapper.batchInsert(interimList);
	}
	
	public void saveExcelTempData(List<EquipmentTemp> tempList) {
		if (tempList != null && tempList.size() > 0) {
			List<EquipmentTemp> interimList = new ArrayList<EquipmentTemp>();
			int j = (int) Math.ceil(tempList.size() / (double) 50);
			int num = 0;
			for (int i = 1; i <= j; i++) {
				if (i*50>=tempList.size()) {
					interimList = new ArrayList<EquipmentTemp>(tempList.subList((i - 1) * 50, tempList.size()));
					num =num+this.batchInsertTemp(interimList);
				}else {
					interimList = new ArrayList<EquipmentTemp>(tempList.subList((i - 1) * 50, i * 50));
					num =num+this.batchInsertTemp(interimList);
				}
			}
			if (num != tempList.size()) {
				throw new BusinessException("导入数据异常!");
			}
		}
	}
	
	private int batchInsertTemp(List<EquipmentTemp> interimList) {
		return equipmentMapper.batchInsertTemp(interimList);
	}

	public List<EquipmentTemp> qryExcelTemp(Long timestamp) {
		return equipmentMapper.qryExcelTemp(timestamp);
	}

	public void delExcelTemp(Long timestamp) {
		equipmentMapper.delExcelTemp(timestamp);
	}

	
	

}
