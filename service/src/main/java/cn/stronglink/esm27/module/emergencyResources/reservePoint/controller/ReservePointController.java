package cn.stronglink.esm27.module.emergencyResources.reservePoint.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.ReservePoint;
import cn.stronglink.esm27.entity.User;
import cn.stronglink.esm27.module.emergencyResources.reservePoint.service.ReservePointService;
import cn.stronglink.esm27.module.emergencyResources.reservePoint.vo.ReservePointVo;
import cn.stronglink.esm27.module.system.user.service.UserService;

@Controller
@RequestMapping("reservePoint")
public class ReservePointController extends AbstractController{
	
	@Autowired
	private ReservePointService reservePointService;
	@Autowired
	private UserService userService;
	
	/**
	 * 查询列表
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		if(request.getSession().getAttribute("admin")!=null){
			boolean isAdmin = (boolean) request.getSession().getAttribute("admin");
			if(!isAdmin) {
//				if(request.getSession().getAttribute("userBrigade")!=null){
//					List<Long> fireBrigadeIds = (List<Long>) request.getSession().getAttribute("userBrigade");
//					params.put("fireBrigadeIds",fireBrigadeIds);
//				}
				User u =  (User) request.getSession().getAttribute("currentUser");
				if(u!=null) {
					Long brigadeId =userService.selectUserBrigade(u.getId());
					List<Long> fireBrigadeIds = userService.selectUserBrigadeChild(brigadeId);	
					params.put("fireBrigadeIds",fireBrigadeIds);
				}
			}
		}
		Page<ReservePointVo> page = (Page<ReservePointVo>) super.getPage(params);
		Page<ReservePointVo> data = reservePointService.qryListByParams(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 根据重点单位查询物资储备点
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryReserveByType")
	public ResponseEntity<ModelMap> qryReserveByType(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		List<ReservePoint> data = reservePointService.qryReserveByType(params);
		return setSuccessModelMap(modelMap, data);
	}
	/**
	 * 查询重点单位列表不带分页
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryReservePointList")
	public ResponseEntity<ModelMap> qryReservePointList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) { 
		List<ReservePoint> data = reservePointService.qryReservePointList();
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 通过id查询信息
	 */
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> qryUserById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Map<String, Object> params) {
		ReservePointVo vo = reservePointService.selectById(params);
		return setSuccessModelMap(modelMap, vo);
	}
	/**
	 * 新建信息
	 */
	@RequestMapping(value = "create")
	@OperateLog(module = "物资储备点管理",desc="添加物资储备点", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody ReservePoint entity) {
		reservePointService.insert(entity);
		return setSuccessModelMap(modelMap, null);
	}
	/**
	 * 修改信息
	 */
	@RequestMapping(value = "edit")
	@OperateLog(module = "物资储备点管理",desc="修改物资储备点", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody ReservePoint entity) {
		reservePointService.update(entity);
		return setSuccessModelMap(modelMap, null);
	}
	/**
	 * 删除信息
	 */
	@RequestMapping(value = "remove")
	@OperateLog(module = "物资储备点管理",desc="删除物资储备点", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		reservePointService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}
	

}
