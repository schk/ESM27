package cn.stronglink.esm27.module.unit.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.KeyUnit;
import cn.stronglink.esm27.entity.User;
import cn.stronglink.esm27.module.system.user.service.UserService;
import cn.stronglink.esm27.module.unit.service.KeyUnitService;
import cn.stronglink.esm27.module.unit.vo.KeyPartsVo;
import cn.stronglink.esm27.module.unit.vo.KeyUnitDangersVo;

@Controller
@RequestMapping(value = "unit")
public class KeyUnitController extends AbstractController  {
	
	@Autowired
	private KeyUnitService keyUnitService;
	@Autowired
	private UserService userService;
	/**
	 * 查询重点单位列表
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "qryList")
	public ResponseEntity<ModelMap> qryList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		if(request.getSession().getAttribute("admin")!=null){
			boolean isAdmin = (boolean) request.getSession().getAttribute("admin");
			if(!isAdmin) {
//				if(request.getSession().getAttribute("userBrigade")!=null){
//					List<Long> fireBrigadeIds = (List<Long>) request.getSession().getAttribute("userBrigade");
//					params.put("fireBrigadeIds",fireBrigadeIds);
//				}
				User u =  (User) request.getSession().getAttribute("currentUser");
				if(u!=null) {
					Long brigadeId =userService.selectUserBrigade(u.getId());
					List<Long> fireBrigadeIds = userService.selectUserBrigadeChild(brigadeId);	
					params.put("fireBrigadeIds",fireBrigadeIds);
				}
			}
		}
		Page<KeyUnit> page = (Page<KeyUnit>) super.getPage(params);
		Page<KeyUnit> data = keyUnitService.getListByParams(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询重点单位
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "qryKeyUnitList")
	public ResponseEntity<ModelMap> qryKeyUnitList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		Map<String, Object> params = new HashMap<String, Object>();
		if(request.getSession().getAttribute("admin")!=null) {
			boolean isAdmin = (boolean) request.getSession().getAttribute("admin");
			if(!isAdmin) {
//				if(request.getSession().getAttribute("userBrigade")!=null){
//					List<Long> fireBrigadeIds = (List<Long>) request.getSession().getAttribute("userBrigade");
//					params.put("fireBrigadeIds",fireBrigadeIds);
//				}
				User u =  (User) request.getSession().getAttribute("currentUser");
				if(u!=null) {
					Long brigadeId =userService.selectUserBrigade(u.getId());
					List<Long> fireBrigadeIds = userService.selectUserBrigadeChild(brigadeId);	
					params.put("fireBrigadeIds",fireBrigadeIds);
				}
			}
			List<KeyUnit> data = keyUnitService.qryKeyUnitList(params);
			return setSuccessModelMap(modelMap, data);
		}
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 查询某个对象
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> getkeyUnitName(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		KeyUnit entity = keyUnitService.qryById(id);
		return setSuccessModelMap(modelMap, entity);
	}
	
	/**
	 * 查询重点单位下的事故信息
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "qryAccidentByUnitId")
	public ResponseEntity<ModelMap> qryAccidentByUnitId(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		KeyUnit data =keyUnitService.qryAccidentByUnitId(id);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 重点单位的查看按钮
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "getkeyUnitInfo")
	public ResponseEntity<ModelMap> getkeyUnitInfo(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		KeyUnit entity = keyUnitService.getkeyUnitInfo(id);
		return setSuccessModelMap(modelMap, entity);
	}
	
	
	@RequestMapping(value = "getkeyUnitName")
	public ResponseEntity<ModelMap> qryRoleById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		KeyUnit entity = keyUnitService.getkeyUnitName(id);
		return setSuccessModelMap(modelMap, entity);
	}
		
	@RequestMapping(value = "create")
	@OperateLog(module = "重点单位",desc="添加重点单位", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody KeyUnit entity) {
		// 查询当前单位名称是否存在
		Integer count = keyUnitService.getCountByUsername(entity);
		if(count > 0){
			throw new BusinessException("单位名称已存在");
		}
		keyUnitService.insert(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	@RequestMapping(value = "edit")
	@OperateLog(module = "重点单位",desc="修改重点单位", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody KeyUnit entity) {
		// 查询当前单位名称是否存在
		Integer count = keyUnitService.getCountByUsername(entity);
		if(count > 0){
			throw new BusinessException("单位名称已存在");
		}
		keyUnitService.update(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	
	@RequestMapping(value = "editLocation")
	@OperateLog(module = "重点单位",desc="修改重点单位坐标点", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> editLocation(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody KeyUnit entity) {
		keyUnitService.updateLocation(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	@RequestMapping(value = "remove")
	@OperateLog(module = "重点单位",desc="删除重点单位", type = OpType.DEL)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		keyUnitService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 增加重点单位的事故信息
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param entity
	 * @return
	 */
	@RequestMapping(value = "addKeyUnitAccident")
	@OperateLog(module = "重点单位",desc="增加重点单位的事故信息", type = OpType.ADD)
	public ResponseEntity<ModelMap> addKeyUnitAccident(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody KeyUnit entity) {
		keyUnitService.addKeyUnitAccident(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 更新重点部门危化品
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "saveDangersOfKeyUnit")
	@OperateLog(module = "重点单位",desc="更新重点单位危化品", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> saveDangersOfKeyUnit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody KeyUnitDangersVo keyUnit) {
		keyUnitService.saveDangersOfKeyUnit(keyUnit);
		return setSuccessModelMap(modelMap, null);
	}
	
	@RequestMapping(value = "delDangers")
	@OperateLog(module = "重点单位",desc="删除重点单位危化品", type = OpType.DEL)
	public ResponseEntity<ModelMap> delDangers(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		keyUnitService.delDangers(id);
		return setSuccessModelMap(modelMap, null);
	}
	
}
