package cn.stronglink.esm27.module.emergencyResources.reservePoint.vo;

import cn.stronglink.esm27.entity.ReservePoint;

public class ReservePointVo extends ReservePoint {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4199352313623378525L;

	private String keyUnitName;
	private String distances;
	private int materialCount;

	public String getKeyUnitName() {
		return keyUnitName;
	}

	public void setKeyUnitName(String keyUnitName) {
		this.keyUnitName = keyUnitName;
	}

	public int getMaterialCount() {
		return materialCount;
	}

	public void setMaterialCount(int materialCount) {
		this.materialCount = materialCount;
	}

	public String getDistances() {
		return distances;
	}

	public void setDistances(String distances) {
		this.distances = distances;
	}
	
	
}
