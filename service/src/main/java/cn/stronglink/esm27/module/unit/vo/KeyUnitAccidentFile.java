package cn.stronglink.esm27.module.unit.vo;

public class KeyUnitAccidentFile {

	/**
	 * 重点部位扩展属性
	 */
	private Long keyUnitId;
	private String path;
	private String fileName;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Long getKeyUnitId() {
		return keyUnitId;
	}
	public void setKeyUnitId(Long keyUnitId) {
		this.keyUnitId = keyUnitId;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	

	
	
}
