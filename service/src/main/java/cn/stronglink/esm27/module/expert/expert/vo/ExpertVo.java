package cn.stronglink.esm27.module.expert.expert.vo;

import cn.stronglink.esm27.entity.Expert;

public class ExpertVo  extends Expert{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4341773777606843144L;
	
	private String  expertTypeName;

	public String getExpertTypeName() {
		return expertTypeName;
	}

	public void setExpertTypeName(String expertTypeName) {
		this.expertTypeName = expertTypeName;
	}

	@Override
	public String toString() {
		return "ExpertVo [expertTypeName=" + expertTypeName + "]";
	}
	
	

}
