package cn.stronglink.esm27.module.accident.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.core.util.AntdFile;
import cn.stronglink.esm27.entity.AccidentCase;
import cn.stronglink.esm27.module.accident.vo.FireCaseVo;

public interface AccidentCaseMapper extends BaseMapper<AccidentCase> {

	List<AccidentCase> qryListByParams(Pagination page, Map<String, Object> params);

	AccidentCase qryById(Long id);

	void deleteFileByCaseId(Long accidentCaseId);

	void inserAccidentCaseFiles(Map<String, Object> map);

	List<AntdFile> selectFileById(Long accidentCaseId);

	List<FireCaseVo> qryFireCaseListByParams(Pagination page, Map<String, Object> params);
	
}
