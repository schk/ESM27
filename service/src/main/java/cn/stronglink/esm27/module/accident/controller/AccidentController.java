package cn.stronglink.esm27.module.accident.controller;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.esm27.entity.GasType;
import cn.stronglink.esm27.entity.IncidentRecord;
import cn.stronglink.esm27.entity.Room;
import cn.stronglink.esm27.module.fireEngine.service.FireEngineUseRecordService;
import cn.stronglink.esm27.web.realTimeData.service.RealTimeDataService;
import cn.stronglink.esm27.web.realTimeData.vo.HistoryGasDataVo;
import cn.stronglink.esm27.web.realTimeData.vo.HistoryQryParams;
import cn.stronglink.esm27.web.realTimeData.vo.WeatherDataVo;
import cn.stronglink.esm27.web.webData.service.AccidentService;

@Controller
@RequestMapping(value = "accident")
public class AccidentController extends AbstractController  {
	
	@Autowired
	private AccidentService accidentService;
	@Autowired
	private FireEngineUseRecordService fUrService;
	@Autowired
	private RealTimeDataService  realTimeDataService;
	
	/**
	 * 跳转后台的报告界面
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("report")
    public String report(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		String accidentId = request.getParameter("accidentId");
		Map<String,Object> map=fUrService.getReportContent(accidentId);
	    model.addAttribute("reportContent", map);
		return "/reportManager";
    }
	
	/**
	 * 获取出勤力量
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "getCZLLData")
	public ResponseEntity<ModelMap> getCZLLData(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long roomId) {		
		Map<String,Object> data = fUrService.getCZLLData(roomId);
		System.out.println(data);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 跳转历史记录页面
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("accidentHistory")
    public String accidentHistory(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		String accidentId = request.getParameter("accidentId");
	    model.addAttribute("accidentId", accidentId);
		return "/accidentHistory";
    }
	
	/**
	 * 查询所有房间信息(带分页)
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping("qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<Room> page = (Page<Room>) super.getPage(params);
		Page<Room> data = accidentService.qryListByParams(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	
	/**
	 * 查询某个房间得记录(带分页，只查询命令和任务得记录)
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("getEditRecord")
	public ResponseEntity<ModelMap> getEditRecord(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		Page<IncidentRecord> page = (Page<IncidentRecord>) super.getPage(params);
		Page<IncidentRecord> data = accidentService.getEditRecord(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	
	/**
	 * 删除操作记录
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param recordId
	 * @return
	 */
	@RequestMapping("delRecord")
	public ResponseEntity<ModelMap> delRecord(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long recordId) {
		accidentService.delRecord(recordId);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 删除操作记录
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param recordId
	 * @return
	 */
	@RequestMapping("editRecord")
	public ResponseEntity<ModelMap> editRecord(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody IncidentRecord record) {
		accidentService.editRecord(record);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 删除操作记录
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param recordId
	 * @return
	 */
	@RequestMapping("getEditRecordById")
	public ResponseEntity<ModelMap> getEditRecordById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long recordId) {
		IncidentRecord data =accidentService.getEditRecordById(recordId);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询历史气象数据
	 * */
	@RequestMapping("queryHistoryWeatherDataByRoom")
	public ResponseEntity<ModelMap> queryHistoryWeatherDataByRoom(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody HistoryQryParams params){
		
		Map<String, Object> pageParams = new HashMap<>();
		pageParams.put("pageNum", params.getPageNum());
		pageParams.put("pageSize", params.getPageSize());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if(params.getCreateTime()!=null && params.getCreateTime().length==2) {
			params.setStartTime(sdf.format(params.getCreateTime()[0]));
			params.setEndTime(sdf.format(params.getCreateTime()[1]));
		}
		@SuppressWarnings("unchecked")
		Page<WeatherDataVo> page = (Page<WeatherDataVo>) super.getPage(pageParams);
		Page<WeatherDataVo> data = realTimeDataService.queryHistoryWeatherData(page,params);
		//List<WeatherDataVo> data = realTimeDataService.queryHistoryWeatherData(params);
		return setSuccessModelMap(modelMap,data);
	}
	
	/**
	 * 查询历史气体数据标题
	 * */
	@RequestMapping("getHistoryGasDataTitle")
	public ResponseEntity<ModelMap> getHistoryGasDataTitle(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody HistoryQryParams params){
		List<GasType> data = accidentService.getHistoryGasDataTitle();
		return setSuccessModelMap(modelMap,data);
	}
	
	/**
	 * 查询历史气体数据
	 * */
	@RequestMapping("queryHistoryGasDataByRoom")
	public ResponseEntity<ModelMap> queryHistoryGasDataByRoom(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody HistoryQryParams params){
		Map<String, Object> pageParams = new HashMap<>();
		pageParams.put("pageNum", params.getPageNum());
		pageParams.put("pageSize", params.getPageSize());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if(params.getCreateTime()!=null && params.getCreateTime().length==2) {
			params.setStartTime(sdf.format(params.getCreateTime()[0]));
			params.setEndTime(sdf.format(params.getCreateTime()[1]));
		}
		@SuppressWarnings("unchecked")
		Page<Map> page = (Page<Map>) super.getPage(pageParams);
		Page<Map> data = accidentService.queryHistoryGasData(page,params);
		//List<HistoryGasDataVo> data = realTimeDataService.queryHistoryGasData(params);
		return setSuccessModelMap(modelMap,data);
	}

	
}
