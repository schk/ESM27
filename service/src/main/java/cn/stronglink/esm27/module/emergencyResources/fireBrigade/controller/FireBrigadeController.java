package cn.stronglink.esm27.module.emergencyResources.fireBrigade.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.FireBrigade;
import cn.stronglink.esm27.entity.KeyUnit;
import cn.stronglink.esm27.entity.User;
import cn.stronglink.esm27.module.emergencyResources.fireBrigade.service.FireBrigadeService;
import cn.stronglink.esm27.module.emergencyResources.fireBrigade.vo.FireBrigadeTreeNodeVo;
import cn.stronglink.esm27.module.fireEngine.service.FireEngineService;
import cn.stronglink.esm27.module.fireEngine.vo.FireEngineVo;
import cn.stronglink.esm27.module.system.user.service.UserService;
import cn.stronglink.esm27.module.unit.service.KeyUnitService;


@Controller
@RequestMapping(value = "fireBrigade")
public class FireBrigadeController extends AbstractController  {
	
	@Autowired
	private FireBrigadeService fireBrigadeService;
	@Autowired
	private KeyUnitService keyUnitService;
	@Autowired
	private FireEngineService fireEngineService;
	
	@Autowired
	private UserService userService;
	/**
	 * 查询列表分页
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		if(request.getSession().getAttribute("admin")!=null){
			boolean isAdmin = (boolean) request.getSession().getAttribute("admin");
			if(!isAdmin) {
//				if(request.getSession().getAttribute("userBrigade")!=null){
//					List<Long> fireBrigadeIds = (List<Long>) request.getSession().getAttribute("userBrigade");
//					params.put("fireBrigadeIds",fireBrigadeIds);
//				}
				User u =  (User) request.getSession().getAttribute("currentUser");
				if(u!=null) {
					Long brigadeId =userService.selectUserBrigade(u.getId());
					List<Long> fireBrigadeIds = userService.selectUserBrigadeChild(brigadeId);	
					params.put("fireBrigadeIds",fireBrigadeIds);
				}
				
			}
		}
		Page<FireBrigade> page = (Page<FireBrigade>) super.getPage(params);
		Page<FireBrigade> data = fireBrigadeService.qryListByParams(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询列表
	 */
	@RequestMapping(value = "qryList")
	public ResponseEntity<ModelMap> qryList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<FireBrigade> data = fireBrigadeService.qryList();
		return setSuccessModelMap(modelMap, data);
	}
	

	/**
	 * 查询带最顶部的消防战队
	 * @param modelMap
	 * @param request
	 * @param response
	 * @return
	 */
/*	@RequestMapping(value = "qryFiBrigade")
	public ResponseEntity<ModelMap> qryFiBrigade(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		Object attribute = request.getSession().getAttribute("userBrigade");
		List<FireBrigadeTreeNodeVo> vo = fireBrigadeService.getFiBrigadeTree(attribute);
		return setSuccessModelMap(modelMap, vo);
	}*/
	
	/**
	 * 查询不带顶部的消防队
	 * @param modelMap
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "qryFireBrigadeTree")
	public ResponseEntity<ModelMap> qryFireBrigadeTree(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<FireBrigadeTreeNodeVo> vo = fireBrigadeService.qryFireBrigadeTree(request);
		return setSuccessModelMap(modelMap, vo);
	}
	
	
	/**
	 * 通过id查询应急战队
	 */
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> qryById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		FireBrigade vo = fireBrigadeService.qryById(id);
		return setSuccessModelMap(modelMap, vo);
	}
		
	/**
	 * 通过id查询应急战队
	 */
	@RequestMapping(value = "getInfoDetail")
	public ResponseEntity<ModelMap> getInfoDetail(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		FireBrigade vo = fireBrigadeService.getInfoDetail(id);
		return setSuccessModelMap(modelMap, vo);
	}
	
	
	/**
	 * 新建人员
	 */
	@RequestMapping(value = "create")
	@OperateLog(module = "应急战队管理",desc="添加应急战队", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody FireBrigade entity) {
		// 查询当前消防队名称是否存在
		Integer count = fireBrigadeService.getCountByUsername(entity);
		if(count > 0){
			throw new BusinessException("消防队名称已存在");
		}
		fireBrigadeService.insert(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 修改应急战队
	 */
	@RequestMapping(value = "edit")
	@OperateLog(module = "应急战队管理",desc="修改应急战队", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody FireBrigade entity) {
		// 查询当前消防队名称是否存在
		Integer count = fireBrigadeService.getCountByUsername(entity);
		if(count > 0){
			throw new BusinessException("消防队名称已存在");
		}
		fireBrigadeService.update(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 删除应急战队
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "remove")
	@OperateLog(module = "应急战队管理",desc="删除应急战队", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Long> fireBrigadeList = new ArrayList<>();
		fireBrigadeList.add(id);
		params.put("fireBrigadeIds", fireBrigadeList);
		List<KeyUnit> keyUnitList = keyUnitService.qryKeyUnitList(params);
		if(keyUnitList!=null&&keyUnitList.size()>0) {
			throw new BusinessException("该消防队下存在重点单位，不能删除！");
		}
		FireEngineVo  fireEngineVo = fireEngineService.qryCarCountByBrigade(id);
		if(fireEngineVo!=null && fireEngineVo.getCarCount()>0) {
			throw new BusinessException("该消防队下存在消防车，不能删除！");
		}
		fireBrigadeService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}
	
	
}
