package cn.stronglink.esm27.module.collect.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.esm27.module.collect.service.CollectService;

@Controller
@RequestMapping(value = "webApi/collect")
public class CollectController extends AbstractController {

	@Autowired
	private CollectService collectService;
	
	@Autowired  
	private Environment env;  
	
	/**
	 * 启动本地采集
	 * @param modelMap
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "localStart")
	public ResponseEntity<ModelMap> localStart(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params)  {
		Boolean isLocal = Boolean.valueOf(env.getProperty("collect.local").toString());
		String closeType= params.get("closeType").toString();
		int code =collectService.startCollect(isLocal,closeType);
		return setSuccessModelMap(modelMap,code);
	}
	
	
	
}
