package cn.stronglink.esm27.module.emergencyResources.emergencySuppliesType.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.EmergencyMaterialType;

public interface EmergencySuppliesTypeMapper extends BaseMapper<EmergencyMaterialType> {
	
	//分页查询
	List<EmergencyMaterialType> qryListByParams(Map<String, Object> params);

}
