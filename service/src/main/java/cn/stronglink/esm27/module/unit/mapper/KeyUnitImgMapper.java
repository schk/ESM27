package cn.stronglink.esm27.module.unit.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.core.util.UploadEntity;
import cn.stronglink.esm27.entity.KeyUnitImg;

public interface KeyUnitImgMapper extends BaseMapper<KeyUnitImg>{

	List<UploadEntity> getImgList(Long id);


	int batchInsertObj(@Param("interimList") List<KeyUnitImg> list);

}
