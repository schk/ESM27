package cn.stronglink.esm27.module.conventionalEquip.specialDutyEquip.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.esm27.entity.SpecialDutyEquip;
import cn.stronglink.esm27.entity.SpecialDutyEquipTemp;
import cn.stronglink.esm27.module.conventionalEquip.specialDutyEquip.vo.SpecialDutyEquipVo;
import cn.stronglink.esm27.module.conventionalEquip.specialDutyEquip.vo.SpecialDutyEquipWeb;

public interface SpecialDutyEquipMapper extends BaseMapper<SpecialDutyEquip> {

	
	List<SpecialDutyEquipVo> qryListByParams(Pagination page, Map<String, Object> params);
	//根据id关联查询
	SpecialDutyEquipVo getDeptNameById(@Param("id") Long id);
	
	//根据消防车ID查询随车器材信息
	List<SpecialDutyEquipVo>  qryListByEngineId(@Param("fireEngineId") Long fireEngineId);

	List<SpecialDutyEquip> getObject(Pagination page, Map<String, Object> params);
	
	List<Map<String, Object>> getSpecialDutyEquipType();
	
	List<SpecialDutyEquipVo> selectAll(Pagination page);
	//根据类型查询
	List<SpecialDutyEquipVo> selectOneType(Page<SpecialDutyEquipVo> page,Map<String, String> params);
	//清空使用数据
	void updateTempCount(@Param("list") List list);
	
	int batchInsertObjTemp(@Param("interimList") List<SpecialDutyEquipTemp> list);
	
	int batchInsertObj(@Param("interimList") List<SpecialDutyEquip> list);
	
	List<SpecialDutyEquipTemp> qryObjTemp(Long timestamp);

	void delObjTemp(Long timestamp);
	
	List<SpecialDutyEquipWeb> selectEquipByParams();
	int qryEquipCount(Long id);
}	
