package cn.stronglink.esm27.module.socialResource.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.UploadEntity;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.KeyUnitImg;
import cn.stronglink.esm27.entity.SocialResource;
import cn.stronglink.esm27.module.socialResource.mapper.SocialResourceMapper;
import cn.stronglink.esm27.module.unit.mapper.KeyUnitImgMapper;

@Service
@Transactional(rollbackFor = Exception.class)
public class SocialResourceService {
	@Autowired
	private SocialResourceMapper socialResourceMapper;
	@Autowired
	private KeyUnitImgMapper keyUnitImgMapper;

	public Page<SocialResource> getListByParams(Page<SocialResource> page, Map<String, Object> params) {
		List<SocialResource> list = socialResourceMapper.getListByParams(page, params);
		page.setRecords(list);
		return page;
	}

	public int getCountByName(SocialResource entity) {
		return socialResourceMapper.getCountByName(entity);
	}

	public void update(SocialResource entity) {
		if (!"".equals(entity.getLonLat())) {
			String[] lonLat = entity.getLonLat().split(",");
			entity.setLon(Double.valueOf(lonLat[0]));
			entity.setLat(Double.valueOf(lonLat[1]));
		}
		HashMap<String, Object> columnMap = new HashMap<String, Object>();
		columnMap.put("key_unit_id", entity.getId());
		keyUnitImgMapper.deleteByMap(columnMap);
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if (socialResourceMapper.updateById(entity) == 0) {
			throw new BusinessException("更新失败!");
		}
		if (entity.getImgList().size() > 0) {
			for (String img : entity.getImgList()) {
				KeyUnitImg imgPath = new KeyUnitImg();
				imgPath.setId(IdWorker.getId());
				imgPath.setKeyUnitId(entity.getId());
				imgPath.setPath(img);
				imgPath.setCreateBy(WebUtil.getCurrentUser());
				imgPath.setCreateTime(new Date());
				imgPath.setUpdateTime(new Date());
				imgPath.setUpdateBy(WebUtil.getCurrentUser());
				keyUnitImgMapper.insert(imgPath);
			}
		}
	}

	public void insert(SocialResource entity) {
		if (!"".equals(entity.getLonLat())) {
			String[] lonLat = entity.getLonLat().split(",");
			entity.setLon(Double.valueOf(lonLat[0]));
			entity.setLat(Double.valueOf(lonLat[1]));
		}
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		socialResourceMapper.insert(entity);
		if (entity.getImgList().size() > 0) {
			for (String img : entity.getImgList()) {
				KeyUnitImg imgPath = new KeyUnitImg();
				imgPath.setId(IdWorker.getId());
				imgPath.setKeyUnitId(entity.getId());
				imgPath.setPath(img);
				imgPath.setCreateBy(WebUtil.getCurrentUser());
				imgPath.setCreateTime(new Date());
				imgPath.setUpdateTime(new Date());
				imgPath.setUpdateBy(WebUtil.getCurrentUser());
				keyUnitImgMapper.insert(imgPath);
			}
		}
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public SocialResource qryById(Long id) {
		SocialResource socialResource = socialResourceMapper.qryById(id);
		List<UploadEntity> imgList = keyUnitImgMapper.getImgList(id);
		socialResource.setFileList(imgList);
		return socialResource;
	}

	public int remove(Long id) {
		HashMap<String, Object> columnMap = new HashMap<String, Object>();
		columnMap.put("key_unit_id", id);
		keyUnitImgMapper.deleteByMap(columnMap);
		return socialResourceMapper.deleteById(id);
	}

	public void batchInsert(List<SocialResource> objList) {
		socialResourceMapper.batchInsert(objList);
	}

}
