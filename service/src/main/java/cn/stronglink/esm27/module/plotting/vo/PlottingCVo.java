package cn.stronglink.esm27.module.plotting.vo;

import cn.stronglink.esm27.entity.Plotting;

public class PlottingCVo extends Plotting{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2847764895268467091L;

	private String dicName;

	public String getDicName() {
		return dicName;
	}

	public void setDicName(String dicName) {
		this.dicName = dicName;
	}

	
}
