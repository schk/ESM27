package cn.stronglink.esm27.module.equipment.equipmentType.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.EquipmentType;
import cn.stronglink.esm27.module.equipment.equipmentType.mapper.EquipmentTypeMapper;

@Service
@Transactional(rollbackFor = Exception.class)
public class EquipmentTypeService {

	@Autowired
	private EquipmentTypeMapper equipmentTypeMapper;

	public Page<EquipmentType> getListByParams(Page<EquipmentType> page, Map<String, Object> params) {
		page.setRecords(equipmentTypeMapper.getListByParams(page, params));
		return page;
	}

	public void insert(EquipmentType entity) {
		Wrapper<EquipmentType> wrapper = new EntityWrapper<>();
		wrapper.eq("code_", entity.getCode());
		int count = equipmentTypeMapper.selectCount(wrapper);
		if (count > 0) {
			throw new BusinessException("类型编码已存在！");
		}
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		equipmentTypeMapper.insert(entity);
	}

	public void update(EquipmentType entity) {
		Wrapper<EquipmentType> wrapper = new EntityWrapper<>();
		wrapper.eq("code_", entity.getCode());
		wrapper.ne("id_", entity.getId());
		int count = equipmentTypeMapper.selectCount(wrapper);
		if (count > 0) {
			throw new BusinessException("类型编码已存在！");
		}
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		equipmentTypeMapper.updateById(entity);
	}

	public void remove(Long id) {
		equipmentTypeMapper.deleteById(id);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public EquipmentType selectById(Long id) {
		return equipmentTypeMapper.selectById(id);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<EquipmentType> qryEquipmentTypeList(int type) {
		return equipmentTypeMapper.qryEquipmentTypeList(type);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<EquipmentType> qryEquTypeList(List<String> types) {
		return equipmentTypeMapper.qryEquTypeList(types);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Long qryEquipmentTypeIdByCode(String code) {
		return equipmentTypeMapper.qryEquipmentTypeIdByCode(code);
	}

}
