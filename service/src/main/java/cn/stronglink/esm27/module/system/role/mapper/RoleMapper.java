package cn.stronglink.esm27.module.system.role.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.esm27.entity.Role;
import cn.stronglink.esm27.module.system.role.vo.RoleVo;

public interface RoleMapper extends BaseMapper<Role>{

	public List<Role> qryRoles(@Param("params") Map<String, Object> params);
	
	List<RoleVo> getRoleByParams(Pagination page, Role role);

	Integer getRoleUsers(@Param("id") Long id);

	int getRoleByName(@Param("name") String name);

	public List<Role> qryRoleOption();

	public Integer getCountByUsername(Role entity);

}
