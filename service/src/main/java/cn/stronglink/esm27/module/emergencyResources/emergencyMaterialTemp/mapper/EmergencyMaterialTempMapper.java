package cn.stronglink.esm27.module.emergencyResources.emergencyMaterialTemp.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.esm27.entity.EmergencyMaterialTemp;
import cn.stronglink.esm27.module.emergencyResources.emergencyMaterialTemp.vo.EmergencyMaterialTempVo;


public interface EmergencyMaterialTempMapper extends BaseMapper<EmergencyMaterialTemp> {
	
	
}
