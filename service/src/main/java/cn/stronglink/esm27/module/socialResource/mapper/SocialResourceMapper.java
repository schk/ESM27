package cn.stronglink.esm27.module.socialResource.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.esm27.entity.SocialResource;

public interface SocialResourceMapper extends BaseMapper<SocialResource> {

	List<SocialResource> getListByParams(Page<SocialResource> page, Map<String, Object> params);

	int getCountByName(SocialResource entity);

	SocialResource qryById(Long id);

	int batchInsert(@Param("interimList")List<SocialResource> objList);

}
