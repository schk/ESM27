package cn.stronglink.esm27.module.expert.expertType.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.ExpertType;
import cn.stronglink.esm27.module.expert.expertType.service.ExpertTypeService;

@Controller
@RequestMapping(value = "expertType")
public class ExpertTypeController extends AbstractController {
	
	@Autowired
	private ExpertTypeService expertTypeService;
	
	/**
	 * 查询应急专家类型
	 * @param modelMap
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("qryListByParams")
	public ResponseEntity<ModelMap> qryList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response){
		List<ExpertType> data = expertTypeService.qryListByParams();
		return setSuccessModelMap(modelMap, data);
		
	}
	
	/**
	 * 查询对象
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> qryUserById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		ExpertType vo = expertTypeService.qryById(id);
		return setSuccessModelMap(modelMap, vo);
	}
	
	
	/**
	 * 新建应急专家类型
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param entity
	 * @return
	 */
	@RequestMapping(value = "create")
	@OperateLog(module = "应急专家分类管理",desc="添加应急专家分类", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody ExpertType entity) {
		expertTypeService.insert(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 修改应急专家类型
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param entity
	 * @return
	 */
	@RequestMapping(value = "edit")
	@OperateLog(module = "应急专家分类管理",desc="修改应急专家分类", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody ExpertType entity) {
		expertTypeService.update(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 删除应急专家类型
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "remove")
	@OperateLog(module = "应急专家分类管理",desc="删除应急专家分类", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		expertTypeService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}

}
