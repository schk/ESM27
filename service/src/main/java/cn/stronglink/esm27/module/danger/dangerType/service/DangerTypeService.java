package cn.stronglink.esm27.module.danger.dangerType.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.DangerType;
import cn.stronglink.esm27.module.danger.dangerType.mapper.DangerTypeMapper;
import cn.stronglink.esm27.module.danger.dangerType.vo.DangerTypeTreeNodeVo;

@Service
@Transactional(rollbackFor=Exception.class) 
public class DangerTypeService {

	@Autowired
	private DangerTypeMapper dangerTypeMapper;

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<DangerTypeTreeNodeVo> getTree() {
		List<DangerTypeTreeNodeVo> rootDept = dangerTypeMapper.qryRootDept();	
		List<DangerTypeTreeNodeVo> list = dangerTypeMapper.qryDepts();
		if(rootDept!=null&&rootDept.size()>0){
			for(DangerTypeTreeNodeVo vo:rootDept){
				recursion(list,vo);
			}
		}
        return rootDept;  
	}
	

	public void insert(DangerType entity){
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		dangerTypeMapper.insert(entity);	
	}
	
	public void update(DangerType entity) {
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		dangerTypeMapper.updateAllColumnById(entity);
	}
	
	public void remove(Long id){
		
		dangerTypeMapper.deleteById(id);	
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public int getChildDetpCount(Long id) {
		return dangerTypeMapper.getChildDetpCount(id);
	}  

	private void recursion(List<DangerTypeTreeNodeVo> list, DangerTypeTreeNodeVo node) {
		List<DangerTypeTreeNodeVo> childList = getChildList(list, node);// 得到子节点列表
		if (!CollectionUtils.isEmpty(childList)) {
			node.setChildren(childList);
			Iterator<DangerTypeTreeNodeVo> it = childList.iterator();
			while (it.hasNext()) {
				DangerTypeTreeNodeVo n = (DangerTypeTreeNodeVo) it.next();
				recursion(list, n);
			}
		} else {
			node.setChildren(null);
		}
	}
	
	private List<DangerTypeTreeNodeVo> getChildList(List<DangerTypeTreeNodeVo> list, DangerTypeTreeNodeVo node) {  
        List<DangerTypeTreeNodeVo> nodeList = new ArrayList<DangerTypeTreeNodeVo>();  
        Iterator<DangerTypeTreeNodeVo> it = list.iterator();  
        while (it.hasNext()) {  
        	DangerTypeTreeNodeVo n = (DangerTypeTreeNodeVo) it.next();  
            if (n.getPid().equals(node.getId()) ) {  
                nodeList.add(n);  
            }  
        }  
        return nodeList;  
    }

	public List<DangerTypeTreeNodeVo> qryDangerTypeNoTop() {
		List<DangerTypeTreeNodeVo> rootDept = dangerTypeMapper.qryRootDeptNoTop();	
		List<DangerTypeTreeNodeVo> list = dangerTypeMapper.qryDepts();
		if(rootDept!=null&&rootDept.size()>0){
			for(DangerTypeTreeNodeVo vo:rootDept){
				recursion(list, vo);  
			}
		}
        return rootDept;  
	}

	public DangerType selectById(Long id) {
		return dangerTypeMapper.selectById(id);
	}

	//判断是否有危化品类型数据在使用
	public List<DangerType> getdangerTypeList(Long id) {
		return dangerTypeMapper.getdangerTypeList(id);
	}

	

	

}
