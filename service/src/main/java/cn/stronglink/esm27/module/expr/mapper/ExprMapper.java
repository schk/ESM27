package cn.stronglink.esm27.module.expr.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.esm27.entity.Expr;
import cn.stronglink.esm27.entity.ExprParam;
import cn.stronglink.esm27.entity.ExprParamOption;
import cn.stronglink.esm27.module.expr.vo.ExprVo;

public interface ExprMapper extends BaseMapper<Expr>{	

	public List<Expr> getExprByParams(Page<Expr> page, Expr expr);

	public void deleteParamByExprId(@Param("id") Long id);

	public void createExpr(ExprVo exprVo);

	public void createExprParams(ExprVo exprVo);

	public void updateExpr(ExprVo exprVo);

	public ExprVo qryInfo(@Param("id")Long id);

	public List<ExprParam> getExprParams(@Param("id")Long id);

	public void deleteParamOptionByExprId(Long id);

	public void createExprParamsOption(ExprParam param);

	public List<ExprParamOption> getExprParamsOption(Long id);

	public List<Expr> queryListNoPage(Map<String, Object> params);

}
