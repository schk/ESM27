package cn.stronglink.esm27.module.system.permission.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.stronglink.esm27.module.system.permission.mapper.PermissionMapper;
import cn.stronglink.esm27.module.system.permission.vo.PermissionVo;
import cn.stronglink.esm27.module.system.role.vo.RoleVo;

@Service
@Transactional(rollbackFor=Exception.class)
public class PermissionService {
	
	@Autowired
	private PermissionMapper permissionMapper;

	public List<PermissionVo> selectPermissionList() {
		List<PermissionVo> rootPermission = permissionMapper.getPermissionList(-1l);	
		if(rootPermission!=null&&rootPermission.size()>0){
			for(PermissionVo vo:rootPermission){
				getChildren(vo);
			}
		}
        return rootPermission;  
	}
	
	public void getChildren(PermissionVo vo){
		List<PermissionVo> voList=permissionMapper.getPermissionList(vo.getId());
		if(voList!=null&&voList.size()>0){
			for(PermissionVo child:voList){
				getChildren(child);
			}
			vo.setChildren(voList);
		}
	}

	public List<String> getRolePermission(Long id) {
		List<String> resultList=new ArrayList<String>();
		List<Long> list=permissionMapper.getRolePermission(id);
		if(list!=null&&list.size()>0){
			for(Long child:list){
				resultList.add(child.toString());
			}
		}
		return resultList;
	}

	public void setRolePermission(RoleVo vo) {
		permissionMapper.delRolePermissionById(vo.getId());
		if(vo.getPermission()!=null&&vo.getPermission().size()>0){
			permissionMapper.createRolePermissionById(vo);
		}
	}
	
	public Set<String> getPermsByUserId(Long userId) {
		return permissionMapper.getPermsByUserId(userId);
	}

	public List<String> getRolePermissionNoP(Long id) {
		List<String> resultList=new ArrayList<String>();
		List<Long> list=permissionMapper.getRolePermissionNoP(id);
		if(list!=null&&list.size()>0){
			for(Long child:list){
				resultList.add(child.toString());
			}
		}
		return resultList;
	}
	

}
