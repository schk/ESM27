package cn.stronglink.esm27.module.equipment.equipment.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.esm27.entity.Equipment;
import cn.stronglink.esm27.entity.EquipmentDocCatalog;
import cn.stronglink.esm27.entity.EquipmentTemp;
import cn.stronglink.esm27.module.equipment.equipment.vo.EquipmentDocParam;
import cn.stronglink.esm27.module.equipment.equipment.vo.EquipmentVo;


public interface EquipmentMapper extends BaseMapper<Equipment>{

	
	List<EquipmentVo> qryListByParams(Pagination page, Map<String, Object> params);
	
	//根据id查找设备文档
	public List<EquipmentDocParam> getDocById(@Param("id") Long id);
	
	//根据id删除设备文档信息
	public void deleteDocById(Map<String, Object> params);

	void deleteDocByEquipId(Long id);

	void inserEquipmentDocs(Map<String, Object> map);

	void insertDocCatalogs(Map<String, Object> map);

	void deleteEquipmentDocCatalog(@Param("id")Long id);

	List<EquipmentDocCatalog> getEquipmentDocCatalogs(@Param("equipmentDocId")Long equipmentDocId);

	EquipmentVo qryById(@Param("id") Long id);

	int batchInsert(@Param("interimList") List<Equipment> interimList);

	int batchInsertTemp(@Param("interimList") List<EquipmentTemp> interimList);

	List<EquipmentTemp> qryExcelTemp(Long timestamp);

	void delExcelTemp(Long timestamp);

	List<EquipmentVo> qryList();

}
