package cn.stronglink.esm27.module.dataCount.controller;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.esm27.module.dataCount.param.DataCountParam;
import cn.stronglink.esm27.module.dataCount.service.DataCountService;

@Controller
@RequestMapping("dataCount")
public class DataCountController  extends AbstractController{

	@Autowired
	private DataCountService dataCountService;
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	DecimalFormat   fnum1  =   new   DecimalFormat("00");
	
	
	@RequestMapping(value="qryKeyUnitCount")
	public ResponseEntity<ModelMap>  qryKeyUnitCount(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody DataCountParam param){
		Map<String,Object> data = dataCountService.qryKeyUnitCount(param);
		return setSuccessModelMap(modelMap, data);

	}
	
	
	@RequestMapping(value="qryTeamCount")
	public ResponseEntity<ModelMap>  qryTeamCount(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody DataCountParam param){
		List<Map<String,Object>> data = dataCountService.qryTeamCount(param);
		return setSuccessModelMap(modelMap, data);

	}
	
	@RequestMapping(value="qryAccidentCount")
	public ResponseEntity<ModelMap>  qryAccidentCount(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody DataCountParam param) throws ParseException{
		Date date=new Date();
		List<String> dates=new ArrayList<String>();
		if(param.getDateType()==null||"".equals(param.getDateType())||"month".equals(param.getDateType())){
			Calendar cal=Calendar.getInstance();
			
			if(param.getDateValue()==null||"".equals(param.getDateValue())){
				cal.setTime(date);
			}else{
				cal.setTime(dateFormat.parse(param.getDateValue()+"-01"));
			}
			cal.set(Calendar.DATE, 1);
			param.setStartDate(cal.getTime());
			cal.add(Calendar.MONTH, 1);
			cal.add(Calendar.DATE, -1);
			param.setEndDate(cal.getTime());
			dates.add(dateFormat.format(param.getStartDate()));
			Calendar start=Calendar.getInstance();
			start.setTime(param.getStartDate());
			while(start.getTime().compareTo(param.getEndDate())<0){
				start.add(Calendar.DATE, 1);
				dates.add(dateFormat.format(start.getTime()));
			}
		}else{
			Calendar cal=Calendar.getInstance();
			cal.setTime(date);
			String year=cal.get(Calendar.YEAR)+"";
			if(param.getDateValue()!=null&&!"".equals(param.getDateValue())){
				year=param.getDateValue();
			}
			param.setStartDate(dateFormat.parse(year+"-01-01"));
			param.setEndDate(dateFormat.parse(year+"-12-31"));
			for(int i=1;i<=12;i++){
				dates.add(year+"-"+fnum1.format(fnum1.parse(i+"")));
			}
		}
		param.setDates(dates);
		List<Map<String,Object>> data = dataCountService.qryAccidentCount(param);
		return setSuccessModelMap(modelMap, data);

	}
	@RequestMapping(value="getYearList")
	public ResponseEntity<ModelMap>  getYearList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response){
		int min=1900;
		Date date=new Date();
		Calendar cal=Calendar.getInstance();
		cal.setTime(date);
		int curYear=cal.get(Calendar.YEAR);
		List<Map<String,Object>> mapList=new ArrayList<Map<String,Object>>();
		for(int i=curYear;i>=min;i--){
			Map<String,Object> map=new HashMap<String,Object>();
			map.put("label", i+"年");
			map.put("value", i);
			mapList.add(map);
		}
		return setSuccessModelMap(modelMap, mapList);

	}
	
	@RequestMapping(value="qryPoliceNumber")
	public ResponseEntity<ModelMap>  qryPoliceNumber(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody DataCountParam param) throws ParseException{
		Date date=new Date();
		List<String> dates=new ArrayList<String>();
		if(param.getDateType()==null||"".equals(param.getDateType())||"month".equals(param.getDateType())){
			Calendar cal=Calendar.getInstance();
			
			if(param.getDateValue()==null||"".equals(param.getDateValue())){
				cal.setTime(date);
			}else{
				cal.setTime(dateFormat.parse(param.getDateValue()+"-01"));
			}
			cal.set(Calendar.DATE, 1);
			param.setStartDate(cal.getTime());
			cal.add(Calendar.MONTH, 1);
			cal.add(Calendar.DATE, -1);
			param.setEndDate(cal.getTime());
			dates.add(dateFormat.format(param.getStartDate()));
			Calendar start=Calendar.getInstance();
			start.setTime(param.getStartDate());
			while(start.getTime().compareTo(param.getEndDate())<0){
				start.add(Calendar.DATE, 1);
				dates.add(dateFormat.format(start.getTime()));
			}
		}else{
			Calendar cal=Calendar.getInstance();
			cal.setTime(date);
			String year=cal.get(Calendar.YEAR)+"";
			if(param.getDateValue()!=null&&!"".equals(param.getDateValue())){
				year=param.getDateValue();
			}
			param.setStartDate(dateFormat.parse(year+"-01-01"));
			param.setEndDate(dateFormat.parse(year+"-12-31"));
			for(int i=1;i<=12;i++){
				dates.add(year+"-"+fnum1.format(fnum1.parse(i+"")));
			}
		}
		param.setDates(dates);
		List<Map<String,Object>> data = dataCountService.qryPoliceNumber(param);
		return setSuccessModelMap(modelMap, data);

	}
	
}
