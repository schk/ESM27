package cn.stronglink.esm27.module.emergencyResources.emergencyMaterialTemp.service;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.esm27.entity.EmergencyMaterialTemp;
import cn.stronglink.esm27.module.emergencyResources.emergencyMaterialTemp.mapper.EmergencyMaterialTempMapper;
import cn.stronglink.esm27.module.emergencyResources.emergencyMaterialTemp.vo.EmergencyMaterialTempVo;

@Service
@Transactional(rollbackFor=Exception.class)
public class EmergencyMaterialTempService {
	
	@Autowired
	private EmergencyMaterialTempMapper emergencyMaterialTempMapper;
	
}
