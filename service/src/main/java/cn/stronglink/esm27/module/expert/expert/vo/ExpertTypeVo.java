package cn.stronglink.esm27.module.expert.expert.vo;

import java.util.List;

import cn.stronglink.esm27.entity.ExpertType;

public class ExpertTypeVo  extends ExpertType{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3300285543423486765L;
	private List<ExpertVo>  expertList;
	private int  expertCout;
	
	public List<ExpertVo> getExpertList() {
		return expertList;
	}
	public void setExpertList(List<ExpertVo> expertList) {
		this.expertList = expertList;
	}
	public int getExpertCout() {
		return expertCout;
	}
	public void setExpertCout(int expertCout) {
		this.expertCout = expertCout;
	}


	

}
