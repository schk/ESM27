package cn.stronglink.esm27.module.emergencyResources.emergencyMaterial.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.esm27.entity.EmergencyMaterial;
import cn.stronglink.esm27.entity.EmergencyMaterialTemp;
import cn.stronglink.esm27.entity.EmergencyMaterialType;
import cn.stronglink.esm27.module.emergencyResources.emergencyMaterial.vo.EmergencyMaterialVo;

public interface EmergencyMaterialMapper extends BaseMapper<EmergencyMaterial> {

	List<EmergencyMaterialVo> qryListByParams(Pagination page, Map<String, Object> params);

	EmergencyMaterialVo qryById(Long id);

	int batchInsertEmergencyMaterial(@Param("interimList") List<EmergencyMaterial> interimList);
	int batchInsertEXLEmergencyMaterialTemp(@Param("interimList")List<EmergencyMaterialTemp> interimList);
	
	List<EmergencyMaterialType> qryEmergencySuppliesType();

	List<EmergencyMaterialTemp> qryEmergencyMaterialTemp(Long timestamp);

	void insertEmergencySuppliesType(EmergencyMaterialType entity);

	void delExpertTemp(@Param("timestamp")Long timestamp);
}
