package cn.stronglink.esm27.module.unit.vo;

import java.util.List;

import cn.stronglink.esm27.entity.KeyParts;
import cn.stronglink.esm27.entity.KeyUnitDangers;
import cn.stronglink.esm27.module.plan.vo.PlanVo;
public class KeyPartsVo extends KeyParts{

	/**
	 * 重点部位扩展属性
	 */
	private static final long serialVersionUID = 4366608878209861297L;
	
	private String keyUnitName;
    private List<KeyUnitDangers> dList ;
    private List<PlanVo> planList ;
    
	public String getKeyUnitName() {
		return keyUnitName;
	}

	public void setKeyUnitName(String keyUnitName) {
		this.keyUnitName = keyUnitName;
	}

	public List<KeyUnitDangers> getdList() {
		return dList;
	}

	public void setdList(List<KeyUnitDangers> dList) {
		this.dList = dList;
	}

	public List<PlanVo> getPlanList() {
		return planList;
	}

	public void setPlanList(List<PlanVo> planList) {
		this.planList = planList;
	}
	

	
	
	
}
