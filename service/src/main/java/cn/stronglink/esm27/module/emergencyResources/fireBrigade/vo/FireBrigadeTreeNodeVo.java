package cn.stronglink.esm27.module.emergencyResources.fireBrigade.vo;

import java.util.List;

import cn.stronglink.esm27.entity.FireBrigade;

public class FireBrigadeTreeNodeVo extends FireBrigade{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<FireBrigadeTreeNodeVo> children;

	public List<FireBrigadeTreeNodeVo> getChildren() {
		return children;
	}

	public void setChildren(List<FireBrigadeTreeNodeVo> children) {
		this.children = children;
	}

	

}
