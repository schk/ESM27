package cn.stronglink.esm27.module.fireWaterSource.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.core.util.BmUtils;
import cn.stronglink.core.util.ImportValid;
import cn.stronglink.core.util.MapLonLatUtil;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Dictionary;
import cn.stronglink.esm27.entity.FireWaterSource;
import cn.stronglink.esm27.entity.FireWaterSourceTemp;
import cn.stronglink.esm27.entity.KeyUnit;
import cn.stronglink.esm27.module.dictionary.service.DictionaryService;
import cn.stronglink.esm27.module.fireWaterSource.service.FireWaterSourceService;
import cn.stronglink.esm27.module.unit.service.KeyUnitService;

@Controller
@RequestMapping(value = "fireWaterSource/excel")
public class FireWaterSourceExcelController extends AbstractController {

	@Autowired
	private FireWaterSourceService entityService;
	@Autowired
	private DictionaryService dictionaryService;
	@Autowired
	private KeyUnitService keyUnitService;

	/**
	 * 导入EXCEL文件
	 * 
	 * @param file
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "importExcel")
	public ResponseEntity<ModelMap> importExcel(@RequestParam(value = "file", required = true) MultipartFile file,
			HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) throws Exception {
		List<String> dataList = new ArrayList<String>();
		System.out.println(WebUtil.getCurrentUser());
		// 导入Excel操作
		// 导入Excel文件不为空时，将文件写入流，然后转化为工作薄
		if (file != null) {
			try {
				Workbook workBook = null;
				if (!ImportValid.validXls(file)) {
					return setModelMap(modelMap, HttpCode.CONFLICT, "请选择excel格式文件！");
				}
				int header = 1;
				workBook = WorkbookFactory.create(file.getInputStream());
				if (workBook.getNumberOfSheets() > 0) {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
					// 获取第一个单元薄
					Sheet sheet = workBook.getSheetAt(0);
					// 获取单元薄中表头信息,并将表头中每个单元格中信息存入数组headNames中
					String allTitle = "*消防水源名称$*消防水源类型$所属重点单位$*消防水源地点$储量$流量$*坐标类型(百度坐标/无偏移坐标)$经纬度$消防水源描述$勘察时间(yyyy/MM/dd HH:mm:ss)$勘察人$核查人$水源点负责人$电话$管辖单位";
					Row rowHead = sheet.getRow(header);
					if (rowHead != null) {
						String[] headNames = new String[rowHead.getLastCellNum()];
						// 获取该行中总共有多少列数据row.getLastCellNum()
						for (int j = 0; j < rowHead.getLastCellNum(); j++) {
							Cell cell = rowHead.getCell(j);
							String returnStr = BmUtils.getCellValue(cell);
							headNames[j] = returnStr;
						}
						if (headNames != null && headNames.length > 0) {
							for (String name : headNames) {
								if (allTitle.indexOf(name) == -1) {
									throw new BusinessException("属性：" + name + "在系统中不存在,请使用模板文件！");
								}
							}
						}
						Date date = new Date();
						// 遍历单元簿中每一行，获取每一列字段，并根据表头对应赋值
						int flag = 3;
						List<FireWaterSource> entityList = new ArrayList<FireWaterSource>();
						List<FireWaterSourceTemp> tempList = new ArrayList<FireWaterSourceTemp>();
						List<Dictionary> typeList = dictionaryService.qryByType(6);

						Map<String, Object> params = new HashMap<String, Object>();
						List<KeyUnit> keyUnitList = keyUnitService.qryKeyUnitList(params);

						Boolean typeNoExist = false;
						int dataFormat = 0;
						modelMap.put("errorCode", 0);
						DecimalFormat df = new DecimalFormat("#.000000");
						// 插入execl中得数据到临时表
						long timestamp = System.currentTimeMillis();
						for (int i = header; i < sheet.getPhysicalNumberOfRows(); i++) {
							Row row = sheet.getRow(i + 1);
							Boolean isRow = ImportValid.isRowEmpty(row);
							FireWaterSource entity = null;
							FireWaterSourceTemp temp = null;
							if (!isRow) {
								String rowError = "错误：第" + flag + "行   ";
								String data = "";
								String lonLatType = "";
								entity = new FireWaterSource();
								entity.setGoodUse(true);
								entity.setId(IdWorker.getId());
								entity.setCreateBy(WebUtil.getCurrentUser());
								entity.setCreateTime(date);
								entity.setUpdateTime(date);
								temp = new FireWaterSourceTemp();
								temp.setId(IdWorker.getId());
								temp.setCreateBy(WebUtil.getCurrentUser());
								temp.setTimestamp(timestamp);
								temp.setCreateTime(date);

								// 获取该行中总共有多少列数据row.getLastCellNum()
								for (int j = 0; j < row.getLastCellNum(); j++) {
									if(j>headNames.length-1) {
										continue ;
									}
									Cell cell = row.getCell(j);
									if (StringUtils.hasText(headNames[j])) {
										// 获取单元格内容，并根据单元格类型，进行转化
										String returnStr = BmUtils.getCellValue(cell);
										// 根据给定的Excel格式，进行导入，一下是对应列对应字段
										if (headNames[j].equals("*消防水源名称")) {
											if (returnStr != null && !"".equals(returnStr)) {
												entity.setName(returnStr);
												temp.setName(returnStr);
											} else {
												dataFormat++;
												data += "【消防水源名称】不能为空;";
											}
										} else if (headNames[j].equals("*消防水源类型")) {
											if (returnStr != null && !returnStr.equals("")) {
												temp.setDic(returnStr);
												if (typeList != null && typeList.size() > 0) {
													boolean isExit = false;
													for (Dictionary type : typeList) {
														if (type.getName().equals(returnStr)) {
															isExit = true;
															entity.setDicId(type.getId());
															break;
														}
													}
													if (!isExit) {
														typeNoExist = true;
														data += "【消防水源类型：" + returnStr + "】不存在;";
													}
												} else {
													typeNoExist = true;
													data += "【消防水源类型：" + returnStr + "】不存在;";
												}
											} else {
												dataFormat++;
												data += "【消防水源类型】不能为空;";
											}
										} else if (headNames[j].equals("所属重点单位")) {
											if (returnStr != null && !returnStr.equals("")) {
												if (keyUnitList != null && keyUnitList.size() > 0) {
													boolean isExit = false;
													for (KeyUnit keyUnit : keyUnitList) {
														if ((keyUnit.getName().replace(" ", ""))
																.equals(returnStr.replace(" ", ""))) {
															isExit = true;
															entity.setKeyUnitId(keyUnit.getId());
															temp.setKeyUnitId(keyUnit.getId());
															break;
														}
													}
													if (!isExit) {
														dataFormat++;
														data += "【所属重点单位：" + returnStr + "】不存在，请先添加;";
													}
												} else {
													dataFormat++;
													data += "【所属重点单位：" + returnStr + "】不存在，请先添加;";
												}
											}
										} else if (headNames[j].equals("*坐标类型(百度坐标/无偏移坐标)")) {
											if (returnStr != null && !"".equals(returnStr)) {
												lonLatType = returnStr;
											} else {
												data += "【坐标类型】不能为空;";
											}
										} else if (headNames[j].equals("*消防水源地点")) {
											if (returnStr != null && !"".equals(returnStr)) {
												entity.setAddr(returnStr);
												temp.setAddr(returnStr);
											} else {
												data += "【消防水源地点】不能为空;";
											}
										} else if (headNames[j].equals("储量")) {
											if (returnStr != null && !"".equals(returnStr)) {
												Boolean isNum = isNumeric(returnStr);
												if (isNum) {
													entity.setReserves(new BigDecimal(returnStr));
													temp.setReserves(new BigDecimal(returnStr));
												} else {
													data += "【储量】请使用数字格式;";
												}
											}
										} else if (headNames[j].equals("流量")) {
											if (returnStr != null && !"".equals(returnStr)) {
												Boolean isNum = isNumeric(returnStr);
												if (isNum) {
													entity.setFlow(new BigDecimal(returnStr));
													temp.setFlow(new BigDecimal(returnStr));
												} else {
													data += "【储量】请使用数字格式;";
												}
											}
										} else if (headNames[j].equals("经纬度")) {
											if (returnStr != null && !"".equals(returnStr)) {
												String[] coord = returnStr.split(",");
												if (coord != null && coord.length == 2) {
													coord[0] = df.format(Double.valueOf(coord[0]));
													coord[1] = df.format(Double.valueOf(coord[1]));
													if ("百度坐标".equals(lonLatType)) {
														double[] pointwgs = MapLonLatUtil.bd09py(
																Double.valueOf(coord[0]), Double.valueOf(coord[1]));
														entity.setBaiduLon(Double.valueOf(coord[0]));
														entity.setBaiduLat(Double.valueOf(coord[1]));
														entity.setLon(Double.valueOf(pointwgs[0]));
														entity.setLat(Double.valueOf(pointwgs[1]));
														temp.setBaiduLon(Double.valueOf(coord[0]));
														temp.setBaiduLat(Double.valueOf(coord[1]));
														temp.setLon(Double.valueOf(pointwgs[0]));
														temp.setLat(Double.valueOf(pointwgs[1]));

													} else if ("无偏移坐标".equals(lonLatType)) {
														entity.setLon(Double.valueOf(coord[0]));
														entity.setLat(Double.valueOf(coord[1]));
														temp.setLon(Double.valueOf(coord[0]));
														temp.setLat(Double.valueOf(coord[1]));
													} else {
														dataFormat++;
														data += "【坐标类型】只能是百度坐标/无偏移坐标;";
													}
												} else {
													data += "【经纬度】格式错误;";
												}
											}
										} else if (headNames[j].equals("消防水源描述")) {
											if (returnStr != null && !"".equals(returnStr)) {
												entity.setRemark(returnStr);
												temp.setRemark(returnStr);
											}
										} else if (headNames[j].equals("勘察时间(yyyy/MM/dd HH:mm:ss)")) {
											if (returnStr != null && !"".equals(returnStr)) {
												entity.setSurveyTime(sdf.parse(returnStr));
												temp.setSurveyTime(sdf.parse(returnStr));
											}
										} else if (headNames[j].equals("勘察人")) {
											if (returnStr != null && !"".equals(returnStr)) {
												entity.setSurveyor(returnStr);
												temp.setSurveyor(returnStr);
											}
										} else if (headNames[j].equals("核查人")) {
											if (returnStr != null && !"".equals(returnStr)) {
												entity.setVerifier(returnStr);
												temp.setVerifier(returnStr);
											}
										} else if (headNames[j].equals("水源点负责人")) {
											if (returnStr != null && !"".equals(returnStr)) {
												entity.setWaterSourcePointHead(returnStr);
												temp.setWaterSourcePointHead(returnStr);
											}
										} else if (headNames[j].equals("电话")) {
											if (returnStr != null && !"".equals(returnStr)) {
												entity.setPhone(returnStr);
												temp.setPhone(returnStr);
											}
										} else if (headNames[j].equals("管辖单位")) {
											if (returnStr != null && !"".equals(returnStr)) {
												entity.setJurisdictionUnit(returnStr);
												temp.setJurisdictionUnit(returnStr);
											}
										}
									}
								}
								flag++;
								entityList.add(entity);
								tempList.add(temp);
								if (data != null && !"".equals(data)) {
									dataList.add(rowError + data);
								}
							}
						}
						if (dataList != null && dataList.size() > 0) {
							if (dataFormat > 0) {
								modelMap.put("errorCode", 2);
								return setSuccessModelMap(modelMap, dataList);
							}
							if (typeNoExist) {
								modelMap.put("errorCode", 1);
								modelMap.put("times", timestamp);
								// 存临时表数据
								if (tempList != null && tempList.size() > 0) {
									entityService.saveExcelTempData(tempList);
									return setSuccessModelMap(modelMap, dataList);
								} else {
									throw new BusinessException("文件数据为空!");
								}
							}
						} else {
							// 存数据
							if (entityList != null && entityList.size() > 0) {
								// 数据存储
								entityService.saveExcelData(entityList);
								return setSuccessModelMap(modelMap);
							} else {
								throw new BusinessException("文件数据为空!");
							}
						}
					}
				}
			} catch (IOException e) {
				throw new BusinessException("未知异常!");
			}
		}
		throw new BusinessException("无文件!");
	}

	/**
	 * 继续导入excel数据
	 * 
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param timestamp
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "importExcelConfirm")
	public ResponseEntity<ModelMap> importExcelConfirm(HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap, @RequestBody Long timestamp) throws Exception {
		Date date = new Date();
		// 遍历单元簿中每一行，获取每一列字段，并根据表头对应赋值
		List<FireWaterSource> entityList = new ArrayList<FireWaterSource>();
		List<Dictionary> typeList = dictionaryService.qryByType(6);
		List<FireWaterSourceTemp> tempList = entityService.qryExcelTemp(timestamp);

		for (int j = 0; j < tempList.size(); j++) {
			FireWaterSourceTemp temp = tempList.get(j);
			FireWaterSource entity = null;
			if (temp != null) {
				entity = new FireWaterSource();
				boolean isExit = false;
				if (typeList != null && typeList.size() > 0) {
					for (Dictionary type : typeList) {
						if (type.getName().equals(temp.getDic())) {
							isExit = true;
							entity.setDicId(type.getId());
							break;
						}
					}
				}
				if (!isExit) {
					Dictionary type = new Dictionary();
					type.setName(temp.getDic());
					type.setRemark(temp.getDic());
					type.setType(6);
					dictionaryService.insert(type);
					entity.setDicId(type.getId());
					typeList.add(type);
				}
				entity.setId(IdWorker.getId());
				entity.setName(temp.getName());
				entity.setGoodUse(true);
				entity.setKeyUnitId(temp.getKeyUnitId());
				entity.setAddr(temp.getAddr());
				entity.setFlow(temp.getFlow());
				entity.setReserves(temp.getReserves());
				entity.setLat(temp.getLat());
				entity.setLon(temp.getLon());
				entity.setBaiduLat(temp.getBaiduLat());
				entity.setBaiduLon(temp.getBaiduLon());
				entity.setRemark(temp.getRemark());
				entity.setCreateBy(WebUtil.getCurrentUser());
				entity.setCreateTime(date);
				entity.setUpdateTime(date);
				entityList.add(entity);
			}
		}
		if (entityList != null && entityList.size() > 0) {
			// 数据存储
			entityService.saveExcelData(entityList);
			// 删除临时表数据
			entityService.delExcelTemp(timestamp);
			return setSuccessModelMap(modelMap);
		} else {
			throw new BusinessException("文件数据为空!");
		}

	}

	/**
	 * 删除临时表数据
	 * 
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param timestamp
	 * @return
	 */
	@RequestMapping(value = "delExcelTemp")
	public ResponseEntity<ModelMap> delExcelTemp(HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap, @RequestBody Long timestamp) {
		// 删除临时表数据
		entityService.delExcelTemp(timestamp);
		return setSuccessModelMap(modelMap);
	}

	public static void main(String[] args) {
		String numStr = "123121.11r";
		System.out.println(isNumeric(numStr));
	}

	public static boolean isNumeric(String str) {
		String reg = "^[0-9]+(.[0-9]+)?$";
		return str.matches(reg);
	}

}
