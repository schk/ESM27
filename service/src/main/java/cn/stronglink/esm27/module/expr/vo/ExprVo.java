package cn.stronglink.esm27.module.expr.vo;

import java.util.List;

import cn.stronglink.esm27.entity.Expr;
import cn.stronglink.esm27.entity.ExprParam;

public class ExprVo extends Expr {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<ExprParam> exprParams;

	public List<ExprParam> getExprParams() {
		return exprParams;
	}

	public void setExprParams(List<ExprParam> exprParams) {
		this.exprParams = exprParams;
	}
	
	

}
