package cn.stronglink.esm27.module.emergencyResources.emergencyMaterial.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.MaterialUseRecord;

public interface MaterialUseRecordMapper extends BaseMapper<MaterialUseRecord> {

	int getMaterialCountByRoomId(String roomId);

	int getExtinguisherCountByRoomId(String roomId);

	
}
