package cn.stronglink.esm27.module.accident.controller;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.AccidentCase;
import cn.stronglink.esm27.module.accident.service.AccidentCaseService;
import cn.stronglink.esm27.module.accident.vo.FireCaseVo;

@Controller
@RequestMapping(value = "accidentCase")
public class AccidentCaseController extends AbstractController  {
	
	@Autowired
	private AccidentCaseService accidentCaseService;
	
	/*
	 * 查询所有信息
	 */
	@RequestMapping("qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<FireCaseVo> page = (Page<FireCaseVo>) super.getPage(params);
		Page<FireCaseVo> data = accidentCaseService.qryFireCaseListByParams(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询对象
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		AccidentCase entity =accidentCaseService.selectById(id);
		return setSuccessModelMap(modelMap, entity);
	}
	
	/**
	 * 新建事故案例
	 */
	@RequestMapping(value = "create")
	@OperateLog(module = "事故案例管理",desc="添加事故案例", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody AccidentCase entity) {
		accidentCaseService.insert(entity);  
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 修改事故案例
	 */
	@RequestMapping(value = "edit")
	@OperateLog(module = "事故案例管理",desc="修改事故案例", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody AccidentCase entity) {
		accidentCaseService.update(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 删除事故案例
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "remove")
	@OperateLog(module = "事故案例管理",desc="删除事故案例", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		accidentCaseService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}
	
}
