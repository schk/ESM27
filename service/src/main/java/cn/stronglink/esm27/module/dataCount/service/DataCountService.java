package cn.stronglink.esm27.module.dataCount.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.stronglink.esm27.entity.EquipmentType;
import cn.stronglink.esm27.module.dataCount.mapper.DataCountMapper;
import cn.stronglink.esm27.module.dataCount.param.DataCountParam;

@Service
@Transactional(rollbackFor=Exception.class)
public class DataCountService {

	@Autowired
	private DataCountMapper dataCountMapper;
	
	
	public Map<String, Object> qryKeyUnitCount(DataCountParam param) {
		Map<String,Object> result=new HashMap<String,Object>();
		//获取消防设施分类数据
		List<EquipmentType> equipmentTypes=dataCountMapper.getEquipmentType(1);
		List<Map<String, Object>> list=new ArrayList<Map<String, Object>>();
		List<String> teamNames=new ArrayList<String>();
		List<Map<String,Object>> dataList=dataCountMapper.qryKeyUnitCount(param);
		if(equipmentTypes!=null&&equipmentTypes.size()>0){
			for(EquipmentType type:equipmentTypes){
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("label", type.getName());
				for(Map<String,Object> data:dataList){
					if(teamNames.size()==0||!teamNames.contains(data.get("teamName").toString())){
						teamNames.add(data.get("teamName").toString());
					}
					if(type.getId_().equals(data.get("typeId").toString())){
						map.put(data.get("teamName").toString(),data.get("counts"));
					}
				}
				list.add(map);
			}
		}
		result.put("fields",teamNames);
		result.put("dataList",list);
		return result;
	}


	public List<Map<String, Object>> qryTeamCount(DataCountParam param) {
		return dataCountMapper.qryTeamCount(param);
	}


	public List<Map<String, Object>> qryAccidentCount(DataCountParam param) {
		List<Map<String, Object>> dataList= dataCountMapper.qryAccidentCount(param);
		List<Map<String,Object>> result=new ArrayList<Map<String,Object>>();
		if(dataList!=null&&dataList.size()>0){
			for(String str:param.getDates()){
				boolean isContain=false;
				for(Map<String, Object> data:dataList){
					if(str.equals(data.get("label").toString())){
						result.add(data);
						isContain=true;
						break;
					}
				}
				if(!isContain){
					Map<String,Object> map=new HashMap<String,Object>();
					map.put("label", str);
					map.put("value", 0);
					result.add(map);
				}
			}
		}else{
			for(String str:param.getDates()){
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("label", str);
				map.put("value", 0);
				result.add(map);
			}
		}
		
		return result;
	}


	public List<Map<String, Object>> qryPoliceNumber(DataCountParam param) {
		List<Map<String, Object>> dataList= dataCountMapper.qryPoliceNumber(param);
		List<Map<String,Object>> result=new ArrayList<Map<String,Object>>();
		if(dataList!=null&&dataList.size()>0){
			for(String str:param.getDates()){
				boolean isContain=false;
				for(Map<String, Object> data:dataList){
					if(str.equals(data.get("label").toString())){
						result.add(data);
						isContain=true;
						break;
					}
				}
				if(!isContain){
					Map<String,Object> map=new HashMap<String,Object>();
					map.put("label", str);
					map.put("value", 0);
					result.add(map);
				}
			}
		}else{
			for(String str:param.getDates()){
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("label", str);
				map.put("value", 0);
				result.add(map);
			}
		}
		
		return result;
	}

}
