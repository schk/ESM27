package cn.stronglink.esm27.module.plan.vo;

import java.util.List;

import cn.stronglink.esm27.entity.Dictionary;

public class PlanTypeVo extends Dictionary{

	private static final long serialVersionUID = 1L;
	
	private List<PlanVo>  planList;
	private int  planCount;
	public List<PlanVo> getPlanList() {
		return planList;
	}
	public void setPlanList(List<PlanVo> planList) {
		this.planList = planList;
	}
	public int getPlanCount() {
		return planCount;
	}
	public void setPlanCount(int planCount) {
		this.planCount = planCount;
	}

	
	
}
