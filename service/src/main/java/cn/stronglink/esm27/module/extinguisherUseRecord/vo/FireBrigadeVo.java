package cn.stronglink.esm27.module.extinguisherUseRecord.vo;

import java.util.List;

public class FireBrigadeVo{

	private Long id;
	private Long key;
	private String label;
	private String name;
	private Long value;
	private List<FireBrigadeVo> children;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getKey() {
		return key;
	}
	public void setKey(Long key) {
		this.key = key;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getValue() {
		return value;
	}
	public void setValue(Long value) {
		this.value = value;
	}
	public List<FireBrigadeVo> getChildren() {
		return children;
	}
	public void setChildren(List<FireBrigadeVo> children) {
		this.children = children;
	}
	
}
