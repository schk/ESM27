package cn.stronglink.esm27.module.extinguisher.controller;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.core.util.BmUtils;
import cn.stronglink.core.util.ImportValid;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Dictionary;
import cn.stronglink.esm27.entity.Extinguisher;
import cn.stronglink.esm27.entity.ExtinguisherTemp;
import cn.stronglink.esm27.entity.FireBrigade;
import cn.stronglink.esm27.module.dictionary.service.DictionaryService;
import cn.stronglink.esm27.module.emergencyResources.fireBrigade.service.FireBrigadeService;
import cn.stronglink.esm27.module.extinguisher.service.ExtinguisherService;

@Controller
@RequestMapping(value = "extinguisher/excel")
public class ExtinguisherExcelController extends AbstractController {

	@Autowired
	private ExtinguisherService entityService;
	@Autowired
	private DictionaryService dictionaryService;
	@Autowired
	private FireBrigadeService fireBrigadeService;

	/**
	 * 导入EXCEL文件
	 * 
	 * @param file
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "importExcel")
	public ResponseEntity<ModelMap> importExcel(@RequestParam(value = "file", required = true) MultipartFile file,
			HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) throws Exception {
		List<String> dataList = new ArrayList<String>();
		Workbook workBook = null;
		InputStream is = null;
		System.out.println(WebUtil.getCurrentUser());
		// 导入Excel操作
		// 导入Excel文件不为空时，将文件写入流，然后转化为工作薄
		if (file != null) {
			try {
				if(!ImportValid.validXls(file)){
					return setModelMap(modelMap, HttpCode.CONFLICT, "请选择excel格式文件！");
				}
				int header = 1;
				is =file.getInputStream();
				workBook = WorkbookFactory.create(is);
				if (workBook.getNumberOfSheets() > 0) {
					// 获取第一个单元薄
					Sheet sheet = workBook.getSheetAt(0);
					// 获取单元薄中表头信息,并将表头中每个单元格中信息存入数组headNames中
					String allTitle = "*灭火剂类型$*所属消防队$*配备数量(KG)$*生产日期$*有效日期";
					Row rowHead = sheet.getRow(header);
					if (rowHead != null) {
						String[] headNames = new String[rowHead.getLastCellNum()];
						// 获取该行中总共有多少列数据row.getLastCellNum()
						for (int j = 0; j < rowHead.getLastCellNum(); j++) {
							// 获取表头各个字段名称
							headNames[j] = BmUtils.getCellValue(rowHead.getCell(j));
						}
						if (headNames != null && headNames.length > 0) {
							for (String name : headNames) {
								if (allTitle.indexOf(name) == -1) {
									throw new BusinessException("属性：" + name + "在系统中不存在,请使用模板文件！");
								}
							}
						}
						Date date = new Date();
						// 遍历单元簿中每一行，获取每一列字段，并根据表头对应赋值
						int flag = 3;
						List<Extinguisher> entityList = new ArrayList<Extinguisher>();
						List<ExtinguisherTemp> tempList = new ArrayList<ExtinguisherTemp>();
						List<FireBrigade> fireBrigadeList = fireBrigadeService.qryList();
						List<Dictionary> typeList = dictionaryService.qryByType(5);

						Boolean typeNoExist = false;
						int dataFormat = 0;
						modelMap.put("errorCode", 0);

						// 插入execl中得数据到临时表
						long timestamp = System.currentTimeMillis();
						for (int i = header; i < sheet.getPhysicalNumberOfRows(); i++) {
							Row row = sheet.getRow(i + 1);
							Boolean isRow = ImportValid.isRowEmpty(row);
							Extinguisher entity = null;
							ExtinguisherTemp temp = null;
							if (!isRow) {
								String rowError = "错误：第" + flag + "行   ";
								String data = "";
								entity = new Extinguisher();
								entity.setId(IdWorker.getId());
								entity.setCreateBy(WebUtil.getCurrentUser());
								entity.setCreateTime(date);
								entity.setUpdateTime(date);

								temp = new ExtinguisherTemp();
								temp.setId(IdWorker.getId());
								temp.setCreateBy(WebUtil.getCurrentUser());
								temp.setTimestamp(timestamp);
								temp.setCreateTime(date);

								// 获取该行中总共有多少列数据row.getLastCellNum()
								for (int j = 0; j < headNames.length; j++) {
									if (StringUtils.hasText(headNames[j])) {
										// 获取单元格内容，并根据单元格类型，进行转化
										String returnStr = BmUtils.getCellValue(row.getCell(j));
										// 根据给定的Excel格式，进行导入，一下是对应列对应字段
										if (headNames[j].equals("*灭火剂类型")) {
											if (returnStr != null && !returnStr.equals("")) {
												temp.setType(returnStr);
												if (typeList != null && typeList.size() > 0) {
													for (Dictionary type : typeList) {
														if ((type.getName().replace(" ", "")).equals(returnStr.replace(" ", ""))) {
															entity.setTypeId(type.getId());
															break;
														}
													}
													if (entity.getTypeId()==null||"".equals(entity.getTypeId())) {
														typeNoExist = true;
														data += "【灭火剂类型："+ returnStr +"】不存在;";
													}
												}else{
													typeNoExist = true;
													data += "【灭火剂类型："+ returnStr +"】 不存在;";
												}
											} else {
												dataFormat++;
												data += "【灭火剂类型】不能为空;";
											}
										} else if (headNames[j].equals("*所属消防队")) {
											if (returnStr != null && !"".equals(returnStr)) {
												if (fireBrigadeList != null && fireBrigadeList.size() > 0) {
													for (FireBrigade fireBrigade : fireBrigadeList) {
														if ((fireBrigade.getName().replace(" ", "")).equals(returnStr.replace(" ", ""))) {
															entity.setFireBrigadeId(fireBrigade.getId());
															temp.setFireBrigadeId(fireBrigade.getId());
															break;
														}
													}
													if (entity.getFireBrigadeId()==null||"".equals(entity.getFireBrigadeId())) {
														dataFormat++;
														data += "【所属消防队："+ returnStr +"】不存在;";
													}
												}else{
													dataFormat++;
													data += "【所属消防队："+ returnStr +"】不存在;";
												}
											} else {
												dataFormat++;
												data += "【所属消防队】不能为空;";
											}
										} else if (headNames[j].equals("*配备数量(KG)")) {
											if (returnStr != null && !"".equals(returnStr)) {
												entity.setQuantity(Double.valueOf(returnStr));
												temp.setQuantity(Double.valueOf(returnStr));
											} else {
												dataFormat++;
												data += "【配备数量】不能为空;";
											}
										} else if (headNames[j].equals("*生产日期")) {
											if(ImportValid.isNotEmpty(returnStr)){
												if (ImportValid.isValidDate("yyyy/MM/dd",returnStr)) {
													SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
													entity.setProductDate(sdf.parse(returnStr));
													temp.setProductDate(sdf.parse(returnStr));
												}else {
													dataFormat++;
													data+="【生产日期】格式不对;";
												}
											}else{
												dataFormat++;
												data += "【生产日期】不能为空;";
											}
										} else if (headNames[j].equals("*有效日期")) {
											if(ImportValid.isNotEmpty(returnStr)){
												if (ImportValid.isValidDate("yyyy/MM/dd",returnStr)) {
													SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
													entity.setEffectiveDate(sdf.parse(returnStr));
													temp.setEffectiveDate(sdf.parse(returnStr));
												}else {
													dataFormat++;
													data+="【有效日期】格式不对;";
												}
											}else{
												dataFormat++;
												data += "【有效日期】不能为空;";
											}
										}
									}
								}
								flag++;
								entityList.add(entity);
								tempList.add(temp);
								if (data != null && !"".equals(data)) {
									dataList.add(rowError+data);
								}
							}
						}
						if (dataList != null && dataList.size() > 0) {
							if (dataFormat > 0) {
								modelMap.put("errorCode", 2);
								return setSuccessModelMap(modelMap, dataList);
							}
							if (typeNoExist) {
								modelMap.put("errorCode", 1);
								modelMap.put("times", timestamp);
								// 存临时表数据
								if (tempList != null && tempList.size() > 0) {
									entityService.saveExcelTempData(tempList);
									return setSuccessModelMap(modelMap, dataList);
								} else {
									throw new BusinessException("文件数据为空!");
								}
							}
						} else {
							// 存数据
							if (entityList != null && entityList.size() > 0) {
								// 数据存储
								entityService.saveExcelData(entityList);
								return setSuccessModelMap(modelMap);
							} else {
								throw new BusinessException("文件数据为空!");
							}
						}
					}
				}
			} catch (IOException e) {
				throw new BusinessException("未知异常!");
			}finally {
				if (null != is) {
					try {
						is.close();
					} catch (Exception e) {
						is = null;
						e.printStackTrace();
					}
				}
			}
		}
		throw new BusinessException("无文件!");
	}

	/**
	 * 继续导入excel数据
	 * 
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param timestamp
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "importExcelConfirm")
	public ResponseEntity<ModelMap> importExcelConfirm(HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap, @RequestBody Long timestamp) throws Exception {
		Date date = new Date();
		// 遍历单元簿中每一行，获取每一列字段，并根据表头对应赋值
		List<Extinguisher> entityList = new ArrayList<Extinguisher>();
		List<Dictionary> typeList = dictionaryService.qryByType(5);
		List<ExtinguisherTemp> tempList = entityService.qryExcelTemp(timestamp);

		for (int j = 0; j < tempList.size(); j++) {
			ExtinguisherTemp temp = tempList.get(j);
			Extinguisher entity = null;
			if (temp != null) {
				entity = new Extinguisher();
				boolean isExit = false;
				if (typeList != null && typeList.size() > 0) {
					for (Dictionary type : typeList) {
						if (type.getName().equals(temp.getType())) {
							isExit = true;
							entity.setTypeId(type.getId());
							break;
						}
					}
				}
				if (!isExit) {
					Dictionary type = new Dictionary();
					type.setName(temp.getType());
					type.setRemark(temp.getType());
					type.setType(5);
					dictionaryService.insert(type);
					entity.setTypeId(type.getId());
					typeList.add(type);
				}
				entity.setId(IdWorker.getId());
				entity.setFireBrigadeId(temp.getFireBrigadeId());
				entity.setQuantity(temp.getQuantity());
				entity.setProductDate(temp.getProductDate());
				entity.setEffectiveDate(temp.getEffectiveDate());
				entity.setCreateBy(WebUtil.getCurrentUser());
				entity.setCreateTime(date);
				entity.setUpdateTime(date);
				entityList.add(entity);
			}
		}
		if (entityList != null && entityList.size() > 0) {
			// 数据存储
			entityService.saveExcelData(entityList);
			// 删除临时表数据
			entityService.delExcelTemp(timestamp);
			return setSuccessModelMap(modelMap);
		} else {
			throw new BusinessException("文件数据为空!");
		}

	}

	/**
	 * 删除临时表数据
	 * 
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param timestamp
	 * @return
	 */
	@RequestMapping(value = "delExcelTemp")
	public ResponseEntity<ModelMap> delExcelTemp(HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap, @RequestBody Long timestamp) {
		// 删除临时表数据
		entityService.delExcelTemp(timestamp);
		return setSuccessModelMap(modelMap);
	}

	@SuppressWarnings("deprecation")
	public static boolean isRowEmpty(Row row) {
		if (row == null) {
			return true;
		} else {
			for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
				Cell cell = row.getCell(c);
				if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK)
					return false;
			}
			return true;
		}
	}

	public static boolean isValidDate(String datePattern, String str) {
		boolean convertSuccess = true;
		SimpleDateFormat format = new SimpleDateFormat(datePattern);
		try {
			format.setLenient(false);
			format.parse(str);
		} catch (Exception e) {
			convertSuccess = false;
		}
		return convertSuccess;
	}

}
