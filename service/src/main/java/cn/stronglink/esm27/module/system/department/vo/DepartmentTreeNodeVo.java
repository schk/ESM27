package cn.stronglink.esm27.module.system.department.vo;

import java.util.List;

import cn.stronglink.esm27.entity.Department;

public class DepartmentTreeNodeVo extends Department{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<DepartmentTreeNodeVo> children;

	public List<DepartmentTreeNodeVo> getChildren() {
		return children;
	}

	public void setChildren(List<DepartmentTreeNodeVo> children) {
		this.children = children;
	}

	

}
