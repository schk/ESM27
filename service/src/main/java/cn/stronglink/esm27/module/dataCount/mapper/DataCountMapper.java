package cn.stronglink.esm27.module.dataCount.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import cn.stronglink.esm27.entity.EquipmentType;
import cn.stronglink.esm27.module.dataCount.param.DataCountParam;

public interface DataCountMapper {

	List<Map<String, Object>> qryKeyUnitCount(DataCountParam param);

	List<EquipmentType> getEquipmentType(@Param("type")int type);

	List<Map<String, Object>> qryTeamCount(DataCountParam param);

	List<Map<String, Object>> qryAccidentCount(DataCountParam param);

	List<Map<String, Object>> qryPoliceNumber(DataCountParam param);

}
