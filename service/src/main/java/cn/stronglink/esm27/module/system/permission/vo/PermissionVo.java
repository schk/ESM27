package cn.stronglink.esm27.module.system.permission.vo;

import java.util.List;

import cn.stronglink.esm27.entity.Permission;

public class PermissionVo extends Permission{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<PermissionVo> children;

	public List<PermissionVo> getChildren() {
		return children;
	}

	public void setChildren(List<PermissionVo> children) {
		this.children = children;
	}




}
