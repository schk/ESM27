package cn.stronglink.esm27.module.dictionary.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.esm27.entity.Dictionary;
import cn.stronglink.esm27.module.dictionary.service.DictionaryService;
import cn.stronglink.esm27.module.dictionary.vo.CarTypeTreeNodeVo;

@Controller
@RequestMapping("dictionary")
public class DictionaryController extends AbstractController {
	
	@Autowired
	private DictionaryService dictionaryService;
	
	
	/*
	 * 根据type查询信息
	 */
	@RequestMapping(value="qryByType")
	public ResponseEntity<ModelMap>  qryByType(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Integer type){
		List<Dictionary> data = dictionaryService.qryByType(type);
		return setSuccessModelMap(modelMap, data);

	}
	
	
	/**
	 * 查询树形的车辆类型
	 * @param modelMap
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "qryCarTypeTree")
	public ResponseEntity<ModelMap> qryCarTypeTree(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<CarTypeTreeNodeVo> vo = dictionaryService.qryCarTypeTree();
		return setSuccessModelMap(modelMap, vo);
	}
	
	/**
	 * 查询父级车辆类型列表
	 * @param modelMap
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "qryCarTypeP")
	public ResponseEntity<ModelMap> qryCarTypeP(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<CarTypeTreeNodeVo> vo = dictionaryService.qryCarTypeP();
		return setSuccessModelMap(modelMap, vo);
	}
	
	/**
	 * 查询父级车辆类型列表
	 * @param modelMap
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "qryCarTypeC")
	public ResponseEntity<ModelMap> qryCarTypeC(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long pid) {
		List<CarTypeTreeNodeVo> vo = dictionaryService.qryCarTypeC(pid);
		return setSuccessModelMap(modelMap, vo);
	}
	
	/**
	 * 通过id查询信息
	 */
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> qryById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		Dictionary vo = dictionaryService.selectById(id);
		return setSuccessModelMap(modelMap, vo);
	}
	/**
	 * 新建信息
	 */
	@RequestMapping(value = "create")
	@OperateLog(module = "所有类型管理",desc="添加所有类型", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Dictionary entity) {
		boolean flag =dictionaryService.insert(entity);
		if(!flag){
			return setModelMap(modelMap, HttpCode.CONFLICT, "名称不能重复");
		}
		return setSuccessModelMap(modelMap, null);
	}
	/**
	 * 修改信息
	 */
	@RequestMapping(value = "edit")
	@OperateLog(module = "所有类型管理",desc="修改所有类型", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Dictionary entity) {
		boolean flag =dictionaryService.update(entity);
		if(!flag){
			return setModelMap(modelMap, HttpCode.CONFLICT, "名称不能重复");
		}
		return setSuccessModelMap(modelMap, null);
	}
	/**
	 * 删除信息
	 */
	@RequestMapping(value = "remove")
	@OperateLog(module = "所有类型管理",desc="删除所有类型", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		dictionaryService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}

}
