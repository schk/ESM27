package cn.stronglink.esm27.module.plan.vo;

import java.util.List;

import cn.stronglink.esm27.entity.Plan;
import cn.stronglink.esm27.entity.PlansCatalog;

public class PlanVo extends Plan{

	private static final long serialVersionUID = 1L;
	
	private String createUserName;
	private String typeName;
	private String KeyUnitName;
	private String levelName;
	private String fireBrigadeName;
	
	private List<Long> planIdList;
	
	private List<PlansCatalog> plansCatalogs;

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getTypeName() {
		return typeName;
	}

	public String getKeyUnitName() {
		return KeyUnitName;
	}

	public void setKeyUnitName(String keyUnitName) {
		KeyUnitName = keyUnitName;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public List<PlansCatalog> getPlansCatalogs() {
		return plansCatalogs;
	}

	public void setPlansCatalogs(List<PlansCatalog> plansCatalogs) {
		this.plansCatalogs = plansCatalogs;
	}

	public String getFireBrigadeName() {
		return fireBrigadeName;
	}

	public void setFireBrigadeName(String fireBrigadeName) {
		this.fireBrigadeName = fireBrigadeName;
	}

	public List<Long> getPlanIdList() {
		return planIdList;
	}

	public void setPlanIdList(List<Long> planIdList) {
		this.planIdList = planIdList;
	}
	
	
	
}
