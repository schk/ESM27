package cn.stronglink.esm27.module.system.permission.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.module.system.permission.service.PermissionService;
import cn.stronglink.esm27.module.system.permission.vo.PermissionVo;
import cn.stronglink.esm27.module.system.role.vo.RoleVo;

@Controller
@RequestMapping(value = "system/permission")
public class PermissionController extends AbstractController {
	
	@Autowired
	private PermissionService permissionService;
	
	@RequestMapping(value = "qryPermissionList")
	public ResponseEntity<ModelMap> qryPermissionList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<PermissionVo> list = permissionService.selectPermissionList();
		return setSuccessModelMap(modelMap, list);
	}
	
	@RequestMapping(value = "qryRolePermission")
	public ResponseEntity<ModelMap> qryRolePermission(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		List<String> list=permissionService.getRolePermission(id);
		return setSuccessModelMap(modelMap, list);
	}
	
	@RequestMapping(value = "qryRolePermissionNoP")
	public ResponseEntity<ModelMap> qryRolePermissionNoP(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		List<String> list=permissionService.getRolePermissionNoP(id);
		return setSuccessModelMap(modelMap, list);
	}
	
	@OperateLog(module = "角色管理",desc="设置角色权限", type = OpType.UPDATE)
	@RequestMapping(value = "setRolePermission")
	public ResponseEntity<ModelMap> setRolePermission(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody RoleVo vo) {
		permissionService.setRolePermission(vo);
		return setSuccessModelMap(modelMap, null);
	}
	
	

}
