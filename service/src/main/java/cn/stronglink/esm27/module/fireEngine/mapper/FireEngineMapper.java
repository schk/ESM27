package cn.stronglink.esm27.module.fireEngine.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.esm27.entity.FireEngine;
import cn.stronglink.esm27.entity.FireEngineTemp;
import cn.stronglink.esm27.module.fireEngine.vo.FireEngineVo;

public interface FireEngineMapper extends BaseMapper<FireEngine> {
	
	List<FireEngineVo> qryListByParams(Pagination page, Map<String, Object> params);

	void insertIntoBatch(HashMap<String, Object> mapBatch);

	FireEngineVo getById(@Param("id") Long id);

	int batchInsert(@Param("interimList") List<FireEngine> interimList);

	int batchInsertTemp(@Param("interimList") List<FireEngineTemp> interimList);

	List<FireEngineTemp> qryExcelTemp(@Param("timestamp") Long timestamp);

	void delExcelTemp(@Param("timestamp") Long timestamp);

	FireEngineVo qryCarCountByBrigade(Long id);

	List<FireEngineVo> qryFireEngineByBrigandeId(Page<FireEngineVo> page, Map<String, Object> params);

	List<FireEngineVo> qryMieHuoJ(Map<String, Object> p);

	int updateByPlateNumbery(FireEngine fireEngine);

	void updateFireEngineStatusByRoomId(@Param("roomId")Long roomId);
}
