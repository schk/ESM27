package cn.stronglink.esm27.module.fireEngine.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.FireBrigade;
import cn.stronglink.esm27.entity.FireEngineUseRecord;
import cn.stronglink.esm27.module.fireEngine.vo.FireEngineTypeVo;

public interface FireEngineUseRecordMapper extends BaseMapper<FireEngineUseRecord> {

	void updateStatus(Long fireEngineId, int i);

	Map<String,Object> getFireBrigadeByRoomId(String roomId);

	Integer getFireEngineByRoomId(String roomId);

	int getEquipByRoomId(String roomId);

	List<FireBrigade> getFireBrigade(String roomId);

	List<String> getFireEngineByBrigadeId(@Param("fireBrigadeId") Long id, @Param("roomId")String roomId);

	List<FireEngineTypeVo> getFireEngineTypeList(Long roomId);

	List<Map<String, String>> getXFDList(Long roomId);

	List<Map<String, String>> getXFCList(@Param("roomId") Long roomId, @Param("brigadeId") String brigadeId);
	


}
