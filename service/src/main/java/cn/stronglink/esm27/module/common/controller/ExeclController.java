package cn.stronglink.esm27.module.common.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.core.util.BmUtils;
import cn.stronglink.core.util.DownloadExcelUtil;
import cn.stronglink.core.util.ExlVo;
import cn.stronglink.core.util.ImportValid;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.EmergencyMaterial;
import cn.stronglink.esm27.entity.EmergencyMaterialTemp;
import cn.stronglink.esm27.entity.EmergencyMaterialType;
import cn.stronglink.esm27.entity.Expert;
import cn.stronglink.esm27.entity.ExpertTemp;
import cn.stronglink.esm27.entity.ExpertType;
import cn.stronglink.esm27.entity.Expr;
import cn.stronglink.esm27.entity.ReservePoint;
import cn.stronglink.esm27.module.emergencyResources.emergencyMaterial.service.EmergencyMaterialService;
import cn.stronglink.esm27.module.emergencyResources.emergencySuppliesType.service.EmergencySuppliesTypeService;
import cn.stronglink.esm27.module.emergencyResources.reservePoint.service.ReservePointService;
import cn.stronglink.esm27.module.expert.expert.service.ExpertService;
import cn.stronglink.esm27.module.expert.expertType.service.ExpertTypeService;
import cn.stronglink.esm27.module.expr.service.ExprService;
import jxl.format.BorderLineStyle;

@Controller
@RequestMapping(value = "execl")
public class ExeclController extends AbstractController {

	@Autowired
	private ExpertService expertService;

	@Autowired
	private ExprService exprService;
	@Autowired
	private ExpertTypeService expertTypeService;
	// 应急物资类型
	@Autowired
	private EmergencySuppliesTypeService emergencySuppliesTypeService;
	// 应急物资
	@Autowired
	private EmergencyMaterialService emergencyMaterialService;
	// 重点单位
	@Autowired
	private ReservePointService reservePointService;

	@RequestMapping(value = "downLoadTemplate")
	public void downLoadTemplate(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
		try {
			String type = request.getParameter("type");
			TempEnum tempEnum = TempEnum.getEnum(type);
			String fileType = ".xlsx";
			String filePath = ExeclController.class.getResource("/").getPath() + "/static/importTemplate/"
					+ tempEnum.getType() + fileType;
			// path是指欲下载的文件的路径。
			File file = new File(filePath);
			// 取得文件名。
			String filename = tempEnum.getName() + fileType;
			// 以流的形式下载文件。
			InputStream fis = new BufferedInputStream(new FileInputStream(filePath));
			byte[] buffer = new byte[fis.available()];
			fis.read(buffer);
			fis.close();
			// 设置response的Header
			response.addHeader("Content-Disposition",
					"attachment;filename=" + new String(filename.getBytes("GB2312"), "ISO8859-1"));
			response.addHeader("Content-Length", "" + file.length());
			OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
			response.setContentType("application/octet-stream");
			toClient.write(buffer);
			toClient.flush();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public enum TempEnum {
		PLANS("plansTemplate", "应急预案导入模板"), EXPERT("expertTemplate", "应急专家导入模板"), TEAMS("teamsTemplate", "应急队伍导入模板"),
		RESERVEPOINT("reservePointTemplate", "物资储备点导入模板"), MATERIAL("emergencyMaterial", "应急物资导入模板"),
		WATERSOURCE("fireWaterSourceTemplate", "消防水源导入模板"), FACILITIES("facilitiesTemplate", "消防设施导入模板"),
		EQUIPMENT("equipmentTemplate", "生产装置导入模板"), USER("user", "系统用户导入模板"), FIRE_BRIGADE("fireBrigade", "消防队导入模板"),
		KEY_UNIT("keyUnit", "重点单位导入模板"), ZDBW("keyParts", "重点部位导入模板"), CG_EQUIP("cgEquip", "常规器材导入模板"),
		ZQ_EQUIP("zqEquip", "专勤器材导入模板"), KQ_EQUIP("kqEquip", "空气呼吸器导入模板"), XFC("fireEngineTemplate", "消防车导入模板"),
		MHJ("extinguisherTemplate", "灭火剂导入模板"), SOCIAL_RESOURCE("socialResourceTemplate", "社会资源导入模版");

		private String type;
		private String name;

		TempEnum(String type, String name) {
			this.type = type;
			this.name = name;
		}

		public String getType() {
			return type;
		}

		public String getName() {
			return name;
		}

		public static TempEnum getEnum(String value) {
			TempEnum enumObj = null;
			for (TempEnum obj : TempEnum.values()) {
				if (obj.getType().equals(value)) {
					enumObj = obj;
					break;
				}
			}

			return enumObj;
		}
	}

	/**
	 * 导入EXCEL文件
	 * 
	 * @param file
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "importExpert")
	public ResponseEntity<ModelMap> importExpert(@RequestParam(value = "file", required = true) MultipartFile file,
			HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) throws Exception {
		List<String> dataList = new ArrayList<String>();
		// 导入Excel操作
		// 导入Excel文件不为空时，将文件写入流，然后转化为工作薄
		if (file != null) {
			try {
				Workbook workBook = null;
				if (!ImportValid.validXls(file)) {
					return setModelMap(modelMap, HttpCode.CONFLICT, "请选择excel格式文件！");
				}
				int header = 1;
				workBook = WorkbookFactory.create(file.getInputStream());
				if (workBook.getNumberOfSheets() > 0) {
					// 获取第一个单元薄
					Sheet sheet = workBook.getSheetAt(0);
					// 获取单元薄中表头信息,并将表头中每个单元格中信息存入数组headNames中
					String allTitle = "*专家名称$*性别$*专家类型$*联系方式$*所属单位$专业$主修科目$特长$家庭住址$出生日期-(yyyy/MM/dd)";
					Row rowHead = sheet.getRow(header);
					if (rowHead != null) {
						String[] headNames = new String[rowHead.getLastCellNum()];
						// 获取该行中总共有多少列数据row.getLastCellNum()
						for (int j = 0; j < rowHead.getLastCellNum(); j++) {
							Cell cell = rowHead.getCell(j);
							// 获取表头各个字段名称
							String returnStr = BmUtils.getCellValue(cell);
							headNames[j] = returnStr;
						}
						if (headNames != null && headNames.length > 0) {
							for (String name : headNames) {
								if (allTitle.indexOf(name) == -1) {
									throw new BusinessException("属性：" + name + "在系统中不存在,请使用模板文件！");
								}
							}
						}
						Date date = new Date();
						// 遍历单元簿中每一行，获取每一列字段，并根据表头对应赋值
						int flag = 3;
						List<Expert> expertList = new ArrayList<Expert>();
						List<ExpertTemp> expertTempList = new ArrayList<ExpertTemp>();
						List<ExpertType> typeList = expertTypeService.qryListByParams();
						Boolean typeNoExist = false;
						int dataFormat = 0;
						modelMap.put("errorCode", 0);
						// 插入execl中得数据到临时表
						long timestamp = System.currentTimeMillis();
						for (int i = header; i < sheet.getPhysicalNumberOfRows(); i++) {
							Row row = sheet.getRow(i + 1);
							Boolean isRow = ImportValid.isRowEmpty(row);
							Expert expert = null;
							ExpertTemp expertTemp = null;
							if (!isRow) {
								String rowError = "错误： 第" + flag + "行   ";
								String data = "";
								expert = new Expert();
								expert.setId(IdWorker.getId());
								expert.setCreateBy(WebUtil.getCurrentUser());
								expert.setCreateTime(date);
								expert.setUpdateTime(date);

								expertTemp = new ExpertTemp();
								expertTemp.setId(IdWorker.getId());
								expertTemp.setCreateBy(WebUtil.getCurrentUser());
								expertTemp.setTimestamp(timestamp);
								expertTemp.setCreateTime(date);

								// 获取该行中总共有多少列数据row.getLastCellNum()
								for (int j = 0; j < row.getLastCellNum(); j++) {
									if(j>headNames.length-1) {
										continue ;
									}
									Cell cell = row.getCell(j);
//									if (cell == null) {
//										continue;
//									} else 
									if (StringUtils.hasText(headNames[j])) {
										// 获取单元格内容，并根据单元格类型，进行转化
										String returnStr = BmUtils.getCellValue(cell);
										// 根据给定的Excel格式，进行导入，一下是对应列对应字段
										if (headNames[j].equals("*专家名称")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												expert.setName(returnStr);
												expertTemp.setName(returnStr);
											} else {
												dataFormat++;
												data += "【专家名称】不能为空;";
											}
										} else if (headNames[j].equals("*性别")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												expertTemp.setSex(returnStr);
												if ("男".equals(returnStr)) {
													expert.setSex(1);
												} else if ("女".equals(returnStr)) {
													expert.setSex(2);
												} else {
													dataFormat++;
													data += "【性别：" + returnStr + "】不存在,只能是‘男’或者‘女’;";
												}
											} else {
												dataFormat++;
												data += "【性别】不能为空";
											}
										} else if (headNames[j].equals("*专家类型")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												expertTemp.setType(returnStr);
												if (typeList != null && typeList.size() > 0) {
													boolean isExit = false;
													for (ExpertType type : typeList) {
														if ((type.getName().replace(" ", ""))
																.equals(returnStr.replace(" ", ""))) {
															isExit = true;
															expert.setTypeId(type.getId());
															break;
														}
													}
													if (!isExit) {
														typeNoExist = true;
														data += "【专家类型：" + returnStr + "】不存在;";
													}
												} else {
													typeNoExist = true;
													data += "【专家类型：" + returnStr + "】不存在;";
												}
											} else {
												dataFormat++;
												data += "【专家类型】不能为空;";
											}
										} else if (headNames[j].equals("*联系方式")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												expertTemp.setPhone(returnStr);
												expert.setPhone(returnStr);
											} else {
												dataFormat++;
												data += "【联系方式】不能为空;";
											}
										} else if (headNames[j].equals("*所属单位")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												expert.setUnit(returnStr);
												expertTemp.setUnit(returnStr);
											} else {
												dataFormat++;
												data += "【所属单位】不能为空;";
											}
										} else if (headNames[j].equals("专业")) {
											expert.setMajor(returnStr);
											expertTemp.setMajor(returnStr);
										} else if (headNames[j].equals("主修科目")) {
											expert.setMajorSubject(returnStr);
											expertTemp.setMajorSubject(returnStr);
										} else if (headNames[j].equals("特长")) {
											expert.setSpeciality(returnStr);
											expertTemp.setSpeciality(returnStr);
										} else if (headNames[j].equals("家庭住址")) {
											expert.setAddress(returnStr);
											expertTemp.setAddress(returnStr);
										} else if (headNames[j].equals("出生日期-(yyyy/MM/dd)")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												Boolean isTrue = ImportValid.isValidDate("yyyy/MM/dd", returnStr);
												if (isTrue) {
													SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
													expert.setBirth(sdf.parse(returnStr));
													expertTemp.setBirth(sdf.parse(returnStr));
												} else {
													dataFormat++;
													data += "【出生日期】格式不对;";
												}
											}
										}
									}
								}
								flag++;
								expertList.add(expert);
								expertTempList.add(expertTemp);
								if (ImportValid.isNotEmpty(data)) {
									dataList.add(rowError + data);
								}
							}
						}
						if (dataList != null && dataList.size() > 0) {
							if (dataFormat > 0) {
								modelMap.put("errorCode", 2);
								return setSuccessModelMap(modelMap, dataList);
							}
							if (typeNoExist) {
								modelMap.put("errorCode", 1);
								modelMap.put("times", timestamp);
								// 存临时表数据
								if (expertTempList != null && expertTempList.size() > 0) {
									expertService.saveExpertTemp(expertTempList);// TODO 数据存储
									return setSuccessModelMap(modelMap, dataList);
								} else {
									throw new BusinessException("文件数据为空!");
								}
							}
						} else {
							// 存数据
							if (expertList != null && expertList.size() > 0) {
								// TODO 数据存储
								expertService.exportEXLExpert(expertList);
								return setSuccessModelMap(modelMap);
							} else {
								throw new BusinessException("文件数据为空!");
							}
						}
					}
				}
			} catch (IOException e) {
				throw new BusinessException("未知异常!");
			}
		}
		throw new BusinessException("无文件!");
	}

	/**
	 * 导入EXCEL文件 通过request.getParameter("type") 得到前台传来的类型判断
	 * 
	 * @param file
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "importExcelData")
	public ResponseEntity<ModelMap> importExcelData(@RequestParam(value = "file", required = true) MultipartFile file,
			HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) throws Exception {
		// 类型 EmergencyMaterial|应急物资
		String type = request.getParameter("type").toString();
		// 导入Excel操作
		// 导入Excel文件不为空时，将文件写入流，然后转化为工作薄
		if (file != null) {
			try {
				Workbook workBook = null;
				String fileType = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1,
						file.getOriginalFilename().length());
				if (!"xls".equals(fileType) && !"xlsx".equals(fileType)) {
					return setModelMap(modelMap, HttpCode.CONFLICT, "请选择excel格式文件！");
				}
				int header = 1;
				workBook = WorkbookFactory.create(file.getInputStream());
				if (workBook.getNumberOfSheets() > 0) {
					// 获取第一个单元薄
					Sheet sheet = workBook.getSheetAt(0);
					// 获取单元薄中表头信息,并将表头中每个单元格中信息存入数组headNames中
					List<String> allTitle = getExcelTitleBytype(type);
					Row rowHead = sheet.getRow(header);
					if (rowHead != null) {
						String[] headNames = new String[rowHead.getLastCellNum()];
						// 获取该行中总共有多少列数据row.getLastCellNum()
						for (int j = 0; j < rowHead.getLastCellNum(); j++) {
							Cell cell = rowHead.getCell(j);
							// 获取表头各个字段名称
							String returnStr = BmUtils.getCellValue(cell);
							headNames[j] = returnStr;
						}
						if (headNames != null && headNames.length > 0) {
							for (String name : headNames) {
								if (allTitle.indexOf(name) == -1) {
									throw new BusinessException("属性：" + name + "在系统中不存在,请使用模板文件！");
								}
							}
						}
						// 本次导入标志 存入临时表
						long timestamp = System.currentTimeMillis();
						// 根据类型 分流到单独吗模块的数据验证方法
						if ("EmergencyMaterial".equals(type)) {
							return VLDEmergencyMaterialExcel(sheet, headNames, modelMap, timestamp);
						}
					}
				}
			} catch (IOException e) {
				throw new BusinessException("未知异常!");
			}
		}
		throw new BusinessException("无文件!");
	}

	/***
	 * 通过type 得到应该导入的列名 EmergencyMaterial|应急物资
	 * 
	 * @param type
	 * @return
	 */
	private List<String> getExcelTitleBytype(String type) {
		List<String> title = new ArrayList<String>();
		switch (type) {
		case "EmergencyMaterial":
			title.add("*物资名称");
			title.add("*物资储备点");
			title.add("规格");
			title.add("型号");
			title.add("*物资类型");
			title.add("*存储数量");
			title.add("*存储单位");
			title.add("负责人");
			title.add("负责人电话");
			title.add("尺寸大小");
			title.add("物资用途");
			title.add("物资描述");
			title.add("是否智能推荐(是，否)");
			title.add("灾害类型-(若智能推荐选择是,则必填,可多选,中间以英文逗号隔开)");
			break;
		default:
			;
		}
		return title;
	}

	/***
	 * 得到错误信息
	 * 
	 * @param sheet     当前页数据
	 * @param headNames 列的集合
	 * @param modelMap  信息收集器
	 * @param timestamp 当前导入标识
	 * @return
	 */
	private ResponseEntity<ModelMap> VLDEmergencyMaterialExcel(Sheet sheet, String[] headNames, ModelMap modelMap,
			long timestamp) {
		List<String> errlist = new ArrayList<String>();
		// 应急物资
		List<EmergencyMaterial> dataList = new ArrayList<EmergencyMaterial>();
		List<EmergencyMaterialTemp> emergencyMaterialTempList = new ArrayList<EmergencyMaterialTemp>();
		// 应急物资类型
		List<EmergencyMaterialType> typelist = new ArrayList<EmergencyMaterialType>();
		// 消防单位
		List<ReservePoint> listrp = reservePointService.qryReservePointList();
		// 默认返回
		modelMap.put("errorCode", 0);
		// 行在第几行 默认0
		int header = 1;
		// 创建时间
		Date date = new Date();
		// 遍历单元簿中每一行，获取每一列字段，并根据表头对应赋值
		int flag = 3;
		// 必填字段 错误计数器，有一个错误就+1，如果大于0则不让导入
		int errFormat = 0;
		Boolean typeNoExist = false;
		// 循环行 开始
		String reg = "^([1-4],?)*$";
		Pattern pattern = Pattern.compile(reg);
		for (int i = header; i < sheet.getPhysicalNumberOfRows(); i++) {
			// 当前行
			Row row = sheet.getRow(i + 1);
			// 是否为空
			Boolean isRow = ImportValid.isRowEmpty(row);
			EmergencyMaterial data = null;
			EmergencyMaterialTemp expertTemp = null;
			if (!isRow) {
				String rowError = "错误： 第" + flag + "行   ";
				String errstr = "";
				// 初始化 正式数据
				data = new EmergencyMaterial();
				data.setId(IdWorker.getId());
				data.setCreateBy(WebUtil.getCurrentUser());
				data.setCreateTime(date);
				data.setUpdateTime(date);

				expertTemp = new EmergencyMaterialTemp();
				expertTemp.setId(IdWorker.getId());
				expertTemp.setCreateBy(WebUtil.getCurrentUser());
				expertTemp.setCreateTime(date);
				expertTemp.setTimestamp(timestamp);

				// 循环列开始 获取该行中总共有多少列数据row.getLastCellNum()
				for (int j = 0; j < row.getLastCellNum(); j++) {
					if(j>headNames.length-1) {
						continue ;
					}
					Cell cell = row.getCell(j);
					if (StringUtils.hasText(headNames[j])) {
						// 获取单元格内容，并根据单元格类型，进行转化
						String val = BmUtils.getCellValue(cell);
						// 列名
						String colname = headNames[j];
						// 根据列名 解析数据
						switch (colname) {
						case "*物资名称":
							if (val != null && !"".equals(val)) {
								data.setName(val);
								expertTemp.setName(val);
							} else {
								errFormat++;
								errstr += NullString(colname);
							}
							break;
						case "*物资储备点":
							if (val != null && !val.equals("")) {
								if (listrp != null && listrp.size() > 0) {
									boolean isExit = false;
									for (ReservePoint point : listrp) {
										if ((point.getName().replace(" ", "")).equals(val.replace(" ", ""))) {
											isExit = true;
											data.setReservePointId(point.getId());
											expertTemp.setReservePointId(point.getId() + "");
											break;
										}
									}
									if (!isExit) {
										errFormat++;
										errstr += NotfindString(colname, val);
									}
								} else {
									errFormat++;
									errstr += NotfindString(colname, val);
								}
							} else {
								errFormat++;
								errstr += NullString(colname);
							}
							break;
						case "规格":
							data.setSpecifications(val);
							expertTemp.setSpecifications(val);
							break;
						case "型号":
							data.setModel(val);
							expertTemp.setModel(val);
							break;
						case "*物资类型":
							if (val != null && !"".equals(val)) {
								Map<String, Object> msq = new HashMap<>();
								msq.put("name", val);
								typelist = emergencySuppliesTypeService.qryListByParams(msq);
								expertTemp.setTypeId(val);
								if (typelist.size() >= 1) {
									data.setTypeId(typelist.get(0).getId_());
								} else {
									typeNoExist = true;
									errstr += NotfindString(colname, val);
								}
							} else {
								errFormat++;
								errstr += NullString(colname);
							}
							break;
						case "*存储数量":
							if (val != null && !"".equals(val)) {
								if (isInteger(val)) {
									data.setStorageQuantity(Integer.parseInt(val));
									expertTemp.setStorageQuantity(val);
								} else {
									errFormat++;
									errstr += IntString(colname);
								}
							} else {
								errFormat++;
								errstr += NullString(colname);
							}
							break;
						case "*存储单位":
							data.setStorageUnit(val);
							expertTemp.setStorageUnit(val);
							break;
						case "负责人":
							data.setChargePerson(val);
							expertTemp.setChargePerson(val);
							break;
						case "负责人电话":
							data.setPhone(val);
							expertTemp.setPhone(val);
							break;
						case "尺寸大小":
							data.setSize(val);
							expertTemp.setSize(val);
							break;
						case "物资用途":
							data.setPurpose(val);
							expertTemp.setPurpose(val);
							break;
						case "物资描述":
							data.setRemark(val);
							expertTemp.setRemark(val);
							break;

						case "是否智能推荐(是，否)":
							if (val != null && !val.equals("")) {
								if (val.trim().equals("是")) {
									data.setIntelligentRecommendation(1);
									expertTemp.setIntelligentRecommendation(1);
								} else if (val.trim().equals("否")) {
									data.setIntelligentRecommendation(0);
									expertTemp.setIntelligentRecommendation(0);
								} else {
									errFormat++;
									errstr += "列【是否智能推荐(是，否)】只能填是或否;";
								}

							}
							break;
						case "灾害类型-(若智能推荐选择是,则必填,可多选,中间以英文逗号隔开)":
							if (data.getIntelligentRecommendation() != null
									&& data.getIntelligentRecommendation() == 1) {
								if (val != null && !val.equals("")) {
									Matcher isNum = pattern.matcher(val);
									if (isNum.matches()) {
										String[] types = val.split(",");
										if (types != null && types.length > 0) {
											Set<String> disasterTypeSet = new HashSet<>();
											for (String type : types) {
												if (type.equals("1")) {
													disasterTypeSet.add("废油池流淌火");
												}else if (type.equals("2")) {
													disasterTypeSet.add("油罐火灾");
												}else if (type.equals("3")) {
													disasterTypeSet.add("气体泄漏");
												}else if (type.equals("4")) {
													disasterTypeSet.add("爆炸");
												} else {
													errFormat++;
													errstr += "列【灾害类型】上传格式不正确";
													break;
												}
											}
											String disasterType = JSONObject.toJSONString(disasterTypeSet);
											data.setDisasterType(disasterType);
											expertTemp.setDisasterType(disasterType);
										}
									} else {
										errFormat++;
										errstr += "列【灾害类型】上传格式不正确";
									}

								} else {
									errFormat++;
									errstr += NullString(colname);
								}
							}
							break;
						default:
							;
						}
					}
				} // ---循环列结束
				flag++;
				if (errstr != null && !"".equals(errstr)) {
					errlist.add(rowError + errstr);
				}
				dataList.add(data);
				emergencyMaterialTempList.add(expertTemp);
			} // ---行存在结束
		} // ---循环行结束
			// 存储 如果没有错误 直接保存成功， 如果有错误， 提示是否继续，再从临时表存入正式表
		if (errlist != null && errlist.size() > 0) {
			// 必填字段发生错误
			if (errFormat > 0) {
				modelMap.put("errorCode", 2);
				return setSuccessModelMap(modelMap, errlist);
			}
			// 必填字段没有错误 则存入临时表 并返回前台提示
			if (typeNoExist) {
				modelMap.put("errorCode", 1);
				modelMap.put("times", timestamp);
				// 存临时表数据
				if (emergencyMaterialTempList != null && emergencyMaterialTempList.size() > 0) {
					emergencyMaterialService.exportEXLEmergencyMaterialTemp(emergencyMaterialTempList);// TODO 数据存储
					return setSuccessModelMap(modelMap, errlist);
				} else {
					throw new BusinessException("文件数据为空!");
				}
			}
		}
		// 如果没有错误
		else {
			// 存数据
			if (dataList != null && dataList.size() > 0) {
				// TODO 数据存储
				emergencyMaterialService.exportEXLEmergencyMaterial(dataList);
				return setSuccessModelMap(modelMap);
			} else {
				throw new BusinessException("文件数据为空!");
			}
		}

		return setSuccessModelMap(modelMap);
	}

	/***
	 * 判断是否为整数
	 * 
	 * @param str 传入的字符串
	 * @return 是整数返回true,否则返回false
	 */
	public static boolean isInteger(String str) {
		Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
		return pattern.matcher(str).matches();
	}

	/***
	 * 如果为空的提示 用于统一修改 如str不能为空
	 * 
	 * @param str 列名
	 */
	public static String NullString(String str) {
		return "列【" + str + "】不能为空;";
	}

	/***
	 * 不为数组的提示 用于统一修改 如str必须为数字
	 * 
	 * @param str 列名
	 */
	public static String IntString(String str) {
		return "列【" + str + "】必须为数字;";
	}

	/***
	 * 找不到对应值的提示 用于统一修改 如未找到str:val
	 * 
	 * @param str 列名
	 * @param val 值
	 */
	public static String NotfindString(String str, String val) {
		return "【" + str + ":" + val + "】在系统中不存在," + "请添加;";
	}

	/**
	 * 公式导出
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "downLoadExprEXL")
	public String downLoadExprEXL(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// 取出带查询条件的所有数据allList
		String p = request.getParameter("param").toString();
		String param = new String(p.getBytes("ISO8859-1"), "UTF-8");
		@SuppressWarnings("unchecked")
		Map<String, Object> params = JSON.parseObject(param, Map.class);
		List<Expr> allList = exprService.queryListNoPage(params);
		if (allList != null && allList.size() > 0) {
			// 循环sheet
			DownloadExcelUtil dExlUtile = null;
			int dataNum = 1;
			for (Expr item : allList) {
				// 设置对象：title，value ，List<entity>
				StringBuffer sb = new StringBuffer();
				sb.append("公式名称_");
				sb.append(item.getName() != null ? item.getName() : "-");
				sb.append("$");
				sb.append("公式_");
				sb.append(item.getExpr() != null ? item.getExpr() : "-");
				sb.append("$");
				sb.append("公式描述_");
				sb.append(item.getDescribtion() != null ? item.getDescribtion() : "-");
				sb.append("$");
				sb.append("单位_");
				sb.append(item.getUnit() != null ? item.getUnit() : "-");
				sb.append("$");
				sb.append("取整方式_");
				sb.append((item.getDressingMethod() != null && item.getDressingMethod() == 1) ? "四舍五入"
						: item.getDressingMethod() == 2 ? "向上取整" : item.getDressingMethod() == 3 ? "向下取整" : "-");
				sb.append("$");
				sb.append("小数点位数_");
				sb.append(item.getScale() != null ? item.getScale() : "-");
				String tvsb = sb.toString();
				String[] tvsbArray = tvsb.split("\\$");
				List<ExlVo> entityList = new ArrayList<ExlVo>();
				if (tvsbArray.length > 0) {
					for (String tv : tvsbArray) {
						ExlVo entity = new ExlVo();
						String[] tvArray = tv.split("_");
						entity.setTitle(tvArray[0]);
						entity.setValue(tvArray.length == 1 ? "-" : tvArray[1]);
						entityList.add(entity);
					}
				}
				if (dataNum == 1) {// 若为第一次循环
					// 在response中写入exl。传入当前type的name，和文件名：DownloadExcelUtil
					dExlUtile = new DownloadExcelUtil(response, "公式列表", "公式列表");
					// 把list<entity>中的title按list的顺序放入String【】，调用：setExcelListTitle
					String[] titleArray = new String[entityList.size()];
					for (int i = 0; i < entityList.size(); i++) {
						titleArray[i] = entityList.get(i).getTitle();
					}
					dExlUtile.addRow(titleArray, BorderLineStyle.THIN, jxl.format.Alignment.CENTRE, "bold");
					// dExlUtile.setExcelListTitle(titleArray);
					// 把list<entity>中的value按list的顺序放入String【】，调用：addRow
					String[] valueArray = new String[entityList.size()];
					for (int i = 0; i < entityList.size(); i++) {
						valueArray[i] = entityList.get(i).getValue();
					}
					dExlUtile.addRow(valueArray, BorderLineStyle.THIN, jxl.format.Alignment.CENTRE, "");
				} else {
					// 把list<entity>中的value按list的顺序放入String【】，调用：addRow
					String[] valueArray = new String[entityList.size()];
					for (int i = 0; i < entityList.size(); i++) {
						valueArray[i] = entityList.get(i).getValue();
					}
					dExlUtile.addRow(valueArray, BorderLineStyle.THIN, jxl.format.Alignment.CENTRE, "");
				}
				dataNum++;
			}
			dExlUtile.reportExcel();

		} else {
			// 查询结果为空
		}
		return null;
	}

}
