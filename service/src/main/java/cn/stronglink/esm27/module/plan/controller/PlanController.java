package cn.stronglink.esm27.module.plan.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.module.plan.service.PlanService;
import cn.stronglink.esm27.module.plan.vo.PlanVo;


@Controller
@RequestMapping(value = "plan")
public class PlanController extends AbstractController  {
	
	@Autowired
	private PlanService planService;
	
	/**
	 * 查询列表
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		Page<PlanVo> page = (Page<PlanVo>) super.getPage(params);
		Page<PlanVo> data = planService.qryListByParams(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 通过消防队查询预案
	 */
	@RequestMapping(value = "qryPlansByBrigade")
	public ResponseEntity<ModelMap> qryListByType(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long keyUnitId) {
		List<PlanVo> data = planService.qryPlansByBrigade(keyUnitId);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 通过重点单位查询预案
	 */
	@RequestMapping(value = "qryPlansByKeyUnit")
	public ResponseEntity<ModelMap> qryPlansByKeyUnit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long keyUnitId) {
		List<PlanVo> data = planService.qryPlansByKeyUnit(keyUnitId);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 通过重点单位查询预案
	 */
	@RequestMapping(value = "qryPlansByKeyParts")
	public ResponseEntity<ModelMap> qryPlansByKeyParts(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long keyUnitId) {
		List<PlanVo> data = planService.qryPlansByKeyParts(keyUnitId);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 通过重点部位查询预案
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "qryListByKeyParts")
	public ResponseEntity<ModelMap> qryListByKeyParts(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		Page<PlanVo> page = (Page<PlanVo>) super.getPage(params);
		Page<PlanVo> data = planService.qryListByKeyParts(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 通过id查询预案详情
	 */
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> qryUserById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Map<String, Object> params) {
		PlanVo vo = planService.qryById(params);
		return setSuccessModelMap(modelMap, vo);
	}
	
	/**
	 * 通过id查看
	 */
	@RequestMapping(value = "getPlansInfo")
	public ResponseEntity<ModelMap> getPlansInfo(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		PlanVo vo = planService.getPlansInfo(id);
		return setSuccessModelMap(modelMap, vo);
	}
	
	/**
	 * 新建预案
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param entity
	 * @return
	 */
	@RequestMapping(value = "create")
	@OperateLog(module = "预案管理",desc="添加预案", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody PlanVo entity) {
		planService.createPlan(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 修改预案
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param entity
	 * @return
	 */
	@RequestMapping(value = "edit")
	@OperateLog(module = "预案管理",desc="修改预案", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody PlanVo entity) {
		planService.updatePlans(entity);
		return setSuccessModelMap(modelMap, null);
	}
	/**
	 * 更改预案次数
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "updateFreq")
	public ResponseEntity<ModelMap> updateFreq(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		planService.updateFreq(id);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 删除预案
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "remove")
	@OperateLog(module = "预案管理",desc="删除预案", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		PlanVo planVo = planService.qryPlanById(id);
		if(planVo!=null && planVo.getKeyUnitId()!=null && planVo.getKeyUnitId()!=0) {
			throw new BusinessException("该预案正在使用中，无法删除！");
		}
		planService.removePlans(id);
		return setSuccessModelMap(modelMap, null);
	}
	
	@RequestMapping(value = "updateKeyUnit")
	public ResponseEntity<ModelMap> updateKeyUnit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody  PlanVo planVo) {
		planService.updateKeyUnit(planVo);
		return setSuccessModelMap(modelMap, null);
	}
	
	@RequestMapping(value = "delKeyUnitByPlanId")
	public ResponseEntity<ModelMap> delKeyUnitByPlanId(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody  Long id) {
		planService.delKeyUnitByPlanId(id);
		return setSuccessModelMap(modelMap, null);
	}

	
	@RequestMapping(value = "qryNotAddedPlanList")
	public ResponseEntity<ModelMap> qryNotAddedPlanList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<PlanVo> page = (Page<PlanVo>) super.getPage(params);
		Page<PlanVo> data = planService.qryNotAddedPlanList(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	

}
