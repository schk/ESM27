package cn.stronglink.esm27.module.unit.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.AntdFile;
import cn.stronglink.core.util.UploadEntity;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.FireBrigade;
import cn.stronglink.esm27.entity.KeyUnit;
import cn.stronglink.esm27.entity.KeyUnitAccident;
import cn.stronglink.esm27.entity.KeyUnitDangers;
import cn.stronglink.esm27.entity.KeyUnitDangersCatalog;
import cn.stronglink.esm27.entity.KeyUnitDangersRef;
import cn.stronglink.esm27.entity.KeyUnitImg;
import cn.stronglink.esm27.module.emergencyResources.fireBrigade.mapper.FireBrigadeMapper;
import cn.stronglink.esm27.module.plan.mapper.PlanMapper;
import cn.stronglink.esm27.module.unit.mapper.KeyUnitDangersMapper;
import cn.stronglink.esm27.module.unit.mapper.KeyUnitImgMapper;
import cn.stronglink.esm27.module.unit.mapper.KeyUnitMapper;
import cn.stronglink.esm27.module.unit.vo.KeyUnitDangersVo;

@Service
@Transactional(rollbackFor = Exception.class)
public class KeyUnitService {

	@Autowired
	private KeyUnitMapper keyUnitMapper;
	@Autowired
	private FireBrigadeMapper fireBrigadeMapper;
	@Autowired
	private KeyUnitDangersMapper keyUnitDangersMapper;
	@Autowired
	private KeyUnitImgMapper keyUnitImgMapper;
	@Autowired
	private PlanMapper planMapper;

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Page<KeyUnit> getListByParams(Page<KeyUnit> page, Map<String, Object> params) {
		List<KeyUnit> listByParams2 = keyUnitMapper.getListByParams(page, params);
		page.setRecords(listByParams2);
		return page;
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public KeyUnit qryById(Long id) {
		KeyUnit keyUnit = keyUnitMapper.qryById(id);
		List<KeyUnitDangers> dangersList = keyUnitDangersMapper.qryDangersById(id);
		if (dangersList != null && dangersList.size() > 0) {
			for (KeyUnitDangers dangers : dangersList) {
				List<KeyUnitDangersCatalog> keyUnitDangersCatalogs = keyUnitDangersMapper
						.getKeyUnitDangersCatalogs(dangers.getId());
				dangers.setKeyUnitDangersCatalogs(keyUnitDangersCatalogs);
			}
		}
		keyUnit.setDangersList(dangersList);
		List<UploadEntity> imgList = keyUnitImgMapper.getImgList(id);
		keyUnit.setFileList(imgList);
		return keyUnit;
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public KeyUnit getkeyUnitInfo(Long id) {
		KeyUnit keyUnit = keyUnitMapper.qryById(id);
		List<KeyUnitDangers> dangersList = keyUnitDangersMapper.qryDangersById(id);
		if (dangersList != null && dangersList.size() > 0) {
			for (KeyUnitDangers dangers : dangersList) {
				List<KeyUnitDangersCatalog> keyUnitDangersCatalogs = keyUnitDangersMapper
						.getKeyUnitDangersCatalogs(dangers.getId());
				dangers.setKeyUnitDangersCatalogs(keyUnitDangersCatalogs);
			}
		}
		keyUnit.setDangersList(dangersList);
		List<UploadEntity> imgList = keyUnitImgMapper.getImgList(id);
		keyUnit.setFileList(imgList);
		if (keyUnit.getFireBrigadeId() != null) {
			FireBrigade selectById2 = fireBrigadeMapper.selectById(keyUnit.getFireBrigadeId());
			keyUnit.setFireBrigadeId(selectById2.getName());
			if (keyUnit.getLon() != null && keyUnit.getLat() != null) {
				keyUnit.setLonLat(keyUnit.getLon() + "," + keyUnit.getLat());
			}
		}
		List<AntdFile> fileList = keyUnitMapper.qryAccidentByUnitId(id);
		keyUnit.setAccidentFileList(fileList);
		return keyUnit;
	}

	public void insert(KeyUnit entity) {
		if (!"".equals(entity.getLonLat())) {
			String[] lonLat = entity.getLonLat().split(",");
			entity.setLon(Double.valueOf(lonLat[0]));
			entity.setLat(Double.valueOf(lonLat[1]));
		}
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		keyUnitMapper.insert(entity);

		if (entity.getDangersList().size() > 0) {
			List<KeyUnitDangersCatalog> keyUnitDangersCatalogs = new ArrayList<KeyUnitDangersCatalog>();
			for (KeyUnitDangers dangers : entity.getDangersList()) {
				dangers.setId(IdWorker.getId());
				dangers.setKeyUnitId(entity.getId());
				dangers.setCreateBy(WebUtil.getCurrentUser());
				dangers.setCreateTime(new Date());
				dangers.setUpdateTime(new Date());
				dangers.setUpdateBy(WebUtil.getCurrentUser());
				if (!"".equals(dangers.getMsdsPath()) && dangers.getMsdsPath() != null) {
					String swfPath = dangers.getMsdsPath().split("\\	.")[0] + ".swf";
					dangers.setSwfPath(swfPath);
					if (dangers.getKeyUnitDangersCatalogs() != null && dangers.getKeyUnitDangersCatalogs().size() > 0) {
						for (KeyUnitDangersCatalog catalog : dangers.getKeyUnitDangersCatalogs()) {
							catalog.setId(IdWorker.getId());
							catalog.setKeyUnitDangersId(dangers.getId());
							catalog.setCreateBy(WebUtil.getCurrentUser());
							catalog.setCreateTime(new Date());
							keyUnitDangersCatalogs.add(catalog);
						}
					}
				}
				keyUnitDangersMapper.create(dangers);
			}
			if (keyUnitDangersCatalogs != null && keyUnitDangersCatalogs.size() > 0) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("keyUnitDangersCatalogs", keyUnitDangersCatalogs);
				keyUnitDangersMapper.insertKeyUnitDangersCatalogs(map);
			}
		}
		if (entity.getImgList().size() > 0) {
			for (String img : entity.getImgList()) {
				KeyUnitImg imgPath = new KeyUnitImg();
				imgPath.setId(IdWorker.getId());
				imgPath.setKeyUnitId(entity.getId());
				imgPath.setPath(img);
				imgPath.setCreateBy(WebUtil.getCurrentUser());
				imgPath.setCreateTime(new Date());
				imgPath.setUpdateTime(new Date());
				imgPath.setUpdateBy(WebUtil.getCurrentUser());
				keyUnitImgMapper.insert(imgPath);
			}
		}
	}

	public void update(KeyUnit entity) {
		if (!"".equals(entity.getLonLat())) {
			String[] lonLat = entity.getLonLat().split(",");
			entity.setLon(Double.valueOf(lonLat[0]));
			entity.setLat(Double.valueOf(lonLat[1]));
		}
		HashMap<String, Object> columnMap = new HashMap<String, Object>();
		columnMap.put("key_unit_id", entity.getId());
		keyUnitDangersMapper.deleteByMap(columnMap);
		keyUnitDangersMapper.deleteKeyUnitDangersCatalogs(columnMap);
		keyUnitImgMapper.deleteByMap(columnMap);
		if (entity.getDangersList().size() > 0) {
			List<KeyUnitDangersCatalog> keyUnitDangersCatalogs = new ArrayList<KeyUnitDangersCatalog>();
			for (KeyUnitDangers dangers : entity.getDangersList()) {
				dangers.setId(IdWorker.getId());
				dangers.setKeyUnitId(entity.getId());
				dangers.setCreateBy(WebUtil.getCurrentUser());
				dangers.setCreateTime(new Date());
				dangers.setUpdateTime(new Date());
				dangers.setUpdateBy(WebUtil.getCurrentUser());
				if (!"".equals(dangers.getMsdsPath()) && dangers.getMsdsPath() != null) {
					String swfPath = dangers.getMsdsPath().split("\\.")[0] + ".swf";
					dangers.setSwfPath(swfPath);
					if (dangers.getKeyUnitDangersCatalogs() != null && dangers.getKeyUnitDangersCatalogs().size() > 0) {
						for (KeyUnitDangersCatalog catalog : dangers.getKeyUnitDangersCatalogs()) {
							catalog.setId(IdWorker.getId());
							catalog.setKeyUnitDangersId(dangers.getId());
							catalog.setCreateBy(WebUtil.getCurrentUser());
							catalog.setCreateTime(new Date());
							keyUnitDangersCatalogs.add(catalog);
						}
					}
				}
				keyUnitDangersMapper.create(dangers);
			}
			if (keyUnitDangersCatalogs != null && keyUnitDangersCatalogs.size() > 0) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("keyUnitDangersCatalogs", keyUnitDangersCatalogs);
				keyUnitDangersMapper.insertKeyUnitDangersCatalogs(map);
			}
		}
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if (keyUnitMapper.updateById(entity) == 0) {
			throw new BusinessException("更新失败!");
		}
		if (entity.getImgList().size() > 0) {
			for (String img : entity.getImgList()) {
				KeyUnitImg imgPath = new KeyUnitImg();
				imgPath.setId(IdWorker.getId());
				imgPath.setKeyUnitId(entity.getId());
				imgPath.setPath(img);
				imgPath.setCreateBy(WebUtil.getCurrentUser());
				imgPath.setCreateTime(new Date());
				imgPath.setUpdateTime(new Date());
				imgPath.setUpdateBy(WebUtil.getCurrentUser());
				keyUnitImgMapper.insert(imgPath);
			}
		}
	}

	public void remove(Long id) {
		keyUnitMapper.deleteById(id);
		// 删除应急预案
		planMapper.updateKeyUnitById(id);
	}

	public List<KeyUnit> qryKeyUnitList(Map<String, Object> params) {
		return keyUnitMapper.qryKeyUnitList(params);
	}

	public KeyUnit getkeyUnitName(Long id) {
		return keyUnitMapper.getkeyUnitName(id);
	}

	public Integer getCountByUsername(KeyUnit entity) {
		return keyUnitMapper.getCountByUsername(entity);
	}

	public Long findBrigadeIdByName(String brigadeName) {
		return fireBrigadeMapper.findBrigadeIdByName(brigadeName);
	}

	public void batchInsert(List<KeyUnit> objList) {
		keyUnitMapper.batchInsertObj(objList);
		// keyUnitImgMapper.batchInsertObj(imgList);
	}

	public KeyUnit qryAccidentByUnitId(Long id) {
		KeyUnit keyUnit = keyUnitMapper.qryById(id);
		if (keyUnit != null) {
			List<AntdFile> fileList = keyUnitMapper.qryAccidentByUnitId(id);
			keyUnit.setAccidentFileList(fileList);
		}
		return keyUnit;
	}

	public void addKeyUnitAccident(KeyUnit entity) {
		keyUnitMapper.deleteAccidentInfo(entity.getId());
		Date date = new Date();
		if (entity.getAccidentFileList() != null && entity.getAccidentFileList().size() > 0) {
			List<KeyUnitAccident> fileList = new ArrayList<KeyUnitAccident>();
			for (AntdFile doc : entity.getAccidentFileList()) {
				KeyUnitAccident file = new KeyUnitAccident();
				file.setId(IdWorker.getId());
				file.setFileName(doc.getName());
				file.setPath(doc.getUrl());
				file.setSwfPath(doc.getSwfUrl());
				file.setKeyUnitId(entity.getId());
				file.setCreateTime(date);
				fileList.add(file);
			}
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("files", fileList);
			keyUnitMapper.inserKeyUnitAccident(map);
		}

	}

	public void updateLocation(KeyUnit entity) {
		if (!"".equals(entity.getLonLat())) {
			String[] lonLat = entity.getLonLat().split(",");
			entity.setLon(Double.valueOf(lonLat[0]));
			entity.setLat(Double.valueOf(lonLat[1]));
		}
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if (keyUnitMapper.updateById(entity) == 0) {
			throw new BusinessException("更新失败!");
		}

	}

	public void saveDangersOfKeyUnit(KeyUnitDangersVo keyUnit) {
		if (keyUnit.getdList() != null && keyUnit.getdList().size() > 0) {
			// 删除重点部位的危化品
			keyUnitMapper.delDangersByKeyUnit(keyUnit.getId());
			for (KeyUnitDangers vo : keyUnit.getdList()) {
				KeyUnitDangersRef d = new KeyUnitDangersRef();
				d.setId(IdWorker.getId());
				d.setKeyUnitId(keyUnit.getId());
				d.setDangersId(vo.getId());
				d.setCreateBy(WebUtil.getCurrentUser());
				d.setCreateTime(new Date());
				keyUnitMapper.insertKeyUnitDangersRef(d);
			}
		}
	}

	public void delDangers(Long id) {
		keyUnitMapper.delDangers(id);
	}
}
