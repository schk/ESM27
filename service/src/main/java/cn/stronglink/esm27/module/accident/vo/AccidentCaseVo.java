package cn.stronglink.esm27.module.accident.vo;

import cn.stronglink.esm27.entity.EmergencyMaterial;

public class AccidentCaseVo extends EmergencyMaterial {

	/**
	 * 
	 */
	private static final long serialVersionUID = -294625263411637837L;
	
	private String reservePointName;
	
	private String keyUnitName;
	private String typeName;
	private int type;
	private Long keyUnitId;
	private Long fireBrigadeId;

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getReservePointName() {
		return reservePointName;
	}

	public void setReservePointName(String reservePointName) {
		this.reservePointName = reservePointName;
	}

	public String getKeyUnitName() {
		return keyUnitName;
	}

	public void setKeyUnitName(String keyUnitName) {
		this.keyUnitName = keyUnitName;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Long getKeyUnitId() {
		return keyUnitId;
	}

	public void setKeyUnitId(Long keyUnitId) {
		this.keyUnitId = keyUnitId;
	}

	public Long getFireBrigadeId() {
		return fireBrigadeId;
	}

	public void setFireBrigadeId(Long fireBrigadeId) {
		this.fireBrigadeId = fireBrigadeId;
	}
	
	

}
