package cn.stronglink.esm27.module.dutyequipuserecord.mapper;

import cn.stronglink.esm27.entity.DutyEquipUseRecord;
import cn.stronglink.esm27.entity.DutyEquipUseRecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DutyEquipUseRecordMapper {
    int countByExample(DutyEquipUseRecordExample example);

    int deleteByExample(DutyEquipUseRecordExample example);

    int deleteByPrimaryKey(Long id);

    int insert(DutyEquipUseRecord record);

    int insertSelective(DutyEquipUseRecord record);

    List<DutyEquipUseRecord> selectByExample(DutyEquipUseRecordExample example);

    DutyEquipUseRecord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") DutyEquipUseRecord record, @Param("example") DutyEquipUseRecordExample example);

    int updateByExample(@Param("record") DutyEquipUseRecord record, @Param("example") DutyEquipUseRecordExample example);

    int updateByPrimaryKeySelective(DutyEquipUseRecord record);

    int updateByPrimaryKey(DutyEquipUseRecord record);
}