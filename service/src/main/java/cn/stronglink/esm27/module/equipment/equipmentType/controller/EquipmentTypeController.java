package cn.stronglink.esm27.module.equipment.equipmentType.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.EquipmentType;
import cn.stronglink.esm27.module.equipment.equipmentType.service.EquipmentTypeService;
@Controller
@RequestMapping(value = "equipmentType")
public class EquipmentTypeController extends AbstractController {

	@Autowired
	private EquipmentTypeService typeService;

	/**
	 * 查询带最顶部的组织机构
	 * @param modelMap
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "qryEquipmentTypeList")
	public ResponseEntity<ModelMap> qryEquipmentTypeList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody int type) {
		List<EquipmentType> list = typeService.qryEquipmentTypeList(type);
		return setSuccessModelMap(modelMap, list);
	}
	
	
	
	/**
	 * 查询
	 * @param modelMap
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "qryEquTypeList")
	public ResponseEntity<ModelMap> qryEquTypeList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody List<String> types) {
		List<EquipmentType> list = typeService.qryEquTypeList(types);
		return setSuccessModelMap(modelMap, list);
	}

	
	@RequestMapping(value = "qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<EquipmentType> page = (Page<EquipmentType>) super.getPage(params);
		Page<EquipmentType> data = typeService.getListByParams(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		EquipmentType type =typeService.selectById(id);
		return setSuccessModelMap(modelMap, type);
	}
	
	@OperateLog(module = "生产装置类型",desc="添加生产装置类型", type = OpType.ADD)
	@RequestMapping(value = "create")
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody EquipmentType entity) {
		typeService.insert(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	@OperateLog(module = "生产装置类型",desc="修改生产装置类型", type = OpType.UPDATE)
	@RequestMapping(value = "edit")
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody EquipmentType entity) {
		typeService.update(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	@OperateLog(module = "生产装置类型",desc="删除生产装置类型", type = OpType.DEL)
	@RequestMapping(value = "remove")
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		typeService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}

}
