package cn.stronglink.esm27.module.equipment.equipment.vo;

import cn.stronglink.esm27.entity.Equipment;

public class EquipmentDocVo extends Equipment {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1440274913618947431L;
	
	private String docName;
	
	private String path;
	
	private String swfPath;
	public String getSwfPath() {
		return swfPath;
	}

	public void setSwfPath(String swfPath) {
		this.swfPath = swfPath;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public String toString() {
		return "EquipmentDocVo [docName=" + docName + ", path=" + path + "]";
	}
	
	

}
