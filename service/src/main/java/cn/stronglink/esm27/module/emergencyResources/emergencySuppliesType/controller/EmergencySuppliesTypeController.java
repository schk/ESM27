package cn.stronglink.esm27.module.emergencyResources.emergencySuppliesType.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.EmergencyMaterialType;
import cn.stronglink.esm27.module.emergencyResources.emergencySuppliesType.service.EmergencySuppliesTypeService;

@Controller
@RequestMapping("emergencySuppliesType")
public class EmergencySuppliesTypeController extends AbstractController {
	
	@Autowired
	private EmergencySuppliesTypeService emergencySuppliesTypeService;
	
	  
	/**
	 * 查询列表  
	 */
	@RequestMapping(value = "qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		List<EmergencyMaterialType> data =  emergencySuppliesTypeService.qryListByParams(params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 通过id查询信息
	 */
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> qryUserById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		EmergencyMaterialType vo = emergencySuppliesTypeService.selectById(id);
		return setSuccessModelMap(modelMap, vo);
	}
	/**
	 * 新建信息
	 */
	@RequestMapping(value = "create")
	@OperateLog(module = "应急物资类型管理",desc="添加应急物资类型", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody EmergencyMaterialType entity) {
		emergencySuppliesTypeService.insert(entity);
		return setSuccessModelMap(modelMap, null);
	}
	/**
	 * 修改信息
	 */
	@RequestMapping(value = "edit")
	@OperateLog(module = "应急物资类型管理",desc="修改应急物资类型", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody EmergencyMaterialType entity) {
		emergencySuppliesTypeService.update(entity);
		return setSuccessModelMap(modelMap, null);
	}
	/**
	 * 删除信息
	 */
	@RequestMapping(value = "remove")
	@OperateLog(module = "应急物资类型管理",desc="删除应急物资类型", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		emergencySuppliesTypeService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}

}
