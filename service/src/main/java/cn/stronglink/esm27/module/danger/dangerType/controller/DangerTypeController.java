package cn.stronglink.esm27.module.danger.dangerType.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.config.Resources;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.esm27.entity.DangerType;
import cn.stronglink.esm27.module.danger.dangerType.service.DangerTypeService;
import cn.stronglink.esm27.module.danger.dangerType.vo.DangerTypeTreeNodeVo;
@Controller
@RequestMapping(value = "dangersType")
public class DangerTypeController extends AbstractController {

	@Autowired
	private DangerTypeService dangerTypeService;

	/**
	 * 查询带最顶部的危化品类型
	 * @param modelMap
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "qryDangerType")
	public ResponseEntity<ModelMap> qryDepts(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<DangerTypeTreeNodeVo> vo = dangerTypeService.getTree();
		return setSuccessModelMap(modelMap, vo);
	}

	/**
	 * 查询不带顶部的组织机构
	 * @param modelMap
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "qryDangerTypeNoTop")
	public ResponseEntity<ModelMap> qryDeptsAllTree(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<DangerTypeTreeNodeVo> vo = dangerTypeService.qryDangerTypeNoTop();
		return setSuccessModelMap(modelMap, vo);
	}
	
	
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		DangerType entity =dangerTypeService.selectById(id);
		return setSuccessModelMap(modelMap, entity);
	}
	
	@OperateLog(module = "危化品类型",desc="添加危化品类型", type = OpType.ADD)
	@RequestMapping(value = "create")
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody DangerType entity) {
		dangerTypeService.insert(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	@OperateLog(module = "危化品类型",desc="修改危化品类型", type = OpType.UPDATE)
	@RequestMapping(value = "edit")
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody DangerType entity) {
		dangerTypeService.update(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	@OperateLog(module = "危化品类型",desc="删除危化品类型", type = OpType.DEL)
	@RequestMapping(value = "remove")
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		
		int count = dangerTypeService.getChildDetpCount(id);
		if(count > 0){		
			return setModelMap(modelMap, HttpCode.CONFLICT, Resources.getMessage("HAVECHILDDEPT"));
		}
		List<DangerType> getdangerTypeList = dangerTypeService.getdangerTypeList(id);
		if(getdangerTypeList.size()>0){
			return setModelMap(modelMap, HttpCode.CONFLICT,"此类型正在使用，暂时不能删除！");
		}
		dangerTypeService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}

}
