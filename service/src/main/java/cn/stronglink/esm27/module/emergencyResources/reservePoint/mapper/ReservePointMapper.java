package cn.stronglink.esm27.module.emergencyResources.reservePoint.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.esm27.entity.ReservePoint;
import cn.stronglink.esm27.module.emergencyResources.reservePoint.vo.ReservePointVo;

public interface ReservePointMapper extends BaseMapper<ReservePoint> {
	
	//分页查询
	List<ReservePointVo> qryListByParams(Pagination page, Map<String, Object> params);
	
	List<ReservePointVo> qryListByQueryParams( Map<String, Object> params);
	
	List<ReservePoint> qryReservePointList();
	
	List<ReservePoint> qryReserveByType(Map<String, Object> params);

	List<ReservePointVo> qryListForUnit(Page<ReservePointVo> page, Map<String, Object> params);

	List<ReservePointVo> qryListForBrigade(Page<ReservePointVo> page, Map<String, Object> params);

	ReservePointVo qryById(Long id);

	int batchInsert(@Param("interimList") List<ReservePoint> interimList);

	List<ReservePointVo> qryListForWu(Page<ReservePointVo> page, Map<String, Object> params);

	ReservePointVo qryWithKeyUnit(Long reservePointId);

	ReservePointVo qryWithFireBrigade(Long reservePointId);
}
