package cn.stronglink.esm27.module.extinguisher.mapper;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.esm27.entity.ExtinguisheSum;
import cn.stronglink.esm27.entity.Extinguisher;
import cn.stronglink.esm27.entity.ExtinguisherTemp;
import cn.stronglink.esm27.module.extinguisher.vo.ExtinguisherVo;
import cn.stronglink.esm27.module.extinguisherUseRecord.param.SearchParam;

public interface ExtinguisherMapper extends BaseMapper<Extinguisher> {
	
	List<ExtinguisherVo> qryListByParams(Pagination page, Map<String, Object> params);
	
	Extinguisher getQuantity(@Param("fireBrigadeId") Long fireBrigadeId, @Param("typeId") Long typeId);

	List<ExtinguisherVo> qryListByFireBrigade(Map<String, Object> params);

	ExtinguisherVo qryExtinguisherQu(Map<String, Object> params);
	
	//根据id查询信息
	List<ExtinguisheSum> fireBrigadeName(@Param("typeId") Long typeId);

	List<Extinguisher> getExtinguisherStock(SearchParam params);

	BigDecimal qryExtinguishere(Map<String, Object> params);

	ExtinguisherVo qryById(@Param("id") Long id);

	BigDecimal getExtinguisherUseRecord(SearchParam params);

	BigDecimal getFireEngine(SearchParam params);

	int batchInsert(@Param("interimList") List<Extinguisher> interimList);

	int batchInsertTemp(@Param("interimList") List<ExtinguisherTemp> interimList);

	List<ExtinguisherTemp> qryExcelTemp(@Param("timestamp") Long timestamp);

	void delExcelTemp(@Param("timestamp") Long timestamp);

	Extinguisher getExtinguisherQuantity(Long id);

	void updateQuantityById(@Param("foamTypeId") Long foamTypeId, @Param("capacity") Double capacity);

	void updateAddQuantityById(@Param("foamTypeId") Long foamTypeId, @Param("capacity")  Double capacity);

	List<ExtinguisherVo> qryListAll();

}
