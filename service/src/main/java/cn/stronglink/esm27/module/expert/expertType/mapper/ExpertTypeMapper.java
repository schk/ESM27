package cn.stronglink.esm27.module.expert.expertType.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.ExpertType;

public interface ExpertTypeMapper extends BaseMapper<ExpertType> {
	
	public List<ExpertType> qryListByParams();

}
