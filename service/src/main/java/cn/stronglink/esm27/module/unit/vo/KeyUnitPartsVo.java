package cn.stronglink.esm27.module.unit.vo;

import java.util.List;
public class KeyUnitPartsVo extends KeyPartsVo{

	/**
	 * 重点部位扩展属性
	 */
	private static final long serialVersionUID = 4366608878209861297L;
	
	private List<KeyPartsVo>  keyPartsList;
	private int  keyPartsCount;
	
	public List<KeyPartsVo> getKeyPartsList() {
		return keyPartsList;
	}
	public void setKeyPartsList(List<KeyPartsVo> keyPartsList) {
		this.keyPartsList = keyPartsList;
	}
	public int getKeyPartsCount() {
		return keyPartsCount;
	}
	public void setKeyPartsCount(int keyPartsCount) {
		this.keyPartsCount = keyPartsCount;
	}
	
	
}
