package cn.stronglink.esm27.module.extinguisher.vo;

import cn.stronglink.esm27.entity.Extinguisher;

public class ExtinguisherVo  extends Extinguisher{

	/**
	 * 
	 */
	private static final long serialVersionUID = -227879607016972179L;
	
	
	private String fireBrigadeName;
	
	private String name;
	
	private String typeName;

	public String getTypeName() {
		return typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public String getFireBrigadeName() {
		return fireBrigadeName;
	}


	public void setFireBrigadeName(String fireBrigadeName) {
		this.fireBrigadeName = fireBrigadeName;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	


}
