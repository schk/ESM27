package cn.stronglink.esm27.module.conventionalEquip.specialDutyEquip.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.google.common.collect.Maps;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.DutyEquipUseRecord;
import cn.stronglink.esm27.entity.DutyEquipUseRecordExample;
import cn.stronglink.esm27.entity.IncidentRecord;
import cn.stronglink.esm27.entity.Room;
import cn.stronglink.esm27.entity.SpecialDutyEquip;
import cn.stronglink.esm27.entity.SpecialDutyEquipTemp;
import cn.stronglink.esm27.entity.User;
import cn.stronglink.esm27.module.conventionalEquip.specialDutyEquip.mapper.SpecialDutyEquipMapper;
import cn.stronglink.esm27.module.conventionalEquip.specialDutyEquip.vo.SpecialDutyEquipVo;
import cn.stronglink.esm27.module.conventionalEquip.specialDutyEquip.vo.SpecialDutyEquipWeb;
import cn.stronglink.esm27.module.dutyequipuserecord.mapper.DutyEquipUseRecordMapper;
import cn.stronglink.esm27.module.emergencyResources.fireBrigade.mapper.FireBrigadeMapper;
import cn.stronglink.esm27.web.webData.mapper.mappers.RecordMapper;
import cn.stronglink.esm27.web.webData.mapper.mappers.RoomMapper;

@Service
@Transactional(rollbackFor=Exception.class)
public class SpecialDutyEquipService {
	
	
	@Autowired
	private SpecialDutyEquipMapper  specialDutyEquipMapper;
	@Autowired
	private FireBrigadeMapper fireBrigadeMapper;
	@Autowired
	private DutyEquipUseRecordMapper dutyEquipUseRecordMapper; 
	@Autowired
	private RoomMapper roomMapper;  
	@Autowired
	private RecordMapper recordMapper;	
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<SpecialDutyEquipVo> qryListByParams(Page<SpecialDutyEquipVo> page, Map<String, Object> params) {
		List<SpecialDutyEquipVo> list=specialDutyEquipMapper.qryListByParams(page,params);
//		if(list!=null) {
//			for(SpecialDutyEquipVo vo:list) {
//				int count =specialDutyEquipMapper.qryEquipCount(vo.getId());
//				vo.setCount(count);
//			}
//		}
		page.setRecords(list);	
		return page; 
	}
	//不关联查询器材
	public Page<SpecialDutyEquip> getObject(Page<SpecialDutyEquip> page, Map<String, Object> params) {
		page.setRecords(specialDutyEquipMapper.getObject(page,params));	
		return page;
	}
	//根据消防车ID查询随车器材信息
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<SpecialDutyEquipVo> qryListByEngineId(Long id){
		return specialDutyEquipMapper.qryListByEngineId(id);
	}
	/*
	 * 根据id查询部门名称及所有信息
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public SpecialDutyEquipVo getDeptNameById(Long id){
		return specialDutyEquipMapper.getDeptNameById(id);
	}
	
	/*
	 * 查询信息
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public SpecialDutyEquip selectById(Long id){
		return specialDutyEquipMapper.selectById(id);
	}
	
	/*
	 * 根据id查询
	 */
	public void  remove(Long id){
		specialDutyEquipMapper.deleteById(id);
	}
	
	/*
	 * 添加信息
	 */
	public void insert(SpecialDutyEquip entity) {
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		specialDutyEquipMapper.insert(entity);
	}
	//修改信息
	public void update(SpecialDutyEquip entity) {
		if(entity.getCompute()==2){
			entity.setFlow(0.0);
		}
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if(specialDutyEquipMapper.updateById(entity)==0){
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
	}
	public List<Map<String, Object>> getSpecialDutyEquipType() {
		return specialDutyEquipMapper.getSpecialDutyEquipType();
	}
	
	/**
	 * 分页查询
	 * @param page
	 * @return
	 */
	public Page<SpecialDutyEquipVo> selectAll(Page<SpecialDutyEquipVo> page ) {
		page.setRecords(specialDutyEquipMapper.selectAll(page));	
		return page;
	}
	/**
	 * 按照种类查询
	 * @param page
	 * @param typeId
	 * @param fireBrigadeId 
	 * @return
	 */
	public Page<SpecialDutyEquipVo> selectOneType(Page<SpecialDutyEquipVo> page, String typeId, String fireBrigadeId) {
		Map<String, String> map = Maps.newHashMap();
		map.put("typeId", typeId);
		map.put("fireBrigadeId", fireBrigadeId);
		List<SpecialDutyEquipVo> list = specialDutyEquipMapper.selectOneType(page,map);
		page.setRecords(list );	
		return page;
	}
	
	public Map<String, Object> recordTempUseCount(Map<String, Object> queryMap) {
		Map<String, Object> map = Maps.newHashMap();
		String  equipId       = queryMap.get("equipId").toString();
		String  tempUseCount  = queryMap.get("tempUseCount").toString();
		String  roomId        = queryMap.get("roomId").toString();
		User    user          =(User) queryMap.get("currentUser");
		Room    room          = roomMapper.selectById(roomId);
		SpecialDutyEquip dutyOrigEquip = specialDutyEquipMapper.selectById(equipId);
		
		
		int     tempUseInt    = Integer.valueOf(tempUseCount);
		long    equipIdLong   = Long.valueOf(equipId);
		Date    date          = new Date();
		Long    userId        = user.getId();
		String  userName      = user.getName();
		Long    roomLong      = Long.valueOf(roomId);
		String  roomCode      = room.getRoom();    
		String  equipName     = dutyOrigEquip.getName();
		
		int origTempUse     =  dutyOrigEquip.getTempUse() == null ? 0 :dutyOrigEquip.getTempUse();
		int storageQuantity =  dutyOrigEquip.getStorageQuantity();
		int nowUseConunt    =  origTempUse + tempUseInt;
        int enableUse       =  storageQuantity - nowUseConunt;
        int enableUseOver   =  storageQuantity - origTempUse;
        
		if(nowUseConunt > storageQuantity){
			//超限
			map.put("enableUse" , enableUseOver);
			map.put("flag", false);
			map.put("msg","使用数量大于库存剩余数据量");
			return map; 
		}else{
			//未超使用
			map.put("enableUse" , enableUse);
		}
		
		
		SpecialDutyEquip specialDutyEquip = new SpecialDutyEquip(); 
		specialDutyEquip.setTempUse(nowUseConunt);
		specialDutyEquip.setId(equipIdLong);
		
		specialDutyEquipMapper.updateById(specialDutyEquip);
		
		DutyEquipUseRecord dutyEquipUseRecord = new DutyEquipUseRecord(); 
		dutyEquipUseRecord.setId(IdWorker.getId());
		dutyEquipUseRecord.setCount(tempUseInt);
		dutyEquipUseRecord.setCreateBy(userId);
		dutyEquipUseRecord.setCreateTime(date);
		dutyEquipUseRecord.setDutyEquipId(equipIdLong);
		dutyEquipUseRecord.setRoomId(roomLong);
//		dutyEquipUseRecord.setUpdateTime(date);
//		dutyEquipUseRecord.setUpdateBy(userId);
		dutyEquipUseRecordMapper.insertSelective(dutyEquipUseRecord);
		
		String diffTimeStr = "";
		List<IncidentRecord>  diffTimeRecordList = roomMapper.qryLastTimeDifference();
		if(diffTimeRecordList  != null || diffTimeRecordList.size() > 0) { 
			IncidentRecord diffTimeRecord = diffTimeRecordList.get(0);
			//计算此房间距离上次记录时间
			long timeDiff = date.getTime()  - diffTimeRecord.getCreateTime().getTime();
	    	long min = timeDiff/1000 ;
	    	diffTimeStr = String.valueOf(min);
		}else{
			diffTimeStr = "0";
		}
		
		IncidentRecord incidentRecord = new IncidentRecord();  
		incidentRecord.setId(IdWorker.getId());
		incidentRecord.setDirector(userName);
		incidentRecord.setType(12);
		incidentRecord.setRoomId(roomLong);
		//incidentRecord.setRoomCode(roomCode);
		incidentRecord.setAction("器材调用");
		incidentRecord.setActionDesc("调用"+equipName+"器材"+tempUseCount+"个");
		incidentRecord.setCreateTime(date);
		incidentRecord.setCreateBy(userId);
		incidentRecord.setIsDelete(1);
		incidentRecord.setTimeDifference(diffTimeStr);
		
		recordMapper.insert(incidentRecord);
		
		map.put("flag", true);
		return map; 
	}
	
	
	public Map<String, Object> resetEquipMent(Long roomId) {
		Map<String, Object> map = Maps.newHashMap();
		map.put("flag", true);
		
		DutyEquipUseRecordExample  example = new DutyEquipUseRecordExample();
		example.createCriteria().andRoomIdEqualTo(roomId);
		List<DutyEquipUseRecord> listequipUseRecord = dutyEquipUseRecordMapper.selectByExample(example);
		if(listequipUseRecord==null||listequipUseRecord.size()==0){
			map.put("flag", false);
			return map; 
		}
		List<SpecialDutyEquip> searchlist = new ArrayList<>();
		for(DutyEquipUseRecord record :listequipUseRecord){
			if(record !=null){
				SpecialDutyEquip seDutyEquip = new  SpecialDutyEquip();
				seDutyEquip.setTempUse(0);
				seDutyEquip.setId(record.getDutyEquipId());
				searchlist.add(seDutyEquip);
			}

		}
		specialDutyEquipMapper.updateTempCount(searchlist);
		return map;
	}
	
	public Long findBrigadeIdByName(String brigadeName) {
		return fireBrigadeMapper.findBrigadeIdByName(brigadeName);
	}
	
	public void batchInsertObjTemp(List<SpecialDutyEquipTemp> objTempList) {
		specialDutyEquipMapper.batchInsertObjTemp(objTempList);
	}
	
	public void batchInsertObj(List<SpecialDutyEquip> objList) {
		specialDutyEquipMapper.batchInsertObj(objList);
	}
	
	public List<SpecialDutyEquipTemp> qryObjTemp(Long timestamp) {
		return specialDutyEquipMapper.qryObjTemp(timestamp);
	}

	public void delObjTemp(Long timestamp) {
		specialDutyEquipMapper.delObjTemp(timestamp);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<SpecialDutyEquipWeb> selectEquipByParams() {
		return specialDutyEquipMapper.selectEquipByParams();
	}
}
