package cn.stronglink.esm27.module.common.controller;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.Constants;
import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.LoginException;
import cn.stronglink.core.support.Assert;
import cn.stronglink.core.support.LoginHelper;
import cn.stronglink.core.util.MD5Encrypt;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.SysOperateLog;
import cn.stronglink.esm27.entity.User;
import cn.stronglink.esm27.module.system.log.service.SysLogService;
import cn.stronglink.esm27.module.system.permission.service.PermissionService;
import cn.stronglink.esm27.module.system.user.service.UserService;


@Controller
@RequestMapping(value = "")
public class LoginManagerController extends AbstractController  {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private SysLogService sysLogService;
	
	@Autowired
	private PermissionService permissionService;

	/**
	 * 登录
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "user/login")
	public ResponseEntity<ModelMap> doLogin(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		
		Assert.notNull(params.get("username"), "USERNAME");
		Assert.notNull(params.get("password"), "PASSWORD");
		User u = null;
		if (LoginHelper.login(params.get("username").toString(), params.get("password").toString())) {		
			u =userService.findUserByName(params.get("username").toString());
			Set<String> perms = permissionService.getPermsByUserId(u.getId());
			if (perms==null || perms.size()==0) {
				throw new LoginException("用户无权限，请联系管理员");
			}
			u.setPermset(perms);
			SysOperateLog log = new SysOperateLog();
			log.setArgs("login[参数1，类型：LoginParam，值：{getUsername : " + params.get("username") 
			+ ",getPassword : " + MD5Encrypt.encrypt(params.get("password").toString())+ "}]");
			log.setId(IdWorker.getId());
			log.setUserName(params.get("username").toString());
			log.setModule("登录");
			log.setOperateTime(new Date());
			log.setOperateType("登录");
			log.setDescription("登录系统");
			log.setIp(WebUtil.getHost(request));
			log.setActName(u.getName());
			HttpSession session = request.getSession(); 
			Long brigadeId =userService.selectUserBrigade(u.getId());
			
			List<Long> userBrigade = userService.selectUserBrigadeChild(brigadeId);			
			if(userBrigade!=null&&userBrigade.size()>0){
				session.setAttribute("userBrigade", userBrigade);
			}
			session.setAttribute("admin", u.isAdmin());
			session.setAttribute("currentUser", u);
			sysLogService.insertOperateLog(log);
			SecurityUtils.getSubject().getSession().setAttribute(Constants.CURRENT_USER, u.getId());
		}	
		return setSuccessModelMap(modelMap,u);
	}
	
	
	/**
	 * 查询当前用户信息
	 */
	@RequestMapping(value = "user/queryCurrent")
	public ResponseEntity<ModelMap> queryCurrent(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Long userId = WebUtil.getCurrentUser();
		HttpSession session = request.getSession(); 
		User u=(User) session.getAttribute("currentUser");
		Set<String> perList =new HashSet<String>();
		if(u!=null){
			perList =u.getPermset();
		}
		
		if(userId!=null&&perList.size()>0){
			User vo = userService.selectByid(userId);
			return setSuccessModelMap(modelMap,vo);	
		}else{
			response.setStatus(401);
			response.getWriter().write("");
			return null;
		}
	}
	
	/**
	 * 查询当前用户信息
	 */
	@RequestMapping(value = "user/getUserBrigadeId")
	public ResponseEntity<ModelMap> getUserBrigadeId(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response)  {
		Long brigadeId =userService.selectUserBrigade(WebUtil.getCurrentUser());
		return setSuccessModelMap(modelMap,brigadeId);
	}
	
	/**
	 * 退出
	 */
	@RequestMapping(value = "user/logout")
	public ResponseEntity<ModelMap> logout(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		Subject currentUser = SecurityUtils.getSubject(); 
		currentUser.logout();
		return setSuccessModelMap(modelMap);	
	}
	
	
	/**
	 * 核对原密码
	 */
	@RequestMapping(value = "user/checkOldPassword")
	public ResponseEntity<ModelMap> checkOldPassword(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String,Object> params) {
		Boolean bool = false;
		if(LoginHelper.login(params.get("username").toString(), params.get("oldPassword").toString())){
			bool = true;
		}
		return setSuccessModelMap(modelMap,bool);	
	}
	
	/**
	 * 修改密码
	 */
	@RequestMapping(value = "user/updatePassword")
	public ResponseEntity<ModelMap> updatePassword(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody User user) {
		userService.resetUserPW(user);
		return setSuccessModelMap(modelMap,null);	
	}
	

}
