package cn.stronglink.esm27.module.danger.dangerType.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.DangerType;
import cn.stronglink.esm27.module.danger.dangerType.vo.DangerTypeTreeNodeVo;

public interface DangerTypeMapper extends BaseMapper<DangerType> {
	
	public List<DangerTypeTreeNodeVo> qryDepts();
	
	public List<DangerTypeTreeNodeVo> qryRootDeptNoTop();
	
	public List<DangerTypeTreeNodeVo> qryRootDept();

	public int getChildDetpCount(@Param("id")Long id);

	public List<DangerType> getdangerTypeList(@Param("id")Long id);



}
