package cn.stronglink.esm27.module.common.controller;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Preconditions;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.support.DocConverter;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.core.util.UploadEntity;

@Controller
@RequestMapping(value = "common")
public class CommonController extends AbstractController {

	@Autowired  
	private Environment env;  
	
	@RequestMapping(value = "uploadSwf")
	public ResponseEntity<ModelMap> uploadSwf(@RequestParam(value = "file", required = true) MultipartFile file,HttpServletRequest request, HttpServletResponse response,
				ModelMap modelMap) {	
			if(!file.isEmpty()){ 
				try {  
					String suffix = file.getOriginalFilename().substring  
							(file.getOriginalFilename().lastIndexOf("."));
					String fid = UUID.randomUUID().toString().replace("-", "");
	                // 文件保存路径  
					String uploadPath=env.getProperty("upload.dir")+"uploadFiles/";
	        		String dbFilePath = "/uploadFiles/"+fid+suffix;
	        		String filePath=uploadPath+fid+suffix;
	        		File f = new File(uploadPath);
	        		if(!f.exists()){
	        			f.mkdirs();
	        		}
	        		file.transferTo(new File(filePath)); 	        		
	        		DocConverter d = new DocConverter(filePath,env.getProperty("swf.dir")); 
	    			d.conver(); 
	    			if (!d.isOpen()) {
	    				//throw new BusinessException("swf转换异常，openoffice服务未启动！");
	    				return setModelMap(modelMap, HttpCode.CONFLICT, null, "swf转换异常，openoffice服务未启动！");
					}
	    			System.out.println(d.getswfPath()+"路径"); 
	    			String dbFilePathSwf = "/uploadFiles/"+fid+".swf";
	                UploadEntity result = new UploadEntity();
	    			result.setFid(dbFilePath);
	    			result.setName(file.getOriginalFilename());
	    			result.setUrl(dbFilePath);
	    			result.setSwfUrl(dbFilePathSwf);
	    			result.setSuccess(true);
	    			 
	    			return setSuccessModelMap(modelMap, result);
	            } catch (Exception e) {  
	                e.printStackTrace();  
	            }  		
			}
			return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 判断文件是否存在
	 * @param request
	 * @param modelMap
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	@PostMapping(value = "is-exit")
	public ResponseEntity<ModelMap> isExistOfFile(
			@RequestParam(name = "filePath") String filePath,ModelMap modelMap) throws UnsupportedEncodingException {	
		Preconditions.checkNotNull(filePath,"文件路径不能为空");
		// 解码文件路径，并将路径第一个字符“/”删除
		filePath = URLDecoder.decode(filePath, "UTF-8");
		if("/".equals(filePath.substring(0,1))) {
			filePath=filePath.substring(1);
		}
		
		filePath = env.getProperty("upload.dir")+URLDecoder.decode(filePath, "UTF-8");
        File file = new File(filePath);
        Preconditions.checkArgument(file.exists(),"文件不存在。");
        
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 上传文件转swf(除了危化品库得数据)
	 * @param file
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "uploadFileSwf")
	public ResponseEntity<ModelMap> uploadFileSwf(@RequestParam(value = "file", required = true) MultipartFile file,HttpServletRequest request, HttpServletResponse response,
				ModelMap modelMap) {	
			if(!file.isEmpty()){ 
				try {  
					String suffix = file.getOriginalFilename().substring  
							(file.getOriginalFilename().lastIndexOf("."));
					String fid = UUID.randomUUID().toString().replace("-", "");
	                // 文件保存路径  
					String uploadPath=env.getProperty("upload.dir")+"uploadSwf/";
	        		String dbFilePath = "/uploadSwf/"+fid+suffix;
	        		String filePath=uploadPath+fid+suffix;
	        		File f = new File(uploadPath);
	        		if(!f.exists()){
	        			f.mkdirs();
	        		}
	        		file.transferTo(new File(filePath)); 	        		
	        		String dbFilePathSwf = "";
	        		if (suffix.equals(".doc")||suffix.equals(".docx")||suffix.equals(".pdf")||suffix.equals(".xls")||suffix.equals(".xlsx")) {
						DocConverter d = new DocConverter(filePath,env.getProperty("swf.dir")); 
		    			d.conver();
		    			if (!d.isOpen()) {
		    				//throw new BusinessException("swf转换异常，openoffice服务未启动！");
		    				return setModelMap(modelMap, HttpCode.CONFLICT, null, "swf转换异常，openoffice服务未启动！");
						}
		    			dbFilePathSwf ="/uploadSwf/"+fid+".swf";
					}
	    			System.out.println(dbFilePathSwf+"路径"); 
	                UploadEntity result = new UploadEntity();
	    			result.setFid(dbFilePath);
	    			result.setName(file.getOriginalFilename());
	    			result.setUrl(dbFilePath);
	    			result.setSwfUrl(dbFilePathSwf);
	    			result.setSuccess(true);
	    			 
	    			return setSuccessModelMap(modelMap, result);
	            } catch (Exception e) {  
	                e.printStackTrace();  
	            }  		
			}
			return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 上传不需要转得swf文档
	 * @param file
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "uploadFile")
	public ResponseEntity<ModelMap> uploadFile(@RequestParam(value = "file", required = true) MultipartFile file,HttpServletRequest request, HttpServletResponse response,
				ModelMap modelMap) {	
			if(!file.isEmpty()){ 
				try {  
					String suffix = file.getOriginalFilename().substring  
							(file.getOriginalFilename().lastIndexOf("."));
					String fid = UUID.randomUUID().toString().replace("-", "");
	                // 文件保存路径  
					String uploadPath=env.getProperty("upload.dir")+"files/";
	        		String dbFilePath = "/files/"+fid+suffix;
	        		String filePath=uploadPath+fid+suffix;
	        		File f = new File(uploadPath);
	        		if(!f.exists()){
	        			f.mkdirs();
	        		}
	        		file.transferTo(new File(filePath)); 
	                UploadEntity result = new UploadEntity();
	    			result.setFid(dbFilePath);
	    			result.setName(file.getOriginalFilename());
	    			result.setUrl(dbFilePath);
	    			result.setSuccess(true);
	    			 
	    			return setSuccessModelMap(modelMap, result);
	            } catch (Exception e) {  
	                e.printStackTrace();  
	            }  		
			}
			return setSuccessModelMap(modelMap, null);
	}
	
	@RequestMapping(value = "uploadIcon")
	public ResponseEntity<ModelMap> uploadIcon(@RequestParam(value = "file", required = true) MultipartFile file,HttpServletRequest request, HttpServletResponse response,
				ModelMap modelMap) throws Exception {	
			if(!file.isEmpty()){ 
					InputStream  fis =file.getInputStream();
					BufferedImage bufferedImg = ImageIO.read(fis);
					int imgWidth = bufferedImg.getWidth();
					int imgHeight = bufferedImg.getHeight();
					if(imgWidth!=36&&imgHeight!=36&&file.getSize()>3*1024){
						throw new Exception("图标必须是36*36且大小不能超过3M");
					}
					String suffix = file.getOriginalFilename().substring  
							(file.getOriginalFilename().lastIndexOf("."));
					String fid = UUID.randomUUID().toString().replace("-", "");
	                // 文件保存路径  
	        		String uploadPath=env.getProperty("upload.dir")+"icon/";
	        		String dbFilePath = "/icon/"+fid+suffix;
	        		String filePath=uploadPath+fid+suffix;
	        		File f = new File(uploadPath);
	        		if(!f.exists()){
	        			f.mkdirs();
	        		}
	        		file.transferTo(new File(filePath)); 	        		
	                UploadEntity result = new UploadEntity();
	    			result.setFid(dbFilePath);
	    			result.setName(file.getOriginalFilename());
	    			result.setUrl(dbFilePath);
	    			result.setSuccess(true);
	    			return setSuccessModelMap(modelMap, result);		
			}
			return setSuccessModelMap(modelMap, null);
		}
					
	
	@RequestMapping(value = "uploadImg")
	public ResponseEntity<ModelMap> uploadImg(@RequestParam(value = "file", required = true) MultipartFile file,HttpServletRequest request, HttpServletResponse response,
				ModelMap modelMap) {	
			if(!file.isEmpty()){ 
				try {  
					String suffix = file.getOriginalFilename().substring  
							(file.getOriginalFilename().lastIndexOf("."));
					String fid = UUID.randomUUID().toString().replace("-", "");
	                // 文件保存路径  
					String uploadPath=env.getProperty("upload.dir")+"uploadImage/";
	        		String dbFilePath = "/uploadImage/"+fid+suffix;
	        		String filePath=uploadPath+fid+suffix;
	        		File f = new File(uploadPath);
	        		if(!f.exists()){
	        			f.mkdirs();
	        		}
	        		file.transferTo(new File(filePath)); 
	        		
	                UploadEntity result = new UploadEntity();
	    			result.setFid(dbFilePath);
	    			result.setName(file.getOriginalFilename());
	    			result.setUrl(dbFilePath);
	    			result.setSuccess(true);
	    			return setSuccessModelMap(modelMap, result);
	            } catch (Exception e) {  
	                e.printStackTrace();  
	            }  		
			}
			return setSuccessModelMap(modelMap, null);
		}

	@RequestMapping(value = "uploadCarImg")
	public ResponseEntity<ModelMap> uploadCarImg(@RequestParam(value = "file", required = true) MultipartFile file,HttpServletRequest request, HttpServletResponse response,
				ModelMap modelMap) throws Exception {	
			if(!file.isEmpty()){ 
				InputStream  fis =file.getInputStream();
				BufferedImage bufferedImg = ImageIO.read(fis);
				int imgWidth = bufferedImg.getWidth();
				int imgHeight = bufferedImg.getHeight();
				if(imgWidth!=298&&imgHeight!=180&&file.getSize()>20*1024){
					throw new Exception("图标必须是298*180且大小不能超过20M");
				}
				String suffix = file.getOriginalFilename().substring  
						(file.getOriginalFilename().lastIndexOf("."));
				String fid = UUID.randomUUID().toString().replace("-", "");
				// 文件保存路径  
				String uploadPath=env.getProperty("upload.dir")+"uploadImage/";
				String dbFilePath = "/uploadImage/"+fid+suffix;
				String filePath=uploadPath+fid+suffix;
	        	File f = new File(uploadPath);
	        	if(!f.exists()){
	        		f.mkdirs();
	        	}
	        	file.transferTo(new File(filePath)); 
	        	
	            UploadEntity result = new UploadEntity();
	    		result.setFid(dbFilePath);
	    		result.setName(file.getOriginalFilename());
	    		result.setUrl(dbFilePath);
	    		result.setSuccess(true);
	    		return setSuccessModelMap(modelMap, result);		
			}
			return setSuccessModelMap(modelMap, null);
		}
	
	@RequestMapping(value = "uploadJHIcon")
	public ResponseEntity<ModelMap> uploadJHIcon(@RequestParam(value = "file", required = true) MultipartFile file,HttpServletRequest request, HttpServletResponse response,
				ModelMap modelMap) throws Exception {	
			if(!file.isEmpty()){ 
					InputStream  fis =file.getInputStream();
					BufferedImage bufferedImg = ImageIO.read(fis);
					int imgWidth = bufferedImg.getWidth();
					int imgHeight = bufferedImg.getHeight();
					if(imgWidth!=55&&imgHeight!=30&&file.getSize()>3*1024){
						throw new Exception("图标必须是55*30且大小不能超过3M");
					}
					String suffix = file.getOriginalFilename().substring  
							(file.getOriginalFilename().lastIndexOf("."));
					String fid = UUID.randomUUID().toString().replace("-", "");
	                // 文件保存路径  
	        		String uploadPath=env.getProperty("upload.dir")+"mapIcon/iconCluster/";
	        		String dbFilePath = "/mapIcon/iconCluster/"+fid+suffix;
	        		String filePath=uploadPath+fid+suffix;
	        		File f = new File(uploadPath);
	        		if(!f.exists()){
	        			f.mkdirs();
	        		}
	        		file.transferTo(new File(filePath)); 	        		
	                UploadEntity result = new UploadEntity();
	    			result.setFid(dbFilePath);
	    			result.setName(file.getOriginalFilename());
	    			result.setUrl(dbFilePath);
	    			result.setSuccess(true);
	    			return setSuccessModelMap(modelMap, result);		
			}
			return setSuccessModelMap(modelMap, null);
		}
				
	@RequestMapping(value = "uploadDTIcon")
	public ResponseEntity<ModelMap> uploadDTIcon(@RequestParam(value = "file", required = true) MultipartFile file,HttpServletRequest request, HttpServletResponse response,
				ModelMap modelMap) throws Exception {	
			if(!file.isEmpty()){ 
					InputStream  fis =file.getInputStream();
					BufferedImage bufferedImg = ImageIO.read(fis);
					int imgWidth = bufferedImg.getWidth();
					int imgHeight = bufferedImg.getHeight();
					if(imgWidth!=25&&imgHeight!=25&&file.getSize()>2*1024){
						throw new Exception("图标必须是25*25且大小不能超过2M");
					}
					String suffix = file.getOriginalFilename().substring  
							(file.getOriginalFilename().lastIndexOf("."));
					String fid = UUID.randomUUID().toString().replace("-", "");
	                // 文件保存路径  
	        		String uploadPath=env.getProperty("upload.dir")+"icon/";
	        		String dbFilePath = "/icon/"+fid+suffix;
	        		String filePath=uploadPath+fid+suffix;
	        		File f = new File(uploadPath);
	        		if(!f.exists()){
	        			f.mkdirs();
	        		}
	        		file.transferTo(new File(filePath)); 	        		
	                UploadEntity result = new UploadEntity();
	    			result.setFid(dbFilePath);
	    			result.setName(file.getOriginalFilename());
	    			result.setUrl(dbFilePath);
	    			result.setSuccess(true);
	    			return setSuccessModelMap(modelMap, result);		
			}
			return setSuccessModelMap(modelMap, null);
		}
	
	
	
	
	/**
	 * 文件下载
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param filePath
	 * @return
	 */
	@RequestMapping(value = "downLoadFile")
	public void downLoadFile(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String param = URLDecoder.decode(request.getParameter("param").toString(), "UTF-8");
			@SuppressWarnings("unchecked")
			Map<String, Object> params =  JSON.parseObject(param, Map.class);
			String filePath = env.getProperty("upload.dir")+params.get("filePath").toString();
            // path是指欲下载的文件的路径。
            File file = new File(filePath);
            // 取得文件名。
            String filename = file.getName();
            // 取得文件的后缀名。
            String ext = filename.substring(filename.lastIndexOf(".") + 1);
            String downLoadName = params.get("fileName").toString();
            filename = downLoadName;
            // 以流的形式下载文件。
            InputStream fis = new BufferedInputStream(new FileInputStream(filePath));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
            // 清空response
            // response.reset();
            // 设置response的Header
            response.addHeader("Content-Disposition", "attachment;filename=" + new String(filename.getBytes("GB2312"),"ISO8859-1"));
            response.addHeader("Content-Length", "" + file.length());
            OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
            response.setContentType("application/octet-stream");
            toClient.write(buffer);
            toClient.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
	}
	
}
