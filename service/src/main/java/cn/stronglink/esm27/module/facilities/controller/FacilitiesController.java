package cn.stronglink.esm27.module.facilities.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.Facilities;
import cn.stronglink.esm27.module.facilities.service.FacilitiesService;
import cn.stronglink.esm27.module.facilities.vo.FacilitiesVo;

@Controller
@RequestMapping(value = "facilities")
public class FacilitiesController extends AbstractController {
	
	@Autowired
	private FacilitiesService  facilitiesService;
	

	/*
	 * 查询列表
	 */
	@RequestMapping("qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody Map<String, Object> params){
		@SuppressWarnings("unchecked")
		Page<FacilitiesVo> page = (Page<FacilitiesVo>) super.getPage(params);
		Page<FacilitiesVo> data = facilitiesService.getObject(page, params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/*
	 * 查询列表不带分页
	 */
	@RequestMapping("qryList")
	public ResponseEntity<ModelMap> qryList(HttpServletRequest request,
			HttpServletResponse response,ModelMap modelMap){
		List<FacilitiesVo> data = facilitiesService.qryList();
		return setSuccessModelMap(modelMap, data);
	}
	
	/*
	 * 查询对象
	 */
	@RequestMapping("qryById")
	public ResponseEntity<ModelMap> qryById(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody Long id){
		FacilitiesVo data = facilitiesService.qryById(id);
		return setSuccessModelMap(modelMap, data);
	}
	/*
	 * 根据id删除信息
	 */
	@RequestMapping(value = "remove")
	@OperateLog(module = "生产装置管理-消防设施",desc="删除消防设施", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> remove(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody Long id){
		facilitiesService.deleteObjectById(id);
		
		return setSuccessModelMap(modelMap, null);
	}
	/*
	 * 添加信息
	 */
	@RequestMapping(value = "create")
	@OperateLog(module = "生产装置管理-消防设施",desc="添加消防设施", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap , @RequestBody Facilities fireControl){
		facilitiesService.insertObject(fireControl);
		
		return setSuccessModelMap(modelMap, null);
	}
	/*
	 * 修改信息
	 */
	@RequestMapping(value = "edit")
	@OperateLog(module = "生产装置管理-消防设施",desc="修改消防设施", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap , @RequestBody Facilities fireControl){
		facilitiesService.updateObject(fireControl);
		return setSuccessModelMap(modelMap, null);
		
	}

}
