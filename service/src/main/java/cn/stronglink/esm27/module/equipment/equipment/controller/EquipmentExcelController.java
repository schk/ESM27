package cn.stronglink.esm27.module.equipment.equipment.controller;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.core.util.BmUtils;
import cn.stronglink.core.util.ImportValid;
import cn.stronglink.core.util.MapLonLatUtil;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Equipment;
import cn.stronglink.esm27.entity.EquipmentTemp;
import cn.stronglink.esm27.entity.EquipmentType;
import cn.stronglink.esm27.entity.KeyUnit;
import cn.stronglink.esm27.module.equipment.equipment.service.EquipmentService;
import cn.stronglink.esm27.module.equipment.equipmentType.service.EquipmentTypeService;
import cn.stronglink.esm27.module.unit.service.KeyUnitService;

@Controller
@RequestMapping(value = "equipment/excel")
public class EquipmentExcelController extends AbstractController {

	@Autowired
	private EquipmentService entityService;
	@Autowired
	private EquipmentTypeService equipmentTypeService;
	@Autowired
	private KeyUnitService keyUnitService;

	/**
	 * 导入EXCEL文件
	 * 
	 * @param file
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "importExcel")
	public ResponseEntity<ModelMap> importExcel(@RequestParam(value = "file", required = true) MultipartFile file,
			HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) throws Exception {
		List<String> dataList = new ArrayList<String>();
		System.out.println(WebUtil.getCurrentUser());
		// 导入Excel操作
		// 导入Excel文件不为空时，将文件写入流，然后转化为工作薄
		if (file != null) {
			try {
				Workbook workBook = null;
				if (!ImportValid.validXls(file)) {
					return setModelMap(modelMap, HttpCode.CONFLICT, "请选择excel格式文件！");
				}
				int header = 1;
				workBook = WorkbookFactory.create(file.getInputStream());
				if (workBook.getNumberOfSheets() > 0) {
					// 获取第一个单元薄
					Sheet sheet = workBook.getSheetAt(0);
					// 获取单元薄中表头信息,并将表头中每个单元格中信息存入数组headNames中
					String allTitle = "*装置名称$*装置编码$*所属类型$*重点单位$*介质$*耐火等级$容器直径$装置体积$*坐标类型(百度坐标/无偏移坐标)$经纬度";
					Row rowHead = sheet.getRow(header);
					if (rowHead != null) {
						String[] headNames = new String[rowHead.getLastCellNum()];
						// 获取该行中总共有多少列数据row.getLastCellNum()
						for (int j = 0; j < rowHead.getLastCellNum(); j++) {
							Cell cell = rowHead.getCell(j);
							// 获取表头各个字段名称
							String returnStr = BmUtils.getCellValue(cell);
							headNames[j] = returnStr;
						}
						if (headNames != null && headNames.length > 0) {
							for (String name : headNames) {
								if (allTitle.indexOf(name) == -1) {
									throw new BusinessException("属性：" + name + "在系统中不存在,请使用模板文件！");
								}
							}
						}
						Date date = new Date();
						// 遍历单元簿中每一行，获取每一列字段，并根据表头对应赋值
						int flag = 3;
						List<Equipment> entityList = new ArrayList<Equipment>();
						List<EquipmentTemp> tempList = new ArrayList<EquipmentTemp>();
						List<EquipmentType> typeList = equipmentTypeService.qryEquipmentTypeList(2);

						Map<String, Object> params = new HashMap<String, Object>();
						List<KeyUnit> keyUnitList = keyUnitService.qryKeyUnitList(params);
						// 关系是否存在
						Boolean typeNoExist = false;
						int dataFormat = 0;
						modelMap.put("errorCode", 0);
						DecimalFormat df = new DecimalFormat("#.000000");
						// 插入execl中得数据到临时表
						long timestamp = System.currentTimeMillis();
						for (int i = header; i < sheet.getPhysicalNumberOfRows(); i++) {
							Row row = sheet.getRow(i + 1);
							Boolean isRow = ImportValid.isRowEmpty(row);
							Equipment entity = null;
							EquipmentTemp temp = null;
							if (!isRow) {
								String rowError = "错误：第" + flag + "行   ";
								String data = "";
								String lonLatType = "";
								entity = new Equipment();
								entity.setId(IdWorker.getId());
								entity.setCreateBy(WebUtil.getCurrentUser());
								entity.setCreateTime(date);
								entity.setUpdateTime(date);

								temp = new EquipmentTemp();
								temp.setId(IdWorker.getId());
								temp.setCreateBy(WebUtil.getCurrentUser());
								temp.setTimestamp(timestamp);
								temp.setCreateTime(date);

								// 获取该行中总共有多少列数据row.getLastCellNum()
								for (int j = 0; j < row.getLastCellNum(); j++) {
									if(j>headNames.length-1) {
										continue ;
									}
									Cell cell = row.getCell(j);
									if (StringUtils.hasText(headNames[j])) {
										// 获取单元格内容，并根据单元格类型，进行转化
										String returnStr = BmUtils.getCellValue(cell);
										// 根据给定的Excel格式，进行导入，一下是对应列对应字段
										if (headNames[j].equals("*装置名称")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setName(returnStr);
												temp.setName(returnStr);
											} else {
												dataFormat++;
												data += "【装置名称】不能为空;";
											}
										} else if (headNames[j].equals("*装置编码")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setCode(returnStr);
												temp.setCode(returnStr);
											} else {
												dataFormat++;
												data += "【装置编码】不能为空;";
											}
										} else if (headNames[j].equals("*所属类型")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												temp.setType(returnStr);
												if (typeList != null && typeList.size() > 0) {
													boolean isExit = false;
													for (EquipmentType type : typeList) {
														if (type.getName().equals(returnStr)) {
															isExit = true;
															entity.setTypeId(type.getId().toString());
															break;
														}
													}
													if (!isExit) {
														typeNoExist = true;
														data += "【所属类型：" + returnStr + "】不存在;";
													}
												} else {
													typeNoExist = true;
													data += "【所属类型：" + returnStr + "】不存在;";
												}
											} else {
												dataFormat++;
												data += "【所属类型】不能为空;";
											}
										} else if (headNames[j].equals("*重点单位")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												if (keyUnitList != null && keyUnitList.size() > 0) {
													boolean isExit = false;
													for (KeyUnit keyUnit : keyUnitList) {
														if (keyUnit.getName().equals(returnStr)) {
															isExit = true;
															entity.setKeyUnitId(keyUnit.getId().toString());
															temp.setKeyUnitId(keyUnit.getId().toString());
															break;
														}
													}
													if (!isExit) {
														dataFormat++;
														data += "【所属重点单位：" + returnStr + "】不存在;";
													}
												} else {
													dataFormat++;
													data += "【所属重点单位：" + returnStr + "】不存在;";
												}
											} else {
												dataFormat++;
												data += "【所属重点单位】不能为空;";
											}
										} else if (headNames[j].equals("*介质")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setMedium(returnStr);
												temp.setMedium(returnStr);
											} else {
												dataFormat++;
												data += "【介质】不能为空;";
											}
										} else if (headNames[j].equals("*耐火等级")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												if ("一级".equals(returnStr) || "二级".equals(returnStr)
														|| "三级".equals(returnStr) || "四级".equals(returnStr)) {
													entity.setFireLevel(returnStr);
													temp.setFireLevel(returnStr);
												} else {
													dataFormat++;
													data += "【耐火等级】不存在;";
												}
											} else {
												dataFormat++;
												data += "【耐火等级】不能为空;";
											}
										} else if (headNames[j].equals("容器直径")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setVesselDiameter(Double.valueOf(returnStr));
												temp.setVesselDiameter(Double.valueOf(returnStr));
											}
										} else if (headNames[j].equals("装置体积")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setVolume(Double.valueOf(returnStr));
												temp.setVolume(Double.valueOf(returnStr));
											}
										} else if (headNames[j].equals("*坐标类型(百度坐标/无偏移坐标)")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												lonLatType = returnStr;
											} else {
												dataFormat++;
												data += "【坐标类型】不能为空;";
											}
										} else if (headNames[j].equals("经纬度")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												String[] coord = returnStr.split(",");
												if (coord != null && coord.length == 2) {
													coord[0] = df.format(Double.valueOf(coord[0]));
													coord[1] = df.format(Double.valueOf(coord[1]));
													if ("百度坐标".equals(lonLatType)) {
														double[] pointwgs = MapLonLatUtil.bd09py(
																Double.valueOf(coord[0]), Double.valueOf(coord[1]));
														entity.setBaiduLon(Double.valueOf(coord[0]));
														entity.setBaiduLat(Double.valueOf(coord[1]));
														entity.setLon(Double.valueOf(pointwgs[0]));
														entity.setLat(Double.valueOf(pointwgs[1]));
														temp.setBaiduLon(Double.valueOf(coord[0]));
														temp.setBaiduLat(Double.valueOf(coord[1]));
														temp.setLon(Double.valueOf(pointwgs[0]));
														temp.setLat(Double.valueOf(pointwgs[1]));
													} else if ("无偏移坐标".equals(lonLatType)) {
														entity.setLon(Double.valueOf(coord[0]));
														entity.setLat(Double.valueOf(coord[1]));
														temp.setLon(Double.valueOf(coord[0]));
														temp.setLat(Double.valueOf(coord[1]));
													} else {
														dataFormat++;
														data += "【坐标类型】只能是百度坐标/无偏移坐标;";
													}
												} else {
													data += "【经纬度】格式错误;";
												}

											}
										}
									}
								}
								flag++;
								entityList.add(entity);
								tempList.add(temp);
								if (data != null && !"".equals(data)) {
									dataList.add(rowError + data);
								}
							}
						}
						if (dataList != null && dataList.size() > 0) {
							if (dataFormat > 0) {
								modelMap.put("errorCode", 2);
								return setSuccessModelMap(modelMap, dataList);
							}
							if (typeNoExist) {
								modelMap.put("errorCode", 1);
								modelMap.put("times", timestamp);
								// 存临时表数据
								if (tempList != null && tempList.size() > 0) {
									entityService.saveExcelTempData(tempList); // 数据存储
									return setSuccessModelMap(modelMap, dataList);
								} else {
									throw new BusinessException("文件数据为空!");
								}
							}
						} else {
							// 存数据
							if (entityList != null && entityList.size() > 0) {
								// 数据存储
								entityService.saveExcelData(entityList);
								return setSuccessModelMap(modelMap);
							} else {
								throw new BusinessException("文件数据为空!");
							}
						}
					}
				}
			} catch (IOException e) {
				throw new BusinessException("未知异常!");
			}
		}
		throw new BusinessException("无文件!");
	}

	/**
	 * 继续导入excel数据
	 * 
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param timestamp
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "importExcelConfirm")
	public ResponseEntity<ModelMap> importExcelConfirm(HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap, @RequestBody Long timestamp) throws Exception {
		Date date = new Date();
		// 遍历单元簿中每一行，获取每一列字段，并根据表头对应赋值
		List<Equipment> entityList = new ArrayList<Equipment>();
		List<EquipmentType> typeList = equipmentTypeService.qryEquipmentTypeList(2);
		List<EquipmentTemp> tempList = entityService.qryExcelTemp(timestamp);

		for (int j = 0; j < tempList.size(); j++) {
			EquipmentTemp temp = tempList.get(j);
			Equipment entity = null;
			if (temp != null) {
				entity = new Equipment();
				boolean isExit = false;
				if (typeList != null && typeList.size() > 0) {
					for (EquipmentType type : typeList) {
						if (type.getName().equals(temp.getType())) {
							isExit = true;
							entity.setTypeId(type.getId().toString());
							break;
						}
					}
				}
				if (!isExit) {
					EquipmentType type = new EquipmentType();
					type.setName(temp.getType());
					type.setRemark(temp.getType());
					type.setType(2L);
					equipmentTypeService.insert(type);
					entity.setTypeId(type.getId().toString());
					typeList.add(type);
				}
				entity.setId(IdWorker.getId());
				entity.setName(temp.getName());
				entity.setCode(temp.getCode());
				entity.setFireLevel(temp.getFireLevel());
				entity.setKeyUnitId(temp.getKeyUnitId());
				entity.setLat(temp.getLat());
				entity.setLon(temp.getLon());
				entity.setBaiduLat(temp.getBaiduLat());
				entity.setBaiduLon(temp.getBaiduLon());
				entity.setMedium(temp.getMedium());
				entity.setVesselDiameter(temp.getVesselDiameter());
				entity.setVolume(temp.getVolume());
				entity.setCreateBy(WebUtil.getCurrentUser());
				entity.setCreateTime(date);
				entity.setUpdateTime(date);
				entityList.add(entity);
			}
		}
		if (entityList != null && entityList.size() > 0) {
			// 数据存储
			entityService.saveExcelData(entityList);
			// 删除临时表数据
			entityService.delExcelTemp(timestamp);
			return setSuccessModelMap(modelMap);
		} else {
			throw new BusinessException("文件数据为空!");
		}

	}

	/**
	 * 删除临时表数据
	 * 
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param timestamp
	 * @return
	 */
	@RequestMapping(value = "delExcelTemp")
	public ResponseEntity<ModelMap> delExcelTemp(HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap, @RequestBody Long timestamp) {
		// 删除临时表数据
		entityService.delExcelTemp(timestamp);
		return setSuccessModelMap(modelMap);

	}

}
