package cn.stronglink.esm27.module.system.log.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.esm27.entity.SysOperateLog;
import cn.stronglink.esm27.module.system.log.mapper.SysLogMapper;

@Service
@EnableTransactionManagement
@EnableAsync
@Transactional(rollbackFor=Exception.class) 
public class SysLogService {
	
	@Autowired
	private SysLogMapper sysLogMapper;
	
	public void insertOperateLog(SysOperateLog entity) {
		sysLogMapper.insert(entity);
		
	}
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<SysOperateLog> qryList(Page<SysOperateLog> page, Map<String, Object> params) {
		List<SysOperateLog> list = sysLogMapper.qryList(page,params);
		page.setRecords(list);
		return page;
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public SysOperateLog qryById(Long id) {
		SysOperateLog sysOperateLog = sysLogMapper.qryById(id);
		return sysOperateLog;
	}
	
	
	


	

}
