package cn.stronglink.esm27.module.fireEngine.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.FireEngine;
import cn.stronglink.esm27.entity.FireEngineEquipment;
import cn.stronglink.esm27.entity.FireEngineTemp;
import cn.stronglink.esm27.module.extinguisher.mapper.ExtinguisherMapper;
import cn.stronglink.esm27.module.fireEngine.mapper.FireEngineEquipmentMapper;
import cn.stronglink.esm27.module.fireEngine.mapper.FireEngineMapper;
import cn.stronglink.esm27.module.fireEngine.vo.FireEngineVo;

@Service
@Transactional(rollbackFor=Exception.class)
public class FireEngineService {
	
	@Autowired
	private FireEngineMapper fireEngineMapper;
	@Autowired
	private FireEngineEquipmentMapper  fireEngineEquipmentMapper;

	@Autowired ExtinguisherMapper extinguisherMapper;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<FireEngineVo> qryListByParams(Page<FireEngineVo> page, Map<String, Object> params) {
		page.setRecords(fireEngineMapper.qryListByParams(page,params));	
		return page;
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public FireEngineVo qryById(Long id) {
		FireEngineVo fireEngine = fireEngineMapper.getById(id);
		return fireEngine;
	}
	
	/*
	 * 增加信息
	 */
	public void insert(FireEngine entity) {
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(new Date());
		fireEngineMapper.insert(entity);
		if(entity.getFireList()!=null&&entity.getFireList().size()>0) {
			for(FireEngineEquipment vo :entity.getFireList()) {
				vo.setFireEngineId(entity.getId());
				vo.setDutyEquipId(vo.getId());
				vo.setId(IdWorker.getId());
				vo.setCreateTime(new Date());
				vo.setCreateBy(WebUtil.getCurrentUser());
				vo.setUpdateTime(new Date());
				vo.setUpdateBy(WebUtil.getCurrentUser());
			}
			HashMap<String,Object> mapBatch = new HashMap<String,Object>();
			mapBatch.put("equipmentList", entity.getFireList());
			fireEngineMapper.insertIntoBatch(mapBatch);
		}
		
	}
	/*
	 * 修改信息
	 */
	public void update(FireEngine entity) {
		HashMap<String,Object> map=new HashMap<String,Object>();
		map.put("fire_engine_id", entity.getId());
		fireEngineEquipmentMapper.deleteByMap(map);
		if(entity.getFireList()!=null&&entity.getFireList().size()>0) {
			for(FireEngineEquipment vo :entity.getFireList()) {
				vo.setFireEngineId(entity.getId());
				vo.setDutyEquipId(vo.getId());
				vo.setId(IdWorker.getId());
				vo.setCreateTime(new Date());
				vo.setUpdateTime(new Date());
				vo.setUpdateBy(WebUtil.getCurrentUser());
			}
			HashMap<String,Object> mapBatch = new HashMap<String,Object>();
			mapBatch.put("equipmentList", entity.getFireList());
			fireEngineMapper.insertIntoBatch(mapBatch);
		}
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if(fireEngineMapper.updateById(entity)==0){
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
	}
	/*
	 * 删除对象
	 */
	public void remove(Long id) {
		fireEngineMapper.deleteById(id);
	}

	public void saveExcelData(List<FireEngine> entityList) {
		// 查询所有车辆信息
		List<FireEngine> fireEngineList = fireEngineMapper.selectList(null);
		Map<String, FireEngine> fireEngineMap = new HashMap<>();
		if(fireEngineList!=null&&fireEngineList.size()>0) {
			for(FireEngine fireEngine: fireEngineList) {
				fireEngineMap.put(fireEngine.getPlateNumber(), fireEngine);
			}
		}
		if (entityList != null && entityList.size() > 0) {
			List<FireEngine> interimList = new ArrayList<FireEngine>();
			int num = 0;
			for(int i=0;i<entityList.size();i++){
				//	如果存在车牌号  直接更新
				if(fireEngineMap.get(entityList.get(i).getPlateNumber())!=null) {
					num = num + this.updateByPlateNumbery(entityList.get(i));
				}else {
					interimList.add(entityList.get(i));
					if(i!=0&&i%50==0){
						num = num + this.batchInsert(interimList);
						interimList.clear();
					}
				}
			}
			if(interimList.size()>0){
				num = num + this.batchInsert(interimList);
				interimList.clear();
			}
			if (num != entityList.size()) {
				throw new BusinessException("导入数据异常!");
			}
			
			
		}
	}
	// 根据车牌号更新
	private int updateByPlateNumbery(FireEngine  fireEngine) {
		return fireEngineMapper.updateByPlateNumbery(fireEngine);
	}

	private int batchInsert(List<FireEngine> interimList) {
		return fireEngineMapper.batchInsert(interimList);
	}

	public void saveExcelTempData(List<FireEngineTemp> tempList) {
		if (tempList != null && tempList.size() > 0) {
			List<FireEngineTemp> interimList = new ArrayList<FireEngineTemp>();
			int j = (int) Math.ceil(tempList.size() / (double) 50);
			int num = 0;
			for (int i = 1; i <= j; i++) {
				if (i*50>=tempList.size()) {
					interimList = new ArrayList<FireEngineTemp>(tempList.subList((i - 1) * 50, tempList.size()));
					num =num+this.batchInsertTemp(interimList);
				}else {
					interimList = new ArrayList<FireEngineTemp>(tempList.subList((i - 1) * 50, i * 50));
					num =num+this.batchInsertTemp(interimList);
				}
			}
			if (num != tempList.size()) {
				throw new BusinessException("导入数据异常!");
			}
		}
	}

	private int batchInsertTemp(List<FireEngineTemp> interimList) {
		return fireEngineMapper.batchInsertTemp(interimList);
	}
	
	public List<FireEngineTemp> qryExcelTemp(Long timestamp) {
		return fireEngineMapper.qryExcelTemp(timestamp);
	}

	public void delExcelTemp(Long timestamp) {
		fireEngineMapper.delExcelTemp(timestamp);
	}

	public Page<FireEngineVo> qryFireEngineByBrigandeId(Page<FireEngineVo> page, Map<String, Object> params) {
		page.setRecords(fireEngineMapper.qryFireEngineByBrigandeId(page,params));	
		return page;
	}
	
	public FireEngineVo qryCarCountByBrigade(Long id) {
		return fireEngineMapper.qryCarCountByBrigade(id);
	}
	
}
