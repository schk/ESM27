package cn.stronglink.esm27.module.system.department.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.Department;
import cn.stronglink.esm27.module.system.department.vo.DepartmentTreeNodeVo;

public interface DepartmentMapper extends BaseMapper<Department> {
	
	public List<DepartmentTreeNodeVo> qryDepts();
	
	public List<DepartmentTreeNodeVo> qryRootDept();

	public int getChildDetpCount(@Param("id")Long id);

	public List<DepartmentTreeNodeVo> getChildList(@Param("pid") Long id);

	public List<DepartmentTreeNodeVo> qryRootDeptNoTop();

}
