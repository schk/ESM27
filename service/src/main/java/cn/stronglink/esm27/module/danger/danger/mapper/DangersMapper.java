package cn.stronglink.esm27.module.danger.danger.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.esm27.entity.Dangers;
import cn.stronglink.esm27.entity.DangersCatalog;
import cn.stronglink.esm27.entity.KeyUnitDangers;
import cn.stronglink.esm27.module.danger.danger.vo.DangersVo;
import cn.stronglink.esm27.module.danger.danger.vo.DangersWithTypeVo;

public interface DangersMapper extends BaseMapper<Dangers> {

	List<DangersVo> qryListByParams(Page<DangersVo> page, Map<String, Object> params);
	
	List<KeyUnitDangers> qryDangersByKeyParts(Map<String, Object> params);

	List<DangersCatalog> selectDangersCatalogByDangersId(@Param("id") Long id);
	
	DangersVo queryDangersById(@Param("id")Long id);

	List<DangersCatalog> getDangersCatalogByDangersId(Long id);

	void createDangers(DangersVo entity);

	void createDangersCatalogs(DangersVo entity);

	void deleteDangersCatalogsById(@Param("dangersId")Long dangersId);

	List<DangersWithTypeVo> selectAll(Page<DangersWithTypeVo> page);
	
	DangersWithTypeVo selectOneById(String dangersId);

	List<KeyUnitDangers> qryDangersByKeyUnit(Map<String, Object> params);

}
