package cn.stronglink.esm27.module.equipment.equipment.vo;

import java.util.List;

import cn.stronglink.esm27.entity.Equipment;

public class EquipmentVo extends Equipment{

	private static final long serialVersionUID = 1L;
	
	private String keyUnitName;
	private String equipmentTypeName;
	private String distances;
	private List<EquipmentDocParam> equipmentDocs;
	public String getKeyUnitName() {
		return keyUnitName;
	}
	public void setKeyUnitName(String keyUnitName) {
		this.keyUnitName = keyUnitName;
	}
	public String getEquipmentTypeName() {
		return equipmentTypeName;
	}
	public void setEquipmentTypeName(String equipmentTypeName) {
		this.equipmentTypeName = equipmentTypeName;
	}
	public List<EquipmentDocParam> getEquipmentDocs() {
		return equipmentDocs;
	}
	public void setEquipmentDocs(List<EquipmentDocParam> equipmentDocs) {
		this.equipmentDocs = equipmentDocs;
	}
	public String getDistances() {
		return distances;
	}
	public void setDistances(String distances) {
		this.distances = distances;
	}
	
	
	
}
