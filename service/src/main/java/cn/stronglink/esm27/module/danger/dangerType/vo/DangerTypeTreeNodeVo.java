package cn.stronglink.esm27.module.danger.dangerType.vo;

import java.util.List;

import cn.stronglink.esm27.entity.DangerType;

public class DangerTypeTreeNodeVo extends DangerType{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<DangerTypeTreeNodeVo> children;

	public List<DangerTypeTreeNodeVo> getChildren() {
		return children;
	}

	public void setChildren(List<DangerTypeTreeNodeVo> children) {
		this.children = children;
	}

	

}
