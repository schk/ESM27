package cn.stronglink.esm27.module.danger.danger.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.Dangers;
import cn.stronglink.esm27.entity.DangersCatalog;
import cn.stronglink.esm27.entity.KeyUnitDangers;
import cn.stronglink.esm27.module.danger.danger.service.DangersService;
import cn.stronglink.esm27.module.danger.danger.vo.DangersVo;

@Controller
@RequestMapping(value = "dangers")
public class DangerController extends AbstractController {

	@Autowired
	private DangersService dangerService;
	
	/**
	 * 查询列表
	 */
	@RequestMapping(value = "qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<DangersVo> page = (Page<DangersVo>) super.getPage(params);
		Page<DangersVo> data = dangerService.qryListByParams(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询重点部位下的危化品列表
	 */
	@RequestMapping(value = "qryDangersByKeyParts")
	public ResponseEntity<ModelMap> qryDangersByKeyParts(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		List<KeyUnitDangers> data = dangerService.qryDangersByKeyParts(params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询重点单位下的危化品列表
	 */
	@RequestMapping(value = "qryDangersByKeyUnit")
	public ResponseEntity<ModelMap> qryDangersByKeyUnit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		List<KeyUnitDangers> data = dangerService.qryDangersByKeyUnit(params);
		return setSuccessModelMap(modelMap, data);
	}
	
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		DangersVo entity =dangerService.selectById(id);
		return setSuccessModelMap(modelMap, entity);
	}
	
	@OperateLog(module = "危化品管理",desc="添加危化品", type = OpType.ADD)
	@RequestMapping(value = "create")
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody DangersVo entity) {
		dangerService.createDangersVo(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	@OperateLog(module = "危化品管理",desc="修改危化品", type = OpType.UPDATE)
	@RequestMapping(value = "edit")
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody DangersVo entity) {
		dangerService.updateDangersVo(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	@OperateLog(module = "危化品管理",desc="删除危化品", type = OpType.DEL)
	@RequestMapping(value = "remove")
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		dangerService.deleteDangersVo(id);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 查询危化品文档信息
	 * @param modelMap
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "qryDangerDoc")
	public ResponseEntity<ModelMap> qryDangerDoc(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		Dangers entity = dangerService.selectById(id);
		List<DangersCatalog> catalogs = dangerService.qryDangerDoc(id);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("dangers", entity);
		map.put("catalogs", catalogs);
		return setSuccessModelMap(modelMap,map);
	} 
	
	

}
