package cn.stronglink.esm27.module.facilities.vo;

import java.util.List;

public class FacilitiesUnitVo {

	/**
	 * 
	 */
	private Long keyUnitId;
	private String keyUnitName;
	private List<FacilitiesVo>  fireWaterSourceList;
	private int  fireWaterSourceCount;
	
	public String getKeyUnitName() {
		return keyUnitName;
	}
	public void setKeyUnitName(String keyUnitName) {
		this.keyUnitName = keyUnitName;
	}
	public int getFireWaterSourceCount() {
		return fireWaterSourceCount;
	}
	public void setFireWaterSourceCount(int fireWaterSourceCount) {
		this.fireWaterSourceCount = fireWaterSourceCount;
	}
	public Long getKeyUnitId() {
		return keyUnitId;
	}
	public void setKeyUnitId(Long keyUnitId) {
		this.keyUnitId = keyUnitId;
	}
	public List<FacilitiesVo> getFireWaterSourceList() {
		return fireWaterSourceList;
	}
	public void setFireWaterSourceList(List<FacilitiesVo> fireWaterSourceList) {
		this.fireWaterSourceList = fireWaterSourceList;
	}
	

	

}
