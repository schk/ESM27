package cn.stronglink.esm27.module.fireEngine.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.FireEngineEquipment;
import cn.stronglink.esm27.module.fireEngine.service.FireEngineEquipmentService;
import cn.stronglink.esm27.module.fireEngine.vo.FireEngineEquipmentVo;

@Controller
@RequestMapping("fireEngineEquipment")
public class FireEngineEquipmentController extends AbstractController {
	
	@Autowired
	private  FireEngineEquipmentService fireEngineEquipmentService;
	
	/**
	 * 查询列表
	 */
	@RequestMapping(value = "qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<FireEngineEquipmentVo> page = (Page<FireEngineEquipmentVo>) super.getPage(params);
		Page<FireEngineEquipmentVo> data = fireEngineEquipmentService.qryListByParams(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	
	/**
	 * 通过id查询
	 */
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> qryUserById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		FireEngineEquipment vo = fireEngineEquipmentService.qryById(id);
		return setSuccessModelMap(modelMap, vo);
	}
	
	/**
	 * 新建
	 */
	@RequestMapping(value = "create")
	@OperateLog(module = "随车器材管理",desc="添加随车器材", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody FireEngineEquipment entity) {
		fireEngineEquipmentService.insert(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 修改
	 */
	@RequestMapping(value = "edit")
	@OperateLog(module = "随车器材管理",desc="修改随车器材", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody FireEngineEquipment entity) {
		fireEngineEquipmentService.update(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 删除用户
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "remove")
	@OperateLog(module = "随车器材管理",desc="删除随车器材", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		fireEngineEquipmentService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}
	


}
