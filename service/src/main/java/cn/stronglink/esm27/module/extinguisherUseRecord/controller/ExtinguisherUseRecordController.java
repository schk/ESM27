package cn.stronglink.esm27.module.extinguisherUseRecord.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.esm27.module.extinguisherUseRecord.param.SearchParam;
import cn.stronglink.esm27.module.extinguisherUseRecord.service.ExtinguisherUseRecordService;
import cn.stronglink.esm27.module.extinguisherUseRecord.vo.FireBrigadeVo;

@Controller
@RequestMapping("extinguisherUseRecord")
public class ExtinguisherUseRecordController extends AbstractController {

	@Autowired
	private ExtinguisherUseRecordService extinguisherUseRecordService;
	
	
	@RequestMapping(value = "qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody SearchParam params) {
		Map<String,Object> data = extinguisherUseRecordService.qryListByParams(params);
		System.out.println(data);
		return setSuccessModelMap(modelMap, data);
	}
	
	@RequestMapping(value = "qryFireBrigade")
	public ResponseEntity<ModelMap> qryFireBrigade(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<FireBrigadeVo> data = extinguisherUseRecordService.qryFireBrigade();
		System.out.println(data);
		return setSuccessModelMap(modelMap, data);
	}
	
	
}
