package cn.stronglink.esm27.module.dutyequipuserecord.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Dictionary;
import cn.stronglink.esm27.module.dictionary.mapper.DictionaryMapper;

@Service
@Transactional(rollbackFor=Exception.class)
public class DutyEquipUseRecordService {
	
	@Autowired
	private DictionaryMapper dictionaryMapper;
	
	/*
	 * 根据type查询信息
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<Dictionary> qryByType(Integer type){
		return dictionaryMapper.qryByType(type);
	}
	
	/*
	 * 根据id查询信息
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Dictionary selectById(Long id){
		return dictionaryMapper.selectById(id);
	}
	
	/*
	 * 根据id删除信息
	 */
	public void  remove(Long id){
		dictionaryMapper.deleteById(id);
	}
	/*
	 * 添加信息
	 */
	public boolean insert(Dictionary entity) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("name_", entity.getName());
		List<Dictionary> list =dictionaryMapper.selectByMap(map);
		if(list.size()>0){
			return false;
		}
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		dictionaryMapper.insert(entity);
		return true;
	}
	//根据id修改信息
	public boolean update(Dictionary entity) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("name_", entity.getName());
		List<Dictionary> list =dictionaryMapper.selectByMap(map);
		if(list.size()>0){
			Dictionary p=list.get(0);
			if(!(p.getId().toString()).equals(entity.getId().toString())){
				return false;
			}
		}		
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if(dictionaryMapper.updateById(entity)==0){
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
		return true;
	}
	

}
