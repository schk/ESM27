package cn.stronglink.esm27.module.plotting.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Plotting;
import cn.stronglink.esm27.module.plotting.mapper.PlottingMapper;
import cn.stronglink.esm27.module.plotting.vo.PlottingCVo;

@Service
@Transactional(rollbackFor = Exception.class)
public class PlottingService {

	@Autowired
	private PlottingMapper plottingMapper;

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Page<PlottingCVo> qryListByParams(Page<PlottingCVo> page, Map<String, Object> params) {
		page.setRecords(plottingMapper.qryListByParams(page, params));
		return page;
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Plotting qryById(Long id) {
		return plottingMapper.selectById(id);
	}

	/*
	 * 增加信息
	 */
	public void insert(Plotting entity) {
		int type = plottingMapper.getMaxType(entity.getId());
		entity.setId(IdWorker.getId());
		entity.setType(type + 1);
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		plottingMapper.insert(entity);
	}

	/*
	 * 修改信息
	 */
	public void update(Plotting entity) {
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if (plottingMapper.updateById(entity) == 0) {
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
	}

	/*
	 * 删除对象
	 */
	public void remove(Long id) {
		plottingMapper.deleteById(id);
	}

	public List<Plotting> qryPlottingList() {
		return plottingMapper.qryPlottingList();
	}

	public Integer getCountByUsername(Plotting entity) {
		return plottingMapper.getCountByUsername(entity);
	}

	public List<Plotting> qryJhPlottingList() {
		return plottingMapper.qryJhPlottingList();
	}

}
