package cn.stronglink.esm27.module.system.user.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.esm27.entity.Role;
import cn.stronglink.esm27.entity.User;
import cn.stronglink.esm27.entity.UserRole;
import cn.stronglink.esm27.module.system.user.vo.UserVo;


public interface UserMapper extends BaseMapper<User>{

	List<UserVo> getUserByParams(Pagination page, Map<String, Object> params);

	List<Long> getRoleIds(@Param("userId") Long id);

	void insertUserRole(UserRole userRole);
	
	Integer getCountByUsername(User user);

	void deleteUserRole(@Param("userId") Long id);
	
	List<Role> getRoleOptions();

	List<HashMap<String, Object>> getUserOrginfo(@Param("id") Long id);

	List<String> getOrgTreeIds(List<String> childList);

	User selectUserById(@Param("userId") Long id);
	
	User selectUserByNamePass(@Param("username") String username,@Param("password") String password);

	void resetUserPW(User user);
	
	User findUserByName(@Param("username") String username);
	
	List<String> getSysPermissionByUsername(@Param("username") String username);
	
	//新建消防队id存关联表
	void insertUserBrigadeRef(@Param("id")Long id,@Param("userId")Long userId, @Param("fireBrigadeId")Long fireBrigadeId);

	//修改消防队id存关联表
	void updateUserBrigadeRef(@Param("userId")Long userId, @Param("fireBrigadeId")Long fireBrigadeId);

	Long selectUserBrigade(@Param("userId") Long userId);
	
	User selectUserAndBrigade(@Param("userId") Long userId);
	
	Long selectDeptId(@Param("userId") Long userId);

	Long findBrigedeIdByUserId(@Param("userId") Long userId);
	
	Long findDeptIdByName(@Param("deptName") String deptName);
	
	Long findBrigadeIdByName(@Param("brigadeName") String brigadeName);
	
	int batchInsertUser(@Param("interimList") List<User> userList);
	
	int batchInsertUserBrigadeRef(@Param("interimList") List<User> userList);

	void deleteBrigadeRef(@Param("userId") Long id);
}
