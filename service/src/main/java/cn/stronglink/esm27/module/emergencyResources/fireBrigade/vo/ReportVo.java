package cn.stronglink.esm27.module.emergencyResources.fireBrigade.vo;

public class ReportVo {
	
	private String name;
	private String desc;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}

	

}
