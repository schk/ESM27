package cn.stronglink.esm27.module.geographic.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.GeographicEdit;
import cn.stronglink.esm27.entity.Resource;
import cn.stronglink.esm27.module.geographic.mapper.GeographicMapper;
import cn.stronglink.esm27.web.webData.mapper.WebDataMapper;
import cn.stronglink.esm27.web.webData.util.CoordTranform;
import cn.stronglink.esm27.web.webData.vo.MapFeaturesVo;
import cn.stronglink.esm27.web.webData.vo.MapGeometryVo;
import cn.stronglink.esm27.web.webData.vo.MapPropertiesVo;
import cn.stronglink.esm27.web.webData.vo.MapVo;

@Service
@Transactional(rollbackFor=Exception.class)
public class GeographicService {

	@Autowired
	private GeographicMapper geographicEditMapper;
	@Autowired
	private WebDataMapper webDataMapper;	

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public MapVo qryListByParams(String cateString) {
		MapVo mapvo=new MapVo();
		List<MapFeaturesVo> mapFeaturesVo =new ArrayList<MapFeaturesVo>();
		HashMap<String,Object> param = new HashMap<>();
		mapvo.setType("FeatureCollection");
		if(cateString.contains("zddw")) {
			List<Map<String,Object>> list =webDataMapper.qryDataForMapZddw(param);
			if(list!=null&&list.size()>0) {
				for(Map<String,Object> map:list) {
					if(map!=null) {
						if(!"".equals(map.get("lon"))&&!"".equals(map.get("lat"))&&map.get("lon")!=null&&map.get("lat")!=null){
						double[] coord = {Double.valueOf(map.get("lon").toString()),Double.valueOf(map.get("lat").toString())};
						double[] point = CoordTranform.lnglat2mercator(coord);
						MapGeometryVo geometry =new MapGeometryVo();
						List<Object> coordinates =new ArrayList<Object>();
						coordinates.add(point[0]);
						coordinates.add(point[1]);
						geometry.setCoordinates(coordinates);
						geometry.setType("Point");
						
						MapPropertiesVo properties =new MapPropertiesVo();
						properties.setCatecd("zddw");
						properties.setPoint(point[0]+"_"+point[1]);
						properties.setId(map.get("id").toString());
						properties.setName(map.get("name").toString());
						
						MapFeaturesVo features =new MapFeaturesVo();
						features.setGeometry(geometry);
						features.setProperties(properties);
						features.setType("Feature");
						features.setId(map.get("id").toString());
						mapFeaturesVo.add(features);
						}
					}
				}
			}
		}
        if(cateString.contains("xfsy")) {
			List<Map<String,Object>> list =webDataMapper.qryDataForMapXfsy(param);
			if(list!=null&&list.size()>0) {
				for(Map<String,Object> map:list) {
					if(map!=null) {
						if(!"".equals(map.get("lon"))&&!"".equals(map.get("lat"))&&map.get("lon")!=null&&map.get("lat")!=null){
						double[] coord = {Double.valueOf(map.get("lon").toString()),Double.valueOf(map.get("lat").toString())};
						double[] point = CoordTranform.lnglat2mercator(coord);
						MapGeometryVo geometry =new MapGeometryVo();
						List<Object> coordinates =new ArrayList<Object>();
						coordinates.add(point[0]);
						coordinates.add(point[1]);
						geometry.setCoordinates(coordinates);
						geometry.setType("Point");
						
						MapPropertiesVo properties =new MapPropertiesVo();
						properties.setCatecd("xfsy");
						properties.setPoint(point[0]+"_"+point[1]);
						properties.setId(map.get("id").toString());
						properties.setName(map.get("name").toString());
						
						MapFeaturesVo features =new MapFeaturesVo();
						features.setGeometry(geometry);
						features.setProperties(properties);
						features.setType("Feature");
						features.setId(map.get("id").toString());
						mapFeaturesVo.add(features);
						}
					}
				}
			}
		}
        if(cateString.contains("xfs")) {
			List<Map<String,Object>> sheshiList =webDataMapper.qryDataForMapXfsS(param);
			if(sheshiList!=null&&sheshiList.size()>0) {
				for(Map<String,Object> map:sheshiList) {
					if(map!=null) {
						if(!"".equals(map.get("lon"))&&!"".equals(map.get("lat"))&&map.get("lon")!=null&&map.get("lat")!=null){
						double[] coord = {Double.valueOf(map.get("lon").toString()),Double.valueOf(map.get("lat").toString())};
//						double[] pointwgs =MapLonLatUtil.bd09towgs84(coord[0],coord[1]);
					//	double[] pointwgs =MapLonLatUtil.bd09py(coord[0],coord[1]);
						double[] point = CoordTranform.lnglat2mercator(coord);
						MapGeometryVo geometry =new MapGeometryVo();
						List<Object> coordinates =new ArrayList<Object>();
						coordinates.add(point[0]);
						coordinates.add(point[1]);
						geometry.setCoordinates(coordinates);
						geometry.setType("Point");
						
						MapPropertiesVo properties =new MapPropertiesVo();
						properties.setCatecd("xfs");
						properties.setPoint(point[0]+"_"+point[1]);
						properties.setId(map.get("id").toString());
						properties.setName(map.get("name").toString());
						
						MapFeaturesVo features =new MapFeaturesVo();
						features.setGeometry(geometry);
						features.setProperties(properties);
						features.setType("Feature");
						features.setId(map.get("id").toString());
						mapFeaturesVo.add(features);
						}
					}
				}
			}
        }
        
        if(cateString.contains("yjdw")) {
			List<Map<String,Object>> list =webDataMapper.qryDataForMapYjdw(param);
			if(list!=null&&list.size()>0) {
				for(Map<String,Object> map:list) {
					if(map!=null) {
						if(!"".equals(map.get("lon"))&&!"".equals(map.get("lat"))&&map.get("lon")!=null&&map.get("lat")!=null){
						double[] coord = {Double.valueOf(map.get("lon").toString()),Double.valueOf(map.get("lat").toString())};
						double[] point = CoordTranform.lnglat2mercator(coord);
						MapGeometryVo geometry =new MapGeometryVo();
						List<Object> coordinates =new ArrayList<Object>();
						coordinates.add(point[0]);
						coordinates.add(point[1]);
						geometry.setCoordinates(coordinates);
						geometry.setType("Point");
						
						MapPropertiesVo properties =new MapPropertiesVo();
						properties.setCatecd("yjdw");
						properties.setPoint(point[0]+"_"+point[1]);
						properties.setId(map.get("id").toString());
						properties.setName(map.get("name").toString());
						
						MapFeaturesVo features =new MapFeaturesVo();
						features.setGeometry(geometry);
						features.setProperties(properties);
						features.setType("Feature");
						features.setId(map.get("id").toString());
						mapFeaturesVo.add(features);
						}
					}
				}
			}
		}
        if(cateString.contains("xfdw")) {
			List<Map<String,Object>> list =webDataMapper.qryDataForMapXfdw(param);
			if(list!=null&&list.size()>0) {
				for(Map<String,Object> map:list) {
					if(map!=null) {
						if(!"".equals(map.get("lon"))&&!"".equals(map.get("lat"))&&map.get("lon")!=null&&map.get("lat")!=null){
						double[] coord = {Double.valueOf(map.get("lon").toString()),Double.valueOf(map.get("lat").toString())};
						double[] point = CoordTranform.lnglat2mercator(coord);
						MapGeometryVo geometry =new MapGeometryVo();
						List<Object> coordinates =new ArrayList<Object>();
						coordinates.add(point[0]);
						coordinates.add(point[1]);
						geometry.setCoordinates(coordinates);
						geometry.setType("Point");
						
						MapPropertiesVo properties =new MapPropertiesVo();
						properties.setCatecd("xfdw");
						properties.setPoint(point[0]+"_"+point[1]);
						properties.setId(map.get("id").toString());
						properties.setName(map.get("name").toString());
						
						MapFeaturesVo features =new MapFeaturesVo();
						features.setGeometry(geometry);
						features.setProperties(properties);
						features.setType("Feature");
						features.setId(map.get("id").toString());
						mapFeaturesVo.add(features);
						}
					}
				} 
			}
		}
        
      if(cateString.contains("sczz")) {
			List<Map<String,Object>> list =webDataMapper.qryDataForMapSczz(param);
			if(list!=null&&list.size()>0) {
				for(Map<String,Object> map:list) {
					if(map!=null) {
						if(!"".equals(map.get("lon"))&&!"".equals(map.get("lat"))&&map.get("lon")!=null&&map.get("lat")!=null){
						double[] coord = {Double.valueOf(map.get("lon").toString()),Double.valueOf(map.get("lat").toString())};
						double[] point = CoordTranform.lnglat2mercator(coord);
						MapGeometryVo geometry =new MapGeometryVo();
						List<Object> coordinates =new ArrayList<Object>();
						coordinates.add(point[0]);
						coordinates.add(point[1]);
						geometry.setCoordinates(coordinates);
						geometry.setType("Point");
						
						MapPropertiesVo properties =new MapPropertiesVo();
						properties.setCatecd("sczz");
						properties.setPoint(point[0]+"_"+point[1]);
						properties.setId(map.get("id").toString());
						properties.setName(map.get("name").toString());
						
						MapFeaturesVo features =new MapFeaturesVo();
						features.setGeometry(geometry);
						features.setProperties(properties);
						features.setType("Feature");
						features.setId(map.get("id").toString());
						mapFeaturesVo.add(features);
						}
					}
				}
			}
		}
        
        if(cateString.contains("wzcbd")) {
			List<Map<String,Object>> list =webDataMapper.qryDataForMapWzcbd(param);
			if(list!=null&&list.size()>0) {
				for(Map<String,Object> map:list) {
					if(map!=null) {
						if(!"".equals(map.get("lon"))&&!"".equals(map.get("lat"))&&map.get("lon")!=null&&map.get("lat")!=null){
							double[] coord = {Double.valueOf(map.get("lon").toString()),Double.valueOf(map.get("lat").toString())};
							double[] point = CoordTranform.lnglat2mercator(coord);
							MapGeometryVo geometry =new MapGeometryVo();
							List<Object> coordinates =new ArrayList<Object>();
							coordinates.add(point[0]);
							coordinates.add(point[1]);
							geometry.setCoordinates(coordinates);
							geometry.setType("Point");
							
							MapPropertiesVo properties =new MapPropertiesVo();
							properties.setCatecd("wzcbd");
							properties.setPoint(point[0]+"_"+point[1]);
							properties.setId(map.get("id").toString());
							properties.setName(map.get("name").toString());
							
							MapFeaturesVo features =new MapFeaturesVo();
							features.setGeometry(geometry);
							features.setProperties(properties);
							features.setType("Feature");
							features.setId(map.get("id").toString());
							mapFeaturesVo.add(features);
						}
				   }
			   }
			}
		}
		mapvo.setFeatures(mapFeaturesVo);
		return mapvo;
	}
	
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public GeographicEdit qryById(Long id) {		
		return geographicEditMapper.selectById(id);
	}

	public Long insert(GeographicEdit entity) {
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(new Date());
		geographicEditMapper.insert(entity);
		return entity.getId();
	}

	public void update(GeographicEdit entity) {
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if(geographicEditMapper.updateById(entity)==0){
			throw new BusinessException("记录不存在!");
		}
	}

	public void remove(Long id) {
		geographicEditMapper.deleteById(id);
	}


	public List<GeographicEdit> getRoadList() {
		return geographicEditMapper.getRoadList();
	}


	public List<Resource> queryResource() {
		return geographicEditMapper.queryResource();
	}

	

}
