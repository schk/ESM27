package cn.stronglink.esm27.module.emergencyResources.emergencyMaterial.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.EmergencyMaterial;
import cn.stronglink.esm27.entity.EmergencyMaterialTemp;
import cn.stronglink.esm27.entity.EmergencyMaterialType;
import cn.stronglink.esm27.module.emergencyResources.emergencyMaterial.mapper.EmergencyMaterialMapper;
import cn.stronglink.esm27.module.emergencyResources.emergencyMaterial.vo.EmergencyMaterialVo;
import cn.stronglink.esm27.module.emergencyResources.fireBrigade.mapper.FireBrigadeMapper;
import cn.stronglink.esm27.module.unit.mapper.KeyUnitMapper;

@Service
@Transactional(rollbackFor=Exception.class)
public class EmergencyMaterialService {
	
	@Autowired
	private EmergencyMaterialMapper emergencyMaterialMapper;
	@Autowired
	private KeyUnitMapper keyUnitMapper;
	@Autowired
	private FireBrigadeMapper fireBrigadeMapper;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<EmergencyMaterialVo> qryListByParams(Page<EmergencyMaterialVo> page, Map<String, Object> params) {
		List<EmergencyMaterialVo> list=emergencyMaterialMapper.qryListByParams(page,params);
		if(list!=null&&list.size()>0) {
			for(EmergencyMaterialVo vo:list) {
				if(vo.getType()==1) {
					String name=keyUnitMapper.getUnitName(vo.getKeyUnitId());
					vo.setKeyUnitName(name);
				}else {
					String name=fireBrigadeMapper.getfireBrigadeName(vo.getKeyUnitId());
					vo.setKeyUnitName(name);
				}
			}
		}
		page.setRecords(list);	
		return page;
	}
	
	/*
	 * 根据id查询信息
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public EmergencyMaterialVo selectById(Long id){
		EmergencyMaterialVo item =emergencyMaterialMapper.qryById(id);
		if(item.getType()==1) {
			String name=keyUnitMapper.getUnitName(item.getKeyUnitId());
			item.setKeyUnitName(name);
		}else {
			String name=fireBrigadeMapper.getfireBrigadeName(item.getKeyUnitId());
			item.setKeyUnitName(name);
		}
		return item;
	}
	
	/*
	 * 根据id删除信息
	 */
	public void  remove(Long id){
		emergencyMaterialMapper.deleteById(id);
	}
	/*
	 * 添加信息
	 */
	public void insert(EmergencyMaterial entity) {
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		emergencyMaterialMapper.insert(entity);
	}
	//根据id修改信息
	public void update(EmergencyMaterial entity) {
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if(emergencyMaterialMapper.updateById(entity)==0){
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
	}


	/***
	 * 批量导入
	 * @param listentity
	 */
	public void exportEXLEmergencyMaterial(List<EmergencyMaterial> listentity) {
		if (listentity!=null&&listentity.size()>0) {
			int size = listentity.size();
			List<EmergencyMaterial> interimList = new ArrayList<EmergencyMaterial>();
			int j =  (int)Math.ceil(size/(double)50);
			int num =0;
			for (int i = 1; i <= j;i++) {
				if (i*50>=size) {
					for (int k = (i - 1) * 50; k < size; k++) {
						interimList.add(listentity.get(k));
					}
					num =num+this.batchInsertEmergencyMaterial(interimList);
				}else {
					interimList = listentity.subList((i - 1) * 50, i * 50);
					num =num+this.batchInsertEmergencyMaterial(interimList);
				}
			}
			if (num!=size) {
				throw new BusinessException("导入数据异常!");
			}
		}	
	}
	//批量导入
	public int batchInsertEmergencyMaterial(List<EmergencyMaterial> interimList) {
		return emergencyMaterialMapper.batchInsertEmergencyMaterial(interimList);
	}

	public void exportEXLEmergencyMaterialTemp(List<EmergencyMaterialTemp> emergencyMaterialTempList) {
		List<EmergencyMaterialTemp> interimList = new ArrayList<EmergencyMaterialTemp>();
		int j =  (int)Math.ceil(emergencyMaterialTempList.size()/(double)50);
		int num =0;
		for (int i = 1; i <= j;i++) {
			if (i*50>=emergencyMaterialTempList.size()) {
				interimList = new ArrayList<EmergencyMaterialTemp>(emergencyMaterialTempList.subList((i - 1) * 50, emergencyMaterialTempList.size()));
				num =num+this.batchInsertEXLEmergencyMaterialTemp(interimList);
			}else {
				interimList = new ArrayList<EmergencyMaterialTemp>(emergencyMaterialTempList.subList((i - 1) * 50, i * 50));
				num =num+this.batchInsertEXLEmergencyMaterialTemp(interimList);
			}
		}
		if (num!=emergencyMaterialTempList.size()) {
			throw new BusinessException("导入数据异常!");
		}	
	}
	private int batchInsertEXLEmergencyMaterialTemp(List<EmergencyMaterialTemp> interimList) {
		return emergencyMaterialMapper.batchInsertEXLEmergencyMaterialTemp(interimList);
	}

	public List<EmergencyMaterialType> qryEmergencySuppliesType() {
		return emergencyMaterialMapper.qryEmergencySuppliesType();
	}

	public List<EmergencyMaterialTemp> qryEmergencyMaterialTemp(Long timestamp) {
		return emergencyMaterialMapper.qryEmergencyMaterialTemp(timestamp);
	}

	public void insertEmergencySuppliesType(EmergencyMaterialType entity) {
		emergencyMaterialMapper.insertEmergencySuppliesType(entity);
	}

	public void exportEXLExpert(List<EmergencyMaterial> expertList) {
		emergencyMaterialMapper.batchInsertEmergencyMaterial(expertList);
		
	}

	public void delExpertTemp(Long timestamp) {
		emergencyMaterialMapper.delExpertTemp(timestamp);
	}
}
