package cn.stronglink.esm27.module.fireEngine.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.stronglink.core.util.DateUtil;
import cn.stronglink.esm27.entity.FireBrigade;
import cn.stronglink.esm27.entity.FireEngine;
import cn.stronglink.esm27.entity.FireEngineUseRecord;
import cn.stronglink.esm27.entity.Room;
import cn.stronglink.esm27.module.emergencyResources.emergencyMaterial.mapper.MaterialUseRecordMapper;
import cn.stronglink.esm27.module.emergencyResources.fireBrigade.vo.ReportVo;
import cn.stronglink.esm27.module.fireEngine.mapper.FireEngineMapper;
import cn.stronglink.esm27.module.fireEngine.mapper.FireEngineUseRecordMapper;
import cn.stronglink.esm27.module.fireEngine.vo.FireEngineTypeVo;
import cn.stronglink.esm27.module.incidentRecord.mapper.IncidentRecordMapper;
import cn.stronglink.esm27.module.mapCamera.mapper.MapCameraMapper;
import cn.stronglink.esm27.web.webData.mapper.mappers.RoomMapper;
import cn.stronglink.esm27.web.webData.vo.IncidentRecordVo;

@Service
@Transactional(rollbackFor=Exception.class)
public class FireEngineUseRecordService {
	
	@Autowired
	private FireEngineUseRecordMapper fireEngineUseRecordMapper;
	@Autowired
	private MaterialUseRecordMapper materialUseRecordMapper;
	@Autowired
	private RoomMapper roomMapper;
	@Autowired
	private FireEngineMapper fireEngineMapper;
	@Autowired
	private IncidentRecordMapper incidentRecordMapper;
	@Autowired
	private MapCameraMapper mapCameraMapper;
	
	
	public void insertFireEngineUseRecord(FireEngineUseRecord entity) {
		entity.setCreateTime(new Date());
		entity.setUpdateTime(new Date());
		fireEngineUseRecordMapper.insert(entity);		
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Map<String, Object> getReportContent(String roomId) {
		//查询事故基本信息
		Map<String, Object> map =new HashMap<String, Object>(); 
		Room room = roomMapper.selectById(roomId);	
		map.put("room", room);	
		if(room!=null) {
			    //事故记录
			    List<IncidentRecordVo> incidentRecord = incidentRecordMapper.getIncidentRecordByRoomId(roomId);	    
			    if(incidentRecord!=null&&incidentRecord.size()>0) {
			    	for(IncidentRecordVo vo:incidentRecord) {
			    		String diffTime = DateUtil.getDistanceTime(vo.getCreateTime(),vo.getRoomCreateTime());
			    		vo.setTimeDifference(diffTime);
			    	}
			    }
			    map.put("incidentRecord", incidentRecord);	   
			    //命令记录记录
			    List<IncidentRecordVo> minglingRecord = incidentRecordMapper.getIncidentMingLingRecordByRoomId(roomId);	    
			    map.put("minglingRecord", minglingRecord);	 
			   
			    //出警战队
			    //   String fireBrigadeNames ="";
			    int count =0;
			    List<ReportVo> reportDescList=new ArrayList<ReportVo>();
			    List<FireBrigade> fireBrigadeList =fireEngineUseRecordMapper.getFireBrigade(roomId);
			    if(fireBrigadeList!=null&&fireBrigadeList.size()>0) {
			    	for(FireBrigade fireBrigade:fireBrigadeList) {
			    		count=count+1;
			    		//每个消防队下出了多少个消防车
			    		List<String> brigade=fireEngineUseRecordMapper.getFireEngineByBrigadeId(fireBrigade.getId(),roomId);
			    		String brigadeStr="";
			    		if(brigade.size()>0) {
			    			brigadeStr=String.join(",", brigade);
			    		}
			    		//每个消防队消耗了多少灭火剂
			    		//Double extinguisher=extinguisherUseRecordMapper.getExtinguisherByBrigadeId(fireBrigade.getId(),roomId);
			    		ReportVo reportVo =new ReportVo();
			    		String desc=fireBrigade.getName()+",出了"+brigade.size()+"辆消防车,分别是【"+brigadeStr+"】";
			    		reportVo.setDesc(desc);
			    		reportDescList.add(reportVo);
			    	}
			    }
			  //  map.put("fireBrigadeNames", fireBrigadeNames);
			    
		    	map.put("fireBrigadeCount", count);
		    	map.put("reportDescList", reportDescList);	 
			    //消耗器材
			    int equipCount =fireEngineUseRecordMapper.getEquipByRoomId(roomId);
			    map.put("equipCount", equipCount);
			    
			    //调用消防车
			    int fireEngineCount =fireEngineUseRecordMapper.getFireEngineByRoomId(roomId);
			    map.put("fireEngineCount", fireEngineCount);	    
			    //消防物资
			    int materialCount =materialUseRecordMapper.getMaterialCountByRoomId(roomId);
			    map.put("materialCount", materialCount);	    
			   //救援部署图
			    List<String> imgList =mapCameraMapper.selectList(roomId);
			    map.put("imgList", imgList);
		}
		return map;
	}

	public void updateFireEngineStatus(Long id) {
		FireEngine en =new FireEngine();
		en.setId(id);
		en.setStatus(2);
		fireEngineMapper.updateById(en);
	}

	public Map<String, Object> getCZLLData(Long roomId) {
		Map<String,Object> result=new HashMap<String,Object>();
		List<String> listColumns1 = new ArrayList<String>();
		Map<String,String>  mapColumns3 = new HashMap<String,String>();
		List<String> listColumns2 = new ArrayList<String>();
		listColumns2.add("水");
		listColumns2.add("泡沫");
		listColumns2.add("干粉");
		
		List<Map<String,String>> xfdDatas=fireEngineUseRecordMapper.getXFDList(roomId);
		if(xfdDatas!=null&&xfdDatas.size()>0){
			// 表格每行数据
			List<Object[]> data = new ArrayList<Object[]>();
			List<FireEngineTypeVo> fireEngineTypeList=fireEngineUseRecordMapper.getFireEngineTypeList(roomId);
			for(FireEngineTypeVo item:fireEngineTypeList){
				listColumns1.add(item.getTypeName());
				mapColumns3.put(item.getTypeName(),item.getDicId()+"");
			}
			for(int i=0;i<xfdDatas.size();i++){
				//  每个消防队下按车辆类别分组
				List<Map<String,String>> ceDatas= fireEngineUseRecordMapper.getXFCList(roomId, String.valueOf(xfdDatas.get(i).get("fire_brigade_id")));
				double water =0.0;
				double capacity=0.0;
				double powderQuantity=0.0; 
				Object[] rowobj = new Object[listColumns1.size()+4];
				rowobj[0]=xfdDatas.get(i).get("bridgeName");
			    for(int m=0;m<listColumns1.size();m++ ){
			    	String lbName=listColumns1.get(m);
			    	String lbId=mapColumns3.get(lbName);
			    	boolean flag=false;
			    	for(int j=0;j<ceDatas.size();j++){
			    		String lbid2=String.valueOf(ceDatas.get(j).get("id_"));
			    		if(lbId.equals(lbid2)){
							rowobj[m+1]=ceDatas.get(j).get("cTypeCount");
							water+=Double.parseDouble(String.valueOf(ceDatas.get(j).get("water")));
							capacity+=Double.parseDouble(String.valueOf(ceDatas.get(j).get("capacity")));
							powderQuantity+=Double.parseDouble(String.valueOf(ceDatas.get(j).get("powderQuantity")));
							flag=true;
							break;
			    		}
			    		
			    	}
			    	if(flag){
			    		continue;
			    	}
			    	rowobj[m+1]=0;
	
				}
				rowobj[listColumns1.size()+1]=water;
				rowobj[listColumns1.size()+2]=capacity;
				rowobj[listColumns1.size()+3]=powderQuantity;
				data.add(rowobj);
			}
			
		result.put("data", data);
		}
		result.put("columns1", listColumns1);
		result.put("columns2", listColumns2);
		return result;
	}

	public void updateFireEngineStatusByRoomId(Long roomId) {
		fireEngineMapper.updateFireEngineStatusByRoomId(roomId);
	}
	


}
