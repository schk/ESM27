package cn.stronglink.esm27.module.system.role.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.Role;
import cn.stronglink.esm27.module.system.role.service.RoleService;
import cn.stronglink.esm27.module.system.role.vo.RoleVo;

@Controller
@RequestMapping(value = "system/role")
public class RoleController extends AbstractController  {
	
	@Autowired
	private RoleService roleService;
	
	/**
	 * 获取所有角色
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryRoleOption")
	public ResponseEntity<ModelMap> qryRoles(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<Role> list = roleService.qryRoleOption();
		return setSuccessModelMap(modelMap, list);
	}
	
	/**
	 * 获取角色列表分页
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryRoleList")
	public ResponseEntity<ModelMap> qryRoleList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<RoleVo> page = (Page<RoleVo>) super.getPage(params);
		Role role = new Role();
		if(params.get("name") != null){
			role.setName(params.get("name").toString());
		}
		Page<RoleVo> data = roleService.getRoleByParams(page,role);
		return setSuccessModelMap(modelMap, data);
	}
	
	
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> qryById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		Role role = roleService.qryRoleById(id);
		return setSuccessModelMap(modelMap, role);
	}
	
	@RequestMapping(value = "create")
	@OperateLog(module = "角色管理",desc="添加角色", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Role entity) {
		// 查询当前角色名称是否存在
		Integer count = roleService.getCountByUsername(entity);
		if(count > 0){
			throw new BusinessException("角色名称已存在");
		}
		int i = roleService.getRoleByName(entity.getName());
		if(i > 0){
			throw new BusinessException("角色已经存在！");
		}
		roleService.insertRole(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	@RequestMapping(value = "edit")
	@OperateLog(module = "角色管理",desc="修改角色", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Role entity) {
		// 查询当前角色名称是否存在
		Integer count = roleService.getCountByUsername(entity);
		if(count > 0){
			throw new BusinessException("角色名称已存在");
		}
		roleService.updateRole(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	@RequestMapping(value = "remove")
	@OperateLog(module = "角色管理",desc="删除角色", type = OpType.DEL)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		Integer counts=roleService.getRoleUsers(id);
		if (counts>0) {
			throw new BusinessException("角色包含用户，不能删除");
		}
		roleService.removeRole(id);
		return setSuccessModelMap(modelMap, null);
	}
	
}
