package cn.stronglink.esm27.module.incidentRecord.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.esm27.entity.IncidentRecord;
import cn.stronglink.esm27.web.webData.vo.IncidentRecordVo;


public interface IncidentRecordMapper extends BaseMapper<IncidentRecord>{
	
	List<IncidentRecord> getIncidentRecordDetail(IncidentRecord incidentRecord);
	
	List<IncidentRecordVo> getIncidentRecordByRoomId(String id);
	
	List<IncidentRecord> getEditRecord(Page<IncidentRecord> page, Map<String, Object> params);

	void delRecord(Long recordId);

	List<IncidentRecordVo> getIncidentMingLingRecordByRoomId(String roomId);
	
	
}
