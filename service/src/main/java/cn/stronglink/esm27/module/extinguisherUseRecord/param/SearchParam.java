package cn.stronglink.esm27.module.extinguisherUseRecord.param;

import java.util.List;

public class SearchParam {

	private List<Long> dictionaryIds;
	private List<Long> fireBrigadeIds;
	public List<Long> getDictionaryIds() {
		return dictionaryIds;
	}
	public void setDictionaryIds(List<Long> dictionaryIds) {
		this.dictionaryIds = dictionaryIds;
	}
	public List<Long> getFireBrigadeIds() {
		return fireBrigadeIds;
	}
	public void setFireBrigadeIds(List<Long> fireBrigadeIds) {
		this.fireBrigadeIds = fireBrigadeIds;
	}
	
}
