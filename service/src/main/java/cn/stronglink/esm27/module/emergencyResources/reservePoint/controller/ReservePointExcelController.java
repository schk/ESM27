package cn.stronglink.esm27.module.emergencyResources.reservePoint.controller;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.core.util.BmUtils;
import cn.stronglink.core.util.ImportValid;
import cn.stronglink.core.util.MapLonLatUtil;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.FireBrigade;
import cn.stronglink.esm27.entity.KeyUnit;
import cn.stronglink.esm27.entity.ReservePoint;
import cn.stronglink.esm27.module.emergencyResources.fireBrigade.service.FireBrigadeService;
import cn.stronglink.esm27.module.emergencyResources.reservePoint.service.ReservePointService;
import cn.stronglink.esm27.module.unit.service.KeyUnitService;

@Controller
@RequestMapping(value = "reservePoint/excel")
public class ReservePointExcelController extends AbstractController {

	@Autowired
	private ReservePointService entityService;
	@Autowired
	private KeyUnitService keyUnitService;
	@Autowired
	private FireBrigadeService fireBrigadeService;

	/**
	 * 导入EXCEL文件
	 * 
	 * @param file
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "importExcel")
	public ResponseEntity<ModelMap> importExcel(@RequestParam(value = "file", required = true) MultipartFile file,
			HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) throws Exception {
		List<String> dataList = new ArrayList<String>();
		System.out.println(WebUtil.getCurrentUser());
		// 导入Excel操作
		// 导入Excel文件不为空时，将文件写入流，然后转化为工作薄
		if (file != null) {
			try {
				Workbook workBook = null;
				if (!ImportValid.validXls(file)) {
					return setModelMap(modelMap, HttpCode.CONFLICT, "请选择excel格式文件！");
				}
				int header = 1;
				workBook = WorkbookFactory.create(file.getInputStream());
				if (workBook.getNumberOfSheets() > 0) {
					// 获取第一个单元薄
					Sheet sheet = workBook.getSheetAt(0);
					// 获取单元薄中表头信息,并将表头中每个单元格中信息存入数组headNames中
					String allTitle = "*储备点名称$*所属单位类型(重点单位/消防队/无)$*重点单位/消防队/无$*储备点性质$*负责人$*负责人联系方式$*坐标类型(百度坐标/无偏移坐标)$经纬度$*储备点地址$储备点描述";
					Row rowHead = sheet.getRow(header);
					if (rowHead != null) {
						String[] headNames = new String[rowHead.getLastCellNum()];
						// 获取该行中总共有多少列数据row.getLastCellNum()
						for (int j = 0; j < rowHead.getLastCellNum(); j++) {
							Cell cell = rowHead.getCell(j);
							// 获取表头各个字段名称
							String returnStr = BmUtils.getCellValue(cell);
							headNames[j] = returnStr;
						}
						if (headNames != null && headNames.length > 0) {
							for (String name : headNames) {
								if (allTitle.indexOf(name) == -1) {
									throw new BusinessException("属性：" + name + "在系统中不存在,请使用模板文件！");
								}
							}
						}
						Date date = new Date();
						// 遍历单元簿中每一行，获取每一列字段，并根据表头对应赋值
						int flag = 3;
						List<ReservePoint> entityList = new ArrayList<ReservePoint>();
						Map<String, Object> params = new HashMap<String, Object>();
						List<KeyUnit> keyUnitList = keyUnitService.qryKeyUnitList(params);
						int dataFormat = 0;
						modelMap.put("errorCode", 0);

						DecimalFormat df = new DecimalFormat("#.000000");

						for (int i = header; i < sheet.getPhysicalNumberOfRows(); i++) {
							Row row = sheet.getRow(i + 1);
							Boolean isRow = isRowEmpty(row);
							ReservePoint entity = null;
							if (!isRow) {
								String rowError = "错误： 第" + flag + "行   ";
								String data = "";
								String lonLatType = "";
								entity = new ReservePoint();
								entity.setId(IdWorker.getId());
								entity.setCreateBy(WebUtil.getCurrentUser());
								entity.setCreateTime(date);
								entity.setUpdateTime(date);

								// 获取该行中总共有多少列数据row.getLastCellNum()
								for (int j = 0; j < row.getLastCellNum(); j++) {
									if(j>headNames.length-1) {
										continue ;
									}
									Cell cell = row.getCell(j);
									if (StringUtils.hasText(headNames[j])) {
										// 获取单元格内容，并根据单元格类型，进行转化
										String returnStr = BmUtils.getCellValue(cell);
										// 根据给定的Excel格式，进行导入，一下是对应列对应字段
										if (headNames[j].equals("*储备点名称")) {
											if (returnStr != null && !"".equals(returnStr)) {
												entity.setName(returnStr);
											} else {
												dataFormat++;
												data += "【储备点名称】不能为空;";
											}
										} else if (headNames[j].equals("*所属单位类型(重点单位/消防队/无)")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												if ("重点单位".equals(returnStr)) {
													entity.setType(1);
												} else if ("消防队".equals(returnStr)) {
													entity.setType(2);
												} else if ("无".equals(returnStr)) {
													entity.setType(3);
												} else {
													dataFormat++;
													data += "【所属单位类型】不存在,只能是重点单位/消防队/无;";
												}
											} else {
												dataFormat++;
												data += "【所属单位类型】不存在,只能是重点单位/消防队/无;";
											}
										} else if (headNames[j].equals("*重点单位/消防队/无")) {
											if (entity.getType() != null && !"".equals(entity.getType())) {
												if (ImportValid.isNotEmpty(returnStr)) {
													if (entity.getType() == 1) {
														if (keyUnitList != null && keyUnitList.size() > 0) {
															for (KeyUnit keyUnit : keyUnitList) {
																if ((keyUnit.getName().replace(" ", ""))
																		.equals(returnStr.replace(" ", ""))) {
																	entity.setKeyUnitId(keyUnit.getId());
																	break;
																}
															}
															if (entity.getKeyUnitId() == null) {
																dataFormat++;
																data += "【重点单位：" + returnStr + "】在系统中不存在,请先添加;";
															}
														} else {
															dataFormat++;
															data += "【重点单位：" + returnStr + "】在系统中不存在,请先添加;";
														}
													} else if (entity.getType() == 2) {
														List<FireBrigade> fireBrigadeList = fireBrigadeService
																.qryList();
														if (fireBrigadeList != null && fireBrigadeList.size() > 0) {
															for (FireBrigade fireBrigade : fireBrigadeList) {
																if ((fireBrigade.getName().replace(" ", ""))
																		.equals(returnStr.replace(" ", ""))) {
																	entity.setKeyUnitId(fireBrigade.getId());
																	break;
																}
															}
															if (entity.getKeyUnitId() == null) {
																dataFormat++;
																data += "【消防队：" + returnStr + "】在系统中不存在,请先添加;";
															}
														} else {
															dataFormat++;
															data += "【消防队：" + returnStr + "】在系统中不存在,请先添加;";
														}
													}
												} else {
													if (entity.getType() != 3) {
														dataFormat++;
														data += "【重点单位/消防队/无】不能为空;";
													}
												}
											}
										} else if (headNames[j].equals("*储备点性质")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setProperties(returnStr);
											} else {
												dataFormat++;
												data += "【储备点性质】不能为空;";
											}
										} else if (headNames[j].equals("*负责人")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setCharge(returnStr);
											} else {
												dataFormat++;
												data += "【负责人】不能为空;";
											}
										} else if (headNames[j].equals("*负责人联系方式")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setChargePhone(returnStr);
											} else {
												dataFormat++;
												data += "【负责人联系方式】不能为空;";
											}
										} else if (headNames[j].equals("*坐标类型(百度坐标/无偏移坐标)")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												lonLatType = returnStr;
											} else {
												dataFormat++;
												data += "【坐标类型】不能为空;";
											}
										} else if (headNames[j].equals("经纬度")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												String[] coord = returnStr.split(",");
												if (coord != null && coord.length == 2) {
													coord[0] = df.format(Double.valueOf(coord[0]));
													coord[1] = df.format(Double.valueOf(coord[1]));
													if ("百度坐标".equals(lonLatType)) {
														double[] pointwgs = MapLonLatUtil.bd09py(
																Double.valueOf(coord[0]), Double.valueOf(coord[1]));
														entity.setBaiduLon(Double.valueOf(coord[0]));
														entity.setBaiduLat(Double.valueOf(coord[1]));
														entity.setLon(Double.valueOf(pointwgs[0]));
														entity.setLat(Double.valueOf(pointwgs[1]));
													} else if ("无偏移坐标".equals(lonLatType)) {
														entity.setLon(Double.valueOf(coord[0]));
														entity.setLat(Double.valueOf(coord[1]));
													} else {
														dataFormat++;
														data += "【坐标类型】只能是百度坐标/无偏移坐标;";
													}
												}else {
													data += "【经纬度】格式错误;";
												}
											} 
										} else if (headNames[j].equals("*储备点地址")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setAddress(returnStr);
											} else {
												dataFormat++;
												data += "【储备点地址】不能为空;";
											}
										} else if (headNames[j].equals("储备点描述")) {
											entity.setRemark(returnStr);
										}
									}
								}
								flag++;
								entityList.add(entity);
								if (data != null && !"".equals(data)) {
									dataList.add(rowError + data);
								}
							}
						}
						if (dataList != null && dataList.size() > 0) {
							if (dataFormat > 0) {
								modelMap.put("errorCode", 2);
								return setSuccessModelMap(modelMap, dataList);
							}
						} else {
							// 存数据
							if (entityList != null && entityList.size() > 0) {
								// 数据存储
								entityService.saveExcelData(entityList);
								return setSuccessModelMap(modelMap);
							} else {
								throw new BusinessException("文件数据为空!");
							}
						}
					}
				}
			} catch (IOException e) {
				throw new BusinessException("未知异常!");
			}
		}
		throw new BusinessException("无文件!");
	}

	@SuppressWarnings("deprecation")
	public static boolean isRowEmpty(Row row) {
		if (row == null) {
			return true;
		} else {
			for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
				Cell cell = row.getCell(c);
				if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK)
					return false;
			}
			return true;
		}
	}

	public static boolean isValidDate(String datePattern, String str) {
		boolean convertSuccess = true;
		SimpleDateFormat format = new SimpleDateFormat(datePattern);
		try {
			format.setLenient(false);
			format.parse(str);
		} catch (Exception e) {
			convertSuccess = false;
		}
		return convertSuccess;
	}

}
