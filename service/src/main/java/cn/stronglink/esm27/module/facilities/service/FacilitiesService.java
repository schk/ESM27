package cn.stronglink.esm27.module.facilities.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Facilities;
import cn.stronglink.esm27.entity.FacilitiesTemp;
import cn.stronglink.esm27.module.facilities.mapper.FacilitiesMapper;
import cn.stronglink.esm27.module.facilities.vo.FacilitiesVo;

@Service
@Transactional(rollbackFor = Exception.class)
public class FacilitiesService {

	@Autowired
	private FacilitiesMapper facilitiesMapper;

	/*
	 * 分页查询
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Page<FacilitiesVo> getObject(Page<FacilitiesVo> page, Map<String, Object> params) {
		page.setRecords(facilitiesMapper.getObject(page, params));
		return page;
	}
	
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<FacilitiesVo> qryList() {
		return facilitiesMapper.qryList();
	}
	
	/*
	 * 删除对象
	 */
	public void deleteObjectById(Long id) {
		facilitiesMapper.deleteById(id);
	}

	/*
	 * 增加信息
	 */
	public void insertObject(Facilities entity) {
		if (!"".equals(entity.getLonLat())) {
			String[] lonLat = entity.getLonLat().split(",");
			Double lonBd = new Double(lonLat[0]);
			Double latBd = new Double(lonLat[1]);
			entity.setLon(lonBd);
			entity.setLat(latBd);
		}
		entity.setId(IdWorker.getId());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		facilitiesMapper.insert(entity);
	}

	/*
	 * 修改信息
	 */
	public void updateObject(Facilities entity) {
		if (!"".equals(entity.getLonLat())) {
			String[] lonLat = entity.getLonLat().split(",");
			Double lonBd = new Double(lonLat[0]);
			Double latBd = new Double(lonLat[1]);
			entity.setLon(lonBd);
			entity.setLat(latBd);
		}
		entity.setUpdateTime(new Date());
		entity.setUpdateBy(WebUtil.getCurrentUser());
		facilitiesMapper.updateById(entity);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public FacilitiesVo qryById(Long id) {
		FacilitiesVo selectById = facilitiesMapper.qryById(id);
		if(selectById.getLon()!=null&&selectById.getLat()!=null) {
			selectById.setLonLat(selectById.getLon() + "," + selectById.getLat());
		}
		return selectById;
	}

	public void saveExcelData(List<Facilities> entityList) {
		if (entityList != null && entityList.size() > 0) {
			List<Facilities> interimList = new ArrayList<Facilities>();
			int num = 0;
			for(int i=0;i<entityList.size();i++){
				interimList.add(entityList.get(i));
				if(i!=0&&i%50==0){
					num = num + this.batchInsert(interimList);
					interimList.clear();
				}
			}
			if(interimList.size()>0){
				num = num + this.batchInsert(interimList);
				interimList.clear();
			}
			if (num != entityList.size()) {
				throw new BusinessException("导入数据异常!");
			}
		}
	}

	private int batchInsert(List<Facilities> interimList) {
		return facilitiesMapper.batchInsert(interimList);
	}

	// 存临时表
	public void saveExcelTempData(List<FacilitiesTemp> tempList) {
		if (tempList != null && tempList.size() > 0) {
			List<FacilitiesTemp> interimList = new ArrayList<FacilitiesTemp>();
			int num = 0;
			for(int i=0;i<tempList.size();i++){
				interimList.add(tempList.get(i));
				if(i!=0&&i%50==0){
					num = num + this.batchInsertTemp(interimList);
					interimList.clear();
				}
			}
			if(interimList.size()>0){
				num = num + this.batchInsertTemp(interimList);
				interimList.clear();
			}
			if (num != tempList.size()) {
				throw new BusinessException("导入数据异常!");
			}
		}
	}

	private int batchInsertTemp(List<FacilitiesTemp> interimList) {
		return facilitiesMapper.batchInsertTemp(interimList);
	}

	public List<FacilitiesTemp> qryExcelTemp(Long timestamp) {
		return facilitiesMapper.qryExcelTemp(timestamp);
	}

	public void delExcelTemp(Long timestamp) {
		facilitiesMapper.delExcelTemp(timestamp);
	}



}
