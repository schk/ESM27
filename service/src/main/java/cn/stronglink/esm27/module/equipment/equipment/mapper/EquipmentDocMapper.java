package cn.stronglink.esm27.module.equipment.equipment.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.EquipmentDoc;


public interface EquipmentDocMapper extends BaseMapper<EquipmentDoc>{

}
