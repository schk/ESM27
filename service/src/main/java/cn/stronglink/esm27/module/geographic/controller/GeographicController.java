package cn.stronglink.esm27.module.geographic.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.GeographicEdit;
import cn.stronglink.esm27.entity.Resource;
import cn.stronglink.esm27.module.geographic.service.GeographicService;
import cn.stronglink.esm27.web.webData.service.WebPottingService;
import cn.stronglink.esm27.web.webData.vo.MapVo;
import cn.stronglink.esm27.web.webData.vo.PlottingVo;

@Controller
@RequestMapping("/api/geographicEdit")
public class GeographicController extends AbstractController{
	
	@Autowired
	private WebPottingService webPottingService;	
	@Autowired
	private GeographicService geographicEditService;
	
	
	/**
	 * 初始化的地理信息编辑显示地理信息编辑和场景编辑的数据
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "qryList")
	public String qryList(Model model, HttpServletRequest request,HttpServletResponse response) {
		List<PlottingVo> plottingVoList = webPottingService.qryPlottingVoList();
		model.addAttribute("plottingVoList", plottingVoList);
        return "/geographicEdit"; 
	}
	
	/**
	 * 初始化的地理信息编辑显示地理信息编辑和场景编辑的数据
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "getRoadList")
	public ResponseEntity<ModelMap> getRoadList(ModelMap modelMap, HttpServletRequest request,HttpServletResponse response) {
		List<GeographicEdit> vo =  geographicEditService.getRoadList();
		return setSuccessModelMap(modelMap,vo);
		
	}
	

	/**
	 * 	查询资源(前台的场景搭建的数据)。
	 * 	1：查询所有终态数据。在列表+地图展示。
	 * 	2：数据，列表+地图展示。
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "queryResource")
	public ResponseEntity<ModelMap> queryResource(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<Resource> data = geographicEditService.queryResource();
		return setSuccessModelMap(modelMap, data);
	}

	/**
	 * 初始化的地理信息编辑显示地理信息编辑和场景编辑的数据
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param id
	 * @return
	 */
	@RequestMapping("qryListAll")
	public ResponseEntity<ModelMap> qryListAll(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap){
		String cate ="wzcbd,zddw,xfsy,xfs,yjdw,sczz,xfdw";
		MapVo data =  geographicEditService.qryListByParams(cate);
		return setSuccessModelMap(modelMap,data);
	}
	
	/**
	 * 根据id查询信息
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param id
	 * @return
	 */
	@RequestMapping("qryById")
	public ResponseEntity<ModelMap> selectById(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody Long id){
		GeographicEdit vo =  geographicEditService.qryById(id);
		return setSuccessModelMap(modelMap,vo);
	}
	
	/*
	 * 根据id修改信息
	 */
	@RequestMapping(value = "edit")
	@OperateLog(module = "地理信息编辑管理",desc="修改地理信息编辑", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody GeographicEdit entity){
		geographicEditService.update(entity);
		return setSuccessModelMap(modelMap, null);
		
	}
	
	/*
	 * 根据id删除信息
	 */
	@RequestMapping(value = "remove")
	@OperateLog(module = "地理信息编辑管理",desc="删除地理信息编辑", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		geographicEditService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}
	/**
	 * 新建信息
	 */
	@RequestMapping(value = "create")
	@OperateLog(module = "地理信息编辑管理",desc="添加地理信息编辑", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody GeographicEdit entity) {
		Long oid = entity.getId();
		GeographicEdit geographicEdit = new GeographicEdit();
		if(oid!=null){
			geographicEditService.update(entity);
			geographicEdit.setId(oid);
		}else{
			Long ooid = geographicEditService.insert(entity);
			geographicEdit.setId(ooid);
		}
		return setSuccessModelMap(modelMap, geographicEdit);
	}
	
	
}
