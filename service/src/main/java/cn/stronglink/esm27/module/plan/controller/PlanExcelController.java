package cn.stronglink.esm27.module.plan.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.core.util.BmUtils;
import cn.stronglink.core.util.ImportValid;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Dictionary;
import cn.stronglink.esm27.entity.FireBrigade;
import cn.stronglink.esm27.entity.KeyParts;
import cn.stronglink.esm27.entity.KeyUnit;
import cn.stronglink.esm27.entity.Plan;
import cn.stronglink.esm27.entity.PlanTemp;
import cn.stronglink.esm27.module.dictionary.service.DictionaryService;
import cn.stronglink.esm27.module.emergencyResources.fireBrigade.service.FireBrigadeService;
import cn.stronglink.esm27.module.plan.service.PlanService;
import cn.stronglink.esm27.module.unit.service.KeyPartsService;
import cn.stronglink.esm27.module.unit.service.KeyUnitService;

@Controller
@RequestMapping(value = "plan/excel")
public class PlanExcelController extends AbstractController {

	@Autowired
	private PlanService planService;
	@Autowired
	private DictionaryService dictionaryService;
	@Autowired
	private KeyUnitService keyUnitService;
	@Autowired
	private FireBrigadeService fireBrigadeService;
	@Autowired
	private KeyPartsService keyPartsService;

	
	/**
	 * 导入EXCEL文件
	 * 
	 * @param file
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "importExcel")
	public ResponseEntity<ModelMap> importExcel(@RequestParam(value = "file", required = true) MultipartFile file,
			HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) throws Exception {
		List<String> dataList = new ArrayList<String>();
		// 导入Excel操作
		// 导入Excel文件不为空时，将文件写入流，然后转化为工作薄
		if (file != null) {
			try {
				Workbook workBook = null;
				if(!ImportValid.validXls(file)){
					return setModelMap(modelMap, HttpCode.CONFLICT, "请选择excel格式文件！");
				}
				int header = 1;
				workBook = WorkbookFactory.create(file.getInputStream());
				if (workBook.getNumberOfSheets() > 0) {
					// 获取第一个单元薄
					Sheet sheet = workBook.getSheetAt(0);
					// 获取单元薄中表头信息,并将表头中每个单元格中信息存入数组headNames中
					String allTitle = "*预案名称$预案编号$*预案类型$*所属单位类型$*重点单位/消防队/重点部位$*编写人$*编写单位$*编写时间-(yyyy/MM/dd)$*预案级别";
					Row rowHead = sheet.getRow(header);
					if (rowHead != null) {
						String[] headNames = new String[rowHead.getLastCellNum()];
						// 获取该行中总共有多少列数据row.getLastCellNum()
						for (int j = 0; j < rowHead.getLastCellNum(); j++) {
							Cell cell = rowHead.getCell(j);
//							if (cell == null) {
//								continue;
//							} else {
//								// 获取表头各个字段名称
//								String returnStr = BmUtils.getCellValue(cell);
//								headNames[j] = returnStr;
//							}
							// 获取表头各个字段名称
							String returnStr = BmUtils.getCellValue(cell);
							headNames[j] = returnStr;
						}
						if (headNames != null && headNames.length > 0) {
							for (String name : headNames) {
								if (allTitle.indexOf(name) == -1) {
									throw new BusinessException("属性：" + name + "在系统中不存在,请使用模板文件！");
								}
							}
						}
						Date date = new Date();
						// 遍历单元簿中每一行，获取每一列字段，并根据表头对应赋值
						int flag = 3;
						List<Plan> entityList = new ArrayList<Plan>();
						List<PlanTemp> tempList = new ArrayList<PlanTemp>();
						List<Dictionary> typeList = dictionaryService.qryByType(3);
						Map<String, Object> params = new HashMap<String, Object>();
						List<KeyUnit> keyUnitList = keyUnitService.qryKeyUnitList(params);
						List<FireBrigade> fireBrigadeList = fireBrigadeService.qryList();
						List<KeyParts> keyPartsList = keyPartsService.qryList();
						Boolean typeNoExist = false;
						int dataFormat = 0;
						modelMap.put("errorCode", 0);

						// 插入execl中得数据到临时表
						long timestamp = System.currentTimeMillis();
						for (int i = header; i < sheet.getPhysicalNumberOfRows(); i++) {
							Row row = sheet.getRow(i + 1);
							Boolean isRow = ImportValid.isRowEmpty(row);
							Plan entity = null;
							PlanTemp temp = null;
							if (!isRow) {
								String rowError = "错误：第" + flag + "行   ";
								String data = "";
								entity = new Plan();
								entity.setId(IdWorker.getId());
								entity.setCreateBy(WebUtil.getCurrentUser());
								entity.setCreateTime(date);
								entity.setUpdateTime(date);
								entity.setUpdateBy(WebUtil.getCurrentUser());
								temp = new PlanTemp();
								temp.setId(IdWorker.getId());
								temp.setCreateBy(WebUtil.getCurrentUser());
								temp.setTimestamp(timestamp);
								temp.setCreateTime(date);

								// 获取该行中总共有多少列数据row.getLastCellNum()
								for (int j = 0; j < row.getLastCellNum(); j++) {
									if(j>headNames.length-1) {
										continue ;
									}
									Cell cell = row.getCell(j);
									// if (cell == null) {
									// continue;
									// } else
									if (StringUtils.hasText(headNames[j])) {
										// 获取单元格内容，并根据单元格类型，进行转化
										String returnStr = BmUtils.getCellValue(cell);
										// 根据给定的Excel格式，进行导入，一下是对应列对应字段
										if (headNames[j].equals("*预案名称")) {
											if(ImportValid.isNotEmpty(returnStr)){
												entity.setName(returnStr);
												temp.setName(returnStr);
											} else {
												dataFormat++;
												data += "【预案名称】不能为空;";
											}
										} else if (headNames[j].equals("预案编号")) {
											entity.setCode(returnStr);
											temp.setCode(returnStr);
										}else if (headNames[j].equals("*预案类型")) {
											if(ImportValid.isNotEmpty(returnStr)){
												temp.setDic(returnStr);
												if (typeList != null && typeList.size() > 0) {
													boolean isExit = false;
													for (Dictionary type : typeList) {
														if ((type.getName().replace(" ", "")).equals(returnStr.replace(" ", ""))) {
															isExit = true;
															entity.setDicId(type.getId());
															break;
														}
													}
													if (!isExit) {
														typeNoExist = true;
														data += "【预案类型："+returnStr+"】不存在;";
													}
												}
											} else {
												dataFormat++;
												data += "【预案类型】不能为空;";
											}
										} else if (headNames[j].equals("*所属单位类型")) {
											if(ImportValid.isNotEmpty(returnStr)){
												if ("重点单位".equals(returnStr)) {
													entity.setType("1");
													temp.setType("1");
												} else if ("消防队".equals(returnStr)) {
													entity.setType("2");
													temp.setType("2");
												} else if ("重点部位".equals(returnStr)) {
													entity.setType("3");
													temp.setType("3");
												}else {
													dataFormat++;
													data += "【所属单位类型】不存在,只能是重点单位/消防队/重点部位;";
												}
												
											}else {
												dataFormat++;
												data += "【所属单位类型】不能为空,只能是重点单位/消防队/重点部位;";
											}
										} else if (headNames[j].equals("*重点单位/消防队/重点部位")) {
											if(ImportValid.isNotEmpty(returnStr)){
												String type = entity.getType();
												if (type != null && !type.equals("")) {
													if ("1".equals(type)) {
														if (keyUnitList != null && keyUnitList.size() > 0) {
															boolean isExit=false;
															for (KeyUnit keyUnit : keyUnitList) {
																if ((keyUnit.getName().replace(" ", "")).equals(returnStr.replace(" ", ""))) {
																	isExit=true;
																	entity.setKeyUnitId(keyUnit.getId());
																	temp.setKeyUnitId(keyUnit.getId().toString());
																	break;
																}
															}
															if(!isExit){
																dataFormat++;
																data+="【重点单位："+returnStr+"】在系统中不存在，请先添加;";
															}
														}else{
															dataFormat++;
															data+="【重点单位："+returnStr+"】在系统中不存在，请先添加;";
														}
														
													} else if ("2".equals(type)) {
														if (fireBrigadeList != null && fireBrigadeList.size() > 0) {
															boolean isExit=false;
															for (FireBrigade fireBrigade : fireBrigadeList) {
																if ((fireBrigade.getName().replace(" ", "")).equals(returnStr.replace(" ", ""))) {
																	isExit=true;
																	entity.setKeyUnitId(fireBrigade.getId());
																	temp.setKeyUnitId(fireBrigade.getId().toString());
																	break;
																}
															}
															if(!isExit){
																dataFormat++;
																data+="【消防队："+returnStr+"】 在系统中不存在，请先添加;";
															}
														}else{
															dataFormat++;
															data+="【消防队："+returnStr+"】 在系统中不存在，请先添加;";
														}
													}else if ("3".equals(type)) {
														if (keyPartsList != null && keyPartsList.size() > 0) {
															boolean isExit=false;
															for (KeyParts keyParts : keyPartsList) {
																if ((keyParts.getName().replace(" ", "")).equals(returnStr.replace(" ", ""))) {
																	isExit=true;
																	entity.setKeyUnitId(keyParts.getId());
																	temp.setKeyUnitId(keyParts.getId().toString());
																	break;
																}
															}
															if(!isExit){
																dataFormat++;
																data+="【重点部位："+returnStr+"】在系统中不存在，请先添加;";
															}
														}else{
															dataFormat++;
															data+="【重点部位："+returnStr+"】 在系统中不存在，请先添加;";
														}
													}
												}
											}else{
												dataFormat++;
												data+="【重点单位/消防队/重点部位】不能为空;";
											}
										} else if (headNames[j].equals("*编写人")) {
											if(ImportValid.isNotEmpty(returnStr)){
												entity.setWriter(returnStr);
												temp.setWriter(returnStr);
											} else {
												dataFormat++;
												data += "【编写人】不能为空;";
											}
										} else if (headNames[j].equals("*编写单位")) {
											if(ImportValid.isNotEmpty(returnStr)){
												if (fireBrigadeList != null && fireBrigadeList.size() > 0) {
													boolean isExit=false;
													for (FireBrigade fireBrigade : fireBrigadeList) {
														if ((fireBrigade.getName().replace(" ", "")).equals(returnStr.replace(" ", ""))) {
															isExit=true;
															entity.setWritingUnit(fireBrigade.getId());
															temp.setWritingUnit(fireBrigade.getId().toString());
															break;
														}
													}
													if(!isExit){
														dataFormat++;
														data+="【编写单位："+returnStr+"】 在消防队中不存在，请先添加;";
													}
												}else{
													dataFormat++;
													data+="【编写单位："+returnStr+"】 在消防队中不存在，请先添加;";
												}
											}else {
												dataFormat++;
												data += "【编写单位】不能为空;";
											}
										} else if (headNames[j].equals("*编写时间-(yyyy/MM/dd)")) {
											if(ImportValid.isNotEmpty(returnStr)){
												Boolean isTrue = ImportValid.isValidDate("yyyy/MM/dd",returnStr);
												if (isTrue) {
													SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
													entity.setWritingTime(sdf.parse(returnStr));
													temp.setWritingTime(sdf.parse(returnStr));
												}else {
													dataFormat++;
													data+="【编写时间格式】不正确;";
												}
											}else{
												dataFormat++;
												data += "【编写时间】不能为空;";
											}
										} else if (headNames[j].equals("*预案级别")) {
											if(ImportValid.isNotEmpty(returnStr)){
												if ("Ⅰ级".equals(returnStr)) {
													entity.setLevel(1);
													temp.setLevel(1);
												} else if ("Ⅱ级".equals(returnStr)) {
													entity.setLevel(2);
													temp.setLevel(2);
												} else if ("Ⅲ级".equals(returnStr)) {
													entity.setLevel(3);
													temp.setLevel(3);
												} else if ("Ⅳ级".equals(returnStr)) {
													entity.setLevel(4);
													temp.setLevel(4);
												} else {
													dataFormat++;
													data += "【预案级别】不存在;";
												}
											} else {
												dataFormat++;
												data += "【预案级别】不能为空;";
											}
										} 
									}
								}
								flag++;
								entityList.add(entity);
								tempList.add(temp);
								if (data != null && !"".equals(data)) {
									dataList.add(rowError+data);
								}
							}
						}
						if (dataList != null && dataList.size() > 0) {
							if (dataFormat > 0) {
								modelMap.put("errorCode", 2);
								return setSuccessModelMap(modelMap, dataList);
							}
							if (typeNoExist) {
								modelMap.put("errorCode", 1);
								modelMap.put("times", timestamp);
								// 存临时表数据
								if (tempList != null && tempList.size() > 0) {
									planService.saveExcelTempData(tempList);
									return setSuccessModelMap(modelMap, dataList);
								} else {
									throw new BusinessException("文件数据为空!");
								}
							}
						} else {
							// 存数据
							if (entityList != null && entityList.size() > 0) {
								// 数据存储
								planService.saveExcelData(entityList);
								return setSuccessModelMap(modelMap);
							} else {
								throw new BusinessException("文件数据为空!");
							}
						}
					}
				}
			} catch (IOException e) {
				throw new BusinessException("未知异常!");
			}
		}
		throw new BusinessException("无文件!");
	}

	/**
	 * 继续导入excel数据
	 * 
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param timestamp
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "importExcelConfirm")
	public ResponseEntity<ModelMap> importExcelConfirm(HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap, @RequestBody Long timestamp) throws Exception {
		Date date = new Date();
		// 遍历单元簿中每一行，获取每一列字段，并根据表头对应赋值
		List<Plan> entityList = new ArrayList<Plan>();
		List<Dictionary> typeList = dictionaryService.qryByType(3);
		List<PlanTemp> tempList = planService.qryExcelTemp(timestamp);

		for (int j = 0; j < tempList.size(); j++) {
			PlanTemp temp = tempList.get(j);
			Plan entity = null;
			if (temp != null) {
				entity = new Plan();
				boolean isExit = false;
				if (typeList != null && typeList.size() > 0) {
					for (Dictionary type : typeList) {
						if (type.getName().equals(temp.getDic())) {
							isExit = true;
							entity.setDicId(type.getId());
							break;
						}
					}
				}
				if (!isExit) {
					Dictionary type = new Dictionary();
					type.setName(temp.getDic());
					type.setRemark(temp.getDic());
					type.setType(3);
					dictionaryService.insert(type);
					entity.setDicId(type.getId());
					typeList.add(type);
				}
				entity.setId(IdWorker.getId());
				entity.setName(temp.getName());
				entity.setCode(temp.getCode());
				entity.setKeyUnitId(Long.parseLong(temp.getKeyUnitId()));
				entity.setLevel(temp.getLevel());
				entity.setType(temp.getType());
				entity.setWriter(temp.getWriter());
				entity.setWritingUnit(Long.valueOf(temp.getWritingUnit()));
				entity.setWritingTime(temp.getWritingTime());
				entity.setCreateBy(WebUtil.getCurrentUser());
				entity.setCreateTime(date);
				entity.setUpdateTime(date);
				entityList.add(entity);
			}
		}
		if (entityList != null && entityList.size() > 0) {
			// 数据存储
			planService.saveExcelData(entityList);
			// 删除临时表数据
			planService.delExcelTemp(timestamp);
			return setSuccessModelMap(modelMap);
		} else {
			throw new BusinessException("文件数据为空!");
		}

	}

	/**
	 * 删除临时表数据
	 * 
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param timestamp
	 * @return
	 */
	@RequestMapping(value = "delExcelTemp")
	public ResponseEntity<ModelMap> delExcelTemp(HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap, @RequestBody Long timestamp) {
		// 删除临时表数据
		planService.delExcelTemp(timestamp);
		return setSuccessModelMap(modelMap);
	}


}
