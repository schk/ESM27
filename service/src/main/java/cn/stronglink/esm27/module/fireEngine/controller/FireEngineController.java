package cn.stronglink.esm27.module.fireEngine.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.FireEngine;
import cn.stronglink.esm27.entity.User;
import cn.stronglink.esm27.module.conventionalEquip.specialDutyEquip.service.SpecialDutyEquipService;
import cn.stronglink.esm27.module.conventionalEquip.specialDutyEquip.vo.SpecialDutyEquipVo;
import cn.stronglink.esm27.module.fireEngine.service.FireEngineService;
import cn.stronglink.esm27.module.fireEngine.vo.FireEngineVo;
import cn.stronglink.esm27.module.system.user.service.UserService;

@Controller
@RequestMapping("fireEngine")
public class FireEngineController extends AbstractController {
	
	@Autowired
	private FireEngineService fireEngineService;
	@Autowired
	private SpecialDutyEquipService specialDutyEquipService;
	@Autowired
	private UserService userService;
	/**
	 * 查询列表
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		if(request.getSession().getAttribute("admin")!=null){
			boolean isAdmin = (boolean) request.getSession().getAttribute("admin");
			if(!isAdmin) {
//				if(request.getSession().getAttribute("userBrigade")!=null){
//					List<Long> fireBrigadeIds = (List<Long>) request.getSession().getAttribute("userBrigade");
//					params.put("fireBrigadeIds",fireBrigadeIds);
//				}
				User u =  (User) request.getSession().getAttribute("currentUser");
				if(u!=null) {
					Long brigadeId =userService.selectUserBrigade(u.getId());
					List<Long> fireBrigadeIds = userService.selectUserBrigadeChild(brigadeId);	
					params.put("fireBrigadeIds",fireBrigadeIds);
				}
			}
		}
	
		Page<FireEngineVo> page = (Page<FireEngineVo>) super.getPage(params);
		Page<FireEngineVo> data = fireEngineService.qryListByParams(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	
	/**
	 * 通过id查询
	 */
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> qryUserById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		FireEngine vo = fireEngineService.qryById(id);
		List<SpecialDutyEquipVo> data = specialDutyEquipService.qryListByEngineId(id);
		modelMap.put("detailList", data);
		return setSuccessModelMap(modelMap, vo);
	}
	/**
	 * 新建
	 */
	@RequestMapping(value = "create")
	@OperateLog(module = "消防车管理",desc="添加消防车", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody FireEngine entity) {
		fireEngineService.insert(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 修改
	 */
	@RequestMapping(value = "edit")
	@OperateLog(module = "消防车管理",desc="修改消防车", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody FireEngine entity) {
		fireEngineService.update(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 删除用户
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "remove")
	@OperateLog(module = "消防车管理",desc="删除消防车", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		fireEngineService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}
	

}
