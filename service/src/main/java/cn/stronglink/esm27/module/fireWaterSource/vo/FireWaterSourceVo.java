package cn.stronglink.esm27.module.fireWaterSource.vo;

import cn.stronglink.esm27.entity.FireWaterSource;

public class FireWaterSourceVo extends FireWaterSource {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6400269125502148290L;
	
	private String keyUnitName;
	private String typeName;
	private String distances;

	public String getKeyUnitName() {
		return keyUnitName;
	}

	public void setKeyUnitName(String keyUnitName) {
		this.keyUnitName = keyUnitName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getDistances() {
		return distances;
	}

	public void setDistances(String distances) {
		this.distances = distances;
	}
	
	
	
	

}
