package cn.stronglink.esm27.module.fireEngine.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.esm27.entity.FireEngineEquipment;
import cn.stronglink.esm27.module.fireEngine.vo.FireEngineEquipmentVo;

public interface FireEngineEquipmentMapper extends BaseMapper<FireEngineEquipment> {
	
	List<FireEngineEquipmentVo> qryListByParams(Pagination page, Map<String, Object> params);

}
