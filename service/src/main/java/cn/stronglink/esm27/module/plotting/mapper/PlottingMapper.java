package cn.stronglink.esm27.module.plotting.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.esm27.entity.Plotting;
import cn.stronglink.esm27.module.plotting.vo.PlottingCVo;

public interface PlottingMapper extends BaseMapper<Plotting> {
	
	List<PlottingCVo> qryListByParams(Pagination page, Map<String, Object> params);

	int getMaxType(Long id);

	List<Plotting> qryPlottingList();

	Integer getCountByUsername(Plotting entity);

	List<Plotting> qryJhPlottingList();

}
