package cn.stronglink.esm27.module.conventionalEquip.specialDutyEquip.vo;

import java.util.Date;

import cn.stronglink.esm27.entity.SpecialDutyEquip;

public class SpecialDutyEquipVo extends SpecialDutyEquip {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1590019169506597735L;
	
	private String deptName;
	
	private Integer count;
	
	private String useDesc;
	
	private Date followDate;
	
	private Long fireEngineId;
	
	private String fireBrigadeName;
	
	private Long dutyEquipId;
	
	private Long fireId;
	
	private String typeName;
	
	public Long getFireEngineId() {
		return fireEngineId;
	}

	public void setFireEngineId(Long fireEngineId) {
		this.fireEngineId = fireEngineId;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getUseDesc() {
		return useDesc;
	}

	public void setUseDesc(String useDesc) {
		this.useDesc = useDesc;
	}

	public Date getFollowDate() {
		return followDate;
	}

	public void setFollowDate(Date followDate) {
		this.followDate = followDate;
	}

	public String getFireBrigadeName() {
		return fireBrigadeName;
	}

	public void setFireBrigadeName(String fireBrigadeName) {
		this.fireBrigadeName = fireBrigadeName;
	}

	public Long getDutyEquipId() {
		return dutyEquipId;
	}

	public void setDutyEquipId(Long dutyEquipId) {
		this.dutyEquipId = dutyEquipId;
	}

	public Long getFireId() {
		return fireId;
	}

	public void setFireId(Long fireId) {
		this.fireId = fireId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


}
