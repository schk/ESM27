package cn.stronglink.esm27.module.unit.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.KeyParts;
import cn.stronglink.esm27.entity.KeyPartsDangers;
import cn.stronglink.esm27.entity.KeyUnitDangers;
import cn.stronglink.esm27.module.danger.danger.mapper.DangersMapper;
import cn.stronglink.esm27.module.plan.mapper.PlanMapper;
import cn.stronglink.esm27.module.plan.vo.PlanVo;
import cn.stronglink.esm27.module.unit.mapper.KeyPartsDangersMapper;
import cn.stronglink.esm27.module.unit.mapper.KeyPartsMapper;
import cn.stronglink.esm27.module.unit.mapper.KeyUnitMapper;
import cn.stronglink.esm27.module.unit.vo.KeyPartsVo;
import cn.stronglink.esm27.module.unit.vo.KeyUnitPartsVo;

@Service
@Transactional(rollbackFor=Exception.class)
public class KeyPartsService {
	
	@Autowired
	private KeyPartsMapper keyPartsMapper;
	@Autowired
	private KeyUnitMapper keyUnitMapper;
	@Autowired
	private DangersMapper dangerMapper;
	@Autowired
	private KeyPartsDangersMapper keyPartsDangersMapper;
	@Autowired
	private PlanMapper planMapper;

	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<KeyPartsVo> getListByParams(Page<KeyPartsVo> page,Map<String, Object> params) {
		List<KeyPartsVo> list = keyPartsMapper.getListByParams(page,params);
		page.setRecords(list);
		return page;
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<KeyPartsVo> qryKeyPartsList(Page<KeyPartsVo> page, Map<String, Object> params) {
		List<KeyPartsVo> list = keyPartsMapper.getListByParams(page,params);
		page.setRecords(list);
		return page;
	}
	
	public KeyPartsVo qryKeyPartsById(Long id) {
		KeyPartsVo vo = keyPartsMapper.qryById(id);
		if(vo!=null) {
			Map <String,Object> paramsP = new HashMap<String,Object>();
			paramsP.put("keyPartsId", vo.getId());
			List<KeyUnitDangers> dList = dangerMapper.qryDangersByKeyParts(paramsP);
			vo.setdList(dList);
			List<PlanVo> pList = planMapper.qryPlanByKeyParts(vo.getId());
			vo.setPlanList(pList);
		}
		return vo;
	}
	
	/**
	 * 重点单位下得重点部位列表
	 * @param page
	 * @param params
	 * @return
	 */
	public Page<KeyUnitPartsVo> qryKeyPartsGroupUnit(Page<KeyUnitPartsVo> page, Map<String, Object> params) {
		List<KeyUnitPartsVo> keyUnitPartsList = keyUnitMapper.qryKeyPartsGroupUnit(page,params);
		if(keyUnitPartsList!=null&&keyUnitPartsList.size()>0){
			for(KeyUnitPartsVo type:keyUnitPartsList){
				params.put("keyUnitId", type.getId());
				List<KeyPartsVo> keyPartList= keyPartsMapper.getPartsByUnit(params);
				if(keyPartList!=null&&keyPartList.size()>0){
					type.setKeyPartsCount(keyPartList.size());
					type.setKeyPartsList(keyPartList);
				}
			}
		}
		page.setRecords(keyUnitPartsList);
		return page;
	}

	
	public void insert(KeyParts entity) {
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		keyPartsMapper.insert(entity);
	
	}

	public void update(KeyParts entity) {
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if (keyPartsMapper.updateById(entity) == 0) {
			throw new BusinessException("更新失败!");
		}
	}

	public void remove(Long id) {
		keyPartsMapper.deleteById(id);
		// 删除应急预案
		planMapper.updateKeyUnitById(id);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public KeyPartsVo qryById(Long id) {
		return keyPartsMapper.qryById(id);
	}

	public void delDangers(Long id) {
		keyPartsMapper.delDangers(id);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<KeyParts> qryList() {
		return  keyPartsMapper.qryList();
	}

	public void saveDangersOfKeyParts(KeyPartsVo keyParts) {
		if(keyParts.getdList()!=null&&keyParts.getdList().size()>0) {
			//删除重点部位的危化品
			keyPartsMapper.delDangersByKeyParts(keyParts.getId());
			for(KeyUnitDangers vo:keyParts.getdList() ) {
				KeyPartsDangers d = new KeyPartsDangers();
				d.setId(IdWorker.getId());
				d.setKeyPartsId(keyParts.getId());
				d.setDangersId(vo.getId());
				d.setCreateBy(WebUtil.getCurrentUser());
				d.setCreateTime(new Date());
				keyPartsDangersMapper.insert(d);
			}
		}
		
	}

	public void saveExcelData(List<KeyParts> entityList) {
		if (entityList!=null&&entityList.size()>0) {
			List<KeyParts> interimList = new ArrayList<KeyParts>();
			int j =  (int)Math.ceil(entityList.size()/(double)50);
			int num =0;
			for (int i = 1; i <= j;i++) {
				if (i*50>=entityList.size()) {
					for (int k = (i - 1) * 50; k < entityList.size(); k++) {
						interimList.add(entityList.get(k));
					}
					num =num+this.batchInsertData(interimList);
				}else {
					interimList = entityList.subList((i - 1) * 50, i * 50);
					num =num+this.batchInsertData(interimList);
				}
			}
			if (num!=entityList.size()) {
				throw new BusinessException("导入数据异常!");
			}
		}	
		
	}

	private int batchInsertData(List<KeyParts> interimList) {
		return keyPartsMapper.batchInsertData(interimList);
	}




	

	
}
