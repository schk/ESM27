package cn.stronglink.esm27.module.sceneBuilding.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;

@Controller
@RequestMapping("/scene")
public class SceneController extends AbstractController{
	 
	/**
	 * 初始化的地理信息编辑显示地理信息编辑和场景编辑的数据
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "qryList")
	public String qryList(Model model, HttpServletRequest request,HttpServletResponse response) {
		
        return "sceneList"; 
	}
	

	
}
