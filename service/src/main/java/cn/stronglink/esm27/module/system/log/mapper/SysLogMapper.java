package cn.stronglink.esm27.module.system.log.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.esm27.entity.SysOperateLog;

public interface SysLogMapper extends BaseMapper<SysOperateLog>{
		
	public List<SysOperateLog> qryList(Pagination page,@Param("params") Map<String, Object> params);

	public SysOperateLog qryById(@Param("id")Long id);
	
}
