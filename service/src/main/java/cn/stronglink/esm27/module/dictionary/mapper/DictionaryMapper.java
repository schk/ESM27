package cn.stronglink.esm27.module.dictionary.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.Dictionary;
import cn.stronglink.esm27.module.dictionary.vo.CarTypeTreeNodeVo;

public interface DictionaryMapper extends BaseMapper<Dictionary> {
	
	//根据type查询信息
	List<Dictionary> qryByType(@Param("type") Integer type);

	List<Dictionary> qryDictionary(Map<String,Object> conditionMap1);

	int batchInsertType(@Param("interimList") List<Dictionary> list);

	List<CarTypeTreeNodeVo> qryRootCarType();

	List<CarTypeTreeNodeVo> qryChildCarType();

	List<CarTypeTreeNodeVo> qryCarTypeC(Long pid);
}
