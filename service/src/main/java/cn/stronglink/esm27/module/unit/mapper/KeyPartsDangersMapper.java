package cn.stronglink.esm27.module.unit.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.KeyPartsDangers;

public interface KeyPartsDangersMapper extends BaseMapper<KeyPartsDangers>{
	
}
