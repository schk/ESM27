package cn.stronglink.esm27.module.collect.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Field;
import org.springframework.stereotype.Service;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.WinNT;

import cn.stronglink.core.exception.BusinessException;

@Service
public class CollectService {

	public static Process collectProcess = null;

	public int startCollect(boolean isLocal, String closeType) {
		int returnCode = 1;
		if (collectProcess != null) {
			collectProcess.destroy();
			collectProcess.destroyForcibly();
			//killProcessTree(collectProcess);
			collectProcess = null;
			returnCode = 0;
		}

		if ("open".equals(closeType)) {
			try {
				if (isLocal) {
					// Process process = Runtime.getRuntime().exec("java -jar
					// d:/ESM27DataSyncService-0.0.4-SNAPSHOT.jar");
					// Process process = Runtime.getRuntime().exec("java -jar
					// D:/ESM27DataSyncService-0.0.7-DEMO.jar");
					// String [] cmd={"cmd","/C","java -jar
					// D:/ESM27DataSyncService-0.0.7-DEMO.jar"};
					Process process = Runtime.getRuntime()
							.exec("cmd /c start java -jar D:/ESM27/Collect/ESM27CollectionService-0.0.4-SNAPSHOT-local.jar");
					printMessage(process.getInputStream());
					printMessage(process.getErrorStream());
					int value;
					try {
						value = process.waitFor();
						returnCode = value;
						System.out.println(value);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					collectProcess = process;
				} else {
					Process process = Runtime.getRuntime()
							.exec("cmd /c start java -jar D:/ESM27/Collect/ESM27CollectionService-0.0.4-SNAPSHOT-center.jar");
					printMessage(process.getInputStream());
					printMessage(process.getErrorStream());
					int value;
					try {
						value = process.waitFor();
						returnCode = value;
						System.out.println(value);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					collectProcess = process;
				}
			} catch (IOException e) {
				throw new BusinessException("采集启动失败");
			}
		}
		return returnCode;

	}

	private static void printMessage(final InputStream input) {
		new Thread(new Runnable() {
			public void run() {
				Reader reader = new InputStreamReader(input);
				BufferedReader bf = new BufferedReader(reader);
				String line = null;
				try {
					while ((line = bf.readLine()) != null) {
						System.out.println(line + "数据同步异常");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	private static void killProcessTree(Process process) {
		try {
			Field f = process.getClass().getDeclaredField("handle");
			f.setAccessible(true);
			long handl = f.getLong(process);
			Kernel32 kernel = Kernel32.INSTANCE;
			WinNT.HANDLE handle = new WinNT.HANDLE();
			handle.setPointer(Pointer.createConstant(handl));
			int ret = kernel.GetProcessId(handle);
			Long PID = Long.valueOf(ret);
			String cmd = getKillProcessTreeCmd(PID);
			Runtime rt = Runtime.getRuntime();
			Process killPrcess = rt.exec(cmd);
			killPrcess.waitFor();
			killPrcess.destroy();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String getKillProcessTreeCmd(Long Pid) {
		String result = "";
		if (Pid != null)
			result = "cmd.exe /c taskkill /PID " + Pid + " /F /T ";
		return result;
	}

}
