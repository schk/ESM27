package cn.stronglink.esm27.module.unit.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.KeyParts;
import cn.stronglink.esm27.module.unit.service.KeyPartsService;
import cn.stronglink.esm27.module.unit.vo.KeyPartsVo;

@Controller
@RequestMapping(value = "keyParts")
public class KeyPartsController extends AbstractController  {
	
	@Autowired
	private KeyPartsService keyPartsService;
	
	/**
	 * 查询重点部位列表
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		Page<KeyPartsVo> page = (Page<KeyPartsVo>) super.getPage(params);
		Page<KeyPartsVo> data = keyPartsService.getListByParams(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询重点部位列表(不带分页)
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryKeyPartsList")
	public ResponseEntity<ModelMap> qryList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<KeyParts> data = keyPartsService.qryList();
		return setSuccessModelMap(modelMap, data);
	}
	
	
	/**
	 * 查询某个对象
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> getkeyUnitName(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		KeyPartsVo entity = keyPartsService.qryById(id);
		return setSuccessModelMap(modelMap, entity);
	}
	
	
	/**
	 * 新增重点部位
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param entity
	 * @return
	 */
	@RequestMapping(value = "create")
	@OperateLog(module = "重点部位",desc="添加重点部位", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody KeyParts entity) {
		keyPartsService.insert(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	@RequestMapping(value = "edit")
	@OperateLog(module = "重点部位",desc="修改重点部位", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody KeyParts entity) {
		keyPartsService.update(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	@RequestMapping(value = "remove")
	@OperateLog(module = "重点部位",desc="删除重点部位", type = OpType.DEL)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		keyPartsService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}
	
	@RequestMapping(value = "delDangers")
	@OperateLog(module = "重点部位",desc="删除重点部位危化品", type = OpType.DEL)
	public ResponseEntity<ModelMap> delDangers(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		keyPartsService.delDangers(id);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 更新重点部位危化品
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "saveDangersOfKeyParts")
	@OperateLog(module = "重点部位",desc="更新重点单位危化品", type = OpType.DEL)
	public ResponseEntity<ModelMap> saveDangersOfKeyParts(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody KeyPartsVo keyParts) {
		keyPartsService.saveDangersOfKeyParts(keyParts);
		return setSuccessModelMap(modelMap, null);
	}
	
}
