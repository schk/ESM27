package cn.stronglink.esm27.module.system.role.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Role;
import cn.stronglink.esm27.module.system.role.mapper.RoleMapper;
import cn.stronglink.esm27.module.system.role.vo.RoleVo;

@Service
@Transactional(rollbackFor=Exception.class)
public class RoleService {
	
	@Autowired
	private RoleMapper roleMapper;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<RoleVo> getRoleByParams(Page<RoleVo> page, Role role) {
		page.setRecords(roleMapper.getRoleByParams(page,role));	
		return page;
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<Role> qryRoleOption() {
		return roleMapper.qryRoleOption();
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Role qryRoleById(Long id) {
		return roleMapper.selectById(id);
	}

	public void insertRole(Role entity) {
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		roleMapper.insert(entity);
	}

	public void updateRole(Role entity) {
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if (roleMapper.updateById(entity) == 0) {
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
		
	}

	public void removeRole(Long id) {
		roleMapper.deleteById(id);
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Integer getRoleUsers(Long id) {
		return roleMapper.getRoleUsers(id);
	}

	public int getRoleByName(String name) {
		return roleMapper.getRoleByName(name);
	}

	public Integer getCountByUsername(Role entity) {
		return roleMapper.getCountByUsername(entity);
	}
}
