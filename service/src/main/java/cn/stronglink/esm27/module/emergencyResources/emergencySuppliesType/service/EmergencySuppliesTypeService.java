package cn.stronglink.esm27.module.emergencyResources.emergencySuppliesType.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.EmergencyMaterialType;
import cn.stronglink.esm27.module.emergencyResources.emergencySuppliesType.mapper.EmergencySuppliesTypeMapper;

@Service
@Transactional(rollbackFor=Exception.class)
public class EmergencySuppliesTypeService {
	
	@Autowired
	private EmergencySuppliesTypeMapper emergencySuppliesTypeMapper;
	
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<EmergencyMaterialType> qryListByParams(Map<String,Object> params){
		return emergencySuppliesTypeMapper.qryListByParams(params);
	}
	/*
	 * 根据id查询信息
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public EmergencyMaterialType selectById(Long id){
		return emergencySuppliesTypeMapper.selectById(id);
	}
	
	/*
	 * 根据id删除信息
	 */
	public void  remove(Long id){
		emergencySuppliesTypeMapper.deleteById(id);
	}
	/*
	 * 添加信息
	 */
	public void insert(EmergencyMaterialType entity) {
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		emergencySuppliesTypeMapper.insert(entity);
	}
	//根据id修改信息
	public void update(EmergencyMaterialType entity) {
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if(emergencySuppliesTypeMapper.updateById(entity)==0){
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
	}

}
