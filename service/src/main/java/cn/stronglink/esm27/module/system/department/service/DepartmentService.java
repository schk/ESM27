package cn.stronglink.esm27.module.system.department.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Department;
import cn.stronglink.esm27.module.system.department.mapper.DepartmentMapper;
import cn.stronglink.esm27.module.system.department.vo.DepartmentTreeNodeVo;

@Service
@Transactional(rollbackFor=Exception.class) 
public class DepartmentService {

	@Autowired
	private DepartmentMapper departmentMapper;

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<DepartmentTreeNodeVo> getTree() {
		List<DepartmentTreeNodeVo> rootDept = departmentMapper.qryRootDept();	
		if(rootDept!=null&&rootDept.size()>0){
			for(DepartmentTreeNodeVo vo:rootDept){
				getChildren(vo);
			}
		}
        return rootDept;  
	}
	
	private void getChildren(DepartmentTreeNodeVo vo) {
		List<DepartmentTreeNodeVo> voList=departmentMapper.getChildList(vo.getId());
		if(voList!=null&&voList.size()>0){
			for(DepartmentTreeNodeVo child:voList){
				getChildren(child);
			}
			vo.setChildren(voList);
		}
		
	}

	public void insertDept(Department entity){
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(new Date());
		departmentMapper.insert(entity);	
	}
	
	public void updateDept(Department entity) {
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		departmentMapper.updateById(entity);	
	}
	
	public void remove(Long id){
		departmentMapper.deleteById(id);	
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public int getChildDetpCount(Long id) {
		return departmentMapper.getChildDetpCount(id);
	}  

	private void recursion(List<DepartmentTreeNodeVo> list, DepartmentTreeNodeVo node) {
		List<DepartmentTreeNodeVo> childList = getChildList(list, node);// 得到子节点列表
		if (!CollectionUtils.isEmpty(childList)) {
			node.setChildren(childList);
			Iterator<DepartmentTreeNodeVo> it = childList.iterator();
			while (it.hasNext()) {
				DepartmentTreeNodeVo n = (DepartmentTreeNodeVo) it.next();
				recursion(list, n);
			}
		} else {
			node.setChildren(null);
		}
	}
	
	private List<DepartmentTreeNodeVo> getChildList(List<DepartmentTreeNodeVo> list, DepartmentTreeNodeVo node) {  
        List<DepartmentTreeNodeVo> nodeList = new ArrayList<DepartmentTreeNodeVo>();  
        Iterator<DepartmentTreeNodeVo> it = list.iterator();  
        while (it.hasNext()) {  
        	DepartmentTreeNodeVo n = (DepartmentTreeNodeVo) it.next();  
            if (n.getPid().equals(node.getId()) ) {  
                nodeList.add(n);  
            }  
        }  
        return nodeList;  
    }

	public List<DepartmentTreeNodeVo> qryDeptsNoTop() {
		List<DepartmentTreeNodeVo> rootDept = departmentMapper.qryRootDeptNoTop();	
		List<DepartmentTreeNodeVo> departmentList = departmentMapper.qryDepts();
		if(rootDept!=null&&rootDept.size()>0){
			for(DepartmentTreeNodeVo vo:rootDept){
				recursion(departmentList, vo);  
			}
		}
        return rootDept;  
	}

	public Department selectById(Long id) {
		return departmentMapper.selectById(id);
	}

	

	

}
