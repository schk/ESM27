package cn.stronglink.esm27.module.dictionary.vo;

import java.util.List;

public class CarTypeTreeNodeVo{
	
	private Long id;
	
	private String name;
	
	private Long pid;
	
	private List<CarTypeTreeNodeVo> children;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getPid() {
		return pid;
	}
	public void setPid(Long pid) {
		this.pid = pid;
	}
	public List<CarTypeTreeNodeVo> getChildren() {
		return children;
	}
	public void setChildren(List<CarTypeTreeNodeVo> children) {
		this.children = children;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
}
