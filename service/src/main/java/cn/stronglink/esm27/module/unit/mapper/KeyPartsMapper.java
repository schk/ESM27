package cn.stronglink.esm27.module.unit.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.esm27.entity.KeyParts;
import cn.stronglink.esm27.module.unit.vo.KeyPartsVo;

public interface KeyPartsMapper extends BaseMapper<KeyParts>{

	List<KeyPartsVo> getListByParams(Page<KeyPartsVo> page, Map<String, Object> params);

	KeyPartsVo qryById(Long id);

	void delDangers(Long id);

	List<KeyPartsVo> getPartsByUnit(Map<String, Object> params);

	List<KeyParts> qryList();

	void delDangersByKeyParts(Long keyPartsId);

	int batchInsertData(@Param("interimList") List<KeyParts> interimList);

}
