package cn.stronglink.esm27.module.system.role.vo;

import java.util.List;

import cn.stronglink.esm27.entity.Role;

public class RoleVo extends Role{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Long> permission;
	
	private String permissionName;

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public List<Long> getPermission() {
		return permission;
	}
	public void setPermission(List<Long> permission) {
		this.permission = permission;
	}
	
	
}
