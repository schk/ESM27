package cn.stronglink.esm27.module.accident.vo;

import cn.stronglink.esm27.entity.AccidentCase;

public class FireCaseVo extends AccidentCase{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String typeName;

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
}
