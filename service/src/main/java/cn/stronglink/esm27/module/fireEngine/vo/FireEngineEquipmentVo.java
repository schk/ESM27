package cn.stronglink.esm27.module.fireEngine.vo;

import cn.stronglink.esm27.entity.FireEngineEquipment;

public class FireEngineEquipmentVo extends FireEngineEquipment {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2183093532687523567L;
	
	 
	private String name;
	
	private String code;
	
	private String selfCode;
	
	private String specificationsModel;
	
	private String unit;
	
	public String getSelfCode() {
		return selfCode;
	}

	public void setSelfCode(String selfCode) {
		this.selfCode = selfCode;
	}

	public String getSpecificationsModel() {
		return specificationsModel;
	}

	public void setSpecificationsModel(String specificationsModel) {
		this.specificationsModel = specificationsModel;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getStorageQuantity() {
		return storageQuantity;
	}

	public void setStorageQuantity(String storageQuantity) {
		this.storageQuantity = storageQuantity;
	}

	private String storageQuantity;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	

}
