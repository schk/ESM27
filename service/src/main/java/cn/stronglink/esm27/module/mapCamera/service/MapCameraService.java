package cn.stronglink.esm27.module.mapCamera.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.MapCamera;
import cn.stronglink.esm27.module.mapCamera.mapper.MapCameraMapper;

@Service
@Transactional(rollbackFor=Exception.class)
public class MapCameraService {
	
	@Autowired
	private MapCameraMapper mapCameraMapper;
	
	
	public void saveMapImg(MapCamera entity) {
		Date date=new Date();
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(date);
		entity.setUpdateTime(date);
		mapCameraMapper.insert(entity);
	}

	
}
