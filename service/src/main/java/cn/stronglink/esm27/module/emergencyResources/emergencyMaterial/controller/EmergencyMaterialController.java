package cn.stronglink.esm27.module.emergencyResources.emergencyMaterial.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.EmergencyMaterial;
import cn.stronglink.esm27.entity.EmergencyMaterialTemp;
import cn.stronglink.esm27.entity.EmergencyMaterialType;
import cn.stronglink.esm27.module.emergencyResources.emergencyMaterial.service.EmergencyMaterialService;
import cn.stronglink.esm27.module.emergencyResources.emergencyMaterial.vo.EmergencyMaterialVo;

@Controller
@RequestMapping("emergencyMaterial")
public class EmergencyMaterialController extends AbstractController {

	@Autowired
	private EmergencyMaterialService emergencyMaterialService;
	

	/**
	 * 查询列表
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		Page<EmergencyMaterialVo> page = (Page<EmergencyMaterialVo>) super.getPage(params);
		Page<EmergencyMaterialVo> data = emergencyMaterialService.qryListByParams(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 通过id查询信息
	 */
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> qryUserById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		EmergencyMaterialVo vo = emergencyMaterialService.selectById(id);
		return setSuccessModelMap(modelMap, vo);
	}
	/**
	 * 新建信息
	 */
	@RequestMapping(value = "create")
	@OperateLog(module = "应急物资管理",desc="添加应急物资", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody EmergencyMaterial entity) {
		emergencyMaterialService.insert(entity);
		return setSuccessModelMap(modelMap, null);
	}
	/**
	 * 修改信息
	 */
	@RequestMapping(value = "edit")
	@OperateLog(module = "应急物资管理",desc="修改应急物资", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody EmergencyMaterial entity) {
		emergencyMaterialService.update(entity);
		return setSuccessModelMap(modelMap, null);
	}
	/**
	 * 删除信息
	 */
	@RequestMapping(value = "remove")
	@OperateLog(module = "应急物资管理",desc="删除应急物资", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		emergencyMaterialService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 继续导入应急专家数据
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param timestamp
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "importConfirm")
	public ResponseEntity<ModelMap> importExpertConfirm(HttpServletRequest request,
			HttpServletResponse response,ModelMap modelMap, @RequestBody Long timestamp) throws Exception{
		Date date = new Date();
		// 遍历单元簿中每一行，获取每一列字段，并根据表头对应赋值
		List<EmergencyMaterial> expertList = new ArrayList<EmergencyMaterial>();
		List<EmergencyMaterialType> typeList = emergencyMaterialService.qryEmergencySuppliesType();
		List<EmergencyMaterialTemp> expertTempList =emergencyMaterialService.qryEmergencyMaterialTemp(timestamp);
		//插入execl中得数据到临时表
		for (int j = 0; j < expertTempList.size(); j++) {
			EmergencyMaterialTemp row = expertTempList.get(j);
			EmergencyMaterial expert = null;
			if (row!=null) {
				expert = new EmergencyMaterial();
				if(typeList!=null &&typeList.size()>0){
					boolean isExit=false;
					for(EmergencyMaterialType type : typeList){
						if(type.getName().equals(row.getTypeId())){
							isExit=true;
							expert.setTypeId(type.getId().toString());
							break;
						}
					}
					if(!isExit){
						//专家类型不存在，添加专家类型
						EmergencyMaterialType entity =new EmergencyMaterialType();
						entity.setId(IdWorker.getId());
						entity.setName(row.getTypeId());
						entity.setRemark(row.getTypeId());
						entity.setCreateTime(new Date());
						emergencyMaterialService.insertEmergencySuppliesType(entity);
						expert.setTypeId(entity.getId().toString());
						typeList.add(entity);
					}
				}
				expert.setId(IdWorker.getId());
				expert.setName(row.getName());
				expert.setReservePointId(Long.valueOf(row.getReservePointId()));
				expert.setSpecifications(row.getSpecifications());
				expert.setModel(row.getModel());
				expert.setStorageQuantity(Integer.parseInt(row.getStorageQuantity()));
				expert.setStorageUnit(row.getStorageUnit());
				expert.setChargePerson(row.getChargePerson());
				expert.setSize(row.getSize());
				expert.setPhone(row.getPhone());
				expert.setAvailable(1);
				expert.setPurpose(row.getPurpose());
				expert.setRemark(row.getRemark());
				expert.setCreateBy(WebUtil.getCurrentUser());
				expert.setCreateTime(date);
				expert.setUpdateTime(date);
				
				expertList.add(expert);
			}
		}
		if(expertList != null && expertList.size() > 0) {
			//TODO 数据存储
			emergencyMaterialService.exportEXLExpert(expertList);
			//删除临时表数据
			emergencyMaterialService.delExpertTemp(timestamp);
			return setSuccessModelMap(modelMap);
		} else {
			throw new BusinessException("文件数据为空!");
		}
		
	}

	
	/**
	 * 删除应急专家临时表数据
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param timestamp
	 * @return
	 */
	@RequestMapping(value = "delTemp")
	public ResponseEntity<ModelMap> delExpertTemp(HttpServletRequest request,
			HttpServletResponse response,ModelMap modelMap, @RequestBody Long timestamp){
		//删除临时表数据
		emergencyMaterialService.delExpertTemp(timestamp);
		return setSuccessModelMap(modelMap);
		
	}
}
