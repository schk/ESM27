package cn.stronglink.esm27.module.expr.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.Expr;
import cn.stronglink.esm27.module.expr.service.ExprService;
import cn.stronglink.esm27.module.expr.vo.ExprVo;

@Controller
@RequestMapping("expr")
public class ExprController extends AbstractController{
	
	@Autowired
	private ExprService exprService;
	
	@RequestMapping(value = "qryExprList")
	public ResponseEntity<ModelMap> qryExprList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<Expr> page = (Page<Expr>) super.getPage(params);
		Expr expr = new Expr();
		if(params.get("name") != null){
			expr.setName(params.get("name").toString());
		}
		Page<Expr> data = exprService.getExprByParams(page,expr);
		return setSuccessModelMap(modelMap, data);
	}
	
	@RequestMapping(value = "remove")
	@OperateLog(module = "公式管理",desc="删除公式", type = OpType.DEL)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		exprService.removeExpr(id);
		return setSuccessModelMap(modelMap, null);
	}
	
	@RequestMapping(value = "create")
	@OperateLog(module = "公式管理",desc="创建公式", type = OpType.DEL)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody ExprVo exprVo) {
		
		exprService.insertExpr(exprVo);
		return setSuccessModelMap(modelMap, null);
	}
	
	@RequestMapping(value = "update")
	@OperateLog(module = "公式管理",desc="编辑公式", type = OpType.DEL)
	public ResponseEntity<ModelMap> update(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody ExprVo exprVo) {
		exprService.updateExpr(exprVo);
		return setSuccessModelMap(modelMap, null);
	}
	
	@RequestMapping(value = "qryInfo")
	public ResponseEntity<ModelMap> qryInfo(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		ExprVo exprVo=exprService.qryInfo(id);
		return setSuccessModelMap(modelMap, exprVo);
	}
}
