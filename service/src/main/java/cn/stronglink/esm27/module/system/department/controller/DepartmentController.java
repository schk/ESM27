package cn.stronglink.esm27.module.system.department.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.config.Resources;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.esm27.entity.Department;
import cn.stronglink.esm27.module.system.department.service.DepartmentService;
import cn.stronglink.esm27.module.system.department.vo.DepartmentTreeNodeVo;
@Controller
@RequestMapping(value = "sys/dept")
public class DepartmentController extends AbstractController {

	@Autowired
	private DepartmentService departmentService;

	/**
	 * 查询带最顶部的组织机构
	 * @param modelMap
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "qryDepts")
	public ResponseEntity<ModelMap> qryDepts(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<DepartmentTreeNodeVo> vo = departmentService.getTree();
		return setSuccessModelMap(modelMap, vo);
	}

	/**
	 * 查询不带顶部的组织机构
	 * @param modelMap
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "qryDeptsNoTop")
	public ResponseEntity<ModelMap> qryDeptsAllTree(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<DepartmentTreeNodeVo> vo = departmentService.qryDeptsNoTop();
		return setSuccessModelMap(modelMap, vo);
	}
	
	
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		Department dept =departmentService.selectById(id);
		return setSuccessModelMap(modelMap, dept);
	}
	
	@OperateLog(module = "组织机构",desc="添加组织机构数据", type = OpType.ADD)
	@RequestMapping(value = "create")
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Department entity) {
		departmentService.insertDept(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	@OperateLog(module = "组织机构",desc="修改组织机构数据", type = OpType.UPDATE)
	@RequestMapping(value = "edit")
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Department entity) {
		departmentService.updateDept(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	@OperateLog(module = "组织机构",desc="删除组织机构数据", type = OpType.DEL)
	@RequestMapping(value = "remove")
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		
		int count = departmentService.getChildDetpCount(id);
		if(count > 0){		
			return setModelMap(modelMap, HttpCode.CONFLICT, Resources.getMessage("HAVECHILDDEPT"));
		}
		departmentService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}

}
