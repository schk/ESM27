package cn.stronglink.esm27.module.facilities.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.esm27.entity.Facilities;
import cn.stronglink.esm27.entity.FacilitiesTemp;
import cn.stronglink.esm27.module.facilities.vo.FacilitiesVo;

public interface FacilitiesMapper extends BaseMapper<Facilities>  {


	//查找全部信息
	List<FacilitiesVo> getObject(Pagination page, Map<String, Object> params);

	FacilitiesVo qryById(@Param("id")Long id);

	int batchInsert(@Param("interimList") List<Facilities> interimList);

	int batchInsertTemp(@Param("interimList") List<FacilitiesTemp> interimList);

	List<FacilitiesTemp> qryExcelTemp(Long timestamp);

	void delExcelTemp(Long timestamp);

	List<FacilitiesVo> qryList();

}
