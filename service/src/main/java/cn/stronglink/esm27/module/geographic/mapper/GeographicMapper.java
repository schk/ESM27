package cn.stronglink.esm27.module.geographic.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.GeographicEdit;
import cn.stronglink.esm27.entity.Resource;


public interface GeographicMapper extends BaseMapper<GeographicEdit>{

	List<GeographicEdit> getRoadList();

	List<Resource> queryResource();




}
