package cn.stronglink.esm27.module.system.user.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.core.util.BmUtils;
import cn.stronglink.core.util.ImportValid;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.User;
import cn.stronglink.esm27.module.system.user.service.UserService;
import cn.stronglink.esm27.module.system.user.vo.UserVo;
import cn.stronglink.esm27.shiro.PasswordSaltUtil;


@Controller
@RequestMapping(value = "user")
public class UserController extends AbstractController  {
	
	@Autowired
	private UserService userService;
	
	/**
	 * 查询列表
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "qryUserList")
	public ResponseEntity<ModelMap> qryUserList(ModelMap modelMap, HttpServletRequest request,
			 HttpServletResponse response,@RequestBody Map<String, Object> params) {
		if(request.getSession().getAttribute("admin")!=null){
			boolean isAdmin = (boolean) request.getSession().getAttribute("admin");
			if(!isAdmin) {
//				if(request.getSession().getAttribute("userBrigade")!=null){
//					List<Long> fireBrigadeIds = (List<Long>) request.getSession().getAttribute("userBrigade");
//					params.put("fireBrigadeIds",fireBrigadeIds);
//				}
				User u =  (User) request.getSession().getAttribute("currentUser");
				if(u!=null) {
					Long brigadeId =userService.selectUserBrigade(u.getId());
					List<Long> fireBrigadeIds = userService.selectUserBrigadeChild(brigadeId);	
					params.put("fireBrigadeIds",fireBrigadeIds);
				}
			}
		}
		Page<UserVo> page = (Page<UserVo>) super.getPage(params);
		Page<UserVo> data = userService.getUserByParams(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 通过id查询人员
	 */
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> qryUserById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		User user = userService.qryUserById(id);
		return setSuccessModelMap(modelMap, user);
	}
	/**
	 * 通过id查询详情
	 */
	@RequestMapping(value = "findInfo")
	public ResponseEntity<ModelMap> findInfo(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		User user = userService.findInfo(id);
		return setSuccessModelMap(modelMap, user);
	}
	/**
	 * 新建人员
	 */
	@RequestMapping(value = "create")
	@OperateLog(module = "用户管理",desc="添加用户", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody User entity) {
		//查询当前用户名是否存在
		Integer usernameCount=userService.getCountByUsername(entity);
		if(usernameCount>0){
			throw new BusinessException("用户名已存在");
		}
		userService.insertUser(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 修改人员
	 */
	@RequestMapping(value = "edit")
	@OperateLog(module = "人员管理",desc="修改人员", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody User entity) {
		//查询当前用户名是否存在
		Integer usernameCount=userService.getCountByUsername(entity);
		if(usernameCount>0){
			throw new BusinessException("用户名已存在");
		}
		userService.updateUser(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 删除用户
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "remove")
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		userService.removeUser(id);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 重置密码
	 */
	@RequestMapping(value = "resetUserPW")
	@OperateLog(module = "人员管理",desc="重置密码", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> resetUserPW(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody User user) {
		userService.resetUserPW(user);
		return setSuccessModelMap(modelMap, null);
	}
	
	@RequestMapping(value = "importExpert")
	public ResponseEntity<ModelMap> importExpert(@RequestParam(value = "file", required = true) MultipartFile file,HttpServletRequest request,
			HttpServletResponse response,ModelMap modelMap) throws Exception{
		List<String> dataList=new ArrayList<String>();
		//导入Excel操作
		//导入Excel文件不为空时，将文件写入流，然后转化为工作薄
		if (file!=null){
			try {	
				Workbook workBook = null;
				if(!ImportValid.validXls(file)){
					return setModelMap(modelMap, HttpCode.CONFLICT, "请选择excel格式文件！");
				}
				
				int header = 1;
				workBook = WorkbookFactory.create(file.getInputStream());
				if (workBook.getNumberOfSheets() > 0){
					//获取第一个单元薄
					Sheet sheet = workBook.getSheetAt(0);
					//获取单元薄中表头信息,并将表头中每个单元格中信息存入数组headNames中
					String allTitle = "*用户名$*姓名$*员工号$*职位$*电话$*邮箱$*性别$*所属机构$*所属消防队";
					Row rowHead = sheet.getRow(header); 
					if (rowHead != null) {
						String[] headNames = new String[rowHead.getLastCellNum()];
						// 获取该行中总共有多少列数据row.getLastCellNum()
						for (int j = 0; j < rowHead.getLastCellNum(); j++) {
							Cell cell = rowHead.getCell(j);
							if (cell == null) {
								continue;
							} else {
								// 获取表头各个字段名称
								String returnStr = BmUtils.getCellValue(cell);
								headNames[j] = returnStr;
								if (!allTitle.contains(returnStr)) {
									throw new BusinessException("属性:"+returnStr+"在系统中不存在,请使用模板文件！");
								}
							}
						}
						
						Date date = new Date();
						// 遍历单元簿中每一行，获取每一列字段，并根据表头对应赋值
						int flag = 3;
						List<User> objList = new ArrayList<User>();
						modelMap.put("errorCode", 0);
						//插入execl中得数据到临时表
						for (int i = header; i < sheet.getPhysicalNumberOfRows(); i++) {
							Row row = sheet.getRow(i + 1);
							Boolean isRow = ImportValid.isRowEmpty(row);
							User obj = null;
							if (!isRow) {
								String rowError = "错误： 第"+ flag +"行：";
								String data="";
								obj = new User();
								obj.setId(IdWorker.getId());
								obj.setPassword("123456");
								obj = PasswordSaltUtil.encrypt(obj);
								obj.setCreateBy(WebUtil.getCurrentUser());
								obj.setCreateTime(date);
								obj.setUpdateTime(date);
								// 获取该行中总共有多少列数据row.getLastCellNum()
								for (int j = 0; j < row.getLastCellNum(); j++) {
									Cell cell = row.getCell(j);
									if (cell == null) {
										continue;
									} else if (StringUtils.hasText(headNames[j])) {
										// 获取单元格内容，并根据单元格类型，进行转化
										String returnStr = BmUtils.getCellValue(cell);
										// 根据给定的Excel格式，进行导入，一下是对应列对应字段
									    if (headNames[j].equals("*用户名")) {
											if(ImportValid.isNotEmpty(returnStr)){
												obj.setAccount(returnStr);
												int exits = 0;
												boolean ifExists = false;
												for (User o : objList) {
													exits++;
													if (returnStr.equals(o.getAccount())) {
														ifExists = true;
														break;
													}
												}
												if (ifExists) {
													data += "与第" + exits + "行【用户名："+returnStr+"】 重复！";
													break;
												}
												
												if (userService.getCountByUsername(obj).intValue() > 0) {
													data += "【用户名："+returnStr+"】已存在！";
													break;
												}
												
											}else{
												data += "【用户名】不能为空;";
											}
										}else if (headNames[j].equals("*姓名")) {				
											if(ImportValid.isNotEmpty(returnStr)){
												obj.setName(returnStr);
											}else{
												data+="【姓名】不能为空;";
											}
										}else if (headNames[j].equals("*性别")) {											
											if(ImportValid.isNotEmpty(returnStr)){
												if ("男".equals(returnStr)) {
													obj.setSex(1);
												}else if("女".equals(returnStr)) {
													obj.setSex(2);
												}else{
													data += "【性别："+returnStr+"】不存在,只能是男/女;";
												}
											}else{
												data += "【性别】不能为空;";
											}
										}else if (headNames[j].equals("*员工号")) {				
											if(ImportValid.isNotEmpty(returnStr)){
												obj.setEmployeeNo(returnStr);
											}else{
												data+="【员工号】不能为空;";
											}
										}else if (headNames[j].equals("*职位")) {
											if(ImportValid.isNotEmpty(returnStr)){
												obj.setPosition(returnStr);
											}else{
												data+="【职位】不能为空;";
											}
										}else if (headNames[j].equals("*电话")) {				
											if(ImportValid.isNotEmpty(returnStr)){
												obj.setPhone(returnStr);
											}else{
												data+="【电话】不能为空;";
											}
										}else if (headNames[j].equals("*邮箱")) {				
											if(ImportValid.isNotEmpty(returnStr)){
												obj.setEmail(returnStr);
											}else{
												data += "【邮箱】不能为空;";
											}
										}else if (headNames[j].equals("*所属机构")) {				
											if(ImportValid.isNotEmpty(returnStr)){
												Long deptId = userService.findDeptIdByName(returnStr);
												if (deptId == null) {
													data+="【所属机构："+returnStr+"】在系统中不存在，请先添加;";
												} else {
													obj.setDeptId(deptId);
												}
											}else{
												data+="【所属机构："+returnStr+"】不能为空;";
											}
										}else if (headNames[j].equals("*所属消防队")) {				
											if(ImportValid.isNotEmpty(returnStr)){
												Long brigadeId = userService.findBrigadeIdByName(returnStr);
												if (brigadeId == null) {
													data+="【所属消防队："+returnStr+"】在系统中不存在，请先添加;";
												} else {
													obj.setFireBrigadeId(brigadeId);
												}
											}else{
												data += "【所属消防队】不能为空;";
											}
										}
									}
								}
								flag++;
								if(ImportValid.isNotEmpty(data)){
									dataList.add(rowError+data);
								} else {
									objList.add(obj);
								}
							}
						}
						if(dataList!=null&&!dataList.isEmpty()){							
							modelMap.put("errorCode",2);
							return setSuccessModelMap(modelMap, dataList);
						}else{
							//存数据
							if(objList != null && !objList.isEmpty()) {
								//TODO 数据存储
								userService.batchInsertUser(objList);;
								return setSuccessModelMap(modelMap);
							} else {
								throw new BusinessException("文件数据为空!");
							}
						}						
					}
				}
			}catch (IOException e) {
				throw new BusinessException("未知异常!");
			}
		}
		throw new BusinessException("无文件!");
	}

}
