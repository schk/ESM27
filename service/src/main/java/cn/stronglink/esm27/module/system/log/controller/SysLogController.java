package cn.stronglink.esm27.module.system.log.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.esm27.entity.SysOperateLog;
import cn.stronglink.esm27.module.system.log.service.SysLogService;


@Controller
@RequestMapping(value = "system/log")
public class SysLogController extends AbstractController {
	
	@Autowired
	private SysLogService sysLogService;
	
	@RequestMapping(value = "qryListByParam")
	public ResponseEntity<ModelMap> qryList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<SysOperateLog> page = (Page<SysOperateLog>) super.getPage(params);
		Page<SysOperateLog> data = sysLogService.qryList(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> qryById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		SysOperateLog data = sysLogService.qryById(id);
		return setSuccessModelMap(modelMap, data);
	}
}
