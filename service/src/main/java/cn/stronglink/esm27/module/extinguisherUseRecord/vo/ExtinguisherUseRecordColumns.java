package cn.stronglink.esm27.module.extinguisherUseRecord.vo;

import java.util.List;

public class ExtinguisherUseRecordColumns {

	private String title;
	private String dataIndex;
	private String key;
	private String width;
	private String	fixed;
	private List<ExtinguisherUseRecordColumns> children;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDataIndex() {
		return dataIndex;
	}
	public void setDataIndex(String dataIndex) {
		this.dataIndex = dataIndex;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getWidth() {
		return width;
	}
	public void setWidth(String width) {
		this.width = width;
	}
	public List<ExtinguisherUseRecordColumns> getChildren() {
		return children;
	}
	public void setChildren(List<ExtinguisherUseRecordColumns> children) {
		this.children = children;
	}
	public String getFixed() {
		return fixed;
	}
	public void setFixed(String fixed) {
		this.fixed = fixed;
	}
	
}
