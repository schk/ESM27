package cn.stronglink.esm27.module.extinguisher.vo;

import java.util.List;

import cn.stronglink.esm27.entity.Extinguisher;

public class ExtinguisherClo  extends Extinguisher{

	/**
	 * 
	 */
	private static final long serialVersionUID = -227879607016972179L;
	
	
	private String title;
	private String dataIndex;
	private String key;
	private String width;
	private String fixed;
	private List<ExtinguisherClo> children;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDataIndex() {
		return dataIndex;
	}
	public void setDataIndex(String dataIndex) {
		this.dataIndex = dataIndex;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getWidth() {
		return width;
	}
	public void setWidth(String width) {
		this.width = width;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public List<ExtinguisherClo> getChildren() {
		return children;
	}
	public void setChildren(List<ExtinguisherClo> children) {
		this.children = children;
	}
	public String getFixed() {
		return fixed;
	}
	public void setFixed(String fixed) {
		this.fixed = fixed;
	}
	
	


}
