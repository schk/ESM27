package cn.stronglink.esm27.module.socialResource.controller;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.core.util.BmUtils;
import cn.stronglink.core.util.ImportValid;
import cn.stronglink.core.util.MapLonLatUtil;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.SocialResource;
import cn.stronglink.esm27.module.equipment.equipmentType.service.EquipmentTypeService;
import cn.stronglink.esm27.module.socialResource.service.SocialResourceService;

@Controller
@RequestMapping(value = "socialResource/excel")
public class SocialResourceExcelController extends AbstractController {
	@Autowired
	private SocialResourceService socialResourceService;
	@Autowired
	private EquipmentTypeService equipmentTypeService;

	@RequestMapping(value = "importExpert")
	public ResponseEntity<ModelMap> importExpert(@RequestParam(value = "file", required = true) MultipartFile file,
			HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) throws Exception {
		List<String> dataList = new ArrayList<String>();
		// 导入Excel操作
		// 导入Excel文件不为空时，将文件写入流，然后转化为工作薄
		if (file != null) {
			try {
				Workbook workBook = null;
				if (!ImportValid.validXls(file)) {
					return setModelMap(modelMap, HttpCode.CONFLICT, "请选择excel格式文件！");
				}

				int header = 1;
				workBook = WorkbookFactory.create(file.getInputStream());
				if (workBook.getNumberOfSheets() > 0) {
					// 获取第一个单元薄
					Sheet sheet = workBook.getSheetAt(0);
					// 获取单元薄中表头信息,并将表头中每个单元格中信息存入数组headNames中
					String allTitle = "*名称$*所属消防队$*地址$*消防管理人员$*所属类型编码$*值班室电话$供水能力$*坐标类型(百度坐标/无偏移坐标)$经纬度$简介";
					Row rowHead = sheet.getRow(header);
					if (rowHead != null) {
						String[] headNames = new String[rowHead.getLastCellNum()];
						// 获取该行中总共有多少列数据row.getLastCellNum()
						for (int j = 0; j < rowHead.getLastCellNum(); j++) {
							Cell cell = rowHead.getCell(j);
							// 获取表头各个字段名称
							String returnStr = BmUtils.getCellValue(cell);
							headNames[j] = returnStr;
							if (!allTitle.contains(returnStr)) {
								throw new BusinessException("属性：" + returnStr + "在系统中不存在,请使用模板文件！");
							}
						}

						Date date = new Date();
						// 遍历单元簿中每一行，获取每一列字段，并根据表头对应赋值
						int flag = 3;
						List<SocialResource> objList = new ArrayList<SocialResource>();
						modelMap.put("errorCode", 0);

						DecimalFormat df = new DecimalFormat("#.000000");

						// 插入execl中得数据到临时表
						for (int i = header; i < sheet.getPhysicalNumberOfRows(); i++) {
							Row row = sheet.getRow(i + 1);
							Boolean isRow = ImportValid.isRowEmpty(row);
							SocialResource obj = null;
							if (!isRow) {
								String rowError = "错误：第" + flag + "行  ";
								String data = "";
								String lonLatType = "";
								obj = new SocialResource();
								obj.setId(IdWorker.getId());
								obj.setCreateBy(WebUtil.getCurrentUser());
								obj.setCreateTime(date);
								obj.setUpdateTime(date);
								// 获取该行中总共有多少列数据row.getLastCellNum()
								for (int j = 0; j < row.getLastCellNum(); j++) {
									if(j>headNames.length-1) {
										continue ;
									}
									Cell cell = row.getCell(j);
									if (StringUtils.hasText(headNames[j])) {
										// 获取单元格内容，并根据单元格类型，进行转化
										String returnStr = BmUtils.getCellValue(cell);
										// 根据给定的Excel格式，进行导入，一下是对应列对应字段
										if (headNames[j].equals("*名称")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												obj.setName(returnStr);
												int exits = 0;
												boolean ifExists = false;
												for (SocialResource o : objList) {
													exits++;
													if (returnStr.replace(" ", "")
															.equals(o.getName().replace(" ", ""))) {
														ifExists = true;
														break;
													}
												}
												if (ifExists) {
													data += "与第" + exits + "行【名称：" + returnStr + "】 重复！";
													break;
												}

												if (socialResourceService.getCountByName(obj) > 0) {
													data += "【名称：" + returnStr + "】已存在！";
													break;
												}

											} else {
												data += "【名称】不能为空;";
											}
										} else if (headNames[j].equals("*所属消防队")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												obj.setFireBrigade(returnStr);
											} else {
												data += "【所属消防队】不能为空;";
											}
										} else if (headNames[j].equals("*地址")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												obj.setAddr(returnStr);
											} else {
												data += "【地址】不能为空;";
											}
										} else if (headNames[j].equals("*消防管理人员")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												obj.setAdmin(returnStr);
											} else {
												data += "【消防管理人员】不能为空;";
											}
										} else if (headNames[j].equals("*所属类型编码")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												Long typeId = equipmentTypeService.qryEquipmentTypeIdByCode(returnStr);
												if (typeId == null) {
													data += "【所属类型编码：" + returnStr + "】不存在,请先添加;";
												} else {
													obj.setTypeId(typeId);
												}
											} else {
												data += "【所属类型编码】不能为空;";
											}
										} else if (headNames[j].equals("*值班室电话")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												obj.setTel(returnStr);
											} else {
												data += "【值班室电话】不能为空;";
											}
										} else if (headNames[j].equals("供水能力")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												obj.setWaterSupply(returnStr);
											}
										} else if (headNames[j].equals("*坐标类型(百度坐标/无偏移坐标)")) {
											if (returnStr != null && !"".equals(returnStr)) {
												lonLatType = returnStr;
											} else {
												data += "坐标类型不能为空;";
											}
										} else if (headNames[j].equals("经纬度")) {
											if (returnStr != null && !"".equals(returnStr)) {
												String[] coord = returnStr.split(",");
												if (coord != null && coord.length == 2) {
													coord[0] = df.format(Double.valueOf(coord[0]));
													coord[1] = df.format(Double.valueOf(coord[1]));
													if ("百度坐标".equals(lonLatType)) {
														double[] pointwgs = MapLonLatUtil.bd09py(
																Double.valueOf(coord[0]), Double.valueOf(coord[1]));
														obj.setBaiduLon(Double.valueOf(coord[0]));
														obj.setBaiduLat(Double.valueOf(coord[1]));
														obj.setLon(Double.valueOf(pointwgs[0]));
														obj.setLat(Double.valueOf(pointwgs[1]));
													} else if ("无偏移坐标".equals(lonLatType)) {
														obj.setLon(Double.valueOf(coord[0]));
														obj.setLat(Double.valueOf(coord[1]));
													} else if ("无偏移坐标".equals(lonLatType)) {
														data += "【坐标类型】只能是百度坐标/无偏移坐标;";
													}
												} else {
													data += "【经纬度】格式错误;";
												}
											}
										} else if (headNames[j].equals("简介")) {
											obj.setDesc(returnStr);
										}
									}
								}
								flag++;

								if (ImportValid.isNotEmpty(data)) {
									dataList.add(rowError + data);
								} else {
									objList.add(obj);
								}
							}
						}
						if (dataList != null && dataList.size() > 0) {
							modelMap.put("errorCode", 2);
							return setSuccessModelMap(modelMap, dataList);
						} else {
							// 存数据
							if (objList != null && !objList.isEmpty()) {
								// TODO 数据存储
								socialResourceService.batchInsert(objList);
								return setSuccessModelMap(modelMap);
							} else {
								throw new BusinessException("文件数据为空!");
							}
						}
					}
				}
			} catch (IOException e) {
				throw new BusinessException("未知异常!");
			}
		}
		throw new BusinessException("无文件!");
	}

}
