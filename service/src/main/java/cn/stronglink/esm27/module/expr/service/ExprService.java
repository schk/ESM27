package cn.stronglink.esm27.module.expr.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Expr;
import cn.stronglink.esm27.entity.ExprParam;
import cn.stronglink.esm27.entity.ExprParamOption;
import cn.stronglink.esm27.module.expr.mapper.ExprMapper;
import cn.stronglink.esm27.module.expr.vo.ExprVo;

@Service
@Transactional(rollbackFor=Exception.class)
public class ExprService {
	
	@Autowired
	private ExprMapper exprMapper;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<Expr> getExprByParams(Page<Expr> page, Expr expr) {
		page.setRecords(exprMapper.getExprByParams(page,expr));	
		return page;
	}

	public void removeExpr(Long id) {
		exprMapper.deleteById(id);
		exprMapper.deleteParamByExprId(id);
	}
	
	public List<Expr> getList(){
		return exprMapper.selectList(null);
	}

	public void insertExpr(ExprVo exprVo) {
		exprVo.setId(IdWorker.getId());
		exprVo.setCreateBy(WebUtil.getCurrentUser());
		exprVo.setCreateTime(new Date());
		exprVo.setUpdateTime(new Date());
		exprMapper.createExpr(exprVo);
		if(exprVo.getExprParams()!=null&&exprVo.getExprParams().size()>0){
			for(ExprParam param:exprVo.getExprParams()){
				param.setId(IdWorker.getId());
				param.setExprId(exprVo.getId());
				param.setCreateTime(new Date());
				param.setCreateBy(WebUtil.getCurrentUser());
				if(param.getChildOption()!=null&&param.getChildOption().size()>0){
					for(ExprParamOption option:param.getChildOption()){
						option.setId(IdWorker.getId());
						option.setExprParamId(param.getId());
						option.setCreateTime(new Date());
						option.setCreateBy(WebUtil.getCurrentUser());
					}
					exprMapper.createExprParamsOption(param);
				}
			}
			exprMapper.createExprParams(exprVo);
		}
		
	}

	public void updateExpr(ExprVo exprVo) {
		exprMapper.deleteParamOptionByExprId(exprVo.getId());
		exprMapper.deleteParamByExprId(exprVo.getId());
		exprVo.setUpdateBy(WebUtil.getCurrentUser());
		exprVo.setUpdateTime(new Date());
		exprMapper.updateExpr(exprVo);
		if(exprVo.getExprParams()!=null&&exprVo.getExprParams().size()>0){
			for(ExprParam param:exprVo.getExprParams()){
				param.setId(IdWorker.getId());
				param.setExprId(exprVo.getId());
				param.setCreateTime(new Date());
				param.setCreateBy(WebUtil.getCurrentUser());
				if(param.getChildOption()!=null&&param.getChildOption().size()>0){
					for(ExprParamOption option:param.getChildOption()){
						option.setId(IdWorker.getId());
						option.setExprParamId(param.getId());
						option.setCreateTime(new Date());
						option.setCreateBy(WebUtil.getCurrentUser());
					}
					exprMapper.createExprParamsOption(param);
				}
			}
			exprMapper.createExprParams(exprVo);
		}
	}

	public ExprVo qryInfo(Long id) {
		ExprVo exprVo=exprMapper.qryInfo(id);
		List<ExprParam> exprParams=exprMapper.getExprParams(id);
		exprVo.setExprParams(exprParams);
		if(exprParams!=null&&exprParams.size()>0){
			for(ExprParam param:exprParams){
				List<ExprParamOption> exprParamsOption=exprMapper.getExprParamsOption(param.getId());
				param.setChildOption(exprParamsOption);
			}
		}
		return exprVo;
	}

	public List<Expr> queryListNoPage(Map<String, Object> params) {
		return exprMapper.queryListNoPage(params);
	}
}
