package cn.stronglink.esm27.module.plotting.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.Plotting;
import cn.stronglink.esm27.module.plotting.service.PlottingService;
import cn.stronglink.esm27.module.plotting.vo.PlottingCVo;

@Controller
@RequestMapping("plotting")
public class PlottingController extends AbstractController{
	
	@Autowired
	private PlottingService plottingService;
	
	/**
	 * 
	 */
	@RequestMapping(value = "qryPlottingList")
	public ResponseEntity<ModelMap> qryList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<Plotting> plottingList = plottingService.qryPlottingList();
		return setSuccessModelMap(modelMap, plottingList);
	}
	/**
	 * 
	 */
	@RequestMapping(value = "qryJhPlottingList")
	public ResponseEntity<ModelMap> qryJhPlottingList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<Plotting> plottingList = plottingService.qryJhPlottingList();
		return setSuccessModelMap(modelMap, plottingList);
	}
	
	
	
	/**
	 * 查询列表
	 */
	@RequestMapping(value = "qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<PlottingCVo> page = (Page<PlottingCVo>) super.getPage(params);
		Page<PlottingCVo> data = plottingService.qryListByParams(page,params);
		return setSuccessModelMap(modelMap, data);
	}

	/**
	 * 通过id查询
	 */
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> qryUserById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		Plotting vo = plottingService.qryById(id);
		return setSuccessModelMap(modelMap, vo);
	}
	
	
	/**
	 * 新建
	 */
	@RequestMapping(value = "create")
	@OperateLog(module = "标绘管理",desc="添加标绘", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Plotting entity) {
		// 查询当前标绘名称是否存在
		Integer count = plottingService.getCountByUsername(entity);
		if(count > 0){
			throw new BusinessException("标绘名称已存在");
		}
		plottingService.insert(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 修改
	 */
	@RequestMapping(value = "edit")
	@OperateLog(module = "标绘管理",desc="修改标绘", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Plotting entity) {
		// 查询当前标绘名称是否存在
		Integer count = plottingService.getCountByUsername(entity);
		if(count > 0){
			throw new BusinessException("标绘名称已存在");
		}
		plottingService.update(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 删除用户
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "remove")
	@OperateLog(module = "标绘管理",desc="删除标绘", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		plottingService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}
	
}
