package cn.stronglink.esm27.module.emergencyResources.teams.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Teams;
import cn.stronglink.esm27.module.emergencyResources.teams.mapper.TeamsMapper;

@Service
@Transactional(rollbackFor = Exception.class)
public class TeamsService {

	@Autowired
	private TeamsMapper teamsMapper;

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Page<Teams> qryListByParams(Page<Teams> page, Map<String, Object> params) {
		page.setRecords(teamsMapper.qryListByParams(page, params));
		return page;
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<Teams> qryList() {
		return teamsMapper.qryList();
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Teams qryById(Long id) {
		Teams selectById = teamsMapper.selectById(id);
		if (selectById.getLon() != null && selectById.getLat() != null) {
			selectById.setLonLat(selectById.getLon() + "," + selectById.getLat());
		}
		return selectById;
	}

	public void insert(Teams entity) {
		if (!"".equals(entity.getLonLat())) {
			String[] lonLat = entity.getLonLat().split(",");
			entity.setLon(Double.valueOf(lonLat[0]));
			entity.setLat(Double.valueOf(lonLat[1]));
		}
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		teamsMapper.insert(entity);

	}

	public void update(Teams entity) {
		if (!"".equals(entity.getLonLat())) {
			String[] lonLat = entity.getLonLat().split(",");
			entity.setLon(Double.valueOf(lonLat[0]));
			entity.setLat(Double.valueOf(lonLat[1]));
		}
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if (teamsMapper.updateById(entity) == 0) {
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
	}

	public void remove(Long id) {
		teamsMapper.deleteById(id);
	}

	public void saveExcelData(List<Teams> entityList) {
		if (entityList != null && entityList.size() > 0) {
			List<Teams> interimList = new ArrayList<Teams>();
			int j = (int) Math.ceil(entityList.size() / (double) 50);
			int num = 0;
			for (int i = 1; i <= j; i++) {
				if (i * 50 >= entityList.size()) {
					interimList = new ArrayList<Teams>(entityList.subList((i - 1) * 50, entityList.size()));
					num = num + this.batchInsert(interimList);
				} else {
					interimList = new ArrayList<Teams>(entityList.subList((i - 1) * 50, i * 50));
					num = num + this.batchInsert(interimList);
				}
			}
			if (num != entityList.size()) {
				throw new BusinessException("导入数据异常!");
			}
		}
	}

	private int batchInsert(List<Teams> interimList) {
		return teamsMapper.batchInsert(interimList);
	}

}
