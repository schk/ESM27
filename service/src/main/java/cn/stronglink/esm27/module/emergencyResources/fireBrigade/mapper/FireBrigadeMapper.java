package cn.stronglink.esm27.module.emergencyResources.fireBrigade.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.esm27.entity.FireBrigade;
import cn.stronglink.esm27.module.emergencyResources.fireBrigade.vo.FireBrigadeTreeNodeVo;
import cn.stronglink.esm27.module.extinguisherUseRecord.vo.FireBrigadeVo;


public interface FireBrigadeMapper extends BaseMapper<FireBrigade>{

	List<FireBrigade> qryListByParams(Pagination page, Map<String, Object> params);

	List<FireBrigade> qryListByParamsForWeb(Page<FireBrigade> page, Map<String, Object> params);
	
	List<FireBrigade> qryList();
	
	public List<FireBrigadeTreeNodeVo> getChildList(@Param("pid") Long id);

	List<FireBrigadeVo> getFileBrigade(Map<String,Object> conditionMap);

	List<Long> getParentIds(Map<String,Object> conditionMap);

	FireBrigade getInfoDetail(Long id);
	
	List<FireBrigadeTreeNodeVo> qryTopById(Long brigadeId);

	List<FireBrigadeTreeNodeVo> qryTop();

	List<FireBrigadeTreeNodeVo> qryAllFireBrigade();
	
	List<FireBrigadeTreeNodeVo> qryBrigadeChild();

	List<FireBrigadeVo> getAllFileBrigade(Map<String, Object> conditionMap);

	Integer getCountByUsername(FireBrigade entity);

	String getfireBrigadeName(Long fireBrigadeId);

	Long findBrigadeIdByName(@Param("brigadeName") String brigadeName);
	
	int batchInsertObj(@Param("interimList") List<FireBrigade> list);

	
}
