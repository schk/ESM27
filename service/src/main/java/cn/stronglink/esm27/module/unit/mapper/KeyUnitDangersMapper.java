package cn.stronglink.esm27.module.unit.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.KeyUnitDangers;
import cn.stronglink.esm27.entity.KeyUnitDangersCatalog;

public interface KeyUnitDangersMapper extends BaseMapper<KeyUnitDangers>{

	List<KeyUnitDangers> qryDangersById(Long id);

	List<KeyUnitDangers> qryKeyUnitDangersById(Long id);

	KeyUnitDangers qryKeyUnitDangersDetailById(Long id);

	void insertKeyUnitDangersCatalogs(Map<String, Object> map);

	void deleteKeyUnitDangersCatalogs(HashMap<String, Object> columnMap);

	List<KeyUnitDangersCatalog> getKeyUnitDangersCatalogs(Long id);

	void create(KeyUnitDangers dangers);



}
