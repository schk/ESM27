package cn.stronglink.esm27.module.emergencyResources.teams.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.Teams;
import cn.stronglink.esm27.module.emergencyResources.teams.service.TeamsService;


@Controller
@RequestMapping(value = "teams")
public class TeamsController extends AbstractController  {
	
	@Autowired
	private TeamsService teamsService;
	
	/**
	 * 查询列表
	 */
	@RequestMapping(value = "qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<Teams> page = (Page<Teams>) super.getPage(params);
		Page<Teams> data = teamsService.qryListByParams(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询列表不带分页
	 */
	@RequestMapping(value = "qryList")
	public ResponseEntity<ModelMap> qryList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<Teams> data = teamsService.qryList();
		return setSuccessModelMap(modelMap, data);
	}
	
	
	/**
	 * 通过id查询人员
	 */
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> qryUserById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		Teams vo = teamsService.qryById(id);
		return setSuccessModelMap(modelMap, vo);
	}
	
	/**
	 * 新建人员
	 */
	@RequestMapping(value = "create")
	@OperateLog(module = "应急队伍管理",desc="添加应急队伍", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Teams entity) {
		teamsService.insert(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 修改人员
	 */
	@RequestMapping(value = "edit")
	@OperateLog(module = "应急队伍管理",desc="修改应急队伍", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Teams entity) {
		teamsService.update(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 删除用户
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "remove")
	@OperateLog(module = "应急队伍管理",desc="删除应急队伍", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		teamsService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}
	
	

}
