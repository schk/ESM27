package cn.stronglink.esm27.module.system.user.vo;

import cn.stronglink.esm27.entity.User;

public class UserVo extends User{

	private static final long serialVersionUID = 1L;
	
	private String deptName;
	private String realName;
	private String roleNames;
	
	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getRoleNames() {
		return roleNames;
	}

	public void setRoleNames(String roleNames) {
		this.roleNames = roleNames;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	
}
