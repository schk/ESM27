package cn.stronglink.esm27.module.unit.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.util.AntdFile;
import cn.stronglink.esm27.entity.KeyUnit;
import cn.stronglink.esm27.entity.KeyUnitDangersRef;
import cn.stronglink.esm27.module.unit.vo.KeyUnitAccidentFile;
import cn.stronglink.esm27.module.unit.vo.KeyUnitPartsVo;

public interface KeyUnitMapper extends BaseMapper<KeyUnit>{

	List<KeyUnit> getListByParams(Page<KeyUnit> page, Map<String, Object> params);
	
	List<KeyUnit> qryKeyUnitList(Map<String, Object> params);

	KeyUnit getkeyUnitName(Long id);	

	KeyUnit qryById(Long id);

	Integer getCountByUsername(KeyUnit entity);

	String getUnitName(Long keyUnitId);

	int batchInsertObj(@Param("interimList") List<KeyUnit> list);

	List<KeyUnitAccidentFile> getKeyUnitAccidentFile(Long id);

	List<AntdFile> qryAccidentByUnitId(Long id);

	void inserKeyUnitAccident(Map<String, Object> map);

	void deleteAccidentInfo(Long id);

	List<KeyUnitPartsVo> qryKeyPartsGroupUnit(Page<KeyUnitPartsVo> page, Map<String, Object> params);

	void delDangersByKeyUnit(@Param("unitId") Long id);

	void insertKeyUnitDangersRef(KeyUnitDangersRef d);

	void delDangers(@Param("id") Long id);
}
