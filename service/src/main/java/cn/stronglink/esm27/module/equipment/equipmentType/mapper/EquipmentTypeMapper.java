package cn.stronglink.esm27.module.equipment.equipmentType.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.esm27.entity.EquipmentType;

public interface EquipmentTypeMapper extends BaseMapper<EquipmentType> {

	List<EquipmentType> getListByParams(Page<EquipmentType> page, Map<String, Object> params);

	List<EquipmentType> qryEquipmentTypeList(int type);

	List<EquipmentType> qryEquTypeList(@Param("list")List<String> types);

	Long qryEquipmentTypeIdByCode(String code);
	

}
