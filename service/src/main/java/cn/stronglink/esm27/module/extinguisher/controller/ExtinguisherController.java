package cn.stronglink.esm27.module.extinguisher.controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.esm27.entity.Extinguisher;
import cn.stronglink.esm27.entity.User;
import cn.stronglink.esm27.module.extinguisher.service.ExtinguisherService;
import cn.stronglink.esm27.module.extinguisher.vo.ExtinguisherVo;
import cn.stronglink.esm27.module.extinguisherUseRecord.param.SearchParam;
import cn.stronglink.esm27.module.system.user.service.UserService;

@Controller
@RequestMapping("extinguisher")
public class ExtinguisherController extends AbstractController{
	
	@Autowired
	private ExtinguisherService extinguisherService;

	@Autowired
	private UserService userService;
	/**
	 * 查询列表
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {		
		if(request.getSession().getAttribute("admin")!=null){
			boolean isAdmin = (boolean) request.getSession().getAttribute("admin");
			if(!isAdmin) {
//				if(request.getSession().getAttribute("userBrigade")!=null){
//					List<Long> fireBrigadeIds = (List<Long>) request.getSession().getAttribute("userBrigade");
//					params.put("fireBrigadeIds",fireBrigadeIds);
//				}
				User u =  (User) request.getSession().getAttribute("currentUser");
				if(u!=null) {
					Long brigadeId =userService.selectUserBrigade(u.getId());
					List<Long> fireBrigadeIds = userService.selectUserBrigadeChild(brigadeId);	
					params.put("fireBrigadeIds",fireBrigadeIds);
				}
			}
		}
		Page<ExtinguisherVo> page = (Page<ExtinguisherVo>) super.getPage(params);
		Page<ExtinguisherVo> data = extinguisherService.qryListByParams(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询消防战队下得灭火剂种类
	 */
	@RequestMapping(value = "qryListByFireBrigade")
	public ResponseEntity<ModelMap> qryListByFireBrigade(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		List<ExtinguisherVo> data = extinguisherService.qryListByFireBrigade(params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询消防战队下得灭火剂种类
	 */
	@RequestMapping(value = "qryListAll")
	public ResponseEntity<ModelMap> qryListAll(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<ExtinguisherVo> data = extinguisherService.qryListAll();
		return setSuccessModelMap(modelMap, data);
	}
	
	
	/**
	 * 查询消防战队下的某种灭火剂的量
	 */
	@RequestMapping(value = "qryExtinguisherQu")
	public ResponseEntity<ModelMap> qryExtinguisherQu(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		Extinguisher data = extinguisherService.getQuantity(params);
		return setSuccessModelMap(modelMap, data);
	}
	/**
	 * 查询某种灭火剂的库存
	 */
	@RequestMapping(value = "qryExtinguishere")
	public ResponseEntity<ModelMap> qryExtinguishere(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody  Map<String, Object> params) {
		BigDecimal data = extinguisherService.qryExtinguishere(params);
		return setSuccessModelMap(modelMap, data);
	}
	/*
	 * 查询灭火剂总量
	 */
	@RequestMapping(value = "qrySumByKeyId")
	public ResponseEntity<ModelMap> qrySumByKeyId(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody SearchParam params) {
		Map<String,Object> data= extinguisherService.qrySumByKeyId(params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 通过id查询信息
	 */
	@RequestMapping(value = "qryById")
	public ResponseEntity<ModelMap> qryUserById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Long id) {
		ExtinguisherVo vo = extinguisherService.selectById(id);
		return setSuccessModelMap(modelMap, vo);
	}
	/**
	 * 新建信息
	 */
	@RequestMapping(value = "create")
	@OperateLog(module = "灭火剂管理",desc="添加灭火剂", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Extinguisher entity) {
		extinguisherService.insert(entity);
		return setSuccessModelMap(modelMap, null);
	}
	/**
	 * 修改信息
	 */
	@RequestMapping(value = "edit")
	@OperateLog(module = "灭火剂管理",desc="修改灭火剂", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Extinguisher entity) {
		extinguisherService.update(entity);
		return setSuccessModelMap(modelMap, null);
	}
	/**
	 * 删除信息
	 */
	@RequestMapping(value = "remove")
	@OperateLog(module = "灭火剂管理",desc="删除灭火剂", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		extinguisherService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}
	

}
