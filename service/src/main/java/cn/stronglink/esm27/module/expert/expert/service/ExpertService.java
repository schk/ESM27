package cn.stronglink.esm27.module.expert.expert.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Expert;
import cn.stronglink.esm27.entity.ExpertTemp;
import cn.stronglink.esm27.module.expert.expert.mapper.ExpertMapper;
import cn.stronglink.esm27.module.expert.expert.vo.ExpertVo;

@Service
@Transactional(rollbackFor=Exception.class)
public class ExpertService {
	
	@Autowired
	private ExpertMapper expertMapper;
	@Resource
	private JdbcTemplate jdbcTemplate;
	
	//查询所有专家信息
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<ExpertVo> qryListByParams(Page<ExpertVo> page, Map<String, Object> params) {
		page.setRecords(expertMapper.qryListByParams(page,params));	
		return page;
	}
	
	/*
	 * 根据id查询信息
	 */
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public ExpertVo selectById(Long id){
	
		return expertMapper.qryById(id);
	}
	//根据id修改信息
	public void update(Expert entity) {
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if(expertMapper.updateAllColumnById(entity)==0){
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
	}
	/*
	 * 删除对象
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public void remove(Long id) {
		expertMapper.deleteById(id);
	}
	/*
	 * 添加信息
	 */
	public void insert(Expert entity) {
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		entity.setUpdateTime(entity.getCreateTime());
		expertMapper.insert(entity);
	}

	public void exportEXLExpert(List<Expert> expertList) {
		if (expertList!=null&&expertList.size()>0) {
			List<Expert> interimList = new ArrayList<Expert>();
			int j =  (int)Math.ceil(expertList.size()/(double)50);
			int num =0;
			for (int i = 1; i <= j;i++) {
				if (i*50>=expertList.size()) {
					interimList = new ArrayList<Expert>(expertList.subList((i - 1) * 50, expertList.size()));
					num =num+this.batchInsertExpert(interimList);
				}else {
					interimList = new ArrayList<Expert>(expertList.subList((i - 1) * 50, i * 50));
					num =num+this.batchInsertExpert(interimList);
				}
			}
			if (num!=expertList.size()) {
				throw new BusinessException("导入数据异常!");
			}
		}	
	}
	
	public void saveExpertTemp(List<ExpertTemp> expertTempList) {
		List<ExpertTemp> interimList = new ArrayList<ExpertTemp>();
		int j =  (int)Math.ceil(expertTempList.size()/(double)50);
		int num =0;
		for (int i = 1; i <= j;i++) {
			if (i*50>=expertTempList.size()) {
				interimList = new ArrayList<ExpertTemp>(expertTempList.subList((i - 1) * 50, expertTempList.size()));
				num =num+this.batchInsertExpertTemp(interimList);
			}else {
				interimList = new ArrayList<ExpertTemp>(expertTempList.subList((i - 1) * 50, i * 50));
				num =num+this.batchInsertExpertTemp(interimList);
			}
		}
		if (num!=expertTempList.size()) {
			throw new BusinessException("导入数据异常!");
		}		
	}
	
	
	private int batchInsertExpertTemp(List<ExpertTemp> interimList) {
		return expertMapper.batchInsertExpertTemp(interimList);
	}

	public int batchInsertExpert(List<Expert> interimList) {
		return expertMapper.batchInsertExpert(interimList);
	}

	public void createTempTable(String sqls) {
		jdbcTemplate.update(sqls);
	}

	public List<ExpertTemp> qryExpertTemp(Long timestamp) {
		return expertMapper.qryExpertTemp(timestamp);
	}

	public void delExpertTemp(Long timestamp) {
		expertMapper.delExpertTemp(timestamp);
		
	}
	
}
