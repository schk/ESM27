package cn.stronglink.esm27.module.extinguisherUseRecord.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.stronglink.esm27.entity.Dictionary;
import cn.stronglink.esm27.entity.ExtinguisherUseRecord;
import cn.stronglink.esm27.entity.FireBrigade;
import cn.stronglink.esm27.module.dictionary.mapper.DictionaryMapper;
import cn.stronglink.esm27.module.emergencyResources.fireBrigade.mapper.FireBrigadeMapper;
import cn.stronglink.esm27.module.extinguisherUseRecord.mapper.ExtinguisherUseRecordMapper;
import cn.stronglink.esm27.module.extinguisherUseRecord.param.SearchParam;
import cn.stronglink.esm27.module.extinguisherUseRecord.vo.ExtinguisherUseRecordColumns;
import cn.stronglink.esm27.module.extinguisherUseRecord.vo.ExtinguisherUseRecordCount;
import cn.stronglink.esm27.module.extinguisherUseRecord.vo.FireBrigadeVo;

@Service
@Transactional(rollbackFor=Exception.class)
public class ExtinguisherUseRecordService {
	
	@Autowired
	private ExtinguisherUseRecordMapper extinguisherUseRecordMapper;
	@Autowired
	private FireBrigadeMapper fireBrigadeMapper;
	
	@Autowired
	private DictionaryMapper dictionaryMapper;
	
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Map<String,Object> qryListByParams(SearchParam params){
		Map<String,Object> result=new HashMap<String,Object>();
		
		List<ExtinguisherUseRecordColumns> columns=new ArrayList<ExtinguisherUseRecordColumns>();
		int width=0;
		//序号
		ExtinguisherUseRecordColumns index=new ExtinguisherUseRecordColumns();
		index.setTitle("序号");
		index.setDataIndex("xh");
		index.setKey("xh");
		index.setFixed("left");
		index.setWidth("60px");
		width+=60;
		columns.add(index);
		ExtinguisherUseRecordColumns types=new ExtinguisherUseRecordColumns();
		types.setTitle("消防队");
		types.setDataIndex("teamName");
		types.setKey("teamName");
		types.setFixed("left");
		types.setWidth("100px");
		width+=150;
		columns.add(types);
		List<Long> brigadeIds=new ArrayList<Long>();
		if(params.getFireBrigadeIds()!=null&&params.getFireBrigadeIds().size()>0){
			brigadeIds.addAll(params.getFireBrigadeIds());
			brigadeIds=getParentId(brigadeIds,brigadeIds);
		}
		Map<String,Object> conditionMap=new HashMap<String,Object>();
		conditionMap.put("pid", -1L);
		conditionMap.put("fileBrigadeIds", brigadeIds);
		//查询消防队数据
		List<FireBrigadeVo> fireBrigadeVoList=fireBrigadeMapper.getAllFileBrigade(conditionMap);
		/*List<FireBrigadeVo> fireBrigadeVoList=fireBrigadeMapper.getFileBrigade(conditionMap);
		if(fireBrigadeVoList!=null&&fireBrigadeVoList.size()>0){
			for(FireBrigadeVo vo:fireBrigadeVoList){
				getChildren(vo,brigadeIds);
			}
		}*/
		
		/*List<ExtinguisherUseRecordColumns> fileColumns=new ArrayList<ExtinguisherUseRecordColumns>();
		if(fireBrigadeVoList!=null&&fireBrigadeVoList.size()>0){
			for(FireBrigadeVo vo:fireBrigadeVoList){
				ExtinguisherUseRecordColumns column=new ExtinguisherUseRecordColumns();
				column.setTitle(vo.getName());
				if(vo.getChildren()!=null&&vo.getChildren().size()>0){
					width=getFileColumns(vo.getChildren(),column,width);
				}else{
					column.setDataIndex(vo.getId()+"");
					column.setKey(vo.getId()+"");
					column.setWidth(100);
					width+=100;
				}
				fileColumns.add(column);
			}
		}
		columns.addAll(fileColumns);*/
		
		List<ExtinguisherUseRecordCount> recordCountList=extinguisherUseRecordMapper.getExtinguisherUseRecordCount(params);
		//获取灭火剂类型
		Map<String,Object> conditionMap1=new HashMap<String,Object>();
		conditionMap1.put("type", 5);
		conditionMap1.put("dictionaryIds", params.getDictionaryIds());
		List<Dictionary> dictionaryList=dictionaryMapper.qryDictionary(conditionMap1);
		for(Dictionary dic:dictionaryList){
			ExtinguisherUseRecordColumns dics=new ExtinguisherUseRecordColumns();
			dics.setTitle(dic.getName());
			dics.setDataIndex(dic.getId_());
			dics.setKey(dic.getId_());
			dics.setWidth("75px");
			width+=75;
			columns.add(dics);
		}
		
		ExtinguisherUseRecordColumns types1=new ExtinguisherUseRecordColumns();
		types1.setTitle("合计");
		types1.setDataIndex("countNums");
		types1.setKey("countNums");
		types1.setWidth("50px");
		width+=50;
		columns.add(types1);
		
		//获取所有不存在子消防队的消防队数据
		List<FireBrigade> fireBrigades=extinguisherUseRecordMapper.getFireBrigade(params);
		
		List<Map<String,Object>> resultDataList=new ArrayList<Map<String,Object>>();
		/*if(dictionaryList!=null&&dictionaryList.size()>0){
			int flag=1;
			for(Dictionary dictionary:dictionaryList){
				Map<String,Object> map=new HashMap<String,Object>();
				BigDecimal usedAmount=BigDecimal.ZERO;
				map.put("id", dictionary.getId());
				map.put("xh", flag);
				map.put("foamTypeName", dictionary.getName());
				if(fireBrigades!=null&&fireBrigades.size()>0){
					for(FireBrigade fileBrigade:fireBrigades){
						for(ExtinguisherUseRecordCount recordCount:recordCountList){
							if(recordCount.getFoamTypeId().equals(dictionary.getId())
									&&fileBrigade.getId().equals(recordCount.getFireBrigadeId())){
								map.put(recordCount.getFireBrigadeId()+"", recordCount.getUsedAmount());
								usedAmount=usedAmount.add(recordCount.getUsedAmount());
							}
						}
					}
				}
				map.put("countNums", usedAmount);
				resultDataList.add(map);
				flag++;
			}
		}*/
		List<Map<String,Object>> pidDataList=new ArrayList<Map<String,Object>>();
		if(fireBrigadeVoList!=null&&fireBrigadeVoList.size()>0){
			int flag=1;
			for(FireBrigadeVo fileBrigade:fireBrigadeVoList){
				Map<String,Object> map=new HashMap<String,Object>();
				BigDecimal usedAmount=BigDecimal.ZERO;
				map.put("id", fileBrigade.getId());
				map.put("xh", flag);
				map.put("teamName", fileBrigade.getName());
				for(Dictionary dictionary:dictionaryList){
					Map<String,Object> charData=new HashMap<String,Object>();
					charData.put("label", fileBrigade.getName());
					charData.put("name", dictionary.getName());
					BigDecimal amount=BigDecimal.ZERO;
					for(ExtinguisherUseRecordCount recordCount:recordCountList){
						if(recordCount.getFoamTypeId().equals(dictionary.getId())
								&&fileBrigade.getId().equals(recordCount.getFireBrigadeId())){
							map.put(dictionary.getId_()+"", recordCount.getUsedAmount());
							amount=recordCount.getUsedAmount();
							usedAmount=usedAmount.add(recordCount.getUsedAmount());
						}
					}
					charData.put("value", amount);
					pidDataList.add(charData);
				}
				map.put("countNums", usedAmount);
				resultDataList.add(map);
				flag++;
			}
			
		}
		
		/*List<String> typeNames=new ArrayList<String>();
		List<Map<String,Object>> pidDataList=new ArrayList<Map<String,Object>>();
		if(dictionaryList!=null&&dictionaryList.size()>0){
			for(Dictionary dictionary:dictionaryList){
				typeNames.add(dictionary.getName());
				
				for(ExtinguisherUseRecordCount recordCount:recordCountList){
					if(recordCount.getFoamTypeId().equals(dictionary.getId())){
						Map<String,Object> map=new HashMap<String,Object>();
						map.put("label", dictionary.getName());
						map.put("value", recordCount.getUsedAmount());
						map.put("name", recordCount.getName());
						pidDataList.add(map);
					}
				}
				
			}
		}*/
		result.put("columns", columns);
		result.put("dataList", resultDataList);
		result.put("width", width);
		result.put("pidDataList", pidDataList);
		return  result;
		
	}
	
	public void getChildren(FireBrigadeVo vo,List<Long> fireBrigadeIds){
		Map<String,Object> conditionMap=new HashMap<String,Object>();
		conditionMap.put("pid", vo.getId());
		conditionMap.put("fileBrigadeIds", fireBrigadeIds);
		List<FireBrigadeVo> fireBrigadeVoList=fireBrigadeMapper.getFileBrigade(conditionMap);
		if(fireBrigadeVoList!=null&&fireBrigadeVoList.size()>0){
			for(FireBrigadeVo child:fireBrigadeVoList){
				getChildren(child,fireBrigadeIds);
			}
			vo.setChildren(fireBrigadeVoList);
		}
	}
	
	public int getFileColumns(List<FireBrigadeVo> fireBrigadeVoList,
			ExtinguisherUseRecordColumns column,int width){
		List<ExtinguisherUseRecordColumns> fileColumns=new ArrayList<ExtinguisherUseRecordColumns>();
		if(fireBrigadeVoList!=null&&fireBrigadeVoList.size()>0){
			for(FireBrigadeVo vo:fireBrigadeVoList){
				ExtinguisherUseRecordColumns chdColumn=new ExtinguisherUseRecordColumns();
				chdColumn.setTitle(vo.getName());
				if(vo.getChildren()!=null&&vo.getChildren().size()>0){
					width=getFileColumns(vo.getChildren(),column,width);
				}else{
					chdColumn.setDataIndex(vo.getId()+"");
					chdColumn.setKey(vo.getId()+"");
					chdColumn.setWidth("100");
					width+=100;
				}
				fileColumns.add(chdColumn);
			}
			column.setChildren(fileColumns);
		}
		return width;
	}
	
	public List<Long> getParentId(List<Long> brigadeIds,List<Long> childrenIds){
		Map<String,Object> conditionMap=new HashMap<String,Object>();
		conditionMap.put("fileBrigadeIds", childrenIds);
		List<Long> parentIds=fireBrigadeMapper.getParentIds(conditionMap);
		if(parentIds!=null&&parentIds.size()>0){
			brigadeIds.addAll(parentIds);
			List<Long> childrens=new ArrayList<Long>();
			for(Long pid:parentIds){
				if(!pid.equals(-1L)){
					childrens.add(pid);
				}
			}
			if(childrens!=null&&childrens.size()>0){
				return getParentId(brigadeIds,childrens);
			}
		}
		return brigadeIds;
	}

	public List<FireBrigadeVo> qryFireBrigade() {
		Map<String,Object> conditionMap=new HashMap<String,Object>();
		conditionMap.put("pid", -1L);
		List<FireBrigadeVo> fireBrigadeVoList=fireBrigadeMapper.getFileBrigade(conditionMap);
		if(fireBrigadeVoList!=null&&fireBrigadeVoList.size()>0){
			for(FireBrigadeVo vo:fireBrigadeVoList){
				getChildren(vo,null);
			}
		}
		return fireBrigadeVoList;
	}

	public void insertExtinguisherUseRecord(ExtinguisherUseRecord entity) {
		extinguisherUseRecordMapper.insert(entity);
		
	}
	
}
