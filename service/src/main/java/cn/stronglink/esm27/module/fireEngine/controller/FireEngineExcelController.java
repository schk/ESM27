package cn.stronglink.esm27.module.fireEngine.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.core.util.BmUtils;
import cn.stronglink.core.util.ImportValid;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Dictionary;
import cn.stronglink.esm27.entity.FireBrigade;
import cn.stronglink.esm27.entity.FireEngine;
import cn.stronglink.esm27.entity.FireEngineTemp;
import cn.stronglink.esm27.module.dictionary.service.DictionaryService;
import cn.stronglink.esm27.module.emergencyResources.fireBrigade.service.FireBrigadeService;
import cn.stronglink.esm27.module.fireEngine.service.FireEngineService;

@Controller
@RequestMapping(value = "fireEngine/excel")
public class FireEngineExcelController extends AbstractController {

	@Autowired
	private FireEngineService entityService;
	@Autowired
	private FireBrigadeService fireBrigadeService;
	@Autowired
	private DictionaryService dictionaryService;


	/**
	 * 导入EXCEL文件
	 * 
	 * @param file
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "importExcel")
	public ResponseEntity<ModelMap> importExcel(@RequestParam(value = "file", required = true) MultipartFile file,
			HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) throws Exception {
		List<String> dataList = new ArrayList<String>();
		System.out.println(WebUtil.getCurrentUser());
		// 导入Excel操作
		// 导入Excel文件不为空时，将文件写入流，然后转化为工作薄
		if (file != null) {
			try {
				Workbook workBook = null;
				if(!ImportValid.validXls(file)){
					return setModelMap(modelMap, HttpCode.CONFLICT, "请选择excel格式文件！");
				}
				int header = 1;
				workBook = WorkbookFactory.create(file.getInputStream());
				if (workBook.getNumberOfSheets() > 0) {
					// 获取第一个单元薄
					Sheet sheet = workBook.getSheetAt(0);
					// 获取单元薄中表头信息,并将表头中每个单元格中信息存入数组headNames中
					String allTitle = "*车辆名称$*所属消防队$*车辆型号$*消防车类别$*车辆编号$*车牌号$车身长度$投用日期-(yyyy/MM/dd)$*水量(KG)$载泡沫类型$载泡沫量(KG)$载干粉类型$载干粉量(KG)$*水泵功率(KG/H)$*炮射程(L/S)$举升高度(米)$泵浦流量(L/S)$底盘型号$生产厂家$*执勤状态$车载炮量(L/S)";
					Row rowHead = sheet.getRow(header);
					if (rowHead != null) {
						String[] headNames = new String[rowHead.getLastCellNum()];
						// 获取该行中总共有多少列数据row.getLastCellNum()
						for (int j = 0; j < rowHead.getLastCellNum(); j++) {
							Cell cell = rowHead.getCell(j);
							// 获取表头各个字段名称
							String returnStr = BmUtils.getCellValue(cell);
							headNames[j] = returnStr;
						}
						if (headNames != null && headNames.length > 0) {
							for (String name : headNames) {
								if (allTitle.indexOf(name) == -1) {
									throw new BusinessException("属性：" + name + "在系统中不存在,请使用模板文件！");
								}
							}
						}
						Date date = new Date();
						// 遍历单元簿中每一行，获取每一列字段，并根据表头对应赋值
						int flag = 3;
						List<FireEngine> entityList = new ArrayList<FireEngine>();
						List<FireEngineTemp> tempList = new ArrayList<FireEngineTemp>();
						List<Dictionary> typeList = dictionaryService.qryByType(1);
						List<Dictionary> miehuoJList = dictionaryService.qryByType(5);
						Map<String,Integer> codeMap = new HashMap<>();
						Map<String,Integer> plateNumberMap = new HashMap<>();
						// 关系是否存在
						Boolean typeNoExist = false;
						int dataFormat = 0;
						modelMap.put("errorCode", 0);

						// 插入execl中得数据到临时表
						long timestamp = System.currentTimeMillis();
						for (int i = header; i < sheet.getPhysicalNumberOfRows(); i++) {
							Row row = sheet.getRow(i + 1);
							Boolean isRow = ImportValid.isRowEmpty(row);
							FireEngine entity = null;
							FireEngineTemp temp = null;
							if (!isRow) {
								String rowError = "错误：第" + flag + "行   ";
								String data = "";
								entity = new FireEngine();
								entity.setId(IdWorker.getId());
								entity.setStatus(2);
								entity.setCreateBy(WebUtil.getCurrentUser());
								entity.setCreateTime(date);
								entity.setUpdateTime(date);

								temp = new FireEngineTemp();
								temp.setId(IdWorker.getId());
								temp.setCreateBy(WebUtil.getCurrentUser());
								temp.setTimestamp(timestamp);
								temp.setCreateTime(date);

								// 获取该行中总共有多少列数据row.getLastCellNum()
								for (int j = 0; j < rowHead.getLastCellNum(); j++) {
									if(j>headNames.length-1) {
										continue ;
									}
									Cell cell = row.getCell(j);
									if (StringUtils.hasText(headNames[j])) {
										// 获取单元格内容，并根据单元格类型，进行转化
										String returnStr = BmUtils.getCellValue(cell);
										// 根据给定的Excel格式，进行导入，一下是对应列对应字段
										if (headNames[j].equals("*车辆名称")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setName(returnStr);
												temp.setName(returnStr);
											} else {
												dataFormat++;
												data += "【车辆名称】不能为空;";
											}
										} else if (headNames[j].equals("*所属消防队")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												List<FireBrigade> fireBrigadeList = fireBrigadeService.qryList();
												if (fireBrigadeList != null && fireBrigadeList.size() > 0) {
													boolean isExit = false;
													for (FireBrigade fireBrigade : fireBrigadeList) {
														if ((fireBrigade.getName().replace(" ", "")).equals(returnStr.replace(" ", ""))) {
															isExit = true;
															entity.setFireBrigadeId(fireBrigade.getId());
															temp.setFireBrigadeId(fireBrigade.getId());
															break;
														}
													}
													if (!isExit) {
														dataFormat++;
														data += "【消防队："+returnStr+"】不存在;";
													}
												}else{
													dataFormat++;
													data += "【消防队："+returnStr+"】不存在;";
												}
											} else {
												dataFormat++;
												data += "【所属消防队】不能为空;";
											}
										} else if (headNames[j].equals("*消防车类别")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												temp.setDicId(returnStr);
												if (typeList != null && typeList.size() > 0) {
													boolean isExit = false;
													for (Dictionary dic : typeList) {
														if ((dic.getName().replace(" ", "")).equals(returnStr.replace(" ", ""))) {
															isExit = true;
															entity.setDicId(dic.getId());
															break;
														}
													}
													if (!isExit) {
														typeNoExist = true;
														data += "【消防车类别："+returnStr+"】不存在;";
													}
												}else{
													typeNoExist = true;
													data += "【消防车类别："+returnStr+"】不存在;";
												}
											} else {
												dataFormat++;
												data += "【消防车类别】不能为空;";
											}
										}else if (headNames[j].equals("*车辆型号")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setModel(returnStr);
												temp.setModel(returnStr);
											} else {
												dataFormat++;
												data += "【车辆型号】不能为空;";
											}
										} else if (headNames[j].equals("*车辆编号")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												if(codeMap.get(returnStr)==null) {
													entity.setCode(returnStr);
													temp.setCode(returnStr);
													codeMap.put(returnStr, flag);
												}else {
													dataFormat++;
													data += "与第"+codeMap.get(returnStr)+"行【车辆编号："+returnStr+"】重复!;";
													break;
												}
												
												
											} else {
												dataFormat++;
												data += "【车辆编号】不能为空;";
											}
										} else if (headNames[j].equals("*车牌号")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												if(plateNumberMap.get(returnStr)==null) {
													entity.setPlateNumber(returnStr);
													temp.setPlateNumber(returnStr);
													plateNumberMap.put(returnStr, flag);
												}else {
													dataFormat++;
													data += "与第"+plateNumberMap.get(returnStr)+"行【车牌号："+returnStr+"】重复!;";
													break;
												}
											} else {
												dataFormat++;
												data += "【车牌号】不能为空;";
											}
										} else if (headNames[j].equals("车身长度")) {
											entity.setCarSize(returnStr);
											temp.setCarSize(returnStr);
										} else if (headNames[j].equals("投用日期-(yyyy/MM/dd)")) {
											if(ImportValid.isNotEmpty(returnStr)){
												Boolean isTrue = ImportValid.isValidDate("yyyy/MM/dd",returnStr);
												if (isTrue) {
													SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
													entity.setInvestmentDate(sdf.parse(returnStr));
													temp.setInvestmentDate(sdf.parse(returnStr));
												}else {
													dataFormat++;
													data+="【投用日期】格式不对;";
												}
											}
										} else if (headNames[j].equals("*水量(KG)")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setWater(Double.valueOf(returnStr));
												temp.setWater(Double.valueOf(returnStr));
											} else {
												dataFormat++;
												data += "【水量】不能为空;";
											}
										} else if (headNames[j].equals("载干粉类型")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												if (miehuoJList != null && miehuoJList.size() > 0) {
													boolean isExit = false;
													for (Dictionary dic : miehuoJList) {
														if ((dic.getName().replace(" ", "")).equals(returnStr.replace(" ", ""))) {
															isExit = true;
															entity.setFoamTypeId(dic.getId());
															temp.setFoamTypeId(dic.getId());
															break;
														}
													}
													if (!isExit) {
														dataFormat++;
														data += "【该粉类型："+returnStr+"】 不存在;";
													}
												}else{
													dataFormat++;
													data += "【该干粉类型："+returnStr+"】不存在;";
												}
											} 
										} else if (headNames[j].equals("载干粉量(KG)")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setCapacity(Double.valueOf(returnStr));
												temp.setCapacity(Double.valueOf(returnStr));
											}
										} else if (headNames[j].equals("载泡沫类型")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												if (miehuoJList != null && miehuoJList.size() > 0) {
													boolean isExit = false;
													for (Dictionary dic : miehuoJList) {
														if ((dic.getName().replace(" ", "")).equals(returnStr.replace(" ", ""))) {
															isExit = true;
															entity.setFoamTypeIdO(dic.getId());
															temp.setFoamTypeIdO(dic.getId());
															break;
														}
													}
													if (!isExit) {
														dataFormat++;
														data += "【该消防队下泡沫类型："+returnStr+"】不存在;";
													}
												}else{
													dataFormat++;
													data += "【该消防队下泡沫类型："+returnStr+"】不存在;";
												}
											} 
										} else if (headNames[j].equals("载泡沫量(KG)")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setPowderQuantity(Double.valueOf(returnStr));
												temp.setPowderQuantity(Double.valueOf(returnStr));
											} 
										} else if (headNames[j].equals("*水泵功率(KG/H)")) {
											if (ImportValid.isNotEmpty(returnStr)) {
												entity.setSpraySpeed(Double.valueOf(returnStr));
												temp.setSpraySpeed(Double.valueOf(returnStr));
											} else {
												dataFormat++;
												data += "【水泵功率】不能为空;";
											}
										} else if (headNames[j].equals("*炮射程(L/S)")) {
											if (returnStr != null && !"".equals(returnStr)) {
												entity.setGunRange(Double.valueOf(returnStr));
												temp.setGunRange(Double.valueOf(returnStr));
											} else {
												dataFormat++;
												data += "【炮射程】不能为空;";
											}
										} else if (headNames[j].equals("举升高度(米)")) {
											if (returnStr != null && !"".equals(returnStr)) {
												entity.setLiftingHeight(Double.valueOf(returnStr));
												temp.setLiftingHeight(Double.valueOf(returnStr));
											}
										} else if (headNames[j].equals("泵浦流量(L/S)")) {
											if (returnStr != null && !"".equals(returnStr)) {
												entity.setPumpFlow(Double.valueOf(returnStr));
												temp.setPumpFlow(Double.valueOf(returnStr));
											}
										} else if (headNames[j].equals("底盘型号")) {
											entity.setChassisModel(returnStr);
											temp.setChassisModel(returnStr);
										} else if (headNames[j].equals("生产厂家")) {
											entity.setManufacturer(returnStr);
											temp.setManufacturer(returnStr);
										} else if (headNames[j].equals("*执勤状态")) {
											if ("执勤".equals(returnStr)) {
												entity.setStateOfDuty(1);
												temp.setStateOfDuty(1);
											}else if("备勤".equals(returnStr)){
												entity.setStateOfDuty(2);
												temp.setStateOfDuty(2);
											}else{
												dataFormat++;
												data += "【执勤状态】只能是'执勤'或者'备勤';";
											}
										} else if (headNames[j].equals("车载炮量(L/S)")) {
											if (returnStr != null && !"".equals(returnStr)) {
												entity.setVehicularArtillery(Double.valueOf(returnStr));
												temp.setVehicularArtillery(Double.valueOf(returnStr));
											}
										}
									}
								}
								flag++;
								entityList.add(entity);
								tempList.add(temp);
								if (data != null && !"".equals(data)) {
									dataList.add(rowError+data);
								}
							}
						}
						if (dataList != null && dataList.size() > 0) {
							if (dataFormat > 0) {
								modelMap.put("errorCode", 2);
								return setSuccessModelMap(modelMap, dataList);
							}
							if (typeNoExist) {
								modelMap.put("errorCode", 1);
								modelMap.put("times", timestamp);
								// 存临时表数据
								if (tempList != null && tempList.size() > 0) {
									entityService.saveExcelTempData(tempList); // 数据存储
									return setSuccessModelMap(modelMap, dataList);
								} else {
									throw new BusinessException("文件数据为空!");
								}
							}
						} else {
							// 存数据
							if (entityList != null && entityList.size() > 0) {
								// 数据存储
								entityService.saveExcelData(entityList);
								return setSuccessModelMap(modelMap);
							} else {
								throw new BusinessException("文件数据为空!");
							}
						}
					}
				}
			} catch (IOException e) {
				throw new BusinessException("未知异常!");
			}
		}
		throw new BusinessException("无文件!");
	}
	

	/**
	 * 继续导入excel数据
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param timestamp
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "importExcelConfirm")
	public ResponseEntity<ModelMap> importExcelConfirm(HttpServletRequest request,
			HttpServletResponse response,ModelMap modelMap, @RequestBody Long timestamp) throws Exception{
		Date date = new Date();
		// 遍历单元簿中每一行，获取每一列字段，并根据表头对应赋值
		List<FireEngine> entityList = new ArrayList<FireEngine>();
		List<FireEngineTemp> tempList = entityService.qryExcelTemp(timestamp);
		List<Dictionary> typeList = dictionaryService.qryByType(1);
		
		for (int j = 0; j < tempList.size(); j++) {
			FireEngineTemp temp = tempList.get(j);
			FireEngine entity = null;
			if (temp!=null) {
				entity = new FireEngine();
				boolean isExit=false;
				if(typeList != null && typeList.size() > 0){
					for(Dictionary dic : typeList){
						if(dic.getName().equals(temp.getDicId())){
							isExit=true;
							entity.setDicId(dic.getId());
							break;
						}
					}
				}
				if(!isExit){
					Dictionary dic =new Dictionary();
					dic.setName(temp.getDicId());
					dic.setRemark(temp.getDicId());
					dic.setType(1);
					dic.setPid(-1L);
					dictionaryService.insert(dic);
					entity.setDicId(dic.getId());
					typeList.add(dic);
				}
				entity.setId(IdWorker.getId());
				entity.setName(temp.getName());
				entity.setFireBrigadeId(temp.getFireBrigadeId());
				entity.setModel(temp.getModel());
				entity.setCode(temp.getCode());
				entity.setPlateNumber(temp.getPlateNumber());
				entity.setStatus(2);
				entity.setFoamTypeId(temp.getFoamTypeId());
				entity.setCapacity(temp.getCapacity());
				entity.setFoamTypeIdO(temp.getFoamTypeIdO());
				entity.setPowderQuantity(temp.getPowderQuantity());
				entity.setSpraySpeed(temp.getSpraySpeed());
				entity.setWater(temp.getWater());
				entity.setFullWeight(temp.getFullWeight());
				entity.setCarSize(temp.getCarSize());
				entity.setGunRange(temp.getGunRange());
				entity.setInvestmentDate(temp.getInvestmentDate());
				entity.setLiftingHeight(temp.getLiftingHeight());
				entity.setPumpFlow(temp.getPumpFlow());
				entity.setStateOfDuty(temp.getStateOfDuty());
				entity.setManufacturer(temp.getManufacturer());
				entity.setChassisModel(temp.getChassisModel());
				entity.setCreateBy(WebUtil.getCurrentUser());
				entity.setCreateTime(date);
				entity.setUpdateTime(date);
				entityList.add(entity);
			}
		}
		if(entityList != null && entityList.size() > 0) {
			// 数据存储
			entityService.saveExcelData(entityList);
			// 删除临时表数据
			entityService.delExcelTemp(timestamp);
			return setSuccessModelMap(modelMap);
		} else {
			throw new BusinessException("文件数据为空!");
		}
		
	}

	/**
	 * 删除临时表数据
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param timestamp
	 * @return
	 */
	@RequestMapping(value = "delExcelTemp")
	public ResponseEntity<ModelMap> delExcelTemp(HttpServletRequest request,
			HttpServletResponse response,ModelMap modelMap, @RequestBody Long timestamp){
		//删除临时表数据
		entityService.delExcelTemp(timestamp);
		return setSuccessModelMap(modelMap);
		
	}

}
