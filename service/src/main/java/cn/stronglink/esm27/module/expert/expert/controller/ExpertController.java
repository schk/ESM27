package cn.stronglink.esm27.module.expert.expert.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.log.annotation.OperateLog;
import cn.stronglink.core.log.annotation.OperateLog.OpType;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Expert;
import cn.stronglink.esm27.entity.ExpertTemp;
import cn.stronglink.esm27.entity.ExpertType;
import cn.stronglink.esm27.module.expert.expert.service.ExpertService;
import cn.stronglink.esm27.module.expert.expert.vo.ExpertVo;
import cn.stronglink.esm27.module.expert.expertType.service.ExpertTypeService;

@Controller
@RequestMapping("expert")
public class ExpertController extends AbstractController {
	
	@Autowired
	private ExpertService expertService;
	@Autowired
	private ExpertTypeService expertTypeService;
	
	/*
	 * 查询所有专家信息
	 */
	@RequestMapping("qryListByParams")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<ExpertVo> page = (Page<ExpertVo>) super.getPage(params);
		Page<ExpertVo> data = expertService.qryListByParams(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	/**
	 * 根据id查询信息
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param id
	 * @return
	 */
	@RequestMapping("qryById")
	public ResponseEntity<ModelMap> selectById(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody Long id){
		ExpertVo vo =  expertService.selectById(id);
		return setSuccessModelMap(modelMap,vo);
	}
	
	/*
	 * 根据id修改信息
	 */
	@RequestMapping(value = "edit")
	@OperateLog(module = "应急专家管理",desc="修改应急专家", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> edit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Expert entity){
		expertService.update(entity);
		return setSuccessModelMap(modelMap, null);
		
	}
	
	/*
	 * 根据id删除信息
	 */
	@RequestMapping(value = "remove")
	@OperateLog(module = "应急专家管理",desc="删除应急专家", type = OpType.UPDATE)
	public ResponseEntity<ModelMap> remove(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		expertService.remove(id);
		return setSuccessModelMap(modelMap, null);
	}
	/**
	 * 新建人员
	 */
	@RequestMapping(value = "create")
	@OperateLog(module = "应急专家管理",desc="添加应急专家", type = OpType.ADD)
	public ResponseEntity<ModelMap> create(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Expert entity) {
		expertService.insert(entity);
		return setSuccessModelMap(modelMap, null);
	}
	
	
	/**
	 * 继续导入应急专家数据
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param timestamp
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "importExpertConfirm")
	public ResponseEntity<ModelMap> importExpertConfirm(HttpServletRequest request,
			HttpServletResponse response,ModelMap modelMap, @RequestBody Long timestamp) throws Exception{
		Date date = new Date();
		// 遍历单元簿中每一行，获取每一列字段，并根据表头对应赋值
		List<Expert> expertList = new ArrayList<Expert>();
		List<ExpertType> typeList = expertTypeService.qryListByParams();
		List<ExpertTemp> expertTempList =expertService.qryExpertTemp(timestamp);
		//插入execl中得数据到临时表
		for (int j = 0; j < expertTempList.size(); j++) {
			ExpertTemp row = expertTempList.get(j);
			Expert expert = null;
			if (row!=null) {
				expert = new Expert();
				if ("男".equals(row.getSex())) {
					expert.setSex(1);
				}else {
					expert.setSex(2);
				}
				if(typeList!=null &&typeList.size()>0){
					boolean isExit=false;
					for(ExpertType type : typeList){
						if(type.getName().equals(row.getType())){
							isExit=true;
							expert.setTypeId(type.getId());
							break;
						}
					}
					if(!isExit){
						//专家类型不存在，添加专家类型
						ExpertType entity =new ExpertType();
						entity.setId(IdWorker.getId());
						entity.setName(row.getType());
						entity.setRemark(row.getType());
						entity.setCreateTime(new Date());
						expertTypeService.insert(entity);
						expert.setTypeId(entity.getId());
						typeList.add(entity);
					}
				}
				expert.setId(IdWorker.getId());
				expert.setName(row.getName());
				expert.setBirth(row.getBirth());
				expert.setMajor(row.getMajor());
				expert.setMajorSubject(row.getMajorSubject());
				expert.setSpeciality(row.getSpeciality());
				expert.setUnit(row.getUnit());
				expert.setPhone(row.getPhone());
				expert.setAddress(row.getAddress());
				expert.setCreateBy(WebUtil.getCurrentUser());
				expert.setCreateTime(date);
				expert.setUpdateTime(date);
				expertList.add(expert);
			}
		}
		if(expertList != null && expertList.size() > 0) {
			//TODO 数据存储
			expertService.exportEXLExpert(expertList);
			//删除临时表数据
			expertService.delExpertTemp(timestamp);
			return setSuccessModelMap(modelMap);
		} else {
			throw new BusinessException("文件数据为空!");
		}
		
	}

	
	/**
	 * 删除应急专家临时表数据
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param timestamp
	 * @return
	 */
	@RequestMapping(value = "delExpertTemp")
	public ResponseEntity<ModelMap> delExpertTemp(HttpServletRequest request,
			HttpServletResponse response,ModelMap modelMap, @RequestBody Long timestamp){
		//删除临时表数据
		expertService.delExpertTemp(timestamp);
		return setSuccessModelMap(modelMap);
		
	}
	
	
}
