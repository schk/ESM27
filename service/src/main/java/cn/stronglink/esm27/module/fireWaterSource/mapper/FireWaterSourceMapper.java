package cn.stronglink.esm27.module.fireWaterSource.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.esm27.entity.FireWaterSource;
import cn.stronglink.esm27.entity.FireWaterSourceTemp;
import cn.stronglink.esm27.module.fireWaterSource.vo.FireWaterSourceVo;

public interface FireWaterSourceMapper extends BaseMapper<FireWaterSource>{
	
	List<FireWaterSourceVo> qryListByParams(Pagination page, Map<String, Object> params);

	Integer findCountByName(@Param("name") String name);

	Integer findCountByNameAndId(@Param("name") String name, @Param("id") Long id);

	FireWaterSourceVo qryById(@Param("id")Long id);

	int batchInsert(@Param("interimList") List<FireWaterSource> interimList);

	int batchInsertTemp(@Param("interimList") List<FireWaterSourceTemp> interimList);

	List<FireWaterSourceTemp> qryExcelTemp(Long timestamp);

	void delExcelTemp(Long timestamp);

	List<FireWaterSourceVo> qryList();

}
