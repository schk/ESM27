package cn.stronglink.esm27.module.fireWaterSource.vo;

import java.util.List;

public class WaterSourceUnitVo {

	/**
	 * 
	 */
	private Long keyUnitId;
	private String keyUnitName;
	private List<FireWaterSourceVo>  fireWaterSourceList;
	private int  fireWaterSourceCount;
	
	public String getKeyUnitName() {
		return keyUnitName;
	}
	public void setKeyUnitName(String keyUnitName) {
		this.keyUnitName = keyUnitName;
	}
	public int getFireWaterSourceCount() {
		return fireWaterSourceCount;
	}
	public void setFireWaterSourceCount(int fireWaterSourceCount) {
		this.fireWaterSourceCount = fireWaterSourceCount;
	}
	public Long getKeyUnitId() {
		return keyUnitId;
	}
	public void setKeyUnitId(Long keyUnitId) {
		this.keyUnitId = keyUnitId;
	}
	public List<FireWaterSourceVo> getFireWaterSourceList() {
		return fireWaterSourceList;
	}
	public void setFireWaterSourceList(List<FireWaterSourceVo> fireWaterSourceList) {
		this.fireWaterSourceList = fireWaterSourceList;
	}

	

}
