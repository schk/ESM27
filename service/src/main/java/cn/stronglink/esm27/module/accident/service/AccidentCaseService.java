package cn.stronglink.esm27.module.accident.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.AntdFile;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.AccidentCase;
import cn.stronglink.esm27.entity.AccidentCaseFile;
import cn.stronglink.esm27.module.accident.mapper.AccidentCaseMapper;
import cn.stronglink.esm27.module.accident.vo.FireCaseVo;

@Service
@Transactional(rollbackFor=Exception.class)
public class AccidentCaseService {
	
	@Autowired
	private AccidentCaseMapper accidentCaseMapper;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<AccidentCase> qryListByParams(Page<AccidentCase> page, Map<String, Object> params) {
		List<AccidentCase> list=accidentCaseMapper.qryListByParams(page,params);
		page.setRecords(list);	
		return page;
	}
	
	public void insert(AccidentCase entity) {
		Date date=new Date();
		entity.setId(IdWorker.getId());
		if(entity.getFileList()!=null&&entity.getFileList().size()>0) {
			List<AccidentCaseFile> fileList = new ArrayList<AccidentCaseFile>();
			for (AntdFile doc:entity.getFileList()) {
				AccidentCaseFile file = new AccidentCaseFile();
				file.setId(IdWorker.getId());
				file.setFileName(doc.getName());
				file.setPath(doc.getUrl());
				file.setSwfPath(doc.getSwfUrl());
				file.setAccidentCaseId(entity.getId());
				file.setCreateTime(date);
				file.setCreateBy(WebUtil.getCurrentUser());
				file.setUpdateTime(date);
	            fileList.add(file);
	        }
			Map<String, Object> map=new HashMap<String, Object>();
			map.put("files", fileList);
			accidentCaseMapper.inserAccidentCaseFiles(map);
		}
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(date);
		entity.setUpdateTime(entity.getCreateTime());
		accidentCaseMapper.insert(entity);
	}
	/*
	 * 修改信息
	 */
	public void update(AccidentCase entity) {
		accidentCaseMapper.deleteFileByCaseId(entity.getId());
		Date date=new Date();
		if(entity.getFileList()!=null&&entity.getFileList().size()>0) {
			List<AccidentCaseFile> fileList = new ArrayList<AccidentCaseFile>();
			for (AntdFile doc:entity.getFileList()) {
				AccidentCaseFile file = new AccidentCaseFile();
				file.setId(IdWorker.getId());
				file.setFileName(doc.getName());
				file.setPath(doc.getUrl());
				file.setSwfPath(doc.getSwfUrl()); 
				file.setAccidentCaseId(entity.getId());
				file.setCreateTime(date);
				file.setCreateBy(WebUtil.getCurrentUser());
				file.setUpdateTime(date);
	            fileList.add(file);
	        }
			Map<String, Object> map=new HashMap<String, Object>();
			map.put("files", fileList);
			accidentCaseMapper.inserAccidentCaseFiles(map);
		}
		entity.setUpdateBy(WebUtil.getCurrentUser());
		entity.setUpdateTime(new Date());
		if(accidentCaseMapper.updateById(entity)==0){
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
	}
	/*
	 * 删除对象   
	 */
	public void remove(Long id) {
		accidentCaseMapper.deleteById(id);
		accidentCaseMapper.deleteFileByCaseId(id);
	}

	public AccidentCase selectById(Long id) {
		AccidentCase obj = accidentCaseMapper.selectById(id);
		if(obj!=null) {
			List<AntdFile> fileList =accidentCaseMapper.selectFileById(id);
			obj.setFileList(fileList);
		}
		return obj;
	}

	public Page<FireCaseVo> qryAccidentCaseList(Page<FireCaseVo> page, Map<String, Object> params) {
		List<FireCaseVo> list=accidentCaseMapper.qryFireCaseListByParams(page,params);
		if(list.size()>0){
			for(AccidentCase c:list){
				List<AntdFile> fileList =accidentCaseMapper.selectFileById(c.getId());
				c.setFileList(fileList);
			}
		}
		page.setRecords(list);	
		return page;
	}

	public Page<FireCaseVo> qryFireCaseListByParams(Page<FireCaseVo> page, Map<String, Object> params) {
		List<FireCaseVo> list=accidentCaseMapper.qryFireCaseListByParams(page,params);
		page.setRecords(list);	
		return page;
	}
	
	
}
