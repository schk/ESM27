package cn.stronglink.esm27.module.extinguisherUseRecord.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.ExtinguisherUseRecord;
import cn.stronglink.esm27.entity.FireBrigade;
import cn.stronglink.esm27.module.extinguisherUseRecord.param.SearchParam;
import cn.stronglink.esm27.module.extinguisherUseRecord.vo.ExtinguisherUseRecordCount;

public interface ExtinguisherUseRecordMapper extends BaseMapper<ExtinguisherUseRecord>{

	List<ExtinguisherUseRecord> qryListByParams(@Param("type") Integer type);

	List<ExtinguisherUseRecordCount> getExtinguisherUseRecordCount(SearchParam params);

	List<FireBrigade> getFireBrigade(SearchParam params);

	Double getExtinguisherCountByRoomId(String roomId);

	Double getExtinguisherByBrigadeId(@Param("fireBrigadeId") Long id, @Param("roomId") String roomId);
	 
	
}
