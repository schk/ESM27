package cn.stronglink.esm27.thirdsdk.explosion;

import com.sun.jna.Native;
import com.sun.jna.win32.StdCallLibrary;

import cn.stronglink.core.util.PathUtil;

public interface GasMathModel extends StdCallLibrary {
	
	GasMathModel INSTANCE = (GasMathModel) Native.loadLibrary(PathUtil.getClassPath() + "sdk\\explosion\\MathModel.dll",
			GasMathModel.class); 
	
		//初始化
		int Init_API();
	
	// 传参数
		// as: 大气稳定度，整型，取值 1,2,3,4,5,6 与大气稳定度取值表的对应关系 1=A, 2=B, 3=C, 4=D, 5=E, 6=F
		// ws: 环境风速，一般取地面 10m 高处的平均风速，单位m/s
		// wd: 风向，单位度
		// gd: 污染气体在常温下的浓度，单位 kg/m3
		// h: 泄漏点离地面的高度
		// dq: 泄漏源强度，kg/s
		// 返回值: true成功，false失败
	    boolean SetGaussionModelParameters_API(String userId, int as, double ws, double wd, double gd, double h, double dq);

		// 设置积分时间间隔，也可以不设置此参数，程序内默认是 1 秒，间隔越短计算次数越多耗时越长，间隔越长计算次数越少耗时越少。
		// dt: 积分时间间隔，单位秒s
		// 返回值: true成功，false失败
		boolean SetDeltaT_API(String userId, double dt);

		// 开始模拟，计算 fCurrentTime 时间点的范围轮廓线
		// fCurrentTime: 扩散时间
		// 返回值 true 计算成功，false 计算失败
		boolean Simulate_API(String userId, double fCurrentTime);

		// 获取等值线上的最远距离
		double GetDistance_API(String userId);
		
		// 设置浓度阈值，计算此浓度范围轮廓线，单位kg/m3
		// t: 等值线浓度
		void  SetThreshold_API(String userId, double t);

		// 获取轮廓线顶点坐标数组的长度
		Integer GetVerticesLength_API(String userId);

		// 获取轮廓线顶点坐标数组[x1,y1,z1,x2,y2,z2,......]
		void GetVertices_API(String userId, double va[]);
	
		
		// 计算泄漏源的位置和泄漏率
		// ws: 风速，单位m/s
		// wd: 风向，单位度
		// as: 大气稳定度，取值 1,2,3,4,5,6 与大气稳定度取值表的对应关系 1=A, 2=B, 3=C, 4=D, 5=E, 6=F
		// data[]: 四组传感器数据，传值(q1,x1,y1,h1,......q2,x2,y2,h2) q浓度，xy传感器坐标，h传感器距离地面高度
		// 返回值: true计算成功，false失败
		boolean LeakSource_API(String userId, double ws, double wd, int as, String data);

		// 获取泄漏率
		// 返回值: 泄漏率，单位公斤每秒(Kg/m)
		double  GetLsQ_API(String userId);

		// 获取泄漏源坐标 x
		// 返回值: x 坐标，单位米(m)
		double  GetLsX_API(String userId);

		// 获取泄漏源坐标 y
		// 返回值: y 坐标，单位米(m)
		 double  GetLsY_API(String userId);

		// 获取泄漏源高度 h
		// 返回值: 高度，单位米(m)
		 double  GetLsH_API(String userId);
		 
		 /*****************************************颗粒物沉降模型***********************************************/

		// 传参数
		// as: 大气稳定度，整型，取值 1,2,3,4,5,6 与大气稳定度取值表的对应关系 1=A, 2=B, 3=C, 4=D, 5=E, 6=F
		// ws: 环境风速，一般取地面 10m 高处的平均风速，单位m/s
		// wd: 风向，单位度
		// h: 烟雾泄漏点离地面的高度
		// dq: 颗粒物源强度，kg/s
		// 返回值: true成功，false失败
		boolean SetPsParameters_API(String userId, int as, double ws, double wd, double h, double dq);

		// 设置积分时间间隔，也可以不设置此参数，程序内默认是 1 秒，间隔越短计算次数越多耗时越长，间隔越长计算次数越少耗时越少。
		// dt: 积分时间间隔，单位秒s
		// 返回值: true成功，false失败
		boolean SetPsDeltaT_API(String userId);

		// 开始模拟，计算 fCurrentTime 时间点的范围轮廓线
		// fCurrentTime: 扩散时间
		// 返回值 true 计算成功，false 计算失败
		boolean SimulatePs_API(String userId, double fCurrentTime);

		// 设置沉降质量阈值，计算此沉降质量范围轮廓线，单位kg/m3
		// t: 等值线
		double SetPsThreshold_API(String userId, double t);

		// 获取轮廓线顶点坐标数组的长度
		int GetPsVerticesLength_API(String userId);

		// 获取轮廓线顶点坐标数组[x1,y1,x2,y2,......]
		void GetPsVertices_API(String userId, double va[]);


		 
		 /***************************************爆炸模型*************************************************/

//		// 计算爆炸伤害
//		// type: 爆炸类型(1-3是物理爆炸, 4是化学爆炸), 1 压缩气体容器爆炸, 2 高压液体容器爆炸, 3 过热液体容器爆炸, 4 蒸气云爆炸
//		// name: 爆炸物名称
////		       1 压缩气体容器爆炸:空气,氮,氧,氢,甲烷,乙烷,乙烯,丙烷,一氧化碳,二氧化碳,一氧化氮,二氧化氮,氨气,氯气,过热蒸气,干饱和蒸气,氢氯酸
////		       2 高压液体容器爆炸:无
////		       3 过热液体容器爆炸:高温饱和水
////		       4 蒸气云爆炸:氢气,氨气,苯,一氧化碳,硫化氢1(生成SO2),硫化氢2(生成SO3),甲烷,乙烷,乙烯,乙炔,丙烷,丙烯,正丁烷,异丁烷,丁烯
//		// pressure: 爆炸物的压力, 单位兆帕(Mpa)
//		// volume: 爆炸物体积, 单位立方米(m³)
//		// r: 距离爆炸点的距离
//		// 返回值: 伤害值(查表获得伤害描述)
		double  CalcExplosionHurt_API(String userId, int type, int name, double pressure, double volume, double r);
//
//		// 计算爆炸伤害半径
//		// type: 爆炸类型(1-3是物理爆炸, 4是化学爆炸), 1 压缩气体容器爆炸, 2 高压液体容器爆炸, 3 过热液体容器爆炸, 4 蒸气云爆炸
//		// name: 爆炸物名称
////		       1 压缩气体容器爆炸:空气,氮,氧,氢,甲烷,乙烷,乙烯,丙烷,一氧化碳,二氧化碳,一氧化氮,二氧化氮,氨气,氯气,过热蒸气,干饱和蒸气,氢氯酸
////		       2 高压液体容器爆炸:无
////		       3 过热液体容器爆炸:高温饱和水
////		       4 蒸气云爆炸:氢气,氨气,苯,一氧化碳,硫化氢1(生成SO2),硫化氢2(生成SO3),甲烷,乙烷,乙烯,乙炔,丙烷,丙烯,正丁烷,异丁烷,丁烯
//		// pressure: 爆炸物的压力, 单位兆帕(Mpa)
//		// volume: 爆炸物体积, 单位立方米(m³)
//		// hurt: 伤害值
//		// 返回值: 能够造成此伤害值的半径范围
		double CalcExplosionHurtR_API(String userId, int type, int idx, double pressure, double volume, double hurt);

		 /*****************************************热辐射模型***********************************************/

		// 传参数（计算热辐射伤害），后 4 个参数查数据库
		// S: 火灾面积，单位平方米(m2)
		// T0: 环境温度，单位开氏度(K)
		// Hc: 物质的燃烧热，单位焦耳每公斤(J/Kg)
		// Hv: 物质的蒸发热，单位焦耳每公斤(J/Kg)
		// cp: 物质的定压比热容，单位焦耳(每公斤每华氏度)(J/(Kg*K))
		// Tb: 物质的沸点，单位开氏度(K)
		// 返回值: true成功，false失败
		boolean  SetHeatHurtParameters_API(String userId, double S, double T0, double Hc, double Hv, double cp, double Tb);

		// 计算热辐射伤害
		// length: 距离着火中心点距离，单位米(m)
		// time: 暴露在热辐射范围内的时间，单位秒(m)
		// 返回值: 目标地点接收到的热通量密度，单位千瓦每平方米（kW/m2）
		double CalcHeatHurt_API(String userId, double length, double time);

		// 获取死亡概率
		// 返回值: 死亡概率，概率单位为5时对应伤亡百分数50%
		double  GetHeatHurtPr1_API(String userId);

		// 获取二级烧伤概率
		// 返回值: 二级烧伤概率，概率单位为5时对应伤亡百分数50%
		double  GetHeatHurtPr2_API(String userId);

		// 获取一级烧伤概率
		// 返回值: 一级烧伤概率，概率单位为5时对应伤亡百分数50%
		double  GetHeatHurtPr3_API(String userId);

		// 计算热辐射伤害半径
		// time: 暴露在热辐射范围内的时间
		void  CalcHeatHurtR_API(String userId, double time);

		// 获取死亡半径（概率单位大于等于5）
		// 返回值: 死亡半径，单位米(m)
		double  GetHeatHurtR1_API(String userId);

		// 获取二级烧伤半径（概率单位大于等于5）
		// 返回值: 二级烧伤半径，单位米(m)
		double  GetHeatHurtR2_API(String userId);

		// 获取一级烧伤半径（概率单位大于等于5）
		// 返回值: 一级烧伤半径，单位米(m)
		double  GetHeatHurtR3_API(String userId);

		// 传参数（计算热辐射伤害），主要用来计算石油、煤油、汽油等混合物燃烧热辐射伤害
		// S: 火灾面积，单位平方米(m2)
		// Hc: 物质的燃烧热，单位焦耳每公斤(J/Kg)
		// mf: 燃烧速率（kg•m-2•s-1），石油最大燃烧速率 0.0781
		// 返回值: true成功，false失败
		boolean SetHeatHurtMf_API(String userId, double S, double mf, double Hc);

		/*****************************************液体泄漏模型***********************************************/

		// 计算液体泄漏率
		// rou: 液体密度（kg/m3）
		// s: 泄露孔面积（m2）
		// p: 容器内压强（Pa）
		// h: h液压高度（液体最高点与泄漏点之间的高度差）（m）
		// 返回值: 泄漏速率（kg/s）
		double  CalcLiquidLeak_API(String userId, double rou, double s, double p, double h);

		// 计算液体泄漏（瞬时泄露）后形成的液池半径
		// m: 泄漏总量（kg）
		// t: 泄漏时间（s）
		// rou: 液体密度（kg/m3）
		// 返回值: 液池半径（m）
		double  CalcLiquidLeakArea1_API(String userId, double m, double t, double rou);

		// 计算液体泄漏（持续泄露）后形成的液池半径
		// m: 泄漏总量（kg）
		// t: 泄漏时间（s）
		// rou: 液体密度（kg/m3）
		// 返回值: 液池半径（m）
		double  CalcLiquidLeakArea2_API(String userId, double m, double t, double rou);




		/*****************************************蒸发模型***********************************************/

		// 计算闪蒸，蒸发速率
		// Cp:定压比热（J•kg-1•K-1）
		// Hv: 蒸发热（J/kg）
		// T0: 温度（开氏度K）
		// Tb: 沸点（开氏度K）
		// m: 泄漏质量（kg）
		// t: 闪蒸时间（s）
		// 返回值: 闪蒸速率（kg/s）
		double  CalcEvaporate1_API(String userId, double Cp, double Hv, double T0, double Tb, double m, double t);

		// 计算热量蒸发，蒸发速率
		// A1: 液池面积（m2）
		// T0: 环境温度（开氏度K）
		// Tb: 液体沸点（开氏度K）
		// Hv: 蒸发热（J/kg）
		// L: 液池长度（m）
		// K: 地面导热系数（J•m-1•K-1）
		// a: 热扩散系数（m2/s）
		// T: 蒸发时间（s）
		// Nu: 努塞尔数
		// 返回值: 蒸发速率（kg/s）
		double  CalcEvaporate2_API(String userId, double A1, double T0, double Tb, double Hv, double L, double K, double a, double T, double Nu);

		// 计算质量蒸发，蒸发速率
		// alpha: 分子扩散系数（m2/s）
		// Sh: 舍伍德数
		// A: 液池面积（m2）
		// L: 液池长度（m）
		// rou: 液体密度（kg/m3）
		// 返回值: 蒸发速率（kg/s）
		double  CalcEvaporate3_API(String userId, double alpha, double Sh, double A, double L, double rou);




		/*****************************************石油燃烧物分析***********************************************/

		// 计算石油燃烧物中污染物的产生率
		// speed: 风速 m/s
		// mt: 油品燃烧速度 kg/s 或 g/s
		// Cc: 石油中含碳的质量百分比
		// Cs: 石油中含硫的质量百分比
		// Cn: 石油中含氮的质量百分比
		void CalcEmissionRate_API(String userId, double speed, double mt, double Cc, double Cs, double Cn);

		// 获取 CO 产生率
		// 返回值: CO 产生率(单位与 CalcEmission_API 中 mt 的单位一致，同为 kg/s 或 g/s)
		double  GetCO_API(String userId);

		// 获取 CO2 产生率
		// 返回值: CO2 产生率(单位与 CalcEmission_API 中 mt 的单位一致，同为 kg/s 或 g/s
		double GetCO2_API(String userId);

		// 获取 SO2 产生率
		// 返回值: SO2 产生率(单位与 CalcEmission_API 中 mt 的单位一致，同为 kg/s 或 g/s
		double  GetSO2_API(String userId);

		// 获取 NO 产生率
		// 返回值: NO 产生率(单位与 CalcEmission_API 中 mt 的单位一致，同为 kg/s 或 g/s
		double  GetNO_API(String userId);

		// 获取 NO2 产生率
		// 返回值: NO2 产生率(单位与 CalcEmission_API 中 mt 的单位一致，同为 kg/s 或 g/s
		double  GetNO2_API(String userId);

		// 获取有风的情况下的热辐射范围
		// ws: 风速
		// wd: 风向
		// d: 罐直径
		// 返回值: 范围线的数组长度
		int CalcHrLine_API(String userId, double ws, double wd, double d);
		
		// 获取轮廓线顶点坐标数组[x1,y1,z1,x2,y2,z2,......]
		void GetHrVertices1_API(String userId, double va[]);
		void GetHrVertices2_API(String userId, double va[]);
		void GetHrVertices3_API(String userId, double va[]);
		
		/*****************************************重质油品喷溅范围***********************************************/

		// 计算重质油品喷溅范围
		// ws: 风速 m/s
		// wd: 风向
		// r: 油罐半径
		// va[]: 轮廓线顶点坐标数组[x1,y1,x2,y2,......], 数组长度 80
		void GetPJRegion_API(String userId, double ws, double wd, double r, double va[]);
		
		/*****************************************火灾蔓延范围和速度***********************************************/

		// 计算火灾蔓延范围和速度
		// ws: 风速 m/s
		// wd: 风向
		// t: 蔓延时间 min
		// va[]: 轮廓线顶点坐标数组[x1,y1,x2,y2,......], 数组长度
		// sa[]: 火灾蔓延速度, 数组长度
		void GetFireSpreadRegion_API(String userId, double ws, double wd, double t, double va[], double sa[]);
		
}
