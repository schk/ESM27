package cn.stronglink.esm27.thirdsdk.explosion;

import com.sun.jna.Native;
import com.sun.jna.win32.StdCallLibrary;

import cn.stronglink.core.util.PathUtil;

public interface Explosion extends StdCallLibrary {
	
	Explosion INSTANCE = (Explosion) Native.loadLibrary(PathUtil.getClassPath() + "sdk\\explosion\\ExplosionModel.dll",
			Explosion.class); 
	
}
