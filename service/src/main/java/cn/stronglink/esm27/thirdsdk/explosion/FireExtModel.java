package cn.stronglink.esm27.thirdsdk.explosion;

import com.sun.jna.Native;
import com.sun.jna.win32.StdCallLibrary;

import cn.stronglink.core.util.PathUtil;

public interface FireExtModel extends StdCallLibrary {
	
	FireExtModel INSTANCE = (FireExtModel) Native.loadLibrary(PathUtil.getClassPath() + "sdk\\explosion\\FiresDLL.dll",
			FireExtModel.class); 

        //1.固体可燃物燃烧火场用水量计算
		/**
		 * 
		 * 其中：
		 * 
		 * 	areas：火场燃烧面积，m2
		 * 	material：燃烧物，可取的值为：纸张、聚合材料、塑料、棉纤维、赛璐珞、原木、锯材、堆垛、原木堆垛、亚麻杆
		 * 	返回值：用水量，L/s
		 * */
        double solid_fires(String areas,String material);
        
//        2、液化石油气储罐冷却用水量计算
        /**
         * 其中：
        * diameter1：着火罐直径，m
        * diameter2：相邻罐直径，m
        * nums：相邻罐数量
        * 返回值：用水量，L/s
         * 
         * */
        double lpg_fires(String diameter1,String diameter2,String nums);
//        3、油罐区消防力量计算
        /**
         * areas：地面火燃烧面积，m2
         * materialType：物质类型，甲类和乙类液体取1，丙类液体取2
         * gDiameters：着火罐直径，m;
         * gNums：着火罐数量
         * gTypes：着火罐类型，可取的值为：固定顶立式罐、浮顶罐、卧式罐、地下或半地下罐
         * cDiameters：邻近罐直径，m
         * cNums：邻近罐数量
         * cTypes：邻近罐类型，可取的值为：固定顶立式非保温罐、固定顶立式保温罐、卧式罐、地下或半地下罐
         * 返回值：泡沫(1%,3%,6%): x1,x2,x3;消防水(1%,3%,6%):y1,y2,y3
         * 返回值说明：
         *  泡沫(1%,3%,6%)表示泡沫配比浓度分别为1%,3%,6%，x1,x2,x3为对应浓度下需要的泡沫量，L/s
         * 消防水(1%,3%,6%)表示泡沫配比浓度分别为1%,3%,6%，y1,y2,y3为对应泡沫配比浓度下需要的消防水量，L/s
         * 
         * */
        String oil_fires(String areas,String materialType,String gDiameters,String gNums,String gTypes,String cDiameters,String cNums,String cTypes);
		
}
