package cn.stronglink.esm27.mobile.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.core.util.Base64EncryptUtil;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.CameraSet;
import cn.stronglink.esm27.entity.GasType;
import cn.stronglink.esm27.entity.Room;
import cn.stronglink.esm27.entity.SysSession;
import cn.stronglink.esm27.entity.User;
import cn.stronglink.esm27.mobile.vo.MobileRoomParam;
import cn.stronglink.esm27.mobile.vo.MobileRoomReturn;
import cn.stronglink.esm27.module.system.user.service.UserService;
import cn.stronglink.esm27.web.camera.service.CameraService;
import cn.stronglink.esm27.web.realTimeData.service.RealTimeDataService;
import cn.stronglink.esm27.web.realTimeData.vo.GasDataInitPageVo;
import cn.stronglink.esm27.web.sysSession.service.SysSessionService;
import cn.stronglink.esm27.web.webData.params.IncidentRecordParam;
import cn.stronglink.esm27.web.webData.service.RoomService;
import cn.stronglink.esm27.web.webData.service.WebDataService;
import cn.stronglink.esm27.web.webData.vo.IncidentRecordVo;

@Controller
@RequestMapping(value = "api/mobile")
public class MobileController extends AbstractController {
	
	@Autowired
	private WebDataService webDataService;
	@Autowired
	private UserService userService;
	@Autowired
	private RoomService roomService;
	@Autowired
	private SysSessionService sysSessionService;
	@Autowired
	private RealTimeDataService  realTimeDataService;
	
	@Autowired
	private CameraService cameraService;
	
	@Value("${stream.server}")
	private String streamserver;
	
	/**
	 * 跳转手机端页面
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("index")
    public String index(Model model,HttpServletRequest request) {
		List<GasType> historyGasTableTitle = realTimeDataService.qryhistoryGasTableTitle();
		model.addAttribute("historyGasTableTitle", historyGasTableTitle);
        return "mobile";
    }
	
	/**
	 * 手机端加入房间
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param mobileRoomParam
	 * @return
	 */
	@RequestMapping("joinRoom")
	public ResponseEntity<ModelMap> joinRoom(HttpServletRequest request, HttpServletResponse response, 
			ModelMap modelMap,@RequestBody MobileRoomParam param){
		User u =userService.findUserByName(param.getAccount());
		if(u==null) return setModelMap(modelMap, HttpCode.CONFLICT, "该用户不存在");
		Room r =roomService.getRoomByCode(param.getRoomCode());
		if(r!=null) {
			SysSession sys = new SysSession();
			sys.setId(IdWorker.getId());
			sys.setUserId(u.getId());
			sys.setCreateTime(new Date());
			sys.setIp(WebUtil.getHost(request));
			sys.setSource(2);
			sys.setSessionId(request.getSession().getId());
			sysSessionService.insertSysSession(sys);
			request.getSession().setAttribute("currentUser", u);
			request.getSession().setAttribute("roomId",r.getId());
			MobileRoomReturn rr= new MobileRoomReturn();
			rr.setRoomId(r.getId());
			rr.setUserId(u.getId());
			rr.setAccidentConductor(r.getAccidentConductor());
			rr.setAccount(param.getAccount());
			rr.setRoomCode(param.getRoomCode());
			Object oo =JSONObject.parse(r.getAccidentLocation());
			JSONObject obj =JSONObject.parseObject(oo.toString());
			if(obj.getString("type").equals("Feature")){
				JSONObject obj1 =(JSONObject) obj.get("geometry");
				JSONArray a =obj1.getJSONArray("coordinates");
				String location=a.get(0)+","+a.get(1);
				rr.setAccidentLocation(location);
			}else{
				rr.setAccidentLocation(obj.getString("name"));
			}
			return setSuccessModelMap(modelMap,rr);
		}else {
            return setModelMap(modelMap, HttpCode.CONFLICT, "该房间不存在");
		}
	}
	
	/**
	 * 获取气体检测仪
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("qryGasData")
	public ResponseEntity<ModelMap> qryGasData(HttpServletRequest request, HttpServletResponse response, 
			ModelMap modelMap){
		List<GasDataInitPageVo> deviceList = realTimeDataService.qryGasDataInitPage();
		return setSuccessModelMap(modelMap,deviceList);
	}
	
	
	/**
	 * 手机端退出房间
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param mobileRoomParam
	 * @return
	 */
	@RequestMapping("quitRoom")
	public ResponseEntity<ModelMap> quitRoom(HttpServletRequest request, HttpServletResponse response, 
			ModelMap modelMap){
		HttpSession session = request.getSession();
		if(session.getAttribute("roomId")!=null){
			String oId = session.getAttribute("roomId").toString();
			if(!"".equals(oId)&&oId!=null){
				User user = (User)session.getAttribute("currentUser");
				SysSession sys = new SysSession();
				sys.setRoomId(null);
				sys.setUserId(user.getId());
				sysSessionService.updateSysSession(sys);
				return setSuccessModelMap(modelMap);
			}	
		}
		 //清空session
		request.getSession().removeAttribute("currentUser");
		return setSuccessModelMap(modelMap, null);
		
	}
	
	
	
	/**
	 * 获取事故基本信息
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param mobileRoomParam
	 * @return
	 */
	@RequestMapping("getRoom")
	public ResponseEntity<ModelMap> getRoom(HttpServletRequest request, HttpServletResponse response, 
			ModelMap modelMap,@RequestBody Long roomId){
		Room r =roomService.getRoomInfo(roomId);
		if(r!=null) {
			return setSuccessModelMap(modelMap,r);
		}
		 return setModelMap(modelMap, HttpCode.CONFLICT, "该房间不存在");
		
	}
	
	/**
	 * 查询未完成应急指挥事故操作详情
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param accident
	 * @return
	 */
	@RequestMapping(value = "getIncidentRecordByAccident")
	public ResponseEntity<ModelMap> getIncidentRecordByAccident(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody String id) {
		List<IncidentRecordVo> irs = webDataService.getIncidentRecordByRoomId(id);
		return setSuccessModelMap(modelMap, irs);
	}
	
	
	/**
	 * 插入应急指挥记录
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param record
	 * @return
	 */
	@RequestMapping(value = "insertIncidentRecordMobile")
	public ResponseEntity<ModelMap> insertIncidentRecordMobile(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody IncidentRecordParam record) {
		webDataService.insertIncidentRecord(record);
		return setSuccessModelMap(modelMap, record);
	}
	
	
	/*
	 * 查询摄像头流媒体地址
	 */
	@RequestMapping(value = "qryCameraSteam")
	public ResponseEntity<ModelMap> qryCamera(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap, Long roomId) throws Exception{
		List<CameraSet> data = cameraService.getCamera();
		List<String> steams = new ArrayList<String>();
		for(CameraSet cs : data) {
			steams.add("rtmp://" + streamserver + "/hls/" + Base64EncryptUtil.encrypt(cs.getIp()));
		}
		return setSuccessModelMap(modelMap, steams);
	}
	
}
