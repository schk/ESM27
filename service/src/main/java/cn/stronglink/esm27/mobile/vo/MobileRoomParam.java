package cn.stronglink.esm27.mobile.vo;

import com.baomidou.mybatisplus.annotations.TableField;

public class MobileRoomParam {
	
	@TableField(exist = false)
	private static final long serialVersionUID = 1421301688225365334L;
	
	private String roomCode;
	private String account;
	private Long userId;
	private Long roomId;
	public String getRoomCode() {
		return roomCode;
	}
	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getRoomId() {
		return roomId;
	}
	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}
   
}
