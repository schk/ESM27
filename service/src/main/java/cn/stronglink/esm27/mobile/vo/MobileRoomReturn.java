package cn.stronglink.esm27.mobile.vo;

import com.baomidou.mybatisplus.annotations.TableField;

public class MobileRoomReturn {
	
	@TableField(exist = false)
	private static final long serialVersionUID = 1421301688225365334L;
	
	private String roomCode;
	private String account;
	private Long userId;
	private Long roomId;
	private String accidentConductor;
	private String accidentLocation;
	public String getRoomCode() {
		return roomCode;
	}
	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getRoomId() {
		return roomId;
	}
	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}
	public String getAccidentConductor() {
		return accidentConductor;
	}
	public void setAccidentConductor(String accidentConductor) {
		this.accidentConductor = accidentConductor;
	}
	public String getAccidentLocation() {
		return accidentLocation;
	}
	public void setAccidentLocation(String accidentLocation) {
		this.accidentLocation = accidentLocation;
	}
   
}
