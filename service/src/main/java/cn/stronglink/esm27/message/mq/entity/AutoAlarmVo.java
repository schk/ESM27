package cn.stronglink.esm27.message.mq.entity;

import java.util.List;

public class AutoAlarmVo {
	private String deviceAddress;
	private List<AlarmInfo> alarmInfoList;
	private String userId;
	private String roomId;
	public String getDeviceAddress() {
		return deviceAddress;
	}
	public void setDeviceAddress(String deviceAddress) {
		this.deviceAddress = deviceAddress;
	}
	public List<AlarmInfo> getAlarmInfoList() {
		return alarmInfoList;
	}
	public void setAlarmInfoList(List<AlarmInfo> alarmInfoList) {
		this.alarmInfoList = alarmInfoList;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	
}
