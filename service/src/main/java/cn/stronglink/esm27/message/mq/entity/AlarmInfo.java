package cn.stronglink.esm27.message.mq.entity;

public class AlarmInfo {
	private boolean alarm;
    private String alarmContent;
    private int sensorChannelNumber;
    private String sensorName;
	public boolean isAlarm() {
		return alarm;
	}
	public void setAlarm(boolean alarm) {
		this.alarm = alarm;
	}
	public String getAlarmContent() {
		return alarmContent;
	}
	public void setAlarmContent(String alarmContent) {
		this.alarmContent = alarmContent;
	}
	public int getSensorChannelNumber() {
		return sensorChannelNumber;
	}
	public void setSensorChannelNumber(int sensorChannelNumber) {
		this.sensorChannelNumber = sensorChannelNumber;
	}
	public String getSensorName() {
		return sensorName;
	}
	public void setSensorName(String sensorName) {
		this.sensorName = sensorName;
	}
    
}
