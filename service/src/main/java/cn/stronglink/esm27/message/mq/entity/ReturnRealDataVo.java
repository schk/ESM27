package cn.stronglink.esm27.message.mq.entity;

import java.util.List;

import cn.stronglink.esm27.entity.GasDetector;
import cn.stronglink.esm27.web.realTimeData.vo.GasDetectorDataVo;

public class ReturnRealDataVo {
	
	private GasDetector gd;
	private List<GasDetectorDataVo> gddVoList;
	public GasDetector getGd() {
		return gd;
	}
	public void setGd(GasDetector gd) {
		this.gd = gd;
	}
	public List<GasDetectorDataVo> getGddVoList() {
		return gddVoList;
	}
	public void setGddVoList(List<GasDetectorDataVo> gddVoList) {
		this.gddVoList = gddVoList;
	}
	
}
