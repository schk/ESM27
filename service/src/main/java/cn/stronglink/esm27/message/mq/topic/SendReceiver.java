package cn.stronglink.esm27.message.mq.topic;

import javax.jms.Topic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class SendReceiver {

	@Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

//    @Autowired
//    private Queue queue;

    @Autowired
    private Topic topic;
    
    @Autowired
    private Topic algorithmTopic;
    
    /**
     * @param param
     * json格式字符串
     * */
    public void send(String param) {
        //send queue.
        //this.jmsMessagingTemplate.convertAndSend(this.queue, "hi,activeMQ");
        //send topic.
        this.jmsMessagingTemplate.convertAndSend(this.topic, param);
    }
    
    public void sendAlgorithm(String param) {
        //send queue.
        //this.jmsMessagingTemplate.convertAndSend(this.queue, "hi,activeMQ");
        //send topic.
        this.jmsMessagingTemplate.convertAndSend(this.algorithmTopic, param);
    }
    
}
