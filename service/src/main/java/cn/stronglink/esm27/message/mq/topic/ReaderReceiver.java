package cn.stronglink.esm27.message.mq.topic;

import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.activemq.command.ActiveMQBytesMessage;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

import cn.stronglink.core.util.DateUtil;
import cn.stronglink.esm27.entity.WeatherStationData;
import cn.stronglink.esm27.message.mq.entity.AutoAlarmVo;
import cn.stronglink.esm27.message.mq.entity.BaseMessage;
import cn.stronglink.esm27.message.mq.entity.ReturnDataVo;
import cn.stronglink.esm27.web.realTimeData.service.RealTimeDataService;
import cn.stronglink.esm27.web.realTimeData.vo.GasDetectorDataVo;
import cn.stronglink.esm27.web.realTimeData.vo.MQReturnDataVo;
import cn.stronglink.esm27.web.realTimeData.vo.MQReturnWeatherDataVo;

@Component
public class ReaderReceiver implements MessageListener {

	@Autowired
	private RealTimeDataService realTimeDataService;

	@Autowired
	private SimpMessagingTemplate template;

	@JmsListener(destination = "ESM27ToService")
	public void onMessage(Message message) {
		try {
			String msg =null;
			if (message instanceof ActiveMQBytesMessage) {
				ActiveMQBytesMessage bytesMessage = (ActiveMQBytesMessage) message;
				long len = bytesMessage.getBodyLength();
				byte[] msgbytes = new byte[(int)len];
				bytesMessage.readBytes(msgbytes);
				msg = new String(msgbytes);
			}else if (message instanceof ActiveMQTextMessage) {
				TextMessage tm = (TextMessage) message;
				msg = tm.getText();
			}else {
				return;
			}
			
			System.out.println("收到的 message 是：" + msg);
			if (msg != null) {
				BaseMessage bm = JSON.parseObject(msg, BaseMessage.class);
				System.out.println("转完的 message 是：" + msg);
				if (bm.getActioncode().equals("GetAllSensorDataReturn")) {// 获取数据返回
					if (bm.getIsSuccess() && bm.getAwsPostdata() != null) {
						List<GasDetectorDataVo> dataVoList = realTimeDataService.insertData(bm.getAwsPostdata());
						MQReturnDataVo mqVo = JSONObject.parseObject(JSONObject.toJSONString(bm.getAwsPostdata()),
								MQReturnDataVo.class);
						ReturnDataVo vo = new ReturnDataVo();
						vo.setReturnCode("realTimeData");
						vo.setPumpState(mqVo.getPumpState());
//						double wd = GetChange(mqVo.getLatitude());
//			            double jd = GetChange(mqVo.getLongitude());
//			            double[] arr = MapLonLatUtil.wgs84togcj02(jd, wd);
//			            vo.setLongitude(39.5849628476339);
//						vo.setLatitude(116.29750763128679);
						
						vo.setLatitude(mqVo.getLatitude());
						vo.setLongitude(mqVo.getLongitude());
						if (mqVo.getDeviceAddress().length() == 1) {
							mqVo.setDeviceAddress("00" + mqVo.getDeviceAddress());
						} else if (mqVo.getDeviceAddress().length() == 2) {
							mqVo.setDeviceAddress("0" + mqVo.getDeviceAddress());
						}
						vo.setDeviceAddress(mqVo.getDeviceAddress());
						List<GasDetectorDataVo> alertDataVoList = new ArrayList<GasDetectorDataVo>();
						for (GasDetectorDataVo gasDetectorDataVo : dataVoList) {
							if (gasDetectorDataVo.getGasIsAlert() == 2) {
								alertDataVoList.add(gasDetectorDataVo);
							}
						}
						String lastUpdateTime = DateUtil.getDateTimeHMS();
						vo.setRealData(dataVoList);
						vo.setAlertRealData(alertDataVoList);
						vo.setLastUpdateTime(lastUpdateTime);
						DecimalFormat df = new DecimalFormat("#.0");
						vo.setBatteryVoltage(df.format(Double.parseDouble(mqVo.getBatteryVoltage())));
						String result = JSONObject.toJSONString(vo, SerializerFeature.DisableCircularReferenceDetect);
						if (StringUtils.isNotBlank(mqVo.getRoomId())) {
							this.template.convertAndSend("/topic/collect/" + mqVo.getRoomId(), result);// 向页面房间内推送实时数据
						} else {
							this.template.convertAndSend("/topic/collect/" + mqVo.getUserId(), result);// 向页面个人推送实时数据
						}

					}
				}
				if (bm.getActioncode().equals("GetSystemSerialPortListReturn")) {// 获取串口返回
					if (bm.getIsSuccess() && bm.getAwsPostdata() != null) {
						ReturnDataVo vo = new ReturnDataVo();
						vo.setReturnCode("serialPortData");
						vo.setReturData(bm.getAwsPostdata().toString());
						String result = JSONObject.toJSONString(vo, SerializerFeature.DisableCircularReferenceDetect);
						this.template.convertAndSend("/topic/all", result);// 向页面推送实时数据
					}
				}
				if (bm.getActioncode().equals("GetWindRealtimeDatasReturn")) {// 获取气象站返回
					if (bm.getIsSuccess() && bm.getAwsPostdata() != null) {
						WeatherStationData data = realTimeDataService.insertWeatherData(bm.getAwsPostdata());
						MQReturnWeatherDataVo mqVo = JSONObject
								.parseObject(JSONObject.toJSONString(bm.getAwsPostdata()), MQReturnWeatherDataVo.class);
						ReturnDataVo vo = new ReturnDataVo();
						vo.setWsd(data);
						vo.setReturnCode("weatherData");
						String lastUpdateTime = DateUtil.getDateTimeHMS();
						vo.setLastUpdateTime(lastUpdateTime);
						String result = JSONObject.toJSONString(vo, SerializerFeature.DisableCircularReferenceDetect);
						if (StringUtils.isNotBlank(mqVo.getRoomId())) {
							this.template.convertAndSend("/topic/collect/" + mqVo.getRoomId(), result);// 向页面房间内推送实时数据
						} else {
							this.template.convertAndSend("/topic/collect/" + mqVo.getUserId(), result);// 向页面个人推送实时数据
						}
					}
				}
				if (bm.getActioncode().equals("ESM27AlgorithmToService")) {// 轻质气体扩散模型
					if (bm.getIsSuccess() && bm.getAwsPostdata() != null) {
						WeatherStationData data = realTimeDataService.insertWeatherData(bm.getAwsPostdata());
						ReturnDataVo vo = new ReturnDataVo();
						// MQMessageOfESM27 gaussionModelSybn = new MQMessageOfESM27();
						// gaussionModelSybn.set
						// vo.setWsd(data);
						vo.setReturnCode("gaussionModelParametersSyn");
						String lastUpdateTime = DateUtil.getDateTimeHMS();
						vo.setLastUpdateTime(lastUpdateTime);
						String result = JSONObject.toJSONString(vo, SerializerFeature.DisableCircularReferenceDetect);
						this.template.convertAndSend("/topic/all", result);// 向页面推送实时数据
					}
				}
				// 实时报警信息
				if (bm.getActioncode().equals("AutoAlarm")) {
					if (bm.getIsSuccess() && bm.getAwsPostdata() != null) {
						AutoAlarmVo autoAlarmVo = ((JSONObject) bm.getAwsPostdata()).toJavaObject(AutoAlarmVo.class);
						if ((autoAlarmVo != null)) {
							List<GasDetectorDataVo> dataVoList = realTimeDataService
									.queryGasDetectorDataByDev(autoAlarmVo);
							if (dataVoList != null && dataVoList.size() > 0) {
								ReturnDataVo vo = new ReturnDataVo();
								vo.setReturnCode("AutoAlarm");
								if (autoAlarmVo.getDeviceAddress().length() == 1) {
									autoAlarmVo.setDeviceAddress("00" + autoAlarmVo.getDeviceAddress());
								} else if (autoAlarmVo.getDeviceAddress().length() == 2) {
									autoAlarmVo.setDeviceAddress("0" + autoAlarmVo.getDeviceAddress());
								}
								vo.setDeviceAddress(autoAlarmVo.getDeviceAddress());
								List<GasDetectorDataVo> alertDataVoList = new ArrayList<GasDetectorDataVo>();
								for (GasDetectorDataVo gasDetectorDataVo : dataVoList) {
									if (gasDetectorDataVo.getGasIsAlert() == 2) {
										alertDataVoList.add(gasDetectorDataVo);
									}
								}
								String lastUpdateTime = DateUtil.getDateTimeHMS();
								vo.setRealData(dataVoList);
								vo.setAlertRealData(alertDataVoList);
								vo.setLastUpdateTime(lastUpdateTime);
								vo.setDeviceAddress(autoAlarmVo.getDeviceAddress());
								String result = JSONObject.toJSONString(vo,
										SerializerFeature.DisableCircularReferenceDetect);
								if (StringUtils.isNotBlank(autoAlarmVo.getRoomId())) {
									this.template.convertAndSend("/topic/collect/" + autoAlarmVo.getRoomId(), result);// 向页面房间内推送实时数据
								} else {
									this.template.convertAndSend("/topic/collect/" + autoAlarmVo.getUserId(), result);// 向页面个人推送实时数据
								}
							}
						}
					}
				}
			}

		} catch (JMSException e) {
			e.printStackTrace();
		}

	}
	
	private static float GetChange(Double double1)
    {
        double num = Math.floor((double)double1);
        double num2 = ((double)double1 - num) * 100.0 / 60.0;
        return Float.valueOf((float) (num+num2));
    }
	
	
	

}
