package cn.stronglink.esm27.message.mq.entity;

import java.util.List;

import cn.stronglink.esm27.entity.WeatherStationData;
import cn.stronglink.esm27.web.realTimeData.vo.GasDetectorDataVo;

public class ReturnDataVo {
	
	private String returnCode;
	private String returData;
	private List<GasDetectorDataVo> realData;
	private List<GasDetectorDataVo> alertRealData;
	private String lastUpdateTime;
	private WeatherStationData wsd;
	private Double latitude;//纬度
	private Double longitude;//精度
	private String deviceAddress;//设备号
	private String batteryVoltage;//电量
	private Double pumpState;//泵状态
	
	public String getDeviceAddress() {
		return deviceAddress;
	}
	public void setDeviceAddress(String deviceAddress) {
		this.deviceAddress = deviceAddress;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public WeatherStationData getWsd() {
		return wsd;
	}
	public void setWsd(WeatherStationData wsd) {
		this.wsd = wsd;
	}
	public List<GasDetectorDataVo> getRealData() {
		return realData;
	}
	public void setRealData(List<GasDetectorDataVo> realData) {
		this.realData = realData;
	}
	public List<GasDetectorDataVo> getAlertRealData() {
		return alertRealData;
	}
	public void setAlertRealData(List<GasDetectorDataVo> alertRealData) {
		this.alertRealData = alertRealData;
	}
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public String getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	public String getReturData() {
		return returData;
	}
	public void setReturData(String returData) {
		this.returData = returData;
	}
	public String getBatteryVoltage() {
		return batteryVoltage;
	}
	public void setBatteryVoltage(String batteryVoltage) {
		this.batteryVoltage = batteryVoltage;
	}

	public Double getPumpState() {
		return pumpState;
	}

	public void setPumpState(Double pumpState) {
		this.pumpState = pumpState;
	}
}
