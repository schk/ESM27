package cn.stronglink.esm27.message.mq.topic;

import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.activemq.command.ActiveMQTextMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

import cn.stronglink.core.util.DateUtil;
import cn.stronglink.esm27.entity.WeatherStationData;
import cn.stronglink.esm27.message.mq.entity.BaseMessage;
import cn.stronglink.esm27.message.mq.entity.ReturnDataVo;
import cn.stronglink.esm27.web.realTimeData.service.RealTimeDataService;
import cn.stronglink.esm27.web.realTimeData.vo.GasDetectorDataVo;
import cn.stronglink.esm27.web.realTimeData.vo.MQReturnDataVo;

@Component
public class AlgorithmReaderReceiver  implements MessageListener {
	
	@Autowired
	private RealTimeDataService  realTimeDataService;
	
	@Autowired
    private SimpMessagingTemplate template;
	
	@JmsListener(destination = "ESM27AlgorithmToService")
	public void onMessage(Message message) {
		try {
			if (!(message instanceof ActiveMQTextMessage)) {
				return;
			}
			TextMessage tm = (TextMessage) message;
			String msg = tm.getText();
			System.out.println("收到的 message 是：" + msg);
			if (msg!=null) {
				BaseMessage bm = JSON.parseObject(msg, BaseMessage.class);
				if (bm.getActioncode().equals("ParticleLandingAlgorithmResult")) {//轻质气体扩散模型
					if (bm.getIsSuccess()&&bm.getAwsPostdata()!=null) {
//						WeatherStationData data = realTimeDataService.insertWeatherData(bm.getAwsPostdata());
						ReturnDataVo vo = new ReturnDataVo();
//						MQMessageOfESM27 gaussionModelSybn = new MQMessageOfESM27();
//						gaussionModelSybn.set
//						vo.setWsd(data);
						vo.setReturnCode("StartParticleLandingAlgorithmServiceReturn");
						String lastUpdateTime = DateUtil.getDateTimeHMS();
						vo.setLastUpdateTime(lastUpdateTime);
						String result = JSONObject.toJSONString(vo,SerializerFeature.DisableCircularReferenceDetect);
						this.template.convertAndSend("/topic/all", result);//向页面推送实时数据
					}
				}
			}
			
			
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
}
