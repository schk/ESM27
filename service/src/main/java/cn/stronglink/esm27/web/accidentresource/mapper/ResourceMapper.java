package cn.stronglink.esm27.web.accidentresource.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import cn.stronglink.esm27.entity.Resource;
import cn.stronglink.esm27.entity.ResourceExample;

public interface ResourceMapper {
	int countByExample(ResourceExample example);

    int deleteByExample(ResourceExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Resource record);

    int insertSelective(Resource record);

    List<Resource> selectByExampleWithBLOBs(ResourceExample example);

    List<Resource> selectByExample(ResourceExample example);

    Resource selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Resource record, @Param("example") ResourceExample example);

    int updateByExampleWithBLOBs(@Param("record") Resource record, @Param("example") ResourceExample example);

    int updateByExample(@Param("record") Resource record, @Param("example") ResourceExample example);

    int updateByPrimaryKeySelective(Resource record);

    int updateByPrimaryKeyWithBLOBs(Resource record);

    int updateByPrimaryKey(Resource record);
    
    List<Resource> queryResource(Map<String, Object> params);

	List<Resource> qryResourceListInMap(Map<String, Object> params);

}