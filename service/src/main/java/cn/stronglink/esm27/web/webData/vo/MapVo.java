package cn.stronglink.esm27.web.webData.vo;

import java.util.List;

public class MapVo{

	private String type;
	private List<MapFeaturesVo> features;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<MapFeaturesVo> getFeatures() {
		return features;
	}
	public void setFeatures(List<MapFeaturesVo> features) {
		this.features = features;
	}
	
	
	
}
