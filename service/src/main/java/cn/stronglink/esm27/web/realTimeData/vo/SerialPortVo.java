package cn.stronglink.esm27.web.realTimeData.vo;

public class SerialPortVo {
	
	private String serialPortName;
	
	private Integer autoPushSensorDataIntervalSeconds;

	public String getSerialPortName() {
		return serialPortName;
	}

	public void setSerialPortName(String serialPortName) {
		this.serialPortName = serialPortName;
	}

	public Integer getAutoPushSensorDataIntervalSeconds() {
		return autoPushSensorDataIntervalSeconds;
	}

	public void setAutoPushSensorDataIntervalSeconds(Integer autoPushSensorDataIntervalSeconds) {
		this.autoPushSensorDataIntervalSeconds = autoPushSensorDataIntervalSeconds;
	}
	
}
