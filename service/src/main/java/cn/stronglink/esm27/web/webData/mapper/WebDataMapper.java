package cn.stronglink.esm27.web.webData.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.esm27.entity.Department;
import cn.stronglink.esm27.entity.Dictionary;
import cn.stronglink.esm27.entity.EmergencyMaterial;
import cn.stronglink.esm27.entity.Equipment;
import cn.stronglink.esm27.entity.EquipmentType;
import cn.stronglink.esm27.entity.Facilities;
import cn.stronglink.esm27.entity.KeyUnit;
import cn.stronglink.esm27.entity.Teams;
import cn.stronglink.esm27.module.danger.danger.vo.DangersVo;
import cn.stronglink.esm27.module.emergencyResources.reservePoint.vo.ReservePointVo;
import cn.stronglink.esm27.module.equipment.equipment.vo.EquipmentVo;
import cn.stronglink.esm27.module.expert.expert.vo.ExpertTypeVo;
import cn.stronglink.esm27.module.expert.expert.vo.ExpertVo;
import cn.stronglink.esm27.module.facilities.vo.FacilitiesUnitVo;
import cn.stronglink.esm27.module.facilities.vo.FacilitiesVo;
import cn.stronglink.esm27.module.fireEngine.vo.FireEngineEquipmentVo;
import cn.stronglink.esm27.module.fireEngine.vo.FireEngineVo;
import cn.stronglink.esm27.module.fireWaterSource.vo.FireWaterSourceVo;
import cn.stronglink.esm27.module.fireWaterSource.vo.WaterSourceUnitVo;
import cn.stronglink.esm27.module.plan.vo.PlanTypeVo;
import cn.stronglink.esm27.module.plan.vo.PlanVo;
import cn.stronglink.esm27.web.webData.vo.EmergencyMaterialStatistics;
import cn.stronglink.esm27.web.webData.vo.FireBridgeVo;
import cn.stronglink.esm27.web.webData.vo.PlottingVo;

public interface WebDataMapper{

	List<Department> qryDepts();

	List<KeyUnit> getListByParams(Page<KeyUnit> page, Map<String, Object> params);

	List<KeyUnit> getKeyUnit();

	List<FireWaterSourceVo> qryFireWaterSource(Page<FireWaterSourceVo> page, Map<String, Object> params);
	
	List<FacilitiesVo> qryFacilities(Page<FacilitiesVo> page, Map<String, Object> params);

	List<DangersVo> qryDangerChemic(Page<DangersVo> page, Map<String, Object> params);

	List<Teams> qryTeams(Page<Teams> page, Map<String, Object> params);

	List<ReservePointVo> qryReservePoint(Page<ReservePointVo> page, Map<String, Object> params);

	List<ExpertTypeVo> qryExpertType(Map<String, Object> params);

	List<ExpertVo> qryExpertList(Map<String, Object> params);
	
	List<PlanVo> qryPlans(Map<String, Object> params);

	List<EquipmentVo> qryEquipmentList(Page<EquipmentVo> page, Map<String, Object> params);

	List<Dictionary> qryDictionaryByType(int type);

	List<EquipmentType> qryEquipmentTypeList(int type);

	List<Map<String, Object>> qryDataForMapZddw(Map<String,Object> params);

	List<Map<String, Object>> qryDataForMapXfsy(Map<String,Object> params);

	List<Map<String, Object>> qryDataForMapWzcbd(Map<String,Object> params);

	List<Map<String, Object>> qryDataForMapYjdw(Map<String,Object> params);
	
	List<Map<String, Object>> qryDataForMapXfdw(Map<String,Object> params);

	List<Map<String, Object>> qryDataForMapSczz(Map<String, Object> param);

	List<FireEngineVo> qryFireEngine(Page<FireEngineVo> page, Map<String, Object> params);
	
	List<Map<String, Object>> qryDataForMapWzcbdByDistance(Map<String,Object> params);

	List<FireEngineVo> qryFireEngineByDept(Map<String, Object> params);
	
	List<FireEngineEquipmentVo> qryFireEngineEquipmentByfId(Long fireEngineId);

	List<EmergencyMaterial> qryReservePointDetail(Long pointId); 

	List<PlottingVo> qryDictionaryByTypeChild(int type);

	KeyUnit qryKeyUnitById(Long id);

	Facilities qryFacilitiesById(Long id);

	Teams qryTeamsById(Long id);
	
	Equipment qryEquipmentById(Long id);

	void updateEmergencyMaterial(@Param("id")Long id, @Param("storageQuantity")Integer storageQuantity);

	List<FireBridgeVo> qryFireBridgeVo(Page<FireBridgeVo> page, Map<String, Object> params);
	
	List<FireBridgeVo> qryFireBridgeVoAll(Map<String, Object> params);

	List<FireEngineVo> getFireEngineByBridgeId(Map<String, Object> params);

	Map<String,Object> getWaterCount(Long fireBrigadeId);

	List<KeyUnit> qryKeyUnitByBridgeId(Map<String, Object> params);

	List<FireBridgeVo> qryFireBridgeForCar(Page<FireBridgeVo> page, Map<String, Object> params);

	List<PlanTypeVo> qryPlansType(Page<PlanTypeVo> page, Map<String, Object> params);

	List<ExpertVo> qryExpertListPage(Page<ExpertVo> page, Map<String, Object> params);

	List<PlanVo> qryPlansPage(Page<PlanVo> page, Map<String, Object> params);

	List<Map<String, Object>> qryDataForMapXfsS(Map<String, Object> param);

	List<EmergencyMaterial> qryWuziListByPage(Page<EmergencyMaterial> page, Map<String, Object> params);

	List<EmergencyMaterial> qryWuziListAll(Map<String, Object> params);

	List<PlanTypeVo> qryPlansUnit(Page<PlanTypeVo> page, Map<String, Object> params);

	List<WaterSourceUnitVo> qryKeyUnitForWaterSource(Page<WaterSourceUnitVo> page, Map<String, Object> params);

	List<FireWaterSourceVo> getWaterSourceByKeyUnit(Map<String, Object> params);

	List<FacilitiesUnitVo> qryKeyUnitForFacilities(Page<FacilitiesUnitVo> page, Map<String, Object> params);

	List<FacilitiesVo> getFacilitiesByKeyUnit(Map<String, Object> params);

	void endRescue(Map<String, Object> params);

	int checkEndRescue(Map<String, Object> params);

	List<EmergencyMaterial> qryZhntjEmergencyMaterial(Map<String, Object> params);

	List<DangersVo> qryZhntjDangerChemic(@Param("cNames")String[] gas);

	List<EmergencyMaterialStatistics> qryEmergencyMaterialStatistics(Map<String, Object> params);

	List<ReservePointVo> qryZhntjReservePoint(Map<String, Object> params);

	List<Map<String, Object>> qryDataForMapShzy(HashMap<String, Object> param);
}
