package cn.stronglink.esm27.web.realTimeData.vo;

import java.util.List;

import cn.stronglink.esm27.entity.GasHistoryData;

public class HistoryGasDataVo {
	
	private String timeString;
	private String serial;
	private List<GasHistoryData> ghdList;
	public String getTimeString() {
		return timeString;
	}
	public void setTimeString(String timeString) {
		this.timeString = timeString;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public List<GasHistoryData> getGhdList() {
		return ghdList;
	}
	public void setGhdList(List<GasHistoryData> ghdList) {
		this.ghdList = ghdList;
	}

}
