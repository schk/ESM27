package cn.stronglink.esm27.web.realTimeData.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.esm27.entity.GasHistoryDataResult;
import cn.stronglink.esm27.web.realTimeData.vo.GasHistoryDataResultVo;
import cn.stronglink.esm27.web.realTimeData.vo.HistoryGasDataVo;
import cn.stronglink.esm27.web.realTimeData.vo.HistoryQryParams;

public interface GasHistoryDataResultMapper extends BaseMapper<GasHistoryDataResult> {

	List<GasHistoryDataResultVo> selectByHistoryParams(HistoryQryParams params);

	List<GasHistoryDataResultVo> selectByHistoryParamsByPage(Page<HistoryGasDataVo> page, HistoryQryParams params);

	List<GasHistoryDataResultVo> selectByHistoryParamsByRoomPage(Page<Map> page, HistoryQryParams params);

	void insertByMonth(GasHistoryDataResult g);

	List<GasHistoryDataResultVo> selectByHistoryParamsByPageTwoMonth(Page<HistoryGasDataVo> page,
			HistoryQryParams params);

	List<GasHistoryDataResultVo> selectByHistoryParamsTwoMonth(HistoryQryParams params);

	List<GasHistoryDataResultVo> selectByHistoryParamsNoPage(HistoryQryParams params);

	List<GasHistoryDataResultVo> selectByHistoryParamsTwoMonthNoPage(HistoryQryParams params);

}
