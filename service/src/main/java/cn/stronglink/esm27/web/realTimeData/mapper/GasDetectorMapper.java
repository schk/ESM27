package cn.stronglink.esm27.web.realTimeData.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.GasDetector;

public interface GasDetectorMapper extends BaseMapper<GasDetector> {

	List<String> selectSerialList();

}
