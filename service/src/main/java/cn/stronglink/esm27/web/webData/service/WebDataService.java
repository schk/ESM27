package cn.stronglink.esm27.web.webData.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.AntdFile;
import cn.stronglink.core.util.MapLonLatUtil;
import cn.stronglink.core.util.UploadEntity;
import cn.stronglink.esm27.entity.Accident;
import cn.stronglink.esm27.entity.Department;
import cn.stronglink.esm27.entity.Dictionary;
import cn.stronglink.esm27.entity.EmergencyMaterial;
import cn.stronglink.esm27.entity.Equipment;
import cn.stronglink.esm27.entity.EquipmentType;
import cn.stronglink.esm27.entity.Facilities;
import cn.stronglink.esm27.entity.FireEngine;
import cn.stronglink.esm27.entity.FireEngineUseRecord;
import cn.stronglink.esm27.entity.IncidentRecord;
import cn.stronglink.esm27.entity.KeyUnit;
import cn.stronglink.esm27.entity.KeyUnitDangers;
import cn.stronglink.esm27.entity.MaterialUseRecord;
import cn.stronglink.esm27.entity.Room;
import cn.stronglink.esm27.entity.SocialResource;
import cn.stronglink.esm27.entity.Teams;
import cn.stronglink.esm27.module.danger.danger.mapper.DangersMapper;
import cn.stronglink.esm27.module.danger.danger.vo.DangersVo;
import cn.stronglink.esm27.module.emergencyResources.emergencyMaterial.mapper.MaterialUseRecordMapper;
import cn.stronglink.esm27.module.emergencyResources.reservePoint.vo.ReservePointVo;
import cn.stronglink.esm27.module.equipment.equipment.vo.EquipmentVo;
import cn.stronglink.esm27.module.expert.expert.vo.ExpertTypeVo;
import cn.stronglink.esm27.module.expert.expert.vo.ExpertVo;
import cn.stronglink.esm27.module.facilities.vo.FacilitiesUnitVo;
import cn.stronglink.esm27.module.facilities.vo.FacilitiesVo;
import cn.stronglink.esm27.module.fireEngine.mapper.FireEngineMapper;
import cn.stronglink.esm27.module.fireEngine.mapper.FireEngineUseRecordMapper;
import cn.stronglink.esm27.module.fireEngine.vo.FireEngineEquipmentVo;
import cn.stronglink.esm27.module.fireEngine.vo.FireEngineVo;
import cn.stronglink.esm27.module.fireWaterSource.mapper.FireWaterSourceMapper;
import cn.stronglink.esm27.module.fireWaterSource.vo.FireWaterSourceVo;
import cn.stronglink.esm27.module.fireWaterSource.vo.WaterSourceUnitVo;
import cn.stronglink.esm27.module.incidentRecord.mapper.IncidentRecordMapper;
import cn.stronglink.esm27.module.plan.mapper.PlanMapper;
import cn.stronglink.esm27.module.plan.vo.PlanTypeVo;
import cn.stronglink.esm27.module.plan.vo.PlanVo;
import cn.stronglink.esm27.module.socialResource.mapper.SocialResourceMapper;
import cn.stronglink.esm27.module.system.user.service.UserService;
import cn.stronglink.esm27.module.unit.mapper.KeyPartsMapper;
import cn.stronglink.esm27.module.unit.mapper.KeyUnitDangersMapper;
import cn.stronglink.esm27.module.unit.mapper.KeyUnitImgMapper;
import cn.stronglink.esm27.module.unit.mapper.KeyUnitMapper;
import cn.stronglink.esm27.module.unit.vo.KeyPartsVo;
import cn.stronglink.esm27.web.webData.mapper.WebDataMapper;
import cn.stronglink.esm27.web.webData.mapper.mappers.AccidentMapper;
import cn.stronglink.esm27.web.webData.mapper.mappers.RecordMapper;
import cn.stronglink.esm27.web.webData.mapper.mappers.RoomMapper;
import cn.stronglink.esm27.web.webData.params.IncidentRecordParam;
import cn.stronglink.esm27.web.webData.util.CoordTranform;
import cn.stronglink.esm27.web.webData.vo.EmergencyMaterialStatistics;
import cn.stronglink.esm27.web.webData.vo.FireBridgeVo;
import cn.stronglink.esm27.web.webData.vo.IncidentRecordVo;
import cn.stronglink.esm27.web.webData.vo.MapFeaturesVo;
import cn.stronglink.esm27.web.webData.vo.MapGeometryVo;
import cn.stronglink.esm27.web.webData.vo.MapPropertiesVo;
import cn.stronglink.esm27.web.webData.vo.MapVo;

@Service
@Transactional(rollbackFor = Exception.class)
public class WebDataService {

	@Autowired
	private IncidentRecordMapper incidentRecordMapper;

	@Autowired
	private WebDataMapper webDataMapper;
	@Autowired
	private DangersMapper dangersMapper;
	@Autowired
	private RecordMapper recordMapper;
	@Autowired
	private AccidentMapper accidentMapper;
	@Autowired
	private RoomMapper roomMapper;
	@Autowired
	private KeyUnitMapper keyUnitMapper;
	@Autowired
	private KeyUnitDangersMapper keyUnitDangersMapper;
	@Autowired
	private KeyUnitImgMapper keyUnitImgMapper;
	@Autowired
	private MaterialUseRecordMapper materialUseRecordMapper;
	@Autowired
	private FireEngineUseRecordMapper fireEngineUseRecordMapper;
	@Autowired
	private FireEngineMapper fireEngineMapper;
	@Autowired
	private UserService userService;
	@Autowired
	private PlanMapper planMapper;
	@Autowired
	private KeyPartsMapper keyPartsMapper;
	@Autowired
	private FireWaterSourceMapper fireWaterSourceMapper;
	@Autowired
	private SocialResourceMapper socialResourceMapper;

	MapLonLatUtil mapLonLatUtil = new MapLonLatUtil();

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<Department> qryDepts() {
		return webDataMapper.qryDepts();
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Page<KeyUnit> getListByParams(Page<KeyUnit> page, Map<String, Object> params) {
		List<KeyUnit> list = webDataMapper.getListByParams(page, params);
		if (list != null && list.size() > 0) {
			for (KeyUnit u : list) {
				if (u.getLon() != null && u.getLat()!=null) {
					double[] coord = { Double.valueOf(u.getLon()), Double.valueOf(u.getLat()) };
					double[] point = CoordTranform.lnglat2mercator(coord);
					u.setLon(point[0]);
					u.setLat(point[1]);
				}
			}
		}
		page.setRecords(list);
		return page;
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Page<FireBridgeVo> qryKeyUnitListKuan(Page<FireBridgeVo> page, Map<String, Object> params) {
		List<FireBridgeVo> list = webDataMapper.qryFireBridgeVo(page, params);
		if (list != null && list.size() > 0) {
			for (FireBridgeVo keyUnitVo : list) {
				params.put("fireBrigadeId", keyUnitVo.getFireBrigadeId());
				List<KeyUnit> keyUnitList = webDataMapper.qryKeyUnitByBridgeId(params);
				if (keyUnitList != null && keyUnitList.size() > 0) {
					keyUnitVo.setKeyUnitCount(keyUnitList.size());
					keyUnitVo.setKeyUnitList(keyUnitList);
				}
			}
		}
		page.setRecords(list);
		return page;
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<FireBridgeVo> qryKeyUnitListKuanAll(Map<String, Object> params) {
		List<FireBridgeVo> list = webDataMapper.qryFireBridgeVoAll(params);
		if (list != null && list.size() > 0) {
			for (FireBridgeVo keyUnitVo : list) {
				params.put("fireBrigadeId", keyUnitVo.getFireBrigadeId());
				List<KeyUnit> keyUnitList = webDataMapper.qryKeyUnitByBridgeId(params);
				if (keyUnitList != null && keyUnitList.size() > 0) {
					keyUnitVo.setKeyUnitCount(keyUnitList.size());
					keyUnitVo.setKeyUnitList(keyUnitList);
				}
			}
		}
		return list;
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<KeyUnit> getKeyUnit() {
		return webDataMapper.getKeyUnit();
	}

	// 查询对象
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Page<FireWaterSourceVo> qryFireWaterSource(Page<FireWaterSourceVo> page, Map<String, Object> params) {
		List<FireWaterSourceVo> list = webDataMapper.qryFireWaterSource(page, params);
		if (list != null && list.size() > 0) {
			for (FireWaterSourceVo u : list) {
				if (u.getLon() != null && u.getLat()!=null) {
					double[] coord = { Double.valueOf(u.getLon()), Double.valueOf(u.getLat()) };
					double[] point = CoordTranform.lnglat2mercator(coord);
					u.setLon(point[0]);
					u.setLat(point[1]);
				}
			}
		}
		page.setRecords(list);
		return page;
	}

	// 查询对象
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Page<FacilitiesVo> qryFacilities(Page<FacilitiesVo> page, Map<String, Object> params) {
		List<FacilitiesVo> list = webDataMapper.qryFacilities(page, params);
		if (list != null && list.size() > 0) {
			for (FacilitiesVo u : list) {
				if (u.getLon() != null && u.getLat()!=null) {
					double[] coord = { Double.valueOf(u.getLon()), Double.valueOf(u.getLat()) };
					double[] point = CoordTranform.lnglat2mercator(coord);
					u.setLon(point[0]);
					u.setLat(point[1]);
				}
			}
		}
		page.setRecords(list);
		return page;
	}

	/**
	 * 查询危化品库
	 * 
	 * @param page
	 * @param params
	 * @return
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Page<DangersVo> qryDangerChemic(Page<DangersVo> page, Map<String, Object> params) {
		page.setRecords(webDataMapper.qryDangerChemic(page, params));
		return page;
	}

	/**
	 * 查询应急队伍
	 * 
	 * @param page
	 * @param params
	 * @return
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Page<Teams> qryTeams(Page<Teams> page, Map<String, Object> params) {
		List<Teams> list = webDataMapper.qryTeams(page, params);
		if (list != null && list.size() > 0) {
			for (Teams u : list) {
				if(u.getLon()!=null&&u.getLat()!=null) {
					double[] coord = { Double.valueOf(u.getLon()), Double.valueOf(u.getLat()) };
					double[] point = CoordTranform.lnglat2mercator(coord);
					u.setLon(point[0]);
					u.setLat(point[1]);
				}
			}
		}
		page.setRecords(list);
		return page;
	}

	/**
	 * 查询物资储备点
	 * 
	 * @param page
	 * @param params
	 * @return
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Page<ReservePointVo> qryReservePoint(Page<ReservePointVo> page, Map<String, Object> params) {
		List<ReservePointVo> list = webDataMapper.qryReservePoint(page, params);
		if (list != null && list.size() > 0) {
			for (ReservePointVo u : list) {
				if (u.getLon() != null && u.getLat()!=null) {
					double[] coord = { Double.valueOf(u.getLon()), Double.valueOf(u.getLat()) };
					double[] point = CoordTranform.lnglat2mercator(coord);
					u.setLon(point[0]);
					u.setLat(point[1]);
				}
			}
		}
		page.setRecords(list);
		return page;
	}

	/**
	 * 查询专家类型
	 * 
	 * @return
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<ExpertTypeVo> qryExpertType() {
		Map<String, Object> params = new HashMap<String, Object>();
		return webDataMapper.qryExpertType(params);
	}

	// 查询所有专家信息
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<ExpertTypeVo> qryExpertList(Map<String, Object> params) {
		List<ExpertTypeVo> typeList = webDataMapper.qryExpertType(params);
		if (typeList != null && typeList.size() > 0) {
			for (ExpertTypeVo type : typeList) {
				params.put("typeId", type.getId());
				List<ExpertVo> expertList = webDataMapper.qryExpertList(params);
				if (expertList != null && expertList.size() > 0) {
					type.setExpertCout(expertList.size());
					type.setExpertList(expertList);
				}
			}
		}
		return typeList;
	}

	/**
	 * 查询应急预案
	 * 
	 * @param page
	 * @param page
	 * @param params
	 * @return
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Page<PlanTypeVo> qryPlans(Page<PlanTypeVo> page, Map<String, Object> params) {
		// List<PlanTypeVo> typeList = webDataMapper.qryPlansType(page,params);
		List<PlanTypeVo> typeList = webDataMapper.qryPlansUnit(page, params);
		if (typeList != null && typeList.size() > 0) {
			for (PlanTypeVo type : typeList) {
				params.put("typeId", params.get("typeId").toString());
				params.put("writingUnit", type.getId());
				List<PlanVo> planList = webDataMapper.qryPlans(params);
				if (planList != null && planList.size() > 0) {
					type.setPlanCount(planList.size());
					type.setPlanList(planList);
				}
			}
		}
		page.setRecords(typeList);
		return page;
	}

	/**
	 * 查询生产装置
	 * 
	 * @param page
	 * @param params
	 * @return
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Page<EquipmentVo> qryEquipmentList(Page<EquipmentVo> page, Map<String, Object> params) {
		List<EquipmentVo> list = webDataMapper.qryEquipmentList(page, params);
		if (list != null && list.size() > 0) {
			for (EquipmentVo u : list) {
				if (u.getLon() != null && u.getLat() != null) {
					double[] coord = { Double.valueOf(u.getLon()), Double.valueOf(u.getLat()) };
					double[] point = CoordTranform.lnglat2mercator(coord);
					u.setLon(point[0]);
					u.setLat(point[1]);
				}
			}
		}
		page.setRecords(list);
		return page;
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<Dictionary> qryDictionaryByType(int type) {
		return webDataMapper.qryDictionaryByType(type);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<EquipmentType> qryEquipmentTypeList(int type) {
		return webDataMapper.qryEquipmentTypeList(type);
	}

	/**
	 * 查询地图数据
	 * 
	 * @param cate
	 * @param extent
	 * @param callback
	 * @return
	 */
	public MapVo qryDataForMap(String cateString, String extentString, String callback) {
		MapVo mapvo = new MapVo();
		List<MapFeaturesVo> mapFeaturesVo = new ArrayList<MapFeaturesVo>();
		HashMap<String, Object> param = new HashMap<>();
		mapvo.setType("FeatureCollection");
		if (cateString.contains("zddw")) {
			List<Map<String, Object>> list = webDataMapper.qryDataForMapZddw(param);
			if (list != null && list.size() > 0) {
				for (Map<String, Object> map : list) {
					if (map != null) {
						if (!"".equals(map.get("lon")) && !"".equals(map.get("lat")) && map.get("lon") != null
								&& map.get("lat") != null) {
							double[] coord = { Double.valueOf(map.get("lon").toString()),
									Double.valueOf(map.get("lat").toString()) };
							// double[] pointwgs =MapLonLatUtil.bd09py(coord[0],coord[1]);
							double[] point = CoordTranform.lnglat2mercator(coord);
							MapGeometryVo geometry = new MapGeometryVo();
							List<Object> coordinates = new ArrayList<Object>();
							coordinates.add(point[0]);
							coordinates.add(point[1]);
							geometry.setCoordinates(coordinates);
							geometry.setType("Point");

							MapPropertiesVo properties = new MapPropertiesVo();
							properties.setCatecd("zddw");
							properties.setPoint(point[0] + "_" + point[1]);
							properties.setId(map.get("id").toString());
							properties.setName(map.get("name").toString());

							MapFeaturesVo features = new MapFeaturesVo();
							features.setGeometry(geometry);
							features.setProperties(properties);
							features.setType("Feature");
							features.setId(map.get("id").toString());
							mapFeaturesVo.add(features);
						}
					}
				}
			}
		}
		if (cateString.contains("xfsy")) {
			List<Map<String, Object>> list = webDataMapper.qryDataForMapXfsy(param);
			if (list != null && list.size() > 0) {
				for (Map<String, Object> map : list) {
					if (map != null) {
						if (!"".equals(map.get("lon")) && !"".equals(map.get("lat")) && map.get("lon") != null
								&& map.get("lat") != null) {
							double[] coord = { Double.valueOf(map.get("lon").toString()),
									Double.valueOf(map.get("lat").toString()) };
							// double[] pointwgs =MapLonLatUtil.bd09py(coord[0],coord[1]);
							double[] point = CoordTranform.lnglat2mercator(coord);
							MapGeometryVo geometry = new MapGeometryVo();
							List<Object> coordinates = new ArrayList<Object>();
							coordinates.add(point[0]);
							coordinates.add(point[1]);
							geometry.setCoordinates(coordinates);
							geometry.setType("Point");

							MapPropertiesVo properties = new MapPropertiesVo();
							properties.setCatecd("xfsy");
							properties.setPoint(point[0] + "_" + point[1]);
							properties.setId(map.get("id").toString());
							properties.setName(map.get("name").toString());

							MapFeaturesVo features = new MapFeaturesVo();
							features.setGeometry(geometry);
							features.setProperties(properties);
							features.setType("Feature");
							features.setId(map.get("id").toString());
							mapFeaturesVo.add(features);
						}
					}
				}
			}
		}
		if (cateString.contains("xfs")) {
			List<Map<String, Object>> sheshiList = webDataMapper.qryDataForMapXfsS(param);
			if (sheshiList != null && sheshiList.size() > 0) {
				for (Map<String, Object> map : sheshiList) {
					if (map != null) {
						if (!"".equals(map.get("lon")) && !"".equals(map.get("lat")) && map.get("lon") != null
								&& map.get("lat") != null) {
							double[] coord = { Double.valueOf(map.get("lon").toString()),
									Double.valueOf(map.get("lat").toString()) };
//    						double[] pointwgs =MapLonLatUtil.bd09towgs84(coord[0],coord[1]);
							// double[] pointwgs =MapLonLatUtil.bd09py(coord[0],coord[1]);
							double[] point = CoordTranform.lnglat2mercator(coord);
							MapGeometryVo geometry = new MapGeometryVo();
							List<Object> coordinates = new ArrayList<Object>();
							coordinates.add(point[0]);
							coordinates.add(point[1]);
							geometry.setCoordinates(coordinates);
							geometry.setType("Point");

							MapPropertiesVo properties = new MapPropertiesVo();
							properties.setCatecd("xfs");
							properties.setPoint(point[0] + "_" + point[1]);
							properties.setId(map.get("id").toString());
							properties.setName(map.get("name").toString());

							MapFeaturesVo features = new MapFeaturesVo();
							features.setGeometry(geometry);
							features.setProperties(properties);
							features.setType("Feature");
							features.setId(map.get("id").toString());
							mapFeaturesVo.add(features);
						}
					}
				}
			}
		}
		if (cateString.contains("yjdw")) {
			List<Map<String, Object>> list = webDataMapper.qryDataForMapYjdw(param);
			if (list != null && list.size() > 0) {
				for (Map<String, Object> map : list) {
					if (map != null) {
						if (!"".equals(map.get("lon")) && !"".equals(map.get("lat")) && map.get("lon") != null
								&& map.get("lat") != null) {
							double[] coord = { Double.valueOf(map.get("lon").toString()),
									Double.valueOf(map.get("lat").toString()) };
							// double[] pointwgs =MapLonLatUtil.bd09py(coord[0],coord[1]);
							double[] point = CoordTranform.lnglat2mercator(coord);
							MapGeometryVo geometry = new MapGeometryVo();
							List<Object> coordinates = new ArrayList<Object>();
							coordinates.add(point[0]);
							coordinates.add(point[1]);
							geometry.setCoordinates(coordinates);
							geometry.setType("Point");

							MapPropertiesVo properties = new MapPropertiesVo();
							properties.setCatecd("yjdw");
							properties.setPoint(point[0] + "_" + point[1]);
							properties.setId(map.get("id").toString());
							properties.setName(map.get("name").toString());

							MapFeaturesVo features = new MapFeaturesVo();
							features.setGeometry(geometry);
							features.setProperties(properties);
							features.setType("Feature");
							features.setId(map.get("id").toString());
							mapFeaturesVo.add(features);
						}
					}
				}
			}
		}
		if (cateString.contains("xfdw")) {
			List<Map<String, Object>> list = webDataMapper.qryDataForMapXfdw(param);
			if (list != null && list.size() > 0) {
				for (Map<String, Object> map : list) {
					if (map != null) {
						if (!"".equals(map.get("lon")) && !"".equals(map.get("lat")) && map.get("lon") != null
								&& map.get("lat") != null) {
							double[] coord = { Double.valueOf(map.get("lon").toString()),
									Double.valueOf(map.get("lat").toString()) };
							// double[] pointwgs =MapLonLatUtil.bd09py(coord[0],coord[1]);
							double[] point = CoordTranform.lnglat2mercator(coord);
							MapGeometryVo geometry = new MapGeometryVo();
							List<Object> coordinates = new ArrayList<Object>();
							coordinates.add(point[0]);
							coordinates.add(point[1]);
							geometry.setCoordinates(coordinates);
							geometry.setType("Point");

							MapPropertiesVo properties = new MapPropertiesVo();
							properties.setCatecd("xfdw");
							properties.setPoint(point[0] + "_" + point[1]);
							properties.setId(map.get("id").toString());
							properties.setName(map.get("name").toString());

							MapFeaturesVo features = new MapFeaturesVo();
							features.setGeometry(geometry);
							features.setProperties(properties);
							features.setType("Feature");
							features.setId(map.get("id").toString());
							mapFeaturesVo.add(features);
						}
					}
				}
			}
		}

		if (cateString.contains("sczz")) {
			List<Map<String, Object>> list = webDataMapper.qryDataForMapSczz(param);
			if (list != null && list.size() > 0) {
				for (Map<String, Object> map : list) {
					if (map != null) {
						if (!"".equals(map.get("lon")) && !"".equals(map.get("lat")) && map.get("lon") != null
								&& map.get("lat") != null) {
							double[] coord = { Double.valueOf(map.get("lon").toString()),
									Double.valueOf(map.get("lat").toString()) };
							// double[] pointwgs =MapLonLatUtil.bd09py(coord[0],coord[1]);
							double[] point = CoordTranform.lnglat2mercator(coord);
							MapGeometryVo geometry = new MapGeometryVo();
							List<Object> coordinates = new ArrayList<Object>();
							coordinates.add(point[0]);
							coordinates.add(point[1]);
							geometry.setCoordinates(coordinates);
							geometry.setType("Point");

							MapPropertiesVo properties = new MapPropertiesVo();
							properties.setCatecd("sczz");
							properties.setPoint(point[0] + "_" + point[1]);
							properties.setId(map.get("id").toString());
							properties.setName(map.get("name").toString());

							MapFeaturesVo features = new MapFeaturesVo();
							features.setGeometry(geometry);
							features.setProperties(properties);
							features.setType("Feature");
							features.setId(map.get("id").toString());
							mapFeaturesVo.add(features);
						}
					}
				}
			}
		}

		if (cateString.contains("wzcbd")) {
			List<Map<String, Object>> list = webDataMapper.qryDataForMapWzcbd(param);
			if (list != null && list.size() > 0) {
				for (Map<String, Object> map : list) {
					if (map != null) {
						if (!"".equals(map.get("lon")) && !"".equals(map.get("lat")) && map.get("lon") != null
								&& map.get("lat") != null) {
							double[] coord = { Double.valueOf(map.get("lon").toString()),
									Double.valueOf(map.get("lat").toString()) };
							// double[] pointwgs =MapLonLatUtil.bd09py(coord[0],coord[1]);
							double[] point = CoordTranform.lnglat2mercator(coord);
							MapGeometryVo geometry = new MapGeometryVo();
							List<Object> coordinates = new ArrayList<Object>();
							coordinates.add(point[0]);
							coordinates.add(point[1]);
							geometry.setCoordinates(coordinates);
							geometry.setType("Point");

							MapPropertiesVo properties = new MapPropertiesVo();
							properties.setCatecd("wzcbd");
							properties.setPoint(point[0] + "_" + point[1]);
							properties.setId(map.get("id").toString());
							properties.setName(map.get("name").toString());

							MapFeaturesVo features = new MapFeaturesVo();
							features.setGeometry(geometry);
							features.setProperties(properties);
							features.setType("Feature");
							features.setId(map.get("id").toString());
							mapFeaturesVo.add(features);
						}
					}
				}
			}
		}
		if (cateString.contains("shzy")) {
			List<Map<String, Object>> list = webDataMapper.qryDataForMapShzy(param);
			if (list != null && list.size() > 0) {
				for (Map<String, Object> map : list) {
					if (map != null) {
						if (!"".equals(map.get("lon")) && !"".equals(map.get("lat")) && map.get("lon") != null
								&& map.get("lat") != null) {
							double[] coord = { Double.valueOf(map.get("lon").toString()),
									Double.valueOf(map.get("lat").toString()) };
							// double[] pointwgs =MapLonLatUtil.bd09py(coord[0],coord[1]);
							double[] point = CoordTranform.lnglat2mercator(coord);
							MapGeometryVo geometry = new MapGeometryVo();
							List<Object> coordinates = new ArrayList<Object>();
							coordinates.add(point[0]);
							coordinates.add(point[1]);
							geometry.setCoordinates(coordinates);
							geometry.setType("Point");

							MapPropertiesVo properties = new MapPropertiesVo();
							properties.setCatecd("shzy" + map.get("equipmentType").toString());
							properties.setPoint(point[0] + "_" + point[1]);
							properties.setId(map.get("id").toString());
							properties.setName(map.get("name").toString());
							if (map.get("dtPath") != null) {
								properties.setDtPath(map.get("dtPath").toString());
							}
							if (map.get("jhPath") != null) {
								properties.setJhPath(map.get("jhPath").toString());
							}

							MapFeaturesVo features = new MapFeaturesVo();
							features.setGeometry(geometry);
							features.setProperties(properties);
							features.setType("Feature");
							features.setId(map.get("id").toString());
							mapFeaturesVo.add(features);
						}
					}
				}
			}
		}
		mapvo.setFeatures(mapFeaturesVo);
		return mapvo;
	}

	/**
	 * 空间搜索
	 * 
	 * @param cate
	 * @param extent
	 * @param callback
	 * @return
	 */
	public MapVo qryDataForMapByGeom(String cateString, Map<String, Object> param) {
		MapVo mapvo = new MapVo();
		List<MapFeaturesVo> mapFeaturesVo = new ArrayList<MapFeaturesVo>();
		mapvo.setType("FeatureCollection");
		if (cateString.contains("zddw")) {
			List<Map<String, Object>> list = webDataMapper.qryDataForMapZddw(param);
			if (list != null && list.size() > 0) {
				for (Map<String, Object> map : list) {
					if (map != null) {
						if (!"".equals(map.get("lon")) && !"".equals(map.get("lat")) && map.get("lon") != null
								&& map.get("lat") != null) {
							double[] coord = { Double.valueOf(map.get("lon").toString()),
									Double.valueOf(map.get("lat").toString()) };
//						double[] pointwgs =MapLonLatUtil.bd09towgs84(coord[0],coord[1]);
							// double[] pointwgs =MapLonLatUtil.bd09py(coord[0],coord[1]);
							double[] point = CoordTranform.lnglat2mercator(coord);
							MapGeometryVo geometry = new MapGeometryVo();
							List<Object> coordinates = new ArrayList<Object>();
							coordinates.add(point[0]);
							coordinates.add(point[1]);
							geometry.setCoordinates(coordinates);
							geometry.setType("Point");

							MapPropertiesVo properties = new MapPropertiesVo();
							properties.setCatecd("zddw");
							properties.setPoint(point[0] + "_" + point[1]);
							properties.setId(map.get("id").toString());
							properties.setName(map.get("name").toString());

							MapFeaturesVo features = new MapFeaturesVo();
							features.setGeometry(geometry);
							features.setProperties(properties);
							features.setType("Feature");
							features.setId(map.get("id").toString());
							mapFeaturesVo.add(features);
						}
					}
				}
			}
		}
		if (cateString.contains("xfsy")) {
			List<Map<String, Object>> list = webDataMapper.qryDataForMapXfsy(param);
			if (list != null && list.size() > 0) {
				for (Map<String, Object> map : list) {
					if (map != null) {
						if (!"".equals(map.get("lon")) && !"".equals(map.get("lat")) && map.get("lon") != null
								&& map.get("lat") != null) {
							double[] coord = { Double.valueOf(map.get("lon").toString()),
									Double.valueOf(map.get("lat").toString()) };
//						double[] pointwgs =MapLonLatUtil.bd09towgs84(coord[0],coord[1]);
							// double[] pointwgs =MapLonLatUtil.bd09py(coord[0],coord[1]);
							double[] point = CoordTranform.lnglat2mercator(coord);
							MapGeometryVo geometry = new MapGeometryVo();
							List<Object> coordinates = new ArrayList<Object>();
							coordinates.add(point[0]);
							coordinates.add(point[1]);
							geometry.setCoordinates(coordinates);
							geometry.setType("Point");

							MapPropertiesVo properties = new MapPropertiesVo();
							properties.setCatecd("xfsy");
							properties.setPoint(point[0] + "_" + point[1]);
							properties.setId(map.get("id").toString());
							properties.setName(map.get("name").toString());

							MapFeaturesVo features = new MapFeaturesVo();
							features.setGeometry(geometry);
							features.setProperties(properties);
							features.setType("Feature");
							features.setId(map.get("id").toString());
							mapFeaturesVo.add(features);
						}
					}
				}
			}
		}
		if (cateString.contains("xfs")) {
			List<Map<String, Object>> sheshiList = webDataMapper.qryDataForMapXfsS(param);
			if (sheshiList != null && sheshiList.size() > 0) {
				for (Map<String, Object> map : sheshiList) {
					if (map != null) {
						if (!"".equals(map.get("lon")) && !"".equals(map.get("lat")) && map.get("lon") != null
								&& map.get("lat") != null) {
							double[] coord = { Double.valueOf(map.get("lon").toString()),
									Double.valueOf(map.get("lat").toString()) };
//						double[] pointwgs =MapLonLatUtil.bd09towgs84(coord[0],coord[1]);
							// double[] pointwgs =MapLonLatUtil.bd09py(coord[0],coord[1]);
							double[] point = CoordTranform.lnglat2mercator(coord);
							MapGeometryVo geometry = new MapGeometryVo();
							List<Object> coordinates = new ArrayList<Object>();
							coordinates.add(point[0]);
							coordinates.add(point[1]);
							geometry.setCoordinates(coordinates);
							geometry.setType("Point");

							MapPropertiesVo properties = new MapPropertiesVo();
							properties.setCatecd("xfs");
							properties.setPoint(point[0] + "_" + point[1]);
							properties.setId(map.get("id").toString());
							properties.setName(map.get("name").toString());

							MapFeaturesVo features = new MapFeaturesVo();
							features.setGeometry(geometry);
							features.setProperties(properties);
							features.setType("Feature");
							features.setId(map.get("id").toString());
							mapFeaturesVo.add(features);
						}
					}
				}
			}
		}
		if (cateString.contains("yjdw")) {
			List<Map<String, Object>> list = webDataMapper.qryDataForMapYjdw(param);
			if (list != null && list.size() > 0) {
				for (Map<String, Object> map : list) {
					if (map != null) {
						if (!"".equals(map.get("lon")) && !"".equals(map.get("lat")) && map.get("lon") != null
								&& map.get("lat") != null) {
							double[] coord = { Double.valueOf(map.get("lon").toString()),
									Double.valueOf(map.get("lat").toString()) };
//							double[] pointwgs =MapLonLatUtil.bd09towgs84(coord[0],coord[1]);
							// double[] pointwgs =MapLonLatUtil.bd09py(coord[0],coord[1]);
							double[] point = CoordTranform.lnglat2mercator(coord);
							MapGeometryVo geometry = new MapGeometryVo();
							List<Object> coordinates = new ArrayList<Object>();
							coordinates.add(point[0]);
							coordinates.add(point[1]);
							geometry.setCoordinates(coordinates);
							geometry.setType("Point");

							MapPropertiesVo properties = new MapPropertiesVo();
							properties.setCatecd("yjdw");
							properties.setPoint(point[0] + "_" + point[1]);
							properties.setId(map.get("id").toString());
							properties.setName(map.get("name").toString());

							MapFeaturesVo features = new MapFeaturesVo();
							features.setGeometry(geometry);
							features.setProperties(properties);
							features.setType("Feature");
							features.setId(map.get("id").toString());
							mapFeaturesVo.add(features);
						}
					}
				}
			}
		}
		if (cateString.contains("xfdw")) {
			List<Map<String, Object>> list = webDataMapper.qryDataForMapXfdw(param);
			if (list != null && list.size() > 0) {
				for (Map<String, Object> map : list) {
					if (map != null) {
						if (!"".equals(map.get("lon")) && !"".equals(map.get("lat")) && map.get("lon") != null
								&& map.get("lat") != null) {
							double[] coord = { Double.valueOf(map.get("lon").toString()),
									Double.valueOf(map.get("lat").toString()) };
//							double[] pointwgs =MapLonLatUtil.bd09towgs84(coord[0],coord[1]);
							// double[] pointwgs =MapLonLatUtil.bd09py(coord[0],coord[1]);
							double[] point = CoordTranform.lnglat2mercator(coord);
							MapGeometryVo geometry = new MapGeometryVo();
							List<Object> coordinates = new ArrayList<Object>();
							coordinates.add(point[0]);
							coordinates.add(point[1]);
							geometry.setCoordinates(coordinates);
							geometry.setType("Point");

							MapPropertiesVo properties = new MapPropertiesVo();
							properties.setCatecd("xfdw");
							properties.setPoint(point[0] + "_" + point[1]);
							properties.setId(map.get("id").toString());
							properties.setName(map.get("name").toString());

							MapFeaturesVo features = new MapFeaturesVo();
							features.setGeometry(geometry);
							features.setProperties(properties);
							features.setType("Feature");
							features.setId(map.get("id").toString());
							mapFeaturesVo.add(features);
						}
					}
				}
			}
		}
		if (cateString.contains("sczz")) {
			List<Map<String, Object>> list = webDataMapper.qryDataForMapSczz(param);
			if (list != null && list.size() > 0) {
				for (Map<String, Object> map : list) {
					if (map != null) {
						if (!"".equals(map.get("lon")) && !"".equals(map.get("lat")) && map.get("lon") != null
								&& map.get("lat") != null) {
							double[] coord = { Double.valueOf(map.get("lon").toString()),
									Double.valueOf(map.get("lat").toString()) };
//							double[] pointwgs =MapLonLatUtil.bd09towgs84(coord[0],coord[1]);
							// double[] pointwgs =MapLonLatUtil.bd09py(coord[0],coord[1]);
							double[] point = CoordTranform.lnglat2mercator(coord);
							MapGeometryVo geometry = new MapGeometryVo();
							List<Object> coordinates = new ArrayList<Object>();
							coordinates.add(point[0]);
							coordinates.add(point[1]);
							geometry.setCoordinates(coordinates);
							geometry.setType("Point");

							MapPropertiesVo properties = new MapPropertiesVo();
							properties.setCatecd("yjzb");
							properties.setPoint(point[0] + "_" + point[1]);
							properties.setId(map.get("id").toString());
							properties.setName(map.get("name").toString());

							MapFeaturesVo features = new MapFeaturesVo();
							features.setGeometry(geometry);
							features.setProperties(properties);
							features.setType("Feature");
							features.setId(map.get("id").toString());
							mapFeaturesVo.add(features);
						}
					}
				}
			}
		}
		if (cateString.contains("wzcbd")) {
			try {
				List<Map<String, Object>> list = webDataMapper.qryDataForMapWzcbd(param);
				if (list != null && list.size() > 0) {
					for (Map<String, Object> map : list) {
						if (map != null) {
							if (!"".equals(map.get("lon")) && !"".equals(map.get("lat")) && map.get("lon") != null
									&& map.get("lat") != null) {
								double[] coord = { Double.valueOf(map.get("lon").toString()),
										Double.valueOf(map.get("lat").toString()) };
//	    						double[] pointwgs =MapLonLatUtil.bd09towgs84(coord[0],coord[1]);
								// double[] pointwgs =MapLonLatUtil.bd09py(coord[0],coord[1]);
								double[] point = CoordTranform.lnglat2mercator(coord);
								MapGeometryVo geometry = new MapGeometryVo();
								List<Object> coordinates = new ArrayList<Object>();
								coordinates.add(point[0]);
								coordinates.add(point[1]);
								geometry.setCoordinates(coordinates);
								geometry.setType("Point");

								MapPropertiesVo properties = new MapPropertiesVo();
								properties.setCatecd("wzcbd");
								properties.setPoint(point[0] + "_" + point[1]);
								properties.setId(map.get("id").toString());
								properties.setName(map.get("name").toString());

								MapFeaturesVo features = new MapFeaturesVo();
								features.setGeometry(geometry);
								features.setProperties(properties);
								features.setType("Feature");
								features.setId(map.get("id").toString());
								mapFeaturesVo.add(features);
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		mapvo.setFeatures(mapFeaturesVo);
		return mapvo;
	}

	public Page<FireEngineVo> qryFireEngine(Page<FireEngineVo> page, Map<String, Object> params) {
		page.setRecords(webDataMapper.qryFireEngine(page, params));
		return page;
	}

	public void insertIncidentRecord(IncidentRecordParam record) {
		record.setCreateTime(new Date());
		// 查询字表信息
		List<IncidentRecord> qryTimeDifference = roomMapper.qryTimeDifference(record.getRoomId());
		if (qryTimeDifference.size() > 0) {
			// 计算此房间距离上次记录时间
			long timeDiff = record.getCreateTime().getTime() - qryTimeDifference.get(0).getCreateTime().getTime();
			long min = timeDiff / 1000;
			record.setTimeDifference(String.valueOf(min));
		}
		recordMapper.insert(record);
		// iconType == 4 添加一个消防车的使用数量记录，并修改消防车的状态为在用,这个type也是type
		if (record.getType() != null && record.getType() == 8) {
			FireEngineUseRecord entity = new FireEngineUseRecord();
			entity.setCount(1);
			entity.setRoomId(record.getRoomId());
			entity.setCreateTime(new Date());
			entity.setId(IdWorker.getId());
			entity.setFireEngineId(record.getFireEngineId());
			fireEngineUseRecordMapper.insert(entity);
			FireEngine en = new FireEngine();
			en.setId(record.getFireEngineId());
			en.setStatus(1);
			fireEngineMapper.updateById(en);
		}
	}

	public void updateIncidentRecord(IncidentRecordParam record) {
		record.setCreateTime(new Date());
		if (recordMapper.updateById(record) == 0) {
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
	}

	public void insertAccident(Accident accident) {
		accident.setId(IdWorker.getId());
		accident.setCreateTime(new Date());
		accidentMapper.insert(accident);
	}

	public List<Accident> getAccidentByRoomid(String roomId) {
		return accidentMapper.getAccidentByRoomid(roomId);
	}

	public List<IncidentRecord> getIncidentRecordByAccident(Long id) {
		return recordMapper.selectList(new EntityWrapper<IncidentRecord>().eq("room_id", id));
	}

	public void updateAccident(Accident accident) {
		accident.setCreateTime(new Date());
		accidentMapper.updateById(accident);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<FireEngineVo> qryFireEngineByDept(Map<String, Object> params) {
		return webDataMapper.qryFireEngineByDept(params);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<FireEngineEquipmentVo> qryFireEngineEquipmentByfId(Long id) {
		return webDataMapper.qryFireEngineEquipmentByfId(id);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<EmergencyMaterial> qryReservePointDetail(Long pointId) {
		return webDataMapper.qryReservePointDetail(pointId);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<IncidentRecord> getIncidentRecordDetail(IncidentRecord incidentRecord) {
		return incidentRecordMapper.getIncidentRecordDetail(incidentRecord);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public KeyUnit qryKeyUnitById(Long id) {
		KeyUnit keyUnit = keyUnitMapper.qryById(id);
		if (keyUnit != null) {
			List<KeyUnitDangers> dangersList = keyUnitDangersMapper.qryKeyUnitDangersById(id);
			keyUnit.setDangersList(dangersList);
			List<UploadEntity> imgList = keyUnitImgMapper.getImgList(id);
			keyUnit.setFileList(imgList);
			List<PlanVo> planList = planMapper.qryPlansByUnit(keyUnit.getId());
			keyUnit.setPlanList(planList);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("keyUnitId", keyUnit.getId());
			List<KeyPartsVo> partsList = keyPartsMapper.getPartsByUnit(params);
			keyUnit.setKeyPartsList(partsList);
			List<AntdFile> fileList = keyUnitMapper.qryAccidentByUnitId(keyUnit.getId());
			keyUnit.setAccidentFileList(fileList);

		}
		return keyUnit;
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public KeyUnitDangers qryKeyUnitDangersDetailById(Long id) {
		KeyUnitDangers info = keyUnitDangersMapper.qryKeyUnitDangersDetailById(id);
		return info;
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Facilities qryFacilitiesById(Long id) {
		return webDataMapper.qryFacilitiesById(id);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Teams qryTeamsById(Long id) {
		return webDataMapper.qryTeamsById(id);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Long getUserBrigadeId(Long id) {
		Long brigadeId = userService.selectUserBrigade(id);
		return brigadeId;
	}

	public void deleteIncidentRecord(Long id) {
		recordMapper.deleteById(id);
	}

	public void insertRoom(Room room) {
		room.setCreateTime(new Date());
		room.setUpdateTime(new Date());
		roomMapper.insert(room);
	}

	public void RoomChangeLeaders(Room room) {
		Room r = new Room();
		r.setId(room.getId());
		r.setAccidentConductor(room.getAccidentConductor());
		r.setUpdateTime(new Date());
		roomMapper.updateById(r);
	}

	public void createRoom(Room room) {
		room.setCreateTime(new Date());
		room.setUpdateTime(new Date());
		roomMapper.insert(room);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Equipment qryEquipmentById(Long id) {
		return webDataMapper.qryEquipmentById(id);
	}

	public List<IncidentRecordVo> getIncidentRecordByRoomId(String id) {
		List<IncidentRecordVo> incidentRecordByRoomId = incidentRecordMapper.getIncidentRecordByRoomId(id);
		return incidentRecordByRoomId;
	}

	public void updateEmergencyMaterial(Long id, Integer storageQuantity, String roomId) {
		webDataMapper.updateEmergencyMaterial(id, storageQuantity);
		MaterialUseRecord record = new MaterialUseRecord();
		record.setId(IdWorker.getId());
		record.setCount(storageQuantity);
		record.setCreateTime(new Date());
		record.setUpdateTime(new Date());
		record.setMaterialId(id);
		record.setRoomId(id);
		materialUseRecordMapper.insert(record);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Room getRoomInfo(String roomCode) {
		Room r = new Room();
		r.setRoom(roomCode);
		r.setStatus(1);
		return roomMapper.selectOne(r);
	}

	public Room getRoomByid(long id) {
		Room r = new Room();
		r.setId(id);
		;
		r.setStatus(1);
		return roomMapper.selectOne(r);
	}

	public void updateRoom(Long roomId) {
		Room r = new Room();
		r.setId(roomId);
		r.setStatus(2);
		r.setUpdateTime(new Date());
		roomMapper.updateById(r);

	}

	public Page<FireBridgeVo> qryFireEngineByFireBridge(Page<FireBridgeVo> page, Map<String, Object> params) {
		List<FireBridgeVo> list = webDataMapper.qryFireBridgeForCar(page, params);
		if (list != null && list.size() > 0) {
			for (FireBridgeVo fireBride : list) {
				params.put("fireBrigadeId", fireBride.getFireBrigadeId());
				List<FireEngineVo> fireEngineList = webDataMapper.getFireEngineByBridgeId(params);
				if (fireEngineList != null && fireEngineList.size() > 0) {
					fireBride.setFireEngineCount(fireEngineList.size());
					fireBride.setFireEngineList(fireEngineList);
					Map<String, Object> water = webDataMapper.getWaterCount(fireBride.getFireBrigadeId());
					if (water != null) {
						fireBride.setWaterCount(new BigDecimal(water.get("waterCount").toString()));
						fireBride.setFoamCount(new BigDecimal(water.get("capacityCount").toString()));
						fireBride.setPowderCount(new BigDecimal(water.get("powderQuantity").toString()));
					}
				}
			}
		}
		page.setRecords(list);
		return page;
	}

	public Page<ExpertVo> qryExpertListPage(Page<ExpertVo> page, Map<String, Object> params) {
		page.setRecords(webDataMapper.qryExpertListPage(page, params));
		return page;
	}

	public Page<PlanVo> qryPlansPage(Page<PlanVo> page, Map<String, Object> params) {
		page.setRecords(webDataMapper.qryPlansPage(page, params));
		return page;
	}

	/**
	 * 更新任务状态
	 * 
	 * @param id
	 */
	public void updateRenWuStatus(IncidentRecord record) {
		record.setCreateTime(new Date());
		roomMapper.updateRenWuStatus(record);

	}

	public DangersVo qryDangersById(Long id) {
		return dangersMapper.queryDangersById(id);
	}

	public List<EmergencyMaterial> qryWuziListAll(Map<String, Object> params) {
		List<EmergencyMaterial> emergencyMaterialList = webDataMapper.qryWuziListAll(params);
		return emergencyMaterialList;
	}

	public Page<EmergencyMaterial> qryWuziListByPage(Page<EmergencyMaterial> page, Map<String, Object> params) {
		page.setRecords(webDataMapper.qryWuziListByPage(page, params));
		return page;
	}

	public FireWaterSourceVo qryWaterSourseById(Long id) {
		return fireWaterSourceMapper.qryById(id);
	}

	public Page<WaterSourceUnitVo> qryFireWaterSourceGroupKeyUnit(Page<WaterSourceUnitVo> page,
			Map<String, Object> params) {
		List<WaterSourceUnitVo> list = webDataMapper.qryKeyUnitForWaterSource(page, params);
		if (list != null && list.size() > 0) {
			for (WaterSourceUnitVo waterSource : list) {
				params.put("keyUnitId", waterSource.getKeyUnitId());
				List<FireWaterSourceVo> fireEngineList = webDataMapper.getWaterSourceByKeyUnit(params);
				if (fireEngineList != null && fireEngineList.size() > 0) {
					waterSource.setFireWaterSourceCount(fireEngineList.size());
					waterSource.setFireWaterSourceList(fireEngineList);
				}
			}
		}
		page.setRecords(list);
		return page;
	}

	public Page<FacilitiesUnitVo> qryFacilitiesGroupKeyUnit(Page<FacilitiesUnitVo> page, Map<String, Object> params) {
		List<FacilitiesUnitVo> list = webDataMapper.qryKeyUnitForFacilities(page, params);
		if (list != null && list.size() > 0) {
			for (FacilitiesUnitVo waterSource : list) {
				params.put("keyUnitId", waterSource.getKeyUnitId());
				List<FacilitiesVo> fireEngineList = webDataMapper.getFacilitiesByKeyUnit(params);
				if (fireEngineList != null && fireEngineList.size() > 0) {
					waterSource.setFireWaterSourceCount(fireEngineList.size());
					waterSource.setFireWaterSourceList(fireEngineList);
				}
			}
		}
		page.setRecords(list);
		return page;
	}

	public void endRescue(Map<String, Object> params) {
		webDataMapper.endRescue(params);
	}

	public Integer checkEndRescue(Map<String, Object> params) {
		return webDataMapper.checkEndRescue(params);
	}

	public List<EmergencyMaterialStatistics> qryEmergencyMaterialStatistics(Map<String, Object> params) {
		List<EmergencyMaterialStatistics> list = webDataMapper.qryEmergencyMaterialStatistics(params);
		if (list != null && list.size() > 0) {
			for (EmergencyMaterialStatistics ems : list) {
				params.put("materialName", ems.getMaterialName());
				List<ReservePointVo> reservePointList = webDataMapper.qryZhntjReservePoint(params);
				ems.setReservePointList(reservePointList);
			}
		}
		return list;
	}

	public List<DangersVo> qryZhntjDangerChemic(Map<String, Object> params) {
		List<DangersVo> list = new ArrayList<>();
		if (params.get("name") != null) {
			String gasName = params.get("name").toString();
			if (!gasName.equals("")) {
				String[] gas = gasName.split(",");
				if (gas != null) {
					list = webDataMapper.qryZhntjDangerChemic(gas);
				}
			}
		}
		return list;
	}

	public SocialResource qrySocialResourceById(Long id) {
		SocialResource socialResource = socialResourceMapper.qryById(id);
		if (socialResource != null) {
			List<UploadEntity> imgList = keyUnitImgMapper.getImgList(id);
			socialResource.setFileList(imgList);
		}
		return socialResource;
	}

}
