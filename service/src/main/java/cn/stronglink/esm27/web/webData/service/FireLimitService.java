package cn.stronglink.esm27.web.webData.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.stronglink.esm27.entity.FireLimit;
import cn.stronglink.esm27.web.webData.mapper.FireLimitMapper;

@Service
@Transactional(rollbackFor=Exception.class)
public class FireLimitService {

	@Autowired
	private  FireLimitMapper  fireLimitMapper;

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<FireLimit> qryList() {
		return fireLimitMapper.qryList();
	}

	public List<FireLimit> qryListByPidId(Long pid) {
		return fireLimitMapper.qryListByPidId(pid);
	}
	
	
}
