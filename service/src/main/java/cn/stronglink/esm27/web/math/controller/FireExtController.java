package cn.stronglink.esm27.web.math.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity; 
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.thirdsdk.explosion.FireExtModel;
@Controller
@RequestMapping(value = "webApi/fireext")
public class FireExtController extends AbstractController  {
	
	@RequestMapping("/gaussionModelParametersSyn")
	public	ResponseEntity<ModelMap>  gaussionModelParametersSyn(ModelMap modelMap, HttpServletRequest request,
				HttpServletResponse response,@RequestBody Map<String, Object> params){
		String areas = "1";  
		String material = "2"; 
		double a = FireExtModel.INSTANCE.solid_fires(areas, material);
		System.out.println(a+"aaa");
		return setSuccessModelMap(modelMap,a+""); 
	}

}
