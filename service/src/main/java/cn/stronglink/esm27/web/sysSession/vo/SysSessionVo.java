package cn.stronglink.esm27.web.sysSession.vo;

import cn.stronglink.esm27.entity.SysSession;

public class SysSessionVo  extends SysSession {
	  /**
	 * 
	 */
	private static final long serialVersionUID = -6171033981575660327L;
	private String userName;
	    
	    public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}
}
