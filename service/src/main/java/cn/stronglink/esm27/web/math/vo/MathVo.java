package cn.stronglink.esm27.web.math.vo;

import java.util.List;

public class MathVo {

	private double coRate;
	private double co2Rate;
	private double so2Rate;
	private double noRate;
	private double no2Rate;
	private double rate;
	private double radius;
	private String radiusList;
	private double dieRadius;
	private double distance;
	private double secondLevelRadius;
	private double forthLevelRadius;
	private double oneLevelRadius;
	private double heatFluxDensity;
	private boolean isHeatHurt;
	private double time;
	private double sX;
	private double sY;
	private double sH;
	private double lsQ;
	private String qq;
	private double pointData[];
	private List<String> mathGranularVoLsit;
	private List<Double> mathDistanceVoLsit;
	private List<double[]> mathGranularDLsit;
	
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
	public double getCoRate() {
		return coRate;
	}
	public void setCoRate(double coRate) {
		this.coRate = coRate;
	}
	public double getCo2Rate() {
		return co2Rate;
	}
	public void setCo2Rate(double co2Rate) {
		this.co2Rate = co2Rate;
	}
	public double getSo2Rate() {
		return so2Rate;
	}
	public void setSo2Rate(double so2Rate) {
		this.so2Rate = so2Rate;
	}
	public double getNoRate() {
		return noRate;
	}
	public void setNoRate(double noRate) {
		this.noRate = noRate;
	}
	public double getNo2Rate() {
		return no2Rate;
	}
	public void setNo2Rate(double no2Rate) {
		this.no2Rate = no2Rate;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	public double getDieRadius() {
		return dieRadius;
	}
	public void setDieRadius(double dieRadius) {
		this.dieRadius = dieRadius;
	}
	public double getSecondLevelRadius() {
		return secondLevelRadius;
	}
	public void setSecondLevelRadius(double secondLevelRadius) {
		this.secondLevelRadius = secondLevelRadius;
	}
	public double getOneLevelRadius() {
		return oneLevelRadius;
	}
	public void setOneLevelRadius(double oneLevelRadius) {
		this.oneLevelRadius = oneLevelRadius;
	}
	public double getHeatFluxDensity() {
		return heatFluxDensity;
	}
	public void setHeatFluxDensity(double heatFluxDensity) {
		this.heatFluxDensity = heatFluxDensity;
	}
	public boolean isHeatHurt() {
		return isHeatHurt;
	}
	public void setHeatHurt(boolean isHeatHurt) {
		this.isHeatHurt = isHeatHurt;
	}
	public double getsX() {
		return sX;
	}
	public void setsX(double sX) {
		this.sX = sX;
	}
	public double getsY() {
		return sY;
	}
	public void setsY(double sY) {
		this.sY = sY;
	}
	public double getsH() {
		return sH;
	}
	public void setsH(double sH) {
		this.sH = sH;
	}
	public double getLsQ() {
		return lsQ;
	}
	public void setLsQ(double lsQ) {
		this.lsQ = lsQ;
	}
	
	
	public List<String> getMathGranularVoLsit() {
		return mathGranularVoLsit;
	}
	public void setMathGranularVoLsit(List<String> mathGranularVoLsit) {
		this.mathGranularVoLsit = mathGranularVoLsit;
	}
	public double[] getPointData() {
		return pointData;
	}
	public void setPointData(double[] pointData) {
		this.pointData = pointData;
	}
	public double getForthLevelRadius() {
		return forthLevelRadius;
	}
	public void setForthLevelRadius(double forthLevelRadius) {
		this.forthLevelRadius = forthLevelRadius;
	}
	public String getRadiusList() {
		return radiusList;
	}
	public void setRadiusList(String radiusList) {
		this.radiusList = radiusList;
	}
	public List<double[]> getMathGranularDLsit() {
		return mathGranularDLsit;
	}
	public void setMathGranularDLsit(List<double[]> mathGranularDLsit) {
		this.mathGranularDLsit = mathGranularDLsit;
	}
	public double getTime() {
		return time;
	}
	public void setTime(double time) {
		this.time = time;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	public List<Double> getMathDistanceVoLsit() {
		return mathDistanceVoLsit;
	}
	public void setMathDistanceVoLsit(List<Double> mathDistanceVoLsit) {
		this.mathDistanceVoLsit = mathDistanceVoLsit;
	}
	
	
}
