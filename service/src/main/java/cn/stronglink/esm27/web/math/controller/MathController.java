package cn.stronglink.esm27.web.math.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.message.mq.topic.SendReceiver;
import cn.stronglink.esm27.thirdsdk.explosion.GasMathModel;
import cn.stronglink.esm27.web.gasHarm.service.GasHarmService;
import cn.stronglink.esm27.web.math.vo.GasDispersionParamsEntity;
import cn.stronglink.esm27.web.math.vo.MQMessageOfESM27;
import cn.stronglink.esm27.web.math.vo.MathGranularVo;
import cn.stronglink.esm27.web.math.vo.MathVo;
@Controller
@RequestMapping(value = "webApi/math")
public class MathController extends AbstractController  {

	@Autowired
	private SendReceiver sendReceiver;
	@Autowired
	private GasHarmService  gasHarmService;
	
//	public void test(){
//		int i = Explosion.INSTANCE.Init_API();
//		System.out.println("i::::::::"+i);	
//		int j = GasMathModel.INSTANCE.Init_API();
//		System.out.println("j::::::::"+j);
//	}
	
	@RequestMapping("/gaussionModelParametersSyn")
	public	ResponseEntity<ModelMap>  gaussionModelParametersSyn(ModelMap modelMap, HttpServletRequest request,
				HttpServletResponse response,@RequestBody Map<String, Object> params){
		int as = Integer.parseInt(params.get("as").toString());
//		int isRealTime = Integer.parseInt(params.get("isRealTime").toString()); 
		double ws = Double.valueOf(params.get("ws").toString());
		double wd = Double.valueOf(params.get("wd").toString());
		double gd = Double.valueOf(params.get("gd").toString());
		double h = Double.valueOf(params.get("h").toString());
		double dq = Double.valueOf(params.get("dq").toString());
		double fCurrentTime = Double.valueOf(params.get("fCurrentTime").toString());
//		double t = Double.valueOf(params.get("t").toString());
			MQMessageOfESM27 mQMessageOfESM27= new MQMessageOfESM27();
			GasDispersionParamsEntity gasObj = new GasDispersionParamsEntity();
			gasObj.setAs(as);
			gasObj.setDq(dq);
			gasObj.setGd(gd);
			gasObj.setH(h);
			gasObj.setfCurrentTime(fCurrentTime);
			gasObj.setPushSpanSecond(30);
			gasObj.setWd(wd);
			gasObj.setWs(ws);
			gasObj.setConcentration(new double[]{0.00001,0.0001});
			//{"awsPostdata":{concentration":[0.0,0.5]},"timestamp":1533458410397}
			mQMessageOfESM27.setAwsPostdata(mQMessageOfESM27);
			mQMessageOfESM27.setAwsPostdata(gasObj);
			mQMessageOfESM27.setTimestamp(1533458410397L);
			mQMessageOfESM27.setActioncode("StartParticleLandingAlgorithmService");
			String param =  JSONObject.toJSONString(mQMessageOfESM27);
			sendReceiver.sendAlgorithm(param);
			return setSuccessModelMap(modelMap);
	}
//	一、计算泄漏率
//	
//	
	@RequestMapping("/getRateGaussionModelParameters")
	public	ResponseEntity<ModelMap>  getRateGaussionModelParameters(ModelMap modelMap, HttpServletRequest request,
				HttpServletResponse response,@RequestBody Map<String, Object> params){
		double ws = Double.valueOf(params.get("ws").toString());
		double wd = Double.valueOf(params.get("wd").toString());
		int as = Integer.parseInt(params.get("as").toString());
//		double gd = Double.valueOf(params.get("gd").toString());
		double h = Double.valueOf(params.get("h").toString());
//		double[] data = (double[]) params.get("data");
		String data = params.get("data").toString();
		//double data[] = params.get("data");
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			GasMathModel.INSTANCE.LeakSource_API(userId, ws, wd, as, data);
			double lsQ = GasMathModel.INSTANCE.GetLsQ_API(userId);
			double sX = GasMathModel.INSTANCE.GetLsX_API(userId);
			double sY = GasMathModel.INSTANCE.GetLsY_API(userId);
			double sH = GasMathModel.INSTANCE.GetLsH_API(userId);
			MathVo  mathVo  = new MathVo();
			mathVo.setsX(sX);
			mathVo.setsY(sY);
			if(sH<0.1){
				sH=0;
			}
			mathVo.setsH(sH);
			mathVo.setLsQ(lsQ);
			double dq = lsQ;
			double fCurrentTime = Double.valueOf(params.get("fCurrentTime").toString());
			String tLine = params.get("tLine").toString();
			if(GasMathModel.INSTANCE.Init_API()==1){
				String gasType=params.get("gasType").toString();
				Map<String,Object> item =gasHarmService.qryItemByGasType(gasType.toUpperCase());
				if(item!=null){
					GasMathModel.INSTANCE.SetGaussionModelParameters_API(userId, as, ws, wd, Double.valueOf(item.get("quality").toString()), h, dq);
//					GasMathModel.INSTANCE.SetGaussionModelParameters_API(userId, as, ws, wd, gd, h, dq);
	//				GasMathModel.INSTANCE.SetDeltaT_API(dt);
					GasMathModel.INSTANCE.Simulate_API(userId, fCurrentTime);
					String[] tLineValues = tLine.split(",");
					List<String> mathGranularVoLsit =  new ArrayList<String>();
					List<Double> mathDistanceVoLsit =  new ArrayList<Double>();
					for(int num=0;num<tLineValues.length;num++){
						GasMathModel.INSTANCE.SetThreshold_API(userId, Double.valueOf(tLineValues[num]));
						int szcd = GasMathModel.INSTANCE.GetVerticesLength_API(userId);
						double[] pointData = new double[szcd];
						GasMathModel.INSTANCE.GetVertices_API(userId, pointData);
						double distance = GasMathModel.INSTANCE.GetDistance_API(userId);
						double[] metersPoint = LatLonToMeters(sX,sY);
						String pointStr = doubleArrayToSrtingPoint(pointData,szcd,metersPoint[0],metersPoint[1]);
						mathGranularVoLsit.add(pointStr);
						mathDistanceVoLsit.add(distance);
					}
					mathVo.setMathGranularVoLsit(mathGranularVoLsit);
					mathVo.setMathDistanceVoLsit(mathDistanceVoLsit);
					return setSuccessModelMap(modelMap, mathVo);
				}else{
					return setModelMap(modelMap, HttpCode.CONFLICT, "无气体信息数据");
				}
			}else{
				return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
			}
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
		}
		
	}
	
//	二、气体扩散模型（比空气轻的气体）
//	1、计算气体扩散过程
//	（1）初始化Init_API 
//	（2）、设置参数SetGaussionModelParameters_API
//	（3）、设置积分时间间隔SetDeltaT_API（也可以不设置此参数，程序内默认是 1 秒，间隔越短计算次数越多耗时越长，间隔越长计算次数越少耗时越少）
//	（4）模拟计算Simulate_API
//	（5）设置浓度等值线SetThreshold_API
//	（6）获取轮廓线顶点坐标数组的长度GetVerticesLength_API
//	（7）获取轮廓线顶点坐标数组GetVertices_API
//	（8）重复5~6步，可以获取多个轮廓线
	@RequestMapping("/gaussionModelParameters")
	public	ResponseEntity<ModelMap>  gaussionModelParameters(ModelMap modelMap, HttpServletRequest request,
				HttpServletResponse response,@RequestBody Map<String, Object> params){
		int as = Integer.parseInt(params.get("as").toString());
		double ws = Double.valueOf(params.get("ws").toString());
		double wd = Double.valueOf(params.get("wd").toString());
		//double gd = Double.valueOf(params.get("gd").toString());
		double h = Double.valueOf(params.get("h").toString());
		double dq = Double.valueOf(params.get("dq").toString());
//		BigDecimal sX=new BigDecimal(params.get("sX").toString());
//		BigDecimal sY =new BigDecimal(params.get("sY").toString());
		double sX = Double.valueOf(params.get("sX").toString());
		double sY = Double.valueOf(params.get("sY").toString());
		double fCurrentTime = Double.valueOf(params.get("fCurrentTime").toString());
		//String tLine = params.get("tLine").toString();
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			String gasType=params.get("gasType").toString();
			Map<String,Object> item =gasHarmService.qryItemByGasType(gasType.toUpperCase());
			if(item!=null){
				GasMathModel.INSTANCE.SetGaussionModelParameters_API(userId, as, ws, wd, Double.valueOf(item.get("quality").toString()), h, dq);
//				GasMathModel.INSTANCE.SetDeltaT_API(dt);
				GasMathModel.INSTANCE.Simulate_API(userId, fCurrentTime);
				String[] tLineValues = item.get("tLine").toString().split(",");
				MathVo  mathVo  = new MathVo();
				List<String> mathGranularVoLsit =  new ArrayList<String>();
				List<Double> mathDistanceVoLsit =  new ArrayList<Double>();
				for(int num=0;num<tLineValues.length;num++){
					GasMathModel.INSTANCE.SetThreshold_API(userId, Double.valueOf(tLineValues[num]));
					int szcd = GasMathModel.INSTANCE.GetVerticesLength_API(userId);
					double[] pointData = new double[szcd];
					GasMathModel.INSTANCE.GetVertices_API(userId, pointData);
					double distance = GasMathModel.INSTANCE.GetDistance_API(userId);
					String pointStr = doubleArrayToSrtingPoint(pointData,szcd,sX,sY);
					mathGranularVoLsit.add(pointStr);
					mathDistanceVoLsit.add(distance);
				}
				mathVo.setMathGranularVoLsit(mathGranularVoLsit);
				mathVo.setMathDistanceVoLsit(mathDistanceVoLsit);
				return setSuccessModelMap(modelMap, mathVo);
			}else{
				return setModelMap(modelMap, HttpCode.CONFLICT, "无气体信息数据");
			}
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
		}
	}
	
	// 计算泄漏源的位置和泄漏率
		// ws: 风速，单位m/s
		// wd: 风向，单位度
		// as: 大气稳定度，取值 1,2,3,4,5,6 与大气稳定度取值表的对应关系 1=A, 2=B, 3=C, 4=D, 5=E, 6=F
		// data[]: 四组传感器数据，传值(q1,x1,y1,h1,......q2,x2,y2,h2) q浓度，xy传感器坐标，h传感器距离地面高度
		// 返回值: true计算成功，false失败
		@RequestMapping("/leakRate")
		public ResponseEntity<ModelMap>  leakRate(ModelMap modelMap, HttpServletRequest request,
				HttpServletResponse response,@RequestBody Map<String, Object> params){
			double ws = Double.valueOf(params.get("ws").toString());
			double wd = Double.valueOf(params.get("wd").toString());
			int as = Integer.parseInt(params.get("as").toString());
			String data = params.get("data").toString();
			if(GasMathModel.INSTANCE.Init_API()==1){
				String userId =WebUtil.getCurrentUser().toString();
				//GasMathModel.INSTANCE.LeakSource_API(userId, ws, wd, as, data);
				double lsQ = GasMathModel.INSTANCE.GetLsQ_API(userId);
				MathVo  mathVo  = new MathVo();
				mathVo.setLsQ(lsQ);
				return setSuccessModelMap(modelMap, mathVo);
			}else{
				return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
			}
		}
	
	// 计算泄漏源的位置和泄漏率
	// ws: 风速，单位m/s
	// wd: 风向，单位度
	// as: 大气稳定度，取值 1,2,3,4,5,6 与大气稳定度取值表的对应关系 1=A, 2=B, 3=C, 4=D, 5=E, 6=F
	// data[]: 四组传感器数据，传值(q1,x1,y1,h1,......q2,x2,y2,h2) q浓度，xy传感器坐标，h传感器距离地面高度
	// 返回值: true计算成功，false失败
	@RequestMapping("/leakSource")
	public ResponseEntity<ModelMap>  leakSource(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		System.out.println("...1.高级反算计算...");
		double ws = Double.valueOf(params.get("ws").toString());
		double wd = Double.valueOf(params.get("wd").toString());
		int as = Integer.parseInt(params.get("as").toString());
//		double[] data = (double[]) params.get("data");
		String data = params.get("data").toString();
		//double data[] = params.get("data");
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			GasMathModel.INSTANCE.LeakSource_API(userId, ws, wd, as, data);
			System.out.println("...2.高级反算计LeakSource_API算...");
			double lsQ = GasMathModel.INSTANCE.GetLsQ_API(userId);
			System.out.println("...3.高级反算lsQ...");
			double sX = GasMathModel.INSTANCE.GetLsX_API(userId);
			double sY = GasMathModel.INSTANCE.GetLsY_API(userId);
			double sH = GasMathModel.INSTANCE.GetLsH_API(userId);
			System.out.println("...4.高级反算sX+....."+sX+".....sY+....."+sY+".....sH+....."+sH);
			MathVo  mathVo  = new MathVo();
			mathVo.setsX(sX);
			mathVo.setsY(sY);
			if(sH<0.1){
				sH=0;
			}
			mathVo.setsH(sH);
			mathVo.setLsQ(lsQ);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
		}
	}
	
	// 颗粒物沉降
		// ws: 风速，单位m/s
		// wd: 风向，单位度
		// as: 大气稳定度，取值 1,2,3,4,5,6 与大气稳定度取值表的对应关系 1=A, 2=B, 3=C, 4=D, 5=E, 6=F
		// data[]: 四组传感器数据，传值(q1,x1,y1,h1,......q2,x2,y2,h2) q浓度，xy传感器坐标，h传感器距离地面高度
		// 返回值: true计算成功，false失败

	@RequestMapping("/granularPrecipitate")
	public ResponseEntity<ModelMap> granularPrecipitate(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		int as = Integer.parseInt(params.get("as").toString());
		double ws = Double.valueOf(params.get("ws").toString());
		double wd = Double.valueOf(params.get("wd").toString());
		double h = Double.valueOf(params.get("h").toString());
		double dq = Double.valueOf(params.get("dq").toString());
		double sX=   Double.valueOf(params.get("sX").toString());
		double sY =  Double.valueOf(params.get("sY").toString());
		double fCurrentTime = Double.valueOf(params.get("fCurrentTime").toString());
		String tl = params.get("t").toString();
		String[] aa = tl.split(",");
		List<String> getMathGranularVoLsit = new ArrayList<String>();
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			boolean f = GasMathModel.INSTANCE.SetPsParameters_API(userId, as, ws, wd, h, dq);
			System.out.print(f);
//			GasMathModel.INSTANCE.SetPsDeltaT_API();
			GasMathModel.INSTANCE.SimulatePs_API(userId, fCurrentTime);
			MathVo  mathVo  = new MathVo();
			for(int i=0;i<aa.length;i++){
				double t = Double.valueOf(aa[i]);
				GasMathModel.INSTANCE.SetPsThreshold_API(userId, t);
				int szcd = GasMathModel.INSTANCE.GetPsVerticesLength_API(userId);
				double[] vertices = new double[szcd];
				GasMathModel.INSTANCE.GetPsVertices_API(userId, vertices);
				MathGranularVo mathGranularVo = new MathGranularVo();
				mathGranularVo.setDzx(t);
				mathGranularVo.setVertices(vertices);
				String pointStr = doubleArrayToSrtingPoint(vertices,szcd,sX,sY);
				getMathGranularVoLsit.add(pointStr);
			}
			mathVo.setMathGranularVoLsit(getMathGranularVoLsit);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
		}
	}
	
	
//	四、爆炸模型
//	1、计算爆炸伤害
//	（1）初始化Init_API 
//	（2）CalcExplosionHurt_API
	@RequestMapping("/explosionHurt")
	public ResponseEntity<ModelMap> explosionHurt(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		int type  = Integer.parseInt(params.get("type1").toString());
		int name = Integer.parseInt(params.get("name1").toString());
		double pressure = Double.valueOf(params.get("pressure").toString());
		double volume = Double.valueOf(params.get("volume").toString());
		double r = Double.valueOf(params.get("r").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			GasMathModel.INSTANCE.Init_API();
			double heatFluxDensity = GasMathModel.INSTANCE.CalcExplosionHurt_API(userId, type, name, pressure, volume, r);
//			double radius =GasMathModel.INSTANCE.CalcExplosionHurtR_API(type, name, pressure, volume, hurt);
			MathVo  mathVo  = new MathVo();
			NumberFormat nf = NumberFormat.getNumberInstance();  
            nf.setMaximumFractionDigits(2);  
			mathVo.setHeatFluxDensity(Double.valueOf(nf.format(heatFluxDensity).replace(",", "")));
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
		}
	}
	
//	2、计算伤害半径,三个伤害
//	（1）初始化Init_API 
//	（2）CalcExplosionHurtR_API
// 0.02轻微损伤，0.03听觉器官损伤，0.05内脏严重损伤，0.1死亡
	@RequestMapping("/explosionHurtR")
	public ResponseEntity<ModelMap>  explosionHurtR(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		int type  = Integer.parseInt(params.get("type1").toString());
		int name = Integer.parseInt(params.get("name1").toString());
		double pressure = Double.valueOf(params.get("pressure").toString());
		double volume = Double.valueOf(params.get("volume").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			MathVo  mathVo  = new MathVo();
			// type = 1
			double[] hurtD1 = {0.02,0.03,0.05,0.1};
			// type = 4
			double[] hurtD4 = {0.15,0.06,0.03,0.015};
			
			double radiusList[] = new double[4];
			if (type == 1){
				for(int i=0;i<hurtD1.length;i++){
					double heatFluxDensity = GasMathModel.INSTANCE.CalcExplosionHurtR_API(userId, type, name, pressure, volume, hurtD1[i]);
					NumberFormat nf = NumberFormat.getNumberInstance();  
		            nf.setMaximumFractionDigits(2);  
					radiusList[i]= Double.valueOf(nf.format(heatFluxDensity).replace(",", ""));
				}
			}
			if (type == 4){
				for(int i=0;i<hurtD4.length;i++){
					double heatFluxDensity = GasMathModel.INSTANCE.CalcExplosionHurtR_API(userId, type, name, pressure, volume, hurtD4[i]);
					NumberFormat nf = NumberFormat.getNumberInstance();  
		            nf.setMaximumFractionDigits(2);  
					radiusList[i]= Double.valueOf(nf.format(heatFluxDensity).replace(",", ""));
				}
			}
			mathVo.setPointData(radiusList);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
		}
	}
	
	//	五、热辐射模型
	//	1、计算热辐射伤害对人
	//	（1）初始化Init_API 
	//	（2）设置参数SetHeatHurtParameters_API
	//	（3）计算热辐射伤害CalcHeatHurt_API
	//	（4）获取伤害等级概率，概率大于5可以认为会受到此等级伤害GetHeatHurtPr1_API、GetHeatHurtPr2_API、GetHeatHurtPr3_API
	// 传参数（计算热辐射伤害），后 4 个参数查数据库
	// S: 火灾面积，单位平方米(m2)
	// T0: 环境温度，单位开氏度(K)
	// Hc: 物质的燃烧热，单位焦耳每公斤(J/Kg)
	// Hv: 物质的蒸发热，单位焦耳每公斤(J/Kg)
	// cp: 物质的定压比热容，单位焦耳(每公斤每华氏度)(J/(Kg*K))
	// Tb: 物质的沸点，单位开氏度(K)
	// 返回值: true成功，false失败
	// 返回值: 目标地点接收到的热通量密度，单位千瓦每平方米（kW/m2）
	// 返回值: 死亡概率，概率单位为5时对应伤亡百分数50%
	// 返回值: 二级烧伤概率，概率单位为5时对应伤亡百分数50%
	// 返回值: 一级烧伤概率，概率单位为5时对应伤亡百分数50%
	// 返回值: 目标地点接收到的热通量密度，单位千瓦每平方米（kW/m2）
	@RequestMapping("/heatHurtParameters")
	public ResponseEntity<ModelMap> heatHurtParameters(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double S  = Double.valueOf(params.get("S").toString());
		double T0 = Double.valueOf(params.get("T0").toString());
		double Hc = Double.valueOf(params.get("Hc").toString());
		double Hv = Double.valueOf(params.get("Hv").toString());
		double cp = Double.valueOf(params.get("cp").toString());
        double Tb = Double.valueOf(params.get("Tb").toString());
        double length = Double.valueOf(params.get("length").toString());
        double time = Double.valueOf(params.get("time").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			MathVo  mathVo  = new MathVo();
			boolean isHeatHurt = GasMathModel.INSTANCE.SetHeatHurtParameters_API(userId, S, T0, Hc, Hv, cp, Tb);
			mathVo.setHeatHurt(isHeatHurt);
			NumberFormat nf = NumberFormat.getNumberInstance();  
            nf.setMaximumFractionDigits(2); 
			if(isHeatHurt){
				GasMathModel.INSTANCE.CalcHeatHurt_API(userId, length, time);
				double dieRadius = GasMathModel.INSTANCE.GetHeatHurtPr1_API(userId);
				double secondLevelRadius = GasMathModel.INSTANCE.GetHeatHurtPr2_API(userId);
				double oneLevelRadius = GasMathModel.INSTANCE.GetHeatHurtPr3_API(userId);
				mathVo.setDieRadius(Double.valueOf(nf.format(dieRadius)));
				mathVo.setSecondLevelRadius(Double.valueOf(nf.format(secondLevelRadius)));
				mathVo.setOneLevelRadius(Double.valueOf(nf.format(oneLevelRadius)));
			}
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
		}
	}
	
//	2、计算热辐射伤害半径
//	（1）初始化Init_API 
//	（2）设置参数SetHeatHurtParameters_API
//	（3）计算热辐射伤害半径CalcHeatHurtR_API
//	（4）获取伤害半径，GetHeatHurtR1_API、GetHeatHurtR2_API、GetHeatHurtR3_API
	// 返回值: 死亡半径，单位米(m)
	// 返回值: 二级烧伤半径，单位米(m)
	// 返回值: 一级烧伤半径，单位米(m)
	@RequestMapping("/heatHurtCalParameters")
	public ResponseEntity<ModelMap> heatHurtCalParameters(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double S  = Double.valueOf(params.get("S").toString());
		double T0 = Double.valueOf(params.get("T0").toString());
		double Hc = Double.valueOf(params.get("Hc").toString());
		double Hv = Double.valueOf(params.get("Hv").toString());
		double cp = Double.valueOf(params.get("cp").toString());
        double Tb = Double.valueOf(params.get("Tb").toString());
        if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			GasMathModel.INSTANCE.SetHeatHurtParameters_API(userId, S, T0, Hc, Hv, cp, Tb);
			double r1D[] = new double[19];
			double r2D[] = new double[19];
			double r3D[] = new double[19];
			for(int t = 1; t < 10; t++)
			{
//				[820, 932, 901, 934, 1290, 1330, 1320]
				GasMathModel.INSTANCE.CalcHeatHurtR_API(userId, t*120);
				double r1 = GasMathModel.INSTANCE.GetHeatHurtR1_API(userId);
				double r2 = GasMathModel.INSTANCE.GetHeatHurtR2_API(userId);
				double r3 = GasMathModel.INSTANCE.GetHeatHurtR3_API(userId);
				NumberFormat nf = NumberFormat.getNumberInstance();  
	            nf.setMaximumFractionDigits(2);  
				r1D[t-1] = Double.valueOf(nf.format(r1));
				r2D[t-1] = Double.valueOf(nf.format(r2));
				r3D[t-1] = Double.valueOf(nf.format(r3));
			}
			List<double[]> mathGranularDLsit =  new ArrayList<double[]>();
			mathGranularDLsit.add(r1D);
			mathGranularDLsit.add(r2D);
			mathGranularDLsit.add(r3D);
			MathVo  mathVo  = new MathVo();
			mathVo.setMathGranularDLsit(mathGranularDLsit);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
		}
	}
	
//	3、石油计算半径和时间对比曲线
//	（1）初始化Init_API 
//	（2）设置参数SetHeatHurtParameters_API
//	（3）计算热辐射伤害半径CalcHeatHurtR_API
//	（4）获取伤害半径，GetHeatHurtR1_API、GetHeatHurtR2_API、GetHeatHurtR3_API
	// 返回值: 死亡半径，单位米(m)
	// 返回值: 二级烧伤半径，单位米(m)
	// 返回值: 一级烧伤半径，单位米(m)
	//燃烧线速度,石油的   ZHMNrswfxModelDivMt = 0.0781;
	// 传参数（计算热辐射伤害），主要用来计算石油、煤油、汽油等混合物燃烧热辐射伤害
	// S: 火灾面积，单位平方米(m2)
	// Hc: 物质的燃烧热，单位焦耳每公斤(J/Kg)
	// mf: 燃烧速率（kg•m-2•s-1），石油最大燃烧速率 0.0781
	// 返回值: true成功，false失败
	@RequestMapping("/hotTimeRadiusLine")
	public ResponseEntity<ModelMap> hotTimeRadiusLine(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double speed  = Double.valueOf(params.get("speed").toString());//0.0781;
		double area  = Double.valueOf(params.get("area").toString());
		double Hc  = Double.valueOf(params.get("Hc").toString());//41816000;
        if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			GasMathModel.INSTANCE.SetHeatHurtMf_API(userId, area, speed, Hc);
			double r1D[] = new double[19];
			double r2D[] = new double[19];
			double r3D[] = new double[19];
			for(int t = 1; t < 10; t++)
			{
//				[820, 932, 901, 934, 1290, 1330, 1320]
				GasMathModel.INSTANCE.CalcHeatHurtR_API(userId, t*120);
				double r1 = GasMathModel.INSTANCE.GetHeatHurtR1_API(userId);
				double r2 = GasMathModel.INSTANCE.GetHeatHurtR2_API(userId);
				double r3 = GasMathModel.INSTANCE.GetHeatHurtR3_API(userId);
				NumberFormat nf = NumberFormat.getNumberInstance();  
	            nf.setMaximumFractionDigits(2);  
				r1D[t-1] = Double.valueOf(nf.format(r1));
				r2D[t-1] = Double.valueOf(nf.format(r2));
				r3D[t-1] = Double.valueOf(nf.format(r3));
			}
			List<double[]> mathGranularDLsit =  new ArrayList<double[]>();
			mathGranularDLsit.add(r1D);
			mathGranularDLsit.add(r2D);
			mathGranularDLsit.add(r3D);
			MathVo  mathVo  = new MathVo();
			mathVo.setMathGranularDLsit(mathGranularDLsit);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
		}
	}
	
	@RequestMapping("/hotTimeRadiusLineFFSF")
	public ResponseEntity<ModelMap> hotTimeRadiusLineFFSF(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double speed  = Double.valueOf(params.get("speed").toString());//0.0781;
		double area  = Double.valueOf(params.get("area").toString());
		double Hc  = Double.valueOf(params.get("Hc").toString());//41816000;
		int time  = Integer.parseInt(params.get("time").toString());
		double tpp  = Double.valueOf(params.get("tpp").toString());
        if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			GasMathModel.INSTANCE.SetHeatHurtMf_API(userId, area, speed, Hc);
			double r = calc_xff_hr_radius(time,tpp,area);	
			if(r<0){
				r=0;
			}
			MathVo  mathVo  = new MathVo();
			mathVo.setDieRadius(r);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
		}
	}
	
	/**
	 * 消防服热辐射范围
	 * @param t 分钟 tpp 防火服 tpp 值	cal/cm2
	 * */
	public double  calc_fhf_r(double tpp, int t)
	{
		double k = tpp * 10000;	// 1平方米面积接受的热量
		double j = k * 4.184;		// 1卡=4.184焦耳
		double w = j / (t*60);	// 1平方米面积上的功率，瓦
		double kw = w / 1000;		// 1平方米面积上的功率，千瓦
		return kw;
	}
	
		/**
		 * 消防服热辐射范围
		 * @param t 一次进攻时间 单位分钟
		 * */
	public double  calc_xff_hr_radius(int time,double tpp,double m_S){
		double kw = calc_fhf_r(tpp, time);
		double w = 0;
		double r = Math.sqrt(m_S / 3.14159);
		String userId =WebUtil.getCurrentUser().toString();
			for (double l = r+1; l < 1000+r; l=l+5)
			{
				w = GasMathModel.INSTANCE.CalcHeatHurt_API(userId, l, time*60);
				if (w < kw)
				{
					return l;
				}
			}
			return 0;
	}

//	六、管道泄漏模型
//	1、计算液体泄漏率
//	（1）初始化Init_API 
//	（2）计算液体泄漏率CalcLiquidLeak_API
	// 返回值: 泄漏速率（kg/s）
	@RequestMapping("/calcLiquidLeak") 
	public ResponseEntity<ModelMap> calcLiquidLeak(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double rou  = Double.valueOf(params.get("rou").toString());
		double s = Double.valueOf(params.get("s").toString());
		double p = Double.valueOf(params.get("p").toString());
		double h = Double.valueOf(params.get("h").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){ 
			String userId =WebUtil.getCurrentUser().toString();
			GasMathModel.INSTANCE.Init_API();
			double rate = GasMathModel.INSTANCE.CalcLiquidLeak_API(userId, rou, s, p, h);
			MathVo  mathVo  = new MathVo();
			mathVo.setRate(rate);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
		}
	}
	
//	2、计算瞬时泄露的液池半径
//	（1）初始化Init_API 
//	（2）计算液体泄漏（瞬时泄露）后形成的液池半径CalcLiquidLeakArea1_API
	// 返回值: 液池半径（m）
	@RequestMapping("/calcLiquidLeakArea1")
	public ResponseEntity<ModelMap> calcLiquidLeakArea1(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double rou  = Double.valueOf(params.get("rou").toString());
		double m = Double.valueOf(params.get("m").toString());
		double t = Double.valueOf(params.get("t").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			GasMathModel.INSTANCE.Init_API();
			double radius = GasMathModel.INSTANCE.CalcLiquidLeakArea1_API(userId, m, t, rou);
			MathVo  mathVo  = new MathVo();
			mathVo.setRate(radius);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
		}
	}
	
//	3、计算持续泄露的液池半径
//	（1）初始化Init_API 
//	（2）计算液体泄漏（持续泄露）后形成的液池半径CalcLiquidLeakArea2_API
	// 返回值: 液池半径（m）
	@RequestMapping("/calcLiquidLeakArea2")
	public ResponseEntity<ModelMap> calcLiquidLeakArea2(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double rou  = Double.valueOf(params.get("rou").toString());
		double m = Double.valueOf(params.get("m").toString());
		double t = Double.valueOf(params.get("t").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			double radius = GasMathModel.INSTANCE.CalcLiquidLeakArea2_API(userId, m, t, rou);
			MathVo  mathVo  = new MathVo();
			mathVo.setRate(radius);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
		}
	}
//	七、液体蒸发模型
//	1、计算闪蒸速率
//	（1）初始化Init_API 
//	（2）计算闪蒸速率CalcEvaporate1_API
	// 返回值: 闪蒸速率（kg/s）
	@RequestMapping("/calcEvaporate1")
	public ResponseEntity<ModelMap> calcEvaporate1(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double Cp  = Double.valueOf(params.get("Cp").toString());
		double Hv = Double.valueOf(params.get("Hv").toString());
		double Tb = Double.valueOf(params.get("Tb").toString());
		double m = Double.valueOf(params.get("m").toString());
		double t = Double.valueOf(params.get("t").toString());
		double A1 = Double.valueOf(params.get("A1").toString());
		double T0 = Double.valueOf(params.get("T0").toString());
		double L = Double.valueOf(params.get("L").toString());
		double K = Double.valueOf(params.get("K").toString());
		double a = Double.valueOf(params.get("a").toString());
		double Nu = Double.valueOf(params.get("Nu").toString());
		double alpha = Double.valueOf(params.get("alpha").toString());
		double Sh = Double.valueOf(params.get("Sh").toString());
		double A = Double.valueOf(params.get("A").toString());
		double rou = Double.valueOf(params.get("rou").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			double rate1 = GasMathModel.INSTANCE.CalcEvaporate1_API(userId, Cp, Hv, T0, Tb, m, t);
			double rate2 = GasMathModel.INSTANCE.CalcEvaporate2_API(userId, A1, T0, Tb, Hv, L, K, a, t, Nu);
			double rate3 = GasMathModel.INSTANCE.CalcEvaporate3_API(userId, alpha, Sh, A, L, rou);
			MathVo  mathVo  = new MathVo();
			double rate = rate1+rate2+rate3;
			mathVo.setRate(rate);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
		}
	}
	
//	2、计算热量蒸发速率
//	（1）初始化Init_API 
//	（2）计算热量蒸发速率CalcEvaporate2_API
//	返回值: 蒸发速率（kg/s）
	@RequestMapping("/calcEvaporate2")
	public ResponseEntity<ModelMap> calcEvaporate2(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double A1  = Double.valueOf(params.get("A1").toString());
		double T0 = Double.valueOf(params.get("T0").toString());
		double Tb = Double.valueOf(params.get("Tb").toString());
		double Hv  = Double.valueOf(params.get("Hv").toString());
		double L = Double.valueOf(params.get("L").toString());
		double K = Double.valueOf(params.get("K").toString());
		double a  = Double.valueOf(params.get("a").toString());
		double T = Double.valueOf(params.get("T").toString());
		double Nu = Double.valueOf(params.get("Nu").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			double rate = GasMathModel.INSTANCE.CalcEvaporate2_API(userId, A1, T0, Tb, Hv, L, K, a, T, Nu);
			MathVo  mathVo  = new MathVo();
			mathVo.setRate(rate);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
		}
		
	}
	
//	3、计算质量蒸发速率
//	（1）初始化Init_API 
//	（2）计算质量蒸发速率CalcEvaporate3_API
	@RequestMapping("/calcEvaporate3")
	public ResponseEntity<ModelMap> calcEvaporate3(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double alpha  = Double.valueOf(params.get("alpha").toString());
		double Sh = Double.valueOf(params.get("Sh").toString());
		double A = Double.valueOf(params.get("A").toString());
		double L = Double.valueOf(params.get("L").toString());
		double rou = Double.valueOf(params.get("rou").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			double rate = GasMathModel.INSTANCE.CalcEvaporate3_API(userId, alpha, Sh, A, L, rou);
			MathVo  mathVo  = new MathVo();
			mathVo.setRate(rate);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
		}
	}
	
//	八、燃烧物分析
//	（1）初始化Init_API 
//	（2）、设置参数CalcEmissionRate_API
//	（3）、获取污染物产生率GetCO_API、GetCO2_API、GetSO2_API、GetNO_API、GetNO2_API
	@RequestMapping("/calcEmissionRate")
	public ResponseEntity<ModelMap> calcEmissionRate(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double ws  = Double.valueOf(params.get("ws").toString());
		double mt =  Double.valueOf(params.get("mt").toString());
		double Cc = Double.valueOf(params.get("Cc").toString())/100;
		double Cs = Double.valueOf(params.get("Cs").toString())/100;
		double Cn = Double.valueOf(params.get("Cn").toString())/100;
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			GasMathModel.INSTANCE.CalcEmissionRate_API(userId, ws, mt, Cc, Cs, Cn);
			double coRate = GasMathModel.INSTANCE.GetCO_API(userId);
			double co2Rate = GasMathModel.INSTANCE.GetCO2_API(userId);
			double so2Rate = GasMathModel.INSTANCE.GetSO2_API(userId);
			double noRate = GasMathModel.INSTANCE.GetNO_API(userId);
			double no2Rate = GasMathModel.INSTANCE.GetNO2_API(userId);
			MathVo  mathVo  = new MathVo();
			mathVo.setCoRate(coRate);
			mathVo.setCo2Rate(co2Rate);
			mathVo.setNo2Rate(no2Rate);
			mathVo.setNoRate(noRate);
			mathVo.setSo2Rate(so2Rate);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
		}
	}
	
//	九、燃烧物分析
	// 获取有风的情况下的热辐射范围
	// ws: 风速
	// wd: 风向
	// d: 罐直径
	// 返回值: 范围线的数组长度
	@RequestMapping("/calcHrLine")
	public ResponseEntity<ModelMap> calcHrLine(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double ws =  Double.valueOf(params.get("ws").toString());
		double wd = Double.valueOf(params.get("wd").toString());
		List<String> mathGranularVoLsit =  new ArrayList<String>();
		double area  = Double.valueOf(params.get("area").toString());
		double speed  = 0.0781;
		double Hc  = 41816000;
//		BigDecimal sX=new BigDecimal(params.get("sX").toString());
//		BigDecimal sY =new BigDecimal(params.get("sY").toString());
		double sX=   Double.valueOf(params.get("sX").toString());
		double sY =  Double.valueOf(params.get("sY").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			GasMathModel.INSTANCE.SetHeatHurtMf_API(userId, area, speed, Hc);
			int szcd = GasMathModel.INSTANCE.CalcHrLine_API(userId, ws, wd, speed);
			double[] pointData1 = new double[szcd];
			double[] pointData2 = new double[szcd];
			double[] pointData3 = new double[szcd];
			GasMathModel.INSTANCE.GetHrVertices1_API(userId, pointData1);
			GasMathModel.INSTANCE.GetHrVertices2_API(userId, pointData2);
			GasMathModel.INSTANCE.GetHrVertices3_API(userId, pointData3);
			MathVo  mathVo  = new MathVo();
			String pointStr1 = doubleArrayToSrtingPoint(pointData1,szcd,sX,sY);
			String pointStr2 = doubleArrayToSrtingPoint(pointData2,szcd,sX,sY);
			String pointStr3 = doubleArrayToSrtingPoint(pointData3,szcd,sX,sY);
			mathGranularVoLsit.add(pointStr1);
			mathGranularVoLsit.add(pointStr2);
			mathGranularVoLsit.add(pointStr3);
			mathVo.setMathGranularVoLsit(mathGranularVoLsit);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
		}
	}
	
	  /*
     * 原油喷溅范围
     * */
	@RequestMapping("/getPJRegion")
	public ResponseEntity<ModelMap> getPJRegion(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double ws =  Double.valueOf(params.get("ws").toString());
		double wd = Double.valueOf(params.get("wd").toString());
		double r =  Double.valueOf(params.get("r").toString());
		double sX=   Double.valueOf(params.get("sX").toString());
		double sY =  Double.valueOf(params.get("sY").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			double[] va = new double[80];
			GasMathModel.INSTANCE.GetPJRegion_API(userId, ws, wd,r,va);

			/*
			// 测试范围边界矩形框
			double x_min = 999999;
			double x_max = -999999;
			double y_min = 999999;
			double y_max = -999999;
			for(int j = 0; j < va.length; j=j+2){
				double x_new = va[j];
				double y_new = va[j+1];
				if (x_min > x_new) x_min = x_new;
				if (x_max < x_new) x_max = x_new;
				if (y_min > y_new) y_min = y_new;
				if (y_max < y_new) y_max = y_new;
			}
			*/
			
			MathVo  mathVo  = new MathVo();
			List<String> mathGranularVoLsit =  new ArrayList<String>();
			String vaPoitnStr = doubleArrayToSrtingPointAll(va,80,sX,sY);
			mathGranularVoLsit.add(vaPoitnStr);
			mathVo.setMathGranularVoLsit(mathGranularVoLsit);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
		}
    }
	
	  /*
     * 火灾蔓延
     * */
	@RequestMapping("/getFireSpreadRegion")
	public ResponseEntity<ModelMap> getFireSpreadRegion(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double ws =  Double.valueOf(params.get("ws").toString());
		double wd = Double.valueOf(params.get("wd").toString());
		double sX=   Double.valueOf(params.get("sX").toString());
		double sY =  Double.valueOf(params.get("sY").toString());
		double t =  Double.valueOf(params.get("fCurrentTime").toString());
		t = t / 60;	// 传进来的 t 是秒，应该用分钟
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			double[] va = new double[252];
			double[] sa = new double[3];
			GasMathModel.INSTANCE.GetFireSpreadRegion_API(userId, ws, wd, t, va, sa);
			MathVo  mathVo  = new MathVo();
			List<String> mathGranularVoLsit =  new ArrayList<String>();
			String vaPoitnStr = doubleArrayToSrtingPointAll(va,252,sX,sY);
			mathGranularVoLsit.add(vaPoitnStr);
			mathVo.setMathGranularVoLsit(mathGranularVoLsit);
			mathVo.setPointData(sa);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "初始化信息错 误");
		}
    }
	
	 /*
	  * len 建筑物距离火灾地点距离
	  * R 安全半径，热辐射通量 37.5kW/m2 的距离值
	  * time 建筑物耐火时间
	  * */ 
	@RequestMapping("/buildingsFlameTime")
	public ResponseEntity<ModelMap> buildingsFlameTime(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
			double len = Double.valueOf(params.get("len").toString());
			double time = Double.valueOf(params.get("time").toString());
			double speed  = 0.0781;
			double area  = Double.valueOf(params.get("area").toString());
			double Hc  = 41816000;
			double returnTime  = 0;
	        if(GasMathModel.INSTANCE.Init_API()==1){
				String userId =WebUtil.getCurrentUser().toString();
				GasMathModel.INSTANCE.SetHeatHurtMf_API(userId, area, speed, Hc);
				
				double kw = 37.5;
				double fire_r = Math.sqrt(area / 3.14159);
				
				double w = 0;
				double R = fire_r+1;	// 安全半径，热辐射通量 37.5kW/m2 的距离值
				for (R = fire_r+1; R < 1000+fire_r; R=R+5)
				{
					w = GasMathModel.INSTANCE.CalcHeatHurt_API(userId, R, time*60);
					if (w < kw)
					{
						break;
					}
				}
				
			 	if (len > R)
			 	{
			 		returnTime = time;
			 	}
			 	else if (len > fire_r)
			 	{
			 		returnTime =  time / 2 * (1 + (len - fire_r) / (R - fire_r));
			 	}
			 	else
			 	{
			 		returnTime =  time / 2;
			 	}

		 		MathVo mathVo = new MathVo();
		 		mathVo.setTime(returnTime);
		 		mathVo.setDieRadius(R);
				return setSuccessModelMap(modelMap, mathVo);
			}else{
				return setModelMap(modelMap, HttpCode.CONFLICT, "数据不标准，初始化信息错误");
			}
		 }
	
	//数组转换成字符串
		public String  doubleArrayToSrtingPointAll(double[] pointDatas,int pointDataLength,double sX,double sY){
			String pointStr = "[";
			String pointStrTemp = "";
			for(int j = 0; j < pointDataLength; j=j+2){
				if(j==0){
					pointStrTemp += "[";
					pointStrTemp +=  sX+pointDatas[j];
					pointStrTemp += ",";
					pointStrTemp += sY+pointDatas[j+1];
					pointStrTemp += "]";
				}
				if(j<pointDataLength-2){
					pointStr += "[";
					pointStr += sX+pointDatas[j];
					pointStr += ",";
					pointStr += sY+pointDatas[j+1];
					pointStr += "],";
				}
			}
			pointStr += pointStrTemp;
			pointStr += "]";
			return pointStr;
		}
	
	//数组转换成字符串
	public String  doubleArrayToSrtingPoint(double[] pointDatas,int pointDataLength,double sX,double sY){
		String pointStr = "[";
		String pointStrTemp = "";
		for(int j = 0; j < pointDataLength; j=j+12){
			if(j==0){
				pointStrTemp += "[";
				pointStrTemp +=  sX+pointDatas[j];
				pointStrTemp += ",";
				pointStrTemp += sY+pointDatas[j+1];
				pointStrTemp += "]";
			}
			if(j<pointDataLength-12){
				pointStr += "[";
				pointStr += sX+pointDatas[j];
				pointStr += ",";
				pointStr += sY+pointDatas[j+1];
				pointStr += "],";
			}
//			if(j==0){
//				pointStrTemp += "[";
//				pointStrTemp +=  sX.add(new BigDecimal(pointDatas[j]));
//				pointStrTemp += ",";
//				pointStrTemp += sY.add(new BigDecimal(pointDatas[j+1]));
//				pointStrTemp += "]";
//			}
//			if(j<pointDataLength-12){
//				pointStr += "[";
//				pointStr += sX.add(new BigDecimal(pointDatas[j]));
//				pointStr += ",";
//				pointStr += sY.add(new BigDecimal(pointDatas[j+1]));
//				pointStr += "],";
//			}
		}
		pointStr += pointStrTemp;
		pointStr += "]";
		return pointStr;
	}
	
    /**
     * Converts XY point from Spherical Mercator EPSG:900913 to lat/lon in WGS84 
     * Datum 
     *  经纬度转墨卡托
     * @return 
     */ 
    double originShift = 2 * Math.PI * 6378137 / 2.0;
    public double[] LatLonToMeters( double lat, double lon ) { 
        double mx = lon * originShift / 180.0; 
        double my = Math.log(Math.tan((90 + lat) * Math.PI / 360.0)) / (Math.PI / 180.0); 
        my = my * originShift / 180.0; 
        return new double[]{mx, my}; 
    } 
 
    /**
     * Converts XY point from Spherical Mercator EPSG:900913 to lat/lon in WGS84 
     * Datum 
     *  墨卡托转经纬度
     * @return 
     */ 
    public double[] MetersToLatLon( double mx, double my ) { 
 
        double lon = (mx / originShift) * 180.0; 
        double lat = (my / originShift) * 180.0; 
 
        lat = 180 / Math.PI * (2 * Math.atan(Math.exp(lat * Math.PI / 180.0)) - Math.PI / 2.0); 
        return new double[]{lat, lon}; 
    }
    
//    public static void main(String[] args) {
//    	 double lon = 107.9671884;
//    	 double lat = 31.35408426;
//    	 double originShift = 2 * Math.PI * 6378137 / 2.0;
//    	double mx = lon * originShift / 180.0; 
//        double my = Math.log(Math.tan((90 + lat) * Math.PI / 360.0)) / (Math.PI / 180.0); 
//        my = my * originShift / 180.0; 
//        System.out.println(mx);
//        System.out.println(my);
//	}
    
}
