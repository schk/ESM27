package cn.stronglink.esm27.web.webData.mapper.mappers;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.IncidentRecord;

public interface RecordMapper extends BaseMapper<IncidentRecord> {

}
