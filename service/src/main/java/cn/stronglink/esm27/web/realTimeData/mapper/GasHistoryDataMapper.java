package cn.stronglink.esm27.web.realTimeData.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.GasHistoryData;

public interface GasHistoryDataMapper extends BaseMapper<GasHistoryData> {

}
