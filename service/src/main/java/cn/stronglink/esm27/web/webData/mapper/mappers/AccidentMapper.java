package cn.stronglink.esm27.web.webData.mapper.mappers;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.esm27.entity.Accident;
import cn.stronglink.esm27.module.fireWaterSource.vo.FireWaterSourceVo;

public interface AccidentMapper extends BaseMapper<Accident> {

	List<FireWaterSourceVo> qryListByParams(Pagination page, Map<String, Object> params);
	List<Accident> getAccidentByRoomid(@Param("roomId") String roomId);
	
}
