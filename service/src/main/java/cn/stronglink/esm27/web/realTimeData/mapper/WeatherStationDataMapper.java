package cn.stronglink.esm27.web.realTimeData.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.WeatherStationData;

public interface WeatherStationDataMapper extends BaseMapper<WeatherStationData> {

}
