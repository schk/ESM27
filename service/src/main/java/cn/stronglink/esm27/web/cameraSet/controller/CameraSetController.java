package cn.stronglink.esm27.web.cameraSet.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.esm27.entity.CameraSet;
import cn.stronglink.esm27.web.cameraSet.service.CameraSetService;

@Controller
@RequestMapping(value = "webApi/cameraSet")
public class CameraSetController extends AbstractController {
	
	@Autowired
	private CameraSetService  cameraSetService;
	
	/**
	 * 查询所有相机
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param serial
	 * @return
	 */
	@RequestMapping("qryAll")
	public ResponseEntity<ModelMap> qryAll(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap){
		List<CameraSet> data = cameraSetService.qryAll();
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 根据编号查询相机信息
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param serial
	 * @return
	 */
	@RequestMapping("qryById")
	public ResponseEntity<ModelMap> qryById(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody String serial){
		CameraSet data = cameraSetService.qryById(serial);
		return setSuccessModelMap(modelMap, data);
	}
	/*
	 * 根据id删除信息
	 */
	@RequestMapping(value = "remove")
	public ResponseEntity<ModelMap> remove(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody Long id){
		cameraSetService.delete(id);
		return setSuccessModelMap(modelMap, null);
	}
	/*
	 * 添加信息
	 */
	@RequestMapping(value = "create")
	public ResponseEntity<ModelMap> create(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap , @RequestBody List<CameraSet> entity){
		cameraSetService.insert(entity);
		return setSuccessModelMap(modelMap, null);
	}
	/*
	 * 修改信息
	 */
	@RequestMapping(value = "edit")
	public ResponseEntity<ModelMap> edit(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap , @RequestBody CameraSet fireControl){
		cameraSetService.update(fireControl);
		return setSuccessModelMap(modelMap, null);
		
	}

}
