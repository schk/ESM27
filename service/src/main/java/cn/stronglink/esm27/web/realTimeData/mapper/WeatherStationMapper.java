package cn.stronglink.esm27.web.realTimeData.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.esm27.entity.WeatherStation;
import cn.stronglink.esm27.web.realTimeData.vo.HistoryQryParams;
import cn.stronglink.esm27.web.realTimeData.vo.WeatherDataVo;

public interface WeatherStationMapper extends BaseMapper<WeatherStation> {

	List<WeatherDataVo> queryHistoryWeatherData(HistoryQryParams params);

	List<WeatherDataVo> queryHistoryWeatherDataByPage(Page<WeatherDataVo> page, HistoryQryParams params);

	List<WeatherDataVo> queryHistoryWeatherDataNoPage(HistoryQryParams params);


}
