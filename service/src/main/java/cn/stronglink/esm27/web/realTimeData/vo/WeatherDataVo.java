package cn.stronglink.esm27.web.realTimeData.vo;

import cn.stronglink.esm27.entity.WeatherStationData;

public class WeatherDataVo extends WeatherStationData{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String stationPort;
	
	private String timeString;

	public String getTimeString() {
		return timeString;
	}

	public void setTimeString(String timeString) {
		this.timeString = timeString;
	}

	public String getStationPort() {
		return stationPort;
	}

	public void setStationPort(String stationPort) {
		this.stationPort = stationPort;
	}
	
}
