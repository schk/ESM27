package cn.stronglink.esm27.web.expr.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.expr.Parser;
import cn.stronglink.core.expr.SyntaxException;
import cn.stronglink.core.expr.Variable;
import cn.stronglink.esm27.entity.Expr;
import cn.stronglink.esm27.entity.ExprParam;
import cn.stronglink.esm27.module.expr.service.ExprService;
import cn.stronglink.esm27.module.expr.vo.ExprVo;

@Controller
@RequestMapping(value = "webApi/expr")
public class ExprAPIController extends AbstractController {
	
	@Autowired
	private ExprService exprService;
	
	/*
	 * 查询所有公式
	 */
	@RequestMapping("qryList")
	public ResponseEntity<ModelMap> qryList(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap){
		List<Expr> data = exprService.getList();
		return setSuccessModelMap(modelMap, data);
	}
	
	/*
	 * 查询公式详情
	 */
	@RequestMapping("qryDetail")
	public ResponseEntity<ModelMap> qryDetail(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap, @RequestBody Long id){
		ExprVo vo = exprService.qryInfo(id);
		return setSuccessModelMap(modelMap, vo);
	}
	
	/*
	 * 公式计算
	 */
	@RequestMapping("calculate")
	public ResponseEntity<ModelMap> calculate(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap, @RequestBody ExprVo vo){
		try {
			cn.stronglink.core.expr.Expr expr = Parser.parse(vo.getExpr());
			for(ExprParam param : vo.getExprParams()) {
				Variable v = Variable.make(param.getName());
				v.setValue((param.getValue()!=null&&!"".equals(param.getValue()))?Double.valueOf(param.getValue()):0);
			}
			return setSuccessModelMap(modelMap, expr.value());
		} catch (SyntaxException e) {
			throw new BusinessException("计算错误");
		}
	}

}
