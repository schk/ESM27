package cn.stronglink.esm27.web.realTimeData.vo;

import java.util.Date;

public class HistoryQryParams {
	
	private String startTime;
	private String endTime;
	private String deviceSerial;
	private String videoIp;
	private String pageNum;
	private String pageSize;
	private String room;
	private Date[] createTime;
	private String startMonth;
	private String endMonth;
	
	public String getStartMonth() {
		return startMonth;
	}
	public void setStartMonth(String startMonth) {
		this.startMonth = startMonth;
	}
	public String getEndMonth() {
		return endMonth;
	}
	public void setEndMonth(String endMonth) {
		this.endMonth = endMonth;
	}
	public Date[] getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date[] createTime) {
		this.createTime = createTime;
	}
	public String getRoom() {
		return room;
	}
	public void setRoom(String room) {
		this.room = room;
	}
	public String getPageNum() {
		return pageNum;
	}
	public void setPageNum(String pageNum) {
		this.pageNum = pageNum;
	}
	public String getPageSize() {
		return pageSize;
	}
	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getDeviceSerial() {
		return deviceSerial;
	}
	public void setDeviceSerial(String deviceSerial) {
		this.deviceSerial = deviceSerial;
	}
	public String getVideoIp() {
		return videoIp;
	}
	public void setVideoIp(String videoIp) {
		this.videoIp = videoIp;
	}
	
}
