package cn.stronglink.esm27.web.realTimeData.vo;

import java.util.List;

import cn.stronglink.esm27.entity.GasDetector;
import cn.stronglink.esm27.entity.GasDetectorData;
import cn.stronglink.esm27.entity.WeatherStation;

public class DataConfigureVo {

	private String serialPort;//串口
	private List<GasDetector> gasDetector;//气体检测仪
	private List<WeatherStation> weatherStation;//气象站
	private String timeCell;   //时间间隔
	private List<GasDetectorData> gasDetectorData;
	
	public String getSerialPort() {
		return serialPort;
	}
	public void setSerialPort(String serialPort) {
		this.serialPort = serialPort;
	}
	public List<GasDetector> getGasDetector() {
		return gasDetector;
	}
	public void setGasDetector(List<GasDetector> gasDetector) {
		this.gasDetector = gasDetector;
	}
	public List<WeatherStation> getWeatherStation() {
		return weatherStation;
	}
	public void setWeatherStation(List<WeatherStation> weatherStation) {
		this.weatherStation = weatherStation;
	}
	public String getTimeCell() {
		return timeCell;
	}
	public void setTimeCell(String timeCell) {
		this.timeCell = timeCell;
	}
	public List<GasDetectorData> getGasDetectorData() {
		return gasDetectorData;
	}
	public void setGasDetectorData(List<GasDetectorData> gasDetectorData) {
		this.gasDetectorData = gasDetectorData;
	}
}
