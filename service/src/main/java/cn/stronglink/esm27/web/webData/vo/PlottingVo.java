package cn.stronglink.esm27.web.webData.vo;

import java.util.List;

import com.baomidou.mybatisplus.annotations.TableField;

import cn.stronglink.esm27.entity.Dictionary;
import cn.stronglink.esm27.entity.Plotting;


public class PlottingVo extends Dictionary{

	/**
	 * 标绘管理表
	 */
	@TableField(exist=false)
	private static final long serialVersionUID = -6662991661172366861L;

	private List<Plotting> children;

	public List<Plotting> getChildren() {
		return children;
	}

	public void setChildren(List<Plotting> children) {
		this.children = children;
	}
	

	
	
}
