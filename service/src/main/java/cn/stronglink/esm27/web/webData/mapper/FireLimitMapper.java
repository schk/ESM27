package cn.stronglink.esm27.web.webData.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.FireLimit;

public interface FireLimitMapper extends BaseMapper<FireLimit>  {
	
	List<FireLimit> qryList();

	List<FireLimit> qryListByPidId(Long pid);
}
