package cn.stronglink.esm27.web.camera.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.util.Base64EncryptUtil;
import cn.stronglink.esm27.entity.CameraSet;
import cn.stronglink.esm27.web.camera.param.CameraParam;
import cn.stronglink.esm27.web.camera.service.CameraService;
import cn.stronglink.esm27.web.camera.vo.CameraVo;

@Controller
@RequestMapping(value = "webApi/camera")
public class CameraController extends AbstractController {

	@Value("${stream.server}")
	private String streamserver;

	@Autowired
	private CameraService cameraService;

	@Autowired
	private Environment env;
	SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
	SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat format2 = new SimpleDateFormat("yyyyMMddHHmmss");

	/*
	 * 查询摄像头
	 */
	@RequestMapping("qryCamera")
	public ResponseEntity<ModelMap> qryCamera(HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap) throws Exception {
		List<CameraSet> data = cameraService.getCameraNotNull();
		// FFMpegUtil.rtspPush(data,steamserver);
		for (CameraSet cs : data) {
			cs.setOip(cs.getIp());
			cs.setOaccount(cs.getAccount());
			cs.setOpassword(cs.getPassword());
			cs.setAccount(Base64EncryptUtil.encrypt(cs.getAccount()));
			cs.setPassword(Base64EncryptUtil.encrypt(cs.getPassword()));
			cs.setIp(Base64EncryptUtil.encrypt(cs.getIp()));
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("cameras", data);
		result.put("streamserver", streamserver);
		return setSuccessModelMap(modelMap, result);
	}

	/*
	 * 查询摄像头
	 */
	@RequestMapping("qry4gCamera")
	public ResponseEntity<ModelMap> qry4gCamera(HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap) throws Exception {
		List<CameraSet> data = cameraService.getCamera();
		return setSuccessModelMap(modelMap, data);
	}

	/*
	 * 查询摄像头
	 */
	@RequestMapping("qryCameraRemote")
	public ResponseEntity<ModelMap> qryCameraRemote(HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap) throws Exception {
		List<CameraSet> data = cameraService.getCamera();
		for (CameraSet cs : data) {
			String account = cs.getAccount();
			String password = cs.getPassword();
			cs.setAccount(Base64EncryptUtil.encrypt(cs.getAccount()));
			cs.setPassword(Base64EncryptUtil.encrypt(cs.getPassword()));
			System.out.println("加密前---------------");
			System.out.println(account + ":" + password + "@" + cs.getIp() + "加密前");
			System.out.println(Base64EncryptUtil.encrypt(account + ":" + password + "@" + cs.getIp()) + "加密后");
			System.out.println("加密前---------------");
			cs.setIp(Base64EncryptUtil.encrypt(account + ":" + password + "@" + cs.getIp()).replaceAll("/", ""));
		}
		return setSuccessModelMap(modelMap, data);
	}

	@RequestMapping("qryCameraFile")
	public ResponseEntity<ModelMap> qryCameraFile(HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap, @RequestBody CameraParam param) throws Exception {
		List<CameraVo> list = new ArrayList<CameraVo>();
		List<String> dateList = new ArrayList<String>();
		if (param.getStartTime() != null && param.getEndTime() != null) {
			dateList = getDates(param.getStartTime(), param.getEndTime());
		}
		File file = new File(env.getProperty("camera.dir"));
		String host = env.getProperty("play.camera.dir");
		File[] files = file.listFiles();
		if (files != null && files.length > 0) {
			if (dateList != null && dateList.size() > 0) {
				for (String st : dateList) {
					for (int i = 0; i < files.length; i++) {
						System.out.println(files[i].getName());
						if (files[i].getName().contains(st)) {
							File[] chdFiles = files[i].listFiles();
							if (chdFiles != null && chdFiles.length > 0) {
								for (File chdFile : chdFiles) {
									if (param.getIp() != null && !"".equals(param.getIp())
											&& chdFile.getName().contains(param.getIp())) {
										String[] fileNames = chdFile.getName().split("_");
										if (fileNames != null && fileNames.length == 3) {
											CameraVo vo = new CameraVo();
											vo.setFileName(fileNames[2]);
											vo.setIp(fileNames[1]);
											vo.setTime(format1.format(format2.parse(fileNames[0])));
											vo.setDir(files[i].getName());
											vo.setPath(chdFile.getName());
											/* vo.setDirPath(files[i].getPath()); */
											vo.setDirPath(host + st + "/" + chdFile.getName());
											list.add(vo);
										}
									} else {
										String[] fileNames = chdFile.getName().split("_");
										if (fileNames != null && fileNames.length == 3) {
											CameraVo vo = new CameraVo();
											vo.setFileName(fileNames[2]);
											vo.setIp(fileNames[1]);
											vo.setTime(format1.format(format2.parse(fileNames[0])));
											vo.setDir(files[i].getName());
											vo.setPath(chdFile.getName());
											/* vo.setDirPath(files[i].getPath()); */
											vo.setDirPath(host + st + "/" + chdFile.getName());
											list.add(vo);
										}
									}
								}
							}
						}
					}
				}

			} else {

				for (int i = 0; i < files.length; i++) {
					File[] chdFiles = files[i].listFiles();
					if (chdFiles != null && chdFiles.length > 0) {
						for (File chdFile : chdFiles) {
							if (param.getIp() != null && !"".equals(param.getIp())
									&& chdFile.getName().contains(param.getIp())) {
								String[] fileNames = chdFile.getName().split("_");
								if (fileNames != null && fileNames.length == 3) {
									CameraVo vo = new CameraVo();
									vo.setFileName(fileNames[2]);
									vo.setIp(fileNames[1]);
									vo.setTime(format1.format(format2.parse(fileNames[0])));
									vo.setDir(files[i].getName());
									vo.setPath(chdFile.getName());
									/* vo.setDirPath(files[i].getPath()); */
									vo.setDirPath(host + format.format(format2.parse(fileNames[0])) + "/"
											+ chdFile.getName());
									list.add(vo);
								}
							} else {
								String[] fileNames = chdFile.getName().split("_");
								if (fileNames != null && fileNames.length == 3) {
									CameraVo vo = new CameraVo();
									vo.setFileName(fileNames[2]);
									vo.setIp(fileNames[1]);
									vo.setTime(format1.format(format2.parse(fileNames[0])));
									vo.setDir(files[i].getName());
									vo.setPath(chdFile.getName());
									/* vo.setDirPath(files[i].getPath()); */
									vo.setDirPath(host + format.format(format2.parse(fileNames[0])) + "/"
											+ chdFile.getName());
									list.add(vo);
								}
							}
						}
					}

				}

			}
		}

		return setSuccessModelMap(modelMap, list);
	}

	public List<String> getDates(Date startTime, Date endTime) throws Exception {
		List<String> dateList = new ArrayList<String>();
		dateList.add(format.format(startTime));
		Calendar cal = Calendar.getInstance();
		cal.setTime(format.parse(format.format(startTime)));
		while (cal.getTime().compareTo(format.parse(format.format(endTime))) < 0) {
			cal.add(Calendar.DATE, 1);
			dateList.add(format.format(cal.getTime()));
		}
		return dateList;
	}

	@RequestMapping("deleeteCameraFile")
	public ResponseEntity<ModelMap> deleeteCameraFile(HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap, @RequestBody CameraVo vo) throws Exception {
		File file = new File(env.getProperty("camera.dir") + "/" + vo.getDir() + "/" + vo.getPath());
		if (file.exists()) {
			if (file.delete()) {
				return setSuccessModelMap(modelMap, null);
			} else {
				throw new Exception("删除失败");
			}
		} else {
			throw new Exception("文件不存在");
		}
	}
}
