package cn.stronglink.esm27.web.accidentresource.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Resource;
import cn.stronglink.esm27.web.accidentresource.domain.MapImage;
import cn.stronglink.esm27.web.accidentresource.mapper.ResourceMapper;

@Service
@Transactional(rollbackFor=Exception.class)
public class AccidentResourceService {
	
	@Autowired
	private ResourceMapper resourceMapper;
	
	/**
	 * 查询 资源<br>
	 * 查询sessionId匹配，或者为终态的图片。(sql体现)
	 * @param sessionId
	 * @return
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<Resource> queryResource(Map<String, Object> params){
		List<Resource> records = resourceMapper.queryResource(params);
		return records;
	}
	
	/**
	 * 修改 资源
	 * @param record
	 * @return
	 * @throws Exception 
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<Resource> updateResource(List<MapImage> records){
		List<Resource> list = new ArrayList<Resource>();
		if (records != null && records.size() > 0) {
			for (MapImage rec : records) {
				if (StringUtils.isBlank(rec.getImageId())) {
					throw new BusinessException("无图片id");
				}
				Long id =  Long.decode(rec.getImageId());
				Resource resource = resourceMapper.selectByPrimaryKey(id);
				resource.setPosition(rec.getExtent());
				resource.setInMap("1");	
				resource.setUpdateTime(new Date());
				resourceMapper.updateByPrimaryKeyWithBLOBs(resource);
				list.add(resource);
			}
		}
		return list;
	}
	
	/**
	 * 新增 资源
	 * @param record
	 * @return
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Resource addResource(Map<String, Object> params) {
		String filePath = params.get("filePath").toString();
		String fileName = params.get("fileName").toString();
		String sessionId = params.get("sessionId").toString();
		int type = Integer.parseInt(params.get("type").toString());
		Resource resource = new Resource();
		resource.setId(IdWorker.getId());
		resource.setResourceName(fileName);
		resource.setResourcePath(filePath);
		resource.setType(type);
		resource.setSessionId(sessionId);
		resource.setCreateTime(new Date());
		resource.setUpdateTime(new Date());
		resource.setCreateUser(WebUtil.getCurrentUser());
		resource.setIsUse("1");
		resourceMapper.insert(resource);
		return resource;
	}
	
	/**
	 * 根据key删除资源
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Integer delResourceById(Map<String, Object> params) {
		Integer data = resourceMapper.deleteByPrimaryKey(Long.parseLong(params.get("id").toString()));
		return data;
	}

	public List<Resource> qryResourceListInMap(Map<String, Object> params) {
		return resourceMapper.qryResourceListInMap(params);
	}
}
