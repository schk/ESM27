package cn.stronglink.esm27.web.realTimeData.vo;

import cn.stronglink.esm27.entity.GasHistoryDataResult;

/**
 * @author bmc
 *
 */
public class GasHistoryDataResultVo extends GasHistoryDataResult{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String serial;
	private String timeString;
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public String getTimeString() {
		return timeString;
	}
	public void setTimeString(String timeString) {
		this.timeString = timeString;
	}
	
}
