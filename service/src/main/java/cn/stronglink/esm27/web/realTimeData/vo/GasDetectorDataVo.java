package cn.stronglink.esm27.web.realTimeData.vo;

import cn.stronglink.esm27.entity.GasDetectorData;

public class GasDetectorDataVo extends GasDetectorData{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String gasName;
	
	private String gasUnit;
	
	private Double gasLow;
	
	private Double gasUp;
	
	private Integer gasIsAlert;
	
	private Double gasValue;
	
	private Integer molecular;
	
	private Double pLow;
	
	private Double pUp;
	
	

	public Double getpLow() {
		return pLow;
	}

	public void setpLow(Double pLow) {
		this.pLow = pLow;
	}

	public Double getpUp() {
		return pUp;
	}

	public void setpUp(Double pUp) {
		this.pUp = pUp;
	}

	public Integer getMolecular() {
		return molecular;
	}

	public void setMolecular(Integer molecular) {
		this.molecular = molecular;
	}

	public String getGasUnit() {
		return gasUnit;
	}

	public void setGasUnit(String gasUnit) {
		this.gasUnit = gasUnit;
	}

	public Integer getGasIsAlert() {
		return gasIsAlert;
	}

	public void setGasIsAlert(Integer gasIsAlert) {
		this.gasIsAlert = gasIsAlert;
	}

	public Double getGasValue() {
		return gasValue;
	}

	public void setGasValue(Double gasValue) {
		this.gasValue = gasValue;
	}

	public Double getGasLow() {
		return gasLow;
	}

	public void setGasLow(Double gasLow) {
		this.gasLow = gasLow;
	}

	public Double getGasUp() {
		return gasUp;
	}

	public void setGasUp(Double gasUp) {
		this.gasUp = gasUp;
	}

	public String getGasName() {
		return gasName;
	}

	public void setGasName(String gasName) {
		this.gasName = gasName;
	}
	
}
