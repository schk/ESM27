package cn.stronglink.esm27.web.cameraSet.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.esm27.entity.CameraSet;
import cn.stronglink.esm27.web.cameraSet.mapper.CameraSetMapper;

@Service
@Transactional(rollbackFor=Exception.class) 
public class CameraSetService {
	
	@Autowired
	private CameraSetMapper cameraSetMapper;
	
	/*
	 * 删除对象
	 */
	public void delete(Long id){
		cameraSetMapper.deleteById(id);
	}
	
	/**
	 * 添加相机信息
	 * @param list
	 */
	public void insert(List<CameraSet> list){
		cameraSetMapper.deleteAll();
		if(list.size()>0){
			for(CameraSet entity:list){
				entity.setId(IdWorker.getId());
				entity.setCreateTime(new Date());
				cameraSetMapper.insert(entity);
			}
		}
	}
	
	/*
	 * 修改信息
	 */
	public void update(CameraSet entity){
		entity.setUpdateTime(new Date());
		cameraSetMapper.updateById(entity);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public CameraSet qryById(String serial) {
		return cameraSetMapper.qryBySerial(serial);
	}

	/**
	 * 查询所有相机
	 * @return
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<CameraSet> qryAll() {
		return cameraSetMapper.selectList(null);
	}
	
	
	

}
