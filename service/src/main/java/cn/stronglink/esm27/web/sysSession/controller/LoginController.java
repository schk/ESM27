package cn.stronglink.esm27.web.sysSession.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.Constants;
import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.support.Assert;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.core.support.LoginHelper;
import cn.stronglink.core.util.MD5Encrypt;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.SysOperateLog;
import cn.stronglink.esm27.entity.SysSession;
import cn.stronglink.esm27.entity.User;
import cn.stronglink.esm27.module.system.log.service.SysLogService;
import cn.stronglink.esm27.module.system.user.service.UserService;
import cn.stronglink.esm27.web.sysSession.service.SysSessionService;
import cn.stronglink.esm27.web.webData.service.WebDataService;

@Controller
@RequestMapping(value = "")
public class LoginController extends AbstractController  {
	
	@Autowired
	private SysSessionService sysSessionService;
	@Autowired
	private UserService userService;
	@Autowired
	private WebDataService webDataService;
	@Autowired
	private SysLogService sysLogService;
//	@Autowired
//	private PermissionService permissionService;

	@Value("${application.version}")
	private String appVersion;

	@RequestMapping("webApi/login")
    public String login(Model model) {
		model.addAttribute("version", appVersion);
        return "login";
    }
	
	/**
	 * 前台登录
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "webApi/doLoginEsm")
	public ResponseEntity<ModelMap> loginEsm(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {	
		Assert.notNull(params.get("username"), "USERNAME");
		Assert.notNull(params.get("password"), "PASSWORD");
		User u = null;
		if (LoginHelper.login(params.get("username").toString(), params.get("password").toString())) {		
			//List<String> permissions = userService.getSysPermissionByUsername(params.get("username").toString());
			u =userService.findUserByName(params.get("username").toString());
//			Set<String> perms = permissionService.getPermsByUserId(u.getId());
//			u.setPermset(perms);
			//同用户不能登录验证
			/*String ip = request.getRemoteAddr();
			String sessionId = request.getSession().getId();
			Date d = new Date();
			long currtime = d.getTime();
			String ipAcount = ip+"_"+u.getAccount()+"_"+sessionId+"_"+currtime;
			ServletContext application = request.getServletContext();
			List<String> loginUser=(List<String>)application.getAttribute("loginUser");
			List<String> newLoginUser = new ArrayList<String>();
			if (loginUser!=null && loginUser.size()>0) {
				int a = 0;
				for (String ipuser : loginUser) {
					String[] result = ipuser.split("_");
					if (result[1].equals(u.getAccount())) {
						a++;
						if ((long)currtime-(long)60000<(long)Long.valueOf(result[3])) {
							if (!result[0].equals(ip)) {
								return setModelMap(modelMap, HttpCode.LOGIN_FAIL, "此用户已登录，请使用其他账号登录");
							}
							if (!result[2].equals(sessionId)) {
								return setModelMap(modelMap, HttpCode.LOGIN_FAIL, "此用户已登录，请使用其他账号登录");
							}
						}else {
							ipuser = ipAcount;
						}
						newLoginUser.add(ipuser);
					}else {
						newLoginUser.add(ipuser);
					}
				}
				if (a==0) {
					newLoginUser.add(ipAcount);
					application.setAttribute("loginUser", newLoginUser);
				}else {
					application.setAttribute("loginUser", newLoginUser);
				}
				
			}else {
				List<String> loginUserList = new ArrayList<String>();
				loginUserList.add(ipAcount);
				application.setAttribute("loginUser", loginUserList);
			}
			*/ 
			SysOperateLog log = new SysOperateLog();
			log.setArgs("login[参数1，类型：LoginParam，值：{getUsername : " + params.get("username") 
			+ ",getPassword : " + MD5Encrypt.encrypt(params.get("password").toString())+ "}]");
			log.setId(IdWorker.getId());
			log.setUserName(params.get("username").toString());
			log.setModule("登录");
			log.setOperateTime(new Date());
			log.setOperateType("登录");
			log.setDescription("登录系统");
			log.setIp(WebUtil.getHost(request));
			log.setActName(u.getName());
			//System.out.println(MD5Encrypt.encrypt("123456"));
			sysLogService.insertOperateLog(log);
			sysSessionService.delSessionByUserId(u.getId());
			SysSession sys = new SysSession();
			sys.setId(IdWorker.getId());
			sys.setUserId(u.getId());
			sys.setCreateTime(new Date());
			sys.setIp(getIpAddr(request));
			sys.setSource(1);
			sys.setSessionId(request.getSession().getId());
			sysSessionService.insertSysSession(sys);
			request.getSession().setAttribute("admin", u.isAdmin());
			request.getSession().setAttribute("isLoadSession", "0");
			request.getSession().setAttribute("currentUser", u);
			SecurityUtils.getSubject().getSession().setAttribute(Constants.CURRENT_USER, u.getId());
		}else {
			return setModelMap(modelMap, HttpCode.CONFLICT, "登录失败");
		}	
		return setSuccessModelMap(modelMap,u);
	}
	
	
	/**
	 * 前台登出
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "webApi/logoutEsm")
	public ResponseEntity<ModelMap> logOutEsm(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession();
		if(session.getAttribute("roomId")!=null){ 
			String oId = session.getAttribute("roomId").toString();
			if(!"".equals(oId)&&oId!=null){
				Long roomId = Long.valueOf(oId);
				User user = (User)session.getAttribute("currentUser");
				SysSession sys = new SysSession();
				sys.setRoomId(null);
				sys.setUserId(user.getId());
				//查session表的room人员
				Integer roomUserCount = sysSessionService.selectByRoomId(roomId);
				if(roomUserCount<=0){
					//改变房间的状态为 关闭
					webDataService.updateRoom(roomId);					
				}
			}	
		}
		/*String ip = request.getRemoteAddr();
		ServletContext application = request.getServletContext();
		List<String> loginUser=(List<String>)application.getAttribute("loginUser");
		User user = (User)session.getAttribute("currentUser");
		String sessionId = request.getSession().getId();
		String ipAcount = ip+"_"+user.getAccount()+"_"+sessionId;
		if (loginUser!=null && loginUser.size()>0) {
			for (int i = 0; i < loginUser.size(); i++) {
				String[] result = loginUser.get(i).split("_");
				String addr =  result[0]+"_"+result[1]+"_"+result[2];
				if (addr.equals(ipAcount) ) {
					loginUser.remove(i);
				}
			}
		}
		application.setAttribute("loginUser", loginUser);*/
		Subject currentUser = SecurityUtils.getSubject(); 
		currentUser.logout();
		request.getSession().removeAttribute("admin");
		request.getSession().removeAttribute("isLoadSession");
		request.getSession().removeAttribute("currentUser");
		return setSuccessModelMap(modelMap, null);
	}
	
	

	/**
	 * 退出房间清空房间session
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "webApi/logoutRoom")
	public ResponseEntity<ModelMap> logOutRoom(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		HttpSession session = request.getSession();
		//清空当前用户session表
	    //清空session
		if(session.getAttribute("roomId")!=null){
			String oId = session.getAttribute("roomId").toString();
			if(!"".equals(oId)&&oId!=null){
				request.getSession().removeAttribute("roomId");
				request.getSession().removeAttribute("roomName");
				Long roomId = Long.valueOf(oId);
				User user = (User)session.getAttribute("currentUser");
				SysSession sys = new SysSession();
				sys.setRoomId(null);
				sys.setUserId(user.getId());
				sysSessionService.updateSysSession(sys);
				//查session表的room人员
				Integer roomUserCount = sysSessionService.selectByRoomId(roomId);
				if(roomUserCount<=0){
					//改变房间的状态为 关闭
					webDataService.updateRoom(roomId);					
				}
				return setSuccessModelMap(modelMap);
			}	
		}
		return setModelMap(modelMap, null);
	}
	
	public static String getIpAddr(HttpServletRequest request) {     
	     String ip = request.getHeader("x-forwarded-for");     
	     if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {     
	         ip = request.getHeader("Proxy-Client-IP");     
	     }     
	      if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {     
	         ip = request.getHeader("WL-Proxy-Client-IP");     
	      }     
	     if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {     
	          ip = request.getRemoteAddr();     
	     }     
	     return ip;     
	}     
		
	
}
