package cn.stronglink.esm27.web.gasHarm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.esm27.entity.GasHarmValue;
import cn.stronglink.esm27.web.gasHarm.service.GasHarmService;

@Controller
@RequestMapping(value = "webApi/gasHarm")
public class GasHarmController extends AbstractController {
	
	@Autowired
	private GasHarmService  gasHarmService;
	
	/**
	 * 气体危害等级列表查询
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param params
	 * @return
	 */
	@RequestMapping("qryList")
	public ResponseEntity<ModelMap> qryList(HttpServletRequest request, HttpServletResponse response, 
			ModelMap modelMap){
		List<GasHarmValue> data = gasHarmService.qryList();
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询对象
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param id
	 * @return
	 */
	@RequestMapping("qryById")
	public ResponseEntity<ModelMap> qryById(HttpServletRequest request, HttpServletResponse response, 
			ModelMap modelMap,@RequestBody Long id){
		GasHarmValue data = gasHarmService.qryById(id);
		return setSuccessModelMap(modelMap, data);
	}
	
	/*
	 * 根据id删除信息
	 */
	@RequestMapping(value = "remove")
	public ResponseEntity<ModelMap> remove(HttpServletRequest request, HttpServletResponse response, 
			ModelMap modelMap,@RequestBody Long id){
		gasHarmService.delete(id);
		return setSuccessModelMap(modelMap, null);
	}
	/*
	 * 添加信息
	 */
	@RequestMapping(value = "create")
	public ResponseEntity<ModelMap> create(HttpServletRequest request, HttpServletResponse response, 
			ModelMap modelMap , @RequestBody GasHarmValue entity){
		gasHarmService.insert(entity);
		return setSuccessModelMap(modelMap, null);
	}
	/*
	 * 修改信息
	 */
	@RequestMapping(value = "edit")
	public ResponseEntity<ModelMap> edit(HttpServletRequest request, HttpServletResponse response, 
			ModelMap modelMap, @RequestBody GasHarmValue entity){
		gasHarmService.update(entity);
		return setSuccessModelMap(modelMap, null);
		
	}

}
