package cn.stronglink.esm27.web.realTimeData.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.GasDetectorData;
import cn.stronglink.esm27.web.realTimeData.vo.GasDetectorDataVo;
import io.lettuce.core.dynamic.annotation.Param;

public interface GasDetectorDataMapper extends BaseMapper<GasDetectorData> {

	List<GasDetectorDataVo> selectAllData();

	List<GasDetectorDataVo> selectByDeviceAddress(@Param("gdNo") String deviceAddress);

	List<GasDetectorDataVo> getTestData();

	Integer openLock(@Param("psd") String psd);

}
