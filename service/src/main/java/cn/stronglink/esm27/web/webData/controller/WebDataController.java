package cn.stronglink.esm27.web.webData.controller;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Accident;
import cn.stronglink.esm27.entity.Dictionary;
import cn.stronglink.esm27.entity.EmergencyMaterial;
import cn.stronglink.esm27.entity.Equipment;
import cn.stronglink.esm27.entity.EquipmentType;
import cn.stronglink.esm27.entity.Facilities;
import cn.stronglink.esm27.entity.FireBrigade;
import cn.stronglink.esm27.entity.IncidentRecord;
import cn.stronglink.esm27.entity.KeyUnit;
import cn.stronglink.esm27.entity.KeyUnitDangers;
import cn.stronglink.esm27.entity.Room;
import cn.stronglink.esm27.entity.SocialResource;
import cn.stronglink.esm27.entity.SysSession;
import cn.stronglink.esm27.entity.Teams;
import cn.stronglink.esm27.entity.User;
import cn.stronglink.esm27.module.accident.service.AccidentCaseService;
import cn.stronglink.esm27.module.accident.vo.FireCaseVo;
import cn.stronglink.esm27.module.danger.danger.vo.DangersVo;
import cn.stronglink.esm27.module.emergencyResources.fireBrigade.service.FireBrigadeService;
import cn.stronglink.esm27.module.emergencyResources.reservePoint.vo.ReservePointVo;
import cn.stronglink.esm27.module.equipment.equipment.vo.EquipmentVo;
import cn.stronglink.esm27.module.expert.expert.vo.ExpertTypeVo;
import cn.stronglink.esm27.module.expert.expert.vo.ExpertVo;
import cn.stronglink.esm27.module.facilities.vo.FacilitiesUnitVo;
import cn.stronglink.esm27.module.facilities.vo.FacilitiesVo;
import cn.stronglink.esm27.module.fireEngine.service.FireEngineService;
import cn.stronglink.esm27.module.fireEngine.vo.FireEngineEquipmentVo;
import cn.stronglink.esm27.module.fireEngine.vo.FireEngineVo;
import cn.stronglink.esm27.module.fireWaterSource.vo.FireWaterSourceVo;
import cn.stronglink.esm27.module.fireWaterSource.vo.WaterSourceUnitVo;
import cn.stronglink.esm27.module.plan.vo.PlanTypeVo;
import cn.stronglink.esm27.module.plan.vo.PlanVo;
import cn.stronglink.esm27.module.unit.service.KeyPartsService;
import cn.stronglink.esm27.module.unit.vo.KeyPartsVo;
import cn.stronglink.esm27.module.unit.vo.KeyUnitPartsVo;
import cn.stronglink.esm27.web.sysSession.service.SysSessionService;
import cn.stronglink.esm27.web.sysSession.vo.SysSessionVo;
import cn.stronglink.esm27.web.webData.params.IncidentRecordParam;
import cn.stronglink.esm27.web.webData.service.RoomService;
import cn.stronglink.esm27.web.webData.service.WebDataService;
import cn.stronglink.esm27.web.webData.util.CoordTranform;
import cn.stronglink.esm27.web.webData.vo.EmergencyMaterialStatistics;
import cn.stronglink.esm27.web.webData.vo.FireBridgeVo;
import cn.stronglink.esm27.web.webData.vo.IncidentRecordVo;
import cn.stronglink.esm27.web.webData.vo.MapVo;

@Controller
@RequestMapping(value = "webApi")
public class WebDataController extends AbstractController  {
	
	@Autowired
	private WebDataService webDataService;
	@Autowired
	private FireBrigadeService fireBrigadeService;
	@Autowired
	private RoomService roomService;
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private SysSessionService sysSessionService;
	@Autowired
	private KeyPartsService keyPartsService;
	@Autowired
	private AccidentCaseService accidentCaseService;
	@Autowired
	private FireEngineService fireEngineService;
	
	@Value("${esm.baidu.ak}")
    private  String ak;
	
	private DecimalFormat formate = new DecimalFormat("#.0");
	
	
	@GetMapping(value = "location")
	public String location( @RequestParam(value="lon") String lon,
			@RequestParam(value="lat") String lat,Model model) {
		model.addAttribute("x", lon);
		model.addAttribute("y", lat);
		return "map";
	}
	/**
	 * 查询地图上的数据
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryDataForMap")
	public ResponseEntity<ModelMap> qryDataForMap(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		String cate =request.getParameter("cate");
		String extent=request.getParameter("extent");
		String callback =request.getParameter("callback");
	    MapVo data = webDataService.qryDataForMap(cate,extent,callback);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询重点单位列表
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryList")
	public ResponseEntity<ModelMap> qryList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		if(params.get("nopage")!=null) {
			List<KeyUnit> data = webDataService.getKeyUnit();
			return setSuccessModelMap(modelMap, data);
		}else {
			@SuppressWarnings("unchecked")
			Page<KeyUnit> page = (Page<KeyUnit>) super.getPage(params);
			Page<KeyUnit> data = webDataService.getListByParams(page,params);
			return setSuccessModelMap(modelMap, data);
		}
	}
	
	/**
	 * 查询重点单位列表,分页
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryKeyUnitListKuan")
	public ResponseEntity<ModelMap> qryKeyUnitListKuan(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<FireBridgeVo> page = (Page<FireBridgeVo>) super.getPage(params);
		Page<FireBridgeVo> data = webDataService.qryKeyUnitListKuan(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	/**
	 * 查询重点单位列表,不分页
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryKeyUnitListKuanAll")
	public ResponseEntity<ModelMap> qryKeyUnitListKuanAll(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		List<FireBridgeVo> data = webDataService.qryKeyUnitListKuanAll(params);
		return setSuccessModelMap(modelMap, data);
	}
	
	
	/**
	 * 查询重点单位对象
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryKeyUnitById")
	public ResponseEntity<ModelMap> qryKeyUnitById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		KeyUnit data = webDataService.qryKeyUnitById(id);
		return setSuccessModelMap(modelMap, data);
	}
	/**
	 * 查询社会资源对象
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qrySocialResourceById")
	public ResponseEntity<ModelMap> qrySocialResourceById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		SocialResource data = webDataService.qrySocialResourceById(id);
		return setSuccessModelMap(modelMap, data);
	}
	
	
	
	/**
	 * 查询重点部位危化品详情
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryDangersById")
	public ResponseEntity<ModelMap> qryDangersById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		DangersVo data = webDataService.qryDangersById(id);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询重点单位危化品详情
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryKeyUnitDangersById")
	public ResponseEntity<ModelMap> qryKeyUnitDangersById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		KeyUnitDangers data = webDataService.qryKeyUnitDangersDetailById(id);
		return setSuccessModelMap(modelMap, data);
	}
	
	
	/**
	 * 查询消防设施对象
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryFacilitiesById")
	public ResponseEntity<ModelMap> qryFacilitiesById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		Facilities data = webDataService.qryFacilitiesById(id);
		return setSuccessModelMap(modelMap, data);
	}
	
	
	/**
	 * 查询消防水源对象
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryWaterSourseById")
	public ResponseEntity<ModelMap> qryWaterSourseById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		FireWaterSourceVo data = webDataService.qryWaterSourseById(id);
		return setSuccessModelMap(modelMap, data);
	}
	

	/**
	 * 应急队伍对象
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryTeamsById")
	public ResponseEntity<ModelMap> qryTeamsById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		Teams data = webDataService.qryTeamsById(id);
		return setSuccessModelMap(modelMap, data);
	}
	

	/**
	 * 查询当前用户信息
	 */
	@RequestMapping(value = "getUserBrigadeId")
	public ResponseEntity<ModelMap> getUserBrigadeId(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response)  {
		Long brigadeId = webDataService.getUserBrigadeId(WebUtil.getCurrentUser());
		return setSuccessModelMap(modelMap, brigadeId);
	}
	
	/**
	
	/**
	 * 消防队伍对象
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryFireBrigadeById")
	public ResponseEntity<ModelMap> qryFireBrigadeById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		FireBrigade data = fireBrigadeService.qryById(id);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 消防水源列表数据
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")  
	@RequestMapping(value = "qryFireWaterSource")
	public ResponseEntity<ModelMap> qryFireWaterSource(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		String datatype="1";
		if(params.get("soureType")!=null){
			datatype= params.get("soureType").toString();//请求的类型 1 消防水源 2消防设备
		}
		if("1".equals(datatype)) {
			Page<FireWaterSourceVo> page = (Page<FireWaterSourceVo>) super.getPage(params);
			Page<FireWaterSourceVo> data = webDataService.qryFireWaterSource(page,params);
			return setSuccessModelMap(modelMap, data);
		}else if("2".equals(datatype)){
			Page<FacilitiesVo> page = (Page<FacilitiesVo>) super.getPage(params);
			Page<FacilitiesVo> data = webDataService.qryFacilities(page,params);
			return setSuccessModelMap(modelMap, data);
		}
		else {
			return setSuccessModelMap(modelMap, null);
		}
	}
	
	
	/**
	 * 消防水源列表数据（按照重点单位分）
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")  
	@RequestMapping(value = "qryFireWaterSourceGroupKeyUnit")
	public ResponseEntity<ModelMap> qryFireWaterSourceGroupKeyUnit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		String datatype="1";
		if(params.get("soureType")!=null){
			datatype= params.get("soureType").toString();//请求的类型 1 消防水源 2消防设备
		}
		if("1".equals(datatype)) {
			Page<WaterSourceUnitVo> page = (Page<WaterSourceUnitVo>) super.getPage(params);
			Page<WaterSourceUnitVo> data = webDataService.qryFireWaterSourceGroupKeyUnit(page,params);
			return setSuccessModelMap(modelMap, data);
		}else if("2".equals(datatype)){
			Page<FacilitiesUnitVo> page = (Page<FacilitiesUnitVo>) super.getPage(params);
			Page<FacilitiesUnitVo> data = webDataService.qryFacilitiesGroupKeyUnit(page,params);
			return setSuccessModelMap(modelMap, data);
		}
		else {
			return setSuccessModelMap(modelMap, null);
		}
	}
	
	
	/**
	 * 消防水源类型
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "GetManageSoureByType")
	public ResponseEntity<ModelMap> GetManageSoureByType(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		String datatype= params.get("type").toString();//请求的类型 1 消防水源 2消防设备
		if("1".equals(datatype)) {
			List<Dictionary> data = webDataService.qryDictionaryByType(6);
			return setSuccessModelMap(modelMap, data);
		}else if("2".equals(datatype)){
			List<EquipmentType> data = webDataService.qryEquipmentTypeList(1);
			return setSuccessModelMap(modelMap, data);
		}else {
			return setSuccessModelMap(modelMap, null);
		}
		//return setSuccessModelMap(modelMap, data);
	}
	
	
	/**
	 * 查询危化品库列表
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryDangerChemic")
	public ResponseEntity<ModelMap> qryDangerChemic(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<DangersVo> page = (Page<DangersVo>) super.getPage(params);
		Page<DangersVo> data = webDataService.qryDangerChemic(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询智能推荐危化品库列表
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryZhntjDangerChemic")
	public ResponseEntity<ModelMap> qryZhntjDangerChemic(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		List<DangersVo> data = webDataService.qryZhntjDangerChemic(params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询应急队伍
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryTeams")
	public ResponseEntity<ModelMap> qryTeams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<Teams> page = (Page<Teams>) super.getPage(params);
		Page<Teams> data = webDataService.qryTeams(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询消防队伍
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryFireBrigade")
	public ResponseEntity<ModelMap> qryFireBrigade(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<FireBrigade> page = (Page<FireBrigade>) super.getPage(params);
		Page<FireBrigade> data = fireBrigadeService.qryListByParamsForWeb(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	/**
	 * 查询应急装备
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryFireEngine")
	public ResponseEntity<ModelMap> qryFireEngine(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<FireEngineVo> page = (Page<FireEngineVo>) super.getPage(params);
		Page<FireEngineVo> data = webDataService.qryFireEngine(page,params);
		return setSuccessModelMap(modelMap, data);
	}	
	
	
	/**
	 * 查询消防车(按照消防队查询消防车信息和消防车得泡沫)
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryFireEngineByFireBridge")
	public ResponseEntity<ModelMap> qryFireEngineByFireBridge(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<FireBridgeVo> page = (Page<FireBridgeVo>) super.getPage(params);
		Page<FireBridgeVo> data = webDataService.qryFireEngineByFireBridge(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	
	/**
	 * 根据组织机构查询应急装备
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryFireEngineByDept")
	public ResponseEntity<ModelMap> qryFireEngineByDept(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		List<FireEngineVo> data = webDataService.qryFireEngineByDept(params);
		return setSuccessModelMap(modelMap, data);
	}
	

	@RequestMapping(value = "qryFireEngineEquipmentByfId")
	public ResponseEntity<ModelMap> qryFireEngineEquipmentByfId(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long fireEngineId) {
		FireEngineVo vo=fireEngineService.qryById(fireEngineId);
		List<FireEngineEquipmentVo> data = webDataService.qryFireEngineEquipmentByfId(fireEngineId);
		modelMap.put("fireEngineVo", vo);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询物资储备点
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryReservePoint")
	public ResponseEntity<ModelMap> qryReservePoint(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<ReservePointVo> page = (Page<ReservePointVo>) super.getPage(params);
		Page<ReservePointVo> data = webDataService.qryReservePoint(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询物资储备点详情
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryReservePointDetail")
	public ResponseEntity<ModelMap> qryReservePointDetail(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long pointId) {
		List<EmergencyMaterial> data = webDataService.qryReservePointDetail(pointId);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询物资储备点下得物资  带分页
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryWuziListByPage")
	public ResponseEntity<ModelMap> qryWuziListByPage(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<EmergencyMaterial> page = (Page<EmergencyMaterial>) super.getPage(params);
		Page<EmergencyMaterial> data = webDataService.qryWuziListByPage(page,params);
		return setSuccessModelMap(modelMap, data);
		
	}
	
	/**
	 * 查询物资储备点下得物资 不分页查全部
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryWuziListAll")
	public ResponseEntity<ModelMap> qryWuziListAll(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		List<EmergencyMaterial> data = webDataService.qryWuziListAll(params);
		return setSuccessModelMap(modelMap, data);
		
	}
	
	/**
	 * 查询智能推荐应急物资
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryZhntjEmergencyMaterial")
	public ResponseEntity<ModelMap> qryZhntjEmergencyMaterial(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		List<EmergencyMaterialStatistics> emergencyMaterialStatistics = webDataService.qryEmergencyMaterialStatistics(params);
		return setSuccessModelMap(modelMap, emergencyMaterialStatistics);
		
	}
	
	/**
	 * 修改物资储备点储存数量,记录物资使用记录
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @param storageQuantity
	 * @return
	 */
	@RequestMapping(value = "updateEmergencyMaterial")
	public ResponseEntity<ModelMap> updateEmergencyMaterial(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, String> params) {
		HttpSession session = request.getSession();
		if(session.getAttribute("roomId")!=null){
			String roomId = session.getAttribute("roomId").toString();
			if(!"".equals(roomId)&&roomId!=null){
				Long id = Long.valueOf(params.get("id"));
				Integer storageQuantity = Integer.valueOf(params.get("storageQuantity"));
				webDataService.updateEmergencyMaterial(id,storageQuantity,roomId);
				return setSuccessModelMap(modelMap);
			}	
		}
		return setModelMap(modelMap, HttpCode.CONFLICT, "当前没有房间信息");
	}	
	
	/**
	 * 查询应急专家带分页
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryExpertListPage")
	public ResponseEntity<ModelMap> qryExpertListPage(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<ExpertVo> page = (Page<ExpertVo>) super.getPage(params);
		Page<ExpertVo> data = webDataService.qryExpertListPage(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询应急专家
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryExpertList")
	public ResponseEntity<ModelMap> qryExpertList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		List<ExpertTypeVo> data = webDataService.qryExpertList(params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**带分页
	 * 查询应急预案
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryPlansPage")
	public ResponseEntity<ModelMap> qryPlansPage(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
	    @SuppressWarnings("unchecked")
		Page<PlanVo> page = (Page<PlanVo>) super.getPage(params);
		Page<PlanVo> data = webDataService.qryPlansPage(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	
	/**
	 * 查询应急预案
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryPlans")
	public ResponseEntity<ModelMap> qryPlans(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
	    @SuppressWarnings("unchecked")
		Page<PlanTypeVo> page = (Page<PlanTypeVo>) super.getPage(params);
		Page<PlanTypeVo> data =webDataService.qryPlans(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询生产装置
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryEquipmentList")
	public ResponseEntity<ModelMap> qryEquipment(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<EquipmentVo> page = (Page<EquipmentVo>) super.getPage(params);
		Page<EquipmentVo> data = webDataService.qryEquipmentList(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询重点部位列表
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "qryKeyPartsList")
	public ResponseEntity<ModelMap> qryListByParams(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		Page<KeyPartsVo> page = (Page<KeyPartsVo>) super.getPage(params);
		Page<KeyPartsVo> data = keyPartsService.qryKeyPartsList(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 查询重点部位列表详情
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryKeyPartsById")
	public ResponseEntity<ModelMap> qryKeyPartsById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		KeyPartsVo data = keyPartsService.qryKeyPartsById(id);
		return setSuccessModelMap(modelMap, data);
	}
	
	
	
	/**
	 * 查询重点部位列表
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "qryKeyPartsGroupUnit")
	public ResponseEntity<ModelMap> qryKeyPartsGroupUnit(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		Page<KeyUnitPartsVo> page = (Page<KeyUnitPartsVo>) super.getPage(params);
		Page<KeyUnitPartsVo> data = keyPartsService.qryKeyPartsGroupUnit(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	
	/**
	 * 查询典型火灾列表
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "qryAccidentCaseList")
	public ResponseEntity<ModelMap> qryAccidentCaseList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		Page<FireCaseVo> page = (Page<FireCaseVo>) super.getPage(params);
		Page<FireCaseVo> data = accidentCaseService.qryAccidentCaseList(page,params);
		return setSuccessModelMap(modelMap, data);
	}
	
	
	/**
	 * 空间搜索
	 * @param dataType 查询的数据类型
	 * @param queryType 空间搜索类型{zb 周边搜索  extent 范围搜索}
	 * @param geometry {周边搜索时为：lon,lat,distance    范围搜索时: minx,miny,maxx,maxy}
	 * @return
	 * */
	@GetMapping("/spaceSearch")
	public @ResponseBody String spaceSearch(@RequestParam(value="keyword",required=false) String keyword,
			@RequestParam(value="dataType",required=true) String dataType,
			@RequestParam(value="queryType",required=true) String queryType,
			@RequestParam(value="geometry",required=true) String geometry,
			@RequestParam(value="isBaiduGd",required=false) boolean isBaiduGd){
		
		HashMap<String,Object> param = new HashMap<>();
		JSONObject geoCoder = null;
		if(queryType.equals("zb")){
			String[] params = geometry.split(",");
			double[] coord = {Double.parseDouble(params[0]),Double.parseDouble(params[1])};
			double degree = CoordTranform.meter2degree(Integer.valueOf(params[2]).intValue());
			param.put("point", CoordTranform.createGeom(coord));
			param.put("distance", degree);
			
			String location = coord[1]+","+coord[0];
			if(isBaiduGd){
				geoCoder = baiduGeoCoder(location);
			}
		}else if(queryType.equals("extent")){
			String[] points = geometry.split(",");
			double[] point_min = {Double.parseDouble(points[0]),Double.parseDouble(points[1])};
			double[] point_max = {Double.parseDouble(points[2]),Double.parseDouble(points[3])};
			double[] min = CoordTranform.mercator2lnglat(point_min);
			double[] max = CoordTranform.mercator2lnglat(point_max);
			double[] extent = {min[0],min[1],max[0],max[1]};
			param.put("polygon", CoordTranform.createGeom(extent));
		}
		MapVo mapVo = webDataService.qryDataForMapByGeom(dataType, param);
		JSONObject feature = JSONObject.parseObject(JSONObject.toJSONString(mapVo));
		JSONObject result = new JSONObject();
		if(geoCoder!=null){
			result.put("address", geoCoder.get("description"));
		}
		result.put("data", feature);
		return result.toJSONString();
	}
	
	/**
	 * 调用百度路径规划
	 * @param start 起点{lat,lon}
	 * @param end   终点 {lat,lon}
	 * @parma type 交通类型
	 * @return json格式的字符串
	 * */
	@GetMapping("/road")
	public @ResponseBody String roadData(@RequestParam(value="start") String origin,
			@RequestParam(value="end") String destination,@RequestParam(value="or") String origin_region,
			@RequestParam(value="dr") String destination_region,@RequestParam(value="region") String region,@RequestParam(value="type") String type){
		String url = "http://api.map.baidu.com/direction/v1?mode=" + type + "&origin=" + origin + "&destination=" + destination + "&coord_type=wgs84&ret_coordtype=gcj02&output=json&ak=" + ak;
		if(type.equals("driving")){
			url += "&origin_region=" + origin_region + "&destination_region=" + destination_region;
		}else if(type.equals("walking")||type.equals("riding")){
			url += "&region=" + region;
		}
		HttpHeaders headers = new HttpHeaders();
		MediaType mediaType = MediaType.parseMediaType("application/json; charset=UTF-8");
		headers.setContentType(mediaType);
		String json = restTemplate.getForEntity(url, String.class).getBody();
		JSONObject result = JSONObject.parseObject(json);
		return createGeoJSON(result).toJSONString();
	}
	
	@GetMapping("/geocoder")
	public @ResponseBody String geoCoder(@RequestParam(value="lon") float lon,@RequestParam(value="lat") float lat){
		String location = lat+","+lon;
		JSONObject result = baiduGeoCoder(location);
		return result.toJSONString();
	}
	
	/**
	 * 插入应急指挥记录
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param record
	 * @return
	 */
	@RequestMapping(value = "insertIncidentRecord")
	public ResponseEntity<ModelMap> insertIncidentRecord(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody IncidentRecordParam record) {
		webDataService.insertIncidentRecord(record);
		return setSuccessModelMap(modelMap, record);
	}
	
	/**
	 * 更新应急指挥记录
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param record
	 * @return
	 */
	@RequestMapping(value = "updateIncidentRecord")
	public ResponseEntity<ModelMap> updateIncidentRecord(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody IncidentRecordParam record) {
		HttpSession session = request.getSession();
		if(session.getAttribute("currentUser")!=null) {
			User user = (User)session.getAttribute("currentUser");
			record.setDirector(user.getName());
		}
		webDataService.updateIncidentRecord(record);
		return setSuccessModelMap(modelMap, record);
	}
	
	/**
	 * 删除应急指挥记录
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param record
	 * @return
	 */
	@RequestMapping(value = "deleteIncidentRecord")
	public ResponseEntity<ModelMap> deleteIncidentRecord(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		webDataService.deleteIncidentRecord(id);
		return setSuccessModelMap(modelMap, id);
	}

	/**
	 * 创建应急指挥事故
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param accident
	 * @return
	 */
	@RequestMapping(value = "insertAccident")
	public ResponseEntity<ModelMap> insertAccident(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Accident accident) {
		String roomId = accident.getRoomId();
		List<Accident> list = webDataService.getAccidentByRoomid(roomId);
		if(list.size()>0){
			Accident accidentN = list.get(0);
			accident.setId(accidentN.getId());
			webDataService.updateAccident(accident);
		}else{
			webDataService.insertAccident(accident);
		}
		return setSuccessModelMap(modelMap, accident);
	}
	
	@RequestMapping(value = "updateRenWuStatus")
	public ResponseEntity<ModelMap> updateRenWuStatus(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody IncidentRecord record) {
		webDataService.updateRenWuStatus(record);
		return setSuccessModelMap(modelMap, null);
	}
	/**
	 * 更新事故状态
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param accident
	 * @return
	 */
	@RequestMapping(value = "updateAccident")
	public ResponseEntity<ModelMap> updateAccident(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Accident accident) {
		webDataService.updateAccident(accident);
		return setSuccessModelMap(modelMap, accident);
	}
	
	/**
	 * 查询未完成应急指挥事故操作详情
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param accident
	 * @return
	 */
	@RequestMapping(value = "getIncidentRecordByAccident")
	public ResponseEntity<ModelMap> getIncidentRecordByAccident(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody String id) {
//		List<IncidentRecord> irs = webDataService.getIncidentRecordByAccident(id);
		List<IncidentRecordVo> irs = webDataService.getIncidentRecordByRoomId(id);
		return setSuccessModelMap(modelMap, irs);
	}
	
	/**
	 * 查询未完成应急指挥事故操作步骤
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param accident
	 * @return
	 */
	@RequestMapping(value = "getIncidentRecordDetail")
	public ResponseEntity<ModelMap> getIncidentRecordDetail(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody IncidentRecord incidentRecord) {		
		HttpSession session = request.getSession();
		//session.setAttribute("roomId", "1019444431976927234");
		if(session.getAttribute("roomId")!=null){
			String roomId = session.getAttribute("roomId").toString();
			if(!"".equals(roomId)&&roomId!=null){
				incidentRecord.setRoomId(Long.parseLong(roomId));
				List<IncidentRecord> irs = webDataService.getIncidentRecordDetail(incidentRecord);
				return setSuccessModelMap(modelMap, irs);
			}	
		}
		return setModelMap(modelMap, HttpCode.CONFLICT, "当前没有房间信息");
		
		
	}	
	
	public static String getIpAddr(HttpServletRequest request) {     
	     String ip = request.getHeader("x-forwarded-for");     
	     if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {     
	         ip = request.getHeader("Proxy-Client-IP");     
	     }     
	      if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {     
	         ip = request.getHeader("WL-Proxy-Client-IP");     
	      }     
	     if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {     
	          ip = request.getRemoteAddr();     
	     }     
	     return ip;     
	}     
	
	/**
	 * 添加房间
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param room
	 * @return
	 */
	@RequestMapping(value = "createRoom")
	public ResponseEntity<ModelMap> createRoom(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Room room) {		
		HttpSession session = request.getSession();
		 //添加一个房间记录		
		room.setStatus(1);
		room.setId(IdWorker.getId());

		if(room.getRoom()==null || "".equals(room.getRoom())) {
			int roomcode = (int)((Math.random()*9+1)*1000);
			room.setRoom(roomcode+"");
		}

		if(session.getAttribute("currentUser")!=null) {
			User user = (User)session.getAttribute("currentUser");
			if(user!=null) {
				room.setCreateBy(user.getId());
			}
		}
		webDataService.insertRoom(room);
		session.setAttribute("roomId",room.getId());
		session.setAttribute("roomName",room.getRoom());
		if(session.getAttribute("roomId")!=null){
			if(session.getAttribute("currentUser")!=null) {
				User user = (User)session.getAttribute("currentUser");
				if(user!=null) {
					SysSession sys = new SysSession();
					sys.setRoomId(room.getId());
					sys.setUserId(user.getId());
					sysSessionService.updateSysSession(sys);
				}
			}
		}
		return setSuccessModelMap(modelMap, room);
	}
	
	/**
	 * 指挥人变更
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param room
	 * @return
	 */
	@RequestMapping(value = "RoomChangeLeaders")
	public ResponseEntity<ModelMap> RoomChangeLeaders(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Room room) {		
		webDataService.RoomChangeLeaders(room);
		return setSuccessModelMap(modelMap, room);
	}
	

	/**
	 * 初始化后加入房间
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param room
	 * @return
	 */
	@RequestMapping(value = "checkRoom")
	public ResponseEntity<ModelMap> checkRoom(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		HttpSession session = request.getSession();
		Room room = null;
		//房间session和用户session存再
		if(session.getAttribute("roomId")!=null && session.getAttribute("currentUser")!=null){
			User user = (User)session.getAttribute("currentUser");
			if(user!=null) {
				long oId = Long.valueOf(session.getAttribute("roomId").toString());
				//找到当前用户是否在房间没有退出
				HashMap<String,Object> param = new HashMap<>();
				param.put("room_id", oId);
				param.put("session_id",request.getSession().getId());
				List<SysSession> sys = sysSessionService.querySysSession(param);
				//通过房间session找到房间信息 返回
				if(!"".equals(oId) && sys!=null && sys.size()>0){
					room = webDataService.getRoomByid(oId);
				}
			}
		}
		return setSuccessModelMap(modelMap, room);
	}
	/**
	 * 初始化后加入房间
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param room
	 * @return
	 */
	@RequestMapping(value = "getRoomUser")
	public ResponseEntity<ModelMap> getRoomUser(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		HttpSession session = request.getSession();
		Room room = null;
		long roomID = -1;
		//房间session和用户session存再
		if(params.get("roomId")!=null) {
			roomID = Long.valueOf(params.get("roomId").toString());
		}
		else if(session.getAttribute("roomId")!=null){
			roomID = Long.valueOf(session.getAttribute("roomId").toString());
		}
		HashMap<String,Object> param = new HashMap<>();
		param.put("roomId", roomID);
		List<SysSessionVo> sys = sysSessionService.getRoomUser(param);
		return setSuccessModelMap(modelMap, sys);
	}
	
	/**
	 * 加入房间
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param room
	 * @return
	 */
	@RequestMapping(value = "joinRoom")
	public ResponseEntity<ModelMap> joinRoom(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Room room) {
		HttpSession session = request.getSession();
		session.setAttribute("roomId",room.getId());
		session.setAttribute("roomName",room.getRoom());
		//加入一个session记录
		if(session.getAttribute("currentUser")!=null) {
			room =  webDataService.getRoomByid(room.getId());
			User user = (User)session.getAttribute("currentUser");
			if(user!=null) {
				//临时使用 用户名传到前台
				room.setAccidentName(user.getName());
				SysSession sys = new SysSession();
				sys.setRoomId(room.getId());
				sys.setUserId(user.getId());
				sysSessionService.updateSysSession(sys);
				room.setIsCreate((user.getId().equals(room.getCreateBy())+""));
			}
			
		}
		return setSuccessModelMap(modelMap, room);
	}
	
	
	/**
	 * 判断房间是否存在
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param room
	 * @return
	 */
	@RequestMapping(value = "roomExist")
	public ResponseEntity<ModelMap> roomExist(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession();
		if(session.getAttribute("roomId")!=null){
			String roomName = session.getAttribute("roomName").toString();
			String roomId = session.getAttribute("roomId").toString();
			Integer cout =roomService.getRoomById(roomId);
			if(cout>0) {
				modelMap.put("roomId", roomId);
				return setSuccessModelMap(modelMap, roomName);
			}else {
				return setModelMap(modelMap, HttpCode.CONFLICT, "当前没有房间信息");
			}
		}			
		return setModelMap(modelMap, HttpCode.CONFLICT, "当前没有房间信息");
	}
	
	/**
	 * 根据房间号查询房间得基本信息
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param room
	 * @return
	 */
	@RequestMapping(value = "getRoomInfo") 
	public ResponseEntity<ModelMap> getRoomInfo(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody String roomCode) {
		Room r=webDataService.getRoomInfo(roomCode);
		if(r!=null){  
			HttpSession session = request.getSession(); 
			if(session.getAttribute("currentUser")!=null) {
				User user = (User)session.getAttribute("currentUser");
				if(user!=null) {
					r.setIsCreate((user.getId().equals(r.getCreateBy())+""));
				}
			}
			return setSuccessModelMap(modelMap, r);   
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "当前没有房间信息");
		}
		
	}
	
	private JSONObject createGeoJSON(JSONObject json){
		JSONObject data = new JSONObject();
		JSONArray roads = new JSONArray();
		JSONObject result = json.getJSONObject("result");
		JSONArray routes = result.getJSONArray("routes");
		for(int n=0;n<routes.size();n++){
			
			JSONObject road = new JSONObject(true);
			JSONObject route = routes.getJSONObject(n);
			Long time = Long.valueOf(String.valueOf(route.getIntValue("duration")*1000));
			road.put("distance", formateDistance(route.getIntValue("distance")));
			road.put("duration", formatTime(time));
			
			JSONObject geojson = new JSONObject(true);
			geojson.put("type", "FeatureCollection");
			
			JSONArray steps = route.getJSONArray("steps");
			JSONArray features = new JSONArray();
			
			for(int i=0;i<steps.size();i++){
				
				JSONObject feature = new JSONObject(true);
				JSONObject step = steps.getJSONObject(i);
				feature.put("type", "Feature");
				String[] path = step.getString("path").split(";");
				double[][] coords = new double[path.length][2];
				for(int m=0;m<path.length;m++){
					String[] _coord = path[m].split(",");
					double[] coord = {Double.parseDouble(_coord[0]),Double.parseDouble(_coord[1])};
					coords[m] = CoordTranform.lnglat2mercator(CoordTranform.gcj02ToWgs84(coord));
				}
				JSONObject geometry = new JSONObject();
				geometry.put("type", "LineString");
				geometry.put("coordinates", coords);
				
				feature.put("geometry", geometry);
				
				JSONObject properties = new JSONObject();
				properties.put("traffic_status", step.getIntValue("traffic_condition"));
				properties.put("desc", step.getString("instruction"));
				feature.put("properties", properties);
				features.add(feature);
			}
			geojson.put("features", features);
			road.put("geojson", geojson);
			roads.add(road);
		}
		
		data.put("roads", roads);
		return data;
	}
	
	private JSONObject baiduGeoCoder(String location){
		String url = "http://api.map.baidu.com/geocoder/v2/?location="+location+"&output=json&pois=0&coord_type=wgs84&ak="+ak;
		HttpHeaders headers = new HttpHeaders();
		MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
		headers.setContentType(type);
		String json = restTemplate.getForEntity(url, String.class).getBody();
		JSONObject result = JSONObject.parseObject(json);
		JSONObject info = new JSONObject(true);
		info.put("status", result.get("status"));
		JSONObject _point = result.getJSONObject("result").getJSONObject("location");
		JSONObject _location = new JSONObject();
		_location.put("lon", _point.getDoubleValue("lng"));
		_location.put("lat", _point.getDoubleValue("lat"));
		info.put("location", _location);
		info.put("address", result.getJSONObject("result").get("formatted_address"));
		info.put("description", result.getJSONObject("result").get("sematic_description"));
		info.put("region", result.getJSONObject("result").getJSONObject("addressComponent").getString("city"));
		return info;
	}
	
	private String createRoadInfo(String roadNm,int distance,int direction){
		
		StringBuffer sb = new StringBuffer();
		if(direction==0){
			sb.append("直行，").append("进入").append(roadNm);
		}else if(direction==1||direction==2){
			sb.append("靠右前方行驶，").append("进入").append(roadNm);
		}else if(direction==3){
			sb.append("右转，").append("进入").append(roadNm);
		}else if(direction==4||direction==5){
			sb.append("右后转弯，").append("进入").append(roadNm);
		}else if(direction==6){
			sb.append("调头，").append("进入").append(roadNm);
		}else if(direction==7||direction==8){
			sb.append("左后转弯，").append("进入").append(roadNm);
		}else if(direction==9){
			sb.append("左转，").append("进入").append(roadNm);
		}else if(direction==10||direction==11){
			sb.append("靠左前方行驶，").append("进入").append(roadNm);
		}
		
		sb.append("，").append("行驶");
		sb.append(formateDistance(distance));
		return sb.toString();
	}
	
	private String formateDistance(int distance){
		StringBuffer sb = new StringBuffer();
		if(distance<1000){
			sb.append(distance).append("米");
		}else{
			String value = formate.format((distance*1.0/1000.0));
			sb.append(value).append("公里");
		}
		return sb.toString();
	}
	
	private String formatTime(Long ms) {
        Integer ss = 1000;
        Integer mi = ss * 60;
        Integer hh = mi * 60;
        Integer dd = hh * 24;
 
        Long day = ms / dd;
        Long hour = (ms - day * dd) / hh;
        Long minute = (ms - day * dd - hour * hh) / mi;
        
        StringBuffer sb = new StringBuffer();
        if(day > 0) {
            sb.append(day+"天");
        }
        if(hour > 0) {
            sb.append(hour+"小时");
        }
        if(minute > 0) {
            sb.append(minute+"分钟");
        }
        return sb.toString();
	}
	

	/**
	 * 查询生产装置的信息
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryEquipmentById")
	public ResponseEntity<ModelMap> qryEquipmentById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		Equipment data = webDataService.qryEquipmentById(id);
		return setSuccessModelMap(modelMap, data);
	}
	
	
	
	/**
	 * 查询生产装置的信息
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "getAccidentByRoomId")
	public ResponseEntity<ModelMap> getAccidentByRoomId(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody String roomId) {
		List<Accident> data = webDataService.getAccidentByRoomid(roomId);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 结束救援
	 */
	@RequestMapping(value="endRescue")
	public ResponseEntity<ModelMap> endRescue(ModelMap modelMap,HttpServletRequest request,
			HttpServletResponse response,@RequestBody String roomId){
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("currentUser");
		Map<String,Object> params = new HashMap<>();
		params.put("userId", user.getId());
		params.put("roomId", roomId);
		webDataService.endRescue(params);
		return setSuccessModelMap(modelMap,null);
	}
	
	/**
	 * 检测救援
	 * 
	 */
	
	@RequestMapping(value="checkEndRescue")
	public ResponseEntity<ModelMap> checkEndRescue(ModelMap modelMap,HttpServletRequest request,
			HttpServletResponse response,@RequestBody String roomId){
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("currentUser");
		Map<String,Object> params = new HashMap<>();
		params.put("userId", user.getId());
		params.put("roomId", roomId);
		Integer isEndRescue = webDataService.checkEndRescue(params);
		return setSuccessModelMap(modelMap,isEndRescue);
	}
	
	
	
	
}
