package cn.stronglink.esm27.web.webData.vo;

import java.util.List;

import cn.stronglink.esm27.module.emergencyResources.reservePoint.vo.ReservePointVo;

/**
 * 应急物资统计
 * 
 * @author tgb
 *
 */
public class EmergencyMaterialStatistics {
	private String materialName; // 物资名称
	private int total; // 总数
	private List<ReservePointVo> reservePointList;

	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<ReservePointVo> getReservePointList() {
		return reservePointList;
	}

	public void setReservePointList(List<ReservePointVo> reservePointList) {
		this.reservePointList = reservePointList;
	}

}
