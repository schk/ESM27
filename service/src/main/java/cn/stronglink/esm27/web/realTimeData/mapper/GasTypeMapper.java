package cn.stronglink.esm27.web.realTimeData.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.GasType;

public interface GasTypeMapper extends BaseMapper<GasType> {

	List<GasType> qryhistoryGasTableTitle();

	List<GasType> qryGasType();

}
