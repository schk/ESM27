package cn.stronglink.esm27.web.realTimeData.vo;

import java.util.List;

import cn.stronglink.esm27.entity.GasDetector;
import cn.stronglink.esm27.entity.GasType;
import cn.stronglink.esm27.entity.WeatherStation;

public class DeviceConfigureVo {

	private List<GasDetector> gasDetector;//气体检测仪
	private List<GasType> gasType;//气体类型
	private List<GasDetectorDataVo> gasDetectorDataVo;//气体数据设备
	private List<WeatherStation> weatherStation;//气象站
	
	
	public List<WeatherStation> getWeatherStation() {
		return weatherStation;
	}
	public void setWeatherStation(List<WeatherStation> weatherStation) {
		this.weatherStation = weatherStation;
	}
	public List<GasDetector> getGasDetector() {
		return gasDetector;
	}
	public void setGasDetector(List<GasDetector> gasDetector) {
		this.gasDetector = gasDetector;
	}
	public List<GasType> getGasType() {
		return gasType;
	}
	public void setGasType(List<GasType> gasType) {
		this.gasType = gasType;
	}
	public List<GasDetectorDataVo> getGasDetectorDataVo() {
		return gasDetectorDataVo;
	}
	public void setGasDetectorDataVo(List<GasDetectorDataVo> gasDetectorDataVo) {
		this.gasDetectorDataVo = gasDetectorDataVo;
	}
}
