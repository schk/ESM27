package cn.stronglink.esm27.web.webData.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.esm27.entity.MapCamera;
import cn.stronglink.esm27.module.mapCamera.service.MapCameraService;

@Controller
@RequestMapping(value = "webApi/mapCamera")
public class MapCarmeraController extends AbstractController  {
	
	@Autowired
	private MapCameraService mapCameraService;
	
	
	/**
	 * 保存截图
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param room
	 * @return
	 */
	@RequestMapping(value = "saveMapImg")
	public ResponseEntity<ModelMap> saveMapImg(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody MapCamera entity) {		
		mapCameraService.saveMapImg(entity);
		return setSuccessModelMap(modelMap, entity);
	}
	
}
