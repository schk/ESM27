package cn.stronglink.esm27.web.camera.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.stronglink.esm27.entity.CameraSet;
import cn.stronglink.esm27.web.camera.mapper.CameraMapper;
import cn.stronglink.esm27.web.cameraSet.mapper.CameraSetMapper;

@Service
@Transactional(rollbackFor=Exception.class) 
public class CameraService {
	
	@Autowired
	private CameraMapper cameraMapper;
	@Autowired
	private CameraSetMapper cameraSetMapper;
	
	/*
	 * 查询摄像头
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<CameraSet> getCamera(){
		List<CameraSet> list = cameraMapper.selectList(null);
		return list;	
	}

	public List<CameraSet> getCameraNotNull() {
		List<CameraSet> list = cameraSetMapper.getCameraNotNull();
		return list;	
	}
	
	

}
