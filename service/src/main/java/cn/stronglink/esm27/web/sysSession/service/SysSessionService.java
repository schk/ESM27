package cn.stronglink.esm27.web.sysSession.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.stronglink.esm27.entity.SysSession;
import cn.stronglink.esm27.web.sysSession.mapper.SysSessionMapper;
import cn.stronglink.esm27.web.sysSession.vo.SysSessionVo;

@Service
@Transactional(rollbackFor=Exception.class)
public class SysSessionService {
	
	@Autowired
	private SysSessionMapper sysSessionMapper;	
	
	public void insertSysSession(SysSession sys) {
		sysSessionMapper.insert(sys);
	}

	public List<SysSession> querySysSession(Map<String,Object> querysys) {
		return sysSessionMapper.selectByMap(querysys);
	}
	
	public List<SysSessionVo> getRoomUser(Map<String,Object> querysys) {
		return sysSessionMapper.getRoomUser(querysys);
	}
	public void updateSysSession(SysSession sys) {
		sysSessionMapper.updateSysSession(sys);
	}
	
	public void delUserBySessionId(String sessionId) {
		sysSessionMapper.deleteBySessionId(sessionId);
	}

	public Integer selectByRoomId(Long roomId) {
		return sysSessionMapper.selectByRoomId(roomId);
	}

	public SysSession selectSessionByUserId(Long userId, String sessionId) {
		return sysSessionMapper.selectSessionByUserId(userId,sessionId);
	}

	public SysSession selectExistSession(Long userId) {
		return sysSessionMapper.selectExistSession(userId);
	}

	public void delSessionByUserId(Long userId) {
		sysSessionMapper.delSessionByUserId(userId);
		
	}
	
}
