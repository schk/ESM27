package cn.stronglink.esm27.web.webData.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.esm27.entity.Room;
import cn.stronglink.esm27.module.fireEngine.service.FireEngineUseRecordService;
import cn.stronglink.esm27.web.webData.service.RoomService;

@Controller
@RequestMapping(value = "webApi/report")
public class IframeController extends AbstractController  {
	
	@Autowired
	private FireEngineUseRecordService fUrService;
	
	@Autowired
	private RoomService roomService;
	
	@RequestMapping("")
    public String login(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		String oId =request.getParameter("roomid").toString();
		if(!"".equals(oId)&&oId!=null){
			Map<String,Object> map=fUrService.getReportContent(oId);
		    model.addAttribute("reportContent", map);
			return "/report";
		}	
		model.addAttribute("errorMsg", "当前没有正在救援的房间");
        return "/report";
    }
	
	
	/**
	 * 保存报告内容
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "getCZLLData")
	public ResponseEntity<ModelMap> getCZLLData(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long roomId) {		
		Map<String,Object> data = fUrService.getCZLLData(roomId);
		System.out.println(data);
		return setSuccessModelMap(modelMap, data);
	}
	
	
	
	@RequestMapping("/accidentHistoryContent")
    public String accidentHistoryContent(HttpServletRequest request,HttpServletResponse response, Model model) {
		String accidentId = request.getParameter("accidentId");
		model.addAttribute("accidentId", accidentId);
        return "/currentHistory";
    }
	
	/**
	 * 当前事故的历史记录
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/accidentHistory")
    public String login(HttpServletRequest request,HttpServletResponse response, Model model) {
		String accidentId = request.getParameter("accidentId");
		model.addAttribute("accidentId", accidentId);
        return "/accidentHistory";
    }
	
	/**
	 * 保存报告内容
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "saveAccidentInfo")
	public ResponseEntity<ModelMap> saveAccidentInfo(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Room room) {		
		room.setStatus(2);
		roomService.saveAccidentInfo(room);
		return setSuccessModelMap(modelMap,room);
	}
	
	
	/**
	 * 跳转文档的ifram页面
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "flexPaper")
	public String flexPaper(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {		
		  return "/flexPaper";
	}
	
}
