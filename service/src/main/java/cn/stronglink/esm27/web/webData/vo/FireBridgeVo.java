package cn.stronglink.esm27.web.webData.vo;

import java.math.BigDecimal;
import java.util.List;

import cn.stronglink.esm27.entity.KeyUnit;
import cn.stronglink.esm27.module.fireEngine.vo.FireEngineVo;
public class FireBridgeVo {

	/**
	 * 前台消防车显示信息对象
	 */
	private Long fireBrigadeId;
	
	private String fireBrigadeName;
	
	private String stationLevel;
	
	private Integer fireEngineCount;
	
	private BigDecimal waterCount;
	
	private BigDecimal powderCount;    //干粉量
	
	private BigDecimal foamCount;
	
    private Integer keyUnitCount;
	
	private List<KeyUnit>  keyUnitList;
	
	private List<FireEngineVo>  fireEngineList;

	public Long getFireBrigadeId() {
		return fireBrigadeId;
	}

	public void setFireBrigadeId(Long fireBrigadeId) {
		this.fireBrigadeId = fireBrigadeId;
	}

	public String getFireBrigadeName() {
		return fireBrigadeName;
	}

	public void setFireBrigadeName(String fireBrigadeName) {
		this.fireBrigadeName = fireBrigadeName;
	}

	public String getStationLevel() {
		return stationLevel;
	}

	public void setStationLevel(String stationLevel) {
		this.stationLevel = stationLevel;
	}

	public Integer getFireEngineCount() {
		return fireEngineCount;
	}

	public void setFireEngineCount(Integer fireEngineCount) {
		this.fireEngineCount = fireEngineCount;
	}

	public BigDecimal getWaterCount() {
		return waterCount;
	}

	public void setWaterCount(BigDecimal waterCount) {
		this.waterCount = waterCount;
	}

	public BigDecimal getPowderCount() {
		return powderCount;
	}

	public void setPowderCount(BigDecimal powderCount) {
		this.powderCount = powderCount;
	}

	public BigDecimal getFoamCount() {
		return foamCount;
	}

	public void setFoamCount(BigDecimal foamCount) {
		this.foamCount = foamCount;
	}

	public List<FireEngineVo> getFireEngineList() {
		return fireEngineList;
	}

	public void setFireEngineList(List<FireEngineVo> fireEngineList) {
		this.fireEngineList = fireEngineList;
	}

	public Integer getKeyUnitCount() {
		return keyUnitCount;
	}

	public void setKeyUnitCount(Integer keyUnitCount) {
		this.keyUnitCount = keyUnitCount;
	}

	public List<KeyUnit> getKeyUnitList() {
		return keyUnitList;
	}

	public void setKeyUnitList(List<KeyUnit> keyUnitList) {
		this.keyUnitList = keyUnitList;
	}
	
	
}
