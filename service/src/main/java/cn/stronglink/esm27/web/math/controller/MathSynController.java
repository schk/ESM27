package cn.stronglink.esm27.web.math.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.message.mq.topic.SendReceiver;
import cn.stronglink.esm27.thirdsdk.explosion.GasMathModel;
import cn.stronglink.esm27.web.math.vo.GasDispersionParamsEntity;
import cn.stronglink.esm27.web.math.vo.MQMessageOfESM27;
import cn.stronglink.esm27.web.math.vo.MathGranularVo;
import cn.stronglink.esm27.web.math.vo.MathVo;

@Controller
@RequestMapping(value = "webApi/mathSyn")
public class MathSynController extends AbstractController  {

	@Autowired
	private SendReceiver sendReceiver;
	
//	public void test(){
//		int i = Explosion.INSTANCE.Init_API();
//		System.out.println("i::::::::"+i);	
//		int j = GasMathModel.INSTANCE.Init_API();
//		System.out.println("j::::::::"+j);
//	}
	
	@RequestMapping("/gaussionModelParametersSyn")
	public	ResponseEntity<ModelMap>  gaussionModelParametersSyn(ModelMap modelMap, HttpServletRequest request,
				HttpServletResponse response,@RequestBody Map<String, Object> params){
		int as = Integer.parseInt(params.get("as").toString());
//		int isRealTime = Integer.parseInt(params.get("isRealTime").toString()); 
		double ws = Double.valueOf(params.get("ws").toString());
		double wd = Double.valueOf(params.get("wd").toString());
		double gd = Double.valueOf(params.get("gd").toString());
		double h = Double.valueOf(params.get("h").toString());
		double dq = Double.valueOf(params.get("dq").toString());
		double fCurrentTime = Double.valueOf(params.get("fCurrentTime").toString());
//		double t = Double.valueOf(params.get("t").toString());
			MQMessageOfESM27 mQMessageOfESM27= new MQMessageOfESM27();
			GasDispersionParamsEntity gasObj = new GasDispersionParamsEntity();
			gasObj.setAs(as);
			gasObj.setDq(dq);
			gasObj.setGd(gd);
			gasObj.setH(h);
			gasObj.setfCurrentTime(fCurrentTime);
			gasObj.setPushSpanSecond(30);
			gasObj.setWd(wd);
			gasObj.setWs(ws);
			gasObj.setConcentration(new double[]{0.00001,0.0001});
			//{"awsPostdata":{concentration":[0.0,0.5]},"timestamp":1533458410397}
			mQMessageOfESM27.setAwsPostdata(mQMessageOfESM27);
			mQMessageOfESM27.setAwsPostdata(gasObj);
			mQMessageOfESM27.setTimestamp(1533458410397L);
			mQMessageOfESM27.setActioncode("StartParticleLandingAlgorithmService");
			String param =  JSONObject.toJSONString(mQMessageOfESM27);
			sendReceiver.sendAlgorithm(param);
			return setSuccessModelMap(modelMap);
	}
	
	
//	二、气体扩散模型（比空气轻的气体）
//	1、计算气体扩散过程
//	（1）初始化Init_API 
//	（2）、设置参数SetGaussionModelParameters_API
//	（3）、设置积分时间间隔SetDeltaT_API（也可以不设置此参数，程序内默认是 1 秒，间隔越短计算次数越多耗时越长，间隔越长计算次数越少耗时越少）
//	（4）模拟计算Simulate_API
//	（5）设置浓度等值线SetThreshold_API
//	（6）获取轮廓线顶点坐标数组的长度GetVerticesLength_API
//	（7）获取轮廓线顶点坐标数组GetVertices_API
//	（8）重复5~6步，可以获取多个轮廓线
	@RequestMapping("/gaussionModelParameters")
	public	ResponseEntity<ModelMap>  gaussionModelParameters(ModelMap modelMap, HttpServletRequest request,
				HttpServletResponse response,@RequestBody Map<String, Object> params){
		int as = Integer.parseInt(params.get("as").toString());
		double ws = Double.valueOf(params.get("ws").toString());
		double wd = Double.valueOf(params.get("wd").toString());
		double gd = Double.valueOf(params.get("gd").toString());
		double h = Double.valueOf(params.get("h").toString());
		double dq = Double.valueOf(params.get("dq").toString());
		double fCurrentTime = Double.valueOf(params.get("fCurrentTime").toString());
		String tLine = params.get("tLine").toString();
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			GasMathModel.INSTANCE.SetGaussionModelParameters_API(userId, as, ws, wd, gd, h, dq);
//			GasMathModel.INSTANCE.SetDeltaT_API(dt);
			GasMathModel.INSTANCE.Simulate_API(userId, fCurrentTime);
			String[] tLineValues = tLine.split(",");
			MathVo  mathVo  = new MathVo();
			List<String> mathGranularVoLsit =  new ArrayList<String>();
			for(int num=0;num<tLineValues.length;){
				GasMathModel.INSTANCE.SetThreshold_API(userId, Double.valueOf(tLineValues[num]));
				int szcd = GasMathModel.INSTANCE.GetVerticesLength_API(userId);
				double[] pointData = new double[szcd];
				GasMathModel.INSTANCE.GetVertices_API(userId, pointData);
				String pointStr = "[";
				String pointStrTemp = "";
				for(int j = 0; j < szcd; j=j+12){
					if(j==0){
						pointStrTemp += "[";
						pointStrTemp += pointData[j] + 11518384.023410;
						pointStrTemp += ",";
						pointStrTemp += pointData[j+1] + 3571012.185491;
						pointStrTemp += "]";
					}
					if(j<szcd-12){
						pointStr += "[";
						pointStr += pointData[j] + 11518384.023410;
						pointStr += ",";
						pointStr += pointData[j+1] + 3571012.185491;
						pointStr += "],";
					}
				}
				pointStr += pointStrTemp;
				pointStr += "]";
				mathGranularVoLsit.add(pointStr);
			}
			mathVo.setMathGranularVoLsit(mathGranularVoLsit);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "初始化信息错误");
		}
	}
	
	// 计算泄漏源的位置和泄漏率
	// ws: 风速，单位m/s
	// wd: 风向，单位度
	// as: 大气稳定度，取值 1,2,3,4,5,6 与大气稳定度取值表的对应关系 1=A, 2=B, 3=C, 4=D, 5=E, 6=F
	// data[]: 四组传感器数据，传值(q1,x1,y1,h1,......q2,x2,y2,h2) q浓度，xy传感器坐标，h传感器距离地面高度
	// 返回值: true计算成功，false失败
	@RequestMapping("/leakSource")
	public ResponseEntity<ModelMap>  leakSource(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double ws = Double.valueOf(params.get("ws").toString());
		double wd = Double.valueOf(params.get("wd").toString());
		int as = Integer.parseInt(params.get("as").toString());
//		double[] data = (double[]) params.get("data");
		String data = params.get("data").toString();
		//double data[] = params.get("data");
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			GasMathModel.INSTANCE.LeakSource_API(userId, ws, wd, as, data);
			double lsQ = GasMathModel.INSTANCE.GetLsQ_API(userId);
			double sX = GasMathModel.INSTANCE.GetLsX_API(userId);
			double sY = GasMathModel.INSTANCE.GetLsY_API(userId);
			double sH = GasMathModel.INSTANCE.GetLsH_API(userId);
			MathVo  mathVo  = new MathVo();
			mathVo.setsX(sX);
			mathVo.setsY(sY);
			mathVo.setsH(sH);
			mathVo.setLsQ(lsQ);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "初始化信息错误");
		}
	}
	
	// 颗粒物沉降
		// ws: 风速，单位m/s
		// wd: 风向，单位度
		// as: 大气稳定度，取值 1,2,3,4,5,6 与大气稳定度取值表的对应关系 1=A, 2=B, 3=C, 4=D, 5=E, 6=F
		// data[]: 四组传感器数据，传值(q1,x1,y1,h1,......q2,x2,y2,h2) q浓度，xy传感器坐标，h传感器距离地面高度
		// 返回值: true计算成功，false失败
	@RequestMapping("/granularPrecipitate")
	public ResponseEntity<ModelMap> granularPrecipitate(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		int as = Integer.parseInt(params.get("as").toString());
		double ws = Double.valueOf(params.get("ws").toString());
		double wd = Double.valueOf(params.get("wd").toString());
		double h = Double.valueOf(params.get("h").toString());
		double dq = Double.valueOf(params.get("dq").toString());
		double fCurrentTime = Double.valueOf(params.get("fCurrentTime").toString());
		String tl = params.get("t").toString();
		String[] aa = tl.split(",");
		List<String> getMathGranularVoLsit = new ArrayList<String>();
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			boolean f = GasMathModel.INSTANCE.SetPsParameters_API(userId, as, ws, wd, h, dq);
			System.out.print(f);
//			GasMathModel.INSTANCE.SetPsDeltaT_API();
			GasMathModel.INSTANCE.SimulatePs_API(userId, fCurrentTime);
			MathVo  mathVo  = new MathVo();
			for(int i=0;i<aa.length;i++){
				double t = Double.valueOf(aa[i]);
				GasMathModel.INSTANCE.SetPsThreshold_API(userId, t);
				int szcd = GasMathModel.INSTANCE.GetPsVerticesLength_API(userId);
				double[] vertices = new double[szcd];
				GasMathModel.INSTANCE.GetPsVertices_API(userId, vertices);
				MathGranularVo mathGranularVo = new MathGranularVo();
				mathGranularVo.setDzx(t);
				mathGranularVo.setVertices(vertices);
				String qq = "[";
				String aab = "";
				for(int j = 0; j < szcd; j=j+12){
					if(j==0){
						aab += "[";
						aab += vertices[j] + 11518384.023410;
						aab += ",";
						aab += vertices[j+1] + 3571012.185491;
						aab += "]";
					}
					if(j<szcd-12){
						qq += "[";
						qq += vertices[j] + 11518384.023410;
						qq += ",";
						qq += vertices[j+1] + 3571012.185491;
						qq += "],";
					}
				}
				qq += aab;
				qq += "]";
				getMathGranularVoLsit.add(qq);
			}
			mathVo.setMathGranularVoLsit(getMathGranularVoLsit);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "初始化信息错误");
		}
	}
	
	
//	四、爆炸模型
//	1、计算爆炸伤害
//	（1）初始化Init_API 
//	（2）CalcExplosionHurt_API
	@RequestMapping("/explosionHurt")
	public ResponseEntity<ModelMap> explosionHurt(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		int type  = Integer.parseInt(params.get("type1").toString());
		int name = Integer.parseInt(params.get("name1").toString());
		double pressure = Double.valueOf(params.get("pressure").toString());
		double volume = Double.valueOf(params.get("volume").toString());
		double r = Double.valueOf(params.get("r").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			GasMathModel.INSTANCE.Init_API();
			double heatFluxDensity = GasMathModel.INSTANCE.CalcExplosionHurt_API(userId, type, name, pressure, volume, r);
//			double radius =GasMathModel.INSTANCE.CalcExplosionHurtR_API(type, name, pressure, volume, hurt);
			MathVo  mathVo  = new MathVo();
			NumberFormat nf = NumberFormat.getNumberInstance();  
            nf.setMaximumFractionDigits(2);  
			mathVo.setHeatFluxDensity(Double.valueOf(nf.format(heatFluxDensity)));
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "初始化信息错误");
		}
	}
	
//	2、计算伤害半径,三个伤害
//	（1）初始化Init_API 
//	（2）CalcExplosionHurtR_API
// 0.02轻微损伤，0.03听觉器官损伤，0.05内脏严重损伤，0.1死亡
	@RequestMapping("/explosionHurtR")
	public ResponseEntity<ModelMap>  explosionHurtR(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		int type  = Integer.parseInt(params.get("type1").toString());
		int name = Integer.parseInt(params.get("name1").toString());
		double pressure = Double.valueOf(params.get("pressure").toString());
		double volume = Double.valueOf(params.get("volume").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			MathVo  mathVo  = new MathVo();
			double[] hurtD = {0.02,0.03,0.05,0.1};
			double radiusList[] = new double[4];
			for(int i=0;i<hurtD.length;i++){
				double heatFluxDensity = GasMathModel.INSTANCE.CalcExplosionHurtR_API(userId, type, name, pressure, volume, hurtD[i]);
				NumberFormat nf = NumberFormat.getNumberInstance();  
	            nf.setMaximumFractionDigits(2);  
				radiusList[i]= Double.valueOf(nf.format(heatFluxDensity));
			}
			mathVo.setPointData(radiusList);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "初始化信息错误");
		}
	}
	
	//	五、热辐射模型
	//	1、计算热辐射伤害对人
	//	（1）初始化Init_API 
	//	（2）设置参数SetHeatHurtParameters_API
	//	（3）计算热辐射伤害CalcHeatHurt_API
	//	（4）获取伤害等级概率，概率大于5可以认为会受到此等级伤害GetHeatHurtPr1_API、GetHeatHurtPr2_API、GetHeatHurtPr3_API
	// 传参数（计算热辐射伤害），后 4 个参数查数据库
	// S: 火灾面积，单位平方米(m2)
	// T0: 环境温度，单位开氏度(K)
	// Hc: 物质的燃烧热，单位焦耳每公斤(J/Kg)
	// Hv: 物质的蒸发热，单位焦耳每公斤(J/Kg)
	// cp: 物质的定压比热容，单位焦耳(每公斤每华氏度)(J/(Kg*K))
	// Tb: 物质的沸点，单位开氏度(K)
	// 返回值: true成功，false失败
	// 返回值: 目标地点接收到的热通量密度，单位千瓦每平方米（kW/m2）
	// 返回值: 死亡概率，概率单位为5时对应伤亡百分数50%
	// 返回值: 二级烧伤概率，概率单位为5时对应伤亡百分数50%
	// 返回值: 一级烧伤概率，概率单位为5时对应伤亡百分数50%
	// 返回值: 目标地点接收到的热通量密度，单位千瓦每平方米（kW/m2）
	@RequestMapping("/heatHurtParameters")
	public ResponseEntity<ModelMap> heatHurtParameters(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double S  = Double.valueOf(params.get("S").toString());
		double T0 = Double.valueOf(params.get("T0").toString());
		double Hc = Double.valueOf(params.get("Hc").toString());
		double Hv = Double.valueOf(params.get("Hv").toString());
		double cp = Double.valueOf(params.get("cp").toString());
        double Tb = Double.valueOf(params.get("Tb").toString());
        double length = Double.valueOf(params.get("length").toString());
        double time = Double.valueOf(params.get("time").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			MathVo  mathVo  = new MathVo();
			boolean isHeatHurt = GasMathModel.INSTANCE.SetHeatHurtParameters_API(userId, S, T0, Hc, Hv, cp, Tb);
			mathVo.setHeatHurt(isHeatHurt);
			NumberFormat nf = NumberFormat.getNumberInstance();  
            nf.setMaximumFractionDigits(2); 
			if(isHeatHurt){
				GasMathModel.INSTANCE.CalcHeatHurt_API(userId, length, time);
				double dieRadius = GasMathModel.INSTANCE.GetHeatHurtPr1_API(userId);
				double secondLevelRadius = GasMathModel.INSTANCE.GetHeatHurtPr2_API(userId);
				double oneLevelRadius = GasMathModel.INSTANCE.GetHeatHurtPr3_API(userId);
				mathVo.setDieRadius(Double.valueOf(nf.format(dieRadius)));
				mathVo.setSecondLevelRadius(Double.valueOf(nf.format(secondLevelRadius)));
				mathVo.setOneLevelRadius(Double.valueOf(nf.format(oneLevelRadius)));
			}
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "初始化信息错误");
		}
	}
	
//	2、计算热辐射伤害半径
//	（1）初始化Init_API 
//	（2）设置参数SetHeatHurtParameters_API
//	（3）计算热辐射伤害半径CalcHeatHurtR_API
//	（4）获取伤害半径，GetHeatHurtR1_API、GetHeatHurtR2_API、GetHeatHurtR3_API
	// 返回值: 死亡半径，单位米(m)
	// 返回值: 二级烧伤半径，单位米(m)
	// 返回值: 一级烧伤半径，单位米(m)
	@RequestMapping("/heatHurtCalParameters")
	public ResponseEntity<ModelMap> heatHurtCalParameters(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double S  = Double.valueOf(params.get("S").toString());
		double T0 = Double.valueOf(params.get("T0").toString());
		double Hc = Double.valueOf(params.get("Hc").toString());
		double Hv = Double.valueOf(params.get("Hv").toString());
		double cp = Double.valueOf(params.get("cp").toString());
        double Tb = Double.valueOf(params.get("Tb").toString());
        double time = Double.valueOf(params.get("time").toString());
        if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			GasMathModel.INSTANCE.SetHeatHurtParameters_API(userId, S, T0, Hc, Hv, cp, Tb);
			GasMathModel.INSTANCE.CalcHeatHurtR_API(userId, time);
			double dieRadius = GasMathModel.INSTANCE.GetHeatHurtR1_API(userId);
			double secondLevelRadius = GasMathModel.INSTANCE.GetHeatHurtR2_API(userId);
			double oneLevelRadius = GasMathModel.INSTANCE.GetHeatHurtR3_API(userId);
			MathVo  mathVo  = new MathVo();
			mathVo.setDieRadius(dieRadius);
			mathVo.setSecondLevelRadius(secondLevelRadius);
			mathVo.setOneLevelRadius(oneLevelRadius);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "初始化信息错误");
		}
	}

//	六、管道泄漏模型
//	1、计算液体泄漏率
//	（1）初始化Init_API 
//	（2）计算液体泄漏率CalcLiquidLeak_API
	// 返回值: 泄漏速率（kg/s）
	@RequestMapping("/calcLiquidLeak") 
	public ResponseEntity<ModelMap> calcLiquidLeak(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double rou  = Double.valueOf(params.get("rou").toString());
		double s = Double.valueOf(params.get("s").toString());
		double p = Double.valueOf(params.get("p").toString());
		double h = Double.valueOf(params.get("h").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){ 
			String userId =WebUtil.getCurrentUser().toString();
			double rate = GasMathModel.INSTANCE.CalcLiquidLeak_API(userId, rou, s, p, h);
			MathVo  mathVo  = new MathVo();
			mathVo.setRate(rate);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "初始化信息错误");
		}
	}
	
//	2、计算瞬时泄露的液池半径
//	（1）初始化Init_API 
//	（2）计算液体泄漏（瞬时泄露）后形成的液池半径CalcLiquidLeakArea1_API
	// 返回值: 液池半径（m）
	@RequestMapping("/calcLiquidLeakArea1")
	public ResponseEntity<ModelMap> calcLiquidLeakArea1(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double rou  = Double.valueOf(params.get("rou").toString());
		double m = Double.valueOf(params.get("m").toString());
		double t = Double.valueOf(params.get("t").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			double radius = GasMathModel.INSTANCE.CalcLiquidLeakArea1_API(userId, m, t, rou);
			MathVo  mathVo  = new MathVo();
			mathVo.setRate(radius);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "初始化信息错误");
		}
	}
	
//	3、计算持续泄露的液池半径
//	（1）初始化Init_API 
//	（2）计算液体泄漏（持续泄露）后形成的液池半径CalcLiquidLeakArea2_API
	// 返回值: 液池半径（m）
	@RequestMapping("/calcLiquidLeakArea2")
	public ResponseEntity<ModelMap> calcLiquidLeakArea2(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double rou  = Double.valueOf(params.get("rou").toString());
		double m = Double.valueOf(params.get("m").toString());
		double t = Double.valueOf(params.get("t").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			double radius = GasMathModel.INSTANCE.CalcLiquidLeakArea2_API(userId, m, t, rou);
			MathVo  mathVo  = new MathVo();
			mathVo.setRate(radius);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "初始化信息错误");
		}
	}
//	七、液体蒸发模型
//	1、计算闪蒸速率
//	（1）初始化Init_API 
//	（2）计算闪蒸速率CalcEvaporate1_API
	// 返回值: 闪蒸速率（kg/s）
	@RequestMapping("/calcEvaporate1")
	public ResponseEntity<ModelMap> calcEvaporate1(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double Cp  = Double.valueOf(params.get("Cp").toString());
		double Hv = Double.valueOf(params.get("Hv").toString());
		double Tb = Double.valueOf(params.get("Tb").toString());
		double m = Double.valueOf(params.get("m").toString());
		double t = Double.valueOf(params.get("t").toString());
		double A1 = Double.valueOf(params.get("A1").toString());
		double T0 = Double.valueOf(params.get("T0").toString());
		double L = Double.valueOf(params.get("L").toString());
		double K = Double.valueOf(params.get("K").toString());
		double a = Double.valueOf(params.get("a").toString());
		double Nu = Double.valueOf(params.get("Nu").toString());
		double alpha = Double.valueOf(params.get("alpha").toString());
		double Sh = Double.valueOf(params.get("Sh").toString());
		double A = Double.valueOf(params.get("A").toString());
		double rou = Double.valueOf(params.get("rou").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			double rate1 = GasMathModel.INSTANCE.CalcEvaporate1_API(userId, Cp, Hv, T0, Tb, m, t);
			double rate2 = GasMathModel.INSTANCE.CalcEvaporate2_API(userId, A1, T0, Tb, Hv, L, K, a, t, Nu);
			double rate3 = GasMathModel.INSTANCE.CalcEvaporate3_API(userId, alpha, Sh, A, L, rou);
			MathVo  mathVo  = new MathVo();
			double rate = rate1+rate2+rate3;
			mathVo.setRate(rate);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "初始化信息错误");
		}
	}
	
//	2、计算热量蒸发速率
//	（1）初始化Init_API 
//	（2）计算热量蒸发速率CalcEvaporate2_API
//	返回值: 蒸发速率（kg/s）
	@RequestMapping("/calcEvaporate2")
	public ResponseEntity<ModelMap> calcEvaporate2(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double A1  = Double.valueOf(params.get("A1").toString());
		double T0 = Double.valueOf(params.get("T0").toString());
		double Tb = Double.valueOf(params.get("Tb").toString());
		double Hv  = Double.valueOf(params.get("Hv").toString());
		double L = Double.valueOf(params.get("L").toString());
		double K = Double.valueOf(params.get("K").toString());
		double a  = Double.valueOf(params.get("a").toString());
		double T = Double.valueOf(params.get("T").toString());
		double Nu = Double.valueOf(params.get("Nu").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			double rate = GasMathModel.INSTANCE.CalcEvaporate2_API(userId, A1, T0, Tb, Hv, L, K, a, T, Nu);
			MathVo  mathVo  = new MathVo();
			mathVo.setRate(rate);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "初始化信息错误");
		}
		
	}
	
//	3、计算质量蒸发速率
//	（1）初始化Init_API 
//	（2）计算质量蒸发速率CalcEvaporate3_API
	@RequestMapping("/calcEvaporate3")
	public ResponseEntity<ModelMap> calcEvaporate3(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double alpha  = Double.valueOf(params.get("alpha").toString());
		double Sh = Double.valueOf(params.get("Sh").toString());
		double A = Double.valueOf(params.get("A").toString());
		double L = Double.valueOf(params.get("L").toString());
		double rou = Double.valueOf(params.get("rou").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			double rate = GasMathModel.INSTANCE.CalcEvaporate3_API(userId, alpha, Sh, A, L, rou);
			MathVo  mathVo  = new MathVo();
			mathVo.setRate(rate);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "初始化信息错误");
		}
	}
	
//	八、燃烧物分析
//	（1）初始化Init_API 
//	（2）、设置参数CalcEmissionRate_API
//	（3）、获取污染物产生率GetCO_API、GetCO2_API、GetSO2_API、GetNO_API、GetNO2_API
	@RequestMapping("/calcEmissionRate")
	public ResponseEntity<ModelMap> calcEmissionRate(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params){
		double speed  = Double.valueOf(params.get("speed").toString());
		double mt =  Double.valueOf(params.get("mt").toString());
		double Cc = Double.valueOf(params.get("Cc").toString());
		double Cs = Double.valueOf(params.get("Cs").toString());
		double Cn = Double.valueOf(params.get("Cn").toString());
		if(GasMathModel.INSTANCE.Init_API()==1){
			String userId =WebUtil.getCurrentUser().toString();
			GasMathModel.INSTANCE.CalcEmissionRate_API(userId, speed, mt, Cc, Cs, Cn);
			double coRate = GasMathModel.INSTANCE.GetCO_API(userId);
			double co2Rate = GasMathModel.INSTANCE.GetCO2_API(userId);
			double so2Rate = GasMathModel.INSTANCE.GetSO2_API(userId);
			double noRate = GasMathModel.INSTANCE.GetNO_API(userId);
			double no2Rate = GasMathModel.INSTANCE.GetNO2_API(userId);
			MathVo  mathVo  = new MathVo();
			mathVo.setCoRate(coRate);
			mathVo.setCo2Rate(co2Rate);
			mathVo.setNo2Rate(no2Rate);
			mathVo.setNoRate(noRate);
			mathVo.setSo2Rate(so2Rate);
			return setSuccessModelMap(modelMap, mathVo);
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "初始化信息错误");
		}
	}
}
