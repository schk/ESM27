package cn.stronglink.esm27.web.math.vo;

public class MQMessageOfESM27 {
	
	private String actioncode;
	private Object awsPostdata;
	private long timestamp=System.currentTimeMillis();
	private String devType;
	
	public String getActioncode() {
		return actioncode;
	}
	public void setActioncode(String actioncode) {
		this.actioncode = actioncode;
	}
	public Object getAwsPostdata() {
		return awsPostdata;
	}
	public void setAwsPostdata(Object awsPostdata) {
		this.awsPostdata = awsPostdata;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public String getDevType() {
		return devType;
	}
	public void setDevType(String devType) {
		this.devType = devType;
	}

}
