package cn.stronglink.esm27.web.webData.vo;

public class MapPropertiesVo{

	private String catecd;
	private String point;
	private String id;
	private String name;
	private String dtPath;
	private String jhPath;
	public String getCatecd() {
		return catecd;
	}
	public void setCatecd(String catecd) {
		this.catecd = catecd;
	}
	public String getPoint() {
		return point;
	}
	public void setPoint(String point) {
		this.point = point;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDtPath() {
		return dtPath;
	}
	public void setDtPath(String dtPath) {
		this.dtPath = dtPath;
	}
	public String getJhPath() {
		return jhPath;
	}
	public void setJhPath(String jhPath) {
		this.jhPath = jhPath;
	}
	
}
