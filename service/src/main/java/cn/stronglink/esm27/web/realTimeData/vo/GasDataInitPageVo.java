package cn.stronglink.esm27.web.realTimeData.vo;

import java.util.List;

public class GasDataInitPageVo {
	
	private String deviceSerial;//设备号
	
	private List<GasDetectorDataVo> gasDetectorDataVo;//气体数据设备
	
	public String getDeviceSerial() {
		return deviceSerial;
	}
	public void setDeviceSerial(String deviceSerial) {
		this.deviceSerial = deviceSerial;
	}
	public List<GasDetectorDataVo> getGasDetectorDataVo() {
		return gasDetectorDataVo;
	}
	public void setGasDetectorDataVo(List<GasDetectorDataVo> gasDetectorDataVo) {
		this.gasDetectorDataVo = gasDetectorDataVo;
	}
	
	

}
