package cn.stronglink.esm27.web.sysSession.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.SysSession;
import cn.stronglink.esm27.web.sysSession.vo.SysSessionVo;


public interface SysSessionMapper extends BaseMapper<SysSession>  {

	void updateSysSession(SysSession sys);

	void deleteBySessionId(String sessionId);

	Integer selectByRoomId(Long roomId);

	SysSession selectSessionByUserId(@Param("userId") Long userId, @Param("sessionId")String sessionId);

	SysSession selectExistSession(Long userId);

	void delSessionByUserId(Long userId);

	 List<SysSessionVo> getRoomUser(Map<String,Object> querysys);
}
