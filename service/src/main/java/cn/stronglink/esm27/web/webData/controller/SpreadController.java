package cn.stronglink.esm27.web.webData.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.stronglink.esm27.web.webData.vo.Info;

@Controller
@RequestMapping("/spread")
public class SpreadController {

	@Value("spread.file.path")
	private String filePath;

	@Autowired
	private Environment env;

	private DecimalFormat df = new DecimalFormat("#.00");

	@GetMapping("/analysis")
	@ResponseBody
	public String analysis(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
//	private String analysis(@RequestParam(value="lon") double lon,@RequestParam(value="lat") double lat,
//			@RequestParam(value="path") String path,@RequestParam(value="index") int findex){
		Double lon = Double.parseDouble(request.getParameter("lon"));
		Double lat = Double.parseDouble(request.getParameter("lat"));
		String path = request.getParameter("path");
		int findex = Integer.parseInt(request.getParameter("index"));
		try {
			String[] colors = createRgba();
			File file = new File(path);
			File[] files = file.listFiles();
			FileInputStream in = new FileInputStream(files[findex]);
			InputStreamReader read = new InputStreamReader(in);
			BufferedReader buffer = new BufferedReader(read);
			String line = "";
			int index = 0;
			Info info = new Info();
			double[] minPoint = null;
			double[] step = null;
			JSONArray features = new JSONArray();
			while ((line = buffer.readLine()) != null) {
				index++;
				if (index == 1) {
					continue;
				} else if (index == 2) {
					String wNum = line.substring(0, 12).trim();
					String hNum = line.substring(12).trim();
					info.setwNum(Integer.parseInt(wNum));
					info.sethNum(Integer.parseInt(hNum));
				} else if (index == 3) {
					String west = line.substring(0, 12).trim();
					String east = line.substring(12).trim();
					info.setWest(Float.parseFloat(west));
					info.setEast(Float.parseFloat(east));
				} else if (index == 4) {
					String south = line.substring(0, 12).trim();
					String north = line.substring(12).trim();
					info.setSouth(Float.parseFloat(south));
					info.setNorth(Float.parseFloat(north));
					minPoint = getMinPoint(lon, lat, info);
					step = getSteps(info);
				} else if (index > 5 && index < 259) {
					String[] values = line.split("  ");
					// int rowNum = Math.abs(index-(info.gethNum()+5));//行号
					int rowNum = index - 5;// 行号
					int colNum = 0; // 列号
					for (String value : values) {
						float val = Float.valueOf(value);
						if (val > 0) {
							double point_x = minPoint[0] + (colNum * step[0]);
							double point_y = minPoint[1] + (rowNum * step[1]) - (step[1] * 2);
							JSONObject feature = createFeature(point_x, point_y, step, val, colors);
							features.add(feature);
						}
						colNum++;
					}
				} else if (index == 259) {
					break;
				}
			}

			JSONObject result = new JSONObject(true);
			result.put("type", "FeatureCollection");
			result.put("features", features);
			buffer.close();
			read.close();
			in.close();
			return result.toJSONString();
		} catch (Exception e) {
			return null;
		}
	}

	private JSONObject createFeature(double point_x, double point_y, float value, String[] colors) throws Exception {

		JSONObject feature = new JSONObject(true);

		double[] point = { point_x, point_y };

		JSONObject geometry = new JSONObject(true);
		geometry.put("type", "Point");
		geometry.put("coordinates", point);

		JSONObject properties = new JSONObject();
		String color = colors[switchColorIndex(value)];
		properties.put("value", color);

		feature.put("type", "Feature");
		feature.put("properties", properties);
		feature.put("geometry", geometry);

		return feature;
	}

	private JSONObject createFeature(double point_x, double point_y, double[] step, float value, String[] colors)
			throws Exception {

		JSONObject feature = new JSONObject(true);

		double minx = Double.valueOf(df.format(point_x - (step[0] / 2.0)));
		double miny = Double.valueOf(df.format(point_y - (step[1] / 2.0)));
		double maxx = Double.valueOf(df.format(point_x + (step[0] / 2.0)));
		double maxy = Double.valueOf(df.format(point_y + (step[1] / 2.0)));

		double[][][] coords = new double[1][5][2];
		double[] left_down = { minx, miny };
		coords[0][0] = left_down;

		double[] right_down = { maxx, miny };
		coords[0][1] = right_down;

		double[] right_up = { maxx, maxy };
		coords[0][2] = right_up;

		double[] left_up = { minx, maxy };
		coords[0][3] = left_up;

		double[] close = { minx, miny };
		coords[0][4] = close;

		JSONObject geometry = new JSONObject(true);
		geometry.put("type", "Polygon");
		geometry.put("coordinates", coords);

		JSONObject properties = new JSONObject();
		String color = colors[switchColorIndex(value)];
		properties.put("value", color);

		feature.put("type", "Feature");
		feature.put("properties", properties);
		feature.put("geometry", geometry);

		return feature;
	}

	private String[] createRgba() {
		String[] colors = new String[6];
		colors[0] = "rgba(255,255,255,0)";
		colors[1] = "rgba(0,252,0,0.3)";
		colors[2] = "rgba(161,252,0,0.4)";
		colors[3] = "rgba(252,252,0,0.4)";
		colors[4] = "rgba(252,199,0,0.4)";
		colors[5] = "rgba(173,102,0,0.6)";
		return colors;
	}

	private String[] createRgba(boolean isValue) {
		String[] colors = new String[6];
		colors[0] = "0.0";
		colors[1] = "0.2";
		colors[2] = "0.4";
		colors[3] = "0.6";
		colors[4] = "0.8";
		colors[5] = "1.0";
		return colors;
	}

	// 100PPM是151785.7142857143 1517
	// 1000PPM 1517857.142857143 15170

	private int switchColorIndex2(float value) {
		System.out.println("======================"+value);
		//int PPMToMG = Integer.valueOf(env.getProperty("global.PPMToMG"));
		float PPMToMG =Float.parseFloat(env.getProperty("global.PPMToMG"));
		if (value > 0 && value <= 0.006) {
			return 0;
		} else if (value > 0.006 && value <= 0.029) {
			return 1;
		} else if (value > 0.029 && value <= 0.043) {
			return 2;
		} else if (value > 0.043 && value <= 0.071) {
			return 3;
		} else if (value > PPMToMG && value <= PPMToMG * 10) {
			return 4;
		} else if (value > PPMToMG * 10 && value <= PPMToMG * 100000) {
			return 5;
		} else {
			return 0;
		}
//			}else if(value>1517&&value<=1024000){
//				return 5;
//			}else{
//				return 0;
//			}
	}

	private int switchColorIndex(float value) {
		if (value > 0 && value <= 0.006) {
			return 0;
		} else if (value > 0.006 && value <= 0.029) {
			return 1;
		} else if (value > 0.029 && value <= 0.043) {
			return 2;
		} else if (value > 0.043 && value <= 0.071) {
			return 3;
		} else if (value > 0.071 && value <= 0.107) {
			return 4;
		} else if (value > 0.107 && value <= 1024) {
			return 5;
		} else {
			return 0;
		}
	}

	// 二氧化硫
	private int switchColorIndexSo2(float value) {
		if (value > 0 && value <= 0.0001) {
			return 0;
		} else if (value > 0.0001 && value <= 0.029) {
			return 1;
		} else if (value > 0.029 && value <= 0.043) {
			return 2;
		} else if (value > 0.043 && value <= 0.071) {
			return 3;
		} else if (value > 0.071 && value <= 0.107) {
			return 4;
		} else if (value > 0.107 && value <= 1024) {
			return 5;
		} else {
			return 0;
		}
	}

	private double[] getMinPoint(double lon, double lat, Info info) {
		float dis_x = info.getEast() - info.getWest();
		float dis_y = info.getNorth() - info.getSouth();
		double dis_x_2 = dis_x / 2.0 * 1000;
		double dis_y_2 = dis_y / 2.0 * 1000;
		double minx = lon - dis_x_2;
		double miny = lat - dis_y_2;
		double[] minPoint = { minx, miny };
		return minPoint;
	}

	private double[] getSteps(Info info) {
		float dis_x = (info.getEast() - info.getWest()) * 1000;
		float dis_y = (info.getNorth() - info.getSouth()) * 1000;
		double stepX = dis_x / info.getwNum();
		double stepY = dis_y / info.gethNum();
		double[] step = { stepX, stepY };
		return step;
	}
}
