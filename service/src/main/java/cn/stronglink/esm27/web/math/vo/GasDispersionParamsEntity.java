package cn.stronglink.esm27.web.math.vo;
/**
 * 气体扩散模型
 * @author 
 *
 */
public class GasDispersionParamsEntity {
	/**
	 * 大气稳定度，整型，取值 1,2,3,4,5,6 与大气稳定度取值表的对应关系 1=A, 2=B, 3=C, 4=D, 5=E, 6=F
	 */
	private int as;
	/**
	 * 环境风速，一般取地面 10m 高处的平均风速，单位m/s
	 */
	private double ws;
	/**
	 * 风向，单位度
	 */
	private double wd;
	/**
	 * 污染气体在常温下的浓度，单位 kg/m3
	 */
	private double gd;
	/**
	 * 泄漏点离地面的高度
	 */
	private double h;
	/**
	 * 泄漏源强度，kg/s
	 */
	private double dq;
	/**
	 * 开始模拟，计算 fCurrentTime 时间点的范围轮廓线 fCurrentTime: 扩散时间 返回值 true 计算成功，false 计算失败
	 */
	private double fCurrentTime;
	/**
	 * 设置浓度阈值，计算此浓度范围轮廓线，单位kg/m3 t: 等值线浓度
	 */
	private double[] concentration;
	/**
	 * 推送数据间隔的描述
	 */
	private long pushSpanSecond;
	
	public int getAs() {
		return as;
	}
	public void setAs(int as) {
		this.as = as;
	}
	public double getWs() {
		return ws;
	}
	public void setWs(double ws) {
		this.ws = ws;
	}
	public double getWd() {
		return wd;
	}
	public void setWd(double wd) {
		this.wd = wd;
	}
	public double getGd() {
		return gd;
	}
	public void setGd(double gd) {
		this.gd = gd;
	}
	public double getH() {
		return h;
	}
	public void setH(double h) {
		this.h = h;
	}
	public double getDq() {
		return dq;
	}
	public void setDq(double dq) {
		this.dq = dq;
	}
	
	public double getfCurrentTime() {
		return fCurrentTime;
	}
	public void setfCurrentTime(double fCurrentTime) {
		this.fCurrentTime = fCurrentTime;
	}
	public double[] getConcentration() {
		return concentration;
	}
	public void setConcentration(double[] concentration) {
		this.concentration = concentration;
	}
	public long getPushSpanSecond() {
		return pushSpanSecond;
	}
	public void setPushSpanSecond(long pushSpanSecond) {
		this.pushSpanSecond = pushSpanSecond;
	}
	
}
