package cn.stronglink.esm27.web.realTimeData.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.plugins.Page;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.util.DateUtil;
import cn.stronglink.core.util.DownloadExcelUtil;
import cn.stronglink.core.util.ExlVo;
import cn.stronglink.core.util.HttpUtil;
import cn.stronglink.esm27.entity.Expr;
import cn.stronglink.esm27.entity.GasDetector;
import cn.stronglink.esm27.entity.GasDetectorData;
import cn.stronglink.esm27.entity.GasHistoryData;
import cn.stronglink.esm27.entity.GasType;
import cn.stronglink.esm27.message.mq.entity.ReturnDataVo;
import cn.stronglink.esm27.web.realTimeData.service.RealTimeDataService;
import cn.stronglink.esm27.web.realTimeData.vo.ChartVo;
import cn.stronglink.esm27.web.realTimeData.vo.DataConfigureVo;
import cn.stronglink.esm27.web.realTimeData.vo.DeviceConfigureVo;
import cn.stronglink.esm27.web.realTimeData.vo.GasDataInitPageVo;
import cn.stronglink.esm27.web.realTimeData.vo.GasDetectorDataVo;
import cn.stronglink.esm27.web.realTimeData.vo.HistoryGasDataVo;
import cn.stronglink.esm27.web.realTimeData.vo.HistoryQryParams;
import cn.stronglink.esm27.web.realTimeData.vo.SerialPortVo;
import cn.stronglink.esm27.web.realTimeData.vo.WeatherDataVo;
import jxl.format.BorderLineStyle;

@Controller
@RequestMapping(value = "webApi/realTimeData")
public class RealTimeDataController extends AbstractController {
	
	@Autowired
	private RealTimeDataService  realTimeDataService;
	@Value("${collect.url}")
	private String collectUrl;
	
	/**
	 * 开启自动推送
	 * */
	@RequestMapping("openAuto")
	public ResponseEntity<ModelMap> openAuto(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap , @RequestBody Map<String,String> params){
		//BaseMessage bm = new BaseMessage(); 
		//bm.setActioncode("OpenSerialPort");
		//bm.setTimeStamp(String.valueOf(new Date().getTime()));
		SerialPortVo spvo = realTimeDataService.qrySerialPortVo();
		//bm.setAwsPostdata(spvo);
		//String param =  JSONObject.toJSONString(bm);
		//sendReceiver.send(param);
		String userId = params.get("userId");
		String roomId = params.get("roomId");
		Map<String,Object> map = new HashMap<String,Object>();
		Map<String,Object> gdMap = new HashMap<String,Object>();
		gdMap.put("is_del", false);
		List<GasDetector> gdList = realTimeDataService.qryGasDetector(gdMap);
		List<Integer> deviceIds = new ArrayList<Integer>();
		if (gdList!=null && gdList.size()>0) {
			for (GasDetector gasDetector : gdList) {
				deviceIds.add(Integer.valueOf(gasDetector.getSerial()));
			}
		}
		map.put("userId", userId);
		map.put("roomId", roomId);
		map.put("serialPortName", spvo.getSerialPortName());
		map.put("autoPushSensorDataIntervalSeconds",spvo.getAutoPushSensorDataIntervalSeconds());
		map.put("deviceIds", deviceIds);
		//String json = HttpUtil.postHttp("http://127.0.0.1:8000/OpenSerialPort", map);
		return setSuccessModelMap(modelMap,map); 
	}
	
	/**
	 * 关闭自动推送
	 * */
	@RequestMapping("closeAuto")
	public ResponseEntity<ModelMap> closeAuto(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap, @RequestBody Map<String,String> params){
//		BaseMessage bm = new BaseMessage();
//		bm.setActioncode("CloseAutoCollectTask");
//		bm.setTimeStamp(String.valueOf(new Date().getTime()));
//		String param =  JSONObject.toJSONString(bm);
//		sendReceiver.send(param);
		String userId = params.get("userId");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("userId", userId);
		String json = HttpUtil.postHttp(collectUrl+"/CloseSerialPort", map);
		return setSuccessModelMap(modelMap,json);
	}
	
	/**
	 * 发送获取所有串口
	 * */
	@RequestMapping("qrySerialPortList")
	public ResponseEntity<ModelMap> qrySerialPortList(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap){
//		BaseMessage bm = new BaseMessage();
//		bm.setActioncode("GetSystemSerialPortList");
//		bm.setTimeStamp(String.valueOf(new Date().getTime()));
//		String param =  JSONObject.toJSONString(bm);
//		sendReceiver.send(param);
		String json = HttpUtil.postHttp(collectUrl+"/GetSystemSerialPortList",null);
		return setSuccessModelMap(modelMap,json);
	}
	
	/**
	 * 发送获取所有串口
	 * */
	@RequestMapping("qrySerialPort")
	public ResponseEntity<ModelMap> qrySerialPort(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap){
		DataConfigureVo data = realTimeDataService.qrySerialPort();
		return setSuccessModelMap(modelMap,data);
	}
	
	/**
	 * 获取数据配置
	 * */
	@RequestMapping("qryDataConfigure")
	public ResponseEntity<ModelMap> qryDataConfigure(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap){
		DataConfigureVo data = realTimeDataService.qryDataConfigure();
		return setSuccessModelMap(modelMap,data);
	}
	
	/**
	 * 获取气体传感器
	 * */
	@RequestMapping("qryGasDetector")
	public ResponseEntity<ModelMap> qryGasDetector(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody DataConfigureVo vo){
		Map<String,Object> gdMap = new HashMap<String,Object>();
		gdMap.put("is_del", false);
		List<GasDetector> gdList = realTimeDataService.qryGasDetector(gdMap);
		return setSuccessModelMap(modelMap,gdList);
	}
	
	/**
	 * 修改数据配置
	 * */
	@RequestMapping("updateDataConfigure")
	public ResponseEntity<ModelMap> updateDataConfigure(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody DataConfigureVo vo){
		realTimeDataService.updateDataConfigure(vo);
		List<GasDataInitPageVo> deviceList = realTimeDataService.qryGasDataInitPage();
		return setSuccessModelMap(modelMap,deviceList);
	}
	
	/**
	 * 增加设备
	 * */
	@RequestMapping("addConfigure")
	public ResponseEntity<ModelMap> addConfigure(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody GasDetector entity){
		realTimeDataService.addConfigure(entity);
		return setSuccessModelMap(modelMap,null);
	}
	
	/**
	 * 删除设备
	 * */
	@RequestMapping("delConfigure")
	public ResponseEntity<ModelMap> delConfigure(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody Long deviceId){
		realTimeDataService.delConfigure(deviceId);
		return setSuccessModelMap(modelMap,null);
	}
	
	/**
	 * 获取设备配置
	 * */
	@RequestMapping("qryDeviceConfigure")
	public ResponseEntity<ModelMap> qryDeviceConfigure(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap){
		DeviceConfigureVo data = realTimeDataService.qryDeviceConfigure();
		return setSuccessModelMap(modelMap,data);
	}
	
	/**
	 * 修改设备配置
	 * */
	@RequestMapping("updateDeviceConfigure")
	public ResponseEntity<ModelMap> updateDeviceConfigure(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody List<GasDetectorData> vo){
		realTimeDataService.updateDeviceConfigure(vo);
		List<GasDataInitPageVo> deviceList = realTimeDataService.qryGasDataInitPage();
		return setSuccessModelMap(modelMap,deviceList);
	}
	
	/**
	 * 获取设备配置时间间隔
	 * */
	@RequestMapping("qryDeviceTime")
	public ResponseEntity<ModelMap> qryDeviceTime(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap){
		String data = realTimeDataService.qryDeviceTime();
		return setSuccessModelMap(modelMap,data);
	}
	
	/**
	 * 修改设备配置时间间隔
	 * */
	@RequestMapping("updateDeviceTimee")
	public ResponseEntity<ModelMap> updateDeviceTimee(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody DataConfigureVo vo){
		realTimeDataService.updateDeviceTimee(vo);
		return setSuccessModelMap(modelMap);
	}
	
	/**
	 * 解锁
	 * */
	@RequestMapping("openLock")
	public ResponseEntity<ModelMap> openLock(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody String psd){
		Integer data = realTimeDataService.openLock(psd);
		return setSuccessModelMap(modelMap,data);
	}
	
	/**
	 * 查询历史气象数据
	 * */
	@RequestMapping("queryHistoryWeatherData")
	public ResponseEntity<ModelMap> queryHistoryWeatherData(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody HistoryQryParams params){
		
		Map<String, Object> pageParams = new HashMap<>();
		pageParams.put("pageNum", params.getPageNum());
		pageParams.put("pageSize", params.getPageSize());
		@SuppressWarnings("unchecked")
		Page<WeatherDataVo> page = (Page<WeatherDataVo>) super.getPage(pageParams);
		Page<WeatherDataVo> data = realTimeDataService.queryHistoryWeatherData(page,params);
		//List<WeatherDataVo> data = realTimeDataService.queryHistoryWeatherData(params);
		return setSuccessModelMap(modelMap,data);
	}

	/**
	 * 查询历史气象图表
	 * */
	@RequestMapping("queryHistoryWeatherChart")
	public ResponseEntity<ModelMap> queryHistoryWeatherChart(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody HistoryQryParams params){
		ChartVo data = realTimeDataService.queryHistoryWeatherChart(params);
		return setSuccessModelMap(modelMap,data);
	}
	
	/**
	 * 查询历史气体数据
	 * */
	@RequestMapping("queryHistoryGasData")
	public ResponseEntity<ModelMap> queryHistoryGasData(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody HistoryQryParams params){
		Map<String, Object> pageParams = new HashMap<>();
		pageParams.put("pageNum", params.getPageNum());
		pageParams.put("pageSize", params.getPageSize());
		@SuppressWarnings("unchecked")
		Page<HistoryGasDataVo> page = (Page<HistoryGasDataVo>) super.getPage(pageParams);
		Page<HistoryGasDataVo> data = null;
		try {
			data = realTimeDataService.queryHistoryGasData(page,params);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		//List<HistoryGasDataVo> data = realTimeDataService.queryHistoryGasData(params);
		return setSuccessModelMap(modelMap,data);
	}

	/**
	 * 查询历史气体图表
	 * */
	@RequestMapping("queryHistoryGasChart")
	public ResponseEntity<ModelMap> queryHistoryGasChart(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody HistoryQryParams params){
		ChartVo data = null;
		try {
			data = realTimeDataService.queryHistoryGasChart(params);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return setSuccessModelMap(modelMap,data);
	}
	
	@Autowired
    private SimpMessagingTemplate template;
	
	/**
	 * test
	 * */
	@RequestMapping("testSocketData")
	public ResponseEntity<ModelMap> testSocketData(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap){
		ReturnDataVo vo = new ReturnDataVo();
		List<GasDetectorDataVo> dataVoList = realTimeDataService.getTestData();
		List<GasDetectorDataVo> alertDataVoList = new ArrayList<GasDetectorDataVo>();
		for (GasDetectorDataVo gasDetectorDataVo : dataVoList) {
			if (gasDetectorDataVo.getGasIsAlert()==2) {
				alertDataVoList.add(gasDetectorDataVo);
			}
		}
		String lastUpdateTime = DateUtil.getDateTimeHMS();
		vo.setReturnCode("realTimeData");
		vo.setRealData(dataVoList);
		vo.setAlertRealData(alertDataVoList);
		vo.setLastUpdateTime(lastUpdateTime);
		 String result = JSONObject.toJSONString(vo,SerializerFeature.DisableCircularReferenceDetect);
		this.template.convertAndSend("/topic/all", result);//向页面推送实时数据
		int data=(int)(Math.random()*100);
				
		return setSuccessModelMap(modelMap,data);
	}
	
	/**
	 * 气体数据导出
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "downLoadGasDataEXL")
	public String downLoadGasDataEXL(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		//取出带查询条件的所有数据allList
		String p = request.getParameter("param").toString();
		String param = new String(p.getBytes("ISO8859-1"), "UTF-8");
		@SuppressWarnings("unchecked")
		HistoryQryParams params =  JSON.parseObject(param, HistoryQryParams.class);
		List<HistoryGasDataVo> allList = realTimeDataService.queryHistoryGasDataNoPage(params);
		if (allList!=null&&allList.size()>0) {
			List<GasType> historyGasTableTitle = realTimeDataService.qryhistoryGasTableTitle();
			//循环sheet
			DownloadExcelUtil dExlUtile = null;
			int dataNum = 1;
			for (HistoryGasDataVo item : allList) {
					//设置对象：title，value ，List<entity>
					StringBuffer sb = new StringBuffer();
					sb.append("时间_");
					sb.append(item.getTimeString()!=null?item.getTimeString():"-");
					sb.append("$");
					sb.append("设备编号_");
					sb.append(item.getSerial()!=null?item.getSerial():"-");
					sb.append("$");
					if (historyGasTableTitle!=null && historyGasTableTitle.size()>0) {
						for (GasType gasType : historyGasTableTitle) {
							sb.append(gasType.getName()+"_");
							Boolean isExt = false;
							for (GasHistoryData data : item.getGhdList()) {
								if (data.getGasId().toString().equals(gasType.getId_()) ) {
									isExt = true;
									sb.append(data.getValue());
									sb.append("$");
								}
							}
							if (!isExt) {
								sb.append("-");
								sb.append("$");
							}
						}
					}
					String tvsb = sb.toString();
					tvsb = tvsb.substring(0, tvsb.length() - 1);
					String[] tvsbArray = tvsb.split("\\$");
					List<ExlVo> entityList = new ArrayList<ExlVo>();
					if (tvsbArray.length>0) {
						for (String tv : tvsbArray) {
							ExlVo entity = new ExlVo();
							String[] tvArray = tv.split("_");
							entity.setTitle(tvArray[0]);
							entity.setValue(tvArray.length==1?"-":tvArray[1]);
							entityList.add(entity);
						}
					}
					if (dataNum == 1) {//若为第一次循环
						//在response中写入exl。传入当前type的name，和文件名：DownloadExcelUtil
						dExlUtile = new DownloadExcelUtil(response,"历史气体数据","历史气体数据");
						//把list<entity>中的title按list的顺序放入String【】，调用：setExcelListTitle
						String[] titleArray = new String[entityList.size()];
						for (int i = 0; i < entityList.size(); i++) {
							titleArray[i] = entityList.get(i).getTitle();
						}
						dExlUtile.addRow(titleArray, BorderLineStyle.THIN,jxl.format.Alignment.CENTRE, "bold");
						//dExlUtile.setExcelListTitle(titleArray);
						//把list<entity>中的value按list的顺序放入String【】，调用：addRow
						String[] valueArray = new String[entityList.size()];
						for (int i = 0; i < entityList.size(); i++) {
							valueArray[i] = entityList.get(i).getValue();
						}
						dExlUtile.addRow(valueArray, BorderLineStyle.THIN,jxl.format.Alignment.CENTRE, "");
					}else {
						//把list<entity>中的value按list的顺序放入String【】，调用：addRow
						String[] valueArray = new String[entityList.size()];
						for (int i = 0; i < entityList.size(); i++) {
							valueArray[i] = entityList.get(i).getValue();
						}
						dExlUtile.addRow(valueArray, BorderLineStyle.THIN,jxl.format.Alignment.CENTRE, "");
					}									
					dataNum++;
			}
		          dExlUtile.reportExcel();
			
		}else {
			//查询结果为空
		}
		return null;
	}
	
	/**
	 * 天气数据导出
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "downLoadWeatherDataEXL")
	public String downLoadWeatherDataEXL(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		//取出带查询条件的所有数据allList
		String p = request.getParameter("param").toString();
		String param = new String(p.getBytes("ISO8859-1"), "UTF-8");
		@SuppressWarnings("unchecked")
		HistoryQryParams params =  JSON.parseObject(param, HistoryQryParams.class);
		List<WeatherDataVo> allList = realTimeDataService.queryHistoryWeatherDataNoPage(params);
		if (allList!=null&&allList.size()>0) {
			//循环sheet
			DownloadExcelUtil dExlUtile = null;
			int dataNum = 1;
			for (WeatherDataVo item : allList) {
					//设置对象：title，value ，List<entity>
					StringBuffer sb = new StringBuffer();
					sb.append("编号_");
					sb.append(item.getStationPort()!=null?item.getStationPort():"-");
					sb.append("$");
					sb.append("时间_");
					sb.append(item.getTimeString()!=null?item.getTimeString():"-");
					sb.append("$");
					sb.append("风速_");
					sb.append(item.getWindSpeed()!=null?item.getWindSpeed()+"m/s":"-");
					sb.append("$");
					sb.append("风向_");
					sb.append(item.getWindDirection()!=null?item.getWindDirection():"-");
					sb.append("$");
					sb.append("PM2.5_");
					sb.append(item.getPmValue()!=null?item.getPmValue():"-");
					sb.append("$");
					sb.append("温度_");
					sb.append(item.getTemperature()!=null?item.getTemperature()+"℃":"-");
					sb.append("$");
					sb.append("湿度_");
					sb.append(item.getHumidity()!=null?item.getHumidity()+"%":"-");
					sb.append("$");
					sb.append("气压_");
					sb.append(item.getPressure()!=null?item.getPressure()+"Pa":"-");
					sb.append("$");
					sb.append("噪音_");
					sb.append(item.getNoise()!=null?item.getNoise():"-");
					String tvsb = sb.toString();
					String[] tvsbArray = tvsb.split("\\$");
					List<ExlVo> entityList = new ArrayList<ExlVo>();
					if (tvsbArray.length>0) {
						for (String tv : tvsbArray) {
							ExlVo entity = new ExlVo();
							String[] tvArray = tv.split("_");
							entity.setTitle(tvArray[0]);
							entity.setValue(tvArray.length==1?"-":tvArray[1]);
							entityList.add(entity);
						}
					}
					if (dataNum == 1) {//若为第一次循环
						//在response中写入exl。传入当前type的name，和文件名：DownloadExcelUtil
						dExlUtile = new DownloadExcelUtil(response,"历史气象数据","历史气象数据");
						//把list<entity>中的title按list的顺序放入String【】，调用：setExcelListTitle
						String[] titleArray = new String[entityList.size()];
						for (int i = 0; i < entityList.size(); i++) {
							titleArray[i] = entityList.get(i).getTitle();
						}
						dExlUtile.addRow(titleArray, BorderLineStyle.THIN,jxl.format.Alignment.CENTRE, "bold");
						//dExlUtile.setExcelListTitle(titleArray);
						//把list<entity>中的value按list的顺序放入String【】，调用：addRow
						String[] valueArray = new String[entityList.size()];
						for (int i = 0; i < entityList.size(); i++) {
							valueArray[i] = entityList.get(i).getValue();
						}
						dExlUtile.addRow(valueArray, BorderLineStyle.THIN,jxl.format.Alignment.CENTRE, "");
					}else {
						//把list<entity>中的value按list的顺序放入String【】，调用：addRow
						String[] valueArray = new String[entityList.size()];
						for (int i = 0; i < entityList.size(); i++) {
							valueArray[i] = entityList.get(i).getValue();
						}
						dExlUtile.addRow(valueArray, BorderLineStyle.THIN,jxl.format.Alignment.CENTRE, "");
					}									
					dataNum++;
			}
		          dExlUtile.reportExcel();
			
		}else {
			//查询结果为空
		}
		return null;
	}

}
