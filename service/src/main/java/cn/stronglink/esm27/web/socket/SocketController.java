package cn.stronglink.esm27.web.socket;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.JSONObject;

import cn.stronglink.core.util.DateUtil;

@Controller
public class SocketController {

	@Autowired
    private SimpMessagingTemplate template;

	@MessageMapping("/team/{roomId}")
    public void roomMessage(String message, @DestinationVariable String roomId){
//        String dest = "/topic/all";
//        this.template.convertAndSend(dest, "111");
//        
        String dest = "/topic/accident" + roomId;
        this.template.convertAndSend(dest, message);
    } 
	
	@MessageMapping("/team/chat/{roomId}")
    public void roomChatMessage(String message, @DestinationVariable String roomId){
//        String dest = "/topic/all";
//        this.template.convertAndSend(dest, "111");
		Map<String,Object> map = (Map<String, Object>) JSONObject.parse(message);
		map.put("createTime", DateUtil.getDateTime());
        String dest = "/topic/chat/" + roomId;
        this.template.convertAndSend(dest, JSONObject.toJSONString(map));
    }
}
