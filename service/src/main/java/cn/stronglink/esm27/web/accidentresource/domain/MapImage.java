package cn.stronglink.esm27.web.accidentresource.domain;

public class MapImage {
	// 点坐标
	private String extent;
	// id
	private String imageId;
	// url
	private String url;
	
	public String getExtent() {
		return extent;
	}
	public void setExtent(String extent) {
		this.extent = extent;
	}
	public String getImageId() {
		return imageId;
	}
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}
