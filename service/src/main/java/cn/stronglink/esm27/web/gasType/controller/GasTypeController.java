package cn.stronglink.esm27.web.gasType.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.esm27.entity.GasType;
import cn.stronglink.esm27.web.gasType.service.GasTypeService;

@Controller
@RequestMapping("webApi/gasType")
public class GasTypeController extends AbstractController {
	@Autowired
	private GasTypeService gasTypeService;

	@RequestMapping("index")
	public String getUser(Model model, HttpServletRequest request) {
		List<GasType> list = gasTypeService.qryGasTypeList();
		model.addAttribute("list", list);
		return "gasTypeConfig.html";
	}
	
	@RequestMapping("create")
	public ResponseEntity<ModelMap> create(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody GasType vo){
		int a = gasTypeService.crate(vo);
		return setSuccessModelMap(modelMap,a);
	}
	
	@RequestMapping("qryById")
	public ResponseEntity<ModelMap> qryById(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody String id){
		GasType a = gasTypeService.qryById(id);
		return setSuccessModelMap(modelMap,a);
	}
	
	@RequestMapping("edit")
	public ResponseEntity<ModelMap> edit(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody GasType vo){
		int a = gasTypeService.edit(vo);
		return setSuccessModelMap(modelMap,a);
	}

	@RequestMapping("del")
	public ResponseEntity<ModelMap> del(HttpServletRequest request , HttpServletResponse response , ModelMap modelMap,@RequestBody String id){
		int a = gasTypeService.del(id);
		return setSuccessModelMap(modelMap,a);
	}
	
}
