package cn.stronglink.esm27.web.webData.vo;

import java.util.List;

public class MapGeometryVo{

	private String type;
	private List<Object> coordinates;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<Object> getCoordinates() {
		return coordinates;
	}
	public void setCoordinates(List<Object> coordinates) {
		this.coordinates = coordinates;
	}
	

	
	
}
