package cn.stronglink.esm27.web.webData.vo;

import java.util.List;

import cn.stronglink.esm27.entity.KeyUnit;
public class KeyUnitVo {

	/**
	 * 前台重点单位显示信息对象
	 */
	private Long fireBrigadeId;
	
	private String fireBrigadeName;
	
	private Integer keyUnitCount;
	
	private List<KeyUnit>  keyUnitList;

	public Long getFireBrigadeId() {
		return fireBrigadeId;
	}

	public void setFireBrigadeId(Long fireBrigadeId) {
		this.fireBrigadeId = fireBrigadeId;
	}

	public String getFireBrigadeName() {
		return fireBrigadeName;
	}

	public void setFireBrigadeName(String fireBrigadeName) {
		this.fireBrigadeName = fireBrigadeName;
	}

	public Integer getKeyUnitCount() {
		return keyUnitCount;
	}

	public void setKeyUnitCount(Integer keyUnitCount) {
		this.keyUnitCount = keyUnitCount;
	}

	public List<KeyUnit> getKeyUnitList() {
		return keyUnitList;
	}

	public void setKeyUnitList(List<KeyUnit> keyUnitList) {
		this.keyUnitList = keyUnitList;
	}
	
	
}
