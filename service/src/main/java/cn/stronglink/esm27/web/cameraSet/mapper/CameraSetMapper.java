package cn.stronglink.esm27.web.cameraSet.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.CameraSet;

public interface CameraSetMapper extends BaseMapper<CameraSet>  {


	CameraSet qryBySerial(String serial);

	void updateBySerial(CameraSet entity);

	List<CameraSet> getCameraNotNull();

	void deleteAll();

	

}
