package cn.stronglink.esm27.web.realTimeData.vo;

import java.util.List;

public class ChartVo {
	
	private String title;
	private List<String> legendData;
	private XAxis xAxis;
	private List<SeriesData> seriesData;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<String> getLegendData() {
		return legendData;
	}
	public void setLegendData(List<String> legendData) {
		this.legendData = legendData;
	}
	public XAxis getxAxis() {
		return xAxis;
	}
	public void setxAxis(XAxis xAxis) {
		this.xAxis = xAxis;
	}
	public List<SeriesData> getSeriesData() {
		return seriesData;
	}
	public void setSeriesData(List<SeriesData> seriesData) {
		this.seriesData = seriesData;
	}
	
}
