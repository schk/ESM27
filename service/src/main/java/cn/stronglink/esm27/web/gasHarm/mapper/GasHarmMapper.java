package cn.stronglink.esm27.web.gasHarm.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.stronglink.esm27.entity.GasHarmValue;

public interface GasHarmMapper extends BaseMapper<GasHarmValue>  {

	List<GasHarmValue> qryList();

	Map<String,Object> qryItemByGasType(String gasType);

	

}
