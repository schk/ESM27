package cn.stronglink.esm27.web.gasHarm.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.GasHarmValue;
import cn.stronglink.esm27.web.gasHarm.mapper.GasHarmMapper;

@Service
@Transactional(rollbackFor=Exception.class) 
public class GasHarmService {
	
	@Autowired
	private GasHarmMapper gasHarmMapper;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<GasHarmValue> qryList() {
		return gasHarmMapper.qryList();
	}
	/*
	 * 删除对象
	 */
	public void delete(Long id){
		gasHarmMapper.deleteById(id);
	}
	/*
	 * 增加信息
	 */
	public void insert(GasHarmValue entity){
		entity.setId(IdWorker.getId());
		entity.setCreateBy(WebUtil.getCurrentUser());
		entity.setCreateTime(new Date());
		gasHarmMapper.insert(entity);
		
	}
	/*
	 * 修改信息
	 */
	public void update(GasHarmValue entity){
		entity.setUpdateTime(new Date());
		entity.setUpdateBy(WebUtil.getCurrentUser());
		gasHarmMapper.updateById(entity);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public GasHarmValue qryById(Long id) {
		return gasHarmMapper.selectById(id);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Map<String,Object> qryItemByGasType(String gasType) {
		return gasHarmMapper.qryItemByGasType(gasType);
		
	}
	
	
	

}
