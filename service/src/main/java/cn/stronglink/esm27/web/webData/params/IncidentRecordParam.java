package cn.stronglink.esm27.web.webData.params;

import cn.stronglink.esm27.entity.IncidentRecord;
public class IncidentRecordParam extends IncidentRecord{

	/**
	 * 应急指挥记录
	 */
	//private int iconType;
	
	private Long fireEngineId;

//	public int getIconType() {
//		return iconType;
//	}
//
//	public void setIconType(int iconType) {
//		this.iconType = iconType;
//	}

	public Long getFireEngineId() {
		return fireEngineId;
	}

	public void setFireEngineId(Long fireEngineId) {
		this.fireEngineId = fireEngineId;
	}
	
	
}
