package cn.stronglink.esm27.web.realTimeData.vo;

import java.util.List;

public class MQReturnDataVo {

	private String deviceAddress;//设备号
	
	private String batteryVoltage;
	
	private Object alarms;//报警信息
	
	private Double latitude;//纬度
	
	private Double pumpState;
	
	private Double longitude;//精度
	
	private List<Double> sensorValues;//通道数值
	
	private String userId;
	
	private String roomId;

	public String getDeviceAddress() {
		return deviceAddress;
	}

	public void setDeviceAddress(String deviceAddress) {
		this.deviceAddress = deviceAddress;
	}

	public String getBatteryVoltage() {
		return batteryVoltage;
	}

	public void setBatteryVoltage(String batteryVoltage) {
		this.batteryVoltage = batteryVoltage;
	}

	public Object getAlarms() {
		return alarms;
	}

	public void setAlarms(Object alarms) {
		this.alarms = alarms;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getPumpState() {
		return pumpState;
	}

	public void setPumpState(Double pumpState) {
		this.pumpState = pumpState;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public List<Double> getSensorValues() {
		return sensorValues;
	}

	public void setSensorValues(List<Double> sensorValues) {
		this.sensorValues = sensorValues;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	
}
