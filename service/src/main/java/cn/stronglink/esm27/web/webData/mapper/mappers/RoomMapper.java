package cn.stronglink.esm27.web.webData.mapper.mappers;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import cn.stronglink.esm27.entity.IncidentRecord;
import cn.stronglink.esm27.entity.Room;

public interface RoomMapper extends BaseMapper<Room>  {
	
	List<Room> qryListByParams(Pagination page, Map<String, Object> params);
	
	List<Room> qryListByRoom();
	
	List<IncidentRecord> qryTimeDifference(@Param("id") Long id);
	
	void updateTimeDifference(@Param("timeDifference") Long timeDifference,@Param("id") Long id);

	void updateRoomStatus(Long roomId);
	
	Room createRoom (Map<String, Object> params);

	Integer getRoomById(String roomId);

	Room getRoomByCode(String roomCode);

	List<IncidentRecord>  qryLastTimeDifference();

	Room getRoomInfo(Long roomId);

	void updateRenWuStatus(IncidentRecord record);

}
