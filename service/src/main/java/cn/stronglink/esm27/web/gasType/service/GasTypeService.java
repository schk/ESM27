package cn.stronglink.esm27.web.gasType.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.stronglink.esm27.entity.GasType;
import cn.stronglink.esm27.web.realTimeData.mapper.GasTypeMapper;

@Service
@Transactional(rollbackFor=Exception.class) 
public class GasTypeService {
	
	@Autowired
	private GasTypeMapper gasTypeMapper;

	public List<GasType> qryGasTypeList() {
		return gasTypeMapper.selectByMap(new HashMap<String, Object>());
	}

	public int crate(GasType vo) {
		return gasTypeMapper.insert(vo);
	}

	public int edit(GasType vo) {
		return gasTypeMapper.updateAllColumnById(vo);
	}

	public int del(String id) {
		return gasTypeMapper.deleteById(Long.valueOf(id));
	}

	public GasType qryById(String id) {
		return gasTypeMapper.selectById(id);
	}
	
}
