package cn.stronglink.esm27.web.realTimeData.vo;

import java.util.List;

public class XAxis {

	private String type;
	private Boolean boundaryGap;
	private List<String> data;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Boolean getBoundaryGap() {
		return boundaryGap;
	}
	public void setBoundaryGap(Boolean boundaryGap) {
		this.boundaryGap = boundaryGap;
	}
	public List<String> getData() {
		return data;
	}
	public void setData(List<String> data) {
		this.data = data;
	}
	
}
