package cn.stronglink.esm27.web.webData.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.core.util.AntdFile;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.GasHistoryData;
import cn.stronglink.esm27.entity.GasType;
import cn.stronglink.esm27.entity.IncidentRecord;
import cn.stronglink.esm27.entity.IncidentRecordFileRef;
import cn.stronglink.esm27.entity.Room;
import cn.stronglink.esm27.module.incidentRecord.mapper.IncidentRecordFileRefMapper;
import cn.stronglink.esm27.module.incidentRecord.mapper.IncidentRecordMapper;
import cn.stronglink.esm27.web.realTimeData.mapper.GasHistoryDataResultMapper;
import cn.stronglink.esm27.web.realTimeData.mapper.GasTypeMapper;
import cn.stronglink.esm27.web.realTimeData.vo.GasHistoryDataResultVo;
import cn.stronglink.esm27.web.realTimeData.vo.HistoryQryParams;
import cn.stronglink.esm27.web.webData.mapper.mappers.RoomMapper;

@Service
@Transactional(rollbackFor=Exception.class)
public class AccidentService {
	
	@Autowired
	private RoomMapper roomMapper;
	@Autowired
	private IncidentRecordFileRefMapper incidentRecordFileRefMapper;
	@Autowired
	private IncidentRecordMapper incidentRecordMapper;
	@Autowired
	private GasTypeMapper gasTypeMapper;
	@Autowired
	private GasHistoryDataResultMapper gasHistoryDataResultMapper;

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<Room> qryListByParams(Page<Room> page, Map<String, Object> params) {
		//查询所有的记录
		List<Room> qryListByParams = roomMapper.qryListByParams(page,params);
		page.setRecords(qryListByParams);	
		return page;
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<IncidentRecord> getEditRecord(Page<IncidentRecord> page, Map<String, Object> params) {
		page.setRecords(incidentRecordMapper.getEditRecord(page,params));	
		return page;
	}

	public void delRecord(Long recordId) {
		incidentRecordMapper.delRecord(recordId);
	}

	public void editRecord(IncidentRecord entity) {
		incidentRecordFileRefMapper.deleteFileByRecordId(entity.getId());
		Date date=new Date();
		if(entity.getFileList()!=null&&entity.getFileList().size()>0) {
			List<IncidentRecordFileRef> fileList = new ArrayList<IncidentRecordFileRef>();
			for (AntdFile doc:entity.getFileList()) {
				IncidentRecordFileRef file = new IncidentRecordFileRef();
				file.setId(IdWorker.getId());
				file.setFileName(doc.getName());
				file.setPath(doc.getUrl());
				file.setIncidentRecordId(entity.getId());
				file.setCreateTime(date);
				file.setCreateBy(WebUtil.getCurrentUser());
	            fileList.add(file);
	        }
			Map<String, Object> map=new HashMap<String, Object>();
			map.put("files", fileList);
			incidentRecordFileRefMapper.inserFiles(map);
		}
		if(incidentRecordMapper.updateById(entity)==0){
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public IncidentRecord getEditRecordById(Long recordId) {
		IncidentRecord obj = incidentRecordMapper.selectById(recordId);
		if(obj!=null) {
			List<AntdFile> fileList =incidentRecordFileRefMapper.selectFileById(recordId);
			obj.setFileList(fileList);
		}
		return obj;
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<GasType> getHistoryGasDataTitle() {
		List<GasType> gasList = gasTypeMapper.qryhistoryGasTableTitle();
		if (gasList!=null && gasList.size()>0) {
			for (GasType gasType : gasList) {
				gasType.setTitleKey("gas_"+gasType.getId_());
			}
		}
		return gasList;
	}

	public Page<Map> queryHistoryGasData(Page<Map> page, HistoryQryParams params) {
		List<GasHistoryDataResultVo> list =  gasHistoryDataResultMapper.selectByHistoryParamsByRoomPage(page,params);
		List<GasType> gasList = gasTypeMapper.qryhistoryGasTableTitle();
		List<Map> resultList = new ArrayList<Map>();
		if (list!=null && list .size()>0) {
			for (GasHistoryDataResultVo vo : list) {
				Map<String, Object> map  = new HashMap<String, Object>();
				map.put("serial", vo.getSerial());
				map.put("timeString", vo.getTimeString().substring(0, 19));
				if (gasList!=null && gasList.size()>0) {
					for (GasType gasType : gasList) {
						gasType.setTitleKey("gas_"+gasType.getId_());
						List<GasHistoryData> ghdList = JSONArray.parseArray(vo.getDataJson(), GasHistoryData.class);
						if (ghdList!=null && ghdList.size()>0) {
							for (GasHistoryData gasHistoryData : ghdList) {
								if (Long.valueOf(gasHistoryData.getGasId()).equals(gasType.getId()) && !gasHistoryData.getGasId().equals(8)) {
									map.put(gasType.getTitleKey(), gasHistoryData.getValue()+gasType.getUnit());
								}
							}
						}
					}
				}
				resultList.add(map);
			}
		}
		page.setRecords(resultList);
		return page;
	}

	
}
