package cn.stronglink.esm27.web.webData.vo;

public class Info {

	private int wNum;
	private int hNum;
	private float west;
	private float east;
	private float south;
	private float north;
	private int lineNum;
	public int getwNum() {
		return wNum;
	}
	public void setwNum(int wNum) {
		this.wNum = wNum;
	}
	public int gethNum() {
		return hNum;
	}
	public void sethNum(int hNum) {
		this.hNum = hNum;
	}
	public float getWest() {
		return west;
	}
	public void setWest(float west) {
		this.west = west;
	}
	public float getEast() {
		return east;
	}
	public void setEast(float east) {
		this.east = east;
	}
	public float getSouth() {
		return south;
	}
	public void setSouth(float south) {
		this.south = south;
	}
	public float getNorth() {
		return north;
	}
	public void setNorth(float north) {
		this.north = north;
	}
	public int getLineNum() {
		return lineNum;
	}
	public void setLineNum(int lineNum) {
		this.lineNum = lineNum;
	}
}
