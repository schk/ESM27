package cn.stronglink.esm27.web.webData.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.esm27.entity.FireLimit;
import cn.stronglink.esm27.web.webData.service.FireLimitService;

@Controller
@RequestMapping(value = "webApi/fireLimit")
public class FireLimitController extends AbstractController  {
	
	@Autowired
	private FireLimitService fireLimitService;
	
	
	/**
	 * 灭火极限列表
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryList")
	public ResponseEntity<ModelMap> qryList(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<FireLimit> data = fireLimitService.qryList();
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 灭火极限列表
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "qryListByPidId")
	public ResponseEntity<ModelMap> qryKeyUnitById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long pid) {
		List<FireLimit> data = fireLimitService.qryListByPidId(pid);
		return setSuccessModelMap(modelMap, data);
	}
	
	
}
