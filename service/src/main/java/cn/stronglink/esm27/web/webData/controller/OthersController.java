package cn.stronglink.esm27.web.webData.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.collect.Maps;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.esm27.entity.User;
import cn.stronglink.esm27.module.conventionalEquip.specialDutyEquip.service.SpecialDutyEquipService;
import cn.stronglink.esm27.module.conventionalEquip.specialDutyEquip.vo.SpecialDutyEquipVo;
import cn.stronglink.esm27.module.conventionalEquip.specialDutyEquip.vo.SpecialDutyEquipWeb;
import cn.stronglink.esm27.module.danger.danger.service.DangersService;
import cn.stronglink.esm27.module.danger.danger.vo.DangersVo;
import cn.stronglink.esm27.module.danger.danger.vo.DangersWithTypeVo;
import cn.stronglink.esm27.module.fireEngine.service.FireEngineService;
import cn.stronglink.esm27.module.fireEngine.vo.FireEngineVo;
@Controller
@RequestMapping(value = "webApi/others")
public class OthersController extends AbstractController {
	
	@Autowired
	private SpecialDutyEquipService specialDutyEquipService; 
	
	@Autowired
	private DangersService dangersService;
	
	@Autowired
	private FireEngineService fireEngineService;
	
	/**
	 * 查询器材表
	 * @param modelMap 
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/qrySpecialDutyEquip")
	public ResponseEntity<ModelMap> qrySpecialDutyEquip(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		List<SpecialDutyEquipWeb> data = specialDutyEquipService.selectEquipByParams();
		return setSuccessModelMap(modelMap, data);
	}
	
	
	/**
	 * 
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/qrySpecialDutyEquipOne")
	public ResponseEntity<ModelMap> qrySpecialDutyEquipOne(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		String typeId = params.get("typeId").toString();
		String fireBrigadeId = params.get("fireBrigadeId").toString();
		logger.info("typeId===================>{}",typeId);
		logger.info("fireBrigadeId============>{}",fireBrigadeId);
		Page<SpecialDutyEquipVo> page = (Page<SpecialDutyEquipVo>) super.getPage(params);
		Page<SpecialDutyEquipVo> data = specialDutyEquipService.selectOneType(page,typeId,fireBrigadeId);
		return setSuccessModelMap(modelMap, data);
	}
	
	
	/**
	 * 危化品库  t_dangers
	 * dutyEquipUseRecord
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/qryDangers")
	public ResponseEntity<ModelMap> qryDangers(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		Page<DangersWithTypeVo> page = (Page<DangersWithTypeVo>) super.getPage(params);
		Page<DangersWithTypeVo> data =  dangersService.selectAll(page);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 危化品库  t_dangers
	 * dutyEquipUseRecord
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/qryDangersOne")
	public ResponseEntity<ModelMap> qryDangersOne(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		String dangerId = params.get("dangersId").toString();
		DangersVo data =  dangersService .queryDangersById(Long.parseLong(dangerId));
		return setSuccessModelMap(modelMap, data);
	}
	
	
	/**
	 * 记录临时使用
	 * dutyEquipUseRecord
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/recordTempUseCount")
	public ResponseEntity<ModelMap> recordTempUseCount(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		String equipId         = params.get("equipId").toString();
		String tempUseCount    = params.get("tempUseCount").toString();
		String roomId = "";
		HttpSession session = request.getSession();
		if(session.getAttribute("roomId")!=null){
			roomId = session.getAttribute("roomId").toString();
			if(roomId=="" || roomId==null||session.getAttribute("currentUser")==null){
				return setModelMap(modelMap, HttpCode.CONFLICT, "当前没有房间信息");
			}
		}else{
			return setModelMap(modelMap, HttpCode.CONFLICT, "当前没有房间信息");
		}
//		roomId = "1022034174993821698";	
		
		User user = (User)session.getAttribute("currentUser");
		Map<String,Object> queryMap = Maps.newHashMap();
		queryMap.put("tempUseCount",tempUseCount);
		queryMap.put("equipId",equipId);
		queryMap.put("currentUser",user);
		queryMap.put("roomId",roomId);
		Map<String, Object> data =  specialDutyEquipService .recordTempUseCount(queryMap);
		if(!(boolean)data.get("flag")){
			String enableUse = data.get("enableUse").toString();
			modelMap.put("enableUse",enableUse );
			return setModelMap(modelMap,HttpCode.CONFLICT, data.get("msg").toString());
		}
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 记录临时使用
	 * dutyEquipUseRecord
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/resetEquipMent")
	public ResponseEntity<ModelMap> resetEquipMent(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long roomId) {
		Map<String, Object> data =  specialDutyEquipService .resetEquipMent(roomId);
		if(!(boolean)data.get("flag")){
			return setModelMap(modelMap, HttpCode.CONFLICT, "当前没有房间信息的器材使用记录");
		}
		return setSuccessModelMap(modelMap, data);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/qryFireEngineByBrigandeId")
	public ResponseEntity<ModelMap> qryFireEngineByBrigandeId(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		Page<FireEngineVo> page = (Page<FireEngineVo>) super.getPage(params);
		Page<FireEngineVo> data =  fireEngineService.qryFireEngineByBrigandeId(page,params);
		return setSuccessModelMap(modelMap, data);
	}
}
