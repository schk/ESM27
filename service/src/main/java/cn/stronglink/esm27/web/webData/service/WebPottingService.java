package cn.stronglink.esm27.web.webData.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.stronglink.esm27.entity.Plotting;
import cn.stronglink.esm27.web.webData.mapper.WebDataMapper;
import cn.stronglink.esm27.web.webData.mapper.WebPottingMapper;
import cn.stronglink.esm27.web.webData.vo.PlottingVo;

@Service
@Transactional(rollbackFor=Exception.class)
public class WebPottingService {
	
	@Autowired
	private WebDataMapper webDataMapper;
	
	@Autowired
	private WebPottingMapper webPottingMapper;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<PlottingVo> qryPlottingVoList() {
		List<PlottingVo> typeList=webDataMapper.qryDictionaryByTypeChild(4);
		List<PlottingVo> retypeList =new ArrayList<PlottingVo>();
		if(typeList.size()>0) {
			for(PlottingVo d:typeList) {
				List<Plotting> list =webPottingMapper.qryPlottingVoList(d.getId());
	            if(list.size()>0) {
	            	d.setChildren(list);
	            	retypeList.add(d);
				}
			}
		}
		return retypeList;
	}

	public List<PlottingVo> qryPlottingoNoList() {
		List<PlottingVo> typeList=webDataMapper.qryDictionaryByTypeChild(4);
		List<PlottingVo> retypeList =new ArrayList<PlottingVo>();
		if(typeList.size()>0) {
			for(PlottingVo d:typeList) {
				List<Plotting> list =webPottingMapper.qryPlottingoNoList(d.getId());
	            if(list.size()>0) {
	            	d.setChildren(list);
	            	retypeList.add(d);
				}
			}
		}
		return retypeList;
	}

	
}
