package cn.stronglink.esm27.web.webData.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.stronglink.core.exception.BusinessException;
import cn.stronglink.esm27.entity.Room;
import cn.stronglink.esm27.web.webData.mapper.mappers.RoomMapper;

@Service
@Transactional(rollbackFor=Exception.class)
public class RoomService {

	@Autowired
	private RoomMapper roomMapper;

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Integer getRoomById(String roomId) {
		return roomMapper.getRoomById(roomId);
	}

	public void saveAccidentInfo(Room room) {
		room.setCreateTime(new Date());
		if(roomMapper.updateById(room)==0){
			throw new BusinessException("更新失败，记录不存在或记录已被更改!");
		}		
	}

	public Room getRoomByCode(String roomCode) {
		return roomMapper.getRoomByCode(roomCode);
	}

	public Room getRoomInfo(Long roomId) {
		return roomMapper.getRoomInfo(roomId);
	}
	
	
}
