package cn.stronglink.esm27.web.math.vo;

public class MathGranularVo {

	private double vertices[];
	private double dzx;
	
	public double[] getVertices() {
		return vertices;
	}
	public void setVertices(double[] vertices) {
		this.vertices = vertices;
	}
	public double getDzx() {
		return dzx;
	}
	public void setDzx(double dzx) {
		this.dzx = dzx;
	}
	
}
