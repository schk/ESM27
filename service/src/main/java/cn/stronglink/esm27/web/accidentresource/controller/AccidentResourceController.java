package cn.stronglink.esm27.web.accidentresource.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.esm27.entity.Resource;
import cn.stronglink.esm27.web.accidentresource.domain.MapImage;
import cn.stronglink.esm27.web.accidentresource.service.AccidentResourceService;

@Controller
@RequestMapping(value = "accidentResource")
public class AccidentResourceController extends AbstractController  {
	
	@Autowired
	private AccidentResourceService accidentResourceService;
	
	/**
	 * 	查询资源。
	 * 	1：查询所有终态数据。在列表+地图展示。
	 * 	2：根据sessionId查询，数据，列表+地图展示。
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "queryResource")
	public ResponseEntity<ModelMap> queryResource(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		String sessionId = request.getSession().getId();
		params.put("sessionId", sessionId);
		List<Resource> data = accidentResourceService.queryResource(params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 * 	查询资源。
	 * 	1：查询所有终态数据。在列表+地图展示。
	 * 	2：根据sessionId查询，数据，列表+地图展示。
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "qryResourceListInMap")
	public ResponseEntity<ModelMap> qryResourceListInMap(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		List<Resource> data = accidentResourceService.qryResourceListInMap(params);
		return setSuccessModelMap(modelMap, data);
	}
	
	/**
	 *  修改资源。
	 *  查询未使用状态的数据，<br>
	 *  is_use=0， 是否使用中，0：未使用1：使用中（修改中）
	 */
	@RequestMapping(value = "updateResource")
	public ResponseEntity<ModelMap> updateResource(ModelMap modelMap, HttpServletRequest request,
		HttpServletResponse response, @RequestBody List<MapImage> params) {
		List<Resource> list=accidentResourceService.updateResource(params);
		return setSuccessModelMap(modelMap, list);
	}
	
	/**
	 *  新增资源。
	 *  *  查询未使用状态的数据，<br>
	 *  is_use=0， 是否使用中，0：未使用1：使用中（修改中）
	 */
	@RequestMapping(value = "addResource")
	public ResponseEntity<ModelMap> addResource(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		String sessionId = request.getSession().getId();
		params.put("sessionId", sessionId);
		try {
			Resource data = accidentResourceService.addResource(params);
			return setSuccessModelMap(modelMap, data);
		} catch (Exception e) {
			return setModelMap(modelMap, HttpCode.CONFLICT, "新增场景资源失败！"); 
		}
		
	}
	/**
	 * 删除 资源
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "delResourceById")
	public ResponseEntity<ModelMap> delResourceById(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Map<String, Object> params) {
		int i = accidentResourceService.delResourceById(params);
		return setSuccessModelMap(modelMap, i);
	}
}
