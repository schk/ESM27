package cn.stronglink.esm27.web.realTimeData.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.stronglink.esm27.entity.SerialPort;

public interface SerialPortMapper extends BaseMapper<SerialPort> {

}
