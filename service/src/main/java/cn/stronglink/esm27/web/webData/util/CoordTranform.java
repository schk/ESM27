package cn.stronglink.esm27.web.webData.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CoordTranform {

	private static final double LAT_OFFSET_0(double x, double y) {
        return -100.0 + 2.0 * x + 3.0 * y + 0.2 * y * y + 0.1 * x * y + 0.2 * Math.sqrt(Math.abs(x));
    }

    private static final double LAT_OFFSET_1(double x, double y) {
        return (20.0 * Math.sin(6.0 * x * Math.PI) + 20.0 * Math.sin(2.0 * x * Math.PI)) * 2.0 / 3.0;
    }

    private static final double LAT_OFFSET_2(double x, double y) {
        return (20.0 * Math.sin(y * Math.PI) + 40.0 * Math.sin(y / 3.0 * Math.PI)) * 2.0 / 3.0;
    }

    private static final double LAT_OFFSET_3(double x, double y) {
        return (160.0 * Math.sin(y / 12.0 * Math.PI) + 320 * Math.sin(y * Math.PI / 30.0)) * 2.0 / 3.0;
    }

    private static final double LON_OFFSET_0(double x, double y) {
        return 300.0 + x + 2.0 * y + 0.1 * x * x + 0.1 * x * y + 0.1 * Math.sqrt(Math.abs(x));
    }

    private static final double LON_OFFSET_1(double x, double y) {
        return (20.0 * Math.sin(6.0 * x * Math.PI) + 20.0 * Math.sin(2.0 * x * Math.PI)) * 2.0 / 3.0;
    }

    private static final double LON_OFFSET_2(double x, double y) {
        return (20.0 * Math.sin(x * Math.PI) + 40.0 * Math.sin(x / 3.0 * Math.PI)) * 2.0 / 3.0;
    }

    private static final double LON_OFFSET_3(double x, double y) {
        return (150.0 * Math.sin(x / 12.0 * Math.PI) + 300.0 * Math.sin(x / 30.0 * Math.PI)) * 2.0 / 3.0;
    }

    private static double RANGE_LON_MAX = 137.8347;
    private static double RANGE_LON_MIN = 72.004;
    private static double RANGE_LAT_MAX = 55.8271;
    private static double RANGE_LAT_MIN = 0.8293;

    private static double jzA  = 6378245.0;
    private static double jzEE = 0.00669342162296594323;

    public static double transformLat(double x, double y) {
        double ret = LAT_OFFSET_0(x, y);
        ret += LAT_OFFSET_1(x, y);
        ret += LAT_OFFSET_2(x, y);
        ret += LAT_OFFSET_3(x, y);
        return ret;
    }

    public static double transformLon(double x, double y) {
        double ret = LON_OFFSET_0(x, y);
        ret += LON_OFFSET_1(x, y);
        ret += LON_OFFSET_2(x, y);
        ret += LON_OFFSET_3(x, y);
        return ret;
    }

    public static boolean outOfChina(double lat, double lon) {
        if (lon < RANGE_LON_MIN || lon > RANGE_LON_MAX)
            return true;
        if (lat < RANGE_LAT_MIN || lat > RANGE_LAT_MAX)
            return true;
        return false;
    }

    public static LatLng gcj02Encrypt(double ggLat, double ggLon) {
        LatLng resPoint = new LatLng();
        double mgLat;
        double mgLon;
        if (outOfChina(ggLat, ggLon)) {
            resPoint.latitude = ggLat;
            resPoint.longitude = ggLon;
            return resPoint;
        }
        double dLat = transformLat(ggLon - 105.0, ggLat - 35.0);
        double dLon = transformLon(ggLon - 105.0, ggLat - 35.0);
        double radLat = ggLat / 180.0 * Math.PI;
        double magic = Math.sin(radLat);
        magic = 1 - jzEE * magic * magic;
        double sqrtMagic = Math.sqrt(magic);
        dLat = (dLat * 180.0) / ((jzA * (1 - jzEE)) / (magic * sqrtMagic) * Math.PI);
        dLon = (dLon * 180.0) / (jzA / sqrtMagic * Math.cos(radLat) * Math.PI);
        mgLat = ggLat + dLat;
        mgLon = ggLon + dLon;

        resPoint.latitude = mgLat;
        resPoint.longitude = mgLon;
        return resPoint;
    }

    public static LatLng gcj02Decrypt(double gjLat, double gjLon) {
        LatLng gPt = gcj02Encrypt(gjLat, gjLon);
        double dLon = gPt.longitude - gjLon;
        double dLat = gPt.latitude - gjLat;
        LatLng pt = new LatLng();
        pt.latitude = gjLat - dLat;
        pt.longitude = gjLon - dLon;
        return pt;
    }

    public static LatLng bd09Decrypt(double bdLat, double bdLon) {
        LatLng gcjPt = new LatLng();
        double x = bdLon - 0.0065, y = bdLat - 0.006;
        double z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * Math.PI);
        double theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * Math.PI);
        gcjPt.longitude = z * Math.cos(theta);
        gcjPt.latitude = z * Math.sin(theta);
        return gcjPt;
    }

    public static LatLng bd09Encrypt(double ggLat, double ggLon) {
        LatLng bdPt = new LatLng();
        double x = ggLon, y = ggLat;
        double z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * Math.PI);
        double theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * Math.PI);
        bdPt.longitude = z * Math.cos(theta) + 0.0065;
        bdPt.latitude = z * Math.sin(theta) + 0.006;
        return bdPt;
    }

    /**
     * @param location 世界标准地理坐标(WGS-84)
     * @return 中国国测局地理坐标（GCJ-02）<火星坐标>
     * @brief 世界标准地理坐标(WGS-84) 转换成 中国国测局地理坐标（GCJ-02）<火星坐标>
     *
     * ####只在中国大陆的范围的坐标有效，以外直接返回世界标准坐标
     */
    public static LatLng wgs84ToGcj02(LatLng location) {
        return gcj02Encrypt(location.latitude, location.longitude);
    }

    /**
     * @param location 中国国测局地理坐标（GCJ-02）
     * @return 世界标准地理坐标（WGS-84）
     * @brief 中国国测局地理坐标（GCJ-02） 转换成 世界标准地理坐标（WGS-84）
     *
     * ####此接口有1－2米左右的误差，需要精确定位情景慎用
     */
    public static LatLng gcj02ToWgs84(LatLng location) {
        return gcj02Decrypt(location.latitude, location.longitude);
    }

    /**
     * @param location 世界标准地理坐标(WGS-84)
     * @return 百度地理坐标（BD-09)
     * @brief 世界标准地理坐标(WGS-84) 转换成 百度地理坐标（BD-09)
     */
    public static LatLng wgs84ToBd09(LatLng location) {
        LatLng gcj02Pt = gcj02Encrypt(location.latitude, location.longitude);
        return bd09Encrypt(gcj02Pt.latitude, gcj02Pt.longitude);
    }

    /**
     * @param location 中国国测局地理坐标（GCJ-02）<火星坐标>
     * @return 百度地理坐标（BD-09)
     * @brief 中国国测局地理坐标（GCJ-02）<火星坐标> 转换成 百度地理坐标（BD-09)
     */
    public static LatLng gcj02ToBd09(LatLng location) {
        return bd09Encrypt(location.latitude, location.longitude);
    }

    /**
     * @param location 百度地理坐标（BD-09)
     * @return 中国国测局地理坐标（GCJ-02）<火星坐标>
     * @brief 百度地理坐标（BD-09) 转换成 中国国测局地理坐标（GCJ-02）<火星坐标>
     */
    public static LatLng bd09ToGcj02(LatLng location) {
        return bd09Decrypt(location.latitude, location.longitude);
    }

    /**
     * @param location 百度地理坐标（BD-09)
     * @return 世界标准地理坐标（WGS-84）
     * @brief 百度地理坐标（BD-09) 转换成 世界标准地理坐标（WGS-84）
     *
     * ####此接口有1－2米左右的误差，需要精确定位情景慎用
     */
    public static LatLng bd09ToWgs84(LatLng location) {
        LatLng gcj02 = bd09ToGcj02(location);
        return gcj02Decrypt(gcj02.latitude, gcj02.longitude);
    }

    /**国测局转wgs84*/
	public static double[] gcj02ToWgs84(double[] coord) {
		LatLng location = gcj02Decrypt(coord[1], coord[0]);
		double[] point = {location.longitude,location.latitude};
		return point;
	}

	/**
	 * 经纬转me卡托
	 * */
	public static double[] lnglat2mercator(double[] point ) {
		double earthRad = 6378137.0;
		double x = point[0] * Math.PI / 180 * earthRad;
		double a = point[1] * Math.PI / 180;
		double y = earthRad / 2
				* Math.log((1.0 + Math.sin(a)) / (1.0 - Math.sin(a)));
		double[] xy = { x, y };
		return xy;
	}

	/**
	 * 格式化坐标值
	 * */
	public static double[] formatLonLat(double[] point){
		BigDecimal lon = new BigDecimal(point[0]).setScale(6, RoundingMode.UP);
		BigDecimal lat = new BigDecimal(point[1]).setScale(6, RoundingMode.UP);
		double[] lonlat = {lon.doubleValue(),lat.doubleValue()};
		return lonlat;
	}
	
	public static double[] mercator2lnglat(double[] point) {
		double lng = point[0] / 20037508.34 * 180;
		double lat = point[1] / 20037508.34 * 180;
		lat = 180 / Math.PI
				* (2 * Math.atan(Math.exp(lat * Math.PI / 180)) - Math.PI / 2);
		double[] xy = { lng, lat };
		return xy;
	}

	/**
	 * 米转度
	 * */
	public static double meter2degree(int distance) {
		double degree = distance / (2 * Math.PI * 6371004) * 360;
		return degree;
	}

	
	public static String createGeom(double[] point) {
		StringBuffer sb = new StringBuffer();
		if(point.length==2){
			sb.append("point(").append(point[0]).append(" ").append(point[1]).append(")");
		}else if(point.length==4){
			sb.append("polygon(")
			.append(point[0]).append(" ").append(point[1]).append(",")
			.append(point[2]).append(" ").append(point[1]).append(",")
			.append(point[2]).append(" ").append(point[3]).append(",")
			.append(point[1]).append(" ").append(point[3]).append(",")
			.append(point[0]).append(" ").append(point[1]).append(")");
		}
		return sb.toString();
	}
}
