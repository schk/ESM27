package cn.stronglink.esm27.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.Department;
import cn.stronglink.esm27.entity.Dictionary;
import cn.stronglink.esm27.entity.EquipmentType;
import cn.stronglink.esm27.entity.FireBrigade;
import cn.stronglink.esm27.entity.GasType;
import cn.stronglink.esm27.entity.KeyUnit;
import cn.stronglink.esm27.entity.User;
import cn.stronglink.esm27.module.dictionary.service.DictionaryService;
import cn.stronglink.esm27.module.emergencyResources.fireBrigade.service.FireBrigadeService;
import cn.stronglink.esm27.module.expert.expert.vo.ExpertTypeVo;
import cn.stronglink.esm27.web.realTimeData.service.RealTimeDataService;
import cn.stronglink.esm27.web.realTimeData.vo.GasDataInitPageVo;
import cn.stronglink.esm27.web.webData.service.WebDataService;
import cn.stronglink.esm27.web.webData.service.WebPottingService;
import cn.stronglink.esm27.web.webData.vo.PlottingVo;

@Controller
@RequestMapping("")
public class IndexController extends AbstractController {
	@Autowired
	private WebDataService webDataService;
	@Autowired
	private RealTimeDataService realTimeDataService;
	@Autowired
	private WebPottingService webPottingService;
	@Autowired
	private FireBrigadeService fireBrigadeService;
	@Autowired
	private DictionaryService dictionaryService;

	@Value("${baseFileUrl}")
	private String baseFileUrl;

	@Value("${collect.url}")
	private String collectUrl;
	
	@Value("${stream.server}")
	private String streamServer;
	
	@Value("${gasUrl}")
	private String gasUrl;
	
	@Value("${offLineMapUrl}")
	private String offLineMapUrl;

	@RequestMapping()
	public String getIndex(Model model, HttpServletRequest request,HttpServletResponse response) throws IOException {
		String requestUrl =request.getContextPath(); 
	    if(WebUtil.getCurrentUser() == null) {
			response.sendRedirect(requestUrl+"/webApi/login.jhtml");
			return "login.html";
		}
		return this.getUser(model, request);
	}
	
	@RequestMapping("webApi/index")
	public String getUser(Model model, HttpServletRequest request) {
		List<Department> deptList = webDataService.qryDepts();
		List<KeyUnit> keyUnitList = webDataService.getKeyUnit();
		List<FireBrigade> fireBrigadeList = fireBrigadeService.qryList();
		List<ExpertTypeVo> expertType = webDataService.qryExpertType();
		List<Dictionary> planTypeList = webDataService.qryDictionaryByType(3);
		List<EquipmentType> equipmentTypeList = webDataService.qryEquipmentTypeList(2);
		List<Dictionary> cardTypeList = webDataService.qryDictionaryByType(1);
		List<GasType> gasTypeList = realTimeDataService.qryGasTypeList();
		List<PlottingVo> plottingVoList = webPottingService.qryPlottingVoList();
		List<PlottingVo> plottingNoList = webPottingService.qryPlottingoNoList();
		List<GasDataInitPageVo> deviceList = realTimeDataService.qryGasDataInitPage();
		List<GasType> historyGasTableTitle = realTimeDataService.qryhistoryGasTableTitle();
		List<EquipmentType> keyTypeList = webDataService.qryEquipmentTypeList(1);
		model.addAttribute("deptList", deptList);
		model.addAttribute("plottingNoList", plottingNoList);
		model.addAttribute("fireBrigadeList", fireBrigadeList);
		model.addAttribute("keyUnitList", keyUnitList);
		model.addAttribute("expertType", expertType);
		model.addAttribute("planTypeList", planTypeList);
		model.addAttribute("cardTypeList", cardTypeList);
		model.addAttribute("equipmentTypeList", equipmentTypeList);
		model.addAttribute("gasTypeList", gasTypeList);
		model.addAttribute("deviceList", deviceList);
		model.addAttribute("plottingVoList", plottingVoList);
		model.addAttribute("historyGasTableTitle", historyGasTableTitle);
		model.addAttribute("keyTypeList", keyTypeList);
		model.addAttribute("baseFileUrl", baseFileUrl);
		model.addAttribute("collectUrl", collectUrl);
		model.addAttribute("streamServer", streamServer);
		model.addAttribute("gasUrl", gasUrl);
		model.addAttribute("offLineMapUrl", offLineMapUrl);
		
		HttpSession session = request.getSession();
		if (session.getAttribute("currentUser") != null) {
			User user = (User) session.getAttribute("currentUser");
			model.addAttribute("user", user);
		}
		String ip = WebUtil.getHost(request);
		model.addAttribute("ip", ip);
		return "index.html";
	}

}
