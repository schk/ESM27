package cn.stronglink.esm27.web.webData.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.stronglink.core.base.AbstractController;
import cn.stronglink.core.support.HttpCode;
import cn.stronglink.esm27.entity.ExtinguisherUseRecord;
import cn.stronglink.esm27.entity.FireEngineUseRecord;
import cn.stronglink.esm27.module.extinguisherUseRecord.service.ExtinguisherUseRecordService;
import cn.stronglink.esm27.module.fireEngine.service.FireEngineUseRecordService;

@Controller
@RequestMapping(value = "webApi/useRecord")
public class WebUseRecordController extends AbstractController  {
	
	@Autowired
	private ExtinguisherUseRecordService eURService;
	
	@Autowired
	private FireEngineUseRecordService fUrService;
	
	
	/**
	 * 添加灭火剂得使用记录
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param room
	 * @return
	 */
	@RequestMapping(value = "insertExtinguisherUseRecord")
	public ResponseEntity<ModelMap> insertExtinguisherUseRecord(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody ExtinguisherUseRecord entity) {
		eURService.insertExtinguisherUseRecord(entity);
		return setSuccessModelMap(modelMap, entity);
	}
	
	/**
	 * 重置消防车得状态为可用
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "updateFireEngineStatus")
	public ResponseEntity<ModelMap> updateFireEngineStatus(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long id) {
		fUrService.updateFireEngineStatus(id);
		return setSuccessModelMap(modelMap, null);
	}
	
	
	/**
	 * 重置消防车得状态为可用
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "updateFireEngineStatusByRoomId")
	public ResponseEntity<ModelMap> updateFireEngineStatusByRoomId(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody Long roomId) {
		fUrService.updateFireEngineStatusByRoomId(roomId);
		return setSuccessModelMap(modelMap, null);
	}
	
	/**
	 * 添加消防车的使用记录
	 * @param modelMap
	 * @param request
	 * @param response
	 * @param room
	 * @return
	 */
	@RequestMapping(value = "insertFireEngineUseRecord")
	public ResponseEntity<ModelMap> insertFireEngineUseRecord(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response,@RequestBody FireEngineUseRecord entity) {	
		HttpSession session = request.getSession();
		if(session.getAttribute("oId")!=null){
			String oId = session.getAttribute("oId").toString();
			if(!"".equals(oId)&&oId!=null){
				entity.setRoomId(Long.parseLong(oId));
				fUrService.insertFireEngineUseRecord(entity);
				return setSuccessModelMap(modelMap, entity);
			}	
		}
		return setModelMap(modelMap, HttpCode.CONFLICT, "当前没有房间信息");
	}
	
	
	
	
}
