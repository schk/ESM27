package cn.stronglink.esm27.web.realTimeData.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;

import cn.stronglink.core.util.WebUtil;
import cn.stronglink.esm27.entity.GasDetector;
import cn.stronglink.esm27.entity.GasDetectorData;
import cn.stronglink.esm27.entity.GasHistoryData;
import cn.stronglink.esm27.entity.GasHistoryDataResult;
import cn.stronglink.esm27.entity.GasType;
import cn.stronglink.esm27.entity.SerialPort;
import cn.stronglink.esm27.entity.TimeInterval;
import cn.stronglink.esm27.entity.WeatherStation;
import cn.stronglink.esm27.entity.WeatherStationData;
import cn.stronglink.esm27.message.mq.entity.AlarmInfo;
import cn.stronglink.esm27.message.mq.entity.AutoAlarmVo;
import cn.stronglink.esm27.module.system.user.mapper.UserMapper;
import cn.stronglink.esm27.web.realTimeData.mapper.GasDetectorDataMapper;
import cn.stronglink.esm27.web.realTimeData.mapper.GasDetectorMapper;
import cn.stronglink.esm27.web.realTimeData.mapper.GasHistoryDataResultMapper;
import cn.stronglink.esm27.web.realTimeData.mapper.GasTypeMapper;
import cn.stronglink.esm27.web.realTimeData.mapper.SerialPortMapper;
import cn.stronglink.esm27.web.realTimeData.mapper.TimeIntervalMapper;
import cn.stronglink.esm27.web.realTimeData.mapper.WeatherStationDataMapper;
import cn.stronglink.esm27.web.realTimeData.mapper.WeatherStationMapper;
import cn.stronglink.esm27.web.realTimeData.vo.ChartVo;
import cn.stronglink.esm27.web.realTimeData.vo.DataConfigureVo;
import cn.stronglink.esm27.web.realTimeData.vo.DeviceConfigureVo;
import cn.stronglink.esm27.web.realTimeData.vo.GasDataInitPageVo;
import cn.stronglink.esm27.web.realTimeData.vo.GasDetectorDataVo;
import cn.stronglink.esm27.web.realTimeData.vo.GasHistoryDataResultVo;
import cn.stronglink.esm27.web.realTimeData.vo.HistoryGasDataVo;
import cn.stronglink.esm27.web.realTimeData.vo.HistoryQryParams;
import cn.stronglink.esm27.web.realTimeData.vo.MQReturnDataVo;
import cn.stronglink.esm27.web.realTimeData.vo.MQReturnWeatherDataVo;
import cn.stronglink.esm27.web.realTimeData.vo.SerialPortVo;
import cn.stronglink.esm27.web.realTimeData.vo.SeriesData;
import cn.stronglink.esm27.web.realTimeData.vo.WeatherDataVo;
import cn.stronglink.esm27.web.realTimeData.vo.XAxis;

@Service
@Transactional(rollbackFor=Exception.class) 
public class RealTimeDataService {
	@Autowired
	private GasTypeMapper gasTypeMapper;
	
	@Autowired
	private GasDetectorMapper gasDetectorMapper;
	
	@Autowired
	private GasDetectorDataMapper gasDetectorDataMapper;
	
	@Autowired
	private GasHistoryDataResultMapper gasHistoryDataResultMapper;
	
	@Autowired
	private SerialPortMapper serialPortMapper;
	
	@Autowired
	private TimeIntervalMapper timeIntervalMapper;
	
	@Autowired
	private WeatherStationMapper weatherStationMapper;
	
	@Autowired
	private WeatherStationDataMapper weatherStationDataMapper;
	
	@Autowired
	private UserMapper userMapper;
	
	public DataConfigureVo qryDataConfigure() {
		DataConfigureVo vo = new DataConfigureVo();
		List<WeatherStation> ws = weatherStationMapper.selectList(null);
		vo.setWeatherStation(ws);
		return vo;
	}
	
	

	public DataConfigureVo qrySerialPort() {
		DataConfigureVo vo = new DataConfigureVo();
		List<SerialPort> sp = serialPortMapper.selectList(null);
		vo.setSerialPort(sp.get(0).getSerialPort());
		TimeInterval ti = timeIntervalMapper.selectById(1L);
		vo.setTimeCell(ti.getTime());
		return vo;
	}

	
	
	public List<GasDetectorDataVo> insertData(Object data) {
		MQReturnDataVo vo = JSONObject.parseObject(JSONObject.toJSONString(data), MQReturnDataVo.class);
		if (vo.getSensorValues()!=null && vo.getSensorValues().size()>0) {
			if (vo.getDeviceAddress()!=null) {
				if(vo.getDeviceAddress().length()==1){
					vo.setDeviceAddress("00"+vo.getDeviceAddress());
				}else if(vo.getDeviceAddress().length()==2){
					vo.setDeviceAddress("0"+vo.getDeviceAddress());
				}
				List<GasDetectorDataVo> gddList = gasDetectorDataMapper.selectByDeviceAddress(vo.getDeviceAddress());
				if (gddList!=null && gddList.size()>0) {
					Long gasDetectorId = gddList.get(0).getGasDetectorId();
					GasHistoryDataResult ghdr = new GasHistoryDataResult();
					ghdr.setId(IdWorker.getId());
					ghdr.setCreateTime(new Date());
					ghdr.setGasDetectorId(gasDetectorId);
					List<GasHistoryData> ghdList = new ArrayList<GasHistoryData>();
					for (int i = 0; i < gddList.size(); i++) {
						GasHistoryData ghd = new GasHistoryData();
						ghd.setGasDateId(gddList.get(i).getId());
						ghd.setGasId(gddList.get(i).getGasId());
						ghd.setValue(vo.getSensorValues().get(i));
						if (gddList.get(i).getGasName().equals("氧气")) {
//							if (vo.getSensorValues().get(i).compareTo(gddList.get(i).getGasLow())>0 && vo.getSensorValues().get(i).compareTo(gddList.get(i).getGasUp())<0) {
//								ghd.setIsAlert(1);
//							}else {
//								ghd.setIsAlert(2);
//							}
							
							if(vo.getSensorValues().get(i).compareTo(gddList.get(i).getpLow())>0 && vo.getSensorValues().get(i).compareTo(gddList.get(i).getpUp())<0) {
								ghd.setIsAlert(1);
							}else {
								if(vo.getSensorValues().get(i)<gddList.get(i).getGasLow()||vo.getSensorValues().get(i)>gddList.get(i).getGasUp()) {
									ghd.setIsAlert(3);
								}else {
									ghd.setIsAlert(2);
								}
							}
							
						}else {
//							if (vo.getSensorValues().get(i).compareTo(gddList.get(i).getGasLow())>0 ) {
//								ghd.setIsAlert(2);
//							}else {
//								ghd.setIsAlert(1);
//							}
							
							if(vo.getSensorValues().get(i).compareTo(gddList.get(i).getGasLow())>0 ) {
								ghd.setIsAlert(3);
							}else {
								if(vo.getSensorValues().get(i)>gddList.get(i).getpLow()){
									ghd.setIsAlert(2);
								}else {
									ghd.setIsAlert(1);
								}
							}
						}
						gddList.get(i).setGasIsAlert(ghd.getIsAlert());
						gddList.get(i).setGasValue(ghd.getValue());
						ghdList.add(ghd);
						//gasHistoryDataMapper.insert(ghd);
					}
					ghdr.setDataJson(JSONArray.toJSONString(ghdList,SerializerFeature.DisableCircularReferenceDetect));
					ghdr.setRoomId(vo.getRoomId());
					Calendar cal = Calendar.getInstance();
					int month = cal.get(Calendar.MONTH) + 1;
					ghdr.setMonth(month+"");
					gasHistoryDataResultMapper.insertByMonth(ghdr);
					return gddList;
				}
			}
		}
		return null;		
	}
	
	public WeatherStationData insertWeatherData(Object data) {
		MQReturnWeatherDataVo vo = JSONObject.parseObject(JSONObject.toJSONString(data), MQReturnWeatherDataVo.class);
		WeatherStationData wsd = new WeatherStationData();
		wsd.setId(IdWorker.getId());
		wsd.setCreateTime(new Date());
		wsd.setHumidity(vo.getHumidity());
		wsd.setNoise(vo.getNoise());
		wsd.setPmValue(vo.getPm2_5());
		wsd.setPressure(vo.getPressure());
		wsd.setTemperature(vo.getTemperature());
		wsd.setWeatherStationId(1L);
		wsd.setWindDirectionValue(vo.getWinDirection());
		wsd.setRoomId(vo.getRoomId());
		if (Double.valueOf(vo.getWinDirection()) < 11.25 || Double.valueOf(vo.getWinDirection())>=348.75) {
			wsd.setWindDirection("N");
		}else if (Double.valueOf(vo.getWinDirection()) < 33.45 && Double.valueOf(vo.getWinDirection())>=11.25) {
			wsd.setWindDirection("NNE");
		}else if (Double.valueOf(vo.getWinDirection()) < 55.95 && Double.valueOf(vo.getWinDirection())>=33.45) {
			wsd.setWindDirection("NE");
		}else if (Double.valueOf(vo.getWinDirection()) < 78.45 && Double.valueOf(vo.getWinDirection())>=55.95) {
			wsd.setWindDirection("ENE");
		}else if (Double.valueOf(vo.getWinDirection()) < 100.95 && Double.valueOf(vo.getWinDirection())>=78.45) {
			wsd.setWindDirection("E");
		}else if (Double.valueOf(vo.getWinDirection()) < 123.45 && Double.valueOf(vo.getWinDirection())>=100.95) {
			wsd.setWindDirection("ESE");
		}else if (Double.valueOf(vo.getWinDirection()) < 145.95 && Double.valueOf(vo.getWinDirection())>=123.45) {
			wsd.setWindDirection("SE");
		}else if (Double.valueOf(vo.getWinDirection()) < 168.45 && Double.valueOf(vo.getWinDirection())>=145.95) {
			wsd.setWindDirection("SSE");
		}else if (Double.valueOf(vo.getWinDirection()) < 190.95 && Double.valueOf(vo.getWinDirection())>=168.45) {
			wsd.setWindDirection("S");
		}else if (Double.valueOf(vo.getWinDirection()) < 213.45 && Double.valueOf(vo.getWinDirection())>=190.95) {
			wsd.setWindDirection("SSW");
		}else if (Double.valueOf(vo.getWinDirection()) < 235.95 && Double.valueOf(vo.getWinDirection())>=213.45) {
			wsd.setWindDirection("SW");
		}else if (Double.valueOf(vo.getWinDirection()) < 258.45 && Double.valueOf(vo.getWinDirection())>=235.95) {
			wsd.setWindDirection("WSW");
		}else if (Double.valueOf(vo.getWinDirection()) < 280.95 && Double.valueOf(vo.getWinDirection())>=258.45) {
			wsd.setWindDirection("W");
		}else if (Double.valueOf(vo.getWinDirection()) < 303.45 && Double.valueOf(vo.getWinDirection())>=280.95) {
			wsd.setWindDirection("WNW");
		}else if (Double.valueOf(vo.getWinDirection()) < 325.45 && Double.valueOf(vo.getWinDirection())>=303.45) {
			wsd.setWindDirection("NW");
		}else if (Double.valueOf(vo.getWinDirection()) < 347.95 && Double.valueOf(vo.getWinDirection())>=325.45) {
			wsd.setWindDirection("NNW");
		}
		wsd.setWindSpeed(vo.getWindSpeed());
		weatherStationDataMapper.insert(wsd);
		return wsd;
	}

	public void updateDataConfigure(DataConfigureVo vo) {
		Long userId = WebUtil.getCurrentUser();
		Long fireBrigedeId = userMapper.findBrigedeIdByUserId(userId);
		
		//以下注释是没修改页面之前的
//		if (vo.getGasDetector()!=null && vo.getGasDetector().size()>0) {
//			List<GasDetector> gdList = gasDetectorMapper.selectList(null);
//			if (gdList!=null && gdList.size()>0) {//表中已经添加过数据，需筛选后更新
//				List<String> dbNoList = new ArrayList<String>();//数据库中存在的NoList
//				for (GasDetector gasDetector : gdList) {
//					dbNoList.add(gasDetector.getSerial());//表中存在的设备
//				}
//				List<String> existNoList = new ArrayList<String>();//新数据在数据库中存在的NoList
//				for (GasDetector gd : vo.getGasDetector()) {//前台页面传过来的设备列表
//					if (dbNoList.contains(gd.getSerial())) {//如果新数据的no在库中存在，更新
//						existNoList.add(gd.getSerial());
//						gd.setShiDel(false);
//						gd.setUpdateTime(new Date());
//						gd.setFireBrigedeId(fireBrigedeId);
//						EntityWrapper<GasDetector> wrapper = new EntityWrapper<GasDetector>();
//						wrapper.where("serial_ = {0}", gd.getSerial());
//						gasDetectorMapper.update(gd, wrapper);
//					}else {//不存在新增
//						gd.setId(IdWorker.getId());
//						gd.setShiDel(false);
//						gd.setFireBrigedeId(fireBrigedeId);
//						gd.setCreateTime(new Date());
//						gasDetectorMapper.insert(gd);
//						for (int i = 0; i < 7; i++) {
//							GasDetectorData gdd = new GasDetectorData();
//							gdd.setId(IdWorker.getId());
//							gdd.setGasDetectorId(gd.getId());
//							gdd.setDetectorPosition(i+1);
//							gdd.setCreateTime(new Date());
//							gasDetectorDataMapper.insert(gdd);
//						}
//					}
//				}
//				if (existNoList.size() != dbNoList.size()) {//若存在的no数不等于库中no数。则把新数据中删除的no，在库中删除
//					for (String db : dbNoList) {
//						if (!existNoList.contains(db)) {
//							GasDetector gd = new GasDetector();
//							gd.setShiDel(true);
//							//gd.setSerial(db);
//							gd.setFireBrigedeId(fireBrigedeId);
//							EntityWrapper<GasDetector> wrapper = new EntityWrapper<GasDetector>();
//							wrapper.where("serial_ = {0}", db);
//							gasDetectorMapper.update(gd, wrapper);
//						}
//					}
//				}
//			}else{//未添加过，直接插入
//				for (GasDetector gd : vo.getGasDetector()) {
//					gd.setId(IdWorker.getId());
//					gd.setShiDel(false);
//					gd.setFireBrigedeId(fireBrigedeId);
//					gasDetectorMapper.insert(gd);
//					for (int i = 0; i < 7; i++) {
//						GasDetectorData gdd = new GasDetectorData();
//						gdd.setId(IdWorker.getId());
//						gdd.setGasDetectorId(gd.getId());
//						gdd.setDetectorPosition(i+1);
//						gasDetectorDataMapper.insert(gdd);
//					}
//				}
//			}
//		}
		
		//添加改通道的气体
		if (vo.getGasDetectorData()!=null && vo.getGasDetectorData().size()>0) {
			for (GasDetectorData entity : vo.getGasDetectorData()) {
				gasDetectorDataMapper.updateById(entity);
			}
		}
		
		if (vo.getWeatherStation()!=null && vo.getWeatherStation().size()>0) {
			WeatherStation ws = vo.getWeatherStation().get(0);
			ws.setId(1L);
			weatherStationMapper.updateById(ws);
		}
		
	}

	public void addConfigure(GasDetector entity) {
		Long userId = WebUtil.getCurrentUser();
		Long fireBrigedeId = userMapper.findBrigedeIdByUserId(userId);
		entity.setId(IdWorker.getId());
		entity.setShiDel(false);
		entity.setFireBrigedeId(fireBrigedeId);
		gasDetectorMapper.insert(entity);
		for (int i = 0; i < 7; i++) {
			GasDetectorData gdd = new GasDetectorData();
			gdd.setId(IdWorker.getId());
			gdd.setGasDetectorId(entity.getId());
			gdd.setDetectorPosition(i+1);
			gasDetectorDataMapper.insert(gdd);
		}
		
	}
	
	public DeviceConfigureVo qryDeviceConfigure() {
		DeviceConfigureVo vo = new DeviceConfigureVo();
		Map<String,Object> gdMap = new HashMap<String,Object>();
		gdMap.put("is_del", false);
		List<GasDetector> gdList = gasDetectorMapper.selectByMap(gdMap);
		vo.setGasDetector(gdList);
		List<GasDetectorDataVo> gddVoList =  gasDetectorDataMapper.selectAllData();
		vo.setGasDetectorDataVo(gddVoList);
		List<WeatherStation> ws = weatherStationMapper.selectList(null);
		vo.setWeatherStation(ws);
		return vo;
	}

	public void updateDeviceConfigure(List<GasDetectorData> vo) {
		if (vo!=null && vo.size()>0) {
			for (GasDetectorData entity : vo) {
				gasDetectorDataMapper.updateById(entity);
			}
		}
	}

	public String qryDeviceTime() {
		TimeInterval ti = timeIntervalMapper.selectById(1L);
		return ti.getTime();
	}

	public void updateDeviceTimee(DataConfigureVo vo) {
		TimeInterval ti = new TimeInterval();
		ti.setId(1L);
		ti.setTime(vo.getTimeCell());
		ti.setUpdateBy(WebUtil.getCurrentUser());
		ti.setUpdateTime(new Date());
		timeIntervalMapper.updateById(ti);
		if (vo.getSerialPort()!=null && !"".equals(vo.getSerialPort())) {
			SerialPort sp = new SerialPort();
			sp.setId(1L);
			sp.setSerialPort(vo.getSerialPort());
			sp.setUpdateBy(WebUtil.getCurrentUser());
			sp.setUpdateTime(new Date());
			serialPortMapper.updateById(sp);
		}
		
	}

	public SerialPortVo qrySerialPortVo() {
		SerialPort sp = serialPortMapper.selectById(1L);
		TimeInterval ti = timeIntervalMapper.selectById(1L);
		SerialPortVo vo = new SerialPortVo();
		vo.setAutoPushSensorDataIntervalSeconds(Integer.valueOf(ti.getTime()));
		vo.setSerialPortName(sp.getSerialPort());
		return vo;
	}

	public List<GasType> qryGasTypeList() {
		List<GasType> gtList =  gasTypeMapper.qryGasType();
		return gtList;
	}

	public List<GasDataInitPageVo> qryGasDataInitPage() {
		List<String> deviceSerialList = gasDetectorMapper.selectSerialList();
		List<GasDataInitPageVo> voList = new ArrayList<GasDataInitPageVo>();
		if (deviceSerialList!=null && deviceSerialList.size()>0) {
			for (String ds : deviceSerialList) {
				GasDataInitPageVo vo = new GasDataInitPageVo();
				vo.setDeviceSerial(ds);
				List<GasDetectorDataVo> gddVo = gasDetectorDataMapper.selectByDeviceAddress(ds);
				if (gddVo!=null && gddVo.size()>0) {
					vo.setGasDetectorDataVo(gddVo);
				}
				voList.add(vo);
			}
		}
		return voList;
	}

	public List<GasDetectorDataVo> getTestData() {
		return gasDetectorDataMapper.getTestData();
	}

	public Integer openLock(String psd) {
		return gasDetectorDataMapper.openLock(psd);
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<WeatherDataVo> queryHistoryWeatherData(Page<WeatherDataVo> page, HistoryQryParams params) {
		List<WeatherDataVo> data = weatherStationMapper.queryHistoryWeatherDataByPage(page,params);
//		if (data!=null && data.size()>0) {
//			for (WeatherDataVo weatherDataVo : data) {
//				weatherDataVo.setTimeString(weatherDataVo.getTimeString().substring(0, 19));
//			}
//		}
		page.setRecords(data);
		return page;
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public ChartVo queryHistoryWeatherChart(HistoryQryParams params) {
		ChartVo vo = new ChartVo();
		vo.setTitle("气象数据折线图");
		List<String> legendData = new ArrayList<String>();
		legendData.add("风速");
		legendData.add("PM2.5");
		legendData.add("温度");
		legendData.add("湿度");
		legendData.add("气压");
		legendData.add("噪音");
		vo.setLegendData(legendData);
		List<WeatherDataVo> list = weatherStationMapper.queryHistoryWeatherData(params);
		XAxis xAxis = new XAxis();
		List<String> xAxisData = new ArrayList<String>();
		List<SeriesData> sdList = new ArrayList<SeriesData>();
		if (list!=null && list.size()>0) {
			List<String> sdataList1 = new ArrayList<String>();
			List<String> sdataList2 = new ArrayList<String>();
			List<String> sdataList3 = new ArrayList<String>();
			List<String> sdataList4 = new ArrayList<String>();
			List<String> sdataList5 = new ArrayList<String>();
			List<String> sdataList6 = new ArrayList<String>();
			for (WeatherDataVo weatherDataVo : list) {
				xAxisData.add(weatherDataVo.getTimeString().substring(0,weatherDataVo.getTimeString().length()-2));
				sdataList1.add(weatherDataVo.getWindSpeed());
				sdataList2.add(weatherDataVo.getPmValue());
				sdataList3.add(weatherDataVo.getTemperature());
				sdataList4.add(weatherDataVo.getHumidity());
				sdataList5.add(weatherDataVo.getPressure());
				sdataList6.add(weatherDataVo.getNoise());
			}
			for (int i = 0; i < legendData.size(); i++) {
				SeriesData sd = new SeriesData();
				sd.setName(legendData.get(i));
				sd.setStack("总量");
				sd.setType("line");
				if (i==0) {
					sd.setData(sdataList1);
				}else if (i==1) {
					sd.setData(sdataList2);
				}else if (i==2) {
					sd.setData(sdataList3);
				}else if (i==3) {
					sd.setData(sdataList4);
				}else if (i==4) {
					sd.setData(sdataList5);
				}else if (i==5) {
					sd.setData(sdataList6);
				}
				sdList.add(sd);
			}
		}
		xAxis.setData(xAxisData);
		vo.setxAxis(xAxis);
		vo.setSeriesData(sdList);
		return vo;
	}

	public List<GasType> qryhistoryGasTableTitle() {
		return gasTypeMapper.qryhistoryGasTableTitle();
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<HistoryGasDataVo> queryHistoryGasData(Page<HistoryGasDataVo> page, HistoryQryParams params) throws ParseException {
		SimpleDateFormat sim=new SimpleDateFormat("yyyy-MM-dd");
		String startDates = params.getStartTime();
		String endDates = params.getEndTime();
		Calendar cal = Calendar.getInstance();
		cal.setTime(sim.parse(startDates));
		int startMonth = cal.get(Calendar.MONTH)+1;
		cal.setTime(sim.parse(endDates));
		int endMonth =  cal.get(Calendar.MONTH)+1;
		List<GasHistoryDataResultVo> list = null;
		if (startMonth == endMonth) {
			params.setStartMonth(startMonth+"");
			list =  gasHistoryDataResultMapper.selectByHistoryParamsByPage(page,params);
		}else {
			params.setStartMonth(startMonth+"");
			params.setEndMonth(endMonth+"");
			list =  gasHistoryDataResultMapper.selectByHistoryParamsByPageTwoMonth(page,params);
		}
		List<HistoryGasDataVo> resultList = new ArrayList<HistoryGasDataVo>();
		if (list!=null && list .size()>0) {
			for (GasHistoryDataResultVo vo : list) {
				HistoryGasDataVo hgdVo = new HistoryGasDataVo();
				hgdVo.setSerial(vo.getSerial());
				hgdVo.setTimeString(vo.getTimeString());
				List<GasHistoryData> ghdList = JSONArray.parseArray(vo.getDataJson(), GasHistoryData.class);
				hgdVo.setGhdList(ghdList);
				resultList.add(hgdVo);
			}
		}
		page.setRecords(resultList);
		return page;
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public ChartVo queryHistoryGasChart(HistoryQryParams params) throws ParseException {
		ChartVo vo = new ChartVo();
		vo.setTitle("气体数据折线图："+params.getDeviceSerial());
		SimpleDateFormat sim=new SimpleDateFormat("yyyy-MM-dd");
		String startDates = params.getStartTime();
		String endDates = params.getEndTime();
		Calendar cal = Calendar.getInstance();
		cal.setTime(sim.parse(startDates));
		int startMonth = cal.get(Calendar.MONTH)+1;
		cal.setTime(sim.parse(endDates));
		int endMonth =  cal.get(Calendar.MONTH)+1;
		List<GasHistoryDataResultVo> dataList = null;
		if (startMonth == endMonth) {
			params.setStartMonth(startMonth+"");
			dataList =  gasHistoryDataResultMapper.selectByHistoryParams(params);
		}else {
			params.setStartMonth(startMonth+"");
			params.setEndMonth(endMonth+"");
			dataList =  gasHistoryDataResultMapper.selectByHistoryParamsTwoMonth(params);
		}
		if (dataList!= null && dataList.size()>0) {
			List<Integer> gasIdList = new ArrayList<Integer>();
			List<String> xAxisData = new ArrayList<String>();
			for (GasHistoryDataResultVo dataVo : dataList) {
				xAxisData.add(dataVo.getTimeString().substring(0,dataVo.getTimeString().length()-2));
				List<GasHistoryData> ghdList = JSONArray.parseArray(dataVo.getDataJson(), GasHistoryData.class);
				for (GasHistoryData gasHistoryData : ghdList) {
					if (!gasIdList.contains(gasHistoryData.getGasId())&&gasHistoryData.getGasId()!=8) {
						gasIdList.add(gasHistoryData.getGasId());
					}
				}
			}
			List<GasType> gtList = gasTypeMapper.selectBatchIds(gasIdList);
			List<String> legendData = new ArrayList<String>();
			for (GasType gasType : gtList) {
				legendData.add(gasType.getName());
			}
			vo.setLegendData(legendData);
			XAxis xAxis = new XAxis();
			xAxis.setData(xAxisData);
			vo.setxAxis(xAxis);
			List<SeriesData> sdList = new ArrayList<SeriesData>();
			for (int i = 0; i < gtList.size(); i++) {
				SeriesData sd = new SeriesData();
				sd.setName(gtList.get(i).getName());
				sd.setStack("总量");
				sd.setType("line");
				List<String> sdataList = new ArrayList<String>();
				for (GasHistoryDataResultVo dataVo : dataList) {
					List<GasHistoryData> ghdList = JSONArray.parseArray(dataVo.getDataJson(), GasHistoryData.class);
					Boolean isHaveGas = false;
					for (GasHistoryData gasHistoryData : ghdList) {
						if (gasHistoryData.getGasId().equals(Integer.valueOf(gtList.get(i).getId_()))) {
							isHaveGas = true;
							sdataList.add(gasHistoryData.getValue().toString());
						}
					}
					if (!isHaveGas) {
						sdataList.add("");
					}
				}
				sd.setData(sdataList);
				sdList.add(sd);
			}
			vo.setSeriesData(sdList);
		}else {
			XAxis xAxis = new XAxis();
			xAxis.setData(new ArrayList<String>());
			vo.setxAxis(xAxis);
		}
		return vo;
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<GasDetectorDataVo> queryGasDetectorDataByDev(AutoAlarmVo autoAlarmVo) {
		if(autoAlarmVo.getDeviceAddress().length()==1){
			autoAlarmVo.setDeviceAddress("00"+autoAlarmVo.getDeviceAddress());
		}else if(autoAlarmVo.getDeviceAddress().length()==2){
			autoAlarmVo.setDeviceAddress("0"+autoAlarmVo.getDeviceAddress());
		}
		List<GasDetectorDataVo> gasDetectorDataVoList = gasDetectorDataMapper.selectByDeviceAddress(autoAlarmVo.getDeviceAddress());
		for(int i=0;i<gasDetectorDataVoList.size();i++) {
			GasDetectorDataVo gasDetectorData = gasDetectorDataVoList.get(i);
			gasDetectorData.setGasIsAlert(1);
			for(int j=0;j<autoAlarmVo.getAlarmInfoList().size();j++) {
				AlarmInfo alarm = autoAlarmVo.getAlarmInfoList().get(j);
				if(alarm.getSensorChannelNumber()==gasDetectorData.getDetectorPosition()) {
					gasDetectorData.setGasIsAlert(alarm.isAlarm()?2:1);
				}
			}
		}
		return gasDetectorDataVoList;
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<GasDetector> qryGasDetector(Map<String, Object> gdMap) {
		return gasDetectorMapper.selectByMap(gdMap);
	}

	/**
	 * 删除设备
	 * @param deviceId
	 */
	public void delConfigure(Long deviceId) {
		gasDetectorMapper.deleteById(deviceId);
		Map<String,Object> columnMap = new HashMap<String,Object>();
		columnMap.put("gas_detector_id", deviceId);
		gasDetectorDataMapper.deleteByMap(columnMap);
	}


	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<HistoryGasDataVo> queryHistoryGasDataNoPage(HistoryQryParams params) throws ParseException {
		SimpleDateFormat sim=new SimpleDateFormat("yyyy-MM-dd");
		String startDates = params.getStartTime();
		String endDates = params.getEndTime();
		Calendar cal = Calendar.getInstance();
		cal.setTime(sim.parse(startDates));
		int startMonth = cal.get(Calendar.MONTH)+1;
		cal.setTime(sim.parse(endDates));
		int endMonth =  cal.get(Calendar.MONTH)+1;
		List<GasHistoryDataResultVo> list = null;
		if (startMonth == endMonth) {
			params.setStartMonth(startMonth+"");
			list =  gasHistoryDataResultMapper.selectByHistoryParamsNoPage(params);
		}else {
			params.setStartMonth(startMonth+"");
			params.setEndMonth(endMonth+"");
			list =  gasHistoryDataResultMapper.selectByHistoryParamsTwoMonthNoPage(params);
		}
		List<HistoryGasDataVo> resultList = new ArrayList<HistoryGasDataVo>();
		if (list!=null && list .size()>0) {
			for (GasHistoryDataResultVo vo : list) {
				HistoryGasDataVo hgdVo = new HistoryGasDataVo();
				hgdVo.setSerial(vo.getSerial());
				hgdVo.setTimeString(vo.getTimeString());
				List<GasHistoryData> ghdList = JSONArray.parseArray(vo.getDataJson(), GasHistoryData.class);
				hgdVo.setGhdList(ghdList);
				resultList.add(hgdVo);
			}
		}
		return resultList;
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<WeatherDataVo> queryHistoryWeatherDataNoPage(HistoryQryParams params) {
		List<WeatherDataVo> data = weatherStationMapper.queryHistoryWeatherDataNoPage(params);
		return data;
	}

	

	

}
