package cn.stronglink.esm27;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.baomidou.mybatisplus.annotations.DataSource;

@EnableAutoConfiguration
@EnableTransactionManagement
@SpringBootApplication(scanBasePackages = { "cn.stronglink" })
public class Esm27Application {
	 
	public static void main(String[] args) {
		SpringApplication.run(Esm27Application.class, args);
	}
}
