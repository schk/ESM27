package cn.stronglink.springboot.config;

import javax.jms.Queue;
import javax.jms.Topic;

import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 创建 点对点模式的对象和发布订阅模式的对象
 * Created by bmc on 2018/7/1.
 */
@Configuration
public class MessageConfig {

	@Bean
    public Queue queue() {
        return new ActiveMQQueue("sample.queue");
    }

    @Bean
    public Topic topic() {
        return new ActiveMQTopic("ServiceToESM27");
    }
    
    @Bean
    public Topic algorithmTopic() {
        return new ActiveMQTopic("ServiceToESM27Algorithm");
    }
}
