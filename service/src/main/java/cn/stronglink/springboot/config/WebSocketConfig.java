package cn.stronglink.springboot.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		// 注册一个stomp的节点，使用SockJS协议
		registry.addEndpoint("/room").setAllowedOriginPatterns("*").withSockJS().setStreamBytesLimit(512*1024).setHttpMessageCacheSize(10000);
	}

	@Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        //config.setApplicationDestinationPrefixes("/app");
        //使用内置的消息代理进行订阅和广播；路由消息的目标头以“/topic”或“/queue”开头。
        config.enableSimpleBroker("/topic");
    }

}
