ESM.Drag = function(dragCallBack,mouseUpCallBack) {
	ol.interaction.Pointer.call(this, {
		handleDownEvent : ESM.Drag.prototype.handleDownEvent,
		handleDragEvent : ESM.Drag.prototype.handleDragEvent,
		handleMoveEvent : ESM.Drag.prototype.handleMoveEvent,
		handleUpEvent : ESM.Drag.prototype.handleUpEvent
	});

	/**
	 * @type {ol.Pixel}
	 * @private
	 */
	this.coordinate_ = null;
	
	this.coordinate_old = null;

	/**
	 * @type {string|undefined}
	 * @private
	 */
	this.cursor_ = 'pointer';

	/**
	 * @type {ol.Feature}
	 * @private
	 */
	this.feature_ = null;
	
	/**
	 * 
	 * */
	this.feature_old = null;

	/**
	 * @type {string|undefined}
	 * @private
	 */
	this.previousCursor_ = undefined;

};
ol.inherits(ESM.Drag, ol.interaction.Pointer);

/**
 * @param {ol.MapBrowserEvent} evt Map browser event.
 * @return {boolean} `true` to start the drag sequence.
 */
ESM.Drag.prototype.handleDownEvent = function(evt) {
	var map = evt.map;
	var feature = map.forEachFeatureAtPixel(evt.pixel, function(feature) {
		if(feature.get('drag')&&feature.get('isNear')){
			return feature;
		}else{
			return null;
		}
	});

	if (feature) {
		this.coordinate_ = evt.coordinate;
		this.feature_ = feature;
		this.coordinate_old = this.coordinate_.concat();
	}

	return !!feature;
};

/**
 * @param {ol.MapBrowserEvent} evt Map browser event.
 */
ESM.Drag.prototype.handleDragEvent = function(evt) {
	if(this.feature_){
		var deltaX = evt.coordinate[0] - this.coordinate_[0];
		var deltaY = evt.coordinate[1] - this.coordinate_[1];

		var geometry = this.feature_.getGeometry();
		geometry.translate(deltaX, deltaY);

		this.coordinate_[0] = evt.coordinate[0];
		this.coordinate_[1] = evt.coordinate[1];
		dragCallBack(evt.coordinate,this.feature_.get('center'),this.feature_);
	}
};

/**
 * @param {ol.MapBrowserEvent} evt Event.
 */
ESM.Drag.prototype.handleMoveEvent = function(evt) {
	if (this.cursor_) {
		var map = evt.map;
		var feature = map.forEachFeatureAtPixel(evt.pixel, function(feature) {
			if(feature.get('drag')&&feature.get('isNear')){
				return feature;
			}else{
				return null;
			}
		});
		var element = evt.map.getTargetElement();
		if (feature) {
			if (element.style.cursor != this.cursor_) {
				this.previousCursor_ = element.style.cursor;
				element.style.cursor = this.cursor_;
			}
		} else if (this.previousCursor_ !== undefined) {
			element.style.cursor = this.previousCursor_;
			this.previousCursor_ = undefined;
		}
	}
};

/**
 * @return {boolean} `false` to stop the drag sequence.
 */
ESM.Drag.prototype.handleUpEvent = function() {
	if(!(this.coordinate_[0]==this.coordinate_old[0]&&this.coordinate_[1]==this.coordinate_old[1])){
		mouseUpCallBack(this.coordinate_,this.feature_.get('center'));
	}
	this.coordinate_ = null;
	this.feature_ = null;
	return false;
};