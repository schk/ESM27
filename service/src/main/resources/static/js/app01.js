var normalPage = 1;
var specialPage = 1;
var carPage = 1;
var wuziPage = 1;
var totalPageSize = 5;
var totalPageSizeForFireCar = 8;
//调用器材函数
function dispatch(event) {
	// 获取调用输入框的value值  如果小于库存禁止调用
	if(event.target.children[0] && event.target.children[0].value){
	    console.info($('.storage_'+event.target.id).text());
		// 比较输入数量与库存
		if($('.storage_'+event.target.id).text()<event.target.children[0].value){
			layer.msg("当前输入的数量大于库存量，无法调用，请重试", {time: 1000});
			return;
		}
		// 调起调用接口
        var data = {
		    tempUseCount:event.target.children[0].value,
            equipId:event.target.id
        };
        $.ajax({
            type : "post",
            url : baseUrl+"/webApi/others/recordTempUseCount.jhtml",
            data: JSON.stringify(data),
            dataType:"json",
            headers: {"Content-Type": "application/json;charset=utf-8"},
            success : function(data) {
                console.info(data.data);
                // 如果获取不到数据，打印后台错误日志
                if(data.httpCode == '409'){
                	layer.msg("当前没有事故，不能使用调用应急物资功能", {time: 1000});
                	// 更新可调用数量
                    $('.storage_'+event.target.id).text(data.enableUse);
                    return;
                }else{
                    // 更新可调用数量
                	layer.msg("调用成功", {time: 1000});
                    $('.storage_'+event.target.id).text(data.data.enableUse);
                }
            },error: function(request) {
                layer.msg("网络错误", {time: 1000});
            }
        })
		console.info(event.target.children[0].value);
		// 获取id值
		console.info(event.target.id);
	}
}

/*根据参数搜索js中的相对应的数据 参数(类型,关键字,所属单位)*/
function getListData(type,fireBrigadeId){
	if(type == "消防车"){
        // 加载消防车
		$("#carList").html("");
        $("#carPage").html("");
        var normalParams={"pageNum":carPage,"pageSize":totalPageSizeForFireCar,"fireBrigadeId":fireBrigadeId};
        var normalHtml = "";
        $.ajax({
            type : "post",
            url : baseUrl+"/webApi/others/qryFireEngineByBrigandeId.jhtml",
            data: JSON.stringify(normalParams),
            dataType:"json",
            ladeview:false,
            headers: {"Content-Type": "application/json;charset=utf-8"},
            success : function(data) {
            	if(data.data&&data.data.length>0){
            		 var currentPage = Number(data.current);
                     var totalPages = Number(data.pages);
                     var html = '';
                     if(currentPage>=1 && totalPages>1){
                         generatePaginationApp(currentPage,totalPages,"carPage",fireBrigadeId);
                     }
                     for(var j = 0,len=data.data.length; j < len; j++) {
                         normalHtml+=getCarHtml(data.data[j],j);
                     }
            	}else{
            		normalHtml+="<tr><td colspan='8' class='tableNoData'>暂无数据</td></tr>";
            	}
               
                $("#carList").append(normalHtml);
            },error: function(request) {
                layer.msg("网络错误", {time: 1000});
            }
        })
    }
    if(type == "常规器材"){
        // 加载常规器材
    	 $("#normalList").html("");
         $("#normal").html("");
        var normalTypeId='3';
        var normalParams={"typeId":normalTypeId,"pageNum":normalPage,"pageSize":totalPageSize,"fireBrigadeId":fireBrigadeId};
        var normalHtml = "";
        $.ajax({
            type : "post",
            url : baseUrl+"/webApi/others/qrySpecialDutyEquipOne.jhtml",
            data: JSON.stringify(normalParams),
            dataType:"json",
            ladeview:false,
            headers: {"Content-Type": "application/json;charset=utf-8"},
            success : function(data) {
            	if(data.data&&data.data.length>0){
            		var currentPage = Number(data.current);
                    var totalPages = Number(data.pages);
                    var html = '';
                    if(currentPage>=1 && totalPages>1){
                        generatePaginationApp(currentPage,totalPages,"normal",fireBrigadeId);
                    }
                    for(var j = 0,len=data.data.length; j < len; j++) {
                        normalHtml+=getNormalHtml(data.data[j],j);
                    }
            	}else{
            		normalHtml = "<tr><td colspan='8' class='tableNoData'>暂无数据</td></tr>";
                }
                
                $("#normalList").append(normalHtml);
            },error: function(request) {
                layer.msg("网络错误", {time: 1000});
            }
        })
    }
    if(type == "专勤器材"){
        // 加载专勤器材
    	$("#specialList").html("");
        $("#special").html("");
        var typeId='1';
        var params={"typeId":typeId,"pageNum":specialPage,"pageSize":totalPageSize,"fireBrigadeId":fireBrigadeId};
        var specialHtml = "";
        $.ajax({
            type : "post",
            url : baseUrl+"/webApi/others/qrySpecialDutyEquipOne.jhtml",
            data: JSON.stringify(params),
            dataType:"json",
            ladeview:false,
            headers: {"Content-Type": "application/json;charset=utf-8"},
            success : function(data) {
            	if(data.data&&data.data.length>0){
            		var currentPage = Number(data.current);
                    var totalPages = Number(data.pages);
                    var features = [];
                    if(currentPage>=1 && totalPages>1){
                        generatePaginationApp(currentPage,totalPages,"special",fireBrigadeId);
                    }
                    for(var j = 0,len=data.data.length; j < len; j++) {
                        specialHtml+=getSpecialHtml(data.data[j],j);
                    }
            	}else{
            		specialHtml+="<tr><td colspan='8' class='tableNoData'>暂无数据</td></tr>";
            	}
                
                $("#specialList").append(specialHtml);
            },error: function(request) {
                layer.msg("网络错误", {time: 1000});
            }
        })
    }

}

// 获取页面下拉
function getPageHtmlApp(start,end,totalPages,pageList,currentPage){
		for(var i=start;i<=end;i++){
			if(i<=totalPages){
				var temp =""
				if(i==currentPage){
					temp="<a name='findPage' pageNum='"+i+"' class='onlink'>"+i+"</a>";
				}else{
					temp="<a name='findPage' pageNum='"+i+"'>"+i+"</a>";
				}
				pageList+=temp;
			}
		}
		return pageList;
	}
// 分页
function generatePaginationApp(cp,tp,keyId,fireBrigadeId){
    var currentPage = Number(cp);
    var totalPages = Number(tp);
    var jump_prev=currentPage>1?(currentPage-1):currentPage;
    var jump_next=(currentPage<totalPages)?(currentPage+1):totalPages;
    var pageList ="<a name='findPage' pageNum='"+jump_prev+"' class='front'><span></span></a>";
    var page_start = Number(currentPage)/5;
    if(page_start>1){
        pageList = getPageHtmlApp(page_start*5-2,page_start*5+3,totalPages,pageList,currentPage);
    }else{
        pageList = getPageHtmlApp(1,6,totalPages,pageList,currentPage);
    }
    pageList +="<a name='findPage' pageNum='"+jump_next+"' class='after'><span></span></a>";
    $("#"+keyId).html(pageList);
    //消防车
    if(keyId=="carPage") {
        //点击常规的页码
        $("#carPage").find("[name=findPage]").on("click", function () {
            carPage = $(this).attr("pageNum");  //获取链接里的页码
            getListData("消防车",fireBrigadeId);
        });
    }
    // 专属器材点击
    if(keyId=="special") {
        //点击专属器材的页码
        $("#special").find("[name=findPage]").on("click", function () {
            specialPage = $(this).attr("pageNum");  //获取链接里的页码
            getListData("专勤器材",fireBrigadeId);
        });
    }
    // 常规器材点击
    if(keyId=="normal") {
        //点击常规的页码
        $("#normal").find("[name=findPage]").on("click", function () {
            normalPage = $(this).attr("pageNum");  //获取链接里的页码
            getListData("常规器材",fireBrigadeId);
        });
    }
    // 常规器材点击
    if(keyId=="wuziPage") {
        //点击常规的页码
        $("#wuziPage").find("[name=findPage]").on("click", function () {
        	wuziPage = $(this).attr("pageNum");  //获取链接里的页码
            getEmergencyMaterialList(fireBrigadeId);
        });
    }
    
}

/*拼接专勤器材html*/
function getSpecialHtml(obj,index){
    var html = "";
    if(obj != undefined){
        html="<tr><td>"+(index+1)+"</td>";
        html+="<td>"+(obj.name !=undefined ? obj.name :' ')+"</td>";
        html+="<td>"+(obj.code !=undefined ? obj.code :' ')+"</td>";
        html+="<td>"+(obj.specificationsModel !=undefined ? obj.specificationsModel :' ')+"</td>";
        html+="<td>"+(obj.type !=undefined ? obj.type :' ')+"</td>";
        html+="<td>"+(obj.storageQuantity !=undefined ? obj.storageQuantity :' ')+"</td>";
       // html+="<td>"+(obj.numberOfCar !=undefined ? obj.numberOfCar :' ')+"</td>";
        html+="<td>"+(obj.position !=undefined ? obj.position :' ')+"</td>";
    }

    return html;
}

// 拼接常规器材html
function  getNormalHtml(obj,index) {
    var html = "";
    // 如果有则显示器材，没有显示提示信息
    if(obj != undefined){
        html = "<tr><td>"+(index+1)+"</td>";
        html+="<td>"+(obj.name !=undefined ? obj.name :' ')+"</td>";
        html+="<td>"+(obj.code !=undefined ? obj.code :' ')+"</td>";
        html+="<td>"+(obj.selfCode !=undefined ? obj.selfCode :' ') +"</td>";
        html+="<td>"+(obj.specificationsModel !=undefined ? obj.specificationsModel :' ')+"</td>";
        html+="<td>"+(obj.type !=undefined ? obj.type :' ')+"</td>";
        html+="<td class='storage_"+obj.id+"'>"+(obj.storageQuantity !=undefined ? obj.storageQuantity :' ')+"</td>";
      //  html+="<td>"+(obj.numberOfCar !=undefined ? obj.numberOfCar :' ')+"</td>";
        html+="<td><a href='javascript:void(0);' id='"+obj.id+"' onclick='dispatch(event)'><input type='text' class='callInput' value='1'/>调用</a></td></tr>";
    }

    return html;
}

function getCarHtml(obj,index) {
    var html = "";
    // 如果有则显示器材，没有显示提示信息
    if(obj != undefined){
        html = "<tr><td>"+(index+1)+"</td>";
        html+="<td>"+(obj.name !=undefined ? obj.name :' ')+"</td>";
        html+="<td>"+(obj.plateNumber !=undefined ? obj.plateNumber :' ')+"</td>";
        html+="<td>"+(obj.stateOfDuty !=undefined ? obj.stateOfDuty==1?'执勤' :'备勤':'')+"</td>";
        html+="<td>"+(obj.water !=undefined ? obj.water :' ')+"</td>";
        html+="<td>"+(obj.powderQuantity !=undefined ? obj.powderQuantity :' ') +"</td>";
        html+="<td>"+(obj.capacity !=undefined ? obj.capacity :' ') +"</td>";
    }

    return html;
}

function getWuziHtml(obj,index) {
    var html = "";
    // 如果有则显示器材，没有显示提示信息
    if(obj != undefined){
        html = "<tr><td>"+(index+1)+"</td>";
        html+="<td>"+(obj.name !=undefined ? obj.name :' ')+"</td>";
        html+="<td>"+(obj.storageQuantity !=undefined ? obj.storageQuantity :' ')+"</td>";
        html+="<td>"+(obj.storageUnit !=undefined ? obj.storageUnit :' ')+"</td>";
        html+="<td>"+(obj.capacity !=undefined ? obj.capacity :' ') +"</td>";
        html+="<td>"+(obj.purpose !=undefined ? obj.purpose :' ')+"</td>";
        html+="<td>"+(obj.remark !=undefined ? obj.remark :' ') +"</td>";
    }

    return html;
}

/*拼接随车器材html*/
function getVehicleEquipmentHtml(obj,index){
    var html = "";
    if(obj != undefined){
        html="<tr><td>"+(index+1)+"</td>";
        html+="<td>"+(obj.name !=undefined ? obj.name :' ')+"</td>";
        html+="<td>"+(obj.code !=undefined ? obj.code :' ')+"</td>";
        html+="<td>"+(obj.selfCode !=undefined ? obj.selfCode :' ')+"</td>";
        html+="<td>"+(obj.specificationsModel !=undefined ? obj.specificationsModel :' ')+"</td>";
        html+="<td>"+(obj.count !=undefined ? obj.count :' ')+"</td>";
        html+="<td>"+(obj.unit !=undefined ? obj.unit :' ')+"</td>";
        html+="<td>"+(obj.useDesc !=undefined ? obj.useDesc :' ')+"</td>";
    }
    return html;
}

// 展示消防队伍详情
function FireBrigadeDetail(fireBrigadeId){
    // className 即消防队id
	getListData("消防车",fireBrigadeId);
    getListData("常规器材",fireBrigadeId);
    getListData("专勤器材",fireBrigadeId);
    $('#materialSeeDiv').show();
}

//展示物资详情
function getEmergencyMaterial(fireBrigadeId){
	getEmergencyMaterialListALL(fireBrigadeId);
    $('#getEmergencyMaterialDiv').show();
}

/*
 * 应急物资部分也查看 
 * */
function getEmergencyMaterialListALL(reservePointId){
	var normalParams={"reservePointId":reservePointId};
	var normalHtml = "";
	$("#wuziList").html("");
	$.ajax({
	    type : "post",
	    url : baseUrl+"/webApi/qryWuziListAll.jhtml",
	    data: JSON.stringify(normalParams),
	    dataType:"json",
	    ladeview:false,
	    headers: {"Content-Type": "application/json;charset=utf-8"},
	    success : function(data) {
	    	if(data.data&&data.data.length>0){
		        // 如果没有信息则做为空判断，显示提示信息
		        if(data.data.length == 0){
		            normalHtml+=getWuziHtml(undefined,0);
		        }
		        for(var j = 0,len=data.data.length; j < len; j++) {
		            normalHtml+=getWuziHtml(data.data[j],j);
		        }
	    	}else{
	    		html+="<li class='NoTime'>暂无数据</li>";
	    	}
	        
	        $("#wuziList").append(normalHtml);
	    },error: function(request) {
	        layer.msg("网络错误", {time: 1000});
	    }
	})
}

/*
 *应急物资分页方法，暂时不需要，改成滚动了 
 * /
/*根据参数搜索js中的相对应的数据 参数(类型,关键字,所属单位)*/
function getEmergencyMaterialList(reservePointId){
	// 加载物资
	var normalParams={"pageNum":wuziPage,"pageSize":5,"reservePointId":reservePointId};
	var normalHtml = "";
	$("#wuziList").html("");
	$.ajax({
	    type : "post",
	    url : baseUrl+"/webApi/qryWuziListByPage.jhtml",
	    data: JSON.stringify(normalParams),
	    dataType:"json",
	    ladeview:false,
	    headers: {"Content-Type": "application/json;charset=utf-8"},
	    success : function(data) {
	    	if(data.data&&data.data.length>0){
	    		var currentPage = Number(data.current);
		        var totalPages = Number(data.pages);
		        var html = '';
		        if(currentPage>=1 && totalPages>1){
	                generatePaginationApp(currentPage,totalPages,"wuziPage",reservePointId);
	            }else{
	            	// 清除底部分页框
	            	$("#wuziPage").html("");
	            }
		        // 如果没有信息则做为空判断，显示提示信息
		        if(data.data.length == 0){
		            normalHtml+=getWuziHtml(undefined,0);
		        }
		        for(var j = 0,len=data.data.length; j < len; j++) {
		            normalHtml+=getWuziHtml(data.data[j],j);
		        }
	    	}else{
	    		html+="<li class='NoTime'>暂无数据</li>";
	    	}
	        
	        $("#wuziList").append(normalHtml);
	    },error: function(request) {
	        layer.msg("网络错误", {time: 1000});
	    }
	})
  }
//展示随车器材详情
function vehicleEquipment(carId){
	$('#vehicleEquipmentDiv').show();
	//得到装备
	$('#vehicleEquipmentContent').html('');
	var url =  baseUrl+"/webApi/qryFireEngineEquipmentByfId.jhtml";
	var params= carId;
    $.ajax({
        type : "post",
        url : url,
        data: JSON.stringify(params),
        dataType:"json",
        headers: {"Content-Type": "application/json;charset=utf-8"},
        success : function(data) {
            // 如果没有信息则做为空判断，显示提示信息
            if(data.httpCode==200){
            	var normalHtml = '';
            	if(data.fireEngineVo){
            		$("#carNameId").text(data.fireEngineVo.name);
            		$("#carOfBrigadeId").text(data.fireEngineVo.fireBrigadeName);
            		$("#plateNumberId").text(data.fireEngineVo.plateNumber);
            		$("#waterId").text(data.fireEngineVo.water);
            		$("#carImgId").attr("src",data.fireEngineVo.path);
            		$("#codeId").text(data.fireEngineVo.code);
            		$("#foamTypeId").text(data.fireEngineVo.capacity);
            		$("#carTypeId").text(data.fireEngineVo.dicName);
            		$("#powderQuantityId").text(data.fireEngineVo.powderQuantity);
            		$("#chassisModelId").text(data.fireEngineVo.chassisModel);
            		$("#manufacturerId").text(data.fireEngineVo.manufacturer);
            		$("#vehicularArtilleryId").text(data.fireEngineVo.vehicularArtillery);
            		$("#investmentDateId").text(data.fireEngineVo.investmentDate?new Date(data.fireEngineVo.investmentDate).Format("yyyy-MM-dd"):"");
            		$("#gunRangeId").text(data.fireEngineVo.gunRange);
            		$("#liftingHeightId").text(data.fireEngineVo.liftingHeight);
            		$("#powderType").text("泡沫(T) "+data.fireEngineVo.foamTypeNameO);
            		if(data.fireEngineVo.stateOfDuty==1){
            			$("#stateOfDuty").text("执勤");
            		}else{
            			$("#stateOfDuty").text("备勤");
            		}
            	}
            	if(data.data&&data.data.length>0){
            		 for(var j = 0,len=data.data.length; j < len; j++) {
                         normalHtml+=getVehicleEquipmentHtml(data.data[j],j);
                     }
            	}else{
            		normalHtml+="<tr><td colspan='8' class='tableNoData'>暂无数据</td></tr>";
            	}
                $("#vehicleEquipmentContent").append(normalHtml);
        	}else{
        		layer.msg(systemAbnormalityMsg, {time: 2000});
        	}
        },error: function(request) {
        	layer.msg(networkErrorMsg, {time: 2000});
        }
    })
}