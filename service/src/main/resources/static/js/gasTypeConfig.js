function showCreateTable(){
	$("#inputTable").show();
	$("#createSaveBu").show();
	$("#closeTableBu").show();
}

function showUpdateTable(id){
	$.ajax({
		type : "post",
		url : baseUrl+"/webApi/gasType/qryById.jhtml",
		data: JSON.stringify(id),
		dataType:"json",
		ladeview:false,
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				$("#inputTable").show();
				$("#updateSaveBu").show();
				$("#closeTableBu").show();
				var entity = data.data;
				$("#id").val(entity.id);
				$("#name").val(entity.name);
				$("#unit").val(entity.unit);
				$("#low").val(entity.low);
				$("#up").val(entity.up);
				$("#pLow").val(entity.pLow);
				$("#pUp").val(entity.pUp);
				$("#del").val(entity.del_);
			}
		}
	})
}

function delEntity(id){
	$.ajax({
		type : "post",
		url : baseUrl+"/webApi/gasType/del.jhtml",
		data: JSON.stringify(id),
		dataType:"json",
		ladeview:false,
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				window.location.href=baseUrl+"/webApi/gasType/index.jhtml";
			}
		}
	})
}

function createSaveFn(){
	var name = $("#name").val();
	var unit = $("#unit").val();
	var low = $("#low").val();
	var up = $("#up").val();
	var pLow = $("#pLow").val();
	var pUp = $("#pUp").val();
	var del = $("#del").val();
	var params = {};
	params.name = name;
	params.unit = unit;
	params.low = low;
	params.up = up;
	params.pLow = pLow;
	params.pUp = pUp;
	params.del = del;
	$.ajax({
		type : "post",
		url : baseUrl+"/webApi/gasType/create.jhtml",
		data: JSON.stringify(params),
		dataType:"json",
		ladeview:false,
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				closeTableFn();
				window.location.href=baseUrl+"/webApi/gasType/index.jhtml";
			}
		}
	})
}

function updateSaveFn(){
	var id = $("#id").val();
	var name = $("#name").val();
	var unit = $("#unit").val();
	var low = $("#low").val();
	var up = $("#up").val();
	var pLow = $("#pLow").val();
	var pUp = $("#pUp").val();
	var del = $("#del").val();
	var params = {};
	params.id = id;
	params.name = name;
	params.unit = unit;
	params.low = low;
	params.up = up;
	params.pLow = pLow;
	params.pUp = pUp;
	params.del = del;
	$.ajax({
		type : "post",
		url : baseUrl+"/webApi/gasType/edit.jhtml",
		data: JSON.stringify(params),
		dataType:"json",
		ladeview:false,
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				closeTableFn();
				window.location.href=baseUrl+"/webApi/gasType/index.jhtml";
			}
		}
	})
}

function closeTableFn(){
	$("#inputTable").hide();
	$("#updateSaveBu").hide();
	$("#createSaveBu").hide();
	$("#closeTableBu").hide();
	$("#name").val("");
	$("#unit").val("");
	$("#low").val("");
	$("#up").val("");
	$("#pLow").val("");
	$("#pUp").val("");
	$("#del").val("");
}

