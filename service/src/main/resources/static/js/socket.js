/**
 * websocket
 */

function initSocket(url,callback){
	var socket = new WebSocket(url);
	
	socket.onopen = function() {  
         
    };  
    //获得消息事件  
    socket.onmessage = function(msg) {  
    	callback(msg.data);
    };  
    //关闭事件  
    socket.onclose = function() {  
//        console.log("Socket已关闭");  
    };  
    
    //发生了错误事件  
    socket.onerror = function() {  
//        console.log("socket连接失败！");  
    }  
	return socket;
}

function closeSocket(socket){
	socket.close();
}
