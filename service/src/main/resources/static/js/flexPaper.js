//加载文档
var fp;
function loadDoc(file){
	if(fp){
		$FlexPaper().loadSwf(file);
	}else{
		fp = new FlexPaperViewer(	
				//baseUrl+'/js/flexPaper/FlexPaperViewer',
				baseFileUrl+'/upload/flexPaper/FlexPaperViewer',
				 'viewerPlaceHolder', { config : {
					SwfFile : escape(file),
					Scale : 0.9, 
					ZoomTransition : 'easeOut',
					ZoomTime : 0.5,
					ZoomInterval : 0.2,
					FitPageOnLoad : true,
					FitWidthOnLoad : false,
					FullScreenAsMaxWindow : false,
					ProgressiveLoading : false,
					MinZoomSize : 0.2,
					MaxZoomSize : 5,
					SearchMatchAll : false,
					InitViewMode : 'Portrait',
					PrintPaperAsBitmap : false,
					ViewModeToolsVisible : true,
					ZoomToolsVisible : true,
					NavToolsVisible : true,
					CursorToolsVisible : true,
					SearchToolsVisible : true,
				 localeChain: 'zh-CN'
		}});
	}
}

//跳转页面
function gotoPage(page){
	$FlexPaper().gotoPage(page);
}

