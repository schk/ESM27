// 危化品库显示详情
function getDetail(id){
	$('#dangerDetail').show();
	
//	初始化详情字段
	$('#whpk-title').text("");
//	化学品
	$('#whpk-cname2').text("");
	$('#whpk-ename2').text("");
	$('#whpk-fzs').text("");
	$('#whpk-fzl').text("");
//	成分、组成信息
	$('#whpk-yhcf').text("");
	$('#whpk-hl').text("");
//	危险性概述
	$('#whpk-jkwh').text("");
	$('#whpk-rbwx').text("");
	$('#whpk-hkwh').text("");
//	急救		
	$('#whpk-pfjc').text("");
	$('#whpk-yjjc').text("");
	$('#whpk-xr').text("");
	$('#whpk-sr').text("");
//	消防措施
	$('#whpk-wxx').text("");
	$('#whpk-yhrscw').text("");
	$('#whpk-mhff').text("");
//	泄露应急处理
	$('#whpk-yjcl').text("");
//	操作处置与存储
	$('#whpk-czcl').text("");
	$('#whpk-zysx').text("");
//	接触控制
	$('#whpk-cmac').text("");
	$('#whpk-qslmac').text("");
	$('#whpk-jcff').text("");
	$('#whpk-gckz').text("");
	$('#whpk-hxxt').text("");
	$('#whpk-yjfh').text("");
	$('#whpk-stfh').text("");
	$('#whpk-sfh').text("");
	$('#whpk-qtfh').text("");
//	理化特性
	$('#whpk-zycf').text("");
	$('#whpk-wgyx').text("");
	$('#whpk-ph').text("");
	$('#whpk-rd').text("");
	$('#whpk-fd').text("");
	$('#whpk-xdmd').text("");
	$('#whpk-xdzq').text("");
	$('#whpk-bhzq').text("");
	$('#whpk-rsr').text("");
	$('#whpk-ljwd').text("");
	$('#whpk-ljyl').text("");
	$('#whpk-xc').text("");
	$('#whpk-bzsx').text("");
	$('#whpk-bzxx').text("");
	$('#whpk-rjx').text("");
	$('#whpk-zyyt').text("");
//	稳定性和反应活性
	$('#whpk-wendingxing').text("");
	$('#whpk-jinpeiwu').text("");
	$('#whpk-bimianjiechu').text("");
	$('#whpk-jixingdusu').text("");
//	废弃处置
	$('#whpk-feiqichuli').text("");
	$('#whpk-feiqishixiang').text("");
//	运输信息
	$('#whpk-baozhuangleibie').text("");
	$('#whpk-yunshuzhuyi').text("");
	$('#whpk-baozhuangfangfa').text("");
//	法规信息
	$('#whpk-faguixinxi').text("");
//	其它信息
	$('#whpk-qita').text("");
	
	var params={"dangersId":id};
	$.ajax({
		type : "post",
		url : baseUrl+"/webApi/others/qryDangersOne.jhtml",
		ladeview:false,
		data: JSON.stringify(params),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {
			if(data.httpCode === 200){
				$('#whpk-title').text(data.data.cName);
//				化学品
				$('#whpk-cname2').text(data.data.cName2);
				$('#whpk-ename2').text(data.data.eName2);
				$('#whpk-fzs').text(data.data.molecularFormula);
				$('#whpk-fzl').text(data.data.molecularWeight);
//				成分、组成信息
				$('#whpk-yhcf').text(data.data.harmfulIngredients);
				$('#whpk-hl').text(data.data.content);
//				危险性概述
				$('#whpk-jkwh').text(data.data.healthHazards);
				$('#whpk-rbwx').text(data.data.environmentalHazards);
				$('#whpk-hkwh').text(data.data.fireExplosionDanger);
//				急救		
				$('#whpk-pfjc').text(data.data.eyeContact);
				$('#whpk-yjjc').text(data.data.skinContact);
				$('#whpk-xr').text(data.data.inhalation);
				$('#whpk-sr').text(data.data.feedInto);
//				消防措施
				$('#whpk-wxx').text(data.data.hazardCharacteristics);
				$('#whpk-yhrscw').text(data.data.harmfulCombustionProducts);
				$('#whpk-mhff').text(data.data.fireExtinguishingMethod);
//				泄露应急处理
				$('#whpk-yjcl').text(data.data.emergencyManagement);
//				操作处置与存储
				$('#whpk-czcl').text(data.data.attentionOperation);
				$('#whpk-zysx').text(data.data.precautionsStorage);
//				接触控制
				$('#whpk-cmac').text(data.data.chinaMac);
				$('#whpk-qslmac').text(data.data.sovietUnionMac);
				$('#whpk-jcff').text(data.data.monitoringMethod);
				$('#whpk-gckz').text(data.data.engineeringControl);
				$('#whpk-hxxt').text(data.data.respiratorySystemProtection);
				$('#whpk-yjfh').text(data.data.eyeProtection);
				$('#whpk-stfh').text(data.data.bodyProtection);
				$('#whpk-sfh').text(data.data.handProtection);
				$('#whpk-qtfh').text(data.data.otherProtection);
//				理化特性
				$('#whpk-zycf').text(data.data.mainComponents);
				$('#whpk-wgyx').text(data.data.appearanceCharacter);
				$('#whpk-ph').text(data.data.ph);
				$('#whpk-rd').text(data.data.meltingPoint);
				$('#whpk-fd').text(data.data.expensePoint);
				$('#whpk-xdmd').text(data.data.relativeDensity);
				$('#whpk-xdzq').text(data.data.relativeSteam);
				$('#whpk-bhzq').text(data.data.saturatedVaporPressure);
				$('#whpk-rsr').text(data.data.burningHeat);
				$('#whpk-ljwd').text(data.data.criticalTemperature);
				$('#whpk-ljyl').text(data.data.criticalPressure);
				$('#whpk-xc').text(data.data.waterDistributionCoefficient);
				$('#whpk-bzsx').text(data.data.upperLimitExplosion);
				$('#whpk-bzxx').text(data.data.lowerLimitExplosion);
				$('#whpk-rjx').text(data.data.solubility);
				$('#whpk-zyyt').text(data.data.mainUses);
//				稳定性和反应活性
				$('#whpk-wendingxing').text(data.data.stability);
				$('#whpk-jinpeiwu').text(data.data.prohibition);
				$('#whpk-bimianjiechu').text(data.data.avoidContact);
				$('#whpk-jixingdusu').text(data.data.acuteToxicity);
//				废弃处置
				$('#whpk-feiqichuli').text(data.data.discardedDisposalMethod);
				$('#whpk-feiqishixiang').text(data.data.discardedNotices);
//				运输信息
				$('#whpk-baozhuangleibie').text(data.data.packingCategory);
				$('#whpk-yunshuzhuyi').text(data.data.attentionTransportation);
				$('#whpk-baozhuangfangfa').text(data.data.packingMethod);
//				法规信息
				$('#whpk-faguixinxi').text(data.data.regulatoryInformation);
//				其它信息
				$('#whpk-qita').text(data.data.otherInformation);
			}else{
				layer.msg(data.msg, {time: 1000});
			}
		},error: function(request) {
			layer.msg("网络错误", {time: 1000});
        }
	})
}


//展示消防队详情
function fireBrigadeNameAccident(data){
	$('#emergencyTeamDivList').hide();
	$('#fireBrigadeContId').show();
	
	$('#fireBrigadeExtinguisher').html("");
	$('#fireBrigadePlanList').html("");
	var html= '';
	var picHtml='';
//	初始化为空值
	$('#fireBrigade-title').text(data.name);
	$('#XFD_FZR-name').text(data.charge+"("+data.phone+")");
	$('#XFD-addr').text(data.position);
	$('#XFDGM-scal').text(data.scale);
	$('#XFDGM-carCount').text(data.carCount);
	
	if(data.extinguisherList&&data.extinguisherList.length>0){
		for(var i=0;i<data.extinguisherList.length;i++){
			var curr =data.extinguisherList[i];
			html+="<p><span class='color3'>"+curr.name+"：</span><span>"+curr.quantity+"KG</span></p>";
		}
	}
	$("#fireBrigadeExtinguisher").append(html);
	
	picHtml+="<div class='typeHeadPlan'><span>预案</span></div>";		
	if(data.planList&&data.planList.length>0){
		for(var i=0;i<data.planList.length;i++){
			var curr =data.planList[i];
			picHtml+="<div class='ca cf ptb5'>";
			picHtml+="<div class='fl firedangerMsgHd'><i></i><span class='color3'>"+curr.name+"</span></div>";
			picHtml+="<div class='fr seeDiv'><span onClick='showReservePlandoc(\""+curr.id+"\")'>查看</span>" ;
			picHtml+="<span onClick='downLoadFile(\""+curr.path+"\",\""+curr.name+"\")'>下载</span>";
			picHtml+="</p>"; 
			picHtml+="</div>";
			picHtml+="</div>";
		}
	}
	picHtml+="</div>"; 
	$("#fireBrigadePlanList").append(picHtml);
}


//消防队返回时间
function fireBrigadeReturn(){
	$('#fireBrigadeContId').css("display","none");
	$('#emergencyTeamDivList').show();
}

//展示重点部位详情
function keyPartsDetail(id){
	$("#locationContent").css("width","560px");
	$('#locationContentZhai').hide();
	$('#locationContentKuan').hide();
	$('#keyPartDetailContId').show();
	keyPartsDetailsInfo(id);
}

//用于重点单位详情页 
function keyPartsDetailsInfo(id){
	$('#keyPartPicSrc').html("");
	$('#keyPartDangerList').html("");
	$('#keyPartYJYAList').html("");
	$('#processFlowPicSrc').html("");
	var html= '';
	var picHtml='';
	var processFlowHtml ='';
	var planHtml= '';
//	初始化为空值
	$('#keyPart-title').text("");
	$('#keyPart-keyUnit').text("");
	$('#keyPart-desc').text("");
	$.ajax({
		type : "post",
		url : baseUrl+"/webApi/qryKeyPartsById.jhtml",
		data: JSON.stringify(id),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {
			if(data.httpCode === 200){
				if(data.data){
					$('#keyPart-title').text(data.data.name);
					$('#keyPart-keyUnit').text(data.data.keyUnitName);
					$('#keyPart-desc').text(data.data.remark);
					$('#keyPartPicSrc').attr("src",baseFileUrl+data.data.path);
					$('#processFlowPicSrc').attr("src",baseFileUrl+data.data.processFlow);
					if(data.data.path){
						picHtml +='<li onClick="showBigImg(\''+baseFileUrl+data.data.path+'\')"><img src="'+baseFileUrl+data.data.path+'" alt="" /></li>';
					}
					$('#keyPartPicSrc').append(picHtml);
					if(data.data.processFlow){
						processFlowHtml +='<li onClick="showBigImg(\''+baseFileUrl+data.data.processFlow+'\')"><img src="'+baseFileUrl+data.data.processFlow+'" alt="" /></li>';
					}
					$('#processFlowPicSrc').append(processFlowHtml);
					
					if(data.data.dList&&data.data.dList.length>0){
						for(j = 0;j < data.data.dList.length; j++) {
							html += companyDangerHtm(data.data.dList[j],"keyParts");
						}
					}
					$('#keyPartDangerList').append(html);
					
					if(data.data.planList&&data.data.planList.length>0){
						for(k = 0;k < data.data.planList.length; k++) {
							planHtml += companyPlanHtml(data.data.planList[k]);
						}
					}
					$('#keyPartYJYAList').append(planHtml);
					
				}
				
			}else{
				layer.msg(data.msg, {time: 1000});
			}
		},error: function(request) {
			layer.msg("网络错误", {time: 1000});
        }
	})
}



//展示重点单位详情
function unitDetail(id){
	$("#leftMenuContentZDDW").css("width","560px");
	//subMeusDivKuan();
	//setMeunPosition();
	$('#zddwListDiv').hide();
	$('#zddwListKuanDiv').hide();
	$('#factoryContId').show();
	keyUnitsDetails(id);
	
}

//用于重点单位详情页 
function keyUnitsDetails(id){
	$('#companyDangerList').html("");
	$('#factoryPicList').html("");
	$('#companyYJYAList').html("");
	$('#accidentSGXXList').html("");
	var html= '';
	var picHtml='';
	var planHtml= '';
	var accidentHtml= '';
//	初始化为空值
	$('#point-title').text("");
	$('#point-name').text("");
	$('#point-addr').text("");
	$('#point-waterSupply').text("");
	$('#point-people').text("");
	$('#point-phone').text("");
	$('#point-other').text("");
	$.ajax({
		type : "post",
		url : baseUrl+"/webApi/qryKeyUnitById.jhtml",
		data: JSON.stringify(id),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {
			if(data.httpCode === 200){
				if(data.data){
					$('#point-title').text(data.data.name);
					$('#point-name').text(data.data.fireBrigadeName);
					$('#point-waterSupply').text(data.data.waterSupply);
					$('#point-addr').text(data.data.addr);
					$('#point-people').text(data.data.admin);
					$('#point-phone').text(data.data.tel);
					$('#point-other').text(data.data.desc);
					var picListLength = data.data.fileList.length;
					for(i=0 ; i<picListLength;i++){
						picHtml += factoryPicHtml(data.data.fileList[i]);
					}
					$('#factoryPicList').append(picHtml);
					var len=data.data.dangersList.length;
					for(j = 0;j < len; j++) {
						html += companyDangerHtm(data.data.dangersList[j],"keyUnit");
					}
					$('#companyDangerList').append(html);
					
					var len=data.data.planList.length;
					for(k = 0;k < len; k++) {
						planHtml += companyPlanHtml(data.data.planList[k]);
					}
					$('#companyYJYAList').append(planHtml);
					var len=data.data.accidentFileList.length;
					for(m = 0;m < len; m++) {
						accidentHtml += accidentListHtml(data.data.accidentFileList[m]);
					}
					$('#accidentSGXXList').append(accidentHtml);
					
				}
				
				setMeunPosition();
			}else{
				layer.msg(data.msg, {time: 1000});
			}
		},error: function(request) {
			layer.msg("网络错误", {time: 1000});
        }
	})
}

// 企业预案拼接html
function companyPlanHtml (obj){
	var html="";
	html+="<li class='ca cf'><div class='dangerMsgHd fl'><i></i>"+obj.name+"</div>";
	if(obj.path){
		html+="<div class='fr seeDiv' style='padding: 10px;'>";
		html+="<span onClick='showReservePlandoc(\""+obj.id+"\")'>查看</span>" ;
		html+="<span onClick='downLoadFile(\""+obj.path+"\",\""+obj.name+"\")'>下载</span>";
		html+="</div>"
	}
	html+="</li>"
	return html;
}

//企业预案拼接html
function accidentListHtml (obj){
	var html="";
	html+="<li class='ca cf'><div class='dangerMsgHd fl'><i></i>"+obj.name+"</div>";
	if(obj.url){
		html+="<div class='fr seeDiv' style='padding: 10px;'>";
		html+="<span onClick='downLoadFile(\""+obj.url+"\",\""+obj.name+"\")'>下载</span>";
		html+="</div>"
	}
	html+="</li>"
	return html;
}


// 厂区平面图拼接html
//factoryPicList
function factoryPicHtml(obj){
    var html="<li onClick='showBigImg(\""+baseFileUrl+obj.url+"\")'><img src='"+ baseFileUrl + obj.url +"' alt='' /></li>"; 			 
	return html;
}

//企业危化品信息拼接html
function companyDangerHtm(obj,type){
 var html="<li><div class='dangerMsgHd'><i></i>"+obj.cName+"</div>"; 			 
		html+="<div class='unitMsg'><p><span class='color3'>英文名称：</span><span>"+obj.eName +"</span></p>";
		html+="<p><span class='color3'>CAS. No：</span><span>"+obj.casNum+"</span></p>"; 
		html+="<div class='unitMsg unitMsgPadding' id='unitMsg"+obj.id+"' style='display:none;'>";
		
		html+="</div>";
		html+="<div class='openMsg' class='icon' onclick='showDangerList(event,"+JSON.stringify(obj.id)+",\""+type+"\")'></div>"; 
	return html;
}

function showDangerList(event,id,type){
	var showDiv = '#unitMsg'+id ;
	$(showDiv).html("");
	if(event.target.className === 'openMsg'){
		event.target.className = 'upMsg';
		var url=baseUrl+"/webApi/qryKeyUnitDangersById.jhtml";
		if(type=="keyParts"){
			url=baseUrl+"/webApi/qryDangersById.jhtml";
		}
		$.ajax({
			type : "post",
			url : url,
			data: JSON.stringify(id),
			dataType:"json",
			ladeview:false,
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {	
				if(data.httpCode === 200){
//					危害程度
				var html="<div class='dangerMsgHd' style='padding:0;margin-bottom:8px;'><i></i>危害程度</div>"
					html+="<p><span class='color3'>健康危害：</span><span>"+data.data.healthHazards+"</span></p>";
					html+="<p><span class='color3'>环境危害：</span><span>"+data.data.environmentalHazards+"</span></p>";
					html+="<p><span class='color3'>燃爆危害：</span><span>"+data.data.fireExplosionDanger+"</span></p>";
					html+="<p><span class='color3'>皮肤接触：</span><span>"+data.data.skinContact+"</span></p>";
					html+="<p><span class='color3'>眼睛接触：</span><span>"+data.data.eyeContact+"</span></p>";
					html+="<p><span class='color3'>吸入：</span><span>"+data.data.inhalation+"</span></p>";
					html+="<p><span class='color3'>食入：</span><span>"+data.data.feedInto+"</span></p>";
//					消防措施
					html+="<div class='dangerMsgHd' style='padding:0;margin-bottom:8px;'><i></i>消防措施</div>"
					html+="<p><span class='color3'>危险特性：</span><span>"+data.data.hazardCharacteristics+"</span></p>";
					html+="<p><span class='color3'>有害燃烧产物：</span><span>"+data.data.harmfulCombustionProducts+"</span></p>";
					html+="<p><span class='color3'>灭火方法：</span><span>"+data.data.fireExtinguishingMethod+"</span></p>";
//					泄漏应急处理
					html+="<div class='dangerMsgHd' style='padding:0;margin-bottom:8px;'><i></i>泄漏应急处理</div>"
					html+="<p><span class='color3'>泄漏应急处理:</span><span id='qywh-xielouchuli'>"+data.data.emergencyManagement+"</span></p>";
//					操作处置与储存
					html+="<div class='dangerMsgHd' style='padding:0;margin-bottom:8px;'><i></i>操作处置与储存</div>"
					html+="<p><span class='color3'>操作注意事项：</span><span id='qywh-caozuo'>"+data.data.attentionOperation+"</span></p>";
					html+="<p><span class='color3'>储存注意事项：</span><span id='qywh-chunchu'>"+data.data.precautionsStorage+"</span></p>";
//					操作处置与储存
					html+="<div class='dangerMsgHd' style='padding:0;margin-bottom:8px;'><i></i>操作处置与储存</div>"
					html+="<p><span class='color3'>呼吸系统防护：</span><span id='qywh-huxifanghu'>"+data.data.respiratorySystemProtection+"</span></p>";
					html+="<p><span class='color3'>眼睛防护：</span><span id='qywh-yanjingfanghu'>"+data.data.eyeProtection+"</span></p>";
					html+="<p><span class='color3'>身体防护：</span><span id='qywh-shentifanghu'>"+data.data.bodyProtection+"</span></p>";
					html+="<p><span class='color3'>手防护：</span><span id='qywh-shoufanghu'>"+data.data.handProtection+"</span></p>";
					$(showDiv).append(html).show();
				}else{
					layer.msg(data.msg, {time: 1000});
				}
			},error: function(request) {
				layer.msg("网络错误", {time: 1000});
        }
		})
	}else{
		event.target.className = 'openMsg';
		$(showDiv).hide();
	}
}
// 重点单位返
function pointReturn(){
	$('#factoryContId').css("display","none");
	if(keyUnit==1){
		$('#zddwListDiv').show(); //展示窄
		$("#leftMenuContentZDDW").css("width","260px");
		//subMeusDivZhai();
		setMeunPosition();
	}else if(keyUnit==2){
		$('#zddwListKuanDiv').show();//展示宽
		$("#leftMenuContentZDDW").css("width","560px");
		///subMeusDivKuan();
		setMeunPosition();
	}
}

//重点部位返回
function keyPartsReturn(){
	$('#keyPartDetailContId').css("display","none");
	if(keyPartsType==1){
		$('#locationContentZhai').show(); //展示窄
		$("#locationContent").css("width","260px");
		//subMeusDivZhai();
		setMeunPosition();
	}else if(keyPartsType==2){
		$('#locationContentKuan').show();//展示宽
		$("#locationContent").css("width","560px");
		//subMeusDivKuan();
		setMeunPosition();
	}
}

// 危化品库返回
function whpkReturn(){
	$("#dangerDetail").hide();
}
