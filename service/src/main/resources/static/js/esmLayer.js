/**
 * 创建普通图层
 * 
 * @param id
 *            图层ID
 * @description 要素的图片是要素的类型
 */
/*
 * 图层图标变色显示
 * 
 * */

function createCustomLayer(id,image) {
	var layer = new ol.layer.Vector({
		source : new ol.source.Vector({
			features : []
		}),
		style : function(feature) {
			var cate = image?image:feature.get('catecd');
			var name = feature.getProperties().name;
			var style = new ol.style.Style({
				image : new ol.style.Icon({
					crossOrigin:'anonymous',
					src : baseFileUrl+'/mapIcon/mapFixedIcon/' + cate + '.png',
					anchor : [ 0.5, 1 ]
				}),
				text : new ol.style.Text({
					text : name,
					offsetY : 10,
					font : 'bold 12px sans-serif',
					fill : new ol.style.Fill({
						color : '#84C1FF'
					}),
					stroke : new ol.style.Stroke({
						color : '#ECF5FF',
						width : 3
					})
				})
			})
			return style;
		},
		zIndex:10
	});
	layer.set('id', id);
	return layer;
}

/**
 * 创建无样式图层
 * */
function createNoStyleLayer(id) {
	var layer = new ol.layer.Vector({
		source : new ol.source.Vector({
			features : []
		}),
		zIndex:5
	});
	layer.set('id', id);
	return layer;
}

/**
 * 创建虚线图层
 * */
function createDotLineLayer(id) {
	var layer = new ol.layer.Vector({
		source : new ol.source.Vector({
			features : []
		}),
		style: new ol.style.Style({
			stroke:new ol.style.Stroke({
				color:'green',
				lineDash:[2,5],
				width:2
			})
		}),
		zIndex:5
	});
	layer.set('id', id);
	return layer;
}

function createSpecialLine(id){
	var layer = new ol.layer.Vector({
		source:new ol.source.Vector({
			features : []
		}),
		style:function(feature){
			var scolor = feature.get('params').color;
			//arrow.png
			var imageUrl= "arrow";
			if(scolor=='#cc1b1b'){
				imageUrl = imageUrl+"hong";
			}else if(scolor=='#dbec4a'){
				imageUrl = imageUrl+"huang";
			}else if(scolor=='#154bb0'){
				imageUrl = imageUrl+"lan";
			}else if(scolor=='#28ac1e'){
				imageUrl = imageUrl+"lv";
			}
			var geometry = feature.getGeometry();
			var styles = [
				new ol.style.Style({
					stroke : new ol.style.Stroke({
						color : color,
						lineDash:[2,5],
						width : 16 
					})
				})
			];
			geometry.forEachSegment(function(start, end) {
				var dx = end[0] - start[0];
				var dy = end[1] - start[1];
				var rotation = Math.atan2(dy, dx);
				styles.push(new Style({
					geometry : new Point(end),
					image : new Icon({
						crossOrigin:'anonymous',
						src : baseFileUrl+'/mapIcon/mapPanel/'+imageUrl+".png",
						anchor : [ 0.75, 0.5 ],
						color : scolor,
						rotateWithView : true,
						rotation : -rotation
					})
				}));
			});
		}
	});
	layer.set('id',id);
	return layer;
}

/**
 * 创建事故图层
 */
function createSpreadMarkerLayer(id,image) {
	var layer = new ol.layer.Vector({
		source : new ol.source.Vector({
			features : []
		}),
		style: function(feature){
			var name = feature.get('name');
			return new ol.style.Style({
				image: new ol.style.Icon({
					crossOrigin:'anonymous',
					src: image,
					offset:[0.5,0.5]
				}),
				text : new ol.style.Text({
					text : name,
					offsetY : 16,
					font : 'bold 12px sans-serif',
					fill : new ol.style.Fill({
						color : '#84C1FF'
					}),
					stroke : new ol.style.Stroke({
						color : '#ECF5FF',
						width : 3
					})
				})
			})
		},
		zIndex:10
	});
	layer.set('id', id);
	return layer;
}

/**
 * 创建同步图层
 * 
 * @param id
 *            图层ID
 * @description 要素的图片是要素的类型
 */
function createSynMarkLayer(id) {
	var layer = new ol.layer.Vector({
		source : new ol.source.Vector({
			features : []
		}),
		style : function(feature) {
			var image = feature.get('params').imagePath;
			var geometry = feature.getGeometry();
			var name = feature.get('name')?feature.get('name'):'';
			var isSpecial = feature.get('params').isSpecial?feature.get('params').isSpecial:false;
			var angle = feature.get('params').rotation?feature.get('params').rotation:0;
			var rotation = Math.PI/180*angle;
			var scolor = feature.get('scolor')?feature.get('scolor'):'#4781d9';
			var fcolor = feature.get('fcolor')?feature.get('fcolor'):'rgba(67, 110, 238, 0.4)';
			var geoType = feature.getGeometry().getType();
			//arrow.png
			var imageUrl= "arrow";
			if(scolor=='#cc1b1b'){
				imageUrl = imageUrl+"hong";
			}else if(scolor=='#dbec4a'){
				imageUrl = imageUrl+"huang";
			}else if(scolor=='#154bb0'){
				imageUrl = imageUrl+"lan";
			}else if(scolor=='#28ac1e'){
				imageUrl = imageUrl+"lv";
			}
			var style;
			var anchor = [0.5,0.5];
			if(feature.get('params').clusterType=="accident"){
				anchor = [0.5,1];
			}
			if(geoType=="Point"){
				if(image){
					style = new ol.style.Style({
						image : new ol.style.Icon({
							crossOrigin:'anonymous',
							src : baseFileUrl+image,
							anchor : anchor,
							rotation : rotation
						}),
						text : new ol.style.Text({
							text : name,
							offsetY : 20,
							font : 'bold 12px sans-serif',
							fill : new ol.style.Fill({
								color : '#84C1FF'
							}),
							stroke : new ol.style.Stroke({
								color : '#ECF5FF',
								width : 3
							})
						})
					});
				}else{
					style = [new ol.style.Style({
						circle : new ol.style.Circle({
							fill : new ol.style.Fill({
								color:'rgba(67, 110, 238, 0.4)'
							}),
							radius:5,
							stroke:new ol.style.Stroke({
								width:1,
								color:'#4781d9'
							})
						}),
						text : new ol.style.Text({
							text : name,
							offsetY : 20,
							font : 'bold 12px sans-serif',
							fill : new ol.style.Fill({
								color : '#84C1FF'
							}),
							stroke : new ol.style.Stroke({
								color : '#ECF5FF',
								width : 3
							})
						})
					})];
				}
			}else if(geoType=="LineString"){
				if(!isSpecial){
					style = [new ol.style.Style({
						stroke:new ol.style.Stroke({
							width:2,
							color: scolor
						})
					})];
				}else{
					style = [new ol.style.Style({
						stroke:new ol.style.Stroke({
							width:7,
							lineDash:[5,3],
							lineCap:'butt',
							color: scolor
						})
					})];
					geometry.forEachSegment(function(start, end) {
						var dx = end[0] - start[0];
						var dy = end[1] - start[1];
						var rotation = Math.atan2(dy, dx);
						style.push(new ol.style.Style({
							geometry : new ol.geom.Point(end),
							image : new ol.style.Icon({
								crossOrigin:'anonymous',
								src : baseFileUrl+'/mapIcon/mapPanel/'+imageUrl+".png",
								anchor : [ 0.75, 0.5 ],
								color : scolor,
								rotateWithView : true,
								rotation : -rotation
							})
						}));
					});
				}
				
			}else if(geoType=="Polygon"){
				style = [new ol.style.Style({
					stroke:new ol.style.Stroke({
						width:2,
						color: scolor
					}),
					fill: new ol.style.Fill({
						color:fcolor
					})
				})];
				
				if(name){
					style.push(new ol.style.Style({
						geometry : feature.getGeometry().getInteriorPoint(),
						text : new ol.style.Text({
							text : name,
							font : 'bold 24px sans-serif',
							fill : new ol.style.Fill({
								color : '#84C1FF'
							}),
							stroke : new ol.style.Stroke({
								color : '#ECF5FF',
								width : 3
							})
						})
					}));
				}
			}
			return style;
		},
		zIndex:5
	});
	layer.set('layerName', id);
	layer.set('id', id);
	return layer;
}

/**
 * 创建图片定位坐标拾取图层
 * 
 * @param id
 *            图层ID
 * @description 要素的图片是要素的类型
 */
function createImageExtentLayer(id) {
	var layer = new ol.layer.Vector({
		source : new ol.source.Vector({
			features : []
		}),
		style : function(feature) {
			var style;
			if(feature.getGeometry().getType()=="Polygon"){
				return new ol.style.Style({
					stroke:new ol.style.Stroke({
						width:3,
						color: "#4781d9"
					}),
					fill: new ol.style.Fill({
						color:"rgba(67, 110, 238, 0)"
					})
				})
			}else{
				return new ol.style.Style({
					image : new ol.style.Icon({
						crossOrigin:'anonymous',
						src : baseFileUrl+'/mapIcon/mapPanel/close-circle.png',
						anchor : [ 0.5, 0.5 ]
					})
				})
			}
			return style;
		},
		zIndex:11
	});
	layer.set('id', id);
	return layer;
}

/**
 * 创建道路图层
 * 
 * @param id
 *            图层ID
 * @param color
 *            线路颜色
 */
function creatRoadLayer(id, color) {
	var roadLayer = new ol.layer.Vector({
		source : new ol.source.Vector({
			features : []
		}),
		style : function(feature){
			return new ol.style.Style({
				stroke : new ol.style.Stroke({
					color : '#4682B4',
					width : 6
				})
			})
		},
		zIndex:5
	});
	roadLayer.set('id', id);
	return roadLayer;
}

/**
 * 更改道路图层样式
 * 	显示道路实况信息
 * */
function changeRoadStyle(feature){
	var status = feature.get("traffic_status");
	var color = '#4682B4';
	if(status==1){
		color = '#66CD00';
	}else if(status==2){
		color = '#DAA520';
	}else if(status==3){
		color = '#CD5B45';
	}else if(status==4){
		color = '#A52A2A';
	}
	
	return new ol.style.Style({
		stroke : new ol.style.Stroke({
			color : color,
			width : 6
		})
	})
}

/**
 * 还原道路样式；关闭实况
 * */
function resetRoadStyle(feature){
	return new ol.style.Style({
		stroke : new ol.style.Stroke({
			color : '#4682B4',
			width : 6
		})
	})
}

/**
 * 创建道路起始点，结束点图层
 * */
function createRoadPointLayer(id){
	var pointLayer = new ol.layer.Vector({
		source : new ol.source.Vector({
			features : []
		}),
		style : function(feature){
			var style = [new ol.style.Style({
					image : new ol.style.Icon({
						crossOrigin:'anonymous',
						src : baseUrl+'/images/route_'+feature.getId()+'.png',
						anchor : [ 0.5, 1 ]
					})
				})];
			return style;
		},
		zIndex:10
	});
	pointLayer.set('id', id);
	return pointLayer;
}

/**
 * 创建聚合图层
 * @param id 图层ID
 * @param type类型，用来生成聚合图片
 * 重点单位等左侧菜单的调用
 * */
function createClusterLayer(id, type) {
	var source = new ol.source.Vector({
		features : []
	});
	var clusterSource = new ol.source.Cluster({
		source : source,
		distance : 30
	});
	var layer = new ol.layer.Vector({
		source : clusterSource,
		style : function(feature) {
			var size = feature.get('features').length;
			var style;
			var typeF = feature.get('features')[0].getProperties().catecd;
			if (size > 4) {
				var icon = '/mapIcon/iconCluster/' + type + '_c.png';
				if(typeF.indexOf('shzy')!=-1){
					icon = feature.get('features')[0].getProperties().jhPath;
				}
				style = [ new ol.style.Style({
					image : new ol.style.Icon({
						crossOrigin:'anonymous',
						src : baseFileUrl+icon,
						anchor : [ 0.5, 0.5 ]
					}),
					text : new ol.style.Text({
						font : '16px -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
						text : size.toString(),
						offsetX : 8,
						offsetY : -3,
						fill : new ol.style.Fill({
							color : '#FFFFFF'
						})
					})
				})];
			} else {
				var name = feature.get('features')[0].get("name");
				var numn = name.length;
				var offsetYH = 25;
				if(numn>7){
					name = name.substring(0,numn/2)+"\n"+name.substring(numn/2,numn);
					offsetYH = 35;
				}
				var icon = '/mapIcon/mapFixedIcon/' + feature.get('features')[0].getProperties().catecd + '.png';
				
				var colorF = '#FFFFFF';
				if(typeF=='wzcbd'){
					colorF = '#4a4122';
				}else if(typeF=='zddw'){
					colorF = '#52bdbf';
				}else if(typeF=='xfsy'){
					colorF = '#fa2b2b';
				}else if(typeF=='yjdw'){
					colorF = '#d59315';
				}else if(typeF=='sczz'){
					colorF = '#efdf42';
				}else if(typeF=='xfdw'){
					colorF = '#f7502a';
				}else if(typeF=='xfs'){
					colorF = '#2fb6d8';
				}else if(typeF.indexOf('shzy')!=-1){
					colorF = '#52bdbf';
					icon = feature.get('features')[0].getProperties().dtPath;
				}
				style = [ new ol.style.Style({
					image : new ol.style.Icon({
						crossOrigin:'anonymous',
						src : baseFileUrl+icon,
						anchor : [ 0.5, 0.5 ]
					}),
					text : new ol.style.Text({
						text : name,
						offsetY : offsetYH,
						font : '16px -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
						fill : new ol.style.Fill({
							color : colorF
						}),
						stroke : new ol.style.Stroke({
							color : '#ECF5FF',
							width : 1
						})
					})
				}) ];
			}
			return style;
		},
		zIndex:10
	});
	layer.set('id', id);
	return layer;
}

/**
 * 创建聚合图层
 * @param id 图层ID
 * @param type类型、clusterImage，用来生成聚合图片
 * */
function createMakerClusterLayer(id, clusterImage) {
	var source = new ol.source.Vector({
		features : []
	});
	var clusterSource = new ol.source.Cluster({
		source : source,
		distance : 30
	});
	var layer = new ol.layer.Vector({
		source : clusterSource,
		style : function(feature) {
			var size = feature.get('features').length;
			var style;
			if (size > 4) {
				style = [ new ol.style.Style({
					image : new ol.style.Icon({
						crossOrigin:'anonymous',
						src : baseFileUrl+clusterImage,
						anchor : [ 0.5, 0.5 ]
					}),
					text : new ol.style.Text({
						font : '12px Arial',
						text : size.toString(),
						offsetX : 5,
						fill : new ol.style.Fill({
							color : '#FFFFFF'
						})
					}),
					zIndex : 3
				})];
			} else {
				var name = feature.get('features')[0].get("params").name;
				var icon = feature.get('features')[0].get("params").imagePath;
				var anchors = feature.get('features')[0].get('params').clusterType=="accident"?[ 0.5, 0.8 ]:[ 0.5, 0.5 ];
				var angle = feature.get('features')[0].get('params').rotation?feature.get('features')[0].get('params').rotation:0;
				var rotation = Math.PI/180*angle;
				style = [ new ol.style.Style({
					image : new ol.style.Icon({
						crossOrigin:'anonymous',
						src : baseFileUrl+icon,
						anchor : anchors,
						rotation:rotation
					}),
					text : new ol.style.Text({
						text : name,
						offsetY : 20,
						font : '16px -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
						fill : new ol.style.Fill({
							color : '#477280'
						}),
						stroke : new ol.style.Stroke({
							color : '#ECF5FF',
							width : 1
						})
					})
				}) ];
			}
			return style;
		},
		zIndex:10
	});
	layer.set('id', id);
	return layer;
}

function createGroupLayer(layerId){
	var group = new ol.layer.Group({
		layers:[]
	});
	group.set('id',layerId);
	return group;
}



/**
 * 判断图层是否存在
 * 
 * @param layerId 图层ID
 * @param map 地图
 */
function layerIsExist(layerId, map) {
	var lay = null;
	if(map!=undefined){
		map.getLayers().forEach(function(layer) {
			if (layer.get('id') == layerId) {
				lay = layer;
			}
		});
	}
	return lay;
}

/**
 * 控制图层显隐
 * */
function controlLayerVisible(layerId,map,visible){
	map.getLayers().forEach(function(layer){
		if(layer.get('id')==layerId){
			layer.setVisible(visible);
		}
	});
}

/**
 * 热力图
 * */
function createHeatMapLayer(id){
	var layer = new ol.layer.Heatmap({
		source : new ol.source.Vector({
			features:[]
		}),
		blur : 20,
		radius : 12,
		gradient: ['#ffffff','#00ff00','#a1fc00','#ffff00','#fcc700','#ad6600'],
		weight:'value',
		opacity: 0.6
	});
	layer.set('id',id);
	return layer;
}

/**
 * 往图层添加数据
 * 
 * @param features
 *            需要添加的要素
 * @param layer
 *            图层
 * @param isCluster 是否为聚合图层
 */
function addFeatureToLayer(features, layer,isCluster) {
	if(isCluster){
		layer.getSource().getSource().clear();
		layer.getSource().getSource().addFeatures(features);
	}else{
		layer.getSource().clear();
		layer.getSource().addFeatures(features);
	}
}

/**
 * 判断这个Feature是否存在
 * */
function featureIsExist(feature_new,layer,isCluster){
	var id = feature_new.getId();
	if(isCluster){
		var cfeature = layer.getSource().getSource().getFeatureById(id);
		if(cfeature){
			layer.getSource().getSource().removeFeature(cfeature);
			layer.getSource().getSource().addFeature(feature_new);
			//cfeature.setProperties(feature_new.getProperties());
			//cfeature.setGeometry(feature_new.getGeometry());
			return true;
		}else{
			layer.getSource().getSource().addFeature(feature_new);
			return false;
		}
	}else{
		var feature = layer.getSource().getFeatureById(id);
		if(feature){
			//feature.setProperties(feature_new.getProperties());
			//feature.setGeometry(feature_new.getGeometry());
			layer.getSource().removeFeature(feature);
			layer.getSource().addFeature(feature_new);
			return true;
		}else{
			layer.getSource().addFeature(feature_new);
			return false;
		}
	}
	
}

function createSpreadLayer(id) {
	var layer = new ol.layer.Vector({
		source : new ol.source.Vector({
			features:[]
		}),
		style : function(feature) {
			var color = feature.getProperties().value;
			var style = new ol.style.Style({
				fill : new ol.style.Fill({
					color : color
				})
			})
			return style;
		},
		zIndex:10
	});
	layer.set('id',id);
	return layer;
}

/**
 * 根据ID情况Layer
 * */
function clearLayerByIds(ids,map){
	for(var i=0;i<ids.length;i++ ){
		var layer = layerIsExist(ids[i],map);
		if(layer){
			layer.getSource().clear();
		}
	}
}

/**
 * 清空所有自定义图层
 * */
function clearAllLayer(){
	map.getLayers().forEach(function(layer){
		if(layer.get("id").indexOf("Layer")>-1){
			layer.getSource().clear();
		}
	});
}