//大窗口
var isNormal = false;
//小窗口
var isMini = false
//当前播放状态
var isPlay = false;

var players = new Array();

function showBigCamera(){
	isNormal = true;
	$("#videoCloseId").show(); 
}

function closeBigCamera(){
	//清除大窗口APPLET 
	$("#cameraCon").html("");
	for(var i=0;i< players.length;i++){
		if(players[i].TAG ==='FlvPlayer'){
			players[i].pause();
      		players[i].unload();
      		players[i].detachMediaElement();
      		players[i].destroy();
		}else{
			if(players[i].isReady_ == true){
				players[i].dispose();  
			}
		}
	}
	players.splice(0,players.length);
	if(isMini == true && isPlay == true){
		//在小窗口创建APPLET
		play(2);
		$("#videoCloseId").hide();
	}else{
		isPlay = false;
		$("#openCamera").html("<i class=''></i>视频");
		$("#videoCloseId").hide();
	}	
	isNormal = false;
}

function openCamera(){
	if(isPlay == false && isNormal == false && isMini == false){
		$("#cameraCon").html("");
		play(1);
		isPlay = true;
		return;
	}
	if(isPlay == false && isNormal == true){
		//插入大窗口APPLET 播放视频
		$("#cameraCon").html("");
		play(1);
		isPlay = true;
		return;
	}
	if(isPlay == false && isMini == true){
		$("#miniCameraCon").html("");
		//插入小窗口APPLET 播放视频
		play(2);
		isPlay = true;
		return;
	}
	//关闭大窗口视频
	if(isNormal == true && isPlay == true){
		//清除大窗口视频播放
		$("#cameraCon").html("");
		isPlay = false;
		$("#videoCloseId").hide();
		isNormal = false;
		$("#openCamera").html("<i class=''></i>视频");
	}else{
		//清除小窗口视频
		$("#miniCameraCon").html("");
		isPlay = false;
		$("#openCamera").html("<i class=''></i>视频");
	}
	// 清除
	for(var i=0;i< players.length;i++){
		if(players[i].TAG ==='FlvPlayer'){
			players[i].pause();
      		players[i].unload();
      		players[i].detachMediaElement();
      		players[i].destroy();
		}else{
			if(players[i].isReady_ == true){
				players[i].dispose();  
			}
		}
	}
	players.splice(0,players.length);
}

function showMiniCamera(){
	isMini = true;
}

function closeMiniCamera(){
	for(var i=players.length-1;i>=0;i--){
		//判断大小视频,只关小视频
		if(players[i].TAG ==='FlvPlayer'){
			if(players[i]._mediaElement.getAttribute("id").indexOf("cMinVideo")!=-1){
				players[i].pause();
				players[i].unload();
				players[i].detachMediaElement();
				players[i].destroy();
				players.splice(i,1);
			}
		}else{
			if(players[i].id_.indexOf("cMinVideo")!=-1){
				if(players[i].isReady_ == true){
					players[i].dispose();  
				}
				players.splice(i,1);
			}
		}
	}
	if(isNormal == false && isMini == true && isPlay == true){
		//清除小窗口视频
		$("#miniCameraCon").html("");
		isPlay = false;
		$("#openCamera").html("<i class=''></i>视频");
	}
	isMini = false;
}

function miniChange(){
	if(isNormal == false && isMini == true && isPlay == true){
		//清除小窗口视频
		$("#miniCameraCon").html("");
		play(1);
	}else if(isNormal == false && isMini == true && isPlay == false){
		layer.msg('请先开启视频'); 
	}
}

function play(type){	
	//连线摄像头
	if(videoType==1){
		//连线摄像头,通过采集判断是远程还是本地
		$.ajax({
			type : "post",
			url : collectUrl+"/GetSystemSerialPortList",
			timeout : 10000,
			headers: {"Content-Type": "application/json;charset=utf-8"},
			data: JSON.stringify({
				 'userId': currentUserId,
			}),
			success : function(data) {	
				
				//本地
				if(data.isSuccess == true && data.awsPostdata != null &&data.awsPostdata != ""){
					playLocal(type);			
				}else{
				//远程
					//playLocal(type);
					playRemote(type);
				}
			},error: function(request) {
				playRemote(type);
			}
		});	
	}else if(videoType==2){
		//4G摄像头
		playRemote4G(type);
	}
	
}

function playLocal(type){
	var cameras = new Array();
	$.ajax({
		type:'POST',
		url: baseUrl+'/webApi/camera/qryCamera.jhtml',
		headers: {"Content-Type": "application/json;charset=utf-8"},
		dataType:'JSON',
		success: function(data){
			if(type == 1){
				$("#openCamera").html("<i class='closeIcon'></i>视频关闭");
				isPlay = true;
				if(isNormal == false){
					$("#videoCloseId").show();	
				}
				$.each(data.data.cameras,function(index,element){
				
						var applet = "<div class='camera_c'><applet codebase='../applet' code='net.rxwy.applet.hik.main.class'"+
						"archive='../applet/hik.jar' name='main' width='100%' height='100%' id='hik"+ index +"'>"+
						"<param name='ip' value='"+element.ip+"' />"+
						"<param name='un' value='"+element.account+"' />"+
						"<param name='ps' value='"+element.password+"' />"+
						"<param name='channel' value='1' /></div>"
						$("#cameraCon").append(applet);	
						//cameras.push(element.oip); 
 						var camerasParam={};
						camerasParam.account=element.oaccount;
						camerasParam.password=element.opassword;
						camerasParam.ip=element.oip;
						camerasParam.width=480;
						camerasParam.height=350;
						// cameras.push('admin:txt12345@'+element.oip)  // 添加了用户名密码的摄像头连接参数
						cameras.push(camerasParam) // 添加了用户名密码的摄像头连接参数，以及视频的宽高
				});
				isNormal = true;
			}else if(type == 2){
				$("#openCamera").html("<i class='closeIcon'></i>视频关闭");
				isPlay = true;
				$.each(data.data.cameras,function(index,element){
					
						var applet = "<li class='minicamera_c'><applet codebase='../applet' code='net.rxwy.applet.hik.main.class'"+
						"archive='../applet/hik.jar' name='main' width='100%' height='100%' id='hik"+ index +"'>"+
						"<param name='ip' value='"+element.ip+"' />"+
						"<param name='un' value='"+element.account+"' />"+
						"<param name='ps' value='"+element.password+"' />"+   
						"<param name='channel' value='1' /></li>"
						$("#miniCameraCon").append(applet);
						//cameras.push(element.oip);
						var camerasParam={}; 
						camerasParam.account=element.oaccount;
						camerasParam.password=element.opassword;
						camerasParam.ip=element.oip;
						camerasParam.width=480;
						camerasParam.height=350;
						// cameras.push('admin:txt12345@'+element.oip)  // 添加了用户名密码的摄像头连接参数
						cameras.push(camerasParam) // 添加了用户名密码的摄像头连接参数，以及视频的宽高
				});
				isMini = true;
			}
			var streamserver = data.data.streamserver;
			
			//开启推流
			$.ajax({
				type:'POST',
				url: collectUrl+"/rtspPush",
				headers: {"Content-Type": "application/json;charset=utf-8"},
				dataType:'JSON',
				data: JSON.stringify({
					 'cameraIds': cameras,
					 'steamserver':streamserver
				}),
				success: function(data){
					layer.msg("开启流媒体推送成功", {time: 1000});
				}
			});	
		}
	});
}

//播放本地
function playRemote(type){
	$.ajax({
		type:'POST',
		url: baseUrl+'/webApi/camera/qryCameraRemote.jhtml', 
		headers: {"Content-Type": "application/json;charset=utf-8"},
		dataType:'JSON',
		success: function(data){
			if(type == 1){
				$("#openCamera").html("<i class='closeIcon'></i>视频关闭");
				isPlay = true;
				if(isNormal == false){
					$("#videoCloseId").show();	
				}
				$.each(data.data,function(index,element){
					var applet = "<div class='camera_c'>" +
					"<video id='cvideo" + index + "' class='video-js vjs-default-skin' controls width='450' height='240' data-setup='{}'>" +
					"<source src='rtmp://" + streamserver + "/hls/"+ element.ip +"' type='rtmp/flv'>" +
					"<p class='vjs-no-js'>您的浏览器不支持RTMP流播放，请升级浏览器</p>" +
					"</video></div>";
					$("#cameraCon").append(applet);
					var player = videojs('cvideo' + index);  
				    player.play();  
				    players.push(player);
				});
				isNormal = true;
			}else if(type == 2){
				$("#openCamera").html("<i class='closeIcon'></i>视频关闭");
				isPlay = true;
				$.each(data.data,function(index,element){
					var applet = "<div class='camera_c'>" +
					"<video id='cMinVideo"  + index +  "' class='video-js vjs-default-skin' controls width='450' height='240' data-setup='{}'>" +
					"<source src='rtmp://" + streamserver + "/hls/"+ element.ip +"' type='rtmp/flv'>" +
					"<p class='vjs-no-js'>您的浏览器不支持RTMP流播放，请升级浏览器</p>" +
					"</video></div>";
					$("#miniCameraCon").append(applet);
					var player = videojs('cMinVideo' + index);  
				    player.play();
				    players.push(player);
				});	
				isMini = true;
			}
		}
	}); 
}


//播放本地
function playRemote4G(type){
	$.ajax({
		type:'POST',
		url: baseUrl+'/webApi/camera/qry4gCamera.jhtml', 
		headers: {"Content-Type": "application/json;charset=utf-8"},
		dataType:'JSON',
		success: function(data){
			if(type == 1){
				$("#openCamera").html("<i class='closeIcon'></i>视频关闭");
				isPlay = true;
				if(isNormal == false){
					$("#videoCloseId").show();	
				}
				$.each(data.data,function(index,element){
					var params={};
					params.account = encodeURI(element.account);
					params.password = encodeURI(element.password);
					$.ajax({
						type:'POST',
			            url: 'http://'+video4gServerIp+':'+video4gServerPort+'/StandardApiAction_login.action',
			            data: params,
			            cache:false,
			            dataType:'json', 
			            success: function (json) {
			               if(json.result == 0){
			            	   /*var src = 'http://'+video4gServerIp+':'+video4gPort+'/hls/1_'+element.deviceNo+'_0_1.m3u8?JSESSIONID='+json.jsession;
			            	   var applet = "<div class='camera_c'>" +
								"<video id='cvideo" + index + "' class='video-js vjs-default-skin' controls width='450' height='240' data-setup='{}'>" +
								"<source src='"+src+"' type='application/x-mpegURL'>" +
								"<p class='vjs-no-js'>您的浏览器不支持RTMP流播放，请升级浏览器</p>" +
								"</video></div>";
								$("#cameraCon").append(applet);
								var player = videojs('cvideo' + index);  
							    player.play();  
							    players.push(player);
							    isNormal = true;*/
							    
							    var src = 'http://'+video4gServerIp+':'+video4gPort+'/3/3?AVType=1&jsession='+json.jsession+'&DevIDNO='+element.deviceNo+'&Channel=0&Stream=1';
							    var applet = "<div class='camera_c'>" +
								"<video id='cvideo" + index + "'  controls width='450' height='240' data-setup='{}'>" +
								"<p class='vjs-no-js'>您的浏览器不支持flv流播放，请升级浏览器</p>" +
								"</video></div>";
							    $("#cameraCon").append(applet);
							    var videoElement = document.getElementById('cvideo' + index);
        						var flvPlayer = flvjs.createPlayer({
          									type: 'flv',
          									isLive: true,
          									url: src
        						});
        						flvPlayer.attachMediaElement(videoElement);
        						flvPlayer.load();
        						flvPlayer.play();
							    players.push(flvPlayer);
							    isNormal = true;
			               } else {
			                 alert("获取视频会话号失败！");
			               }
			            },error:function(XMLHttpRequest, textStatus, errorThrown){
			            	alert("获取视频会话号失败！");
			            }
					});
				});
				
			}else if(type == 2){
				$("#openCamera").html("<i class='closeIcon'></i>视频关闭");
				isPlay = true;
				$.each(data.data,function(index,element){
					var params={};
					params.account = encodeURI(element.account);
					params.password = encodeURI(element.password);
					$.ajax({
						type:'POST',
			            url: 'http://'+video4gServerIp+':'+video4gServerPort+'/StandardApiAction_login.action',
			            data: params,
			            cache:false,
			            dataType:'json', 
			            success: function (json) {
			               if(json.result == 0){
			            	   /*var src = 'http://'+video4gServerIp+':'+video4gPort+'/hls/1_'+element.deviceNo+'_0_1.m3u8?JSESSIONID='+json.jsession;
			            	   var applet = "<div class='camera_c'>" +
								"<video id='cMinVideo"  + index +  "' class='video-js vjs-default-skin' controls width='450' height='240' data-setup='{}'>" +
								"<source src='"+src+"' type='application/x-mpegURL'>" +
								"<p class='vjs-no-js'>您的浏览器不支持RTMP流播放，请升级浏览器</p>" +
								"</video></div>";
								$("#miniCameraCon").append(applet);
								var player = videojs('cMinVideo' + index);  
							    player.play();*/
							    
							    var src = 'http://'+video4gServerIp+':'+video4gPort+'/3/3?AVType=1&jsession='+json.jsession+'&DevIDNO='+element.deviceNo+'&Channel=0&Stream=1';
							    var applet = "<div style='background-color: #000;margin: 5px;margin-left: 10px;float: left;display: inline-block;' >" +
								"<video id='cMinVideo"  + index +  "'  class='video-js vjs-default-skin' controls data-setup='{}'>" +
								"<p class='vjs-no-js'>您的浏览器不支持RTMP流播放，请升级浏览器</p>" +
								"</video></div>";
								$("#miniCameraCon").append(applet);
							    var videoElement = document.getElementById('cMinVideo' + index)
        						var flvPlayer = flvjs.createPlayer({
          									type: 'flv',
          									isLive: true,
          									url: src
        						});
        						flvPlayer.attachMediaElement(videoElement);
        						flvPlayer.load()
        						flvPlayer.play()
							    players.push(flvPlayer);
							    isMini = true;
			               } else {
			                 alert("获取视频会话号失败！")
			               }
			            },error:function(XMLHttpRequest, textStatus, errorThrown){
			            	alert("获取视频会话号失败！")
			            }
					});
				});	
				
			}
		}
	}); 
}