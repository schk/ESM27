var fp;
var keyUnit=1;
var keyPartsType=1;
var chatTimer;
var resourceType=1;
var roomFlag=false;
var currentUserName = "";
var currentUserId ="";
 var currentRoomId = "";
var currentRoomName = "";
var pageNum=1;
var pageSize=10;	
var $layer = layer;
var isDrawing = false;


//气象数据

var global_weatherData = {};
//智能推荐报警组合
var global_alertGrasZNTJ = new Map(); 
var global_realtimeData = new Map();
//面板发开 
var spreadTimer = null;
var isOpenConsult = false;
var _stopTimer = false;
//事故单位信息
var accidentCompany={};
var accidentMsg={};
//事故点的坐标值
var zhmnAccidentPoint=[];
//var esm27Point=new HashMap();
var global_gasIsAlertNameData = "";
var global_gasIsAlertM=0;
//判断是不是被同步的计算灾害模拟
var zhmnFlag = false;
//判断灾害模拟DIV开关
var qxzsIsopen = true;
var zzyppjIsopen = true;
var rsxfxIsopen = true;
//扩散播放
var showSpreadTimeOut = null;
//射水类灭火剂
var waterSupplyEquipment = [];
//行政图和卫星图的图片数据加载list
var xzImageResourceList=[];
var wxImageResourceList=[];
var disasterType = "";
/**
 * 增加ajax拦截 弹出遮罩
 */
var ajaxBack = $.ajax;


//由于get/post/getJSON等，最后还是调用到ajax，因此只要改ajax函数
/**
 * ajax请求重写，会在请求前显示正在加载的div 由ladeview(bool) 控制
 */
$.ajax = function(setting){
	var beforeSend = setting.beforeSend;
	//是否显示加载div 默认为true
	if(typeof(setting.ladeview)=="undefined"){
		setting.ladeview = true;
	}
	setting.beforeSend=function(){
		if(beforeSend){
			beforeSend();
		}
		if($('#lodingdiv').length==0){
			//是否显示加载div
			if(setting.ladeview){
				var c = '<div id="lodingdiv" index="1" style="width:100%;top:0px;height: 100%;position: absolute;text-align: center;z-index: 100000;margin: 0 auto;background-color: rgba(0,0,0,0.3);">';
				c += '<span style="top: 50%;position: absolute;left:50%;"><img src="'+baseUrl+'/images/loading.gif" alt="" /></span>';
				c += '</div>';
				$('body').after(c);
				//关闭后门
				$('#lodingdiv').click(function(){
					var v = parseInt($(this).attr("index"));
					v++;
					$(this).attr("index",v);
					if(v==20){
						$('#lodingdiv').remove();
					}
				})
			}
		}
	};
	var complete= setting.complete ;
	setting.complete = function(XMLHttpRequest,status){
		var res = XMLHttpRequest.responseText;
		/*console.log(XMLHttpRequest);
		console.log(status);*/
		if(XMLHttpRequest.readyState==4){
	        if(XMLHttpRequest.status==401){
	           // 没登录跳转登陆页面
	        	location.href = baseUrl+"/webApi/login.jhtml";
	        }
	    }
		
		if($('#lodingdiv').length>=1){
			$('#lodingdiv').remove();
		}else{
			setTimeout(function(){
				$('#lodingdiv').remove();
			}, 10);
		}
		if(complete){
			complete();
		}
	};
	ajaxBack(setting);
}
//关闭后门
window.onerror=function(){$('#lodingdiv').remove();} 

function look1Point(e){
	var lon = $(e).attr("lon");
	var lat  = $(e).attr("lat");
	gotoPointView(lat,lon,17);
}


//show截取
var showBack = $.fn.show;
$.fn.show = function(){
	var t = this;//自己 当前元素
	var classs=["ol-popup","blast","gaswarp","frame","toolBox","materialSee","iconBox","yaFrame","dangerous-frame","frameWarp","frameVideo"];//隐藏的class
	var notclasss=[""];//不用隐藏的class createRoom   
	var ol_viewport = ['urgent','reWarp'];//.ol_viewport 样式
	var h = false;//是否包含 class
	//循环 判断是否包含
	classs.forEach(function(item){
		if(t.hasClass(item)){
			h = true;
		}
	})
	//如果有
	if(h){
		//循环 关闭 指定class
		classs.forEach(function(item){
			//是否有不需要关闭的class
			var h2 = false;
			notclasss.forEach(function(item2){
				//元素包含 不关闭的class
				if(t.hasClass(item2)){
					h2=true;
				}
			});
			//如果不为true
			if(!h2){
				$('.'+item).hide();
			}
		});
	}
	//显示
	return showBack.apply(t,arguments);
}

$(document).ready(function(){
	 currentUserName = $("#currentUserName").val();
	 currentUserId = $("#currentUserId").val();
	 if(videoType==1){
		 $("#sxtpzVideoType").html("视频配置(连线版)");
	 }else{
		 $("#sxtpzVideoType").html("视频配置(4G版)");
	 }
	 if($("#leftMenuContent").is(":hidden")){
		$("#capsZNFX").css("left","103px");
		if(accidentCompany.lonlat4326){
			$("#fireDosageDiv").css("top","130px");
		}
		$("#subMeusDiv").css("margin-left","90px")
		setMeunPosition();
     }
	 $("#homeMenu").click(function () {
		if($("#leftMenuContent").is(":hidden")){
			$("#leftMenuContent").show();
			$("#capsZNFX").css("left","363px");
			$("#fireDosageDiv").css("top","120px");
			if($(".subMeus").is(":hidden")){
				$("#fireDosageDiv").css("top","130px");
			}else{
				$("#fireDosageDiv").css("top","130px");
			}
			if(accidentCompany.lonlat4326){
				if($(".subMeus").is(":hidden")){
					$("#fireDosageDiv").css("top","75px");
				}else{
					$("#fireDosageDiv").css("top","130px");
				}
			}
			$("#subMeusDiv").css("margin-left","670px")
			setMeunPosition();

	    }else{
		    $("#leftMenuContent").hide(); 
			$("#capsZNFX").css("left","103px");
			$("#fireDosageDiv").css("left","");
			$("#fireDosageDiv").css("top","75px");
			if($(".subMeus").is(":hidden")){
				$("#fireDosageDiv").css("top","75px");
			}else{
				$("#fireDosageDiv").css("top","130px");
			}
			if(accidentCompany.lonlat4326){
				if($(".subMeus").is(":hidden")){
					$("#fireDosageDiv").css("top","75px");
				}else{
					$("#fireDosageDiv").css("top","130px");
				}
			}
			$("#subMeusDiv").css("margin-left","90px")
			setMeunPosition();
		}			   
	  });

    /*切换顶部菜单*/
    $("#topNavMenu").find("li").click(function(){	
        if($(this).hasClass("ZtopNavHover")){
            $(this).removeClass("ZtopNavHover");
        }else{
            $(this).addClass("ZtopNavHover").siblings().removeClass("ZtopNavHover"); //切换选中的按钮高亮状态
        }
    });
	    
	/*content块*/
	 $("#allContents").find("ul li").click(function(){
		 $("#leftMenuContent").show();
		 $("#capsZNFX").css("left","363px");
		 if(accidentCompany.lonlat4326){
			if($(".subMeus").is(":hidden")){
				$("#fireDosageDiv").css("top","75px");
			}else{
				$("#fireDosageDiv").css("top","130px");
			}
		 }
		 $("#subMeusDiv").css("margin-left","370px");
		 setMeunPosition();
		 pageNum=1; 	   
	     $("#specialistDetailId").empty();	
	     $("#specialistDetailId").hide();
		 $(this).addClass("ant-menu-item-current").siblings().removeClass("ant-menu-item-current"); //切换选中的按钮高亮状态
		 var tagT = $(this).attr("t");
		 if(tagT==9){
			 if(currentRoomId==''){
			 }else{
				 $("#leftMenuContent > div").eq(tagT).show().siblings().hide(); //在按钮选中时在下面显示相应的内容，同时隐藏不需要的框架内容
			 }
		 }else{
			 $("#leftMenuContent > div").eq(tagT).show().siblings().hide(); //在按钮选中时在下面显示相应的内容，同时隐藏不需要的框架内容
		 }
		 if(tagT==0){
			 getDataByParam("重点单位");
			 $('#factoryContId').css("display","none");
			 keyUnit=1;
			 $("#manageUnitList2").html("");
			 $("#keyUnitPage2").html("");
			 $("#zddwListKuanDiv").hide();
			 $("#zddwListDiv").show();
			 $("#leftMenuContentZDDW").css("width","260px");
			 if($('#AccidentAddress').length>0){
				$('#AccidentAddress').trigger('click');
			 }
		 }else if(tagT==1){
			 getDataByParam("消防水源");
			 $("#soureManageList2").html("");
			 $("#fireWaterSourcePage2").html("");
			 $("#waterContentKuanDiv").hide();
			 $("#waterContentZhaiDiv").show();
			 $("#waterContent").css("width","260px");
		 }else if(tagT==2){ 
			 $("#dangerChemicStoreData").show();
			 $("#dangerChemicDetail").hide();
			 $("#dangerChemicPage").show();
		     getDataByParam("危化品库");
		 }else if(tagT==3){
			 getDataByParam("应急队伍");
		 }else if(tagT==4){ 
			 $("#emergencyEquipment2").html("");
			 $("#emergencyEquipmentPage2").html("");
			 $("#carInfoKuanDiv").hide();
			 $("#carInfoZhaiDiv").show();
			 $("#carInfoContent").css("width","260px");
			 getDataByParam("消防车辆");
		 }else if(tagT==5){ 
			 getDataByParam("物资储备点");
		 }else if(tagT==6){ 
			 $("#specialistData2").html("");
			 $("#expertPage2").html("");
			 $("#expertDivKuan").hide();
			 $("#expertDivZhai").show();
			 $("#expertContent").css("width","260px");
			 getDataByParam("应急专家");
		 }else if(tagT==7){
			 $("#reservePlanList2").html("");
			 $("#plansPage2").html("");
			 $("#ReservePlanDivKuan").hide();
			 $("#ReservePlanDivZhai").show();
			 $("#reservePlanContent").css("width","260px");
		     getDataByParam("应急预案");
		 }else if(tagT==8){
		     getDataByParam("生产装置");
		 }else if(tagT==9){
			 if(currentRoomId==''){
				 layer.msg("目前没有发生被记录的事故", {time: 1000});
			 }else{
				 $("#accidentHistoryId").attr("src",baseUrl+"/webApi/report/accidentHistoryContent.jhtml?accidentId="+currentRoomId);
				 $("#accidentHistoryId").show();
//				 getDataByParam("救援记录");
////				 //打开地图
//				 initHistoryMap();
			 }
		 }else if(tagT==10){
			getDataByParam("场景搭建");
		 }else if(tagT==11){
			getDataByParam("消防队伍");
		 }else if(tagT==12){
			$("#ZHMNyygqfyModelCGZYMGD").val("17");
			$("#ZHMNyygqfyModelCYZSDCGD").val("0.2");
			$("#ZHMNrswfxModelGTZJ").val("80");
			$("#ZHMNrswfxModelDivCc").val("85");
			$("#ZHMNrswfxModelDivCs").val("0.07");
			$("#ZHMNrswfxModelDivCn").val("1");
			$("#ZHMNrfsModelHZMJ").val("5026");
			$("#ZHMNrfsModelXLDJDMGD").val("21.8");
		 }else if(tagT==13){
			 getDataByParam("典型火灾");
		 }else if(tagT==14){
			 $("#keyPartsList2").html("");
			 $("#keyPartsPage2").html("");
			 $("#locationContentKuan").hide();
			 $("#keyPartDetailContId").hide();
			 $("#locationContentZhai").show();
			 $("#locationContent").css("width","260px");
			 getDataByParam("重点部位");
		 }
		 
	 });
	 
	$('#searBtn').on('click',function(){	
		var allSearchInput=$("#allSearchInput").val();
		var allSearchTypeId=$("#allSearchTypeId").val();
		var htmlUrl='';
		if(allSearchTypeId=='zddw'){
			htmlUrl=baseUrl+'/webApi/qryList.jhtml';
		}else if(allSearchTypeId=='xfsy'){
			htmlUrl=baseUrl+'/webApi/qryFireWaterSource.jhtml';
		}else if(allSearchTypeId=='yjdw'){
			htmlUrl=baseUrl+'/webApi/qryTeams.jhtml';
		}else if(allSearchTypeId=='xfdw'){
			htmlUrl=baseUrl+'/webApi/qryFireBrigade.jhtml';
		}else if(allSearchTypeId=='wzcbd'){
			htmlUrl=baseUrl+'/webApi/qryReservePoint.jhtml';
		}else if(allSearchTypeId=='sczz'){
			htmlUrl=baseUrl+'/webApi/qryEquipmentList.jhtml';
		}
		var params={"name":allSearchInput};
	   $.ajax({
			type : "post",
			url : htmlUrl,
			data: JSON.stringify(params),
			dataType:"json",
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {
				if(data.httpCode==200){
					var html='';
					for(j = 0,len=data.data.length; j < len; j++) {
						var obj = data.data[j];
						html+='<li class="olf ca cf" id='+obj.id+' catecd='+allSearchTypeId+'><i></i><span>'+obj.name+'</span></li>';
					}
					$("#aroundSearchResult").html(html);	
					$("#searchResutePanel").show();
					$(".olf").click(function(){
						mapSearchLocation($(this).attr("catecd"),$(this).attr("id"));
					});
				}else{
					layer.msg(systemAbnormalityMsg, {time: 2000});
				}
				
			},error: function(request) {
				layer.msg(networkErrorMsg, {time: 2000});
            }
		});
		
	});
	
	
	$('#selectOptionZDDW').on('click','li',function(){
		$("#allSearchTypeId").val($(this).attr("name"));
		$("#allSearchInput").val("");
	    $("#selectOptionZDDW").hide();   
		if($(this).text()){
		   $("#aroundSearchResult").html('');
		   $("#zdddName").text($(this).text()); 
		   $(this).addClass("ZselectOptionHover").siblings().removeClass("ZselectOptionHover");//默认选中当前选项	
		}					
		
	});
	/*头部搜索框事件 结束*/
	
    /*遍历所有数据，取出是该类型的数据*/
	getDataByParam("重点单位");
	
	/*点击重点单位的选项，展示选项*/
	$("#changeSelectUnit").click(function () {
		if($("#unitSelectUnit").is(":hidden")){
			$("#unitSelectUnit").show();    //如果元素为隐藏,则将它显现
		}else{
			$("#unitSelectUnit").hide();     //如果元素为显现,则将其隐藏
		} 
    });
	/*点击重点单位的选项，展示选项  宽页面*/
	$("#changeSelectUnit2").click(function () {
		if($("#unitSelectUnit2").is(":hidden")){
			$("#unitSelectUnit2").show();    //如果元素为隐藏,则将它显现
		}else{
			$("#unitSelectUnit2").hide();     //如果元素为显现,则将其隐藏
		} 
    });
	/*给每个重点单位的li绑定事件*/
	$('#unitSelectUnit').on('click','li',function(){
		pageNum=1; 
		$("#zdswFireBrigadeId").val($(this).attr("name"));
		if($(this).text()){
		   $("#unitName").text($(this).text()); 
		   $(this).addClass("ZhoverCss").siblings().removeClass("ZhoverCss");//默认选中当前选项	
		}			
        getDataByParam("重点单位");				
		$("#unitSelectUnit").hide();   
	});
	/*点击重点单位的选项，展示选项*/
	$("#unitSearEvent").click(function () {
		pageNum=1; 
		getDataByParam("重点单位");
    });
	
	/*给每个重点单位的li绑定事件*/
	$('#unitSelectUnit2').on('click','li',function(){
		pageNum=1; 
		$("#zdswFireBrigadeId2").val($(this).attr("name"));
		if($(this).text()){
		   $("#unitName2").text($(this).text()); 
		   $(this).addClass("ZhoverCss").siblings().removeClass("ZhoverCss");//默认选中当前选项	
		}			
		getKeyUnitKuanAjax();		
		$("#unitSelectUnit2").hide();   
	});
	/*点击重点单位的选项，展示选项*/
	$("#unitSearEvent2").click(function () {
		pageNum=1; 
		getKeyUnitKuanAjax();
    });
	/*重点单位结束*/
	
	/*点击重点部位的选项，展示选项*/
	$("#changeKeyPartsUnit").click(function () {
		if($("#keyPartsUnitSelect").is(":hidden")){
			$("#keyPartsUnitSelect").show();    //如果元素为隐藏,则将它显现
		}else{
			$("#keyPartsUnitSelect").hide();     //如果元素为显现,则将其隐藏
		}
    });
	
	/*给每个重点单位的li绑定事件*/
	$('#keyPartsUnitSelect').on('click','li',function(){
		pageNum=1; 
		$("#selectKeyPartsOfUnit").val($(this).attr("name"));
		if($(this).text()){
		   $("#keyPartsUnitName").text($(this).text()); 
		   $(this).addClass("ZhoverCss").siblings().removeClass("ZhoverCss");//默认选中当前选项	
		}			
        getDataByParam("重点部位");				
		$("#keyPartsUnitSelect").hide();   
	});
				
	/*点击重点单位的选项，展示选项*/
	$("#keyPartsSearEvent").click(function () {
		pageNum=1; 
		getDataByParam("重点部位");
    });

	/*重点部位宽内容，展示选项*/
	$("#changeKeyPartsUnit2").click(function () {
		$("#keyPartsUnitSelect2").show();   
    });
	
	/*给每个重点单位的li绑定事件*/
	$('#keyPartsUnitSelect2').on('click','li',function(){
		pageNum=1; 
		$("#selectKeyPartsOfUnit2").val($(this).attr("name"));
		if($(this).text()){
		   $("#keyPartsUnitName2").text($(this).text()); 
		   $(this).addClass("ZhoverCss").siblings().removeClass("ZhoverCss");//默认选中当前选项	
		}			
		getLocationContentAjax();				
		$("#keyPartsUnitSelect2").hide();   
	});
				
	/*点击重点单位的选项，展示选项*/
	$("#keyPartsSearEvent2").click(function () {
		pageNum=1; 
		getLocationContentAjax();
    });
	
	/*点击典型火灾输入框搜索事件*/
	$("#accidentCaseSearEvent").click(function () {
		pageNum=1; 
		getDataByParam("典型火灾");
    });
	
	/*切换消防水源的类型*/
	//重置类型
	$("#soureTypeId").val('2');//1消防水源 2消防设施
	$("#tabSoureId").find("ul li").click(function(){	
		pageNum=1; 
		var type = $(this).attr("name");
		$("#soureTypeId").val(type);//1消防水源 2消防设施
		$("#soureManageType").hide(); 
		$("#soureManageUnit").hide(); 
		//创建本地缓存
		if(!window._ManageSoure){window._ManageSoure = {};};
		//如果为1 因为初始化为1所以缓存 避免回发
		if(type != "1" && !window._ManageSoure["1"]){
			window._ManageSoure["1"] = $("#soureManageType ul").html();
		}
		//如果未被激活则切换数据
		if(!$(this).hasClass("onCurrent")){
			GetManageSoureByType(type);
		}
		$(this).addClass("onCurrent").siblings().removeClass("onCurrent"); //切换选中的按钮高亮状态
	});	
	
	//通过类型1消防水源 2消防设施  得到类型
	function GetManageSoureByType(_type){
		$("#soureManageList").html("");
		$("#fireWaterSourcePage").html("");//重置数据
		$("#soureKeyTypeId").val('');//重置类型
		$("#soureManageTypeName").text($("#soureManageType ul li").eq(0).text());
		//$("#soureTypeId").val('');//重置大类
		if(window._ManageSoure[_type]){
			$("#soureManageType ul").html('').html(window._ManageSoure[_type]);
			getDataByParam("消防水源");
		}else{
			var params={"type":_type};
			$.ajax({
				type : "post",
				url : baseUrl+"/webApi/GetManageSoureByType.jhtml",
				data: JSON.stringify(params),
				dataType:"json",
				headers: {"Content-Type": "application/json;charset=utf-8"},
				success : function(data) {					
					var html = GetManageSoureHtml(data);
					//leftResultAddToMap(features,'xfsy','showLeftLayer');
					$("#soureManageType ul").html('').html(html);
					window._ManageSoure[_type] = $("#soureManageType ul").html();
					getDataByParam("消防水源");
				},error: function(request) {
					layer.msg(networkErrorMsg, {time: 2000});
				}
			});
		}
	}

	/*消防水源下拉显示选项事件*/
	$("#changeSoureManageType").click(function () {
		if($("#soureManageType").is(":hidden")){
			$("#soureManageType").show();    //如果元素为隐藏,则将它显现
		}else{
			$("#soureManageType").hide();     //如果元素为显现,则将其隐藏
		}  
    });	
	/*消防水源下拉切换选项事件*/
	$("#soureManageType").on('click','li',function(){	
		pageNum=1; 
		$("#soureKeyTypeId").val($(this).attr("name"));
		if($(this).text()){
		   $("#soureManageTypeName").text($(this).text()); 
		   $(this).addClass("ZhoverCss").siblings().removeClass("ZhoverCss");//默认选中当前选项	
		}	
		$("#soureManageType").hide(); 
		getDataByParam("消防水源");			
	});	
	
	/*消防水源下拉显示选项事件*/
	$("#changeSoureManageUnit").click(function () {
		if($("#soureManageUnit").is(":hidden")){
			$("#soureManageUnit").show();    //如果元素为隐藏,则将它显现
		}else{
			$("#soureManageUnit").hide();     //如果元素为显现,则将其隐藏
		}
    });	
	/*消防水源下拉切换选项事件*/
	$("#soureManageUnit").on('click','li',function(){	
		pageNum=1; 
		$("#soureKeyUnitId").val($(this).attr("name"));
		if($(this).text()){
		   $("#soureManageUnitName").text($(this).text()); 
		   $(this).addClass("ZhoverCss").siblings().removeClass("ZhoverCss");//默认选中当前选项	
		}	
		$("#soureManageUnit").hide(); 
		getDataByParam("消防水源");			
	});	
	/*消防水源查询按钮事件*/
	$("#soureManageBtnEvent").click(function(){
		pageNum=1; 
		getDataByParam("消防水源");
	});
	
	
	//重置类型
	$("#soureTypeId2").val('2');//1消防水源 2消防设施
	$("#tabSoureId2").find("ul li").click(function(){	
		pageNum=1; 
		var type = $(this).attr("name");
		$("#soureTypeId2").val(type);//1消防水源 2消防设施
		$("#soureManageType2").hide(); 
		$("#soureManageUnit2").hide(); 
		//创建本地缓存
		if(!window._ManageSoure2){window._ManageSoure2 = {};};
		//如果为1 因为初始化为1所以缓存 避免回发
		if(type != "1" && !window._ManageSoure2["1"]){
			window._ManageSoure2["1"] = $("#soureManageType2 ul").html();
		}
		//如果未被激活则切换数据
		if(!$(this).hasClass("onCurrent")){
			GetManageSoureByType2(type);
		}
		$(this).addClass("onCurrent").siblings().removeClass("onCurrent"); //切换选中的按钮高亮状态
	});	
	
	//通过类型1消防水源 2消防设施  得到类型
	function GetManageSoureByType2(_type){
		$("#soureManageList2").html("");
		$("#fireWaterSourcePage2").html("");//重置数据
		$("#soureKeyTypeId2").val('');//重置类型
		$("#soureManageTypeName2").text($("#soureManageType2 ul li").eq(0).text());
		//$("#soureTypeId").val('');//重置大类
		if(window._ManageSoure2[_type]){
			$("#soureManageType2 ul").html('').html(window._ManageSoure2[_type]);
			getWaterContentAjax();	
		}else{
			var params={"type":_type};
			$.ajax({
				type : "post",
				url : baseUrl+"/webApi/GetManageSoureByType.jhtml",
				data: JSON.stringify(params),
				dataType:"json",
				headers: {"Content-Type": "application/json;charset=utf-8"},
				success : function(data) {					
					var html = GetManageSoureHtml(data);
					//leftResultAddToMap(features,'xfsy','showLeftLayer');
					$("#soureManageType2 ul").html('').html(html);
					window._ManageSoure2[_type] = $("#soureManageType2 ul").html();
					getWaterContentAjax();	
				},error: function(request) {
					layer.msg(networkErrorMsg, {time: 2000});
				}
			});
		}
	}

//	消防水源搜索事件 宽页面
	$("#changeSoureManageType2").click(function () {
		if($("#soureManageType2").is(":hidden")){
			$("#soureManageType2").show();    //如果元素为隐藏,则将它显现
		}else{
			$("#soureManageType2").hide();     //如果元素为显现,则将其隐藏
		}   
    });
	
	/*消防水源下拉切换选项事件*/
	$("#soureManageType2").on('click','li',function(){	
		pageNum=1; 
		$("#soureKeyTypeId2").val($(this).attr("name"));
		if($(this).text()){
		   $("#soureManageTypeName2").text($(this).text()); 
		   $(this).addClass("ZhoverCss").siblings().removeClass("ZhoverCss");//默认选中当前选项	
		}	
		$("#soureManageType2").hide(); 
		getWaterContentAjax();	
	});	
	
	/*消防水源下拉显示选项事件*/
	$("#changeSoureManageUnit2").click(function () {
		if($("#soureManageUnit2").is(":hidden")){
			$("#soureManageUnit2").show();    //如果元素为隐藏,则将它显现
		}else{
			$("#soureManageUnit2").hide();     //如果元素为显现,则将其隐藏
		}
    });	
	/*消防水源下拉切换选项事件*/
	$("#soureManageUnit2").on('click','li',function(){	
		pageNum=1; 
		$("#soureKeyUnitId2").val($(this).attr("name"));
		if($(this).text()){
		   $("#soureManageUnitName2").text($(this).text()); 
		   $(this).addClass("ZhoverCss").siblings().removeClass("ZhoverCss");//默认选中当前选项	
		}	
		$("#soureManageUnit2").hide(); 
		getWaterContentAjax();			
	});	
    //消防水源宽     
	$("#soureManageBtnEvent2").click(function () {
		pageNum=1; 	    
		getWaterContentAjax();
    });
	/*$("#changePlanType2").click(function () {
		$("#reservePlanSelect2").show();   
    });*/
	$('#reservePlanSelect2').on('click','li',function(){
		pageNum=1; 
		$("#planTypeId2").val($(this).attr("name"));
		if($(this).text()){
		   $("#reservePlanType2").text($(this).text()); 
		   $(this).addClass("ZhoverCss").siblings().removeClass("ZhoverCss");//默认选中当前选项	
		}
		$("#reservePlanSelect2").hide(); 
		getReservePlanAjax();				
	});
	
	/*给危化品库添加搜索事件*/
	$('#dangerChemicStoreBtnEvent').click(function () {
	    $("#dangerChemicStoreData").show();
        $("#dangerChemicDetail").hide();
        pageNum=1; 
        getDataByParam("危化品库");
	});
	
	//应急队伍
	$("#emergencyTeamBtnEvent").click(function(){
		pageNum=1; 
		getDataByParam("应急队伍");
	});
	
	//消防队伍
	$("#emergencyfireBrigadeBtnEvent").click(function(){
		pageNum=1; 
		getDataByParam("消防队伍");
	});

	/*消防车辆 开始*/
	$("#changeEmergencyEquipmentType2").click(function () {
		if($("#emergencyEquipmentType2").is(":hidden")){
			$("#emergencyEquipmentType2").show();    //如果元素为隐藏,则将它显现
		}else{
			$("#emergencyEquipmentType2").hide();     //如果元素为显现,则将其隐藏
		}  
    });
	/*消防车辆 开始 宽页面*/
	$("#changeEmergencyEquipmentType").click(function () {
		if($("#emergencyEquipmentType").is(":hidden")){
			$("#emergencyEquipmentType").show();    //如果元素为隐藏,则将它显现
		}else{
			$("#emergencyEquipmentType").hide();     //如果元素为显现,则将其隐藏
		}  
    });
	$('#emergencyEquipmentType').on('click','li',function(){
		pageNum=1; 
		$("#cardTypeId").val($(this).attr("name"));
		if($(this).text()){
			$("#emergencyEquipmentTypeName").text($(this).text()); 
			$(this).addClass("ZhoverCss").siblings().removeClass("ZhoverCss");//默认选中当前选项	
		}	
		$("#emergencyEquipmentType").hide(); 
		getDataByParam("消防车辆");	
	});
	$("#changeEmergencyEquipmentUnit").click(function () {
		if($("#emergencyEquipmentUnit").is(":hidden")){
			$("#emergencyEquipmentUnit").show();    //如果元素为隐藏,则将它显现
		}else{
			$("#emergencyEquipmentUnit").hide();     //如果元素为显现,则将其隐藏
		}	
    });
//	消防车辆 宽页面 消防队选择事件
	$("#changeEmergencyEquipmentUnit2").click(function () {
		if($("#emergencyEquipmentUnit2").is(":hidden")){
			$("#emergencyEquipmentUnit2").show();    //如果元素为隐藏,则将它显现
		}else{
			$("#emergencyEquipmentUnit2").hide();     //如果元素为显现,则将其隐藏
		}	
    });
	$('#emergencyEquipmentUnit').on('click','li',function(){
		pageNum=1; 
		$("#emergencyEquipKeyUnitId").val($(this).attr("name"));
		if($(this).text()){
		   $("#emergencyEquipmentUnitName").text($(this).text()); 
		   $(this).addClass("ZhoverCss").siblings().removeClass("ZhoverCss");//默认选中当前选项	
		}	
		getDataByParam("消防车辆");	
		$("#emergencyEquipmentUnit").hide();   
	});
	$("#emergencyEquipmentBtnEvent").click(function(){
		pageNum=1; 
		getDataByParam("消防车辆");
	});
	
	/*$("#changeEmergencyEquipmentType2").click(function () {
		$("#emergencyEquipmentType2").show();   
    });*/
	$('#emergencyEquipmentType2').on('click','li',function(){
		pageNum=1; 
		$("#cardTypeId2").val($(this).attr("name"));
		if($(this).text()){
			$("#emergencyEquipmentTypeName2").text($(this).text()); 
			$(this).addClass("ZhoverCss").siblings().removeClass("ZhoverCss");//默认选中当前选项	
		}	
		$("#emergencyEquipmentType2").hide(); 
		getCarInfoAjax();
	});
	/*$("#changeEmergencyEquipmentUnit2").click(function () {
		$("#emergencyEquipmentUnit2").show();   
    });*/
	$('#emergencyEquipmentUnit2').on('click','li',function(){
		pageNum=1; 
		$("#emergencyEquipKeyUnitId2").val($(this).attr("name"));
		if($(this).text()){
		   $("#emergencyEquipmentUnitName2").text($(this).text()); 
		   $(this).addClass("ZhoverCss").siblings().removeClass("ZhoverCss");//默认选中当前选项	
		}	
		getCarInfoAjax();
		$("#emergencyEquipmentUnit2").hide();   
	});
	$("#emergencyEquipmentBtnEvent2").click(function(){
		pageNum=1; 
		getCarInfoAjax();
	});
	/*消防车辆 结束*/
	
	/*物资储备点开始*/	
	//点击物资储备点的选项，展示选项
	$("#changeReserveUnit").click(function () {
		$("#materialReserveUnit").show();   
    });
	//给每个物资储备点的li绑定事件
	$('#materialReserveUnit').on('click','li',function(){
		pageNum=1; 
		$("#reservePointKeyUnitId").val($(this).attr("name"));
		if($(this).text()){
		   $("#materialReserveUnitName").text($(this).text()); 
		   $(this).addClass("ZhoverCss").siblings().removeClass("ZhoverCss");//默认选中当前选项	
		}			
        getDataByParam("物资储备点");				
		$("#materialReserveUnit").hide();
	});

	$("#materialReserveBtnEvent").click(function(){
		pageNum=1; 
		getDataByParam("物资储备点");
	});
	/*物资储备点结束*/	
	
	
	/*应急专家开始*/	
	//点击应急专家的选项，展示选项
	$("#changeExpertType").click(function () {
		if($("#expertTypeOption").is(":hidden")){
			$("#expertTypeOption").show();    //如果元素为隐藏,则将它显现
		}else{
			$("#expertTypeOption").hide();     //如果元素为显现,则将其隐藏
		} 
    });
	//给应急专家类型的li绑定事件
	$('#expertTypeOption').on('click','li',function(){
		pageNum=1; 
		$("#expertTypeId").val($(this).attr("name"));
		if($(this).text()){
		   $("#expertTypeName").text($(this).text()); 
		   $(this).addClass("ZhoverCss").siblings().removeClass("ZhoverCss");//默认选中当前选项	
		}			
		$("#expertTypeOption").hide();
        getDataByParam("应急专家");				
	});

	$("#specialistSearEvent").click(function(){
		pageNum=1; 
		getDataByParam("应急专家");
	});
	
   /*宽内容*/
	/*$("#changeExpertType2").click(function () {
		$("#expertTypeOption2").show();   
    });*/
	
	/*应急专家开始*/	
	//点击应急专家的选项，展示选项  宽内容
	$("#changeExpertType2").click(function () {
		if($("#expertTypeOption2").is(":hidden")){
			$("#expertTypeOption2").show();    //如果元素为隐藏,则将它显现
		}else{
			$("#expertTypeOption2").hide();     //如果元素为显现,则将其隐藏
		} 
    });
	
	
	//给应急专家类型的li绑定事件
	$('#expertTypeOption2').on('click','li',function(){
		pageNum=1; 
		$("#expertTypeId2").val($(this).attr("name"));
		if($(this).text()){
		   $("#expertTypeName2").text($(this).text()); 
		   $(this).addClass("ZhoverCss").siblings().removeClass("ZhoverCss");//默认选中当前选项	
		}			
		$("#expertTypeOption2").hide();
		getExpertAjax();				
	});

	$("#specialistSearEvent2").click(function(){
		pageNum=1; 
		getExpertAjax();
	});
	
	/*应急专家结束*/	
	
	
	/*应急预案  start  */
	/*应急预案搜索事件*/
	$("#reservePlanSearEvent").click(function () {
		pageNum=1; 	    
	    getDataByParam("应急预案");
    });
	$("#changePlanType").click(function () {
		if($("#reservePlanSelect").is(":hidden")){
			$("#reservePlanSelect").show();    //如果元素为隐藏,则将它显现
		}else{
			$("#reservePlanSelect").hide();     //如果元素为显现,则将其隐藏
		}   
    });
//	应急预案搜索事件 宽页面
	$("#changePlanType2").click(function () {
		if($("#reservePlanSelect2").is(":hidden")){
			$("#reservePlanSelect2").show();    //如果元素为隐藏,则将它显现
		}else{
			$("#reservePlanSelect2").hide();     //如果元素为显现,则将其隐藏
		}   
    });
	$('#reservePlanSelect').on('click','li',function(){
		pageNum=1; 
		$("#planTypeId").val($(this).attr("name"));
		if($(this).text()){
		   $("#reservePlanType").text($(this).text()); 
		   $(this).addClass("ZhoverCss").siblings().removeClass("ZhoverCss");//默认选中当前选项	
		}
		$("#reservePlanSelect").hide(); 
		getDataByParam("应急预案");				
	});
	
    //预案宽     
	$("#reservePlanSearEvent2").click(function () {
		pageNum=1; 	    
		getReservePlanAjax();
    });
	/*$("#changePlanType2").click(function () {
		$("#reservePlanSelect2").show();   
    });*/
	$('#reservePlanSelect2').on('click','li',function(){
		pageNum=1; 
		$("#planTypeId2").val($(this).attr("name"));
		if($(this).text()){
		   $("#reservePlanType2").text($(this).text()); 
		   $(this).addClass("ZhoverCss").siblings().removeClass("ZhoverCss");//默认选中当前选项	
		}
		$("#reservePlanSelect2").hide(); 
		getReservePlanAjax();				
	});
	/*应急预案 end  */
	
	/*场景 开始*/
	$('#scenebuildingname .searIcon').click(function(){
		getDataByParam("场景搭建");	
	});
	
	getAllImageMap();

	//通过ID删除场景
	function delscenebuildingByid(id){	
		params = {"id":id};
		$.ajax({
			type : "post",
			url : baseUrl+"/accidentResource/delResourceById.jhtml",
			data: JSON.stringify(params),
			dataType:"json",
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {
				if(data.data==1){
					layer.msg('删除成功', {icon: 1, time: 1000});
					getDataByParam("场景搭建");
					removeImagesByid(id);
					send(JSON.stringify({"evetype":"deleteSceneResource","id":id}));
					$("#scenebuilding_FileUpload").val("");
				}else{
					layer.msg('删除失败', {icon: 1, time: 1000});
				}
			},error: function(request) {
				layer.msg(networkErrorMsg, {time: 2000});
			}
		})
	}
	$("body").delegate('#scenebuilding_FileUpload', 'change', function(){
	//$('#scenebuilding_FileUpload').on('change',(function(){
		return scenebuilding_FileUpload_onselect();
	});
	function scenebuilding_FileUpload_onselect(){
		var _File = document.getElementById("scenebuilding_FileUpload").files[0];
		if(!/image\/\w+/.test(_File.type)) {
			layer.msg('请上传图片文件', {icon: 1,time: 2000});
			return false;
		}
		var formData = new FormData();
		formData.append("file",_File);
		formData.append("enctype","multipart/form-data");
		$.ajax({
			type: 'POST',
			url: baseUrl+'/common/uploadImg.jhtml',
			data: formData,
			processData: false,
			contentType: false,
			success:function(data){
				var params={
					filePath: data.data.url,
					fileName :data.data.name,
					type :resourceType
				}
				$.ajax({
					type : "post",
					url : "/accidentResource/addResource.jhtml",
					data: JSON.stringify(params),
					dataType:"json",
					headers: {"Content-Type": "application/json;charset=utf-8"},
					success : function(data) {
						layer.msg('上传成功', {icon: 1,time: 1000});
//							getDataByParam("场景搭建");
						// 动态追加 新增的图片信息
						if (data.data) {
							var imgHtml = getScenebuildingHtml(data.data);
							$("#scenebuildingList ul").append(imgHtml);
							
							//注册鼠标经过事件 显示删除图层
							$("#scenebuildingList ul").find('.sceneDel').hide();
							$("#scenebuildingList ul").find('li').mouseenter(function(){
								if($(this).find(".sceneDel").css("display")=="none"){
									$(this).find(".sceneDel").fadeIn("fast");
								}
							})
							$("#scenebuildingList ul").find('li').mouseleave(function(){
								if($(this).find(".sceneDel").css("display")!="none"){
									$(this).find(".sceneDel").fadeOut("fast");
								}
							})
							
							//注册删除事件
							$("#scenebuildingList ul").find('.sceneDel').click(function(){

								var id = $(this).attr("data");
								layer.confirm('是否删除该信息？', {
							         btn: ['确定','取消'] //按钮
							    },function(){
									delscenebuildingByid(id);
							    }, function(){
							    	layer.msg('已取消', {icon: 1, time: 1000});
							    });
							})
							
						} else {
							
							layer.msg("上传失败！", {time: 1000});
						}
							
					},error: function(request) {
						layer.msg("上传失败！", {time: 1000});
					}
				})
				
			}
		});
	}
	
	/*场景的详情*/
	function getScenebuildingHtml(obj){
		var html='';
		html+='<li id="'+obj.id+'">';
		html+='	<div class="sceneImg">';
		html+='	  <img src="'+baseFileUrl+obj.resourcePath+'" alt="" />';
		html+='   <div class="sceneDel" data="'+obj.id+'"><span><i></i></span></div>';
		html+=' </div>';
		html+=' <div class="sceneTxt">';
		html+='	  <span>'+obj.resourceName+'</span>';
		html+='	  <label class="custom-checkbox" id="'+obj.id+'Radio"><input id="'+obj.id+'RadioInput" class="check-zhengshe" style="display:none;" type="radio" name="sbradio" value="'+obj.id+'"><i></i></label>';
		html+=' </div>';
		html+='</li>';
		
		return html;
	}
	
	$("#sceneSearEvent").click(function () {	
		pageNum=1; 
		getDataByParam("场景搭建");
    });
	
	/*场景结束*/
		
	
	/*生产装置 start */
	$("#deviceTypeSearEvent").click(function () {	
		pageNum=1; 
		getDataByParam("生产装置");
    });
	$("#changeDeviceType").click(function () {
		if($("#deviceTypeSelect").is(":hidden")){
			$("#deviceTypeSelect").show();    //如果元素为隐藏,则将它显现
		}else{
			$("#deviceTypeSelect").hide();     //如果元素为显现,则将其隐藏
		}
    });
	$('#deviceTypeSelect').on('click','li',function(){
		pageNum=1; 
		$("#deviceTypeId").val($(this).attr("name"));
		if($(this).text()){
		   $("#deviceType").text($(this).text()); 
		   $(this).addClass("ZhoverCss").siblings().removeClass("ZhoverCss");//默认选中当前选项	
		}	
		$("#deviceTypeSelect").hide();   
		getDataByParam("生产装置");				
	});
	/*生产装置 end */
	
	
	/*
	 * 情况左侧菜单内容数据
	 * */
	function clearMenuDetail(){
		$("#manageUnitList").html("");
	    $("#keyUnitPage").html("");
	    $("#soureManageList").html("");
		$("#fireWaterSourcePage").html("");
	    $("#emergencyTeam").html("");
		$("#emergencyTeamPage").html("");
		$("#dangerChemicPage").html("");
	    $("#dangerChemicStoreData").html("");
	    $("#emergencyTeam").html("");
		$("#emergencyTeamPage").html("");
		$("#emergencyEquipment").html("");
		$("#emergencyEquipmentPage").html("");
		$("#materialReserve").html("");			
	    $("#reservePointPage").html("");
	    $("#specialistData").html("");		
	    $("#expertPage").html("");
	    $("#reservePlanList").html("");		
	    $("#plansPage").html("");
	    $("#deviceList").html("");		
	    $("#devicePage").html("");
	    $("#incidentPlanList").html("");
	    $("#scenebuildingList ul").html("");
		$("#fireBrigade").html("");
		$("#fireBrigadePage").html("");
		$("#keyPartsList").html("");
		$("#keyPartsPage").html("");
		$("#accidentCaseList").html("");
		$("#accidentCasePage").html("");
		$("#emergencyEquipment2").html("");
		$("#emergencyEquipmentPage2").html("");
		$("#specialistData2").html("");
		$("#expertPage2").html("");
		$("#reservePlanList2").html("");
		$("#plansPage2").html("");
		$("#keyUnitPage2").html("");
		$("#manageUnitList2").html("");
		$("#keyPartsList2").html("");
		$("#keyPartsPage2").html("");
		$("#soureManageList2").html("");
		$("#fireWaterSourcePage2").html("");
	}
	/*根据参数搜索js中的相对应的数据 参数(类型,关键字,所属单位)*/
	function getDataByParam(type){
	    var html="";	    
//清空所有list
	    clearMenuDetail();
	    // 隐藏弹窗
	    $('#materialSeeDiv').css("display","none");
	    
	    //情况所有的list
		if(type=="重点单位"){
			var keyWord =$("#unitKeyword").val();
			var zdswFireBrigadeId =$("#zdswFireBrigadeId").val();
			var params={"name":keyWord,"fireBrigadeId":zdswFireBrigadeId,"pageNum":pageNum,"pageSize":pageSize};	
			if(accidentCompany.lonlat4326){
				params.lon=accidentCompany.lonlat4326[0];
				params.lat=accidentCompany.lonlat4326[1];
			}
		    $.ajax({
				type : "post",
				url : baseUrl+"/webApi/qryList.jhtml",
				data: JSON.stringify(params),
				dataType:"json",
				headers: {"Content-Type": "application/json;charset=utf-8"},
				success : function(data) {	
					if(data.data&&data.data.length>0){
						var currentPage = Number(data.current);
						var totalPages = Number(data.pages);
						if(currentPage>=1 && totalPages>1){
							generatePagination(currentPage,totalPages,"keyUnitPage");
						}
						for(j = 0,len=data.data.length; j < len; j++) {
							html+=getUnitHtml(data.data[j]);
						}
					}else{
						html+="<li class='NoTime'>暂无数据</li>";
					}
				
					$("#manageUnitList").append(html);	
				},error: function(request) {
					layer.msg(networkErrorMsg, {time: 2000});
                }
			});
		}else if(type=="重点部位"){
			var keyUnitId=$("#selectKeyPartsOfUnit").val();//1是消防水源 2是消防设备
			var keyPartsKeyword =$("#keyPartsKeyword").val();
			var params={"keyUnitId":keyUnitId,"name":keyPartsKeyword,"pageNum":pageNum,"pageSize":pageSize};
			var url = baseUrl+"/webApi/qryKeyPartsList.jhtml";
		    $.ajax({
				type : "post",
				url :url,
				data: JSON.stringify(params),
				dataType:"json",
				headers: {"Content-Type": "application/json;charset=utf-8"},
				success : function(data) {
							if(data.httpCode==200){
					    		if(data){
					    			if(data.data&&data.data.length>0){
						    			var currentPage = Number(data.current);
										var totalPages = Number(data.pages);
										if(currentPage>=1 && totalPages>1){
											generatePagination(currentPage,totalPages,"keyPartsPage");
										}
										if(data.data.length>0){
											for(j = 0,len=data.data.length; j < len; j++) {
												html+=getKeyPartsHtml(data.data[j]);
											}
										}
							}else{
								html+="<li class='NoTime'>暂无数据</li>";
							}
							$("#keyPartsList").append(html);	
							//注册展开收起事件
//							$("#keyPartsList").find("li div.openMsg").click(function(e){
//								$(this).toggleClass("upMsg");
//								$(this).siblings(".listFrom").toggle("fast");
//								stopBubble(e); 
//							});

						}
			    	}else {
			    		layer.msg(data.msg, {time: 1000});
			    	}
				},error: function(request) {
					layer.msg(networkErrorMsg, {time: 2000});
	            }
			});
		}else if(type=="典型火灾"){
			var keyword =$("#accidentCaseKeyword").val();
			var params={"name":keyword,"pageNum":pageNum,"pageSize":pageSize};
			var url = baseUrl+"/webApi/qryAccidentCaseList.jhtml";
		    executeAjax(url,params,function(data){
				if(data.httpCode==200){
		    		if(data){
		    			if(data.data&&data.data.length>0){
		    				var currentPage = Number(data.current);
							var totalPages = Number(data.pages);
							if(currentPage>=1 && totalPages>1){
								generatePagination(currentPage,totalPages,"accidentCasePage");
							}
							if(data.data.length>0){
								for(j = 0,len=data.data.length; j < len; j++) {
									html+=getAccidentCaseHtml(data.data[j]);
								}
							}
		    			}else{
		    				html+="<li class='NoTime'>暂无数据</li>";
		    			}
		    			
						$("#accidentCaseList").append(html);	
						//注册展开收起事件
						$("#accidentCaseList").find("li div.openMsg").click(function(e){
							$(this).toggleClass("upMsg");
							$(this).siblings(".listFrom").toggle("fast");
							stopBubble(e); 
						});

					}
		    	}else {
		    		layer.msg(data.msg, {time: 1000});
		    	}
		    });
	
		}else if(type=="消防水源"){
			var soureType=$("#soureTypeId").val();//1是消防水源 2是消防设备
			var keytype=$("#soureKeyTypeId").val();
			var soureKeyUnitId=$("#soureKeyUnitId").val();
			var soureInputContent =$("#soureManageIn").val();
			
			var params={"name":soureInputContent,"keyUnitId":soureKeyUnitId,"type":keytype,"soureType":soureType,"pageNum":pageNum,"pageSize":pageSize};
			if(accidentCompany.lonlat4326){
				params.lon=accidentCompany.lonlat4326[0];
				params.lat=accidentCompany.lonlat4326[1];
			}
			$.ajax({
				type : "post",
				url : baseUrl+"/webApi/qryFireWaterSource.jhtml",
				data: JSON.stringify(params),
				dataType:"json",
				headers: {"Content-Type": "application/json;charset=utf-8"},
				success : function(data) {	
					if(data.data&&data.data.length>0){
						var currentPage = Number(data.current);
						var totalPages = Number(data.pages);
						var features = [];
						if(currentPage>=1 && totalPages>1){
							generatePagination(currentPage,totalPages,"fireWaterSourcePage");
						}
						for(j = 0,len=data.data.length; j < len; j++) {
							html+=getSoureManageUnitHtml(data.data[j],soureType);
						}
					}else{
						html+="<li class='NoTime'>暂无数据</li>";
					}
					
					$("#soureManageList").append(html);
					
				},error: function(request) {
					layer.msg(networkErrorMsg, {time: 2000});
                }
			});
		}else if(type=="危化品库"){
			var keyWord=$("#dangerChemicStoreIn").val();
			var params={"name":keyWord,"pageNum":pageNum,"pageSize":pageSize};
		    $.ajax({
				type : "post",
				url : baseUrl+"/webApi/qryDangerChemic.jhtml",
				data: JSON.stringify(params),
				dataType:"json",
				headers: {"Content-Type": "application/json;charset=utf-8"},
				success : function(data) {	
					if(data.data&&data.data.length>0){
						var currentPage = Number(data.current);
						var totalPages = Number(data.pages);
						if(currentPage>=1 && totalPages>1){
							generatePagination(currentPage,totalPages,"dangerChemicPage");
						}
						for(j = 0,len=data.data.length; j < len; j++) {
							html+=getDangerChemicHtml(data.data[j]);
						}
					}else{
						html+="<li class='NoTime'>暂无数据</li>";
					}
					
					$("#dangerChemicStoreData").append(html);
				},error: function(request) {
					layer.msg(networkErrorMsg, {time: 2000});
                }
			});
			
		}else if(type=="应急队伍"){
			var keyWord=$("#emergencyTeamIn").val();
			var params={"name":keyWord,"pageNum":pageNum,"pageSize":pageSize};
			if(accidentCompany.lonlat4326){
				params.lon=accidentCompany.lonlat4326[0];
				params.lat=accidentCompany.lonlat4326[1];
			}
		    $.ajax({
				type : "post",
				url : baseUrl+"/webApi/qryTeams.jhtml",
				data: JSON.stringify(params),
				dataType:"json",
				headers: {"Content-Type": "application/json;charset=utf-8"},
				success : function(data) {					
					var currentPage = Number(data.current);
					var totalPages = Number(data.pages);
					var features = [];
					if(currentPage>=1 && totalPages>1){
						generatePagination(currentPage,totalPages,"emergencyTeamPage");
					}
					for(j = 0,len=data.data.length; j < len; j++) {
						html+=getEmergencyTeamHtml(data.data[j]);
						//features.push(createFeature(data.data[j]));
					}
					//leftResultAddToMap(features,'yjdw','showLeftLayer');
					$("#emergencyTeam").append(html);

					//注册展开收起事件
					$("#emergencyTeam").find("li div.openMore").click(function(e){
						$(this).toggleClass("takeupMore");
						$(this).siblings(".listFrom").toggle("fast");
						stopBubble(e); 
					});

				},error: function(request) {
					layer.msg(networkErrorMsg, {time: 2000});
                }
			});
		}else if(type=="消防车辆"){
			var cardTypeId=$("#cardTypeId").val();
			var keyUnitId=$("#emergencyEquipKeyUnitId").val();
			var keyWord=$("#emergencyEquipmentIn").val();
			var params={"name":keyWord,"cardTypeId":cardTypeId,"fireBrigadeId":keyUnitId,"pageNum":pageNum,"pageSize":pageSize};
		    $.ajax({
				type : "post",
				url : baseUrl+"/webApi/qryFireEngine.jhtml",
				data: JSON.stringify(params),
				dataType:"json",
				headers: {"Content-Type": "application/json;charset=utf-8"},
				success : function(data) {	
					if(data.data&&data.data.length>0){
						var currentPage = Number(data.current);
						var totalPages = Number(data.pages);
						if(currentPage>=1 && totalPages>1){
							generatePagination(currentPage,totalPages,"emergencyEquipmentPage");
						}
						for(j = 0,len=data.data.length; j < len; j++) {
							html+=getEmergencyEquipmentHtml(data.data[j]);
						}
					}else{
						html+="<li class='NoTime'>暂无数据</li>";
					}
					
					$("#emergencyEquipment").append(html);
				},error: function(request) {
					layer.msg(networkErrorMsg, {time: 2000});
                }
			});
		}else if(type=="物资储备点"){
			var keyWord =$("#materialReserveIn").val();
			var keyUnitId =$("#reservePointKeyUnitId").val();
			var params={"name":keyWord,"keyUnitId":keyUnitId,"pageNum":pageNum,"pageSize":pageSize};
			if(accidentCompany.lonlat4326){
				params.lon=accidentCompany.lonlat4326[0];
				params.lat=accidentCompany.lonlat4326[1];
			}
		    $.ajax({
				type : "post",
				url : baseUrl+"/webApi/qryReservePoint.jhtml",
				data: JSON.stringify(params),
				dataType:"json",
				headers: {"Content-Type": "application/json;charset=utf-8"},
				success : function(data) {	
					if(data.data&&data.data.length>0){
						var currentPage = Number(data.current);
						var totalPages = Number(data.pages);
						var features = [];
						if(currentPage>=1 && totalPages>1){
							generatePagination(currentPage,totalPages,"reservePointPage");
						}
						for(j = 0,len=data.data.length; j < len; j++) {
							html+=getReservePointHtml(data.data[j]);
						}
					}else{
						html+="<li class='NoTime'>暂无数据</li>";
					}
					
					$("#materialReserve").append(html);			
				},error: function(request) {
					layer.msg(networkErrorMsg, {time: 2000});
                }
			});
		}else if(type=="应急专家"){
			var keyWord=$("#specialistDataInput").val();	
			var typeId=$("#expertTypeId").val();	
			var params={"name":keyWord,"typeId":typeId,"pageNum":pageNum,"pageSize":pageSize};
		    $.ajax({
				type : "post",
				url : baseUrl+"/webApi/qryExpertListPage.jhtml",
				data: JSON.stringify(params),
				dataType:"json",
				headers: {"Content-Type": "application/json;charset=utf-8"},
				success : function(data) {	
					if(data.data&&data.data.length>0){
						var currentPage = Number(data.current);
						var totalPages = Number(data.pages);
						if(currentPage>=1 && totalPages>1){
							generatePagination(currentPage,totalPages,"expertPage");
						}
						for(j = 0,len=data.data.length; j < len; j++) {
							html+=getSpecialistHtml(data.data[j],j);
						}
					}else{
						html+="<li class='NoTime'>暂无数据</li>";
					}
					
					$("#specialistData").append(html);	

					//注册展开收起事件
					$("#specialistData").find("li div.openMore").click(function(){
						$(this).toggleClass("takeupMore");
						$(this).siblings(".listFrom").toggle("fast");
					});
					 /*消防车辆信息事件*/
				    var changeType = $('#specialistData').find('.agencyHd');
					  $.each(changeType, function () {
					    $(this).on('click', function () {
					      $(this).next().slideToggle(100);
					    });
					})
						
				},error: function(request) {
					layer.msg(networkErrorMsg, {time: 2000});
                }
			})
		}else if(type=="应急预案"){
			var keyWord=$("#reservePlanInput").val();	
			var typeId=$("#planTypeId").val();	
			var params={"name":keyWord,"typeId":typeId,"pageNum":pageNum,"pageSize":pageSize};
		    $.ajax({
				type : "post",
				url : baseUrl+"/webApi/qryPlansPage.jhtml",
				data: JSON.stringify(params),
				dataType:"json",
				headers: {"Content-Type": "application/json;charset=utf-8"},
				success : function(data) {
					if(data.data&&data.data.length>0){
						var currentPage = Number(data.current);
						var totalPages = Number(data.pages);
						if(currentPage>=1 && totalPages>1){
							generatePagination(currentPage,totalPages,"plansPage");
						}
						if(data.data){
							for(j = 0,len=data.data.length; j < len; j++) {
								html+=getReservePlanHtml(data.data[j]);
							}
						}
					}else{
						html+="<li class='NoTime'>暂无数据</li>";
					}
					
					$("#reservePlanList").append(html);	
				},error: function(request) {
					layer.msg(networkErrorMsg, {time: 2000});
                }
			})
		}else if(type=="生产装置"){
			var keyWord=$("#deviceTypeInput").val();	
			var typeId=$("#deviceTypeId").val();	
			var params={"name":keyWord,"typeId":typeId,"pageNum":pageNum,"pageSize":pageSize};
			if(accidentCompany.lonlat4326){
				params.lon=accidentCompany.lonlat4326[0];
				params.lat=accidentCompany.lonlat4326[1];
			}
		    $.ajax({
				type : "post",
				url : baseUrl+"/webApi/qryEquipmentList.jhtml",
				data: JSON.stringify(params),
				dataType:"json",
				headers: {"Content-Type": "application/json;charset=utf-8"},
				success : function(data) {
					if(data.data&&data.data.length>0){
						var currentPage = Number(data.current);
						var totalPages = Number(data.pages);
						if(currentPage>=1 && totalPages>1){
							generatePagination(currentPage,totalPages,"devicePage");
						}
						for(j = 0,len=data.data.length; j < len; j++) {
							html+=getDeviceTypeHtml(data.data[j]);
						}
					}else{
						html+="<li class='NoTime'>暂无数据</li>";
					}
					
					$("#deviceList").append(html);	
				},error: function(request) {
					layer.msg(networkErrorMsg, {time: 2000});
                }
			})
		}else if(type=="救援记录"){
			var params="";
			if(currentRoomId){
				params=currentRoomId;//当前房间id todo （这是个动态id）
			}
		    $.ajax({
				type : "post",
				url : baseUrl+"/webApi/getIncidentRecordByAccident.jhtml",
				data: JSON.stringify(params),
				dataType:"json",
				headers: {"Content-Type": "application/json;charset=utf-8"},
				success : function(data) {
					for(j = 0,len=data.data.length; j < len; j++) {
						html+=getIncidentRecordHtml(data.data[j],j);
					}
					if(data.data.length<1){
						html+="该事故没有处置记录！";
					}
					$("#incidentPlanList").append(html);	
				},error: function(request) {
					layer.msg(networkErrorMsg, {time: 2000});
                }
			})
		}else if(type=="场景搭建"){
			var name = $("#sceneFileName").val();	
			params = {"name":name,type:resourceType};
		    $.ajax({
				type : "post",
				url : baseUrl+"/accidentResource/queryResource.jhtml",
				data: JSON.stringify(params),
				dataType:"json",
				headers: {"Content-Type": "application/json;charset=utf-8"},
				success : function(data) {
					if(data.data&&data.data.length>0){
						for(j = 0,len=data.data.length; j < len; j++) {
							html+=getScenebuildingHtml(data.data[j]);
						}
					}
					
					//加载HTML
					$("#scenebuildingList ul").append(html);
					//注册鼠标经过事件 显示删除图层
					$("#scenebuildingList ul").find('.sceneDel').hide();
					$("#scenebuildingList ul").find('li').mouseenter(function(){
						if($(this).find(".sceneDel").css("display")=="none"){
							$(this).find(".sceneDel").fadeIn("fast");
						}
					})
					$("#scenebuildingList ul").find('li').mouseleave(function(){
						if($(this).find(".sceneDel").css("display")!="none"){
							$(this).find(".sceneDel").fadeOut("fast");
						}
					})
					
					//注册删除事件
					$("#scenebuildingList ul").find('.sceneDel').click(function(){

						var id = $(this).attr("data");
						layer.confirm('是否删除该信息？', {
					         btn: ['确定','取消'] //按钮
					    },function(){
							delscenebuildingByid(id);
					    }, function(){
					    	layer.msg('已取消', {icon: 1, time: 1000});
					    });
					})
				},error: function(request) {
					layer.msg(networkErrorMsg, {time: 2000});
                }
			})
		}else if(type=="消防队伍"){
			var keyWord=$("#fireBrigadeIn").val();
			var params={"name":keyWord,"pageNum":pageNum,"pageSize":pageSize};
			if(accidentCompany.lonlat4326){
				params.lon=accidentCompany.lonlat4326[0];
				params.lat=accidentCompany.lonlat4326[1];
			}
		    $.ajax({
				type : "post",
				url : baseUrl+"/webApi/qryFireBrigade.jhtml",
				data: JSON.stringify(params),
				dataType:"json",
				headers: {"Content-Type": "application/json;charset=utf-8"},
				success : function(data) {
					if(data.data&&data.data.length>0){
						var currentPage = Number(data.current);
						var totalPages = Number(data.pages);
						var features = [];
						if(currentPage>=1 && totalPages>1){
							generatePagination(currentPage,totalPages,"fireBrigadePage");
						}
						for(j = 0,len=data.data.length; j < len; j++) {
							html+=getFireBrigadeHtml(data.data[j]);
							//features.push(createFeature(data.data[j]));
						}
						//leftResultAddToMap(features,'yjdw','showLeftLayer');
						
					}else{
						html+="<li class='NoTime'>暂无数据</li>";
					}
					$("#fireBrigade").append(html);

					//注册展开收起事件
					$("#fireBrigade").find("li div.openMore").click(function(e){
						$(this).toggleClass("takeupMore");
						$(this).siblings(".foamMsg").toggle("fast");
						stopBubble(e); 
					});

				},error: function(request) {
					layer.msg(networkErrorMsg, {time: 2000});
                }
			});
		}else if(type==""){

        }
	}
	

	//分页
	function generatePagination(cp,tp,keyId){
		this.currentPage = Number(cp);
		this.totalPages = Number(tp);
		var jump_prev=currentPage>1?(currentPage-1):currentPage;
		var jump_next=(currentPage<totalPages)?(currentPage+1):totalPages;
		var pageList ="<a name='findPage' pageNum='"+jump_prev+"' class='front'><span></span></a>";
		var page_start = Number(currentPage)/5;
		if(page_start>1){
			pageList = getPageHtml(page_start*5-2,page_start*5+3,totalPages,pageList);
		}else{
			pageList = getPageHtml(1,6,totalPages,pageList);
		}
		pageList +="<a name='findPage' pageNum='"+jump_next+"' class='after'><span></span></a>";
		$("#"+keyId).html(pageList);
		$("#"+keyId).find("[name=findPage]").on("click",function(){
		   pageNum=$(this).attr("pageNum");  //获取链接里的页码  
		   if(keyId=="keyUnitPage"){
			   getDataByParam("重点单位");	
		   }else if(keyId=="fireWaterSourcePage"){
			   getDataByParam("消防水源");
		   }else if(keyId=="dangerChemicPage"){
			   getDataByParam("危化品库");
		   }else if(keyId=="emergencyTeamPage"){
			   getDataByParam("应急队伍");
		   }else if(keyId=="emergencyEquipmentPage"){
			   getDataByParam("消防车辆");
		   }else if(keyId=="reservePointPage"){
			   getDataByParam("物资储备点");
		   }else if(keyId=="expertPage"){
			   getDataByParam("应急专家");
		   }else if(keyId=="plansPage"){
			   getDataByParam("应急预案");
		   }else if(keyId=="devicePage"){
			   getDataByParam("生产装置");
		   }else if(keyId=="fireBrigadePage"){
			   getDataByParam("消防队伍");
		   }else if(keyId=="keyPartsPage"){
			   getDataByParam("重点部位");
		   }
		});  
	
	}

	function getPageHtml(start,end,totalPages,pageList){
		for(var i=start;i<=end;i++){
			if(i<=totalPages){
				var temp =""
				if(i==currentPage){
					temp="<a name='findPage' pageNum='"+i+"' class='onlink'>"+i+"</a>";
				}else{
					temp="<a name='findPage' pageNum='"+i+"'>"+i+"</a>";
				}
				pageList+=temp;
			}
		}
		return pageList;
	}
	
	/*救援记录拼接html*/
	function getIncidentRecordHtml(obj,num){
		if(obj.content){
			 var content = eval('(' + obj.content + ')');
		  }
	      var html="<div class='succor' onClick='getIncidentRecordDetail("+ obj.createTime+")'>"; 
	      html+="<div class='succorHd'><i></i>步骤:"+num+1+"</div>";
	      html+="<div class='sucMsg'>"; 
	      html+="<div class='ca cf sucMsgli'><span class='yearIcon'></span><div class='yearTime'>"+timestampToTime(obj.createTime)+"</div></div>";
	      html+="<div class='ca cf sucMsgli'>";		
	      html+="<div class='sucMsgTxt'>";
			if(obj.type==1){			
						if(obj.content){
							html+="<p><span class='green'>"+content.previousName+"</span>把指挥权移交给<span class='green'>"+content.name+"</span></p>";
						}
				}else if(obj.type==2){
						if(obj.content){
							html+="<p><span class='green'>"+content.director+"</span> 执行<i class='red'>标会</i>命令</p>"+content.action;														
						}
				}else if(obj.type==3){
						if(obj.content){
							html+="<p><span class='green'>"+content.director+"</span> 加入了房间<i class='yellow'>"+content.room+"</i>语音组</p>";
						}
				}else if(obj.type==4){ 
						html+="<p><span class='green'>"+content.director+"</span>调用<i class='yellow'>"+content.actionDesc+"</i></p>";
				}else if(obj.type==5){ 
						if(obj.content){
							html+="<p><span class='green'>"+content.director+"</span> 加入了房间<i class='yellow'>"+content.room+"</i></p>";
						}
				}else if(obj.type==6){ 
						html+="<p>事故点以确认，在GIS地里信息系统经纬度坐标：<i class='brown'>（经纬度）</i></p>";
						html+="<p>距地面高度<i class='brown'>高度取值</i></p>";
						html+="<p>事故类型<i class='brown'>（事故类型）</i></p>";
				}else if(obj.type==7){ 
						if(obj.content){
							html+="<p><span class='green'>"+content.director+"</span>发布发布向作战人员发布消息命令"+content.command+"</p>";	
						}
				}else if(obj.type==8){ 
					if(obj.content){
						html+="<p><span class='green'>"+content.director+"</span>摆放一辆消防车："+content.action+"</p>";	
					}
				}else if(obj.type==9){ 
					if(obj.content){
						html+="<p><span class='green'>"+content.director+"</span>执行一个标注:"+content.action+"</p>";	
					}
			   }
			html+="</div>";
			html+="</div>";
			html+="</div>";
		return html;
	}
	
	/**
	 * 判断是否为空
	 * @param value
	 * @returns
	 */
	function isEmptyOrNo(value){
		if(value){
			return value;
		}else{
			return "";
		}
	}
	
	function timestampToTime(timestamp) {
//      var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
		var date = new Date(timestamp);
		Y = date.getFullYear() + '-';
      M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
      D = date.getDate() + ' ';
      h = date.getHours() + ':';
      m = date.getMinutes() + ':';
      s = date.getSeconds();
      return Y+M+D+h+m+s;
  }
	
	/*拼接重点单位html*/
	function getUnitHtml(obj){
		obj.type='zddw';
	    var html="<li onclick='gotoPointLocation("+ JSON.stringify(obj)+")'><div class='listHd'><i class='address'></i>"+obj.name+"";
	        if(obj.distances){
			   html+="<span style='color: #ff731e;font-weight: bolder;'>("+obj.distances+"千米)</span>";
		    }
		    html+="</div>"; 
			html+="<div class='listFrom'>";
			html+="<p>"+obj.desc+"</p>"; 
			html+="<p><span class='color3'>辖区消防队：</span><span>"+obj.fireBrigadeName+"</span></p>";
			html+="<p><span class='color3'>消防管理员：</span><span>"+obj.admin+"("+obj.tel+")</span></p>";
			// html+="<p><span class='color3'>供水能力：</span><span>"+obj.waterSupply+"</span></p>";
			html+="</div>";
			html+="<div class='unitDetails' onclick='unitDetail("+ JSON.stringify(obj.id) +")'></div>";
			html+="</li>"; 
		return html;
   }

    /*拼接重点部位html*/
	function getKeyPartsHtml(obj){
	    var html="<li ><div class='listHd ellipsis'><i class='address'></i>"+obj.name+"</div>";
			html+="<div class='listFrom'>";
			html+="<p><span class='color3'>所属重点单位：</span><span>"+obj.keyUnitName+"</span></p>";
			html+="<p><span class='color3'>描述：</span><span>"+obj.remark+"</span></p>";
			html+="</div>";
			html+="<div class='unitDetails' onClick='keyPartsDetail("+ JSON.stringify(obj.id) +")'></div>";
			html+="</li>"; 
		return html;
	}
	
	/*拼接事故案例html*/
	function getAccidentCaseHtml(obj){
	    var html="<li><div class='listHd ellipsis'><i class='address'></i>"+obj.name+"</div>";
		html+="<div class='listFrom'>";		
		html+="<p><span class='color3'>时间：</span><span>"+(obj.time?new Date(obj.time).Format("yyyy-MM-dd"):"")+"</span></p>";
		html+="<p><span class='color3'>类型：</span><span>"+obj.typeName+"</span></p>";
		html+="<p><span class='color3'>描述：</span><span>"+obj.desc+"</span></p>";
		html+="<div class='listFrom' style='padding: 0;'>";
		html+="<div class='typeHeadPlan'><span>事故案例信息</span></div>";
		if(obj.fileList.length>0){
			for(var i=0;i<obj.fileList.length;i++){
				var curr =obj.fileList[i];
				html+="<div class='ca cf'>";
				html+="<div class='fl firedangerMsgHd'><i></i><span class='color3'>"+curr.name+"</span></div>";
				html+="<div class='fr seeDiv'>" ;
				if(curr.swfUrl!=null&&curr.swfUrl!=""){
					html+="<span onClick='showAccidentCaseFile(\""+curr.name+"\",\""+curr.swfUrl+"\")'>查看</span>" ;
				}else{
					html+="<a href='javascript:void(0);' onClick='showBigImg(\""+baseFileUrl+curr.url+"\")'>查看</a>" ;
				}
				html+="<span onClick='downLoadFile(\""+curr.url+"\",\""+curr.name+"\")'>下载</span>";
				html+="</div>"; 
				html+="</div>";
			}
		}
		html+="</div>"; 
		html+="</div>"; 
		html+="</li>"; 
	    return html;
	}
	
	 /*拼接消防水源类型html*/
	 function GetManageSoureHtml(obj){
		//obj.type='xfsy';
		var html='<li value="" class="ZhoverCss">请选择类型</li>';  
		obj.data.forEach(function(data){
			html+='<li name="'+data.id+'">'+data.name+'</li>';
		})
		return html;
	}	
	 /*拼接消防水源html*/
	function getSoureManageUnitHtml(obj,type){
		if(type==1){
			obj.type='xfs';
		}else{
			obj.type='xfsy';
		}
		var html="<li onclick='gotoPointLocation("+ JSON.stringify(obj)+")'><div class='listHd'><i class='address waterdrop firewater native'></i>"+obj.name+"";  
		    if(obj.distances){
			   html+="<span style='color: #ff731e;font-weight: bolder;'>("+obj.distances+"千米)</span>";
		    }
		    html+="</div>"; 
		    html+="<div class='listFrom'>";
			html+="<p><span class='color3'>类型：</span><span>"+obj.typeName+"</span></p>";
			html+="<p><span class='color3'>所属单位：</span><span>"+obj.keyUnitName+"</span></p>";
			if(obj.goodUse){
				html+="<p><span class='color3'>是否好用：</span><span>好用</span></p>";
			}else{
				html+="<p><span class='color3'>是否好用：</span><span>不好用</span></p>";
			}
			if(type==1){
				html+="<p><span class='color3'>地址：</span><span>"+obj.addr+"</span></p>";
			}
			if(obj.reserves){
				html+="<p><span class='color3'>数量：</span><span>"+obj.reserves+"</span></p>";
			}else{
				html+="<p><span class='color3'>数量：</span><span>无</span></p>";
			}
			if(obj.flow){
				html+="<p><span class='color3'>参数：</span><span>"+obj.flow+"</span></p>";
			}else{
				html+="<p><span class='color3'>参数：</span><span>无</span></p>";
			}
			
			if(type==1){
				if(obj.surveyTime&&obj.surveyTime != ""){
					html+="<p><span class='color3'>勘查时间：</span><span>"+timestampToStr(obj.surveyTime)+"</span></p>";
				}
				if(obj.surveyor&&obj.surveyor!=""){
					html+="<p><span class='color3'>勘查人：</span><span>"+obj.surveyor+"</span></p>";
				}
				if(obj.verifier&&obj.verifier!=""){
					html+="<p><span class='color3'>核查人：</span><span>"+obj.verifier+"</span></p>";
				}
				if(obj.waterSourcePointHead && obj.waterSourcePointHead!=null){
					html+="<p><span class='color3'>水源点负责人：</span><span>"+obj.waterSourcePointHead+"</span></p>";
				}
				if(obj.phone && obj.phone!=""){
					html+="<p><span class='color3'>电话：</span><span>"+obj.phone+"</span></p>";
				}
				if(obj.jurisdictionUnit && obj.jurisdictionUnit != ""){
					html+="<p><span class='color3'>管辖单位：</span><span>"+obj.jurisdictionUnit+"</span></p>";
				}
				
			}
			html+="</div>"; 
			html+="</li>";
		return html;
	}
	
	/*危化品库拼接html*/
	function getDangerChemicHtml(obj){
		var html="<li>"; 
		html+="<div class='china ca cf'>"; 
		html+="<div class='chinaBar fl'><span>中文名</span></div>"; 
		html+="<div class='chinaName fl'>"+obj.cName+"</div>"; 
		html+="</div>"; 
		html+="<div class='dgMsg'>"; 
		html+="<p class='ca cf'>"; 
		html+="<label class='dgMsg-lab fl'>英文名：</label>"; 
	    html+="<span class='dgMsg-span fl'>"+obj.eName+"</span>"; 
		html+="</p>"; 
		html+="<p class='ca cf'>"; 
		html+="<label class='dgMsg-lab fl'>UN号：</label>"; 
		html+="<span class='dgMsg-span fl'>"+obj.unNum+"</span>"; 
		html+="</p>"; 
		html+="<p class='ca cf'>"; 
		html+="<label class='dgMsg-lab fl'>CAS：</label>"; 
		html+="<span class='dgMsg-span fl'>"+obj.casNum+"</span>"; 
	    html+="</p>"; 
	    html+="<p class='ca cf'>"; 
	    html+="<label class='dgMsg-lab fl'>危险性：</label>"; 
	    html+="<span class='dgMsg-span fl'>"+obj.typeName+"</span>"; 
	    html+="</p>"; 
	    html+="</div>"; 
	    if(obj.msdsPath){
	    	html+="<div class='gdBtn ca cf'>"; 
	 	    html+="<a href='javascript:viewDoc(\"" + obj.id_ + "\");' class='see'><i class='eye'></i>SMDS文档</a>"; 
//	 	    html+="<a href="+obj.msdsPath+"><i class='load'></i>下载</a>"; 
	 	    html+="</div>"; 
	    }
	    html+="<a class='unitDetails fr' style='cursor:pointer'  onclick='getDetail("+ obj.id +")'></a>";
	    html+="</li>"; 
	    return html;
	}

	//应急队伍
	function getEmergencyTeamHtml(obj){
		obj.type='yjdw';
	    var html="<li onclick='gotoPointLocation("+ JSON.stringify(obj)+")'><div class='listHd'><i class='address'></i>"+obj.name+"";  
		   if(obj.distances){
			   html+="<span style='color: #ff731e;font-weight: bolder;'>("+obj.distances+"千米)</span>";
		   }
		    html+="</div>";
			html+="<div class='listFrom'>";
			html+="<p><span class='color3'>负责人：</span><span>"+obj.charge+"("+obj.phone+")</span></p>";
			html+="<p><span class='color3'>地址：</span><span>"+obj.position+"</span></p>";
			html+="<div class='listFrom' style=' padding:0; display:none;'><p>";
			html+="<span class='color3'>规模：</span><span>"+obj.scale+"</span></p><p>";
			html+="<span class='color3'>性质：</span><span>"+obj.properties+"</span></p></div>";
			html+="<div class='openMore'></div>";
			html+="</div>"; 
			html+="</li>"; 
		return html;
	}

	//消防车辆拼接html
	function getEmergencyEquipmentHtml(obj){
		obj.type='yjzb';
	    var html="<li>";
	        html+="<div class='listHd ellipsis'><i class='address'></i>"+obj.name+"</div>"; 
	        html+="<div onclick='vehicleEquipment(\""+obj.id+"\")' style='width:13px; height:14px; display:inline-block; background: url("+baseUrl+"/images/detailsICON.png) no-repeat;  position: absolute; right: 5px; top:8px;'></div>";
	        html+="<div class='ca cf'>";
	        html+="<div class='listFrom ca cf newListFrom'>";
			html+="<p class='fl'><span class='color3'>车辆牌照号:</span><span>"+obj.plateNumber+"</span></p>";
			html+="<p class='fl'><span class='color3'>所属消防队:</span><span>"+obj.fireBrigadeName+"</span></p>";
			html+="<p class='fl'><span class='color3'>车辆类型:</span><span>"+obj.dicName+"</span></p>";			
			html+="</div>";			
			html+="<div>";
			html+="</li>";
		return html;
	}
			
	//物资储备点  todogetWZCBDDetailForMark
	function getReservePointHtml(obj){
		obj.type='wzcbd';
	    var html="<li onClick='gotoPointLocation("+ JSON.stringify(obj)+")'><div class='listHd ellipsis'><i class='address'></i>"+obj.name+""; 
	        if(obj.distances){
			   html+="<span style='color: #ff731e;font-weight: bolder;'>("+obj.distances+"千米)</span>";
		    }
	        html+="</div>"; 
			html+="<div class='listFrom'>"; 
			html+="<p><span class='color3'>物资种类数量：</span><span>"+obj.materialCount+"</span></p>";
			html+="<p><span class='color3'>所属单位：</span><span>"+obj.keyUnitName+"</span></p>";
			html+="<p><span class='color3'>性质：</span><span>"+obj.properties+"</span></p>";
			html+="<p><span class='color3'>电话：</span><span>"+obj.chargePhone+"</span></p>";
			html+="<p><span class='color3'>负责人：</span><span>"+obj.charge+"</span></p>";
			html+="<p><span class='color3'>地址：</span><span>"+obj.address+"</span></p>";
			html+="</div>"; 
			html+="<div onclick='getEmergencyMaterial(\""+obj.id+"\")' style='width:13px; height:14px; display:inline-block; background: url("+baseUrl+"/images/detailsICON.png) no-repeat;  position: absolute; right: 5px; top:8px;'></div>";
			html+="</li>";
		return html;
	}
	
	/*应急专家拼接html*/
	function getSpecialistHtml(obj,j){
		/*var index =j+1;
		var html="<li onClick='getSpecialistDetail("+ JSON.stringify(obj)+")'>";
		html+="<div class='agencyHd ellipsis'>";
		html+="<div class='expertNew ca cf'>";
		html+="<div class='fl'><span class='expertNumber'><i>"+index+"</i></span>"+obj.name+"</div>";
		html+="<span class='expertAmount fl'>"+obj.expertCout+"</span>";
		html+="</div>";
		html+="<div class='expertMroe'><img src='"+baseUrl+"/images/fyMoreIcon.png' alt='' /></div>";
		html+="</div>";
		html+="<div class='expertDiv'>";
		html+="<table cellpadding='0' cellspacing='0' border='0' width='100%' class='expertDivTab'>";
		html+="<thead>";
	    html+="<tr>";
		html+="<th>专家姓名</th>";
		html+="<th>性别</th>";
		html+="<th>出生日期</th>";
		html+="<th>所属单位</th>";
		html+="<th>专业</th>";
		html+="<th>家庭住址</th>";
		html+="</tr>";
		html+="</thead>";
		html+="<tbody>";
		if(obj.expertList){
			for(var i=0;i<obj.expertList.length;i++){
				var curr =obj.expertList[i];
				html+="<tr>";
				html+="<td>"+curr.name+"</td>";
				html+="<td>"+(curr.sex?(curr.sex=="1"?"男":"女"):"")+"</td>";
				html+="<td style='width:80px;'>"+(curr.birth?new Date(curr.birth).Format("yyyy-MM-dd"):"")+"</td>";
				html+="<td>"+curr.unit+"</td>";
				html+="<td>"+curr.major+"</td>";
				html+="<td>"+(curr.address?curr.address:"")+"</td>";
				html+="</tr>";
			}
		}
		html+="</tbody>";
		html+="</table>";
		html+="</div>";
		html+="</li>";	*/
	
		var html="<li onClick='getSpecialistDetail("+ JSON.stringify(obj)+")'>";
			html+="<div class='expertBox ca cf'>";
			html+="<div class='fl photopic'><img src='"+baseUrl+"/images/photopic.png' alt='' /></div><div class='fl expertMsg'><p class='experName'>"+obj.name+"</p><p>"+obj.phone+"</p></div>";
			html+="</div>";
			html+="<div class='listFrom' >"; 		
			html+="<p class='clamp2'><span class='color3'>所属单位：</span><span>"+obj.unit+"</span></p>";
			html+="<p><span class='color3'>类型：</span><span>"+obj.expertTypeName+"</span></p>";			
			html+="<div class='listFrom' style=' padding:0;display:none;'>";
			html+="<p><span class='color3'>专业：</span><span>"+obj.major+"</span></p>";
			html+="<p><span class='color3'>特长：</span><span>"+obj.speciality+"</span></p>";
			html+="<p><span class='color3'>性别：</span><span>"+(obj.sex?(obj.sex=="1"?"男":"女"):"")+"</span></p>";
			html+="<p><span class='color3'>职务：</span><span>"+obj.position+"</span></p>";
			html+="<p><span class='color3'>主修科目：</span><span>"+(obj.majorSubject?obj.majorSubject:"")+"</span></p>";
			html+="<p><span class='color3'>家庭住址：</span><span>"+(obj.address?obj.address:"")+"</span></p>";
			html+="<p><span class='color3'>出生日期：</span><span>"+(obj.birth?new Date(obj.birth).Format("yyyy-MM-dd"):"")+"</span></p>";
			html+="</div>";
			html+="<div class='openMore '></div>";
			html+="</div>"; 
			html+="</li>"; 
		return html;
	}
	/*应急预案拼接html*/
	function getReservePlanHtml(obj){
		var html='<li>'; 
		    html+='<div class="specialHd">'+obj.name+'</div>'; 
			html+='<div class="dgMsg ca cf">'; 
			//html+='<div class="pdf fl"><img src="'+baseUrl+'/images/pdf.png" alt="" /></div>'; 
			html+='<div class="fl pdfdetils">';
			html+='<p class="ca cf">'; 
			html+='<label class="dgMsg-lab fl">预案编号：</label>'; 
			html+='<span class="dgMsg-span fl w165">'+obj.code+'</span>'; 
			html+='</p>'; 
			html+='<p class="ca cf">'; 
			html+='<label class="dgMsg-lab fl">预案类型：</label>'; 
			html+='<span class="dgMsg-span fl w165">'+obj.typeName+'</span>'; 
			html+='</p>'; 
			html+='<p class="ca cf">'; 
			html+='<label class="dgMsg-lab fl">编写人：</label>'; 
			html+='<span class="dgMsg-span fl w165">'+obj.writer+'</span>'; 
			html+='</p>'; 
			html+='<p class="ca cf">'; 
			html+='<label class="dgMsg-lab fl">编写时间：</label>'; 
			html+='<span class="dgMsg-span fl w165">'+(obj.writingTime?new Date(obj.writingTime).Format("yyyy-MM-dd"):"")+'</span>'; 
			html+='</p>'; 
			html+='<p class="ca cf">'; 
			html+='<label class="dgMsg-lab fl">编写单位：</label>'; 
			html+='<span class="dgMsg-span fl w165">'+obj.fireBrigadeName+'</span>'; 
			html+='</p>'; 
			html+='</div>'; 
			html+='</div>'; 
			if(obj.swfPath){
				html+='<div class="prepress ca cf" style="padding-bottom: 10px;">'; 
				html+='<a href="javascript:void(0)" class="see" onClick="showReservePlandoc(\''+obj.id+'\')"><i class="eye"></i>查看</a>'; 
				html+='<a href="javascript:void(0)" onclick="downLoadFile(\''+obj.path+'\',\''+obj.fileName+'\')" ><i class="load"></i>下载</a>';
				html+='</div>'; 
			}
			html+='</li>';
			return html;
	}	
	
	/*生产装置拼接html*/
	function getDeviceTypeHtml(obj){
		// onclick='gotoPointLocation(\""+obj.lon+"\",\""+obj.lat+"\",\"xfdw\",\""+obj.name+"\",\""+obj.id+"\",\"other\")
		obj.type='sczz';
		obj.other=obj.keyUnitName;
	    var html="<li  onClick='gotoPointLocation("+ JSON.stringify(obj)+")'><div class='listHd ellipsis'><i class='address'></i>"+obj.name+""; 
	        if(obj.distances){
			   html+="<span style='color: #ff731e;font-weight: bolder;'>("+obj.distances+"千米)</span>";
		    }
	        html+="</div>"; 
			html+="<div class='listFrom'>";
			html+="<p><span class='color3'>装置类型：</span><span>"+obj.equipmentTypeName+"</span></p>";
			html+="<p class='clamp2'><span class='color3'>所属单位：</span><span>"+obj.keyUnitName+"</span></p>";
			html+="<p class='clamp2'><span class='color3'>装置编码：</span><span>"+obj.code+"</span></p>";
			html+="<p class='clamp2'><span class='color3'>介质：</span><span>"+obj.medium+"</span></p>";
			html+="<p class='clamp2'><span class='color3'>耐火等级：</span><span>"+obj.fireLevel+"</span></p>";
			html+="<p class='clamp2'><span class='color3'>容器直径：</span><span>"+obj.vesselDiameter+"</span></p>";
			html+="<p class='clamp2'><span class='color3'>装置体积：</span><span>"+obj.volume+"</span></p>";
			html+="</div>"; 
			html+="</li>";
		return html;
	}	
	
	//消防队伍
	function getFireBrigadeHtml (obj){
		obj.type='xfdw';
	    var html="<li><div class='listHd ellipsis' onclick='gotoPointLocation("+ JSON.stringify(obj)+")'><i class='address'></i>"+obj.name+"";
	        if(obj.distances){
			   html+="<span style='color: #ff731e;font-weight: bolder;'>("+obj.distances+"千米)</span>";
		    }
	        html+="</div>";
	        html+="<div class='listFrom'>";
			//html+="<p><span class='color3'>负责人：</span><span>"+obj.charge+"("+obj.phone+")</span></p>";
			html+="<p><span class='color3'>地址：</span><span>"+obj.position+"</span></p>";
			html+="<p><span class='color3'>电话：</span><span>"+obj.phone+"</span></p>";
			html+="<p><span class='color3'>消防车辆：</span><span>"+obj.carCount.carCount+"台</span></p>";			
			html+="<div class='foamMsg'style=' padding:0; display:none;'>";
			html+="<div>";
			html+="<div class='libraryHd foamMsgHd'><i></i>车载灭火剂(T)</div>";
			html+="<div class='listFrom' style=' padding:0;'>";
			html+="<table cellpadding='0' cellspacing='0' border='0' width='100%' class='foamTable'>";
			html+="<thead>";
			html+="<tr>";
			html+="<th>水<span></span></th>";
			html+="<th>泡沫<span></span></th>";
			html+="<th>干粉<span></span></th>";
			html+="</tr>";
			html+="</thead>";
			html+="<tbody>";
			html+="<tr>";
			html+="<td>"+(obj.carCount.water?obj.carCount.water:0)+"</td>";
			html+="<td>"+(obj.carCount.powderQuantity?obj.carCount.powderQuantity:0)+"</td>";
			html+="<td>"+(obj.carCount.capacity?obj.carCount.capacity:0)+"</td>";
			html+="</tr>";
			html+="</tbody>";
		    html+="</table>";
			html+="</div>";
			
			if(obj.miehuojList.length>0){
				for(var i=0;i<obj.miehuojList.length;i++){
					var curr =obj.miehuojList[i];
					html+="<p><span class='color3'>"+curr.foamTypeName+"：</span><span>"+curr.capacity+"kg</span></p>";
				}
			}
			html+="<div class='libraryHd foamMsgHd'><i></i>预案</div>";
			if(obj.planList.length>0){
				for(var i=0;i<obj.planList.length;i++){
					var curr =obj.planList[i];
					html+="<div class='ca cf ptb5'>";
					html+="<div class='fl firedangerMsgHd'><i></i><span class='color3'>"+curr.name+"</span></div>";
					html+="<div class='fr seeDiv'><span onClick='showReservePlandoc(\""+curr.id+"\")'>查看</span>" ;
					html+="<span onClick='downLoadFile(\""+curr.path+"\",\""+curr.name+"\")'>下载</span>";
					html+="</p>"; 
					html+="</div>";
					html+="</div>";
				}
			}else{
				html+="<div class='NoReservePlan'>暂无预案</div>";
			}
			html+="</div>"; 
			html+="</div>";
//			html+="<p><span class='color3'>救援能力：</span><span>"+obj.rescueAbility+"</span></p>";
//			html+="<p><span class='color3'>性质：</span><span>"+obj.properties+"</span></p>";
			html+="<div class='openMore'></div>";
			html+="</div>"; 
			html+="<div onclick='FireBrigadeDetail(\""+obj.id+"\")' style='width:13px; height:14px; display:inline-block; background: url("+baseUrl+"/images/detailsICON.png) no-repeat;  position: absolute; right: 5px; top:8px;'></div>";
			html+="</li>"; 
		return html;
	}
	
    /*$("#xfcShow").click(function(){
        $(".listDd").css("display","none");
        $(".arrow").removeClass("open");
        $(this).find("span:first-child").addClass("open");
        $(this).next().css("display","block")
    });*/
//会商绘标面板的元素
//    $("#carContentId ul li").click(function(){
//    	var t=$(this).attr("t");
//    	var iconType=$(this).attr("iconType");
//    	alert(iconType);
//    	var name = $.trim($(this).text());
//        addMaker($(this).attr("t"),name,iconType);
//    });
	
    $("#zddwsw").click(function(){
        if($("#zddwsw").prop("checked")){
        	layerVisible('zddwClusterLayer',true);
        	layerVisible('sczzClusterLayer',true);
        }else{
        	layerVisible('zddwClusterLayer',false);
        	layerVisible('sczzClusterLayer',false);
        }
    });

    $("#xfsysw").click(function(){
        if($("#xfsysw").prop("checked")){
        	layerVisible('xfsyClusterLayer',true);
        	layerVisible('xfsClusterLayer',true);
        }else{
        	layerVisible('xfsyClusterLayer',false);
        	layerVisible('xfsClusterLayer',false);
        }
    });

    $("#yjdwsw").click(function(){
        if($("#yjdwsw").prop("checked")){
        	layerVisible('yjdwClusterLayer',true);
        }else{
        	layerVisible('yjdwClusterLayer',false);
        }
	});
	
	$("#xfdwsw").click(function(){
        if($("#xfdwsw").prop("checked")){
        	layerVisible('xfdwClusterLayer',true);
        }else{
        	layerVisible('xfdwClusterLayer',false);
        }
    });
    
    $("#yjzbsw").click(function(){
        if($("#yjzbsw").prop("checked")){
        	layerVisible('yjzbClusterLayer',true);
        }else{
        	layerVisible('yjzbClusterLayer',false);
        }
    });
    
    $("#sczzsw").click(function(){
        if($("#sczzsw").prop("checked")){
        	layerVisible('sczzClusterLayer',true);
        }else{
        	layerVisible('sczzClusterLayer',false);
        }
    });
    

    $("#wzcbdsw").click(function(){
        if($("#wzcbdsw").prop("checked")){
        	layerVisible('wzcbdClusterLayer',true);
        }else{
        	layerVisible('wzcbdClusterLayer',false);
        }
    });
    
	/*图层显示隐藏事件*/
	$('#tuCengEvent').click(function () {
		if($("#toolBtnDiv").is(":hidden")){
			$("#toolBtnDiv").show();
	    }else{
		    $("#toolBtnDiv").hide();
		}			   
	});
	
	 /*图层显示隐藏事件*/
	$('#showTucengBtn').click(function () {
		if($("#toolBoxBTN").is(":hidden")){
			$("#toolBoxBTN").show();
	    }else{
		    $("#toolBoxBTN").hide();
		}			   
	});
	
	/*视频配置等功能切换*/
	 $("#videoNavSPPZ").find("ul li").click(function(){	
		 $(this).addClass("onActive").siblings().removeClass("onActive"); //切换选中的按钮高亮状态
		 var index=$(this).index(); //获取被按下按钮的索引值，需要注意index是从0开始的
		 $("#videoNavSPPZ > li").eq(index).show().siblings().hide(); //在按钮选中时在下面显示相应的内容，同时隐藏不需要的框架内容
		 if(index==0){
			 $("#contBox1").show();
			 $("#contBox2").hide();
			 $("#contBox3").hide();
			 $("#contBox4").hide();
		 }else if(index==1){
			 $("#contBox1").hide();
			 $("#contBox2").show();
			 $("#contBox3").hide();
			 $("#contBox4").hide();
		 }else if(index==2){ 
			 $("#contBox1").hide();
			 $("#contBox2").hide();
			 if(videoType==1){
				 $("#contBox3").show();
				 $("#contBox4").hide();
			 }else if(videoType==2){
				 $("#contBox4").show();
				 $("#contBox3").hide();
			 }
		 }
	 });
	
	 
	 /*相机tab切换*/
	 $("#cameraSetTabId").find("ul li").click(function(){	
		 $("#cameraSerial").val($(this).attr("name"));
		 $(this).addClass("onLink").siblings().removeClass("onLink"); 
		 var index=$(this).index(); 
		 $("#cameraSetTabId > li").eq(index).show().siblings().hide();
		 getCameraInfoAjax($(this).attr("name"));
		 
	 });
	 
	$._Accident = new Accident();
	$._IconBox = new IconBox();
	//隐藏按钮
	$("#zhmnToolBar").hide();
	$("#disasterOnclickUl li").hide();
	//t_fire_brigade
	// 添加Tdrag
//	
	addTdrageDiv();

	$('.userPhotoWarp').on('mouseenter',function(){
		var v = $('.userPhotoWarp').offset();
		if($.userPhotoblur){
			//console.log("头像移入 清除")
			clearTimeout($.userPhotoblur);
		}
		//console.log("头像移入 显示")
		$('#dropDivBox').fadeIn("fast").attr("blur","")/*.css('left',(v.left-30)+'px')*/;
	}).on('mouseleave',function(){
		if($.userPhotoblur){
			//console.log("头像移出 清除")
			clearTimeout($.userPhotoblur);
		}
		if($("#dropDivBox").attr("blur")=="out"){
			//console.log("头像移出 隐藏")
			$('#dropDivBox').fadeOut("fast");
		}
		$.userPhotoblur = setTimeout(function(){
			if($("#dropDivBox").attr("blur")==""){
				//console.log("1.5秒后头像移出 隐藏")
				$('#dropDivBox').hide().attr("blur","out");
			}
		},1000)
	});
	$('#dropDivBox').on('mouseenter',function(){
		//console.log("模块移入 清除")
		clearTimeout($.userPhotoblur);
		//console.log("模块移入 显示")
		$('#dropDivBox').show().attr("blur","in");
	}).on('mouseleave',function(){
		$("#dropDivBox").attr("blur","out");
		if($("#dropDivBox").attr("blur")=="out"){
			//console.log("模块移出 隐藏")
			$('#dropDivBox').fadeOut("fast");
		}
	});


	$('#dataTypeId li').click(function(){
		$('#dataTypeId li').removeClass('onlike');
		$(this).addClass('onlike');
		switch($(this).attr('id')){
			case "realtimeDataLi":
				$('#realTimeDataDivId').show();
				$('#picArchiveDataDivId').hide();
				$('#historyDataDivId').hide();
				closeMiniCamera();
				break;
			case "tuwenshujuLi":
				$('#picArchiveDataDivId').show();
				$('#realTimeDataDivId').hide();
				$('#historyDataDivId').hide();
				break;
			case "hitoryDataLi":
				$('#historyDataDivId').show();
				$("#alarm").css('width','100%');
				$('#policeBJ').css('width','100%');
				$('#policeBJ').css('left','0');
				$('#picArchiveDataDivId').hide();
				$('#realTimeDataDivId').hide();
				closeMiniCamera();
				break;
		};
		
		$("#deviceC").attr("class","Big").removeClass("facility");
	});
	
	//获取喷射类器材的数量 
	$.ajax({
		type : "post",
		url : baseUrl+"/webApi/others/qrySpecialDutyEquip.jhtml",
		data: null,
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {					
			if(data.httpCode==200){
				waterSupplyEquipment = data.data;
			}
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	});
	
	
	//全屏事件
	$('#fullScreenDiv').on('click', function(e) {
		var element = document.documentElement;
		if (!$('body').hasClass("full-screen")) {
			$('body').addClass("full-screen");
			$('#fullScreenDiv').addClass("active");
			$('#fullScreenDiv i').addClass("fullIconSmall");
			$('#fullScreenDiv i').attr("title","缩小");
			if (element.requestFullscreen) {
				element.requestFullscreen();
			} else if (element.mozRequestFullScreen) {
				element.mozRequestFullScreen();
			} else if (element.webkitRequestFullscreen) {
				element.webkitRequestFullscreen();
			} else if (element.msRequestFullscreen) {
				element.msRequestFullscreen();
			}

		} else {
			$('body').removeClass("full-screen");
			$('#fullScreenDiv').removeClass("active");
			$('#fullScreenDiv i').removeClass("fullIconSmall");
			$('#fullScreenDiv i').attr("title","放大");
			if (document.exitFullscreen) {
				document.exitFullscreen();
			} else if (document.mozCancelFullScreen) {
				document.mozCancelFullScreen();
			} else if (document.webkitCancelFullScreen) {
				document.webkitCancelFullScreen();
			} else if (document.msExitFullscreen) {
				document.msExitFullscreen();
			}

		}
	});
	
	//灾害模拟提示信息弹框事件
	$(".hintIcon").mouseover(function(){
		$(this).next().show();
	}).mouseout(function(){
		 $(this).next().hide();
	  });
	  //自定义公式提示信息弹框事件
	$(".exprforHintIcon").mouseover(function(){
		$("#formulaExplain").show();
	}).mouseout(function(){
		$("#formulaExplain").hide();
	  });
	
	  setTimeout(function(){
		//$("#subMeusDiv").css("margin-left","90px");
	},200);
	  
	//连接消息服务
	connect();
});


function fireCarDetail(oid){
	
}

function addTdrageDiv(){
	$("#videoCloseId").Tdrag({
		scope:"#map",
		handle:"#videoDivHandle" 
	});
	$("#xlksModelDiv").Tdrag({
		scope:"#map",
		handle:"#xlksModelDivHandle"
	});
	$("#rswfxTubiaoDivOut").Tdrag({
		scope:"#map",
		handle:"#rswfxTubiaoDivOutHandle"
	});
	$("#klwcjModelDiv").Tdrag({
		scope:"#map",
		handle:"#klwcjModelDivHandle"
	});
	$("#toolBoxBTN").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#xftcTanKuangDiv"
	});
	$("#xlydwModelDiv").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#xlydwModelDivHandle"
	});
	$("#rfsModelDiv").Tdrag({
		scope:"#map",
		handle:"#rfsModelDivHandle"
	});
	$("#docView").Tdrag({
		scope:"#map",
		handle:"#docViewHandle"
	});
	$("#rswfxModelDiv").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#rswfxModelDivHandle"
	});
	$("#yygqfyModelDiv").Tdrag({
		scope:"#map",
		handle:"#yygqfyModelDivHandle"
	});
	$("#weatherMsgModelDiv").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#weatherMsgModelDivHandle"
	});
	$("#fzdxModelDiv").Tdrag({
		scope:"#map",
		handle:"#fzdxModelDivHandle"
	});
	$("#tqxxModelDiv").Tdrag({
		scope:"#map",
		handle:"#tqxxModelDivHandle"
	});
	$("#zhmnQxzsOutDiv").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#zhmnQxzsOutDivHandle"
	});
	$("#zhmnZZYPPJDiv").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#zhmnZZYPPJDivHandle"
	});
	$("#zhmnRSWFXDivOut").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#zhmnRSWFXDivOutHandle"
	});
	$("#zhmnKSSJDivOut").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#zhmnKSSJDivOutHandle"
	});
	$("#zhmnKSSJDivOutSyn").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#zhmnKSSJDivOutHandleSyn"
	});

	$("#ysslDiv").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#ysslDivHandle"
	});
	
	$("#cgybzModelDiv").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#cgybzModelDivHandle"
	});
	$("#ReservePlandocview").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#ReservePlandocviewDivHandle"
	});
	$("#gjfsModelDiv").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#gjfsModelDivHandle"
	});
	$("#mhjModelDiv").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#mhjModelDivHandle" 
	});
	$("#formulaCalculationDiv").Tdrag({
		scope:"#map",
		//iscenter:false,
		handle:"#zdygsModelDivHandle" 
	});
	$("#damageWaveDiv").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#damageWaveDivHandle" 
	});
	$("#damagePeopleDiv").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#damagePeopleDivHandle" 
	});
	$("#toolBoxBTN").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#xftcTanKuangDiv" 
	});
	$("#gqmhjTJModelDiv").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#gqmhjTJModelDivHandle" 
	});
	$("#fireDosageDiv").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#fireDosageDivHandle" 
	});
	$("#nhjxModelDiv").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#nhjxModelDivHandle" 
	});
	$("#hzmyModelDiv").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#hzmyModelDivHandle" 
	});
	$("#getEmergencyMaterialDiv").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#getEmergencyMaterialDivHandle" 
	});
	$("#materialSeeDiv").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#materialSeeDivHandle" 
	});
	$("#vehicleEquipmentDiv").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#vehicleEquipmentDivHandle" 
	});
	$("#zhmnHZMYFXDivOut").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#zhmnHZMYFXDivOutHandle" 
	});
	$("#iconBoxZNTJ").Tdrag({
		scope:"#map",
		iscenter:false,
		handle:"#zntjdrag" 
	});
}

/*隐藏配置页面*/
function pejmHide(){	
	 $("#pzjmPanel").hide();
}

/*关闭图层显示隐藏事件*/
function toolCloseBTN() {
	$("#toolBoxBTN").hide();
}

/*头部搜索框事件 开始*/
function optionZDDWShow(){	
	if($("#selectOptionZDDW").is(":hidden")){
		$("#selectOptionZDDW").show();    //如果元素为隐藏,则将它显现
	}else{
		$("#selectOptionZDDW").hide();     //如果元素为显现,则将其隐藏
	}  
}

/*智能分析显示隐藏事件*/
function showOrHideZNFC(){
	if($("#analysisZNFX").is(":hidden")){
		$("#analysisZNFX").show();
    }else{
	    $("#analysisZNFX").hide();
	}
}

/*左侧点击定位*/
function gotoPointLocation(obj){
	leftRecordLocation(Number(obj.lon),Number(obj.lat),obj.type,obj.name,obj.id,obj.other);
	//leftRecordLocation(Number(obj.lon),Number(obj.lat),obj.type,obj.name,obj.id,"",obj.other);
}


/*显示危化品库详情*/
function getUnitInfo1(){
	$(".information").show();
	$(".planBox").hide();
	$("#dangerDetailDiv").hide();   
	$("#weihuapinList").hide();
	$(".detailMenu ul").find("li").eq(0).addClass("activeLink")
	$(".detailMenu ul").find("li").eq(2).removeClass("activeLink")	
	$(".detailMenu ul").find("li").eq(1).removeClass("activeLink")

}

function getUnitInfo2(){
	$(".information").hide();
	$(".planBox").show();
	$("#dangerDetailDiv").hide();
	$("#weihuapinList").hide();
	$(".detailMenu ul").find("li").eq(1).addClass("activeLink")
	$(".detailMenu ul").find("li").eq(0).removeClass("activeLink")
	$(".detailMenu ul").find("li").eq(2).removeClass("activeLink")
}


function getUnitInfo3(){
	$(".information").hide();
	$(".planBox").hide();
	$("#dangerDetailDiv").hide();
	$("#weihuapinList").show();
	$(".detailMenu ul").find("li").eq(2).addClass("activeLink")
	$(".detailMenu ul").find("li").eq(0).removeClass("activeLink")	
	$(".detailMenu ul").find("li").eq(1).removeClass("activeLink")
}

function getUnitInfo4(){
	$(".information").show();
	$(".equipmentdoc").hide();
	$(".detailMenu ul").find("li").eq(0).addClass("activeLink")
	$(".detailMenu ul").find("li").eq(1).removeClass("activeLink")	
}

//生产装置 装置文档 显示方法
function getUnitInfo5(id){
	$(".information").hide();
	$("#equipmentdocview .dgBox").removeClass("dgBox");
	$("#equipmentdocview.dangerous-frame").removeClass("dangerous-frame");
	$("#equipmentdocview .dgHdName").hide();
	$("#equipmentdocview .wordMore").show();
	$(".detailMenu ul").find("li").eq(1).addClass("activeLink")
	$(".detailMenu ul").find("li").eq(0).removeClass("activeLink")
	//得到文件列表
	//	选择第一个
	//		绘制目录 绘制文件
	//注册在关闭时移动doc控件
	$('a.ol-popup-closer').on("click",function(){
		$('#docView .dgWord').append($('#viewerPlaceHolder'));
	})
	
	//var  params =id;
	$.ajax({
		type : "post",
		url : baseUrl+"/equipment/qryById.jhtml",
		data: JSON.stringify(id),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				var docview = $('#equipmentdocview');		
				docview.find('ul.wordList').html('');//ctMeuns wordList	
				docview.find("ul.ctMeuns").html('');	
				if(data.docList){
					window.equipmentdocList = data.docList;
					$.each(data.docList, function(i,item){  
						var html= ('<li onclick="equipmentshowdoc(\''+item.id+'\',this)"><div class="wordPic"><img src="'+baseUrl+'/images/pdf.png" alt="" /></div><p>'+item.docName+'</p></li>')
						docview.find('ul.wordList').append(html);
						if(i==0){
							equipmentshowdoc(item.id);
							$(html).addClass("activate");
						}
					});
					$(".equipmentdoc").show();
				}
			}else{
				layer.msg(systemAbnormalityMsg, {time: 2000});
			}
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	})
}



//灭火剂计算显示

function showFireDiv1(e){
	$("#solidDiv").show();
	$("#liquefyingDiv").hide();
	$(".detailMenuNew ul li").removeClass("activeLink");
	$(e).addClass("activeLink");

}
function showFireDiv2(e){
	$("#solidDiv").hide();
	$("#liquefyingDiv").show();
	$(".detailMenuNew ul li").removeClass("activeLink");
	$(e).addClass("activeLink");
}
function showFireDiv3(){
	$("#solidDiv").hide();
	$("#liquefyingDiv").hide();
	$("#petroleumDiv").show();   
	$(".detailMenuNew ul").find("li").eq(2).addClass("activeLink")
	$(".detailMenuNew ul").find("li").eq(0).removeClass("activeLink")	
	$(".detailMenuNew ul").find("li").eq(1).removeClass("activeLink")

}



//显示文档
function equipmentshowdoc(id,e){
	var item;
	window.equipmentdocList.find(function(m){
		if(m.id==id){
			item = m;
		}
	})
	var docview = $('#equipmentdocview');
	docview.find(".wordList li").removeClass('activate');
	docview.find(".docName").text(item.docName);
	docview.find(".docName2").text(item.docName);
	docview.find(".docLX").text(item.equipmentTypeName);
	docview.find("ul.ctMeuns").html('');
	var viewDocId="equipmentdocview";
	$.each(item.equipmentDocCatalogs,function(i,v){
//		console.log(JSON.stringify(v));
		if(v && v.page){
			var html=$('<li docml="'+v.page+'"><a href="javascript:gotoPage(\''+ v.page +'\',\''+ viewDocId +'\')"><span></span>'+v.name+'</a></li>');
			docview.find("ul.ctMeuns").append(html);
			if(i==0){
				html.addClass("activeOn");
			}
		}
	})
	if(e){
		$(e).addClass('activate');
	}
	if(item.swfPath){
		loadDoc(baseFileUrl + item.swfPath,"equipmentdocview");
	}
}

/*显示危化品库详情*/

function getDangerChemicDetail(obj){
	 $("#dangerChemicStoreData").hide();
	 $("#dangerChemicDetail").show();
	 $("#dangerChemicPage").hide();
}


/*消防水源的详情*/
function getXFSYDetailForMark(obj,name){
	var html='';
	 html+='<div class="reWarp">';
	 html+='<div class="reWarpHd">'+name+'</div>';
	 html+='<div class="reWarpMsg">';
	 html+='<p><span>类型：</span><span>'+obj.typeName+'</span></p>';
	 html+='<p><span>所属重点单位：</span><span>'+obj.keyUnitName+'</span></p>';
	 html+='<p><span>地址：</span><span>'+obj.addr+'</span></p>';
	 html+='<p><span>储量：</span><span>'+(obj.reserves? obj.reserves :'无')+'</span></p>';
	 if(obj.surveyTime&&obj.surveyTime != ""){
		 var surveyTime = timestampToStr(obj.surveyTime);
		html+="<p><span>勘查时间：</span><span>"+surveyTime+"</span></p>";
	}
	if(obj.surveyor&&obj.surveyor!=""){
		html+="<p><span>勘查人：</span><span>"+obj.surveyor+"</span></p>";
	}
	if(obj.verifier&&obj.verifier!=""){
		html+="<p><span>核查人：</span><span>"+obj.verifier+"</span></p>";
	}
	if(obj.waterSourcePointHead && obj.waterSourcePointHead!=null){
		html+="<p><span>水源点负责人：</span><span>"+obj.waterSourcePointHead+"</span></p>";
	}
	if(obj.phone && obj.phone!=""){
		html+="<p><span>电话：</span><span>"+obj.phone+"</span></p>";
	}
	if(obj.jurisdictionUnit && obj.jurisdictionUnit != ""){
		html+="<p><span>管辖单位：</span><span>"+obj.jurisdictionUnit+"</span></p>";
	}
	 html+='</div>';
	 html+='</div>';
	return html;
}

/*消防设施的详情*/
function getXFSSDetailForMark(obj,name,type){
	var html='';
	 html+='<div class="reWarp">';
	 html+='<div class="reWarpHd">'+name+'</div>';
	 html+='<div class="reWarpMsg">';
	 html+='<p><span>消防栓半径：</span><span>'+obj.fireRadius+'(m)</span></p>';
	 html+='<p><span>消防栓额定流量：</span><span>'+obj.flow+'(L/S)</span></p>';
	 html+='<p><span>已开启流量：</span><span id="setCustomValueXFSYKQ">0</span>(L/S)</p>';
	// html+='<p class="ca cf"><span class="fl">修改流量：</span><span class="fl"><input type="text" id="setCustomValueXFS" class="flowInto" value="'+obj.flow+'"/></span>(L/S)</p>';
//	 if(type=="xfsy"){
		 html+='<p><input type="button" class="unlockBtn" onclick="openXFSSDetailForMark(\''+obj.id+'\',\''+obj.flow+'\',this)" value="全开"/></p>';
		 html+='<p><input type="button" class="unlockBtn" onclick="openXFSSDetailForMark(\''+obj.id+'\',\''+obj.flow/2+'\',this)" value="半开"/></p>';
//	 }else{
		 html+='<p><input type="button" class="unlockBtn" onclick="openXFSSDetailForMark(\''+obj.id+'\',\'0\',this)" value="关闭"/></p>';
//	 }
	 html+='</div>';
	 html+='</div>';
	return html;
}

/*应急队伍详情*/
function getYYDWDetailForMark(obj){
	var html='';
	 html+='<div class="reWarp" style="width:250px;">';
	 html+='<div class="reWarpHd">'+obj.name+'</div>';
	 html+='<div class="reWarpMsg">';
	 html+='<p><span>规模：</span><span>'+obj.scale+'</span></p>';
	 html+='<p><span>性质：</span><span>'+obj.properties+'</span></p>';
	// html+='<p><span>负责人：</span><span>'+obj.charge+'</span></p>';
	// html+='<p><span>负责人联系方式：</span><span>'+obj.phone+'</span></p>';
	 html+='<p><span>地址：</span><span>'+obj.position+'</span></p>';
	 html+='</div>';
	 html+='</div>';
	return html;
}


/*消防队伍详情*/
function getXFDWDetailForMark(obj){
	var html='';
	if(!obj){return "";}
	 html+='<div class="reWarp" style="width:250px;">';
	 html+='<div class="reWarpHd">'+obj.name+'</div>';
	 html+='<div class="reWarpMsg">';
	 html+='<p><span>规模：</span><span>'+obj.scale+'</span></p>';
	 html+='<p><span>性质：</span><span>'+obj.properties+'</span></p>';
	 html+='<p><span>救援能力：</span><span>'+obj.rescueAbility+'</span></p>';
	// html+='<p><span>负责人：</span><span>'+obj.charge+'</span></p>';
	// html+='<p><span>负责人联系方式：</span><span>'+obj.phone+'</span></p>';
	 html+='<p><span>地址：</span><span>'+ obj.position+'</span></p>';
	 html+='<p style="text-align: center;">';
	 html+='<input type="button" style="width:70px;height: 26px;background-color: #1890ff;font-size: 14px;color: #fff; cursor: pointer; text-align: center;border-radius: 5px;border: none;margin: 0 5px;" value="查看器材" onclick="FireBrigadeDetail(\''+obj.id+'\')"/>';
	// html+='<input type="button" style="width:70px;height: 26px;background-color: #1890ff;font-size: 14px;color: #fff; cursor: pointer; text-align: center;border-radius: 5px;border: none;margin: 0 5px;" value="查看预案" onclick="FireBrigadeDetail(\''+obj.id+'\')"/>';
	 html+='</p>';
	 html+='</div>';
	 html+='</div>';
	return html;
}

/*生产装置的详情*/

function getSCZZDetailForMark(obj,parentName){
	var html='<div class="urgent">';
	html+='<div class="dgBox" style="width: 852px;height: 720px;">';
	html+='<div class="dgHdName">'; 
	html+='<span>'+obj.name+'</span>'; 
	html+='</div>';
	html+='<div class="dgContent">';
	html+='<div class="detailMenu">';
	html+='<ul class="ca cf">';            				
	html+='<li onClick="getUnitInfo4(this);" class="activeLink">基本信息</li>';
	html+='<li onClick="getUnitInfo5(\''+obj.id+'\');" >装置文档</li>';
//	html+='<li >平面图</li>';
//	html+='<li >工艺流程图</li>';
//	html+='<li >危化品</li>';
//	html+='<li >预案</li>';
	html+='</ul>';
	html+='</div>';
	html+='<div class="information ca cf borderNew" style="height: 625px;">';
	html+='<ul class="ifBar">';
	html+='<li class="ca cf bgColor"><label class="staff fl">所属重点单位：</label><span class="fl plant">'+parentName+'</span></li>';
	html+='<li class="ca cf"><label class="staff fl">装置分类：</label><span>'+obj.equipmentTypeName+'</span class="fl plant"></li>';
	html+='<li class="ca cf bgColor"><label class="staff fl">装置编号：</label><span class="fl plant">'+obj.code+'</span></li>';
	html+='<li class="ca cf"><label class="staff fl">装置介质：</label><span class="fl plant">'+obj.medium+'</span></li>';
	html+='<li class="ca cf bgColor"><label class="staff fl">容器直径：</label><span class="fl plant">'+(obj.vesselDiameter!=undefined? obj.vesselDiameter :' ')+'</span></li>';
	html+='<li class="ca cf"><label class="staff fl">耐火等级：</label><span class="fl plant">'+obj.fireLevel+'</span></li>';
	html+='<li class="ca cf bgColor"><label class="staff fl">体积：</label><span class="fl plant">'+(obj.volume!=undefined? obj.volume :' ')+'</span></li>';
	html+='</ul>';
	html+='</div>';
	html+='<div class="equipmentdoc ca cf borderNew" style="display:none">';
	html+='<div id="equipmentdocview" class="dangerous-frame">';
	html+= $('#ReservePlandocview').html();
	html+='</div>';
	html+='</div>';
	html+='<div class="detCont factory borderNew" style="display:none;">';
	html+='<img src="'+baseUrl+'/images/img-01.png" alt="" />';
	html+='</div>';
	html+='</div>';
	html+='</div>';
	html+='</div>';
	return html;
}

/*物资储备点的详情*/ 
//todo
function getWZCBDDetailForMark(obj,name){
	var html='';
	 html+='<div class="reWarp">';
	 html+='<div class="reWarpHd">'+name+'</div>';
	 html+='<div class="reWarpMsg">';
	 html+='<div class="reservePoint">';
	 html+='<table cellpadding="0" cellspacing="0" border="0" width="100%" class="reservePointTab">';
	 html+='<thead>';
	 html+='<tr>';   
	 html+='<th>物资名称</th>';
	 html+='<th>数量</th>';
	 html+='<th>用途</th>';
	 html+='<th>备注</th>';
	 html+='<th>操作</th>';
	 html+='</tr>';
	 html+='</thead>';
	 html+='<tbody>';
	 for(var i=0;i<obj.length;i++){
		var curr = obj[i];
		//html+='<p class="reservePointName ca cf"><span class="fl w60">'+curr.name+':</span><span class="fl"><span id="'+curr.id+'C" class="reservePointNumber">'+curr.storageQuantity+'</span>个</span></p>';
		//html+='<p class="ca cf"><input class="fl reservePointInto" type="text" id="'+curr.id+'S" value="'+curr.storageQuantity+'"/><input type="button" class="fl reservePointBtn" value="调用" onclick="useEmergencyMaterial(\''+curr.id+'\',\''+name+'\',\''+curr.name+'\')"/></p>';
		html+='<tr>';
		html+='<td>'+curr.name+'</td>';
		html+='<td id="'+curr.id+'C">'+curr.storageQuantity+'</td>';
		html+='<td>'+curr.purpose+'</td>';
		html+='<td>'+curr.remark+'</td>';
		html+='<td style="width:135px;"><input class="reservePointInto" style="color:#fff; text-align: center;" type="text" id="'+curr.id+'S" value="'+curr.storageQuantity+'"/><input type="button" class="reservePointBtn" value="调用" onclick="useEmergencyMaterial(\''+curr.id+'\',\''+name+'\',\''+curr.name+'\')"/></td>';
		html+='</tr>';	
	 }
	 html+='</tbody>';
	 html+='</table>';
	 html+='</div>';
	 html+='</div>';
	 html+='</div>';
	return html;
}

/*场景的详情*/
function getScenebuildingHtml(obj){
	var html='';
	html+='<li id="'+obj.id+'">';
	html+='<div class="sceneImg">';
	html+='<img src="'+baseFileUrl+obj.resourcePath+'" alt="" />';
	html+='<div class="sceneDel" data="'+obj.id+'"><span><i></i></span></div>';
	html+='</div>';
	html+='<div class="sceneTxt">';
	html+='<span>'+obj.resourceName+'</span>';
	html+='<label class="custom-checkbox"><input class="check-zhengshe" style="display:none;" type="radio" name="sbradio" value="'+obj.id+'"><i></i></label>';
	html+='</div>';
	html+='</li>';	
	return html;
}


//开始绘制标点
function setImageActivate(){
	var id = $('#scenebuildingList input[name="sbradio"]:checked ').val();
	if(!id){
		layer.msg('请选择一个场景', {icon: 0});
		return;
	}
	
	var scr = $('#scenebuildingList input[name="sbradio"]:checked ').parents('li').find('.sceneImg img').attr('src');
	var imageId = $('#scenebuildingList input[name="sbradio"]:checked ').parents('li').attr("id");
	locationImageActivate(scr,imageId);
}


//标点保存
function saveImageActivate(){
	var id = $('#scenebuildingList input[name="sbradio"]:checked ').val();
	var ex = getImageExtent();
	if(!id){
		layer.msg('请选择一个场景', {icon: 0});
		return;
	}else if(ex.length == 0){
		layer.msg('未绘制标点', {icon: 0});
		return;
	}
	var params= ex;
	$.ajax({
		type : "post",
		url : baseUrl+"/accidentResource/updateResource.jhtml",
		data: JSON.stringify(params),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			var addtomap=[];
			for(j = 0,len=data.data.length; j < len; j++) {
				var d = data.data[j];
				if(d.position && d.position!=""){
					addtomap.push({url:d.resourcePath,extent:d.position,id:d.id,imageId:d.id});
				}
			}
			addImagesToMap(addtomap);
			send(JSON.stringify({"evetype":"changeSceneResource","val":addtomap}));
			layer.msg("保存成功");
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
	    }
	})
} 

//获取初始化场景中地图加载的图片
function getAllImageMap(_data){
	var params = {type:resourceType};
	$.ajax({
		type : "post",
		url : baseUrl+"/accidentResource/qryResourceListInMap.jhtml",
		data: JSON.stringify(params),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {
			var addtomap=[];
			if(resourceType==1){
				xzImageResourceList=data.data;
			}else if(resourceType==2){
				wxImageResourceList=data.data;
			}
			for(j = 0,len=data.data.length; j < len; j++) {
				var d = data.data[j];
				if(d.position && d.position!="")
				{
					addtomap.push({url:d.resourcePath,extent:d.position,id:d.id,imageId:d.id});
				}
			}
			
			addImagesToMap(addtomap);
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
	    }
	});
}

//得到场景图
function getImageToMap(_data){
	var params = {type:resourceType};
	if(_data){
		var addtomap=[];
		for(j = 0,len=_data.data.length; j < len; j++) {
			var d = _data[j];
			if(d.position && d.position!=""){
				addtomap.push({url:d.filePath,extent:d.position,id:d.id,imageId:d.id});
			}
		}
		addImagesToMap(addtomap);
	}else{
		$.ajax({
			type : "post",
			url : baseUrl+"/accidentResource/queryResource.jhtml",
			data: JSON.stringify(params),
			dataType:"json",
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {
				var addtomap=[];
				for(j = 0,len=data.data.length; j < len; j++) {
					var d = data.data[j];
					if(d.position && d.position!="")
					{
						addtomap.push({url:d.filePath,extent:d.position,id:d.id,imageId:d.id});
						//addImageToMap(d.position.split(','),d.filePath);
					}
				}
				addImagesToMap(addtomap);
			},error: function(request) {
				layer.msg(networkErrorMsg, {time: 2000});
    	    }
		})
	}
}

/*用于重点单位详情*/
function getUnitDetailForMarkHtml(obj){
	var html='<div class="urgent">';
	html+='<div class="dgBox">';
	html+='<div class="dgHdName">';
	html+='<span>'+obj.name+'</span>';
	html+='</div>';
	html+='<div class="dgContent">';
	html+='<div class="detailMenu">';
	html+='<ul class="ca cf">';            				
	html+='<li onClick="getUnitInfo1(this);" class="activeLink toc">基本信息</li>';
	html+='<li onClick="getUnitInfo2(this);" class="toc">厂区平面图</li>';
	html+='<li onClick="getUnitInfo3(this);" class="toc">企业危化品</li>';
	html+='</ul>';
	html+='</div>';
	html+='<div class="jj1 borderNew">';
	html+='<div class="information ca cf">'; 
	html+='<ul class="ifBar">';
	html+='<li class="ca cf bgColor"><label class="staff fl">总供水能力：</label><span class="fl plant" style="color:#15a4fa;font-weight:bold;font-size:16px;">'+obj.waterSupply+'(L/S)</span></li>';
	html+='<li class="ca cf"><label class="staff fl">消防管理人员：</label><span class="fl plant">'+obj.addr+'</span></li>';
	html+='<li class="ca cf bgColor"><label class="staff fl">单位值班电话：</label><span class="fl plant">'+obj.tel+'</span></li>';
	html+='<li class="ca cf"><label class="staff fl">所属消防战队：</label><span class="fl plant">'+obj.fireBrigadeName+'</span></li>';
	html+='<li class="ca cf bgColor"><label class="staff fl">总体概况：</label><span class="fl plant">'+obj.desc+'</span></li>';
	html+='<li class="ca cf"><label class="staff fl">地址：</label><span class="fl plant">'+obj.addr+'</span></li>';
	html+='</ul>';
	html+='<div class="keyParts mb20">';
 	html+='<div class="keyPartsHead"><i></i>重点部位</div>';
 	html+='<div class="keyPartsUl ca cf">';
 	html+='<ul>';
	if(obj.keyPartsList.length>0){
		for(var j=0;j<obj.keyPartsList.length;j++){
			var curr =obj.keyPartsList[j];
			html+='<li>'+curr.name+'</label></li>';
		}
	}
	html+='</ul>';
 	html+='</div>';
 	html+='</div>';
 	//消防隐患及事故信息
 	html+='<div class="keyParts">';
 	html+='<div class="keyPartsHead"><i></i>消防隐患及事故信息</div>';
 	html+='<div class=" faultDiv ca cf">';
 	html+='<ul>';
	if(obj.accidentFileList.length>0){
		for(var j=0;j<obj.accidentFileList.length;j++){
			var curr =obj.accidentFileList[j];
			html+='<li class="bgColor" onclick="downLoadFile(\''+curr.url+'\',\''+curr.name+'\')">'+curr.name+'</li>';
		}
	}
	html+='</ul>';
 	html+='</div>';
 	html+='</div>';
	html+='</div>';
	//图片列表
	html+='<div class="planBox ca cf" style="display:none;" >';
	html+='<div class="fl planbig">';
	html+='<div class="tabs9_context">';
	if(obj.fileList.length>0){
		html+='<img src="'+baseFileUrl+obj.fileList[0].url+'" alt="" id="mainShwoImage"/>';	
	}
	html+='</div>';
	if(obj.fileList.length>0){
		html+='<a id="uploadPicUrl" href="javascript:void(0);" onclick="downLoadFile(\''+obj.fileList[0].url+'\',\''+obj.fileList[0].url+'\')" ><span class="uploadPic"></span></a>';
	}
	html+='</div>';
	html+='<div class="fl planSmall">';
	html+='<div class="topUp" id="lBtn_vd9"></div>';
	html+='<div class="planSmallList">';
	html+='<ul id="planSmallListUl">';
	for(var j=0;j<obj.fileList.length;j++){
		if(j==0){
			html+='<li class="tocli planSelect"><img src="'+baseFileUrl+obj.fileList[j].url+'" alt="" /></li>';
		}else{
			html+='<li class="tocli cur"><img src="'+baseFileUrl+obj.fileList[j].url+'" alt="" /></li>';
		}
	}
	html+='</ul>';
	html+='</div>';
	html+='<div class="bottomDown" id="rBtn_vd9" ></div>';
	html+='</div>';
	html+='</div>';
	//危化品列表数据   dangersList
	html+='<div class="dangerList ca cf" id="weihuapinList" style="display:none;">'; 
	html+='<ol>';
		for(var i=0;i<obj.dangersList.length;i++){
			var dangersObj = obj.dangersList[i];
			html+='<li onclick="showDngerDetail(\''+dangersObj.id+'\')">';
			html+='<div class="dangerPic"><img src="'+baseFileUrl+dangersObj.imgPath+'" alt=""></div>';
			html+='<div class="dangerDetils">';
			html+='<h2>'+dangersObj.cName+'</h2>';
			html+='<p><span class="tiltetxt">危化品类别：</span><span class="tilteName">'+dangersObj.typeName+'</span></p>';
			html+='<p><span class="tiltetxt">CAS：</span><span class="tilteName">'+dangersObj.casNum+'</span></p>';
			html+='<p><span class="tiltetxt">UN编码：</span><span class="tilteName">'+dangersObj.unNum+'</span></p>';
			html+='</div>';
			html+='</li>';
		}									
	html+='</ol>';
	html+='</div>';
	//单个危化品的详情
	html+='<div class="danger" style="display:none;" id="dangerDetailDiv">';
	html+='</div>';
	return html;
		
}


/*用于社会资源详情*/
function getShzyDetailForMarkHtml(obj){
	var html='<div class="urgent">';
	html+='<div class="dgBox">';
	html+='<div class="dgHdName">';
	html+='<span>'+obj.name+'</span>';
	html+='</div>';
	html+='<div class="dgContent">';
	html+='<div class="detailMenu">';
	html+='<ul class="ca cf">';            				
	html+='<li onClick="getUnitInfo1(this);" class="activeLink toc">基本信息</li>';
	html+='<li onClick="getUnitInfo2(this);" class="toc">厂区平面图</li>';
//	html+='<li onClick="getUnitInfo3(this);" class="toc">企业危化品</li>';
	html+='</ul>';
	html+='</div>';
	html+='<div class="jj1 borderNew">';
	html+='<div class="information ca cf">'; 
	html+='<ul class="ifBar">';
	html+='<li class="ca cf bgColor"><label class="staff fl">总供水能力：</label><span class="fl plant" style="color:#15a4fa;font-weight:bold;font-size:16px;">'+obj.waterSupply+'(L/S)</span></li>';
	html+='<li class="ca cf"><label class="staff fl">消防管理人员：</label><span class="fl plant">'+obj.addr+'</span></li>';
	html+='<li class="ca cf bgColor"><label class="staff fl">单位值班电话：</label><span class="fl plant">'+obj.tel+'</span></li>';
	html+='<li class="ca cf"><label class="staff fl">所属消防战队：</label><span class="fl plant">'+obj.fireBrigade+'</span></li>';
	html+='<li class="ca cf bgColor"><label class="staff fl">总体概况：</label><span class="fl plant">'+obj.desc+'</span></li>';
	html+='<li class="ca cf"><label class="staff fl">地址：</label><span class="fl plant">'+obj.addr+'</span></li>';
	html+='</ul>';
//	html+='<div class="keyParts mb20">';
// 	html+='<div class="keyPartsHead"><i></i>重点部位</div>';
// 	html+='<div class="keyPartsUl ca cf">';
// 	html+='<ul>';
//	if(obj.keyPartsList.length>0){
//		for(var j=0;j<obj.keyPartsList.length;j++){
//			var curr =obj.keyPartsList[j];
//			html+='<li>'+curr.name+'</label></li>';
//		}
//	}
//	html+='</ul>';
// 	html+='</div>';
// 	html+='</div>';
 	//消防隐患及事故信息
// 	html+='<div class="keyParts">';
// 	html+='<div class="keyPartsHead"><i></i>消防隐患及事故信息</div>';
// 	html+='<div class=" faultDiv ca cf">';
// 	html+='<ul>';
//	if(obj.accidentFileList.length>0){
//		for(var j=0;j<obj.accidentFileList.length;j++){
//			var curr =obj.accidentFileList[j];
//			html+='<li class="bgColor" onclick="downLoadFile(\''+curr.url+'\',\''+curr.name+'\')">'+curr.name+'</li>';
//		}
//	}
//	html+='</ul>';
// 	html+='</div>';
// 	html+='</div>';
//	html+='</div>';
	html+='</div>';
	html+='</div>';
	//图片列表
	html+='<div class="planBox ca cf" style="display:none;" >';
	html+='<div class="fl planbig">';
	html+='<div class="tabs9_context">';
	if(obj.fileList.length>0){
		html+='<img src="'+baseFileUrl+obj.fileList[0].url+'" alt="" id="mainShwoImage"/>';	
	}
	html+='</div>';
	if(obj.fileList.length>0){
		html+='<a id="uploadPicUrl" href="javascript:void(0);" onclick="downLoadFile(\''+obj.fileList[0].url+'\',\''+obj.fileList[0].url+'\')" ><span class="uploadPic"></span></a>';
	}
	html+='</div>';
	html+='<div class="fl planSmall">';
	html+='<div class="topUp" id="lBtn_vd9"></div>';
	html+='<div class="planSmallList">';
	html+='<ul id="planSmallListUl">';
	for(var j=0;j<obj.fileList.length;j++){
		if(j==0){
			html+='<li class="tocli planSelect"><img src="'+baseFileUrl+obj.fileList[j].url+'" alt="" /></li>';
		}else{
			html+='<li class="tocli cur"><img src="'+baseFileUrl+obj.fileList[j].url+'" alt="" /></li>';
		}
	}
	html+='</ul>';
	html+='</div>';
	html+='<div class="bottomDown" id="rBtn_vd9" ></div>';
	html+='</div>';
	html+='</div>';
	//危化品列表数据   dangersList
//	html+='<div class="dangerList ca cf" id="weihuapinList" style="display:none;">'; 
//	html+='<ol>';
//		for(var i=0;i<obj.dangersList.length;i++){
//			var dangersObj = obj.dangersList[i];
//			html+='<li onclick="showDngerDetail(\''+dangersObj.id+'\')">';
//			html+='<div class="dangerPic"><img src="'+baseFileUrl+dangersObj.imgPath+'" alt=""></div>';
//			html+='<div class="dangerDetils">';
//			html+='<h2>'+dangersObj.cName+'</h2>';
//			html+='<p><span class="tiltetxt">危化品类别：</span><span class="tilteName">'+dangersObj.typeName+'</span></p>';
//			html+='<p><span class="tiltetxt">CAS：</span><span class="tilteName">'+dangersObj.casNum+'</span></p>';
//			html+='<p><span class="tiltetxt">UN编码：</span><span class="tilteName">'+dangersObj.unNum+'</span></p>';
//			html+='</div>';
//			html+='</li>';
//		}									
//	html+='</ol>';
//	html+='</div>';
//	//单个危化品的详情
//	html+='<div class="danger" style="display:none;" id="dangerDetailDiv">';
//	html+='</div>';
	return html;
}

/**
 * 下载文件
 * @returns
 */
function downLoadFile(filePath,fileName){
	var param = {
			filePath:encodeURIComponent(filePath),
			fileName:encodeURIComponent(fileName)
	}
	$.ajax({
		type:'POST',
		url: baseUrl+'/common/is-exit.jhtml',
		data: param,
		dataType:'JSON',
		success: function(data){
			if(data.httpCode==200){
				window.open(baseUrl+"/common/downLoadFile.jhtml?param="+encodeURIComponent(JSON.stringify(param)));
			}else{
				layer.msg(data.msg, {time: 2000});
			}
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	})
}


function unitUploadPic(){
	window.open(baseUrl+"/execl/downLoadExpertTemplate.jhtml");
}

function showDngerDetail(oid){
	var  params =oid;
    $.ajax({
		type : "post",
		url : baseUrl+"/webApi/qryKeyUnitDangersById.jhtml",
		data: JSON.stringify(params),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				var html = showDangerDetailDiv(data.data);
				$("#dangerDetailDiv").html(html);
				$("#weihuapinList").hide();
				$("#dangerDetailDiv").show();
			}else{
				layer.msg(networkErrorMsg, {time: 2000});
			}
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	})
}



//展示企业危化品详细信息
function showDangerDetailDiv(obj){
	var jkwh = obj.healthHazards;
	var hjwh =  obj.environmentalHazards;
	var rbwh = obj.fireExplosionDanger;
	var pfwh = obj.skinContact;
	var yjwh = obj.eyeContact;
	var xrwh = obj.inhalation;
	var srwh = obj.feedInto;
	var wxtx = obj.hazardCharacteristics;
	var yhrscw = obj.harmfulCombustionProducts;
	var mhff = obj.fireExtinguishingMethod;
	var czzysx = obj.attentionOperation;
	var cczysx = obj.precautionsStorage;
	var hxxtfh = obj.respiratorySystemProtection;
	var yjfh = obj.eyeProtection;
	var stfh = obj.bodyProtection;
	var sfh = obj.handProtection;
	
	var html='';
	html+='<div class="dangerMsg ca cf">';
	html+='<div class="fl dangerImg">';
	html+='<img src="'+baseFileUrl+obj.imgPath+'" alt="" />';
	html+='</div>';
	html+='<div class="fl describe">';
	html+='<h2 class="describeTilte">'+obj.cName+'</h2>';
	html+='<div class="describe">';
	html+='<ul class="ca cf">';
	html+='<li>';
	html+='<span class="dbname fl">英文名称</span><span class="dbproperty fl">'+obj.eName+'</span>';
	html+='</li>';
	html+='<li>';
	html+='<span class="dbname fl">CAS No</span><span class="dbproperty fl">'+obj.casNum+'</span>';
	html+='</li>';
	html+='<li>';
	html+='<span class="dbname fl">分子式</span><span class="dbproperty fl">'+obj.molecularFormula+'</span>';
	html+='</li>';
	html+='<li>';
	html+='<span class="dbname fl">主要成分</span><span class="dbproperty fl">'+obj.mainComponents+'</span>';
	html+='</li>';
	html+='<li>';
	html+='<span class="dbname fl">UN编号</span><span class="dbproperty fl">'+obj.unNum+'</span>';
	html+='</li>';
	html+='</ul>';
	html+='</div>';
	html+='</div>';
	html+='</div>';
	html+='<div class="dangerMsg ca cf">';
	html+='<div class="dangerNav fl">';
	html+='<ul id="dangerDetailsDetailUl" >';
	html+='<li onclick="showDangerDetailsDetail(0,this,\''+jkwh+'\')" class="oncurrentTab" ><a href="javascript:void(0)"><i></i><span>危害程度</span></a></li>';
	html+='<li onclick="showDangerDetailsDetail(1,this,\''+wxtx+'\')"><a href="javascript:void(0)"><i></i><span>消防措施</span></a></li>';
	html+='<li onclick="showDangerDetailsDetail(2,this,\''+jkwh+'\')"><a  href="javascript:void(0)"><i></i><span>泄漏应急处理</span></a></li>';
	html+='<li onclick="showDangerDetailsDetail(3,this,\''+czzysx+'\')"><a  href="javascript:void(0)"><i></i><span>操作处置与存储</span></a></li>';
	html+='<li onclick="showDangerDetailsDetail(4,this,\''+hxxtfh+'\')"><a  href="javascript:void(0)"><i></i><span>个人防护</span></a></li>';
	html+='<li onclick="showDangerDetailsDetail(5,this,\'0\')"><a  href="javascript:void(0)"><i></i><span>化学属性</span></a></li>';
	html+='</ul>';
	html+='</div>';
	html+='<div class="fl dangerCont" id="dangerDetailsDetail">';
	//危害程度
	html+='<div class="tag" id="whcdDiv">';
	html+='<ul class="ca cf">';
	html+='<li onclick="detailButtonTag(\''+jkwh+'\')" class="selectActive">健康危害</li>';
	html+='<li onclick="detailButtonTag(\''+hjwh+'\')">环境危害</li>';
	html+='<li onclick="detailButtonTag(\''+rbwh+'\')">燃爆危害</li>';
	html+='<li onclick="detailButtonTag(\''+pfwh+'\')">皮肤接触</li>';
	html+='<li onclick="detailButtonTag(\''+yjwh+'\')">眼睛接触</li>';
	html+='<li onclick="detailButtonTag(\''+xrwh+'\')">吸入</li>';
	html+='<li onclick="detailButtonTag(\''+srwh+'\')">食入</li>';
	html+='</ul>';
	html+='</div>';
	//消防措施
	html+='<div class="tag" id="xfcsDiv" style="display:none;">';
	html+='<ul class="ca cf">';
	html+='<li onclick="detailButtonTag(\''+wxtx+'\')">危险特性</li>';
	html+='<li onclick="detailButtonTag(\''+yhrscw+'\')">有害燃烧产物</li>';
	html+='<li onclick="detailButtonTag(\''+mhff+'\')">灭火方法</li>';
	html+='</ul>';
	html+='</div>';
	//泄漏应急处理
	
	//操作处置与存储
	html+='<div class="tag" id="czczyccDiv" style="display:none;">';
	html+='<ul class="ca cf">';
	html+='<li onclick="detailButtonTag(\''+czzysx+'\')">操作注意事项</li>';
	html+='<li onclick="detailButtonTag(\''+cczysx+'\')">储存注意事项</li>';
	html+='</ul>';
	html+='</div>';
	//个人防护
	html+='<div class="tag" id="grfhDiv" style="display:none;">';
	html+='<ul class="ca cf">';
	html+='<li onclick="detailButtonTag(\''+hxxtfh+'\')">呼吸系统防护</li>';
	html+='<li onclick="detailButtonTag(\''+yjfh+'\')">眼睛防护</li>';
	html+='<li onclick="detailButtonTag(\''+stfh+'\')">身体防护</li>';
	html+='<li onclick="detailButtonTag(\''+sfh+'\')">手防护</li>';
	html+='</ul>';
	html+='</div>';
	//化学属性
	//tabtn显示的内容
	html+='<div class="dangerTxt" id="whcdDivDetail">'+jkwh+'</div>';
	//	化属	
	html+='<div class="dangerTxt" id="hxsxDivDetail" style="display:none;">';
	html+='外观与性状  熔点(℃):'+obj.appearanceCharacter+'沸点(℃):'+obj.meltingPoint+'相对密度(水=1):'+obj.relativeDensity+'相对蒸气密度(空气=1)'+obj.relativeSteam+'饱和蒸气压(kPa)'+obj.saturatedVaporPressure+'燃烧热(kJ/mol)'+obj.burningHeat+'临界温度(℃)'+obj.criticalTemperature+'临界压力(MPa)'+obj.criticalPressure+'闪点(℃):'+obj.flashPoint+'引燃温度(℃):'+obj.IgnitionTemperature+'爆炸上限%(V/V):'+obj.upperLimitExplosion+'爆炸下限%(V/V)'+obj.lowerLimitExplosion+'溶解性:'+obj.solubility+'。';
	html+='</div>';
	html+='</div>';
	html+='</div>';
	html+='</div>'; 
	return html;
}

function detailButtonTag(divDetail){
	//修改初始化及选中样式 todo
	$("#whcdDivDetail").html(divDetail);
}

function showDangerDetailsDetail(type,obj,content){
	$("#dangerDetailsDetailUl li").removeClass('oncurrentTab');
	$(obj).addClass('oncurrentTab');
	$("#whcdDiv ul li").removeClass('selectActive');
	$(obj).addClass('selectActive');
	$("#whcdDivDetail").html(content);
	switch(type)
    {
    	case 0:
            $("#whcdDiv").show();
            $("#xfcsDiv").hide();
            $("#grfhDiv").hide();
            $("#czczyccDiv").hide();
            $("#whcdDivDetail").show();
            $("#hxsxDivDetail").hide();
            break;
    	case 1:
    		 $("#xfcsDiv").show();
    		 $("#whcdDiv").hide();
             $("#czczyccDiv").hide();
             $("#grfhDiv").hide();
             $("#whcdDivDetail").show();
             $("#hxsxDivDetail").hide();
            break;
        case 2:
        	 $("#whcdDiv").hide();
             $("#xfcsDiv").hide();
             $("#grfhDiv").hide();
             $("#czczyccDiv").hide();
             $("#whcdDivDetail").show();
             $("#hxsxDivDetail").hide();
            break;
        case 3:
        	 $("#whcdDiv").hide();
             $("#xfcsDiv").hide();
             $("#czczyccDiv").show();
             $("#grfhDiv").hide();
             $("#whcdDivDetail").show();
             $("#hxsxDivDetail").hide();
            break;
        case 4:
        	 $("#whcdDiv").hide();
             $("#xfcsDiv").hide();
             $("#czczyccDiv").hide();
             $("#grfhDiv").show();
             $("#whcdDivDetail").show();
             $("#hxsxDivDetail").hide();
            break;
        case 5:
        	 $("#whcdDiv").hide();
             $("#xfcsDiv").hide();
             $("#grfhDiv").hide();
             $("#czczyccDiv").hide();
             $("#whcdDivDetail").hide();
             $("#hxsxDivDetail").show();
            break;
    }
}




function addEvent(){
	var numLi = $("#planSmallListUl .tocli").length, time, i = 0, auto = true,moveNum=1;
	$("#planSmallListUl li").each(function(i){
		$(this).click(function(){
			$(this).addClass("planSelect").siblings().removeClass("planSelect"); //切换选中的按钮高亮状态
	       // $("#planSmallListUl li").removeClass('planSelect').eq(i).addClass('planSelect');
	        var src = $(this).find('img').attr('src');
	        $("#mainShwoImage").attr("src",src);
	        $('#uploadPicUrl').attr('href',src); 
	    })
	})
	
	$("#lBtn_vd9").click(function () {
		if(moveNum>1){
			$("#planSmallListUl").animate({
				marginTop:'+=77px'
			});
			moveNum--;
		}
	})
	
	$("#rBtn_vd9").click(function () {
		if(moveNum<numLi-5){
			$("#planSmallListUl").animate({
				   marginTop:'-=77px'
			});
			moveNum++;
		}
	})

	
}

/*隐藏显示应急预案详情*/
function getReservePlanDetail(url){	
    $("#planLayerDetailId").show();	 
  
}	

//应急预案显示文档
function showAccidentCaseFile(accidentCaseName,swfPath){
	$('#accidentCaseName').text(accidentCaseName)
	$.when($("#accidentCaseDocview").show()).then(function (d) {
		loadDoc(baseFileUrl +swfPath,"accidentCaseDocview");
	});
	
}

//应急预案显示文档
function showReservePlandoc(id){
	$.ajax({
		type : "post",
		url : baseUrl+"/plan/getPlansInfo.jhtml",
		data: id,
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				var docview = $('#ReservePlandocview');
				var viewDocId="ReservePlandocview";
				docview.find('.docName').text(data.data.name);
				docview.find('.docName2').text(data.data.name);
				docview.find('.docLX').text(data.data.typeName);
				var docML = docview.find('.docML');
				docML.html('');
				if(data.data.plansCatalogs){
					$.each(data.data.plansCatalogs, function(i,item){  
						var html = "";
						html = $("<li docml='" + item.page + "'><a href='javascript:gotoPage(\""+ item.page +"\",\""+viewDocId+"\")'><span></span>" + item.name + "</a></li>");
						docML.append(html);
						if(i == 0){
							$(html).addClass('activeOn');
						}
					});
				}
				//$('#ReservePlandocview .docML').html(html);
				if(data.data.swfPath!=""){
					$.when($("#ReservePlandocview").show()).then(function (d) {
						loadDoc(baseFileUrl + data.data.swfPath,"ReservePlandocview");
					});
				}
				
			}else{
				layer.msg(systemAbnormalityMsg, {time: 2000});
			}
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	})
}


/*关闭物资储备点详情事件*/
function reWarpBtn(){
	$(".reserve").empty().hide();
}

//事故点
function pointBtnClick(){
	if(!$._Accident.isInroom()){
		$._Accident.joinRoom();
		return;
	}
	var accident = $._Accident.accident;
	var params ={};
	params.clusterType='accident';
	params.imagePath="/mapIcon/mapFixedIcon/sgd.png";
	params.isCluster=true;
	params.clusterImagePath="/mapIcon/iconCluster/sgd_c.png";
	params.isSyn=true;
	params.isSave=true;
	params.recordType='6';
	params.action="事故点";
	if(accident){
		params.name=accident.accidentName;
		params.desc=accident.accidentRemark;
		params.accidentType = accident.accidentType;
	}
	addMakerEngine(params);
	//事故点特殊属性 todo 
//	params.groundHeight;
//	params.desc;
//	params.type;
}

/*往地图上扎消防车*/
function makerFireEngineEvent(obj){
	var params ={};
	params = obj;
	params.oid=obj.id;
	if(obj.foamTypeName){
		params.foamTypeName=obj.foamTypeName;//泡沫类型
		params.foamTypeId=obj.foamTypeId;//泡沫类型
	}
	if(typeof(obj.spraySpeed)!="undefined" && typeof(obj.water)!="undefined" && typeof(obj.capacity)!="undefined"){
		params.spraySpeed=obj.spraySpeed;//速度
		params.water=obj.water;//水量
		params.capacity=obj.capacity;//干粉量
	}
	params.clusterType='xfc';
	params.catecd='xfc';
	params.imagePath=obj.iconPath;
	params.isCluster=true;
	params.clusterImagePath="/mapIcon/iconCluster/xsc_c.png";
	params.isSyn=true;
	params.name = obj.plateNumber;//名称是消防车号
	params.isSave=true;
	params.type="marker";
	params.recordType=obj.iconType;
	params.action="消防车";
	params.sprayTime=obj.sprayTime;
	if($('li[makerFireEngineoid="'+params.oid+'"]').hasClass('disable')){
		return;
	}else{
		addMakerEngine(params);
	}
}


//图层面板扎图标
function makerFireEngine(dtPath,id,iconType,name,jhPath,clusterType){
	var params ={};
	params.oid=id;
	params.name=name;
	params.recordType=iconType;
	params.clusterType=clusterType;
	params.catecd=clusterType;
	params.imagePath=dtPath;
	params.isCluster=true;
	params.clusterImagePath=jhPath;
	params.isSyn=true;
	params.isSave=true;
	params.iconType=iconType;
	params.action="标注";
	addMakerEngine(params);
}

//救援路线
function makerEscapeLineEngine(iconType,name,scolor){
	if(isDrawing){
		layer.msg("有未绘制完的图形", {time: 1000});
		return;
	}
	isDrawing = true;
	var params ={};
	params.name=name;
	params.isSyn=true;
	params.isSave=true;
	params.recordType="10";
	params.action="救援路线";
	params.scolor=scolor;
	addSpecialLineEvent('jylx',params);
}

//会商绘标
function makerConsultationEngine(type,iconType){
	if(isDrawing){
		layer.msg("有未绘制完的图形", {time: 1000});
		return;
	}
	isDrawing = true;
	var params ={};
	params.isSyn=true;
	params.isSave=true;
	params.recordType="2";
	params.action="会商绘标";
	if(type=="Curve"){
		params.scolor='#e1001a';
		params.fcolor='rgba(222, 1, 27, 0.4)';	
	}else if(type=="Ellipse"){
		params.scolor='#eafd31';
		params.fcolor='rgba(234, 253, 49, 0.4)';	
	}else if(type=="Polygon"){
		params.scolor='#ea4596';
		params.fcolor='rgba(234, 69, 150, 0.4)';	
	}else if(type=="Circle"){
		params.scolor='#58eb3f';
		params.fcolor='rgba(88, 235, 63, 0.4)';	
	}else if(type=="Sector"){
		params.scolor='#f10035';
		params.fcolor='rgba(241, 0, 53, 0.4)';	
	}else if(type=="Lune"){
		params.scolor='#426dff';
		params.fcolor='rgba(66, 105, 255, 0.4)';	
	}else if(type=="ClosedCurve"){
		params.scolor='#454545';
		params.fcolor='rgba(69, 69, 69, 0.4)';	
	}else if(type=="GatheringPlace"){
		params.scolor='#69ffe4';
		params.fcolor='rgba(105, 255, 228, 0.4)';	
	}else if(type=="DoubleArrow"){
		params.scolor='#eafd31';
		params.fcolor='rgba(234, 253, 49, 0.4)';	
	}else if(type=="StraightArrow"){
		params.scolor='#976924';
		params.fcolor='rgba(151, 105, 36, 0.4)';	
	}else if(type=="FineArrow"){
		params.scolor='#1a41a6';
		params.fcolor='rgba(26, 65, 166, 0.4)';	
	}else if(type=="AssaultDirection"){
		params.scolor='#ea36dc';
		params.fcolor='rgba(234, 54, 220, 0.4)';	
	}else if(type=="AttackArrow"){
		params.scolor='#5155e2';
		params.fcolor='rgba(81, 85, 226, 0.4)';	
	}else if(type=="TailedAttackArrow"){
		params.scolor='#e2a02a';
		params.fcolor='rgba(226, 160, 42, 0.4)';	
	}else if(type=="SquadCombat"){
		params.scolor='#70d87d';
		params.fcolor='rgba(107, 226, 126, 0.4)';	
	}else if(type=="TailedSquadCombat"){
		params.scolor='#5feffe';
		params.fcolor='rgba(95, 239, 254, 0.4)';	
	}else if(type=="TriangleFlag"){
		params.scolor='#f37b40';
		params.fcolor='rgba(243, 123, 64, 0.4)';	
	}
	consultationMarkDraw(type,params);
}

//调用应物资
function useEmergencyMaterial(oid,point,oname){
	if(!$._Accident.isInroom()){
		$._Accident.joinRoom();
		return;
	}
	var useNum = parseInt($("#"+oid+"S").val().replace(/\s*/g,""));
	var countNum = parseInt($("#"+oid+"C").html().replace(/\s*/g,""));
	if(countNum<useNum){
		layer.msg("调用数量大于库存量", {time: 1000});
		return;
	}
	var params={};
	params.id=oid;
	params.storageQuantity=useNum;
    $.ajax({
		type : "post",
		url : baseUrl+"/webApi/updateEmergencyMaterial.jhtml",
		data: JSON.stringify(params),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				$("#"+oid+"C").html(countNum-useNum);
				$("#"+oid+"S").val(countNum-useNum);
			}
			var params = {};
			params.id =getTimestamp();
			params.roomId = currentRoomId;
			params.action = "物资调用";
			params.actionDesc = "从"+point+"调用应急物资"+oname+"("+useNum+")个";
			params.type = 4;
			params.isDelete = 1;//删除0，添加1
			params.createBy = currentUserId; //修改等路人 todo
			params.director = currentUserName; //修改等路人 todo
			saveRecord(params);
		},error: function(request){
			layer.msg(networkErrorMsg, {time: 2000});
        }
	 })
}

/*往地图上扎消防车*/
function changeFireEngine(id){
	$("#"+id).find("img").attr("src","../images/huisetu.png");
	$("#"+id).find("[class='txtName']").eq(1).text("0辆");
	$("#"+id).removeAttr('onclick');
}

/*显示重点单位详情*/
function getSpecialistDetail(obj){
    if($("#specialistDetailId").is(":hidden")){
		//$("#specialistDetailId").show(); 
		var html='<div class="urgent">';
		html+='<div class="dgBox">';
		html+='<div class="dgHdName">';
		html+='<span>专家信息查看</span>';
		html+='</div>';
		html+='<div class="dgContent">';
		html+='<div class="information ca cf docur">';
		html+='<ul class="ifBar" style="padding-top:0;">';
		html+='<li  class="ca cf bgColor" class="ca cf bgColor"><label class="staff fl">专家姓名：</label><span class="fl plant">'+obj.name+'</span></li>';
		html+='<li  class="ca cf"><label class="staff fl">专家类型：</label><span class="fl plant">'+obj.expertTypeName+'</span></li>';
		html+='<li  class="ca cf bgColor"><label class="staff fl">专家电话：</label><span class="fl plant">'+obj.phone+'</span></li>';
		html+='<li  class="ca cf"><label class="staff fl">专业：</label><span class="fl plant">'+obj.major+'</span></li>';
		html+='<li  class="ca cf bgColor"><label class="staff fl">主修科目：</label><span class="fl plant">'+obj.majorSubject+'</span></li>';
		html+='<li  class="ca cf"><label class="staff fl">所属单位：</label><span class="fl plant">'+obj.unit+'</span></li>';
		html+='<li  class="ca cf bgColor"><label class="staff fl">特长：</label><span class="fl plant">'+obj.speciality+'</span></li>';
		html+='</ul>';
		html+='</div>';		
	    html+='<div onClick="closeExpertBtn();" class="closeBtn"></div>';
		html+='</div>';
		html+='</div>';
		html+='</div>';
		html+='</div>';
		$("#specialistDetailId").append(html);
	}else{
		closeExpertBtn();
	}
}


function closeExpertBtn(){
	$("#specialistDetailId").empty();
    $("#specialistDetailId").hide();	   
}

/*点击元素描绘事件*/
function elementPlotting(flag){
	if(flag==0){
		if($("#iconBox").is(":hidden")){
			$("#iconBoxZNTJ").hide();
			$("#iconBox").show();
		}else{
			$("#iconBox").hide();
		}	
	}else{
		if($("#iconBoxZNTJ").is(":hidden")){
			$("#iconBox").hide();
			$("#iconBoxZNTJ").show();
		}else{
			$("#iconBoxZNTJ").hide();
		}
	}
}
/**添加摄像头配置**/
function addCameraSetting(isAddClose,videoType){
	var index;
	if(videoType == 1){
		index = $("#connCameraSettingList").children().length;
	}else{
		index = $("#cameraSettingList").children().length;
	}
	index++;
	if(index > 2){
		for(var i=2;i<index;i++){
			$("#cameraClose_"+i).hide();
		}
	}
	var html = "";
	if(videoType==1){
		html += '<div class="cameraList mb30">';
		if(isAddClose){
			html +='<img id="cameraClose_'+index+'" style="float:right; padding:5px;"  src="'+baseUrl+'/images/minColse.png" onclick="deleteCameraSetting(this,'+videoType+')" alt="" />'
		}
		html += '<input type="hidden" id="cameraSerial_'+index+'" value="'+index+'">';
		html += '<div class="cameraHead"><span>相机'+index+'</span></div>';
		html += '<div class="cameraWarp"><div class="ca cf mb10">';
		html += '<div class="fl"><div class="ca cf"><label class="lab">IP：</label><div class="inBox"><input type="text" id="cameraIp_'+index+'"/></div></div></div>';
		html += '<div class="fr"><div class="ca cf"><label class="lab">端口：</label><div class="inBox"><input type="text" id="cameraPort_'+index+'" /></div></div></div>'
		html += '</div>';
		html += '<div class="ca cf">';
		html += '<div class="fl mr10"><div class="ca cf"><label class="lab">用户：</label><div class="inBox"><input type="text" id="cameraAccount_'+index+'"/></div></div></div>';
		html += '<div class="fr"><div class="ca cf"><label class="lab">密码：</label><div class="inBox"><input id="cameraPassword_'+index+'"/></div></div></div></div>';
		html += '</div></div>';
		$("#connCameraSettingList").append(html);
	}else{
		html = "<div class='cameraList mb30'>";
		if(isAddClose){
			html +='<img id="cameraClose_'+index+'" style="float:right; padding:5px;"  src="'+baseUrl+'/images/minColse.png" onclick="deleteCameraSetting(this,'+videoType+')" alt="" />'
		}
		html +='<input type="hidden" id="cameraSerial4G_'+index+'" value="'+index+'">';
		html +='<div class="cameraHead"><span>相机'+index+'</span></div>';
		html += '<div class="cameraWarp"><div class="ca cf mb10">';
		html += '<div class="fl mr10"><div class="ca cf"><label class="lab">用户：</label><div class="inBox"><input type="text" id="cameraAccount4G_'+index+'"/></div></div></div>';
		html += '<div class="fr"><div class="ca cf"><label class="lab">密码：</label><div class="inBox"><input id="cameraPassword4G_'+index+'"/></div></div></div></div>';
		html += '<div class="ca cf"><div class="fl mr10"><div class="ca cf"><label class="lab">编号：</label><div class="inBox"><input type="text" id="cameraDeviceNo4G_'+index+'" /></div></div></div></div></div></div>';
		$("#cameraSettingList").append(html);
	}
}
/**删除相机配置**/
function deleteCameraSetting(_this,videoType){
	if(confirm('确认要删除配置吗?')){
		$(_this).parent("div").remove();
		var len;
		if(videoType==1){
			len =  $("#connCameraSettingList").children().length;
		}else{
			len =  $("#cameraSettingList").children().length;
		}
		if(len>1){
			$("#cameraClose_"+len).show();
		}
	}
}
/*保存相机的信息*/
function saveCameraInfo(){
	var lockValue = $("#configureLockId").attr("class");
	if (lockValue == "shutIcon") {
		layer.msg("请先进行解锁！！！", {time: 1000});
	}else{
		var cameraList=[];
		var len = 1;
		if(videoType==1){
			len =  $("#connCameraSettingList").children().length;
		}else{
			len =  $("#cameraSettingList").children().length;
		} 
		for(var i=1;i<=len;i++){
			if(videoType==1){
				var serial=$("#cameraSerial_"+i).val();
				var ip=$("#cameraIp_"+i).val();
				var port=$("#cameraPort_"+i).val();
				var account=$("#cameraAccount_"+i).val();
				var password=$("#cameraPassword_"+i).val();
				if(ip==null||ip==""||port==null||port==""||account==null||account==""||password==null||password=="")continue;
				var params={"ip":ip,"serial":serial,"port":port,"account":account,"password":password};
				cameraList.push(params);
			}else{
				var serial=$("#cameraSerial4G_"+i).val();
				var account=$("#cameraAccount4G_"+i).val();
				var password=$("#cameraPassword4G_"+i).val();
				var deviceNo=$("#cameraDeviceNo4G_"+i).val();
				if(account==null||account==""||password==null||password==""||deviceNo==null||deviceNo=="")continue;
				var params={"serial":serial,"account":account,"password":password,"deviceNo":deviceNo};
				cameraList.push(params);
			}	
		}
	    $.ajax({
			type : "post",
			url : baseUrl+"/webApi/cameraSet/create.jhtml",
			data: JSON.stringify(cameraList),
			dataType:"json",
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {	
				if(data.httpCode==200){
					layer.msg("相机设置成功", {time: 1000});
				}
				
			},error: function(request) {
				layer.msg(networkErrorMsg, {time: 2000});
	        }
		})
	}
}

/*显示隐藏配置页面*/
function showConfigureEvent(){
	$("#alarmOpenMove").css('right','-1px');
	$("#alarm").removeClass("ZindexCss");
	$("#alarmOpenMove").removeClass("alarmOpenCollect");
	$("#policeBJ").fadeOut();
	$("#starsDiv").css('right','10px');
	$("#ysslDiv").css('right','45px');
	$("#toolMove").css('right','30px');
	$("#toolBoxBTN").css('right','40px');
	$("#iconBox").css('right','93px');
	//关闭小视频
	closeMiniCamera();
	$("#policeBJ").hide();
	$('#configureLockId').removeClass('openIcon').addClass('shutIcon');
	$("#pzjmPanel").show();
	$("#videoNavSPPZ ul").each(function(){
	      $(this).find("li").first().addClass("onActive").siblings().removeClass("onActive");
	    });
	$("#contBox1").show();
	$("#contBox2").hide();
	$("#contBox3").hide();
	$("#contBox4").hide();
	//getCameraInfoAjax();
	if(videoType==1){
		 $("#sxtpzVideoType").html("视频配置(连线版)");
	 }else{
		 $("#sxtpzVideoType").html("视频配置(4G版)");
	 }
	getTimeConfigureEvent();
}

function getCameraInfoAjax(){
    $.ajax({
		type : "post",
		url : baseUrl+"/webApi/cameraSet/qryAll.jhtml",
		data: null,
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				$("#connCameraSettingList").html("");
				$("#cameraSettingList").html("");
				if(data.data && data.data.length>0){
					for(j = 0,len=data.data.length; j < len; j++) {
						var curr =data.data[j];
						if(videoType==1){
							if(j>0){
								addCameraSetting(true,videoType);
								$("#cameraIp_"+(j+1)).val(curr.ip);
								$("#cameraPort_"+(j+1)).val(curr.port);
								$("#cameraAccount_"+(j+1)).val(curr.account);
								$("#cameraPassword_"+(j+1)).val(curr.password);
							}else{
								addCameraSetting(false,videoType);
								$("#cameraIp_1").val(curr.ip);
								$("#cameraPort_1").val(curr.port);
								$("#cameraAccount_1").val(curr.account);
								$("#cameraPassword_1").val(curr.password);
							}
						}else{
							if(j>0){
								addCameraSetting(true,videoType);
								$("#cameraAccount4G_"+(j+1)).val(curr.account);
								$("#cameraPassword4G_"+(j+1)).val(curr.password);	
								$("#cameraDeviceNo4G_"+(j+1)).val(curr.deviceNo);
							}else{
								addCameraSetting(false,videoType);
								$("#cameraAccount4G_1").val(curr.account);
								$("#cameraPassword4G_1").val(curr.password);	
								$("#cameraDeviceNo4G_1").val(curr.deviceNo);	
							}
						}
//						if(curr.serial=="1"){
//							if(videoType==1){
//								$("#cameraIp_1").val(curr.ip);
//								$("#cameraPort_1").val(curr.port);
//								$("#cameraAccount_1").val(curr.account);
//								$("#cameraPassword_1").val(curr.password);
//							}else{
//								$("#cameraAccount4G_1").val(curr.account);
//								$("#cameraPassword4G_1").val(curr.password);	
//								$("#cameraDeviceNo4G_1").val(curr.deviceNo);	
//							}		
//						}else if(curr.serial=="2"){
//							if(videoType==1){
//								$("#cameraIp_2").val(curr.ip);
//								$("#cameraPort_2").val(curr.port);
//								$("#cameraAccount_2").val(curr.account);
//								$("#cameraPassword_2").val(curr.password);
//							}else{
//								$("#cameraAccount4G_2").val(curr.account);
//								$("#cameraPassword4G_2").val(curr.password);
//								$("#cameraDeviceNo4G_2").val(curr.deviceNo);
//							}
//							
//						}else if(curr.serial==3){
//							if(videoType==1){
//								$("#cameraIp_3").val(curr.ip);
//								$("#cameraPort_3").val(curr.port);
//								$("#cameraAccount_3").val(curr.account);
//								$("#cameraPassword_3").val(curr.password);
//							}else{
//								$("#cameraAccount4G_3").val(curr.account);
//								$("#cameraPassword4G_3").val(curr.password);
//								$("#cameraDeviceNo4G_3").val(curr.deviceNo);
//							}
//						}else if(curr.serial==4){
//							if(videoType==1){
//								$("#cameraIp_4").val(curr.ip);
//								$("#cameraPort_4").val(curr.port);
//								$("#cameraAccount_4").val(curr.account);
//								$("#cameraPassword_4").val(curr.password);
//							}else{
//								$("#cameraAccount4G_4").val(curr.account);
//								$("#cameraPassword4G_4").val(curr.password);
//								$("#cameraDeviceNo4G_4").val(curr.deviceNo);
//							}
//						}
						
					}
					
				}else{
					addCameraSetting(false,videoType);
					layer.msg("暂无相机配置", {time: 2000});
				}
			}
			
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	})  
 }

/**
 * 连接socket
 */
var socket;

var stompClient;

var heart;

var isReconnectionSocket=false;
var beginTime = 0;//执行onbeforeunload的开始时间
var differTime = 0;//时间差

/**
 * 浏览器关闭时主动关闭websocket
 */
window.onbeforeunload = function(){
	isReconnectionSocket=false;
	if(stompClient != null){
		stompClient.disconnect(function(){
			console.log("websocket连接已主动断开，不会重连。");
		});
		stompClient=null;
	}	
	beginTime = new Date().getTime();
}

window.onunload = function (){
    differTime = new Date().getTime() - beginTime;
    if(differTime <= 3) {
        console.log("浏览器关闭")
        $.ajax({
			type : "post",
			url : baseUrl+'/webApi/logoutEsm.jhtml',
			dataType:"json",
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {	
				
			},error: function(request) {
				//layer.msg("网络错误", {time: 1000});
	        }
		});
    }else{
        console.log("浏览器刷新")
    }

}



/**
 * 断线回调
 */
var errorCallback = function(error){
	console.log("websocket连接已断线");
	isReconnectionSocket=true;
	connectState('unconnect');
	connect();
}

//传感器，气象张缓存数据
var rtData = new Array();
//初始化数据
rtData.push({name:'#0_FS#',value:0});
rtData.push({name:'#0_FX#',value:0});
rtData.push({name:'#0_PM#',value:0});
rtData.push({name:'#0_WD#',value:0});
rtData.push({name:'#0_QY#',value:0});
rtData.push({name:'#0_SD#',value:0});
rtData.push({name:'#0_ZX#',value:0});

var $socketLayerIndex = null;

function connectState(state){
	$('#websocke-state').removeClass();
	switch(state){
	case 'connect':
		$('#websocke-state').html('连接中...');
		break;
	case 'connected':
		$('#websocke-state').html('已连接');
		break;
	case 'unconnect':
		$('#websocke-state').html('连接中断');
		break;
	}
}

function connect(){
	connectState('connect');
	if(isReconnectionSocket==true && $socketLayerIndex==null){
		$socketLayerIndex=layer.open({
			  type: 0,
			  closeBtn: 0, //不显示关闭按钮
			  anim: 2,
			  shadeClose: true, //开启遮罩关闭
			  content: '网络通讯中断，正在尝试重新连接。'
			});
	}
	socket = new SockJS(baseUrl+'/room');
	stompClient = Stomp.over(socket);
	stompClient.connect({}, function (frame) {
		console.log('websocket连接成功');
		connectState('connected');
		setTimeout(function(){
			$._Accident.init(); // 重新监听websocke信息
		},2000);
		
		if($socketLayerIndex!=null){
			layer.close($socketLayerIndex); // 关闭websocke连接信息对话框
			
			$socketLayerIndex=null;
			
			layer.open({
				  type: 0,
				  closeBtn: 0, //不显示关闭按钮
				  anim: 2,
				  shadeClose: true, //开启遮罩关闭
				  time: 10000, //20s后自动关闭
				  content: '网络通讯已恢复。'
				});
		}
//		if(currentRoomName != null && currentRoomName != ""){
//			haveRoomEvent(currentRoomName);
//			if(isCollect == true){
//				subCollect = stompClient.subscribe('/topic/collect/' + currentRoomName, function (mes) {
//					 handleData(mes);
//				});
//			}
//		}else{
//			if(isCollect == true){
//				subCollect = stompClient.subscribe('/topic/collect/' + currentUserId, function (mes) {
//					 handleData(mes);
//				});
//			}
//		}
	},errorCallback);  	
}

var isBH = false;



 /**
 * 会商绘标
 **/
function consultationMark(type){
	
	if(!$._Accident.isInroom()){
		$._Accident.joinRoom();
		return;
	}
	isOpenConsult = true;
	var params={};
	$.ajax({
		type : "post",
		url : baseUrl+"/webApi/roomExist.jhtml",
		data: JSON.stringify(params),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode=="200"){
				if(type=='0'){
					showOrHideHSHB();
				}else if(type=='1'){
					activateXx('Polyline');
				}
			}else if(data.httpCode=="409"){
				confirmSynchronization();
			}
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	})
}

//显示3D
function consultationMark3D(type){
	if($("#vrMainDiv").is(":hidden")){
		$("#htmlMainDiv").css("width","50%");
		document.PowerSpotLight.pauseRender(0);
	}else{
		$("#htmlMainDiv").css("width","100%");
		document.PowerSpotLight.pauseRender(1);
	}
	$("#vrMainDiv").toggle();
}

/*
 * 展示房间确认
 * */

function confirmSynchronization(){
	layer.confirm('是否需要记录本事故的信息？', {
        btn: ['记录','不记录'] //按钮
   },function(){
	   //记录
	   layer.closeAll('dialog');
	   $._Accident.joinRoom();
   }, function(){
   		layer.msg('已取消', {icon: 1, time: 1000});
   });
}


/**
 * 获取session房间id
 * @param value
 * @returns
 */
function getRoomSession(){
	
}

// 定位到事故点
function accidentPointLocation(){
	centerAndZoom(zhmnAccidentPoint,12);
}

/*
 * 打开会商绘标的界面
 * */

function showOrHideHSHB(){
	 if($(".subMeus").is(":hidden")){
         $(".subMeus").show();						
         $(".tool").css("top","70px");
         $("#fxbss").css("top","80px");
         $("#weatherMsgModelDiv").css("top","140px");
         $('#policeBJ').css('top','56px');
         $("#countDownDiv").css("top","103px");
     	$("#fireDosageDiv").css("top","130px");
     }else{
         $("#fxbss").css("top","10px");
         $("#weatherMsgModelDiv").css("top","70px");
         $(".subMeus").hide();
         $(".tool").css("top","0px");
         $('#policeBJ').css('top','0px');
         $("#countDownDiv").css("top","50px");
     	$("#fireDosageDiv").css("top","75px");
	 }
	 setMeunPosition();
}

/*退出用户*/
function showOrHideUser(){
	 if($("#dropDivBox").is(":hidden")){
        $("#dropDivBox").show();						
        
    }else{
        $("#dropDivBox").hide();
    }
}

/*聊天信息事件*/
function showTabChatContNavDiv(num){
	$("#chatContNavDiv li").removeClass('activeLink');
	if(num==0){
		$("#layui-layer-pageDiv0").show();
		$("#layui-layer-pageDiv1").hide();
		$("#layui-layer-pageDiv2").hide();
		$("#layui-layer-pageDiv3").hide();
		$("#chatContNavDivLi"+ num).addClass('activeLink');
	}else if(num==1){
		$("#layui-layer-pageDiv1").show();
		$("#layui-layer-pageDiv0").hide();
		$("#layui-layer-pageDiv2").hide();
		$("#layui-layer-pageDiv3").hide();
		$("#chatContNavDivLi"+ num).addClass('activeLink');
	}else if(num==2){
		$("#layui-layer-pageDiv0").hide();
		$("#layui-layer-pageDiv1").hide();
		$("#layui-layer-pageDiv3").hide();
		$("#layui-layer-pageDiv2").show();
		$("#chatContNavDivLi"+ num).addClass('activeLink');
	}else if(num==3){
		$("#layui-layer-pageDiv0").hide();
		$("#layui-layer-pageDiv1").hide();
		$("#layui-layer-pageDiv2").hide();
		$("#layui-layer-pageDiv3").show();
		$("#chatContNavDivLi"+ num).addClass('activeLink');
	}
}

function selectCYY(num){
	$("#layui-layer-pageDiv0").show();
	$("#layui-layer-pageDiv1").hide();
	$("#layui-layer-pageDiv2").hide();
	var content = $("#languageCont"+num).html();
	$("#chatCon").html(content);
}

/*
*所有人退出关闭房间；
*关闭房间后再次输入创建房间；
*登录后判断如果没退出房间session存在则直接进入房间；
*进入房间后在地图绘制历史信息；
 */
/*
1.事故处理按钮改为事故接报；功能为创建事故(原创建房间);
2.增加加入房间按钮，输入邀请码加入房间，如果邀请码不存在(注意过滤关闭的事故)提示未找到房间，加入房间后 按钮变为退出房间，事故接报按钮改为事故信息，可以查看当前事故信息；
3.创建事故表单房间号改为邀请码，在创建事故时自动生成（随机4位）;
4.增加事故位置字段，可以选择重点单位或直接在地图上标注；
5.新增事故完成后，当前登录人自动进入房间同2逻辑；
6.右上角标题增加事故名称字段，创建事故时显示；
7.点击【结束救援】关闭事故，房间邀请码失效，全部终端推送事故报告，事故接报按钮还原；
8.输入邀请码加入房间用户【退出房间】，事故接报按钮还原可以新增事故；事故创建人【退出房间】，保留事故信息，直到点击【结束救援】
*/
/**
 * 事故类
 * @class
 */
var Accident = function(){
	var t = this;
	//t.init();
	return t;
}
Accident.prototype = {
	/**
	 * 当前事故信息
	 */
	accident:{},
	/**
	 * 是否在房间
	 * @returns {boolean} 在房间返回true
	 */
	isInroom:function(){
		var state = $("#joinRoomBtn").attr("state");
		//查看
		if(state && state=="close"){
			return true;
		}else{
			return false;
		}
	},
	/**
	 * 初始化事故 注册按钮事件
	 */
	init:function(){
		var t = this;
		//消防车和事故点 隐藏
		$('#ysbhli').hide();
		$('#pointBtn').hide();
		$('#ysbhBtn').hide();
		$('#zntjBtn').hide();
		$('#iconBoxZNTJ').hide();
	//	$('#sprayTimeBeginBtn').hide();//显示消防车
		$('#zhmibtn').hide();
		$('#countDownDiv').hide();

		//创建/查看事故接报
		$("#newAccidentBtn").on('click',function(){
			var state = $(this).attr("state");
			//查看
			if(state && state=="check"){
				t.checkAccident();
			}
			//创建
			else{
				t.createAccident();
			}
		});
		//加入/退出房间
		$("#joinRoomBtn").on('click',function(){
			var state = $(this).attr("state");
			//退出房间
			if(state && state=="close"){
				//先判断是否是最后一个人，当前事故房间只有您一人，是否退出并结束房间；点[是]结束报告；点[否]退出房间，不结束事故。上面有个差，是取消刚才操作；
				t.getRoomUser(function(data){
					if(data && data.length==1){
						//前事故房间只有您一人，是否退出并结束房间
						layer.confirm('目前事故救援只有您一人，如选择继续退出，将结束事故救援。确认结束并退出', {
							btn: ['是'] //按钮
						   },function(){
								layer.closeAll('dialog');   
								//退出   
								t.closeRoom();
								//弹出事故报告记录
								t.closeAccident();
								closePanel('fireDosageDiv'); 
						   }
//						   , function(){
//								//退出   
//								//t.closeRoom();
//							   layer.msg('已取消', {icon: 1, time: 1000});
//							   layer.closeAll('dialog');
//							}
//						   ,function(){
//								layer.msg('已取消', {icon: 1, time: 1000});
//								layer.closeAll('dialog');
//						   }
						   );	
					} else{  
						//退出   
						t.closeRoom();	
						closePanel('fireDosageDiv'); 
						//弹出事故报告记录
						t.closeAccident();
					}
				});
				//t.closeRoom();
			}
			//加入房间
			else{
				t.joinRoom();
			}
		})

		//结束事件按钮
		$("#jsjyButton").on("click",function(){
			if($("#jsjyTitle").text()=="结束救援"){
				layer.confirm('是否确认结束救援？', {
					btn: ['确定','取消'] //按钮
			   	},function(){
					//结束救援房间号失效
			   		t.closeAccident();
					//记录
					layer.closeAll('dialog');
			   	}, function(){
			 	   layer.msg('已取消', {icon: 1, time: 1000});
				});	
			}else{
				t.closeAccident();
			}
		});

		$("#reportAccident").on("load",function(){ 
			var src = $('#reportAccident').attr("src");
			if(src && src!="" && src!="nudefined"){
				$("#reportAccident").show(); 
			}
		});
		//查看登录人有没有房间 有就重新登录
		$.ajax({
			type : "post",
			url : baseUrl+"/webApi/checkRoom.jhtml",
			data: JSON.stringify({}),
			dataType:"json",
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {
				//返回就是有房间
				if(data.data){
					t.joinRoomEnd(data.data);
				}
			}
		});

		//$("#roomContentInsert").css("marginTop","-260px");
	},
	checkEndRescue:function(roomId){
		$.ajax({
			type : "post",
			url : baseUrl+"/webApi/checkEndRescue.jhtml",
			data: JSON.stringify(roomId),
			dataType:"json",
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {
				if(data.data && data.data > 0){
					$("#jsjyTitle").text("查看报告");
					$("#jsjyIcon").attr("class","seeReport");
				}
			}
		});
	},
	
	/**
	 * 创建事故
	 */
	createAccident:function(){
		var t = this;
		//绘制表单
		{var content = '<div class="roomClose" onclick="closeCreateAccidentPanel(\'roomContentInsert\')"><a id="" style="z-index:1000;" class="ol-popup-closer" ></a></div>'
			+'<div class="pre">'
			// 创建新房间
			+'<div class="ctmatter">'	
			+'<div class="foundRoom"><img src="'+baseUrl+'/images/foundRoom.png" alt="" /></div>'		    
			+'<div>'
			+'<div class="decorate" id="roomContentCreateDiv" >'
			+'<div class="ca cf">'
			+'<span class="fl ctName"><i></i>事故名称：</span>'
			+'<span class="fl ctInput"><input id="accNameD" type="text" value="" maxlength="30"></span>'
			+'</div>'
			+'<div class="ca cf">'
			+'<span class="fl ctName"><i></i>事故等级：</span>'
			+'<span class="fl selectBgColor">'
			+'<select id="accLevel" class="selectLevel">'
			+'<option class="">请选择</option>'
			+'<option class="">I</option>'
			+'<option class="">II</option>'
			+'<option class="">III</option>'
			+'</select>'
			+'</span>'
			+'</div>'
			+'<div class="ca cf">'
			+'<span class="fl ctName"><i></i>事故描述：</span>'
			+'<span class="fl ctInput h40">'
			+'<textarea  name="desc" class="textarea" id="accNote" type="text" value="" rows=""></textarea>'
			+'</span>'
			+'</div>'			
			+'<div class="ca cf">'
			+'<span class="fl ctName"><i></i>事故类型：</span>'
			+'<span class="fl selectBgColor">'
			+'<select id="accType" class="selectLevel">'
			+'<option class="" value="废油池流淌火">废油池流淌火</option>'
			+'<option class="" value="油罐火灾">油罐火灾</option>'
			+'<option class="" value="气体泄漏">气体泄漏</option>'
			+'<option class="" value="爆炸">爆炸</option>'
			+'</select>'
			+'</span>'
			+'</div>'			
			+'<div class="ca cf">'
			+'	<span class="fl ctName"><i></i>事故位置：</span>'
			+'	<span class="fl ctInput"><input id="accLocation" readonly="true" type="text" value="">'
			+'   <i id="setLocation" style="width: 17px;height: 24px;position: absolute;right: 2px;top: 1px;background: url(../images/sgd_sgjb.png) no-repeat;background-position: 0px 0px;"></i>'
			+'  </span>'
			+'</div>'
			+'<div class="ca cf">'
			+'  <span class="fl ctName"><i></i>事故指挥人：</span>'
			+'  <span class="fl ctInput"><input id="accidentConductor" type="text" value="" maxlength="30"></span>'
			+'  <span class="fl"></span>'
			+'</div>'
			+'<div class="ca cf" style="display:none">'
			+'  <span class="fl ctName"><i></i>邀请码：</span>'
			+'  <span class="fl ctInput"><input id="roomcode" type="text" value="'+t.getRoomCode()+'"></span>'
			+'  <span class="fl"></span>'
			+'</div>'
			+'</div>'
			+'<div class="ctbtn">'
			+'<input type="button" value="确定" id="btnsaveroom" class="ctSure fl"><input type="button" value="取消" onClick="closeRoomPanel()" class="ctCancel fl">'
			+'</div>'
			+'</div>'
			+'</div>'
			+'</div>'
		$("#roomContentInsert").html(content);}
		$("#roomContentInsert").Tdrag({
			scope:"#map"
			,handle:".foundRoom"
			,iscenter:false
			,cbEnd:function(){
				var x = $('#accLocation').parent().offset();
				$('#selectZDDW').css('top',x.top+$('#accLocation').parent().height());
				$('#selectZDDW').css('left',x.left);
				$('#selectZDDW').width($('#accLocation').parent().width());
			}//移动结束时候的回调函数
		});
		//选择重点单位的HTML
		{var selectHTML = '<div id="selectZDDW"  class="frame createRoom selectZDDWdiv">';
			selectHTML +='	<div style="text-align: center;">';
			selectHTML +='		<div class="head" style="display: ruby;">';
			selectHTML +='			<div class="search" style="margin-top:10px;"><input placeholder="请输入名称" class="into" type="text"><i class="searIcon" id=""></i></div>';
			selectHTML +='		</div>';
			selectHTML +='		<div class="bodyNew ca cf" style="margin: 0 auto;">';
			selectHTML +='			<ul style="margin-top: 10px;">';
	 		selectHTML +='			</ul>';
	  		selectHTML +='		</div>';
			selectHTML +='	</div>';
			selectHTML +='</div>';
			if($('#selectZDDW').length<1){
				$("#roomContentInsert").after(selectHTML);
			}
		}
		$('#selectZDDW').hide();

		$("#roomContentInsert").show();
		//创建房间保存
		$('#btnsaveroom').on('click',function(){
			var accident = t.accident;	
			var accidentName=$("#accNameD").val();//名称
			var accidentRemark=$("#accNote").val();//描述
			var accidentLevel=$("#accLevel").val();//等级
			var accidentType=$("#accType").val();//类型	
			var accLocation=$("#accLocation").val();//位置
			var accidentConductor=$("#accidentConductor").val();//指挥人
			if(!roomFlag){
			   	if(accidentName.length<1){
			   		layer.msg("请填写事故名称!", {time: 1000});
			   		return false;
			   	}
			   	if(accidentLevel.length<1){
			   		layer.msg("请填写事故等级!", {time: 1000});
			   		return false;
			   	}
			   	if(accidentRemark.length<1){
			   		layer.msg("请填写事故描述", {time: 1000});
			   		return false;
			   	}
			   	if(accidentType.length<1){
			   		layer.msg("请填写事故类型!", {time: 1000});
			   		return false;
			   	}
			   	if(accLocation.length<1){
			   		layer.msg("请选择事故位置!", {time: 1000});
			   		return false;
			   	}
			   	if(accidentConductor.length<1){
			   		layer.msg("请填写事故指挥人!", {time: 1000});
			   		return false;
			   	}
				var params={
					"accidentName":accidentName,//事故名称
					"accidentRemark":accidentRemark,//事故等级
					"accidentLevel":accidentLevel,//事故描述
					"accidentType":accidentType,//事故类型
					"accidentLocation":JSON.stringify(accident.accidentLocation),//位置
					"accidentConductor":accidentConductor
				};
			    $.ajax({
					type : "post",
					url : baseUrl+"/webApi/createRoom.jhtml",
					data: JSON.stringify(params),
					dataType:"json",
					headers: {"Content-Type": "application/json;charset=utf-8"},
					success : function(data) {
						//本地创建
						data.data.isCreate = 'true';
						t.joinRoomEnd(data.data);
						layer.msg('保存成功', {icon: 1, time: 1000});
					},error: function(request) {
						layer.msg(networkErrorMsg, {time: 2000});
			        }
				})
			}
		})
		//输入框获得焦点定位 失去焦点隐藏
		$('#accLocation').on('focus',function(){
			$('#selectZDDW .search input').trigger('input');
			var x = $('#accLocation').parent().offset();
			$('#selectZDDW').css('top',x.top+$('#accLocation').parent().height());
			$('#selectZDDW').css('left',x.left);
			$('#selectZDDW').width($('#accLocation').parent().width());
			showBack.apply($('#selectZDDW'));//不触发这个，返回关闭
		}).on('blur',function(){
			var blur = $("#accLocation").attr("blur");
			if(!blur || blur=="out"){
				$('#selectZDDW').hide();
			}
		});
		//输入时搜索 并绘制
		var si = "";
		$('#selectZDDW .search input').on('input',function(){
			var name = $('#selectZDDW .search input').val();
			//绘制方法
			var newselecthtml = function(_data){
				var newdata= _data.filter(function(m){
					return m.name.indexOf(name)>=0
				});
				//开始绘制
				$('#selectZDDW ul').html('');
				var tr = [];
				$.each(newdata,function(i,v){
					v.type='zddw';
					tr.push("<li><input ZDDWdata='"+ JSON.stringify(v)+'\' value="'+v.name+'" type="button"></li>');
					if(tr.length==3){
						$('#selectZDDW ul').append('<li>'+tr.join('')+'</li>');
						tr = [];
					}
				})
				$('#selectZDDW ul').append('<li>'+tr.join('')+'</li>');
				//选中后赋值
				$('#selectZDDW ul input').on("click",function(){
					var ZDDWdata = $(this).attr('ZDDWdata');
					ZDDWdata = eval('('+ZDDWdata+')');
					//定位跳转
					accidentCompany.lonlat3857 = leftRecordLocation(null,null,ZDDWdata.type,null,ZDDWdata.id,null);
					accidentCompany.lonlat4326 = ol.proj.transform(accidentCompany.lonlat3857,'EPSG:3857','EPSG:4326');
					$('#accLocation').val($(this).val());
					
					//清除缓存数据再次展开再次获取
					$('#selectZDDW').data('ZDDWdata',false);
					$("#accLocation").attr("blur","out");
					$('#selectZDDW').hide();
					//不隐藏就循环
//					var c = 0;
//					if(si){clearInterval(si)};
//					si = setInterval(function(){
//						if(!$('#selectZDDW').is(":hidden")){
////							console.log("隐藏执行")
//							$('#selectZDDW').hide();
//							$('#accLocation').blur();
//							$("#accidentConductor").focus();
//							c++;
//						}else{
//							clearInterval(si);
//						}
//						if(c==2){
//							clearInterval(si);
//						}
//					},300)
					//先删除已经选的点
					if(t.accident.accidentLocation && t.accident.accidentLocation.id){
						deleteAccidentFeatureByid(t.accident.accidentLocation.id);
					}
					t.accident.accidentLocation = JSON.stringify(ZDDWdata);
				})
			}
			//得到数据
			if(!$('#selectZDDW').data('ZDDWdata')){
				var params={"name":'',"nopage":""};		    
				$.ajax({
					type : "post",
					url : baseUrl+"/webApi/qryList.jhtml",
					data: JSON.stringify(params),
					dataType:"json",
					headers: {"Content-Type": "application/json;charset=utf-8"},
					success : function(data) {
						$('#selectZDDW').data('ZDDWdata',data.data);	
						newselecthtml(data.data);
					},error: function(request) {
						layer.msg(networkErrorMsg, {time: 2000});
					}
				});
			}else{
				//在本地搜索
				newselecthtml($('#selectZDDW').data('ZDDWdata'));
			}
		});
		//直接选择地点
		$("#setLocation").on('click',function(){
			var params ={};
			params.oid = "555333";
			params.name = $('#accNameD').val();
			params.clusterType='accident';
			params.imagePath="/mapIcon/mapFixedIcon/sgd_sgjb.png";
			params.isCluster=true;
			params.clusterImagePath="/mapIcon/mapFixedIcon/sgd_sgjb.png";
			params.isSyn=false;
			params.isSave=false;
			params.recordType='accLocationCompany';
			params.action="事故位置";
			addMakerEngine(params);
			t.framemin();
		})
		//影藏界限控制
		$("#selectZDDW").on('mouseenter',function(){
			$("#accLocation").attr("blur","in");
		}).on('mouseleave',function(){
			$("#accLocation").attr("blur","out");
//			$('#accLocation').focus();
		});
	},
	/**
	 * 关闭事故,执行关闭事故的唯一入口是：点击结束救援按钮；
	 * @param roomid 为空时推送结束事故事件 ，不为空时执行
	 */
	closeAccident:function(roomid){
		var t = this;
		if($("#jsjyTitle").text()=="结束救援"){
			$("#jsjyTitle").text("查看报告");
			$("#jsjyIcon").attr("class","seeReport");
			
			$.ajax({
				type : "post",
				url : baseUrl+"/webApi/endRescue.jhtml",
				data: JSON.stringify(t.accident.id),
				dataType:"json",
				headers: {"Content-Type": "application/json;charset=utf-8"},
				success : function(data) {
				}
			});
		}
		//关闭事故弹出事故报告；房间号失效；第一次进入没有房间ID
		$("#reportAccident").attr("src",baseUrl+"/webApi/report.jhtml?roomid="+t.accident.id);
		
//		if(!roomid){
//			$("#reportAccident").attr("src",baseUrl+"/webApi/report.jhtml?roomid="+t.accident.id);
//		}else{
//			//报告完成后 所有终端弹出报告结果
//			$("#jsjyButton").hide();
//			//状态改变
//			t.accident.state=2;
//			//事故关闭，所以不判断是否是创建人 
//			t.accident.isCreate="false";
//			//隐藏
//			$('yjzhqButton').hide();
//			$('jsjyButton').hide();
//			//计时清除
//			$('#countDownDiv').hide();
//			//top清除计时器
//			if(t.timeInterval){clearInterval(t.timeInterval)}
//
//			//创建人 可以先退出房间 再结束事故 ，如果退出房间了并结束事故 则重置
//			if($("#joinRoomBtn").attr("state")=="join"){
//				//话筒 移交 和结束
//				$("#huatongButton").hide();
//				$("#yjzhqButton").hide();
//				$("#jsjyButton").hide();
//				$('#ysbhli').hide();
//				$('#pointBtn').hide();
//				$('#ysbhBtn').hide();
//				//$('#sprayTimeBeginBtn').hide();
//				$('#zhmibtn').hide();
//				//$('#countDownDiv').hide();
//				
//				$("#newAccidentBtn").text("事故接报").attr("state","create");//修改事故按钮
//				$("#accidentName").text('');//事故名称
//				$("#identConductor").text('');//指挥人
//				$("#room").text('');//邀请码 房间号
//				//移除全部历史绘制的绘标
//				t.removeFeature();
//
//				//单位详情 返回
//				pointReturn();
//			}
//			
//			//弹出报告
//			$("#reportAccident").attr("src",baseUrl+"/webApi/report.jhtml?isview=1&roomid="+t.accident.id);
//		}
	},
	/**
	 * 报表保存按钮 完成后回发 到closeAccident(roomid)方法
	 */
	saveReport:function(_params){
		var t = this;
		var urls = baseUrl + "/webApi/report/saveAccidentInfo.jhtml";
		var params = _params;
		params.id = t.accident.id;
		
		
		executeAjax(urls,params,function(data){
			if(data.httpCode==200){
				layer.msg("事故报告保存成功", {time: 3000}); 	
				//推送后又回到改方法
				//t.trigger('closeAccident',t.accident.id);
    		}else {
    			layer.msg(data.msg, {time: 1000});
    		}
    	});	
	},
	//关闭报表
	closeReport:function(){	$("#reportAccident").hide();},
	/**
	 * 查看事故信息
	 */
	checkAccident:function(){
		var t = this;
		var accident = t.accident;
		//绘制表单
		{var content = '<div class="roomClose" onclick="closePanel(\'roomContentInsert\')"><a id="" style="z-index:1000;" class="ol-popup-closer"></a></div>'
		+'<div class="pre">'
		// 创建新房间
		+'<div class="ctmatter">'	
		+'<div class="foundRoom"><img src="'+baseUrl+'/images/foundRoom.png" alt="" /></div>'		    
		+'<div>'
		+'<div class="decorate" id="roomContentCreateDiv" >'
		+'<div class="ca cf">'
		+'	<span class="fl ctName"><i></i>事故名称：</span>'
		+'	<span class="fl ctInputMsg">'+accident.accidentName+'</span>'
		+'</div>'
		+'<div class="ca cf">'
		+'	<span class="fl ctName"><i></i>事故等级：</span>'
		+'	<span class="fl ctInputMsg">'+accident.accidentLevel+'</span>'
		+'</div>'
		+'<div class="ca cf">'
		+'	<span class="fl ctName"><i></i>事故描述：</span>'
		+'	<span class="fl ctInputMsg">'+accident.accidentRemark+'</span>'
		+'</div>'			
		+'<div class="ca cf">'
		+'	<span class="fl ctName"><i></i>事故类型：</span>'
		+'	<span class="fl ctInputMsg">'+accident.accidentType+'</span>'
		+'</div>'			
		+'<div class="ca cf">'
		+'	<span class="fl ctName"><i></i>事故位置：</span>'
		+'	<span class="fl ctInputMsg" >'+(accident.accidentLocation?(accident.accidentLocation.name||"事故位置"):"事故位置")+'</span>'
		+'</div>'
		+'<div class="ca cf">'
		+'  <span class="fl ctName"><i></i>事故指挥人：</span>'
		+'	<span class="fl ctInputMsg" >'+accident.accidentConductor+'</span>'
		+'  <span class="fl"></span>'
		+'</div>'
		+'<div class="ca cf" style="display:none">'
		+'  <span class="fl ctName"><i></i>邀请码：</span>'
		+'	<span class="fl ctInputMsg" >'+accident.room+'</span>'
		+'</div>'
		+'</div>'
		+'</div>'
		+'</div>'
		+'</div>'
		$("#roomContentInsert").html(content);}
		$("#roomContentInsert").show();
		$("#roomContentInsert").Tdrag({
			scope:"#map"
			,handle:".foundRoom"
			,iscenter:false
		});
		if(accident.accidentLocation && typeof(accident.accidentLocation)=="string"){
			accident.accidentLocation = eval('('+accident.accidentLocation+')');
		}
		//绘制并跳转 选择的点需要绘制
		if(accident.accidentLocation.type && accident.accidentLocation.type=="Feature")
		{
			positionSynMark(accident.accidentLocation);
			gotoPointLocation({type:"accidentMarker",id:accident.accidentLocation.id});
		}else{
			gotoPointLocation(accident.accidentLocation);
		}
	},
	/**
	 * 加入房间
	 */
	joinRoom:function(){
		var t = this;
		//如果有事故 则说明是创建人，在结束救援之前不能加入其它房间
		{
		var content = '<div class="roomClose"><a id="" style="z-index:1000;" class="ol-popup-closer" onclick="closePanel(\'roomContentJoin\')"></a></div>'
		+'<div class="pre">'
		+'<div class="foundRoom"><img src="'+baseUrl+'/images/addRoom.png" alt="" /></div>'
		// 邀请码
		+'<div class="invite">'
		+'<div class="ca cf inviteNumber">'
		+'<span class="fl ctName">邀请码：</span>'
		+'<span class="fl inviteInto"><input id="accRoom" type="text" placeholder="请输入邀请码"></span>'
		+'</div>'
		+'</div>'	
		+'<div class="ctbtn">'
		+'<input type="button" value="确定" id="btnjoinroom" class="ctSure fl"><input type="button" value="取消" onClick="closeRoomPanel()" class="ctCancel fl">'
		+'</div>'
		$("#roomContentJoin").html(content);}
		$("#roomContentJoin").show();
		$('#accRoom').focus();
		$("#roomContentJoin").Tdrag({
			scope:"#map"
			,handle:".foundRoom"
			,iscenter:false
		});
		
		//加入房间
		$('#btnjoinroom').on('click',function(){
			//判断房间是否存在不存在提示
			var accRoom=$("#accRoom").val();
			if(accRoom==""){
				return;
			}	
			$.ajax({
				  type : "post",
				  url : baseUrl+"/webApi/getRoomInfo.jhtml",
				  data: JSON.stringify(accRoom),
				  dataType:"json",
				  headers: {"Content-Type": "application/json;charset=utf-8"},
				  success : function(data) {
					  //如果房间存在
					  if(data.httpCode == '200'){
						//非本地创建
						//data.data.isCreate = false;
						var i = 0;
						while(data.data.accidentLocation && typeof(data.data.accidentLocation)=="string" && i<5){
							data.data.accidentLocation = eval('('+data.data.accidentLocation+')');
							var accidentLocation=[];
							accidentLocation[0]=data.data.accidentLocation.lon;
							accidentLocation[1]=data.data.accidentLocation.lat;
							accidentCompany.lonlat4326=accidentLocation;
							i++;
						}
						t.joinRoomEnd(data.data);
					  }else{
						layer.msg("未找到房间", {time: 1000});
					  }
				  },error: function(request) {
					  layer.msg(networkErrorMsg, {time: 2000});
				  }
			  })
		})
		//回车事件
		$('#accRoom').on("keydown",function(event){
			if(event.keyCode ==13){
				$("#btnjoinroom").trigger("click");
			}
		})
	},
	/**
	 * 加入房间或创建事故之后触发
	 * @param _roomdata room表数据 记录事件
	 */
	joinRoomEnd:function(_roomdata){
		var t = this;
		if(_roomdata){
	
			//创建事故完成
			t.accident = $.extend(t.accident,_roomdata);//保存数据
			var accident = t.accident;
			currentRoomId = accident.id;
			currentRoomName = accident.room;
			disasterType = accident.accidentType;
			haveRoomEvent(accident.room);//注册同步方法
			showThreeButton();//显示快捷绘制按钮
			//加入房间保存session
			$.ajax({
				type : "post",
				url : baseUrl+"/webApi/joinRoom.jhtml",
				data: JSON.stringify({"id":accident.id,"room":accident.room}),
				dataType:"json",
				headers: {"Content-Type": "application/json;charset=utf-8"},
				success : function(data) {
					//临时字段， 存用户名称
					currentUserName = data.data.accidentName ;
					//加入房间日志
					(function(){
						var params = {};
						params.id =getTimestamp();
						params.roomId = accident.id;
						params.action = "加入房间";
						params.content = JSON.stringify({"name":currentUserName,"roomname":accident.accidentName});
						params.type = 5;
						params.isDelete = 1;//删除0，添加1
						params.createBy = currentUserId; //修改等路人 todo
						params.director = currentUserName; //修改等路人 todo
						saveRecord(params);
					})();
					// 判断是否结束过救援
					t.checkEndRescue(accident.id);
					//显示用户并且通知其他终端
					t.getRoomUser();
					t.trigger("updateRoomUser",{"userId":currentUserId,"userName":currentUserName});

					//显示时间 create_time accident.createTime
					if( accident.createTime){
						//var myDate = Date.parse(accident.createTime);
						var datenow = new Date();
						var _time = parseInt((datenow.getTime()-accident.createTime)/1000);
						$('#countDownDiv').show();
						if(t.timeInterval){clearInterval(t.timeInterval)};
						t.timeInterval = setInterval(function(){
							$('#countDownDiv .yellow').text(getDateTimeT(_time));
							_time++;
						},1000);
					}
					t.getAccidentByRoomId(currentRoomId);
				}
			});

			//打开会商绘标
			if(isOpenConsult){
				showOrHideHSHB();
				isOpenConsult = false;
			}
			//回显绘标
			//if(accident.isCreate=="false"){
				t.initHistory();
			//}
			/** 按钮控制 */
			$("#accidentName").text(accident.accidentName);//事故名称
			$("#identConductor").text(accident.accidentConductor);//指挥人
			$("#room").text(accident.room);//邀请码 房间号
			$('.fjbh').show();
			$("#changeLeadersDiv .Operator i").text(accident.accidentConductor);//指挥人移交
			$("#joinRoomBtn").text("退出事故").attr("state","close");//修改房间按钮
			$("#newAccidentBtn").text("事故信息查看").attr("state","check");//修改事故按钮
			$("#roomContentInsert").hide();//关闭创建窗口
			$("#roomContentJoin").hide();//关闭创建窗口
			$("#jsjyTitle").text("结束救援");// 显示结束救援
			$("#jsjyIcon").attr("class","endrescue"); // 显示结束救援图标
			//如果是重点单位， 在 changeSelectUnit 上面增加一个按钮显示当前重点单位
			var i = 0;
			while(accident.accidentLocation && typeof(accident.accidentLocation)=="string" && i<5){
				accident.accidentLocation = eval('('+accident.accidentLocation+')');
				i++;
			}
			if(accident.accidentLocation.type && accident.accidentLocation.type=="Feature")
			{
				positionSynMark(accident.accidentLocation);
				gotoPointLocation({type:"accidentMarker",id:accident.accidentLocation.id});
			}else{
				
				gotoPointLocation(accident.accidentLocation);
				//重点单位详情按钮 布局 位移 详情
				if($("#AccidentAddress").length > 0) {
					$("#AccidentAddress").val('事故点：'+accident.accidentLocation.name);
				}else{
					var sginput = $('<input id="AccidentAddress" style="width: 100%;margin-bottom: 10px;" class="returnPotin" type="button" value="事故点：'+accident.accidentLocation.name+'"/>');
					$('#changeSelectUnit').parent().before(sginput);
					$('#AccidentAddress').on("click",function(){
						setMeunPosition('maxMenu');
						unitDetail(accident.accidentLocation.id.toString());
						//显示厂区消防力量
						//alert( accident.accidentLocation.waterSupply);
						if(accident.accidentLocation.waterSupply){
							$("#fireDosageDiv").show();
							$("#zddwCQZL").html(accident.accidentLocation.waterSupply);
							$("#zddwCQZLKYLL").html(accident.accidentLocation.waterSupply);
						}
						if(accident.accidentLocation.lon){
							//显示事故点范围内的数据
	//						accident.accidentLocation.lon = accident.accidentLocation.lon-0.01185-0.0014;
	//						accident.accidentLocation.lat = accident.accidentLocation.lat-0.00328-0.004222-0.000507;
							accident.accidentLocation.lon = accident.accidentLocation.lon;
							accident.accidentLocation.lat = accident.accidentLocation.lat;
							var point = ol.proj.transform([accident.accidentLocation.lon,accident.accidentLocation.lat], 'EPSG:4326', 'EPSG:3857');
							drawAccidetCommptyRange(point);
							gotoPointView(point[0],point[1],14);
							$("#isShowCommptyRangeInputValue").attr("lon",point[0]);
							$("#isShowCommptyRangeInputValue").attr("lat",point[1]);
							$("#showHideBar").show();
	//						$("#toolBoxBTN").show();
						}
						 $("#zhmnListId").hide();
						//setMeunPosition('maxMenu');
					});
				}
				//leftMenuContent warpSroll
				$('#AccidentAddress').parent().next().animate({top: '+=40px'},function(){
				});
				setTimeout(function(){
					$('#AccidentAddress').trigger('click');
				},2000);

				//消防车详情 扩展一个ul li
				//fireBrigadeId
				var params=accident.accidentLocation.fireBrigadeId;
				executeAjax( baseUrl+"/webApi/qryFireBrigadeById.jhtml",params,function(_data){
					if(_data.data){
						$("#fireBrigadeNameA").remove();
						//重点单位详情按钮 布局 位移 详情
						var sginput = $('<input id="fireBrigadeNameA" style="width: 100%;margin-bottom: 10px;" class="returnPotin" type="button" value="当前事故消防队：'+_data.data.name+'"/>');
						$('#fireBrigadeIn').parent().before(sginput);
						$('#fireBrigadeNameA').on("click",function(){
							fireBrigadeNameAccident(_data.data);
							 $("#zhmnListId").hide();
						})
						//leftMenuContent warpSroll
						$('#fireBrigadeNameA').parent().next().animate({top: '+=35px'},function(){
						});
						setTimeout(function(){
							$('#fireBrigadeNameA').trigger('click');
						},2000);
					}
				})
//				var params={"fireBrigadeId":accident.accidentLocation.fireBrigadeId,"pageNum":0,"pageSize":10};
//				executeAjax( baseUrl+"/webApi/qryFireBrigade.jhtml",params,function(_data){
//					if(_data.data && _data.data.length>0){
//						var html = getFireBrigadeHtml(_data.data[0]);
//						var fireBrigade = $('#fireBrigade');
//						fireBrigade.before('<ul id="AccidentfireBrigade">'+html+'</ul>');
//
//						//注册展开收起事件
//						$("#AccidentfireBrigade").find("li div.openMore").click(function(e){
//							$(this).toggleClass("takeupMore");
//							$(this).siblings(".listFrom").toggle("fast");
//							stopBubble(e); 
//						});
//
//						$("#AccidentfireBrigade").find(".listHd.ellipsis .address").after("当前事故消防队:");
//					}
//				})
			}
			if(accident.accidentLocation.lon){
				var accidentLocation=[];
				accidentLocation[0]=accident.accidentLocation.lon;
				accidentLocation[1]=accident.accidentLocation.lat;
				accidentCompany.lonlat4326=accidentLocation;
			}
			getZhntjEmergencyMaterial();
			/** 实时聊天框 */
			$('.speech .faultName span').text(accident.accidentName);
			$('.speech .faulthouse span').text(accident.room);
			
			showCollectionAndVideoButton(true);
		}
	},
	/**
	 * 退出房间
	 */
		closeRoom:function(){
			var t = this;
			var accident = t.accident;
			//退出房间日志
			var params = {};
			params.id =getTimestamp();
			params.roomId = currentRoomId;
			params.action = "退出房间";
			params.content = JSON.stringify({"roomname":accident.accidentName});
			params.type = 5;
			params.isDelete = 0;//删除0，添加1
			params.createBy = currentUserId; //修改等路人 todo
			params.director = currentUserName; //修改等路人 todo
			saveRecord(params);
	
			//返回后台 清除session
			(function(){
				var params={};
				 $.ajax({
					type : "post",
					url : baseUrl+"/webApi/logoutRoom.jhtml",
					data: JSON.stringify(params),
					dataType:"json",
					headers: {"Content-Type": "application/json;charset=utf-8"},
					success : function(data) {					
						layer.msg("退出成功", {time: 1000});
						$('.speechList ul').html('');
						$("#accidentName").text('');//事故名称
						$("#identConductor").text('');//指挥人
						$("#room").text('');//邀请码 房间号
						
						t.updateFireEngineStatusByRoomId(currentRoomId);
					},error: function(request) {
						layer.msg(networkErrorMsg, {time: 2000});
			     	}
				});
				//同步 删除右侧用户的展示
				t.trigger("updateRoomUser",currentUserId);
			})();
			
			//取消房间订阅
			if(subRoom != null && subChat != null){
				subRoom.unsubscribe();
				subChat.unsubscribe();
				subRoom = null;
				subChat = null;
			}
			
			currentRoomName = null;
			
	//		(function(){
	//		if(stompClient!=null){
	//			stompClient.disconnect(); 
	//			stompClient = null;
	//		}
	//		if(socket != null){
	//			socket.close();
	//			socket = null;
	//		}})();
			$('#accidentIcon').css("display","none");
			$("#joinRoomBtn").text("加入事故").attr("state","join");//修改房间按钮 
			$("#roomContentInsert").hide();//关闭创建窗口
			if($('#AccidentAddress').length>0){
				$('#AccidentAddress').parent().next().animate({top: '-=40px'},function(){
				});
				$('#AccidentAddress').remove();//重点单位按钮删除
				//$("#leftMenuContent .warpSroll").animate({top: '-=40px'});
			}
			if($('#AccidentfireBrigade').length>0){
				$('#AccidentfireBrigade').remove();//消防详情
			}
			//如果是创建人 以下不重置 如果不是 false 以下重置
//			if(accident.isCreate=="false"){
				//话筒 移交 和结束
				$("#huatongButton").hide();
				$("#yjzhqButton").hide();
				$("#jsjyButton").hide();
				$('#ysbhli').hide();
				$('#pointBtn').hide();
				$('#ysbhBtn').hide();
				$('#iconBoxZNTJ').hide();
				$('#zntjBtn').hide();
			//	$('#sprayTimeBeginBtn').hide();
				$('#zhmibtn').hide();
				$('#countDownDiv').hide();
				//top清除计时器
				if(t.timeInterval){clearInterval(t.timeInterval)}
				
				$("#newAccidentBtn").text("事故接报").attr("state","create");//修改事故按钮
				$("#accidentName").text('');//事故名称
				$("#identConductor").text('');//指挥人
				$("#room").text('');//邀请码 房间号
				//移除全部历史绘制的绘标
				t.removeFeature();
	
				//单位详情 返回
				pointReturn();
				
				closePanel('iconBox');
				$(".subMeus").hide();
//			}
			$("#jsjyButton").hide();
			//关闭 绘标
//			$('#iconBox').hide();
			//top清除计时器
			//计时清除
			$('#countDownDiv').hide();
			//top清除计时器
			if(t.timeInterval){clearInterval(t.timeInterval)}
			if(isCollect){
				closeAuto();
			}
			showCollectionAndVideoButton(false);
			
			// 删除事故接报位置
			deleteAccidentFeatureByid(t.accident.accidentLocation.id);
		},
	/**
	* 得到用户显示的HTML
	*/
   	getUserHTML:function(data){
	   var html = '<li userid = "$$userId$$" class="ca cf">';
	   html +='	<i class="fl"></i>';
	   html +='	<span class="fl">$$name$$</span>';
	  // html +='	<span class="voiceBoxbg fr "></span>';
	   html +='</li>';
	   if(data && data.userId){
		   html =	html.replace('$$name$$',data.userName).replace('$$userId$$',data.userId);
	   }
	   return html;
   	},
	/**
	* 得到当前房间在线的用户
	* @param _fn 回调函数，在退出时先判断是否还有用户
	*/
   	getRoomUser:function(_fn){
	   var t = this;
	   doCallback = function (fn, args) {
		   return fn.apply(this, args);
	   }
	   var url =  baseUrl+"/webApi/getRoomUser.jhtml";
	   var param = {};
	   executeAjax(url,param,function(data){
		   if(data.data){
			   if(_fn){
				   doCallback(_fn,[data.data]);
			   }
			   if($('.speechList ul').length==1){
				   $('.speechList ul').html('');
				   $.each(data.data,function(i,v){
						var _html = t.getUserHTML(v);
					   $('.speechList ul').append(_html);
				   })
			   }
		   }
	   });
	},
	
	getAccidentByRoomId:function(roomId){
	   var url =  baseUrl+"/webApi/getAccidentByRoomId.jhtml";
	   executeAjax(url,roomId,function(data){
		   if(data.data && data.data.length>0){
			   zhmnAccidentPoint[0]= Number(data.data[0].lon.toFixed(6));
			   zhmnAccidentPoint[1]= Number(data.data[0].lat.toFixed(6));
//			   zhmnAccidentPoint[0]= data.data[0].lat;
//			   zhmnAccidentPoint[1]= data.data[0].lon;
			   //在底图上画事故点
			   var content = eval("("+data.data[0].content+")");
			   positionSynMark(content);
			   $('#accidentIcon').css("display","block");
		   }
	   });
	},
	// 更新消防车状态
	updateFireEngineStatusByRoomId:function(roomId){
		var url =  baseUrl+"/webApi/useRecord/updateFireEngineStatusByRoomId.jhtml";
		   executeAjax(url,roomId,function(data){
			   
		   });
	},
	
	/**
	* 更新用户
	* @param userid 用户ID 如果是 对象 则通过id和name 新增
	*/
	updateRoomUser:function(userid){
		var t = this;
		//删除
		if(typeof userid == "string"){
		  $('.speechList').find('[userid="'+userid+'"]').remove();
		}
		//新增
		else if(userid.userId){
		  var _html = t.getUserHTML(userid);
		  if($('.speechList').find('[userid="'+userid.userId+'"]').length<=0){
			  $('.speechList ul').append(_html);
		  };
		}
	},
	//得到四位随机码，注意数据库不要重复
	getRoomCode:function(){

	},
	/**
	 * 加入房间后绘制历史中的绘标
	 */
	initHistory:function(){
		var t = this;
		var accident = t.accident;
		$.ajax({
			type : "post",
			url : baseUrl+"/webApi/getIncidentRecordByAccident.jhtml",
			data: JSON.stringify(accident.id),
			dataType:"json",
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {
				//返回的数据中 有删除和新建的，如果新建后删除了就不调用绘制方法
	
				//需要绘制的类型
				var newjson =[2,4,6,8,9];
				var accidentIsHave=false;
				//得到删除的ID
				var contents = data.data.map(function(m){
					var content = eval("("+m.content+")");
					// shigudian_20190614为事故点固定IP
//					if(m.isDelete===1 && content.id==='shigudian_20190614'){
//						accidentIsHave=true;
//					}
					if(typeof m.isDelete != 'undefined' && m.isDelete==0)
					{
						return content.id;
					}else{return '0';}
				});
				
				for(j = 0,len= data.data.length; j < len; j++){
					var jsoncontent =  data.data[j].content;
					var content = {};
					if(jsoncontent){
						content = eval("("+jsoncontent+")");
					}
					//如果需要绘制 并且不是删除的ID
//					console.log('j='+j);
//					console.log('newjson.indexOf(data.data[j].type)='+newjson.indexOf(data.data[j].type));
//					console.log('contents.indexOf(content.id)='+contents.indexOf(content.id))
//					if(newjson.indexOf(data.data[j].type)>=0 && (contents.indexOf(content.id)<0||(accidentIsHave && data.data[j].type===6))){
					if(newjson.indexOf(data.data[j].type)>=0 && contents.indexOf(content.id)<0){
//						console.log(jsoncontent);
						if(data.data[j].type != 6){
							positionSynMark(content);
						}
						if(data.data[j].type == 8){
							//如果是消防车 记录已经扎上的消防车ID
							if($._IconBox && $._IconBox.makerFireEngineoid){
								$._IconBox.makerFireEngineoid.push(content.id);
							}else{
								$._IconBox.makerFireEngineoid=[];
							}
						}else if(data.data[j].type == 6){
							//事故点位置赋值
//							zhmnAccidentPoint=content.properties.points[0];
						}else if(data.data[j].type == 12){
							
						}
					}
				}
			}
		});
	},
	/**
	 * 移除全部历史绘制的绘标
	 */
	removeFeature:function(){
		var t = this;
		var accident = t.accident;
		$.ajax({
			type : "post",
			url : baseUrl+"/webApi/getIncidentRecordByAccident.jhtml",
			data: JSON.stringify(accident.id),
			dataType:"json",
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {
				//返回的数据中 有删除和新建的，如果新建后删除了就不调用绘制方法
				//需要绘制的类型
				var newjson =[2,4,6,8,9,10];
				//得到删除的ID
				var contents = data.data.map(function(m){
					if(typeof m.isDelete != 'undefined' && m.isDelete==0)
					{
						var content = eval("("+m.content+")");
						return content.id;
					}else{return '0';}
				});
				for(j = 0,len= data.data.length; j < len; j++){
					var jsoncontent =  data.data[j].content;
					var content = eval("("+jsoncontent+")");
					//如果需要绘制 并且不是删除的ID
					if(newjson.indexOf(data.data[j].type)>=0 && contents.indexOf(content.id)<0){
						//console.log("删除绘标:"+JSON.stringify(jsoncontent));
						//clearMarkerClusterLayers();
						noSynMarkerRemove(content);	
						if(data.data[j].type==6){
							deleteAccidentFeatureNoSyn(content.id);
						}
					}
				}
			}
		});
	},
	/**
	 * 选择位置的回调
	 */
	setLocationEnd:function(feature){
		var t= this;
		//先删除已经选的点
		if(t.accident.accidentLocation && t.accident.accidentLocation.id && (!feature.id || feature.id!=t.accident.accidentLocation.id) ){
			//type是accidentMarkerClusterLayer 
			deleteAccidentFeatureByid(t.accident.accidentLocation.id);
		}
		var name = feature.properties?feature.properties.name:"";
		$('#accLocation').val(name);
		t.accident.accidentLocation = feature;
		t.framemax();
	},
	/**
	 * 最小化
	 */
	framemin:function(){
		$("#roomContentInsert").find(".foundRoom").nextAll().hide("fast");
		$("#roomContentInsert").animate({
			top:'50px',
			marginTop:'0px'
		},"fast")
	},
	/**
	 * 最大化
	 */
	framemax:function(){
		$("#roomContentInsert").find(".foundRoom").nextAll().show("fast");
		$("#roomContentInsert").animate({
			top:'50%',
			marginTop:'-260px'
		},"fast")
	},
	/**
	 * 触发事件 触发当前类方法
	 * @param name 事件名称 
	 * @param args 参数
	 */
	trigger:function(name,args){
		send(JSON.stringify({"evetype":name,"val":args})); 
	}
}



/**
 * 右侧图标盒子
 * 消防车逻辑 显示消防车分类，明细显示车牌，点击开始喷淋计算剩余时间，点击结束 暂停，计算剩余喷淋时间，注意可以在水和其他之间切换
 */
var IconBox = function(){
	var t = this;
	t.init();
	return t;
}
IconBox.prototype = {
	//消防车集合
	fireEngine:[],
	/**	
	 * 正在喷淋的车 直到删除消防车 从本集合移除
	 * @param sprayState 状态 当前是否喷淋，1是正在 0是暂停
	 * @param time 喷淋剩余时间 开始后 当前喷淋的剩余时间 如果切换 需要通过总量重新计算
	 * @param surpluswater 开始喷淋后 随时计算 剩余水量 比例 100/94
	 * @param surpluscapacity 开始喷淋后 随时计算 剩余泡沫量 100/6
	 * @param sprayType 喷淋类型 不等于water 即为泡沫 存泡沫ID
	 */
	sprayFireEngine:[],
	//同步时 先同步 sprayFireEngine
	setsprayFireEngine:function(_sprayFireEngine){
		var t = this;
		t.sprayFireEngine = _sprayFireEngine;
	},
	//已经选择的消防车ID 判断不用选择
	makerFireEngineoid:[],
	//按钮事件绘制HTML
	init:function(){
		var t = this;
		//绑定事件
		$('#iconBox .iconTab li').on('click',function(){
			$('#iconBox .iconTab li').removeClass('onlike');
			$(this).addClass('onlike');
			var index = $('#iconBox .iconTab li').index(this);
			$('#iconBox .iconTabBody .iconList').hide();
			$('#iconBox .iconTabBody .iconList').eq(index).show();
		})
		//默认点击第一个
		$('#iconBox .iconTab li').eq(0).trigger('click');
		//测试用
		//$('#iconBox').show();
		//拖动
		$('#iconBox').Tdrag({
			handle:".iconDivHead",
			scope:"#map",
			iscenter:false
		});
		//栏默认展开
		$('#iconBox .iconList .listDl').on("click",function(){
			var span = $(this).find("span.arrow");
			console.log(span.html())
			//有open了就关闭
			if(span.hasClass('open'))
			{
				span.removeClass('open');
				$(this).next().hide('fast');
			}
			//没有就展开
			else{
				span.addClass('open')
				$(this).next().show('fast');
			}
		})
		/*点击元素标绘的组织机构的选项，展示选项*/
		$("#changeLayerSelectUnit").click(function () {
			$("#layerUnitSelectUnit").show();   
   		});
		/*给元素标绘的组织机构的li绑定事件*/
		$('#layerUnitSelectUnit').on('click','li',function(){
			$("#layerDeptId").val($(this).attr("name"));
			if($(this).text()){
			   $("#layerUnitName").text($(this).text()); 
			   $(this).addClass("ZhoverCss").siblings().removeClass("ZhoverCss");//默认选中当前选项	
			}
			$("#layerUnitSelectUnit").hide();   
			var params={"deptId":$(this).attr("name")};
		    $.ajax({
				type : "post",
				url : baseUrl+"/webApi/qryFireEngineByDept.jhtml",
				data: JSON.stringify(params),
				dataType:"json",
				headers: {"Content-Type": "application/json;charset=utf-8"},
				success : function(data) {					
					t.initFireEngine(data.data);
				},error: function(request) {
					layer.msg(networkErrorMsg, {time: 2000});
   	        }
			});
		});
		
		//显示关闭消防车倒计时按钮 当有隐藏元素，则全部显示， 当没有隐藏的就全部隐藏
//		$('#sprayTimeBeginBtn').on("click",function(){
//			//sprayTimeBegin esm-ol-popup
//			$('.sprayTimeBegin').parents(".esm-ol-popup").toggle();
//		})

		//得到用户的消防队， 默认选择
		$.ajax({
			type : "post",
			url : baseUrl+"/webApi/getUserBrigadeId.jhtml",
			data: JSON.stringify({}),
			dataType:"json",
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {
				$('#layerUnitSelectUnit [name="'+data.data+'"]').trigger('click');			
			},error: function(request) {
				layer.msg(networkErrorMsg, {time: 2000});
		   }
		});
		
		showCollectionAndVideoButton(false);
	},
	//绘制消防车分类HTML
	initFireEngine:function(_data){
		var t = this;
		if(_data){
			t.fireEngine = _data;
		}
		/*拼接图层的消防车html*/
		{
		var getFireEngineByDeptHtml = function (obj){
			obj.iconType='8';
			var html="<li id="+obj.id+" onClick='makerFireEngineEvent("+ JSON.stringify(obj)+");' iconType='8' t="+baseUrl+obj.path+">"; 	
			    html+="<div class='meeting'><span><img src="+baseFileUrl+obj.iconPath+"/></span></div>"; 	
			    html+="<p class='txtName'>"+obj.name+"</p>"; 	
			    html+="<p class='txtName'>1辆</p>"; 
			    html+="</li>"; 	
		    return html;
		}
		$("#carContentId").html("");
		var html="";	
		//消防车HTNL
		for(j = 0,len=t.fireEngine.length; j < len; j++) {
			//html+=getFireEngineByDeptHtml(t.fireEngine[j]);
		}
		//不显示消防车
		$("#carContentId").remove();
		}
		/*拼消防分类*/
		//得到分类数据
		var typedata = [];
		for(j = 0,len=t.fireEngine.length; j < len; j++) {
			if(t.fireEngine[j].dicName){
				var _d = typedata.find(function(m){
					return m && m.name && m.name==t.fireEngine[j].dicName
				});
				if(!_d){
					typedata.push({name:t.fireEngine[j].dicName,id:t.fireEngine[j].dicId});
				}
			}
		}
		//console.log("typedata="+JSON.stringify(typedata));
		$('#iconBox .pumperList ol').html('');
		$.each(typedata,function(i,v){
			//按分类得到消防车
			var _fireEngine = t.fireEngine.filter(function(m){
				return m.dicId ==v.id
			});
			//console.log("_fireEngine="+JSON.stringify(_fireEngine));
			//拼接html
			var li = $('<li></li>');
			$('#iconBox .pumperList ol').append(li);
			li.append('<div class="pumperDl">'+v.name+'<em>（'+_fireEngine.length+'）</em><span class="open arrow"></span></div>');
			var pumperDd = $('<div class="pumperDd ca cf"><ul></ul></div>');
			li.append(pumperDd);
			for(j = 0,len=_fireEngine.length; j < len; j++) {
				var html = "";
				_fireEngine[j].iconType='8';
				html+='<li makerFireEngineoid="'+_fireEngine[j].id+'" onClick=\'makerFireEngineEvent('+ JSON.stringify(_fireEngine[j])+');\'>';
				html+='	  <div class="pumpermeeting">';
				html+='		  <span>';
				html+='			  <img src="'+baseFileUrl+_fireEngine[j].iconPath+'">';
				html+='		  </span>';
				html+='	  </div>';
				html+='	  <p class="txtName">'+_fireEngine[j].plateNumber+'</p>';
				html+='</li>';
				pumperDd.find('ul').append(html);
				if(_fireEngine[j].status=="1"){
					t.disableli(_fireEngine[j].id);
				}
			}
		})

		//展开收起
		$('#iconBox .pumperList .pumperDl').on("click",function(){
			var span = $(this).find("span.arrow");
			//有open了就关闭
			if(span.hasClass('open'))
			{
				span.removeClass('open');
				$(this).next().hide('fast');
			}
			//没有就展开
			else{
				span.addClass('open')
				$(this).next().show('fast');
			}
		})
	},
	//点击图标返回HTML
	getFeatureContent:function(Featureobj){
		var t = this;
		var content = '';
		{
		content+='<div class="equip fireEnginefloat" fireEngineId="'+Featureobj.oid+'">';
		content+='<div class="pre"><div class="equipCont"><div class="listDdYJZBTK ca cf" style="display:block;">';
		content+='			<ul>';
		content+='				<li class="location-01 equipCurrent">';
		content+='					<div class="meeting"><span><img src="'+baseUrl+'/images/mapPanel/mee-26.png" /></span></div>';
		content+='					<p class="txtName">装备详情</p>';
		content+='				</li>';
//		content+='				<li class="location-02">';
//		content+='					<div class="meeting"><span><img src="'+baseUrl+'/images/mapPanel/mee-27.png" /></span></div>';
//		content+='					<p class="txtName">喷淋时间</p>';
//		content+='				</li>';
		content+='				<li class="location-03" >';
		content+='					<div class="meeting"><span><img src="'+baseUrl+'/images/mapPanel/mee-28.png" /></span></div>';
		content+='					<p class="txtName">收车</p>';
		content+='				</li>';
		content+='			</ul>';
		/*content+='<div class="directionBox">';
		content+='<div class="topArr fl" onclick="changeCarDirection(270)"><i></i></div>';					
		content+='<div class="rightTopArr fl" onclick="changeCarDirection(315)"><i></i></div>';				
		content+='<div class="rightNewArr fl" onclick="changeCarDirection(0)"><i></i></div>';					
		content+='<div class="rightDownArr fl" onclick="changeCarDirection(45)"><i></i></div>';					
		content+='<div class="downNewArr fl" onclick="changeCarDirection(90)"><i></i></div>';					
		content+='<div class="leftDownArr fl" onclick="changeCarDirection(135)"><i></i></div>';					
		content+='<div class="leftNewArr fl" onclick="changeCarDirection(180)"><i></i></div>';					
		content+='<div class="leftTopArr fl" onclick="changeCarDirection(225)"><i></i></div>';	
		content+='</div>';*/		
		content+='<div class="directionDiv">';
		content+='<div class="carDirection">';
		content+='<div class="carBox">';
		content+='<div class="carBoxWarp"><img src="'+baseUrl+Featureobj.iconPath+'" id="carImgIcon"/></div>';//<div>'+Featureobj.name+'</div>Featureobj.name
		content+='</div>';
		content+='<div class="newTopArr" onclick="changeCarDirection(270)"></div>';		
		content+='<div class="newLeftTopArr" onclick="changeCarDirection(225)"></div>';
		content+='<div class="newLeftArr" onclick="changeCarDirection(180)"></div>';
		content+='<div class="newLeftDownArr"onclick="changeCarDirection(135)"></div>';
		content+='<div class="newDownArr" onclick="changeCarDirection(90)"></div>';
		content+='<div class="newRightDownArr" onclick="changeCarDirection(45)"></div>';
		content+='<div class="newRightArr" onclick="changeCarDirection(0)"></div>';
		content+='<div class="newRightTopArr" onclick="changeCarDirection(315)"></div>';
		content+='</div>';
		content+='</div>';
		
		
		content+='</div></div>';	
		content+='	<!-- 装备信息显示 -->';
		content+='	<div class="equipMsg msgbox"><div class="equipMsgTxt">';
		content+='			<!-- 装备详情 -->';
		content+='			<div class="detilsBox" ><a style="z-index:1000;" class="ol-popup-closer"></a><div class="datum">';
		content+='					<ul class="fireEngineEquipment">';
		content+='						<li class="ca cf">';
		content+='							<span class="fl datumName">灭火器：</span>';
		content+='						    <span class="fl datumNub">X1</span>';
		content+='						</li>';
		content+='						<li class="ca cf">';
		content+='							<span class="fl datumName">口罩：</span>';
		content+='						    <span class="fl datumNub">X2</span>';
		content+='						</li>';
		content+='						<li class="ca cf">';
		content+='							<span class="fl datumName">消防栓：</span>';
		content+='						    <span class="fl datumNub">X1</span>';
		content+='						</li>';
		content+='					</ul></div></div>';
		content+='		</div>';
		content+='	</div>';
//		content+='	<div class="equipMsgTime msgbox"><div class="equipMsgTxt">';
//		content+='			<!-- 喷淋时间 -->';
//		content+='			<div class="detilsBox"><a style="z-index:1000;" class="ol-popup-closer"></a><div class="datum">';
//		content+='					<ul>';
//		content+='						<li class="ca cf">';
//		content+='							<input type="radio" name="foamType" value="water" />水';
//		content+='						</li>';
//		content+='						<li class="ca cf foamTypeName">';
//		content+='							<input type="radio" name="foamType" value="泡沫" />泡沫';
//		content+='						</li>';
//		content+='						<li class="ca cf sprayTime">';
//		content+='							总喷淋时间:<span></span>';
//		content+='						</li>';
//		content+='						<li class="ca cf" style="text-align: center;">';
//		content+='							<input type="button" class="begin" fireEngineId="'+Featureobj.oid+'" value="开始"/>';
//		content+='							<input type="button" class="end" fireEngineId="'+Featureobj.oid+'" value="停止"/>';
//		content+='						</li>';
//		content+='					</ul>';
//		content+='				</div></div>';
//		content+='		</div></div>';
		content+='</div></div>';
		}

		//通知全局 禁用
		t.trigger('disableli',Featureobj);
		return content;
	},
	//禁用 图标 不让选择
	disableli:function(Featureobj){
		var t = this;
		if(Featureobj && Featureobj.oid){
			$('li[makerFireEngineoid="'+Featureobj.oid+'"]').addClass("disable");
		}else if(Featureobj!=""){
			$('li[makerFireEngineoid="'+Featureobj+'"]').addClass("disable");
		}

		var _sprayFireEngine = t.sprayFireEngine.find(function(m){
			return m.oid == Featureobj.oid;
		})
		if(_sprayFireEngine && _sprayFireEngine.sprayState=="1"){
			//按钮控制
			$('.fireEnginefloat .begin').attr('disabled',"disabled");
			$('.fireEnginefloat .end').removeAttr('disabled');
			$('.fireEnginefloat .begin').addClass("disable");
			$('.fireEnginefloat .end').removeClass('disabled');
		}
	},
	//禁用 图标 不让选择
	removedisableli:function(Featureobj){
		//重置消防车
		var pam = Featureobj.oid || Featureobj;

		$('li[makerFireEngineoid="'+pam+'"]').removeClass("disable");
		$('.sprayTimeBegin[fireengineid="'+pam+'"]').parents(".esm-ol-popup").remove();

		var url = baseUrl + "/webApi/useRecord/updateFireEngineStatus.jhtml";
		$.ajax({
			type : "post",
			url :url,
			data: JSON.stringify(pam),
			dataType:"json",
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {	
			}
		});
	},
	//显示后注册点击事件 关闭按钮等
	//参数用 makerFireEngineEvent 方法传递 qryFireEngineByDept 地址获取
	showFeatureEnd:function(Featureobj){
		var t = this;
		$('.fireEnginefloat .equipMsg').hide();//隐藏装备
		$('.fireEnginefloat .equipMsgTime').hide();//隐藏喷淋
		$('#popup-closer').removeClass('fireColseIcon');
		//装备
		$('.fireEnginefloat .location-01').on('click',function(){
			var inited =  $(this).attr("inited");
			$('.fireEnginefloat .equipMsgTime').hide();
			if(inited && inited=="1"){
				$('.fireEnginefloat .equipMsg').show();
			}else{
				//得到装备
				$('.fireEngineEquipment').html('');
				var url =  baseUrl+"/webApi/qryFireEngineEquipmentByfId.jhtml";
				executeAjax(url,Featureobj.oid,function(_dd){
					if(_dd.httpCode==200){
						$.each(_dd.data,function(i,v){
							if(v.name){
								var content = "";
								content+='<li class="ca cf">';
								content+='	<span class="fl datumName">'+v.name+'：</span>';
								content+='	<span class="fl datumNub">X'+v.storageQuantity+'</span>';
								content+='</li>';
								$('.fireEngineEquipment').append(content);
							}
						})
						if($('.fireEngineEquipment').html()==""){
							$('.fireEngineEquipment').html('没有装备')
						}
						$('.fireEnginefloat .equipMsg').show();
						$(this).attr("inited","1");
					}else {
						//layer.msg(_dd.msg, {time: 1000});
					}
				});	
			}
		})
		//喷淋时间
		$('.fireEnginefloat .location-02').on('click',function(){
			var inited =  $(this).attr("inited");
			$('.fireEnginefloat .equipMsg').hide();
			if(inited && inited=="1"){
				$('.fireEnginefloat .equipMsgTime').show();
			}else{
				$('.foamTypeName').html('');
				if(Featureobj.foamTypeId){
					$('.foamTypeName').html('<input type="radio" name="foamType" value="'+Featureobj.foamTypeId+'" />水+'+Featureobj.foamTypeName);
				}
				$('.fireEnginefloat .equipMsgTime').show();
				$(this).attr("inited","1");
				//喷淋时间 spraySpeed 选择喷淋模式事件
				$(".fireEnginefloat .equipMsgTime :radio").click(function(){
					if($(this).attr("name")=="foamType"){
						var time = "";
						time = t.getSurplusTime(Featureobj.water,Featureobj.capacity,$(this).val(),Featureobj.spraySpeed);
						
						//$('.fireEnginefloat .begin').attr('time',time);
						Featureobj.bdgintime = time;
						if(parseInt(time)>=0){
						}else{
							time = 0;
						}
						$(".fireEnginefloat .equipMsgTime .sprayTime span").text(getDateTimeT(time));
					}
				});
				$('.fireEnginefloat .equipMsgTime :radio[value="water"]').click();
			}
		})
		//移除图标 收车
		$('.fireEnginefloat .location-03').on('click',function(){
			t.trigger('sprayRemove',Featureobj);
			remove();
		})
		//关闭弹出敞口
		$('.ol-popup #popup-closer').on('click',function(){
			$(this).parents(".ol-popup").hide();
		})
		
		$('.fireEnginefloat .ol-popup-closer').removeClass('fireColseIcon');
		//关闭弹出敞口
		$('.fireEnginefloat .ol-popup-closer').on('click',function(){
			$(this).parents(".msgbox").hide();
		})
		//开始喷淋
		$('.fireEnginefloat .begin').on('click',function(){
			var sprayType = $('.fireEnginefloat .equipMsgTime input[name="foamType"]:checked ').val(); 
			Featureobj.sprayType = sprayType;
			
			t.trigger('sprayTimeBegin',Featureobj);
		})
		//暂停
		$('.fireEnginefloat .end').on('click',function(){
			t.trigger('sprayTimeEnd',Featureobj);
			//当前喷淋对象
			var _sprayFireEngine = t.sprayFireEngine.find(function(m){
				return m.oid == Featureobj.oid;
			})
			if(_sprayFireEngine){
				clearInterval(_sprayFireEngine.interval);
				_sprayFireEngine.sprayState = 0;//暂停喷淋
				//根据时间计算剩余量 getSurplusVal 更具已使用时间计算 usedtime 随后usedtime 重置
				var v = t.getSurplusVal(_sprayFireEngine.surpluswater,_sprayFireEngine.surpluscapacity,_sprayFireEngine.sprayType,_sprayFireEngine.spraySpeed,_sprayFireEngine.usedtime);
				_sprayFireEngine.surpluswater= v.surpluswater;
				_sprayFireEngine.surpluscapacity=v.surpluscapacity;
				if(_sprayFireEngine.sprayType != "water"){
					//保存使用记录
					var pam = {roomId:currentRoomId,foamTypeId:_sprayFireEngine.sprayType ,fireEngineId:_sprayFireEngine.oid,usedAmount:_sprayFireEngine.surpluscapacity}
					var url = baseUrl + "/webApi/useRecord/insertExtinguisherUseRecord.jhtml";
					executeAjax(url,pam,function(data){
						if(data.httpCode==200){

						}
					});
				}
			}
		})
	},
	//开始喷淋
	sprayTimeBegin:function(Featureobj){
		var  t = this;
		$('.fireEnginefloat .equipMsgTime input[name="foamType"]:checked ').val(Featureobj.sprayType);
		//按钮控制
		$('.fireEnginefloat .begin').attr('disabled',"disabled");
		$('.fireEnginefloat .end').removeAttr('disabled');
		$('.fireEnginefloat .begin').addClass("disable");
		$('.fireEnginefloat .end').removeClass('disabled');
		//当前喷淋对象
		var _sprayFireEngine = t.sprayFireEngine.find(function(m){
			return m.oid == Featureobj.oid;
		})
		//第一次点击开始传来的总时间 第二次通过剩余量重新计算
		var time;
		if(!_sprayFireEngine){
			_sprayFireEngine = Featureobj;
			//存入到喷淋集合
			t.sprayFireEngine.push(_sprayFireEngine);
			//初始TIME
			time = Featureobj.bdgintime;
			//剩余量等于总量
			_sprayFireEngine.surpluswater=parseFloat(Featureobj.water)
			_sprayFireEngine.surpluscapacity=parseFloat(Featureobj.capacity)
		}else{
			//当前时间等于剩余时间
			time = t.getSurplusTime(_sprayFireEngine.surpluswater,_sprayFireEngine.surpluscapacity,Featureobj.sprayType,Featureobj.spraySpeed);
		}
		_sprayFireEngine.sprayType = Featureobj.sprayType;//当前喷淋类型
		_sprayFireEngine.sprayState = 1;//开始喷淋
		_sprayFireEngine.usedtime = 0;//本次使用时间
		//显示窗口
		//得到计时器显示标签
		var sprayTimeBegin = $('[fireEngineId="'+Featureobj.oid+'"].sprayTimeBegin');
		if(sprayTimeBegin.length==0){
			var content = '<div fireEngineId="'+Featureobj.oid+'" class="sprayTimeBegin">剩余喷淋时间<span>'+getDateTimeT(time)+'</span></div>';
			var newpoint = [Featureobj._point[0],parseFloat(Featureobj._point[1])];
			openMultiPopup(Featureobj.oid,newpoint,content);
			sprayTimeBegin = $('[fireEngineId="'+Featureobj.oid+'"].sprayTimeBegin');
			$('.esm-ol-popup').addClass('delete-ol-popup').css('bottom','-60px').css("left","-100px");
			$('.esm-ol-popup-closer').hide();
			
		}
		if(_sprayFireEngine.interval){
			clearInterval(_sprayFireEngine.interval);
		}
		//开始循环
		if(_sprayFireEngine.interval){clearInterval(_sprayFireEngine.interval)};
		_sprayFireEngine.interval = setInterval(function(){
			//console.log("计时:"+time);
			time = parseInt(time)-1;
			if(time && time>0 && typeof(time)!="undefined"){
				sprayTimeBegin.find('span').text(getDateTimeT(time));
				sprayTimeBegin.attr('time',time);
				//使用时间累加
				_sprayFireEngine.usedtime++;
			}
			//为0停止
			if(time==0 || time<0){
				if(_sprayFireEngine.interval){clearInterval(_sprayFireEngine.interval)};
				(function(_Featureobj){
					$('.sprayTimeBegin').parents(".esm-ol-popup").remove();
					t.sprayTimeEnd(_Featureobj);
				})(Featureobj);
			}
		},1000);
	},
	//暂停喷淋
	sprayTimeEnd:function(Featureobj){
		var  t = this;
		
		$('.fireEnginefloat .end').attr('disabled',"disabled");
		$('.fireEnginefloat .begin').removeAttr('disabled');
		$('.fireEnginefloat .begin').removeClass("disable");
		$('.fireEnginefloat .end').addClass('disabled');
		

		
	},
	//清除消防车 从 sprayFireEngine
	sprayRemove:function(Featureobj){
		var t = this;
		//执行所有的暂停
		t.sprayTimeEnd(Featureobj);
		var index = -1;
		for (var i = 0; i < t.sprayFireEngine.length; i++) {
			var element = t.sprayFireEngine[i];
			if(element.oid == Featureobj.oid){
				index = i;
			}
		}
		if(index>=0){
			t.sprayFireEngine.splice(index, 1);
			var sprayTimeBegin = $('[fireEngineId="'+Featureobj.oid+'"].sprayTimeBegin').parents(".ol-selectable");
			sprayTimeBegin.remove();
		}
		//关闭 收车那些按钮
		closeInfoWindow();
		//('.sprayTimeBegin').parents(".esm-ol-popup").remove();

		//移除样式
		//t.removedisableli(Featureobj.oid);

		
	},
	/**
	 * 计算剩余时间算法
	 * @param water 当前水容量
	 * @param capacity 当前泡沫容量
	 * @param sprayType 当前类型 是混合还是水 不等于water 即为泡沫 存泡沫ID
	 * @param spraySpeed 速度
	 * @returns 返回time 剩余时间 以秒为单位
	 */
	getSurplusTime:function(water,capacity,sprayType,spraySpeed){
		try{
			isundefined = function(v){
				if(typeof(v) == "undefined" || v==null){
					return true;
				}
				return false;
			}
			if(isundefined(water)||isundefined(capacity)||isundefined(sprayType)||isundefined(spraySpeed)){
				return 0;
			}
			water = parseFloat(water);
			capacity = parseFloat(capacity);
			spraySpeed =  parseFloat(spraySpeed);
			if(!spraySpeed || water<=0 || capacity<=0 ){return 0;}
//			console.log("水:"+water+";添加剂"+capacity+";速度"+spraySpeed+";类型"+sprayType)
			if(sprayType=="water"){
				time = water/spraySpeed;
			}
			else{
				//如果是混合类型不 判断谁少算谁
				var v = 6/94;//单位配比
				var uwater=0.00,ucapacity=0.00,_time=0,b =true;
				while(b){
					_time++;
					uwater = uwater + spraySpeed*_time*0.94;
					ucapacity = ucapacity + spraySpeed*_time*0.06;
					if(uwater>=water || ucapacity>=capacity){
						b = false;
					}
				}
				time = _time;
			}
			return parseFloat(time)*60;
		}
		catch(e){
			return 0;
		}
	},
	/**
	 * 计算剩余容量算法 在暂停时执行
	 * @param water 当前水容量
	 * @param capacity 当前泡沫容量
	 * @param sprayType 当前类型 是混合还是水 不等于water 即为泡沫 存泡沫ID
	 * @param spraySpeed 速度
	 * @param usedtime 使用时间 已经用了多长时间
	 * @returns 返回 容量
	 */
	getSurplusVal:function(water,capacity,sprayType,spraySpeed,usedtime){
		var surpluswater,surpluscapacity;
		try{
			isundefined = function(v){
				if(typeof(v) == "undefined"){
					return true;
				}
				return false;
			}
			if(isundefined(water)||isundefined(capacity)||isundefined(sprayType)||isundefined(spraySpeed)){
				return 0;
			}
			water = parseFloat(water);
			capacity = parseFloat(capacity);
			spraySpeed =  parseFloat(spraySpeed)/60;
			usedtime = parseFloat(usedtime);
			if(!spraySpeed || water<=0 || capacity<=0 ){return 0;}
//			console.log("水:"+water+";添加剂"+capacity+";速度"+spraySpeed+";类型"+sprayType)
			if(sprayType=="water"){
				surpluswater = water-(spraySpeed*usedtime);
				surpluscapacity = capacity;
			}
			else{
				surpluswater = water-(spraySpeed*usedtime*0.94);
				surpluscapacity = capacity-(spraySpeed*usedtime*0.06);
			}
		}
		catch(e){}
		return {'surpluswater':surpluswater,'surpluscapacity':surpluscapacity};
	},
	/**
	 * 触发事件 触发当前类方法
	 * @param name 事件名称 
	 * @param args 参数
	 */
	trigger:function(name,args){
		var t = this;
		//同步事件之前先把重要集合同步
		send(JSON.stringify({"evetype":"setsprayFireEngine","val":t.sprayFireEngine})); 
		send(JSON.stringify({"evetype":name,"val":args})); 
	}
}



//订阅房间
var subRoom;
//订阅聊天
var subChat;
//
var subCollect;

//已有的房间所有操作 同步回显
function haveRoomEvent(accRoom){
	$("#chatBtn").show();
	$("#talkAccident").val(currentRoomName+"房间命令");
	//如果等于空 重新注册
	
	//增加延迟， 避免正在断开注册失败
	//setTimeout(function(){
	//创建房间编号并同步
		subRoom = stompClient.subscribe('/topic/accident' + accRoom, function (mes) {
		 var result = JSON.parse(mes.body);
		 //判断是否为当前用户，若当前用户不同步
		 //if(result.userId == currentUserId){
			//return false;
		// }
		 if(result.isdelete){
			 synMarkerRemove(result);
			 if(result.datatype==="6"){
				 deleteAccidentFeatureByid(result.id);
			 }
			 if(result.datatype==="8"){
				 $._IconBox.removedisableli(result.dataid);
			 }
		 }
		 
		 
		 //推送修改指挥权
		 else if(result.evetype && result.evetype=="changeLeaderName"){
			if(typeof result.val == "undefined" || result.val == null || result.val == ""){
				$layer.msg('指挥权名称不能为空。', {icon: 2, time: 2000});
			}else{
				$("#identConductor").text(result.val);
				$("#changeLeadersDiv .Operator i").text(result.val);
				$layer.msg('修改成功', {icon: 1, time: 1000});
				cancleChangeLeaderDiv();
			}
		 }
		//推送场景搭建，地图展示场景图片
		 else if(result.evetype && result.evetype=="changeSceneResource"){
			var addtomap=[];
			var obj = result.val;
			obj.forEach(function(val){
				addtomap.push({
					url : val.url,
					extent : val.extent,
					id : val.imageId,
					imageId : val.imageId
				});
			})
			
			addImagesToMap(addtomap);
	 	}else if(result.evetype && result.evetype=="deleteSceneResource"){
			var addtomap=[];
			var id = result.id;
			removeImagesByid(id);
	 	}
		 else if(result.evetype && $._Accident[result.evetype]){ //推送关闭事故，弹出事故报告 结束事故
			$._Accident[result.evetype](result.val);
		 }else if(result.evetype && $._IconBox[result.evetype]){
			$._IconBox[result.evetype](result.val);
		 }else if(result.evetype && result.evetype=="zhmn"){
		// 事故模拟分析
			 var zhmnObj = result.val;
			 var zhmnType = zhmnObj.accType;
			 var objData = zhmnObj.content;
			
			 //zhmnFlag为fasle是被同步者，最小化展示
			 if(!zhmnFlag){
				 if(zhmnType=="zhmn_rfsfx"){
//					 $("#zhmnQxzsOutDiv").show();
					 cleanKuoSanDrawLayer();
					 //热辐射分析
					 if(qxzsIsopen){
						 $("#zhmnQxzsOutDiv").show();
					 }
					 var mathGranularDLsit = objData.mathGranularDLsit;
					 zhmn_rsfxRbl(mathGranularDLsit[0],mathGranularDLsit[1],mathGranularDLsit[2]);
				 }else if(zhmnType=="zhmn_rfsfhf"){
					 //防辐射服计算
//					 $("#zhmnQxzsOutDiv").show();
					 
					 var point = objData.point;
					var items=[{distance:objData.dieRadius,color:'#FF0000'}];
					$("#zhmnQxzs_aqjl").html(objData.dieRadius);
					drawBZ('cgybz',items,point,1,true,objData.rotation);
				 }else if(zhmnType=="zhmn_pjsj"){
//					 $("#zhmnZZYPPJDiv").show();
//					 timeIntervalEnd();
					 //喷溅时间
					 if(zzyppjIsopen){
						 $("#zhmnZZYPPJDiv").show();
					 }
					 startTimeInterval(objData);
//					 $("#jieguo").val("预计时间："+shortTime);
				 }else if(zhmnType=="zhmn_rsfx"){
//					 $("#zhmnRSWFXDivOut").show();
					 //燃烧分析
					 $("#closeZhmnRSWFXDivOut").hide();
					 if(rsxfxIsopen){
						 $("#zhmnRSWFXDivOut").show();
					 }
					 var co2Rate = objData.co2Rate;
					 var coRate = objData.coRate;
					 var no2Rate = objData.no2Rate;
					 var noRate = objData.noRate;
					 var so2Rate = objData.so2Rate;
					 rswfxTubiaoChartShow2(coRate,co2Rate,no2Rate,noRate,so2Rate);
				 }else if(zhmnType=="zhmn_qzks"){
					 cleanKuoSanDrawLayer();
					 $("#zhmnKSSJDivOutSyn").show();
					 $("#ysslDiv").show();
					 timeIntervalEnd();
					 sjzUrl = objData.sjzUrl;
					 sjzParams = objData.sjzParams;
					 if(objData.titleTime!=""){
						 $("#zhmnKSSJContentSyn").html(objData.titleTime);
					 }
					 //扩散zhmn_qzks
					 var layer = layerIsExist('buildGeometryLayer',global.map);
					 if(layer){
						 clearBuildGeometryLayer();
					 }
					 for(var i=0;i<objData.pointDataList.length;i++){
						 objData.coords = eval('(' + objData.pointDataList[i] + ')');
						 objData.isBorder = false;
						 objData.borderColor = '';
						 objData.fillColor = iteamsColor[i];
						 objData.fillTransparency = '0.6';
						 buildGeometryByCoords(objData.coords,objData.isBorder,objData.borderColor,objData.fillColor,objData.fillTransparency);
					}
					drawHighLine('drawHighLine',objData.point);
				 }else if(zhmnType=="zhnm_bz"){
						var items = [];
						for(var i=0;i<objData.length;i++){
							var obj = {};
							obj.distance = objData[i];
							obj.color = iteamsColorBZ[i];
							items[i]=obj;
						}
						var point =[zhmnAccidentPoint[0],zhmnAccidentPoint[1]];
						drawBZ('cgybz',items,point,1,true);
						$("#damagePeopleDiv").show();
						 timeIntervalEnd();
				 }else if(zhmnType=="zhmn_qzksTime"){
//					 $("#zhmnHZMYFXDivOut").show();
					 	
					 //扩散
					 var layer = layerIsExist('buildGeometryLayer',global.map);
						if(layer){
							clearBuildGeometryLayer();
						}
					 $("#zhmnKSSJDivUlSyn li").removeClass();
					 $("#"+objData.liId).addClass("currentTime");
					 var layer = layerIsExist('buildGeometryLayer',global.map);
					 if(layer){
						clearBuildGeometryLayer();
					 }
					 for(var i=0;i<objData.pointDataList.length;i++){
						 objData.coords = eval('(' + objData.pointDataList[i] + ')');
						 objData.isBorder = false;
						 objData.borderColor = '';
						 objData.fillColor = iteamsColor[i];
						 objData.fillTransparency = '0.6';
						 buildGeometryByCoords(objData.coords,objData.isBorder,objData.borderColor,objData.fillColor,objData.fillTransparency);
					}
					 var layer = layerIsExist('drawHighLine',global.map);
					 if(layer){
						 clearBuildGeometryLayer();
					 }
					 drawHighLine('drawHighLine',objData.point);
				 }else if(zhmnType=="zhmn_zzks"){
//					 $("#zhmnHZMYFXDivOut").show();
					 cleanKuoSanDrawLayer();
					 sjzUrl = objData.sjzUrl;
					 var result = objData;
					 centerAndZoom(zhmnAccidentPoint,17);
					 spread(zhmnAccidentPoint[0],zhmnAccidentPoint[1],objData.isLeft,objData.wtoken,0);
					 drawHighLine('drawHighLine',zhmnAccidentPoint);
					 $("#zhmnKSSJDivOut").show();
				 }else if(zhmnType=="isShwo"){
					 hidePanl();
					 if(objData==0){
						 $("#zhmnRSWFXDivOut").show();
						 $("#zhmnZZYPPJDiv").show();
						 $("#zhmnZZYPPJDiv").show();
					 }else if(objData==1){
						 $("#zhmnRSWFXDivOut").show();
						 $("#zhmnZZYPPJDiv").show();
						 $("#zhmnZZYPPJDiv").hide();
						 timeIntervalEnd();
					 }else if(objData==2){
						 $("#zhmnKSSJDivOut").show();
						 timeIntervalEnd();
					 }else if(objData==3){
						 $("#damagePeopleDiv").show();
						 timeIntervalEnd();
					 }
				 }
			 }
		 }
		 else{
			 if(result.properties.params.action==="事故点"){ // 如果传过来事故点的信息，就记录变量
				 zhmnAccidentPoint=result.geometry.coordinates;
			 }

			 positionSynMark(result);
		 }
	  });

		 //接到聊天数据
		subChat = stompClient.subscribe('/topic/chat/' + accRoom, function (mes) {	
		 var obj = eval('(' + mes.body + ')');
		 if(obj.type=="mingling"){
			 //显示在MINI聊天界面
			 if($("#chatPanel").css("display") == "none"){
				 $(".chatWarpCont").html('<span>'+ obj.user +'：</span>' + obj.val);
				 $("#chatMini").show();
				 
				 if(chatTimer){
					 clearTimeout(chatTimer);
					 chatTimer = null;
					 chatTimer = setTimeout(function(){
						 $("#chatMini").hide();
					 },3000);
				 }else{
					 chatTimer = setTimeout(function(){
						 $("#chatMini").hide();
					 },3000);
				 }
			 }
			 //显示正常聊天界面
			 var html = "<li><div class='layim-chat-user'><cite style='left:0px'>" + obj.user + "<i>" + obj.createTime + "</i></cite></div><div class='layim-chat-text'>" + obj.val + "</div></li>"
			 $("#chatList").append(html);
			 $("#chatListDiv").scrollTop($("#chatListDiv").prop('scrollHeight'));
	     } else if(obj.type=="renwu"){
	    	 //显示在MINI聊天界面
	    	 if($("#chatPanel").css("display") == "none"){
	    		 $(".chatWarpCont").html('<span>'+ obj.user +'：</span>' + obj.val);
	    		 $("#chatMini").show();
	    		 if(chatTimer){
	    			 clearTimeout(chatTimer);
	    			 chatTimer = null;
	    			 chatTimer = setTimeout(function(){
	    				 $("#chatMini").hide();
	    			 },3000);
	    		 }else{
	    			 chatTimer = setTimeout(function(){
	    				 $("#chatMini").hide();
	    			 },3000);
	    		 }
	    	 }
	    	 //显示正常聊天界面
	    	 var renwuhtml = "<tr><td>"+ obj.val+"</td><td>"+ obj.renWuFaBuRen+"</td><td class='green' id="+obj.id+" onClick='changeRenWuStatus("+obj.id+");'><a>"+ obj.status+"</a></td><td id='feedbackUser"+obj.id+"'></td><td id='feedbackDesc"+obj.id+"'></td></tr>";
			 $("#chatRenwuListInfo").append(renwuhtml);
	    	 $("#chatRenWuListDiv").scrollTop($("#chatRenWuListDiv").prop('scrollHeight')); 
		 }else if(obj.type=="feedback"){
			 $("#"+obj.id).text("已反馈");
			 $("#feedbackUser"+obj.id).text(obj.feedback);
			 $("#feedbackDesc"+obj.id).text(obj.feedbackDesc);
		 }
		 
		});
		
  //},100);
}


function changeRenWuStatus(id){
	if($("#"+id).text()=="已反馈"){
	  layer.msg("该任务不能重复反馈", {time: 2000});
	  return;
	}
	$("#renWuId").val(id);
	$("#taskContentDiv").show();
}

/**
 * 将任务状态变成已反馈
 * @returns
 */
function saveRenWu(){
	if(!$("#feedbackId").val()){
		layer.msg("请输入反馈者", {time: 2000});
		return;
	}
	var json = {id:$("#renWuId").val(),feedback:$("#feedbackId").val(),type:"feedback",feedbackDesc:$("#renWuDesc").val()}
	var params = {id:$("#renWuId").val(),feedback:$("#feedbackId").val(),desc:$("#renWuDesc").val()}
	stompClient.send("/team/chat/" + $("#room").text(),{},JSON.stringify(json));
	var url = baseUrl+'/webApi/updateRenWuStatus.jhtml';
	executeAjax(url,params,function(data){
		if(data.httpCode==200){
			closePanelMath('taskContentDiv');
			$("#"+$("#renWuId").val()).text("已反馈");
			$("#feedbackUser"+$("#renWuId").val()).text($("#feedbackId").val());
			$("#feedbackDesc"+$("#renWuId").val()).text($("#renWuDesc").val());
		}
	});
}


function showThreeButton(){
	$("#huatongButton").show();
	$("#yjzhqButton").show();
	$("#jsjyButton").show();
	$('#ysbhli').show();
	$('#pointBtn').show();
	$('#ysbhBtn').show();
	$('#zntjBtn').show();
	//$('#sprayTimeBeginBtn').show();
	$('#zhmibtn').show();
}

function changeLeaders(){
	$("#changeLeadersDiv").show();
	$("#changeLeaderName").val('');
	var leaderName = $("#changeLeaderName").val();
}

function cancleChangeLeaderDiv(){
	$("#changeLeadersDiv").hide();
}

function saveChangeLeaders(){
	var name = $('#changeLeaderName').val();
    var previousName = $('#identConductor').text();
	var params = {};
	params.id =getTimestamp();
	params.roomId = currentRoomId;
	params.action = "指挥权移交";
	params.content = JSON.stringify({"name":name,"previousName":previousName});
	params.type = 1;
	params.isDelete = 1;//删除0，添加1
	params.createBy = currentUserId; //修改等路人 todo
	params.director = currentUserName; //修改等路人 todo
	saveRecord(params);
	var params2 = {};
	params2.id=currentRoomId;
	params2.accidentConductor=name;
	var url2 = baseUrl+'/webApi/RoomChangeLeaders.jhtml';
	$.ajax({
		type:'POST',
		url: url2,
		headers: {"Content-Type": "application/json;charset=utf-8"},
		data: JSON.stringify(params2),
		dataType:'JSON',
		success: function(data){
			if(data.httpCode==200){
				send(JSON.stringify({"evetype":"changeLeaderName","val":name}));
			}else{
			}
		}
	});
}

function send(featureJSON){
	var oJson = JSON.parse(featureJSON);
	oJson.userId = currentUserId;
	var featureJSON = JSON.stringify(oJson);
	if(currentRoomName){
		stompClient.send("/team/" + currentRoomName,{},featureJSON);
	}
}

/**
 * 保存标绘图形 todo
 * */
function saveSynMarker(featureJson,action,feature,callback){
	var params = {};
	params.id =getTimestamp();
	params.content = featureJson;
	params.roomId = currentRoomId;
	var objO = feature.get('params');
	params.action = objO.action;
	params.fireEngineId = objO.id;
	//params.content = featureJson;
	params.createBy = currentUserId;  
	params.director = currentUserName;  
	params.type=objO.recordType=="esm27"?11:objO.recordType;
	params.isDelete=1;
	var url = baseUrl+'/webApi/insertIncidentRecord.jhtml';
	if(feature.get('params').isExist){
		params.id = feature.getId();
		params.id = feature.get('params').dataid;
		url = baseUrl+'/webApi/updateIncidentRecord.jhtml';
	}
	$.ajax({
		type:'POST',
		url: url,
		ladeview:false,
		headers: {"Content-Type": "application/json;charset=utf-8"},
		data: JSON.stringify(params),
		dataType:'JSON',
		success: function(data){
			if(data.httpCode==200){
				feature.get('params').isExist = true;
				callback(feature,data.data.id);
				if(params.iconType==8){
					changeFireEngine(params.fireEngineId);
				}
			}else{
				layer.msg(data.msg, {time: 1000});
			}
		}
	});
}

/**
 * 从数据库删除标注
 * */
function deleteSynMarker(featureJson,action,feature){
	//todo 调用后台接口 不能直接刪除 要新增 
	var params = {};
	params.id =getTimestamp();
	params.content = featureJson;
	params.roomId = currentRoomId;
	var objO = feature.get('params');
	params.action = objO.action;
	params.fireEngineId = objO.id;
	//params.content = featureJson;
	params.isDelete = 0;//添加 1 刪除 0
	params.type = objO.iconType;
	params.createBy = currentUserId;  
	params.director = currentUserName;  
	params.type=objO.recordType;
	//如果类型为 地图标点 创建事故点 则不回发
	if(feature.get("params").recordType=="accLocationCompany"){
		return;
	}
	var url = baseUrl+'/webApi/insertIncidentRecord.jhtml';
	$.ajax({
		type:'POST',
		url: url,
		headers: {"Content-Type": "application/json;charset=utf-8"},
		data: JSON.stringify(params),
		dataType:'JSON',
		success: function(data){
			if(data.httpCode==200){
				//feature.get('params').isExist = true;
				sendDeleteSocker(feature.getId(),feature.get('plotType'),feature.get('params').catecd,params.type,feature.get('params').id);
				if(params.iconType==8){
					//消防车退回+1
					//changeFireEngine(params.fireEngineId);
				}if(params.iconType==6){
					//消防车退回+1
					//deleteAccidentFeatureByid(feature.getId());
				}
			}else{
				layer.msg(data.msg, {time: 1000});
			}
		}
	});
}

/**
 * 同步删除标注
 * */
function sendDeleteSocker(id,plotType,catecd,datatype,dataid){
	var param = {};
	param.isdelete = true;
	param.id = id;
	param.plotType = plotType;
	param.catecd = catecd;
	param.datatype = datatype;
	param.dataid = dataid;
	if(currentRoomName){
		stompClient.send("/team/" + currentRoomName,{},JSON.stringify(param));
	}
}

/**
 * 查看事故的流程
 * @param obj
 * @returns
 */
function getIncidentRecordDetail(createTime){
	$("#jyjlMapDiv").show();
	var params={"createTime":createTime};
	$.ajax({
		type : "post",
		url : baseUrl+"/webApi/getIncidentRecordDetail.jhtml",
		data: JSON.stringify(params),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				var dataList = data.data;
				if(dataList.length>0){
					showHistoryFeature(dataList);
				}
			}
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	})
}

//查看文档
function viewDoc(id){
	$.ajax({
		type : "post",
		url :baseUrl+"/dangers/qryDangerDoc.jhtml",
		data: id,
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				$("#docName").text(data.data.dangers.cName);
				$("#docName2").text(data.data.dangers.cName);
				$("#docLX").text(data.data.dangers.typeName);
				$("#docML").html('');
				var html = "";
				var viewDocId="docView";
				$.each(data.data.catalogs, function(i,item){  
					if(i == 0){
						html += "<li class='activeOn' id='docml" + item.page + "'><a href='javascript:gotoPage(\""+ item.page +"\",\""+viewDocId+"\")'><span></span>" + item.name + "</a></li>";
					}else{
						html += "<li id='docml" + item.page + "'><a href='javascript:gotoPage(\""+ item.page +"\",\""+viewDocId+"\")'><span></span>" + item.name + "</a></li>";
					}
				});
				$("#docML").html(html);
				$.when($("#docView").show()).then(function (d) {
					loadDoc(baseFileUrl + data.data.dangers.swfPath,'docView');
			    });
				
				
			}else{
				layer.msg(systemAbnormalityMsg, {time: 2000});
			}
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	})
}

function loadDoc(file,viewid){
	/*if(viewid){
		$('#'+viewid+' .dgWord').append($('#viewerPlaceHolder'));
	}
	if(fp){
		$FlexPaper().loadSwf(file);
	}else{
		fp = new FlexPaperViewer(	
				 'js/flexPaper/FlexPaperViewer',
				 'viewerPlaceHolder', { config : {
				 SwfFile : escape(file),
				 Scale : 0.9, 
				 ZoomTransition : 'easeOut',
				 ZoomTime : 0.5,
				 ZoomInterval : 0.2,
				 FitPageOnLoad : true,
				 FitWidthOnLoad : false,
				 FullScreenAsMaxWindow : false,
				 ProgressiveLoading : false,
				 MinZoomSize : 0.2,
				 MaxZoomSize : 5,
				 SearchMatchAll : false,
				 InitViewMode : 'Portrait',
				 PrintPaperAsBitmap : false,
				 ViewModeToolsVisible : true,
				 ZoomToolsVisible : true,
				 NavToolsVisible : true,
				 CursorToolsVisible : true,
				 SearchToolsVisible : true,
				 localeChain: 'zh-CN'
		}});
	}*/
	
	$('#'+viewid+' .dgWord iframe')[0].onload=function(){
		var iframe = $('#'+viewid+' .dgWord iframe')[0].contentWindow;
		iframe.loadDoc(file);
	}
	$('#'+viewid+' .dgWord iframe').attr("src",baseUrl+"/webApi/report/flexPaper.jhtml");
}

/**
 * 显示文档
 * @param file 文档路径
 * @param title 文档标题
 */
function showDoc(file,title){
	title = title && title!=''?title:"文档显示";
	var html = $('<div class="dangerous-frame">'
              +'    <div class="dgBox"> <i onclick="closeDocView(this)" class="dgColse"></i>'
              +'        <div class="dgHdName dragDiv" style="cursor: move;"><span>文档显示</span></div>'
              +'        <div class="ca cf dgContent borderNew" style="padding: 5px 5px;">'
              +'            <div class="fr dgCtright" style="width: 100%;margin: 0;">'
              +'                <div style="width: 100%;margin: 0;padding: 0;" class=""><iframe style="width:100%;height:480px;border:0;" src="'+baseUrl+'/webApi/report/flexPaper.jhtml"></iframe></div>'
              +'            </div>'
              +'        </div>'
              +'    </div>'
              +'</div>');
	
	$('body').after(html);
	html.find(' iframe').attr("src",baseUrl+"/webApi/report/flexPaper.jhtml");
	html.find(' iframe').on("load",function(){ 
    var iframe = html.find(' iframe')[0].contentWindow;
	  iframe.loadDoc(file);
     var _a = iframe.document.getElementById('viewerPlaceHolder');//width:100%;height:470px;display:block
    $(_a).css('width','100%').css('height','470px');
	});  
}

//关闭文档
function closeDocView(e){
	$(e).parents('.dangerous-frame').hide();
}

function gotoPage(page,viewid){
	$(".activeOn").removeClass('activeOn');
	//$FlexPaper().gotoPage(page);
	$("#docml"+ page).addClass('activeOn');
	$('li[docml="'+page+'"]').addClass('activeOn');
	var iframe = $('#'+viewid+' .dgWord iframe')[0].contentWindow;
	iframe.gotoPage(parseInt(page));
}

/*
 * 地图搜索框展示
 * */
function showOrHideSearchPanel(){
	$("#showSearchPanel").show();
}

/*
 * 搜索框展示
 * */
function showOrHideSearchResutePanel(){
	$("#searchResutePanel").show();
}

/*
 * 关闭搜索框
 */
function closeSearchResutePanel(isAround){
	$("#searchResutePanel").hide();
	$("#aroundSearchResult").html('');
	$("#allSearchInput").val('');
	if(isAround){
		$("#showSearchPanel").hide();
		searchClear();
	}else{
		
	}
}


//开启聊天界面
function openChat(){
	$("#chatMini").hide();
	$("#chatPanel").show();
}

//关闭聊天界面
function closeChatPanel(){
	$("#chatPanel").hide();
}

//发送聊天信息
function sendChat(){
	if($("#chatCon").val()){
		var json = {user:currentUserName,val:$("#chatCon").val(),type:"mingling"}
		stompClient.send("/team/chat/" + $("#room").text(),{},JSON.stringify(json));
		var params = {};
		params.id =getTimestamp();
		params.roomId = currentRoomId;
		params.action = "发布命令";
		params.actionDesc=$("#chatCon").val();
		params.content = JSON.stringify({"command": $("#chatCon").val()});
		params.type = 7;
		params.isDelete = 1;//删除0，添加1
		params.createBy = currentUserId; //修改等路人 todo
		params.director = currentUserName; //修改等路人 todo
		saveRecord(params);
		$("#chatCon").val('');
	}
}

//发送任务信息
function sendChatRenWu(){
	if(!$("#renWuFaBuRen").val()){
		layer.msg("请输入发布者", {time: 2000});
		return;
	}
	if($("#chatRenWuCon").val()){
		var params = {};
		params.id =getTimestamp();
		var json = {user:currentUserName,
				    val:$("#chatRenWuCon").val(),
				    type:"renwu",
				    renWuFaBuRen:$("#renWuFaBuRen").val(),
				    status:"未反馈",
				    id:params.id
				    }
		stompClient.send("/team/chat/" + $("#room").text(),{},JSON.stringify(json));
		params.roomId = currentRoomId;
		params.action = "发布任务";
		params.actionDesc=$("#chatRenWuCon").val();
		params.content = JSON.stringify({"command": $("#chatRenWuCon").val()});
		params.type = 13;
		params.isDelete = 1;//删除0，添加1
		params.status=1;
		params.publisher=$("#renWuFaBuRen").val();
		params.createBy = currentUserId; //修改等路人 todo
		params.director = currentUserName; //修改等路人 todo
		saveRecord(params);
		$("#chatRenWuCon").val('');
	}
}

//文档查看向左
function wordListleft(){
//margin-left -135px equipmentdocview
	var ul  = $('#equipmentdocview ul.wordList');
	var val = 135;
	//边界
	if(parseInt(ul.css("marginLeft"))>=0){
	  return;
	}else{
	 ul.animate({
		"marginLeft":'+='+val+'px'
	 })
	}
}

//文档查看向右
function wordListright(){
	var ul  = $('#equipmentdocview ul.wordList');
	var val = 135;
	//边界
	if(false){
	  return;
	}else{
	 ul.animate({
		"marginLeft":'-='+val+'px'
	 })
	}
}

function setRoadPointInfo(name,type){
	if(type=="start"){
		$("#start").text(name);
	}else{
		$("#end").text(name);
	}
	$("#routePanel").show();
	closeSearchResutePanel(true);
}

function setRoadInfo(distance,time){
	var html = "推荐路线 耗时：<span>"+time+"</span>| 距离：<span>"+distance+"</span>";
	$("#resultInfo").html(html);
}

function walkTime(){
	$("#resultInfo").val();
	$("#routePanel").attr("class","oute route-content walk");
	showRoadView('walking');
}

function bikeTime(){
	$("#routePanel").attr("class","oute route-content bike");
	showRoadView('riding');
}

function driveTime(){
	$("#routePanel").attr("class","oute route-content drive");
	showRoadView('driving');
}
function closeJyjlMapDiv(){
	$("#jyjlMapDiv").hide();
}

function showRoutePanel(){
	$("#routePanel").show();
}

function hiddenRoutePanel(){
	$("#routePanel").hide();
	$("#start").text('');
	$("#end").text('');
	$("#resultInfo").html('');
	routeClear();
}

 //阻止冒泡事件的兼容性处理 
 function stopBubble(e) { 
	if(e && e.stopPropagation) { //非IE  
	  e.stopPropagation(); 
	} else { //IE 
	  window.event.cancelBubble = true; 
	} 
  } 

/**
 * 周边搜索结果列表展示
 * */
function initAroundSearchResult(features){
	$("#aroundSearchResult").html('');
	for(var i=0;i<features.length;i++){
		var feature = features[i];
		$("#aroundSearchResult").append("<li id='"+feature.getId()+"' catecd='"+feature.get('catecd')+"' class='olf ca cf'><i></i>"+feature.get("name")+"</span></li>")
	}
	$(".olf").click(function(){
		mapSearchLocation($(this).attr("catecd"),$(this).attr("id"));
	});
	
	$("#searchResutePanel").show();
}

function clearResultWindow(){
	$("#aroundSearchResult").html('');
}
/*
 * 
 */
function closeRoomPanel(){
	$("#roomContentInsert").hide();
	$("#roomContentJoin").hide();
}

//切换重点单位得宽 、窄页面
function showKeyUnitZhai(){
	keyUnit=1;
	$("#zddwListKuanDiv").hide();
	$("#zddwListDiv").show();
	$("#leftMenuContentZDDW").css("width","260px");
	//subMeusDivZhai();
	setMeunPosition();
}

//切换重点单位得宽 、窄页面
function showKeyUnitKuan(){
	pageNum=1;	
	keyUnit=2;
	$("#zddwListDiv").hide();
	$("#zddwListKuanDiv").show();
	$("#leftMenuContentZDDW").css("width","560px");
	getKeyUnitKuanAjax();
	//subMeusDivKuan();
	setMeunPosition('maxMenu');
}

//切换重点单位得宽 、窄页面
function getKeyUnitKuanAjax(){
	var html="";
	$("#manageUnitList2").html("");
	$("#keyUnitPage2").html("");
	var keyWord =$("#unitKeyword2").val();
	var zdswFireBrigadeId =$("#zdswFireBrigadeId2").val();
	
	/*
	 * 去掉分页
	 * @ 修改params的参数
	 * @ 修改url, qryKeyUnitListKuan.jhtml
	 * */
	//var params={"name":keyWord,"fireBrigadeId":zdswFireBrigadeId,"pageNum":pageNum,"pageSize":pageSize};
	var params={"name":keyWord,"fireBrigadeId":zdswFireBrigadeId};	
	if(accidentCompany.lonlat4326){
		params.lon=accidentCompany.lonlat4326[0];
		params.lat=accidentCompany.lonlat4326[1];
	}
    $.ajax({
		type : "post",
		url : baseUrl+"/webApi/qryKeyUnitListKuanAll.jhtml",
		data: JSON.stringify(params),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				if(data.data&&data.data.length>0){
					for(j = 0,len=data.data.length; j < len; j++) {
						html+=getUnitKuanHtml(data.data[j],j);
					}
				}else{
					html+="<li class='NoTime'>暂无数据</li>";
				}
				
				$("#manageUnitList2").append(html);	
				var changeType = $('#manageUnitList2').find('.keynoteHd');
				  $.each(changeType, function () {
				    $(this).on('click', function () {
					$(".keynoteList").slideUp(100);
					      if (false == $(this).next().is(':hidden')) {
					        $('.keynoteList').slideUp(100);
					      }
				      $(this).next().slideToggle(100);
				    });
				})
				
				setMeunPosition('maxMenu');
			}else{
				layer.msg(systemAbnormalityMsg, {time: 2000});
			}
		
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	});
}


/*拼接重点单位html*/
function getUnitKuanHtml(obj,j){
	var html="<li>";
	html+="<div class='keynoteHd ellipsis'><span>"+obj.fireBrigadeName+"</span><span class='keynoteHdNub'>"+(obj.keyUnitCount!=undefined? obj.keyUnitCount :0)+"</span>";
	html+="<div class='keynoteMore'></div>";
	html+="</div>";
	if(j==0){
		html+="<div class='keynoteList'>";
	}else{
		html+="<div style='display:none;' class='keynoteList'>";
	}
				if(obj.keyUnitList&&obj.keyUnitList.length>0){
		for(var i=0;i<obj.keyUnitList.length;i++){
			var curr=obj.keyUnitList[i];
			curr.type='zddw';
			html+="<div class='ktWarp'>";
			html+="<div class='ca cf mr20' onclick='gotoPointLocation("+ JSON.stringify(curr)+")'>";
			html+="<span class='ktAddress fl'></span>";
			html+="<span class='ktW180 ellipsis fl'>"+curr.name+"</span>";
			if(curr.distances){
			     html+="<span class='fl' style='color: #ff731e;font-weight: bolder;'>("+curr.distances+"千米)</span>";
			    }
			html+="</div>";
			html+="<span class='ktArrow' onclick='unitDetail("+ JSON.stringify(curr.id) +")'></span>";
			html+="</div>";
		}
	}else{
		html+="<div class='NoData'>暂无数据</div>";
		}
	html+="</div>";
	html+="</li>";
	return html;
}

//切换消防水源得宽 、窄页面
function waterContentZhaiDiv(){
	$("#waterContentKuanDiv").hide();
	$("#waterContentZhaiDiv").show();
	$("#waterContent").css("width","260px");
	//subMeusDivZhai();
	setMeunPosition();

}

//切换消防水源得宽 、窄页面
function waterContentKuanDiv(){
	pageNum=1;	
	$("#waterContentZhaiDiv").hide();
	$("#waterContentKuanDiv").show();
	$("#waterContent").css("width","560px");
	getWaterContentAjax();
	//subMeusDivKuan();
	setMeunPosition('maxMenu');
}
//切换消防水源得宽 、窄页面
function getWaterContentAjax(){
	var html="";
	$("#soureManageList2").html("");
	$("#fireWaterSourcePage2").html("");
	var soureType=$("#soureTypeId2").val();//1是消防水源 2是消防设备
	var keytype=$("#soureKeyTypeId2").val();
	var soureKeyUnitId=$("#soureKeyUnitId2").val();
	var soureInputContent =$("#soureManageIn2").val();
	
	var params={"name":soureInputContent,"keyUnitId":soureKeyUnitId,"type":keytype,"soureType":soureType,"pageNum":pageNum,"pageSize":pageSize};
	
	$.ajax({
		type : "post",
		url : baseUrl+"/webApi/qryFireWaterSourceGroupKeyUnit.jhtml",
		data: JSON.stringify(params),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				if(data.data&&data.data.length>0){
					var currentPage = Number(data.current);
					var totalPages = Number(data.pages);
					var features = [];
					if(currentPage>=1 && totalPages>1){
						generatePagination2(currentPage,totalPages,"fireWaterSourcePage2");
					}
					for(j = 0,len=data.data.length; j < len; j++) {
						html+=getWaterContentKuanHtml(data.data[j],soureType,j);
					}
				}else{
					html+="<li class='NoTime'>暂无数据</li>";
				}
				
				$("#soureManageList2").append(html);
				/*消防水源信息事件*/
				  var changeType = $('#soureManageList2').find('.SourceWaterHd');
				  $.each(changeType, function () {
				    $(this).on('click', function () {
						$(".SourceWaterDiv").slideUp(100);
					      if (false == $(this).next().is(':hidden')) {
					        $('.SourceWaterDiv').slideUp(100);
					      }
				        $(this).next().slideToggle(100);
				    });
				  })
			}else{
				layer.msg(systemAbnormalityMsg, {time: 2000});
			}
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	});
	
}
//消防水源拼接html 宽页面的
function getWaterContentKuanHtml(obj,type,j){	
	var html='<li>';
	html+='<div class="SourceWaterHd ellipsis">';
	html+='<div class="SourceWaterNew ca cf">';
	html+='<div class="fl"><span class="SourceWaterNumber"><i></i></span>'+obj.keyUnitName+'</div>';
	html+='<span class="SourceWaterAmount fl">'+obj.fireWaterSourceCount+'</span>';
	html+='</div>';
	html+='<div class="SourceWaterMroe"><img src="'+baseUrl+'/images/fyMoreIcon.png" alt="" /></div>';
	html+='</div>';

		if(j==0){
			html+="<div class='SourceWaterDiv'>";
		}else{
			html+="<div style='display:none;' class='SourceWaterDiv'>";
		}
		if(obj.fireWaterSourceList&&obj.fireWaterSourceList.length>0){
		html+='<table cellpadding="0" cellspacing="0" border="0" width="100%" class="SourceWaterDivTab">';
		html+='<thead>';
		html+='<tr>';
		html+='<th align="left">名称</th>';
		html+='<th>类型</th>';
		html+='<th>数量</th>';
		html+='<th>参数</th>';
		html+='<th>是否好用</th>';
		if(type==1){
			html+='<th>电话</th>';
			html+='<th>管辖单位</th>';
		}
		html+='</tr>';
		html+='</thead>';
		html+='<tbody>';	
			for(var i=0;i<obj.fireWaterSourceList.length;i++){
				var curr=obj.fireWaterSourceList[i];
				if(type==1){
					curr.type='xfs';
				}else{
					curr.type='xfsy';
				}
				html+='<tr>';
				html+="<td onclick='gotoPointLocation("+ JSON.stringify(curr)+")'><div class='ca cf wtWarp'><span class='wtAddress fl'></span><span class='wtW180 ellipsis fl'>"+curr.name+"</span></div></td>";
				html+='<td>'+curr.typeName+'</td>';
				if(curr.reserves){
					html+='<td>'+curr.reserves+'</td>';
				}else{
					html+='<td>无</td>';
				}
				if(curr.flow){
					html+='<td>'+curr.flow+'</td>';
				}else{
					html+='<td>无</td>';
				}
				if(curr.goodUse){
					html+='<td>好用</td>';
				}else{
					html+='<td>不好用</td>';
				}
				if(type==1){
					if(curr.phone && curr.phone != ""){
						html+='<td>'+curr.phone+'</td>';
					}else{
						html+='<td>无</td>';
					}
					if(curr.jurisdictionUnit && curr.jurisdictionUnit != ""){
						html+='<td>'+curr.jurisdictionUnit+'</td>';
					}else{
						html+='<td>无</td>';
					}
				}
				html+='</tr>';
			}
			html+='</tbody>';
			html+='</table>';
			html+='</div>';

		}else{
				html+="<div class='NoData'>暂无数据</div>";
			}
			html+='</li>';
		return html;
}




//切换消防车得宽 、窄页面
function showCarInfoZhai(){
	$("#carInfoKuanDiv").hide();
	$("#carInfoZhaiDiv").show();
	$("#carInfoContent").css("width","260px");
	//subMeusDivZhai();
	setMeunPosition();

}

//切换消防车得宽 、窄页面
function showCarInfoKuan(){
	pageNum=1;	
	$("#carInfoZhaiDiv").hide();
	$("#carInfoKuanDiv").show();
	$("#carInfoContent").css("width","560px");
	getCarInfoAjax();
	//subMeusDivKuan();
	setMeunPosition('maxMenu');
}

//切换消防车得宽 、窄页面
function getCarInfoAjax(){
	var html="";
	$("#emergencyEquipment2").html("");
	$("#emergencyEquipmentPage2").html("");
	var cardTypeId=$("#cardTypeId2").val();
	var keyUnitId=$("#emergencyEquipKeyUnitId2").val();
	var keyWord=$("#emergencyEquipmentIn2").val();
	var params={"name":keyWord,"cardTypeId":cardTypeId,"fireBrigadeId":keyUnitId,"pageNum":pageNum,"pageSize":pageSize};
    $.ajax({
		type : "post",
		url : baseUrl+"/webApi/qryFireEngineByFireBridge.jhtml",
		data: JSON.stringify(params),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.data&&data.data.length>0){
				var currentPage = Number(data.current);
				var totalPages = Number(data.pages);
				var features = [];
				if(currentPage>=1 && totalPages>1){
					generatePagination2(currentPage,totalPages,"emergencyEquipmentPage2");
				}
				if(data.data){
					for(j = 0,len=data.data.length; j < len; j++) {
						html+=getEmergencyEquipmentKuanHtml(data.data[j],j);
					}
				}
			}else{
				html+="<li class='NoTime'>暂无数据</li>";
			}
			$("#emergencyEquipment2").append(html);
			/*消防车辆信息事件*/
			  var changeType = $('#emergencyEquipment2').find('.fireCompany');
			  $.each(changeType, function () {
			    $(this).on('click', function () {
					$(".fireCompanyMsg").slideUp(100);
				      if (false == $(this).next().is(':hidden')) {
				        $('.fireCompanyMsg').slideUp(100);
				      }
			        $(this).next().slideToggle(100);
			    });
			  })
			setMeunPosition('maxMenu');
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	});
}

//消防车辆拼接html 宽页面的
function getEmergencyEquipmentKuanHtml(obj,j){
	obj.type='yjzb';
    var html="<li>";
		html+="<div class='fireCompany'>";
		html+="<table cellpadding='0' cellspacing='0' border='0' width='100%' class='fireCompanyTab'>";
		html+="<tbody>";
		html+="<tr>";
		html+="<td class='fyWidth100'><i></i><span class='fyHead'>"+obj.fireBrigadeName+"</span>";
		html+="<span class='fyLevel'>"+obj.stationLevel+"</span></td>";
		html+="<td align='center'><span>消防车</span><span class='fyBlod'>"+(obj.fireEngineCount?obj.fireEngineCount:0)+"</span></td>";
		html+="<td align='center'><span>水</span><span class='fyBlod'>"+(obj.waterCount?obj.waterCount:0)+"</span></td>";
		html+="<td align='center'><span>泡沫</span><span class='fyBlod'>"+(obj.powderCount?obj.powderCount:0)+"</span></td>";
		html+="<td align='center'><span>干粉</span><span class='fyBlod'>"+(obj.foamCount?obj.foamCount:0)+"</span></td>";
		html+="<td align='center'><img src='"+baseUrl+"/images/fyMoreIcon.png' alt=''/></td>";
		html+="</tr>";
		html+="</tbody>";
		html+="</table>";
		html+="</div>";
		if(j==0){
			html+="<div class='fireCompanyMsg'>";
		}else{
			html+="<div style='display:none;' class='fireCompanyMsg'>";
		}
		if(obj.fireEngineList&&obj.fireEngineList.length>0){
		html+="<table cellpadding='0' cellspacing='0' border='0' width='100%' class='carTableMsg'>";
		html+="<thead>";
		html+="<tr>";
		html+="<th align='center'>序号</th>";
		html+="<th>车辆名称</th>";
		html+="<th>车辆牌照号</th>";			
		html+="<th align='center'>状态</th>";
		html+="<th align='center'>水</th>";
		html+="<th align='center'>泡沫</th>";
		html+="<th align='center'>干粉</th>";
		html+="<th></th>";
		html+="</tr>";
		html+="</thead>";
		html+="<tbody>";
			for(var i=0;i<obj.fireEngineList.length;i++){
				var curr =obj.fireEngineList[i];
				html+="<tr>";
				html+="<td align='center'>"+(i+1)+"</td>";
				html+="<td class='ctWidth80'>"+curr.name+"</td>";
				html+="<td class='ctWidth80'>"+curr.plateNumber+"</td>";
				if(curr.stateOfDuty==1){
					html+="<td align='center'><span class='dutySpare'>备勤</span></td>";
				}else{
					html+="<td align='center'><span class='dutyOn'>执勤</span></td>";
				}
				html+="<td align='center'>"+curr.water+"</td>";
				html+="<td align='center'>"+(curr.foamTypeNameO&&curr.foamTypeNameO!=""?curr.foamTypeNameO+"<br>"+curr.powderQuantity:curr.powderQuantity)+"</td>";
				html+="<td align='center'>"+curr.capacity+"</td>";
				html+="<td><div onclick='vehicleEquipment(\""+curr.id+"\")'  class='fyIcon'></div></td>";
				html+="</tr>";
			}
		html+="</tbody>";
		html+="</table>";
		html+="</div>";
		}else{
			html+="<div class='NoData'>暂无数据</div>";
		}
		html+="</li>";
	return html;
}

//分页
function generatePagination2(cp,tp,keyId){
	this.currentPage = Number(cp);
	this.totalPages = Number(tp);
	var jump_prev=currentPage>1?(currentPage-1):currentPage;
	var jump_next=(currentPage<totalPages)?(currentPage+1):totalPages;
	var pageList ="<a name='findPage' pageNum='"+jump_prev+"' class='front'><span></span></a>";
	var page_start = Number(currentPage)/5;
	if(page_start>1){
		pageList = getPageHtml(page_start*5-2,page_start*5+3,totalPages,pageList);
	}else{
		pageList = getPageHtml(1,6,totalPages,pageList);
	}
	pageList +="<a name='findPage' pageNum='"+jump_next+"' class='after'><span></span></a>";
	$("#"+keyId).html(pageList);
	$("#"+keyId).find("[name=findPage]").on("click",function(){
	   pageNum=$(this).attr("pageNum");  //获取链接里的页码  	
	   if(keyId=="emergencyEquipmentPage2"){
		   getCarInfoAjax();
	   }else if(keyId=="keyPartsPage2"){
		   getLocationContentAjax();
	   }else if(keyId=="plansPage2"){
		   getReservePlanAjax();
	   }else if(keyId=="fireWaterSourcePage2"){
		   getWaterContentAjax();
	   }
	}); 
}

//切换应急专家得宽 、窄页面
function showExpertZhai(){
	$("#expertDivKuan").hide();
	$("#expertDivZhai").show();
	$("#expertContent").css("width","260px");
	//subMeusDivZhai();
	setMeunPosition();
}

//切换应急专家得宽 、窄页面
function showExpertKuan(){
	pageNum=1;	
	$("#expertDivZhai").hide();
	$("#expertDivKuan").show();
	$("#expertContent").css("width","560px");
	getExpertAjax();
	//subMeusDivKuan();
	setMeunPosition('maxMenu');
}

//切换应急专家得宽 、窄页面
function getExpertAjax() {
	var html="";
	$("#specialistData2").html("");
	$("#expertPage2").html("");
	var keyWord=$("#specialistDataInput2").val();	
	var typeId=$("#expertTypeId2").val();	
	var params={"name":keyWord,"typeId":typeId,/*"pageNum":pageNum,"pageSize":pageSize*/};
    $.ajax({
		type : "post",
		url : baseUrl+"/webApi/qryExpertList.jhtml",
		data: JSON.stringify(params),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {					
//			var currentPage = Number(data.current);
//			var totalPages = Number(data.pages);
//			if(currentPage>=1 && totalPages>1){
//				generatePagination(currentPage,totalPages,"expertPage");
//			}
			if(data.data&&data.data.length>0){
				for(j = 0,len=data.data.length; j < len; j++) {
					html+=getSpecialistKuanHtml(data.data[j],j);
				}
			}else{
				html+="<li class='NoTime'>暂无数据</li>";
			}
			
			$("#specialistData2").append(html);	

			//注册展开收起事件
			/*$("#specialistData").find("li div.openMore").click(function(){
				$(this).toggleClass("takeupMore");
				$(this).siblings(".listFrom").toggle("fast");
			});*/
			/*消防车辆信息事件*/
			    var changeType = $('#specialistData2').find('.agencyHd');
				  $.each(changeType, function () {
				    $(this).on('click', function () {
						$(".expertDiv").slideUp(100);
						      if (false == $(this).next().is(':hidden')) {
						        $('.expertDiv').slideUp(100);
						      }
				      $(this).next().slideToggle(100);
				    });
				})
			setMeunPosition('maxMenu');
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	})
	
}
//应急专家拼接html 宽页面的
function getSpecialistKuanHtml(obj,j){
	var index =j+1;
	var html="<li onClick='getSpecialistDetail("+ JSON.stringify(obj)+")'>";
	html+="<div class='agencyHd ellipsis'>";
	html+="<div class='expertNew ca cf'>";
	html+="<div class='fl'><span class='expertNumber'><i>"+index+"</i></span>"+obj.name+"</div>";
	html+="<span class='expertAmount fl'>"+obj.expertCout+"</span>";
	html+="</div>";
	html+="<div class='expertMroe'><img src='"+baseUrl+"/images/fyMoreIcon.png' alt='' /></div>";
	html+="</div>";
	if(j==0){
		html+="<div class='expertDiv'>";
	}else{
		html+="<div style='display:none;' class='expertDiv'>";
	}
	if(obj.expertList){
		html+="<table cellpadding='0' cellspacing='0' border='0' width='100%' class='expertDivTab'>";
		html+="<thead>";
	    html+="<tr>";
		html+="<th>专家姓名</th>";
		html+="<th>性别</th>";
		html+="<th>出生日期</th>";
		html+="<th>所属单位</th>";
		html+="<th>专业</th>";
		html+="<th>家庭住址</th>";
		html+="</tr>";
		html+="</thead>";
		html+="<tbody>";
		for(var i=0;i<obj.expertList.length;i++){
			var curr =obj.expertList[i];
			html+="<tr>";
			html+="<td>"+curr.name+"</td>";
			html+="<td>"+(curr.sex?(curr.sex=="1"?"男":"女"):"")+"</td>";
			html+="<td style='width:80px;'>"+(curr.birth?new Date(curr.birth).Format("yyyy-MM-dd"):"")+"</td>";
			html+="<td>"+curr.unit+"</td>";
			html+="<td>"+curr.major+"</td>";
			html+="<td>"+(curr.address?curr.address:"")+"</td>";
			html+="</tr>";
		}
		html+="</tbody>";
		html+="</table>";
	}else{
		html+="<div class='NoData'>暂无数据</div>";
	}
	html+="</div>";
	html+="</li>";
	return html;
}

//切换应急预案得宽 、窄页 面
function showReservePlanZhai(){
	$("#ReservePlanDivKuan").hide();
	$("#ReservePlanDivZhai").show();
	$("#reservePlanContent").css("width","260px");
	//subMeusDivZhai();
	setMeunPosition();
}

//切换应急预案得宽 、窄页面
function showReservePlanKuan(){
	pageNum=1;	
	$("#ReservePlanDivZhai").hide();
	$("#ReservePlanDivKuan").show();
	$("#reservePlanContent").css("width","560px");
	getReservePlanAjax();
	//subMeusDivKuan();	
	setMeunPosition('maxMenu');
}

//切换应急预案得宽 、窄页面
function getReservePlanAjax() {
	var html="";
	$("#reservePlanList2").html("");
	$("#planTypeId2").html("");
	var keyWord=$("#reservePlanInput2").val();	
	var typeId=$("#planTypeId2").val();	
	var params={"name":keyWord,"typeId":typeId,"pageNum":pageNum,"pageSize":pageSize};
	//var params={"name":keyWord,"typeId":typeId};
    $.ajax({
		type : "post",
		url : baseUrl+"/webApi/qryPlans.jhtml",
		data: JSON.stringify(params),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {
			if(data.httpCode==200){
				if(data.data&&data.data.length>0){
					var currentPage = Number(data.current);
					var totalPages = Number(data.pages);
					if(currentPage>=1 && totalPages>1){
						generatePagination2(currentPage,totalPages,"plansPage2");
					}
					if(data.data){
						for(j = 0,len=data.data.length; j < len; j++) {
							html+=getReservePlanKuanHtml(data.data[j],j);
						}
					}
				}else{
					html+="<li class='NoTime'>暂无数据</li>";
				}
				$("#reservePlanList2").append(html);	
				 var changeType = $('#reservePlanList2').find('.reserveHd');
					  $.each(changeType, function () {
					    $(this).on('click', function () {
						$(".reserveDiv").slideUp(100);
						      if (false == $(this).next().is(':hidden')) {
						        $('.reserveDiv').slideUp(100);
						      }
						
					      $(this).next().slideToggle(100);
					    });
				 })
					
				setMeunPosition();
			}else{
				layer.msg(systemAbnormalityMsg, {time: 2000});
			}
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	})
}
	
	/*应急预案拼接html*/
	function getReservePlanKuanHtml(obj,j){	
			var html='<li>';
			html+='<div class="reserveHd ellipsis">';
			html+='<div class="reserveNew ca cf">';
			html+='<div class="fl"><span class="reserveNumber"><i></i></span>'+obj.name+'</div>';
			html+='<span class="reserveAmount fl">'+obj.planCount+'</span>';
			html+='</div>';
			html+='<div class="reserveMroe"><img src="'+baseUrl+'/images/fyMoreIcon.png" alt="" /></div>';
			html+='</div>';
			if(obj.planList){
				if(j==0){
					html+="<div class='reserveDiv'>";
				}else{
					html+="<div style='display:none;' class='reserveDiv'>";
				}
				html+='<table cellpadding="0" cellspacing="0" border="0" width="100%" class="reserveDivTab">';
				html+='<thead>';
				html+='<tr>';
				html+='<th align="left">预案名称</th>';
				html+='<th>预案编号</th>';
				html+='<th>编写人</th>';
				html+='<th>编写时间</th>';
				html+='<th>预案类型</th>';
				html+='<th>操作</th>';
				html+='</tr>';
				html+='</thead>';
				html+='<tbody>';
					for(var i=0;i<obj.planList.length;i++){
						var curr =obj.planList[i];
						html+='<tr>';
						html+='<td>'+curr.name+'</td>';
						html+='<td>'+curr.code+'</td>';
						html+='<td>'+curr.writer+'</td>';
						html+='<td>'+(curr.writingTime?new Date(curr.writingTime).Format("yyyy-MM-dd"):"")+'</td>';
						html+='<td>'+curr.typeName+'</td>';
						html+='<td>';
						if(curr.swfPath){
							html+='<div class="prepress ca cf">';
							html+='<a href="javascript:void(0)" onClick="showReservePlandoc(\''+curr.id+'\')"><i class="eye"></i>查看</a>';
							html+='<a href="javascript:void(0)" onclick="downLoadFile(\''+curr.path+'\',\''+curr.fileName+'\')" ><i class="load"></i>下载</a>';
							html+='</div>';
						}
						html+='</td>';
						html+='</tr>';
					}
				
				html+='</tbody>';
				html+='</table>';
				html+='</div>';
			}else{
		             html+="<div class='NoData'>暂无数据</div>";		
				}
			html+='</li>';
			return html;
	}	
	
	//切换重点部位得宽 、窄页面
	function showLocationContentZhai(){
		keyPartsType=1;
		$("#locationContentKuan").hide();
		$("#locationContentZhai").show();
		$("#locationContent").css("width","260px");
		//subMeusDivZhai();
		setMeunPosition();
	}

	//切换重点部位得宽 、窄页面
	function showLocationContentKuan(){
		pageNum=1;	
		keyPartsType=2;
		$("#locationContentZhai").hide();
		$("#locationContentKuan").show();
		$("#locationContent").css("width","560px");
		getLocationContentAjax();
		//subMeusDivKuan();
		setMeunPosition('maxMenu');
	}	
	
	//切换重点部位得宽 、窄页面
	function getLocationContentAjax() {
		$("#keyPartsList2").html("");	
		var html="";
		var keyUnitId=$("#selectKeyPartsOfUnit2").val();
		var keyPartsKeyword =$("#keyPartsKeyword2").val();
		var params={"keyUnitId":keyUnitId,"name":keyPartsKeyword,"pageNum":pageNum,"pageSize":pageSize};
		var url = baseUrl+"/webApi/qryKeyPartsGroupUnit.jhtml";
	    $.ajax({
			type : "post",
			url :url,
			data: JSON.stringify(params),
			dataType:"json",
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {
					if(data.httpCode==200){
			    		if(data){
			    			if(data.data&&data.data.length>0){
					    			var currentPage = Number(data.current);
									var totalPages = Number(data.pages);
									if(currentPage>=1 && totalPages>1){
										generatePagination2(currentPage,totalPages,"keyPartsPage2");
									}
									if(data.data.length>0){
										for(j = 0,len=data.data.length; j < len; j++) {
											html+=getKeyPartsKuanHtml(data.data[j],j);
										}
									}
						}else{
							html+="<li class='NoTime'>暂无数据</li>";
						}
				
						$("#keyPartsList2").append(html);	
						//注册展开收起事件
						$("#keyPartsList2").find("li div.openMsg").click(function(e){
							$(this).toggleClass("upMsg");
							$(this).siblings(".listFrom").toggle("fast");
							stopBubble(e); 
						});

						//li展开收起
						var changeType = $('#keyPartsList2').find('.keynoteHd');
						  $.each(changeType, function () {
						    $(this).on('click', function () {
								$(".keynoteList").slideUp(100);
								      if (false == $(this).next().is(':hidden')) {
								        $('.keynoteList').slideUp(100);
								      }
						      $(this).next().slideToggle(100);
						    });
						})
					}
		    	}else {
		    		layer.msg(data.msg, {time: 1000});
		    	}
				setMeunPosition('maxMenu');
			},error: function(request) {
				layer.msg(networkErrorMsg, {time: 2000});
            }
		})
	}
		/*重点部位拼接html*/
		function getKeyPartsKuanHtml(obj){	
			var html="<li>";
			html+="<div class='keynoteHd ellipsis'>";
			html+="<span>"+obj.name+"</span>";
			html+="<span class='keynoteHdNub'>"+obj.keyPartsCount+"</span>";
			html+="<div class='keynoteMore'></div>";
			html+="</div>";
			if(obj.keyPartsList){
				if(j==0){
					html+="<div class='keynoteList'>";
				}else{
					html+="<div style='display:none;' class='keynoteList'>";
				}
				for(var i=0;i<obj.keyPartsList.length;i++){
					var curr =obj.keyPartsList[i];
					html+="<div class='ktWarp'>";
					html+="<div class='ca cf mr20'>";
					html+="<span class='ktAddress fl'></span>";
					html+="<span class='ktW180 ellipsis fl'>"+curr.name+"</span>";
					html+="</div>";
					html+="<span class='ktArrow' onClick='keyPartsDetail("+ JSON.stringify(obj.id) +")'></span>";
					html+="</div>";
				}
			}
			html+="</li>";
		return html;
		}

//关闭面板
function closePanel(oid){
	$("#"+oid).hide();
}

function closeCreateAccidentPanel(id){
	$("#"+id).hide();
	$('#selectZDDW').hide();
}

//关闭数据同步面板
function closePanelDate(oid){
	$("#"+oid).hide();
	$("#synchroBgDiv").hide();
}

//会商会标宽窄切换事件调整
/*function subMeusDivKuan(){
	$("#subMeusDiv").css("margin-left","670px");
	$("#fxbss").css("left","690px");
	$("#fireDosageDiv").css("top","177px");
	$("#fireDosageDiv").css("left","929px");
	
	if($(".subMeus").is(":hidden")){
		$("#fireDosageDiv").css("top","121px");
		$("#fireDosageDiv").css("left","690px");
	}else{
		$("#fireDosageDiv").css("left","690px");
		
	}
}
function subMeusDivZhai(){
	$("#subMeusDiv").css("margin-left","370px")
	//$("#fxbss").css("left","390px");
	$("#fxbss").css("top","26px");
	//$("#fireDosageDiv").css("margin-left","135px");
	$("#fireDosageDiv").css("top","75px");
	$("#fireDosageDiv").css("left","1100px");
	if($(".subMeus").is(":hidden")){
		
		$("#fireDosageDiv").css("left","755px");
	}else{
		$("#fireDosageDiv").css("top","125px");
		$("#fireDosageDiv").css("left","761px");
		$("#fxbss").css("top","75px");
		
	}
}*/


/**
 * 此方法主要处理三个对象 ：
 * 	TOP会商会标 subMeusDiv
 *	重点单位搜索 fxbss
 * 	消防用水量 fireDosageDiv
 * 分为以下情况
 * 如果subMeusDiv收起 && 左侧收起 fxbss fireDosageDiv weatherMsgModelDiv并排
 */
function setMeunPosition(menu){
	var subMeusDiv = $('#subMeusDiv');
	var fxbss = $('#fxbss');
	var zhmnSmallIcon = $("#zhmnSmallIcon");
	var fireDosageDiv = $('#fireDosageDiv');
	var weatherMsgModelDiv = $('#weatherMsgModelDiv');
	//查看左侧是收起还是展开 max是最大 min是最小 false是收起
	var getLeft = function(){
		var b = false;
		if($(".content:visible").length==0){
			b = false;
		}
		else{
			b = {};
			if($(".content:visible").width()>=560)
			{
				b.max = true;
			}else{
				b.min = true;
			}
		}
		return b;
	}
	//得到会商会标菜单是否展开 展开是true
	var getMenu = function(){
		var b = false;
		if($(".subMeus").is(":hidden")){}else{
			b = true;
		}
		return b;
	}
	//菜单 Max Middle Min
	var setsubMeusDiv = function(type){
		
		if(menu=="maxMenu"){
			type = 'Min';
		}
		switch(type){
			case "Max":
				subMeusDiv.css("margin-left","90px");
				break;
			case "Middle":
				subMeusDiv.css("margin-left","370px");
				break;
			case "Min":
				subMeusDiv.css("margin-left","670px");
				break;
		}
	}
	//重点单位搜索 消防用水量 是一套
	//六种情况 NoLeft_NoTop MinLeft_NoTop MaxLeft_NoTop
	//NoLeft_Top MinLeft_Top MaxLeft_Top
	var setfxbss = function(type){
		var _setfxbss={
			//左侧和顶都收起
			NoLeft_NoTop:function(){
				fxbss.css("top","25px");
				fxbss.css("left","110px");
				fireDosageDiv.css("top","75px");
				fireDosageDiv.css("left","32%");
				weatherMsgModelDiv.css("left","110px");
				weatherMsgModelDiv.css("top","75px");
				zhmnSmallIcon.css("left","110px");
			},
			//左侧小和顶收起
			MinLeft_NoTop:function(){
				fxbss.css("top","25px");
				fxbss.css("left","393px");
				fireDosageDiv.css("top","75px");
				fireDosageDiv.css("left","755px");
				weatherMsgModelDiv.css("left","393px");
				weatherMsgModelDiv.css("top","75px");
				zhmnSmallIcon.css("left","393px");
			},
			//左侧最大和顶收起
			MaxLeft_NoTop:function(){
				fxbss.css("top","25px");
				fxbss.css("left","690px");
				fireDosageDiv.css("top","122px");
				fireDosageDiv.css("left","690px");
				weatherMsgModelDiv.css("left","690px");
				weatherMsgModelDiv.css("top","122px");
				zhmnSmallIcon.css("left","690px");
			},
			//左侧收起和顶展开
			NoLeft_Top:function(){
				fxbss.css("top","80px");
				fxbss.css("left","110px");
				fireDosageDiv.css("top","130px");
				fireDosageDiv.css("left","32%");
				weatherMsgModelDiv.css("left","110px");
				weatherMsgModelDiv.css("top","130px");
				zhmnSmallIcon.css("left","110px");
			},
			//左侧小和顶展开
			MinLeft_Top:function(){
				fxbss.css("top","80px");
				fxbss.css("left","393px");
				fireDosageDiv.css("top","130px");
				fireDosageDiv.css("left","755px");
				weatherMsgModelDiv.css("left","393px");
				weatherMsgModelDiv.css("top","130px");
				zhmnSmallIcon.css("left","393px");
			},
			//左侧最大和顶展开
			MaxLeft_Top:function(){
				fxbss.css("top","80px");
				fxbss.css("left","690px");
				fireDosageDiv.css("top","177px");
				fireDosageDiv.css("left","690px");
				weatherMsgModelDiv.css("left","690px");
				weatherMsgModelDiv.css("top","177px");
				zhmnSmallIcon.css("left","690px");
			}
		}
		return _setfxbss;
	}
	if(menu=="maxMenu"){
		setsubMeusDiv('Min');
	}
	var _setMeunPosition = function(){
		if(!getLeft() && !getMenu()){
//			console.log("左侧 会商会标都没有")
			setsubMeusDiv('Max');
			setfxbss().NoLeft_NoTop();
		}
		if(getLeft().max && !getMenu()){
//			console.log("左侧最长 会商会标没有")
			setsubMeusDiv('Min');
			setfxbss().MaxLeft_NoTop();
		}
		if(getLeft().min && !getMenu()){
//			console.log("左侧最短 会商会标没有")
			setsubMeusDiv('Middle');
			setfxbss().MinLeft_NoTop();
		}
		if(!getLeft() && getMenu()){
//			console.log("左侧没有 会商会标有")
			setsubMeusDiv('Max');
			setfxbss().NoLeft_Top();
		}
		if(getLeft().max && getMenu()){
//			console.log("左侧最长 会商会标有")
			setsubMeusDiv('Min');
			setfxbss().MaxLeft_Top();
		}
		if(getLeft().min && getMenu()){
//			console.log("左侧最短 会商会标有")
			setsubMeusDiv('Middle');
			setfxbss().MinLeft_Top();
		}
	}
	setTimeout(function(){
		_setMeunPosition();
	},200)
}


/**  
/**
* 保存操作记录的公共方法
* @param params t_incident_record 对象 操作人director 类型type 房间号roomId 子动作action 动作描述actionDesc 动作内容content
* @param callback 回调参数
* */
function saveRecord(params,callback){
	doCallback = function (fn, args) {
		return fn.apply(this, args);
	}
	if(!params.type){
		params.type=params.recordType;
	}
	var url = baseUrl+'/webApi/insertIncidentRecord.jhtml';
	$.ajax({
		type:'POST',
		url: url,
		ladeview:false,
		headers: {"Content-Type": "application/json;charset=utf-8"},
		data: JSON.stringify(params),
		dataType:'JSON',
		success: function(data){
			if(data){
				if(callback){
				  doCallback(callback,[data]);
				}
			}
		}
	});
}
function updateRecord(params,callback){
	doCallback = function (fn, args) {
		return fn.apply(this, args);
	}
	params.type=params.recordType;
	var url = baseUrl+'/webApi/updateIncidentRecord.jhtml';
	$.ajax({
		type:'POST',
		url: url,
		headers: {"Content-Type": "application/json;charset=utf-8"},
		data: JSON.stringify(params),
		dataType:'JSON',
		success: function(data){
			if(data){
				if(callback){
					doCallback(callback,[data]);
				}
			}
		}
	});
}

/*
 * 历史数据、实时数据展示
 */

//全部隐藏
function hideRightDetatil(){
	$("#alarmOpenMove").css('right','-1px');
	$("#alarm").removeClass("ZindexCss");
	$("#alarmOpenMove").removeClass("alarmOpenCollect");
	$("#policeBJ").fadeOut();
	$("#alarm").fadeOut();
	$("#tuwenshujuLi").removeClass("onlike");
	$("#hitoryDataLi").removeClass("onlike");
	$("#realtimeDataLi").addClass("onlike")
	$("#toolMove").css('right','30px');
	$("#toolBoxBTN").css('right','40px')
	$("#iconBox").css('right','93px');
	$("#starsDiv").css('right','10px');
	$("#ysslDiv").css('right','45px');
	//关闭小视频
	closeMiniCamera();
	
}
function RightShowOrClose(){
	var rightWidth = $("#alarmOpenMove").attr('class');
	if(rightWidth.indexOf("alarmOpenCollect")!=-1){
		hideRightDetatil();
	}else{
		showRightDetail();
	}
}

// 显示右侧
function showRightDetail(){
	$("#alarmOpenMove").css('right','349px');
	$("#alarm").show();	
    $("#spreadDiv").show();	
    $("#alarm").addClass("ZindexCss");	
    $("#alarmOpenMove").addClass("alarmOpenCollect");
    $("#rightThreeBtn").each(function(){
	      $(this).find("li").first().addClass("onlike").siblings().removeClass("onlike");
	    });
    $("#dataTypeId").each(function(){
	      $(this).find("li").first().addClass("onlike").siblings().removeClass("onlike");
	    });
    $("#alarm").css('width','');
    $('#policeBJ').css('width','330px');
    $('#policeBJ').css('left',"");
	$("#historyDataDivId").hide();
	$("#picArchiveDataDivId").hide();
	$("#realTimeDataDivId").show();
	$("#policeBJ").fadeIn();
	$("#shrinkBtn").hide();
	$('#tuwenshujuLi').show();
	$("#rightThreeBtn").show();
	$("#allScreenTwoBtn").hide();
	$("#toolMove").css('right','380px');
	$("#toolBoxBTN").css('right','390px')
	$("#iconBox").css('right','430px');
	$("#starsDiv").css('right','360px');
	$("#ysslDiv").css('right','395px');
	$("#deviceC").attr("class","facility").removeClass("Big");
}
//全屏展示
function showAllScreenDeatail(){
	$("#alarm").css('width','100%');
	$('#policeBJ').css('width','100%');
	$('#policeBJ').css('left','0');
	$("#shrinkBtn").show();
	$("#spreadDiv").hide();	
	$('#tuwenshujuLi').hide();
	$("#rightThreeBtn").hide();
	$("#allScreenTwoBtn").show();
	$("#allScreenTwoBtn").each(function(){
	      $(this).find("li").first().addClass("onlike").siblings().removeClass("onlike");
	    });
	$("#videoCloseId").hide();
	$("#deviceC").attr("class","Big").removeClass("facility");
}

/**
 * 显示和展开 最下面的图标工具
 */
function showdisasterWarp(){
	var state = $(this).attr("state");//1为展开 0为收起
	if(typeof(state)=="undefined"){state = 1;}//因为初始页面是收起 所以默认展开
	//得到所有li
	var li = $('#disasterOnclickUl li');
	//从中间向两边
	var half = parseInt(li.length/2);
	if(li.length%2!=0){
		half = parseInt(li.length/2)+1;
	}
	//正序list 和 反序list
	var list = [],rlist = [];
	//计数器
	var j=0;
	//延迟
	var sec = 120;
	//从中间进行排序
	for(var i=half;i>=0;i--){
	  j++
	  list.push(li[i]);
	  //console.log("第"+i+"个");
	  list.push(li[i+j]);
	  //console.log("第"+(i+j)+"个");
	  j++
	}
	//反序 显示用
	$.each(list,function(n,v){
	  rlist.push(list[((list.length-1)-n)]); 
	});
	//收起
	if(state=="0"){
		$("#zhmnToolBar").hide();
		$("#damageWarpDiv").show();
		$("#damageWarpDiv").attr("state","1");
	}
	//展开
	if(state=="1"){
		$("#zhmnToolBar").show();
		$("#damageWarpDiv").hide();
		$("#damageWarpDiv").attr("state","0");
		//重置计数器 该计数器控制 左右两个同时隐藏|显示
		var index1 = half;
		var index2 = half+1;
		   for(var num = 0;num<half;num++){
			   if(!(li.length%2!=0) && num==0){
				   $(li[index2-1]).show();
				   var width = (0-$('#zhmnToolBarOutDiv').width())/2;
				   $("#zhmnToolBarOutDiv").css({"marginLeft":width});
				   index2++;
			   }
			   (function(index1,index2,s,num){
				   setTimeout(function(){
					   if(index1-1>=0){
						   $(li[index1-1]).show();
					   }
					   if(index2<=li.length){
						   $(li[index2-1]).show();
					   }
					   var width = (0-$('#zhmnToolBarOutDiv').width())/2;
					   $("#zhmnToolBarOutDiv").css({"marginLeft":width});
				   },s);
			   })(index1--,index2++,sec+=120,num);
		   }
	}
}

function disasterMenuClose(){
	$('#damageWarpDiv').attr("state","1");
	$('#zhmnToolBar').hide();
	$("#disasterOnclickUl li").hide();
	$('#damageWarpDiv').show();
	$("#zhmnToolBarOutDiv").css({"marginLeft":""});
}

//退出
function logoutEsm(){
	var url =  baseUrl+"/webApi/logoutEsm.jhtml";
	var param = {};
	layer.confirm('是否确认退出', {
		btn: ['确定','取消'] //按钮
 	},function(){
		executeAjax(url,param,function(data){
			if(data.httpCode==200){
				window.location.href="login.jhtml";
			}
		});
 	}, function(){
	   layer.msg('已取消', {icon: 1, time: 1000});
 	});
}

/**
 * 
 * @returns
 */
function testIfram(){
	$("#myFrameNameTest").attr("src",baseUrl+"/webApi/report.jhtml");
    $("#myFrameNameTest").show(); 
}

/*
 * 获取当前时间
 * */
function getTime(){
	var myDate = new Date();
	//获取当前年
	var year=myDate.getFullYear();
	//获取当前月
	var month=myDate.getMonth()+1;
	//获取当前日
	var date=myDate.getDate(); 
	var h=myDate.getHours();       //获取当前小时数(0-23)
	var m=myDate.getMinutes();     //获取当前分钟数(0-59)
	var s=myDate.getSeconds();  
	return year+'-'+month+"-"+date+" "+h+':'+m+":"+s;
}

/*
 * 获取时间差
 * */
function getEndTime(sprayTime){
    var myDate = new Date();
    myDate.setTime(myDate.getTime() + sprayTime*1000);
    return DataLocaleString(myDate);
}

function DataLocaleString(data) {
    return data.getFullYear() + "-" + (data.getMonth() + 1) + "-" + data.getDate() + "-" + data.getHours() + ":" + data.getMinutes() + ":" + data.getSeconds();
}

/*
 * 秒转时分秒
 */
function getDateTimeT(time){
	//秒转毫秒
	var mss = time*1000;
	var days = parseInt(mss / (1000 * 60 * 60 * 24)); 
	if(days<10){
		days = "0"+days;
	}
	var hours = parseInt((mss % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)); 
	if(hours<10){
		hours = "0"+hours;
	}
	var minutes = parseInt((mss % (1000 * 60 * 60)) / (1000 * 60));
	if(minutes<10){
		minutes = "0"+minutes;
	}
	var seconds = (mss % (1000 * 60)) / 1000; 
	if(seconds<10){
		seconds = "0"+seconds;
	}
	return days + " 天 " + hours + " 小时 " + minutes + " 分钟 " + seconds + " 秒 ";
}

function timestampToStr(v){
    var now = new Date(v);
        var yy = now.getFullYear();      //年
        var mm = now.getMonth() + 1;     //月
        var dd = now.getDate();          //日
        var hh = now.getHours();         //时
        var ii = now.getMinutes();       //分
        var ss = now.getSeconds();       //秒
        var clock = yy + "-";
        if(mm < 10) clock += "0";
        clock += mm + "-";
        if(dd < 10) clock += "0";
        clock += dd + " ";
        if(hh < 10) clock += "0";
        clock += hh + ":";
        if (ii < 10) clock += '0'; 
        clock += ii + ":";
        if (ss < 10) clock += '0'; 
        clock += ss;
     return clock;
}

//设置EMS27事故点坐标
function setESM7Equment(postype){
	var params ={};
	params.postype = postype;
	params.clusterType='esm27q';
	if(postype==1){
		params.imagePath="/mapIcon/mapFixedIcon/hostIcon.png";
		params.clusterImagePath="/mapIcon/iconCluster/qtIcon_c.png";
	}else{
		params.imagePath="/mapIcon/mapFixedIcon/qtIcon.png";
		params.clusterImagePath="/mapIcon/iconCluster/qtIcon_c.png";
	}
	params.isCluster=true;
	params.isSave=true;
	params.catecd='esm27';
	params.recordType='esm27';
	params.action="ESM27设备";
	addMakerEngine(params);
	
}

//数据同步显示
function synchronizationDate(){
	$("#synchroBgDiv").show(); 
    $("#synchroDiv").show(); 
}

//显示事故点范围内的数据
function drawAccidetCommptyRange(point){
	var items = [{distance:5000,color:'#FFB5B5',distanceUnity:"5公里"},{distance:10000,color:'#FFDAC8',distanceUnity:"10公里"},{distance:15000,color:'#CEFFCE',distanceUnity:"15公里"}];
	accidentCompany.layerId="layerAcidentCommpty";
	var id = accidentCompany.layerId;
	var image = baseUrl+"/images/start.png";
	/**
	 * 爆炸接口显示
	 * @param id 主键
	 * @param point[x,y]
	 * @param items[{distance:1000,color:'#FF0000'},..]
	 * @param image 中心点的图片
	 * @param isFill 是否需要填充色
	 * @param showDistance 是否显示半径距离
	 * */
	addBzy(id,point,items,image,2,true,0);
}

function showAccidetCommptyRange(){
	if($("#isShowCommptyRange").prop("checked")){
		var lon = parseFloat($("#isShowCommptyRangeInputValue").attr("lon"));
		var lat = parseFloat($("#isShowCommptyRangeInputValue").attr("lat"));
		var point=[];
		point[0] = lon;
		point[1] = lat;
		drawAccidetCommptyRange(point);
	}else{
		clearBzy(accidentCompany.layerId);
	}
	
}

//判断只能输入纯数字或者小数点，及负整数
function clearNoNum(event, obj) {
    //响应鼠标事件，允许左右方向键移动
    event = window.event || event;
    if (event.keyCode == 37 | event.keyCode == 39) {
        return;
    }
   
    var t = obj.value.charAt(0);
    //先把非数字的都替换掉，除了数字和.
    obj.value = obj.value.replace(/[^\d.]/g, "");
    //必须保证第一个为数字而不是.
    obj.value = obj.value.replace(/^\./g, "");
    //保证只有出现一个.而没有多个.
    obj.value = obj.value.replace(/\.{2,}/g, ".");
    //保证.只出现一次，而不能出现两次以上
    obj.value = obj.value.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
    //如果第一位是负号，则允许添加   如果不允许添加负号 可以把这块注释掉
    if (t == '-') {
        obj.value = '-' + obj.value;
    }
	if(t=='0'){
    	if(obj.value.charAt(1)&&obj.value.charAt(1)!='.'){
    		var reg = new RegExp( '0' , "g" )
    		obj.value =obj.value.replace(reg,'');
    	}
    }
	if(obj.value.length > 8){
		obj.value = obj.value.slice(0,7);
	}
	if(obj.value > 30000){
		obj.value = obj.value.slice(0,obj.value.length-1);
		if(obj.value > 30000){
			obj.value = obj.value.slice(0,obj.value.length-1);
		}
	}
}

/**
 * 显示或隐藏采集和视频按钮
 */
function showCollectionAndVideoButton(visible){
	if(visible){
		$('#cameraSwitch').show();
		$('#flagSwitch').show();
	}else{
//		$('#cameraSwitch').hide();
		$('#flagSwitch').hide();
	}
}

//智能推荐危化品
function showZNTJWHPQT(){
	var html = '';
	var ajaxGasNames="";
	global_alertGrasZNTJ.forEach(function (element, index, array) {
		var names = element+',';
		ajaxGasNames += names;
	});
	if(ajaxGasNames!=""){
		var params = {"name":ajaxGasNames};
		$.ajax({
			type : "post",
			url : baseUrl+'/webApi/qryZhntjDangerChemic.jhtml',
			data: JSON.stringify(params),
			dataType:"json",
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {
				if(data.httpCode==200){
					for(var i=0;i<data.data.length;i++){
						var htmlObj = '<li onclick="viewDoc(\''+data.data[i].id+'\')"><div class="china ca cf"><i class="dgLbHd"></i>';
						htmlObj += '<div class="chinaName fl" style="width:100%;margin-left:0px;color:#fff;"><i class="eye"></i>'+data.data[i].cName+'('+data.data[i].molecularFormula+')</div>';
						htmlObj += '</div></li>';
						html += htmlObj;
					}
					$("#showZNTJWHPQT").html(html);
				}else{
					layer.msg(data.msg, {time: 2000});
				}
			},error: function(request) {
				layer.msg(networkErrorMsg, {time: 2000});
	        }
		});
	}
}

// 查询应急物资智能推荐列表
function getZhntjEmergencyMaterial(){
	var html = "";
	var keyWord = $("#zhtjMaterialKeyword").val();
	var params={"keyWord":keyWord,"disasterType":disasterType};	
	if(accidentCompany.lonlat4326){
		params.lon=accidentCompany.lonlat4326[0];
		params.lat=accidentCompany.lonlat4326[1];
	}
	$.ajax({
		type : "post",
		url : baseUrl+'/webApi/qryZhntjEmergencyMaterial.jhtml',
		data: JSON.stringify(params),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {
			if(data.httpCode==200){
				if(data.data && data.data.length>0){
					for(var i=0;i<data.data.length;i++){
						var item = data.data[i];
						html += "<li><div class='pumperDl'>"+item.materialName+"<em style='float: right;margin-right: 20px;color: #f9a204;'>共"+item.total+"件</em><span class='open arrow'></span></div>";
						html += '<div style="color:#fff;display:none"><ul>'
						if(item.reservePointList!=null&&item.reservePointList.length>0){
							for(var j=0;j<item.reservePointList.length;j++){
								var rsItem = item.reservePointList[j];
								rsItem.type='wzcbd';
								html += "<li onclick='gotoPointLocation("+ JSON.stringify(rsItem)+")'> <div class='listHdZNTJNew ca cf'>";
								html += '<div class="fl distanceNew"><i class="addressNew"></i><span class="dtNumber">'+rsItem.distances+'千米</span></div>'
								html += '<div class="fl listHdZNTJName"><span class="rsname">'+rsItem.name+'</span><span class="mi">('+rsItem.materialCount+')</span></div>'
								html += '</div></li>';
							}
						}
						html += '</ul></div></li>';
					}
				}
			}else{
				layer.msg(data.msg, {time: 2000});
			}
			$("#materialList").off('click').on('click','.pumperDl',function(){
				if($(this).next().is(":hidden")){
					$(this).next().show();
					$(this).find('span').attr("class","arrow");
				}else{
					$(this).next().hide();
					$(this).find('span').attr("class","open arrow");
				}
			});
			$("#materialList").html(html);
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	});
}

//通用显示Select
function showSelectDiv(divId){
	if($("#"+divId).is(":hidden")){
		$("#"+divId).show();    //如果元素为隐藏,则将它显现
	}else{
		$("#"+divId).hide();     //如果元素为显现,则将其隐藏
	} 
}

function selectOption(divId,value){
	$("#"+divId).hide(); 
	$("#unitSelectSPPZ li").siblings().removeClass("ZhoverCss");//默认选中当前选项	
	$("#"+divId+value).addClass("ZhoverCss");
}
