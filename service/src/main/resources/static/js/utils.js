/**
 * 一些公用的js方法，为静态方法
 * @type {Object}
 */
ESM.Utils = function(){
	this.format = new ol.format.GeoJSON();
};

/**
 * 多个对象的合并并返回合并后的对象，可以传入任意多个参数
 * @param  {object} obj 要合并的对象参数，可以传入多个参数
 * @return {object}     合并后的对象
 */
ESM.Utils.prototype.mergeObjects = function(obj){
	var length = arguments.length;
	if(length < 2 || obj == null) 
		return obj;
	for(var index = 1; index < length; index++){
		var source = arguments[index];
		for(var key in source){
			obj[key] = source[key];
		}
	}
	return obj;
}

/**
 * 判断参数是否为数组
 *
 * @param value			传入检测的参数(返回值为Bool)
 * @constructor
 *
 * @example
 * var cBool = HYW.Utils().isArrayFn(value); 
 */
ESM.Utils.prototype.isArrayFn = function(value){
	if (typeof Array.isArray === "function"){
		return Array.isArray(value); 
    }
	else{
		return Object.prototype.toString.call(value) === "[object Array]"; 
    } 
};
 	
/**
 * @classdesc
 * 原生js绑定事件
 *
 * @param obj			事件对象
 * @param type			事件类型
 * @param fn			添加事件
 * @constructor
 *
 * @example
 * var bind=HYW.Utils().bindEvent(); 
 * bind(elem, type, fn);
 */
ESM.Utils.prototype.bindEvent = function(){
	 if (window.addEventListener){
		 return function(elem, type, fn){
			 elem.addEventListener(type, fn, false);
		 }
	 }
	 else if (window.attachEvent){
		 return function(elem, type, fn){
			 elem.attachEvent("on" + type, fn);
		 }
	 }
};
/**
 * @classdesc
 * 原生js解除绑定事件
 *
 * @param elem			事件对象
 * @param type			事件类型
 * @param fn			添加事件
 * @constructor
 *
 * @example
 * var unbind=HYW.Utils().unbindEvent(elem, type, fn); 
 * unbind(elem, type, fn);
 */
ESM.Utils.prototype.unbindEvent = function(){
	 if (window.removeEventListener) {
		 return function(elem, type, fn) {
			 elem.removeEventListener(type, fn, false);
		 }
	 } 
	 else if (window.detachEvent) {
		 return function(elem, type, fn) {
			 elem.detachEvent("on" + type, fn);
		 }
	 }
};
/**
 * @classdesc
 * 取消默认行为
 *
 * @param e			event对象
 * @constructor
 */
ESM.Utils.prototype.stopDefault = function(e){
	if ( e && e.preventDefault )  e.preventDefault(); 
	else window.event.returnValue = false; 
	return false;
};
/**
 * @classdesc
 * 格式化路程
 *
 * @param length	 路程 单位米
 * @constructor
 */
ESM.Utils.prototype.formatLength=function(length){
	if(length>=1000){
		length=(length/1000).toFixed(1)+'km';
	}
	else{
		length=length+'m';
	}
	return length;
};
/**
 * @classdesc
 * 格式化时间
 *
 * @param time		时间 单位秒
 * @constructor
 */
ESM.Utils.prototype.formatTime=function(time){
	time=parseInt(time/60);
	if(time>=60){
		if(parseInt(time%60)==0) time=parseInt(time/60)+'小时';
		else time=parseInt(time/60)+'小时'+parseInt(time%60)+'分';
	}
	else{
		time=time+'分钟';
	}
	return time;
}
/**
 * @classdesc
 * 根据多个坐标点，设置地图中心点，可以显示所有坐标点
 *
 * @param map		            
 * @param coordinatesArray	 坐标点数组
 * @constructor
 */
ESM.Utils.prototype.fitMapSize=function(map,coordinatesArray){
	var extent=ol.extent.boundingExtent(coordinatesArray);
	var size= map.getSize();
	map.getView().fit(extent,size);
}


ESM.Utils.prototype.projTransform = function(coordinate, source, destination) {
	source = source ? source : 'EPSG:4326';
	destination = destination ? destination : 'UTMK';
	var _coord = ol.proj.transform(coordinate, source, destination);
	return _coord;
};

/**
 * 根据坐标点、距离、角度，计算另一个坐标点
 * @param {ol.Coordinate} coordinate 原点 (`[lon, lat]`) 4326
 * @param {number} distance 原点与目标点之间的距离
 * @param {number} bearing 原点与目标点的角度
 * @return {ol.Coordinate} 目标点
 */
ESM.Utils.prototype.calculatePoint = function(coordinate,distance,bearing){
	var wgs84Sphere = new ol.Sphere(6378137);
	if(!bearing){
		bearing = 1.57;
	}
	return wgs84Sphere.offset(coordinate, distance, bearing);
}

/**
 * 坐标系为3857
 * */
ESM.Utils.prototype.calculateRadius = function(start,end){
	return Math.sqrt(Math.pow(end[0] - start[0], 2) + Math.pow(end[1] - start[1], 2));
}

/**
 * 坐标系为4326
 * */
ESM.Utils.prototype.calculateLength = function(start,end,format){
	var wgs84Sphere = new ol.Sphere(6378137);
	var length = wgs84Sphere.haversineDistance(this.projTransform(start, 'EPSG:3857', 'EPSG:4326'), 
					this.projTransform(end, 'EPSG:3857', 'EPSG:4326'));
	var output = [];
	if (length > 1000) {
		output = (Math.round(length / 1000 * 100) / 100) + ' ' + 'km';
	} else {
		output = (Math.round(length * 100) / 100) + ' ' + 'm';
	}
	if (format) { 
		output = [(Math.round(length * 100) / 100).toString(), output];
	}
    return output;
}

ESM.Utils.prototype.compare = function(prop) {
	return function (obj1, obj2) {
        var val1 = obj1[prop];
        var val2 = obj2[prop];
        if (val1 < val2) {
            return -1;
        } else if (val1 > val2) {
            return 1;
        } else {
            return 0;
        }            
    }
}

ESM.Utils.prototype.hex2Rgba = function(color,alpha) {
	var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
	var sColor = color.toLowerCase();
	if(sColor && reg.test(sColor)){
		if(sColor.length === 4){
			var sColorNew = "#";
			for(var i=1; i<4; i+=1){
				sColorNew += sColor.slice(i,i+1).concat(sColor.slice(i,i+1));	
			}
			sColor = sColorNew;
		}
		//处理六位的颜色值
		var sColorChange = [];
		for(var i=1; i<7; i+=2){
			sColorChange.push(parseInt("0x"+sColor.slice(i,i+2)));	
		}
		return "rgba(" + sColorChange.join(",") + ", " + alpha + ")";
	}else{
		return sColor;	
	}
};

ESM.Utils.prototype.rgba2Hex = function(color) {
    var rgb = color.split(',');
    var r = parseInt(rgb[0].split('(')[1]);
    var g = parseInt(rgb[1]);
    var b = parseInt(rgb[2]);
    var hex = "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
    return hex;
 }

//feature转json字符串
ESM.Utils.prototype.feature2Json = function(feature) {
    var json = this.format.writeFeature(feature);
    return json;
}

//json字符串转feature对象
ESM.Utils.prototype.json2Feature = function(json) {
    var feature = this.format.readFeature(json);
    return feature;
}

ESM.Utils.prototype.fromLonLat = function(coord) {
    return ol.proj.fromLonLat(coord);
}

ESM.Utils.prototype.toLonLat = function(coord) {
	return ol.proj.toLonLat(coord);
}