ESM.InfoWindow = function(options) {
	var defaults_={
		elems:'',
		autoPan:true,
		offset:[0,-5],
		className:'',
		positioning:'center-left',
		singleIcon:null,
		autoPanDuration:300,
		autoPanMargin:200
	};
	for(key in options){
	    if(!options.hasOwnProperty(key)) continue;
		defaults_[key]=options[key];
	}
	this.config_=defaults_;
	
	/**
	 * 公用工具类对象
	 * @type {HYW}
	 */
	this.utils_=new ESM.Utils();
	/**
	 * 公用工具类，绑定事件
	 * @type {HYW}
	 */
	this.bind=this.utils_.bindEvent();
	
	this.coordinate=null;
	/**
	 * InfoWindow中的dom
	 * @type {DOM}
	 */
	this.infoWindowElement=document.createElement('div');
	
	/**
	 * 新添加的类名
	 * @type {String}
	 */
	this.infoWindowElement.className='ol-popup';
	this.infoWindowElement.id='popup';
	this.infoWindowElement.innerHTML+='<a id="popup-closer" style="z-index:1000;" class="ol-popup-closer"></a>';
	this.infoWindow_= new ol.Overlay({
		element :this.infoWindowElement,
		offset:this.config_.offset,
		autoPan:this.config_.autoPan,
		positioning :this.config_.positioning,
		autoPanAnimation:{
		    duration:this.config_.autoPanDuration
		},
		autoPanMargin:this.config_.autoPanMargin,
		zIndex:this.config_.zIndex
	});
	
	if(this.config_.map){
	    this.config_.map.addOverlay(this.infoWindow_);
	}
	
	this.elem_=this.infoWindow_.getElement(),This=this;
	
	for(var i=0;i<this.elem_.childNodes.length;i++){
	    if(this.elem_.childNodes[i].className=='ol-popup-closer'){
		    this.bind(this.elem_.childNodes[i],'click',function(){This.close()}); 
		}
	}
};

/**
 * 设置坐标点
 */
ESM.InfoWindow.prototype.setPosition=function(coordinate){
	this.coordinate=coordinate;
	var elem=this.infoWindow_.getElement();
	elem.style.display='block';
    this.infoWindow_.setPosition(coordinate);
}

/**
 * 获取坐标点
 */
ESM.InfoWindow.prototype.getPosition=function(){
	return this.coordinate;
}
/**
 * 设置内容
 */
ESM.InfoWindow.prototype.setContent=function(content){
	this.content=content;
	var contentDoc = document.getElementById("popup-content");
	if(contentDoc){
		this.infoWindow_.getElement().removeChild(contentDoc);
	}
    this.infoWindow_.getElement().innerHTML += ('<div id="popup-content">' +content +'</div>');
	var elem=this.infoWindow_.getElement(),This=this;
	for(var i=0;i<this.elem_.childNodes.length;i++){
	    if(elem.childNodes[i].className=='ol-popup-closer'){
		    this.bind(elem.childNodes[i],'click',function(){This.close()});
		}
	}
}
/**
 * 获取坐标点
 */
ESM.InfoWindow.prototype.getContent=function(){
	return this.content;
}
/**
 * 设置布局方式
 */
ESM.InfoWindow.prototype.setPositioning=function(positioning){
    this.infoWindow_.setPositioning(positioning);
}
/**
 * 获取布局方式
 */
ESM.InfoWindow.prototype.getPositioning=function(){
    return this.infoWindow_.getPositioning();
}
/**
 * 设置位置偏移
 */
ESM.InfoWindow.prototype.setOffset=function(oarray){
	this.infoWindow_.setOffset(oarray);
}
/**
 * 获取位置偏移
 */
ESM.InfoWindow.prototype.getOffset=function(){
	return this.infoWindow_.getOffset();
}

/**
 * 获取infowindow类
 */
ESM.InfoWindow.prototype.getClass=function(){
	var iClassArray=this.infoWindowElement.className.split(' ');
	iClassArray.splice(0, 1);
	return iClassArray;
}

/**
 * 打开infowindow
 */
ESM.InfoWindow.prototype.open=function(marker){
	if(!!marker){
		this.coordinate=marker.getPosition();
	    this.infoWindow_.setPosition(this.coordinate);
	}
}
/**
 * 关闭infowindow
 */
ESM.InfoWindow.prototype.close=function(){
	var elem=this.infoWindow_.getElement(),This=this;
	elem.style.display='none';
}