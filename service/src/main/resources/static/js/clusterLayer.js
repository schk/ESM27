//ESM.ClusterLayer = function(type,image,features) {
//	this.layid = type+'ClusterLayer';
//	var source = new ol.source.Vector({
//		features : features
//	});
//	var clusterSource = new ol.source.Cluster({
//		source : source,
//		distance : 30
//	});
//
//	this.layer = new ol.layer.Vector({
//		source : clusterSource,
//		style : function(feature) {
//			var size = feature.get('features').length;
//			var style;
//			if (size > 1) {
//				style = [ new ol.style.Style({
//					image : new ol.style.Icon({
//						src : 'images/' + image + '.png',
//						anchor : [ 0.5, 0.5 ]
//					}),
//					text : new ol.style.Text({
//						font : '12px Arial',
//						text : size.toString(),
//						offsetX:5,
//						fill : new ol.style.Fill({
//							color : '#FFFFFF'
//						})
//					}),
//					zIndex : 3
//				}) ];
//			} else {
//				style = [ new ol.style.Style({
//					image : new ol.style.Icon({
//						src : 'images/' + image + '.png',
//						anchor : [ 0.5, 0.5 ]
//					})
//				}) ];
//			}
//			
//			return style;
//		}
//	});
//	
//	this.layer.set('id',this.layid);
//}
//
///**
// * 获取DOM
// */
//ESM.ClusterLayer.prototype.addTo = function(map){
//	var layers = map.getLayers()
//	for(var i=layers.lenght-1;i>-1;i--){
//		var lay = layers[i];
//		if(lay.get('id')==this.layid){
//			map.removeLayer(lay);
//			return;
//		}
//	};
//	map.addLayer(this.layer);
//}