/**
 * 地图组件
 */
ESM.HistoryMap = function(options){
	this.extent = [20037508.3427892,20037508.3427892,-20037508.3427892,-20037508.3427892];
	this.center = ol.proj.transform(options.center, 'EPSG:4326', 'EPSG:3857');
	this.zoom = options.zoom;
	var scaleLineControl = new ol.control.ScaleLine({className:'ol-scale-line'});
	var tileLayer = new ol.layer.Tile({
		 source: new ol.source.OSM()
    });
	this.historyLayer = new  ol.layer.Vector({
		source : new ol.source.Vector({
			features : []
		}),
		style : function(feature) {
			var image = feature.get('params').imagePath?feature.get('params').imagePath:'/mapIcon/mapFixedIcon/sczz.png';
			var name = feature.get('name')?feature.get('name'):'';
			var scolor = feature.get('scolor')?feature.get('scolor'):'#4781d9';
			var fcolor = feature.get('fcolor')?feature.get('fcolor'):'rgba(67, 110, 238, 0.4)';
			var geoType = feature.getGeometry().getType();
			var style;
			if(geoType=="Point"){
				style = new ol.style.Style({
					image : new ol.style.Icon({
						src : baseFileUrl+image,
						anchor : [ 0.5, 0.5 ]
					}),
					text : new ol.style.Text({
						text : name,
						offsetY : 20,
						font : 'bold 12px sans-serif',
						fill : new ol.style.Fill({
							color : '#84C1FF'
						}),
						stroke : new ol.style.Stroke({
							color : '#ECF5FF',
							width : 3
						})
					})
				})
			}else if(geoType=="LineString"){
				style = new ol.style.Style({
					stroke:new ol.style.Stroke({
						width:2,
						color: scolor
					})
				});
			}else if(geoType=="Polygon"){
				style = new ol.style.Style({
					stroke:new ol.style.Stroke({
						width:2,
						color: scolor
					}),
					fill: new ol.style.Fill({
						color:fcolor
					})
				})
			}
			return style;
		},
		zIndex:5
	});
	this.map = new ol.Map({
		controls: ol.control.defaults({
            attributionOptions: {
              collapsible: false
            },
            zoom: false
          }).extend([
            scaleLineControl
         ]),
         layers: [
        	 tileLayer,
        	 this.historyLayer
         ],
         view: new ol.View({
             // 设置成都为地图中心
             center: this.center,
             zoom: this.zoom,
             minZoom: 3,
             maxZoom: 18,
         }),
         target: options.element
	});
}

ESM.HistoryMap.prototype.addFeature = function(feature){
	this.historyLayer.getSource().addFeature(feature);
	var extent = feature.getGeometry().getExtent();
	if(extent[0]<this.extent[0]){
		this.extent[0] = extent[0];
	}
	if(extent[1]<this.extent[1]){
		this.extent[1] = extent[1];
	}
	if(extent[2]>this.extent[2]){
		this.extent[2] = extent[2];
	}
	if(extent[3]>this.extent[3]){
		this.extent[3] = extent[3];
	}
	this.map.getView().fit(this.extent,{size:this.map.getSize(),duration:1000});
}

ESM.HistoryMap.prototype.addFeatures = function(features){
	this.historyLayer.getSource().clear();
	this.historyLayer.getSource().addFeatures(features);
	for(var i=0;i<features.length;i++){
		var feature = features[i];
		var extent = feature.getGeometry().getExtent();
		if(extent[0]<this.extent[0]){
			this.extent[0] = extent[0];
		}
		if(extent[1]<this.extent[1]){
			this.extent[1] = extent[1];
		}
		if(extent[2]>this.extent[2]){
			this.extent[2] = extent[2];
		}
		if(extent[3]>this.extent[3]){
			this.extent[3] = extent[3];
		}
	}
	this.map.getView().fit(this.extent,{size:this.map.getSize(),duration:1000});
}

ESM.HistoryMap.prototype.clearAll = function(){
	this.historyLayer.getSource().clear();
	this.extent = [20037508.3427892,20037508.3427892,-20037508.3427892,-20037508.3427892];
	this.map.getView().setCenter(this.center);
	this.map.getView().setZoom(this.zoom);
}

ESM.HistoryMap.prototype.removeFeature = function(id){
	var feature = this.historyLayer.getSource().getFeatureById(id);
	this.historyLayer.getSource().removeFeature(feature);
}

ESM.HistoryMap.prototype.saveMap = function(url){
	this.map.once('postcompose', function(event) {
		const
		canvas = event.context.canvas;
		if (navigator.msSaveBlob) {
			navigator.msSaveBlob(canvas.msToBlob(), 'historyMap.png');
		} else {
			canvas.toBlob(function(blob) {
				if(url&&url!=''){
					var formData = new FormData();
					formData.append("file",blob,'historyMap.png');
					formData.append("enctype","multipart/form-data");
					$.ajax({
						type: 'POST',
					    url: url,
					    data: formData,
					    processData: false,
					    contentType: false,
					    success:function(data){
					    	layer.msg("保存成功！");
					    }
					});
				}else{
					layer.msg("上传地图失败！");
				}
				saveAs(blob, 'historyMap.png');
			});
		}
	});
	this.map.renderSync();
}