//ajax读取数据
function getAjaxData(url,param){
	var defer = $.Deferred();
	$.ajax({
       url : url, 
       type : "POST",       
       timeout : 2000,
       data:JSON.stringify(param),
       dataType:"json",
	   headers: {"Content-Type": "application/json;charset=utf-8"},
       async: false,
       success: function(data){
           defer.resolve(data)
       },
	   error: function(request) {
		   layer.msg("网络错误", {time: 1000});
       },
    }); 
    return defer.promise();
}

//加载数据
function executeAjax(url,param,fn){
    $.when(getAjaxData(url,param)).done(function(data){
	   	fn(data);
    });
}


function getTimestamp(){
	return new Date().getTime();
}

//判断div里面的属性值非空
function checkIsEmpty(oid){
    var checkRight=true;
    $("#"+oid).find("input[name='check']:visible").each(function(){
        var value = $(this).val();
        var checkS = $(this).attr("t"); 
        if ($(this).parent().css("border-color")== 'rgb(255, 0, 0)') {
        	$(this).parent().css("border-color","rgb(211, 211, 211)");
        }
        var digitDouble = /^-?\d+(\.\d+)?$/;
        if (!value ||$.trim(value) == '') {
        	$(this).parent().css("border-color","red");
        	checkRight=false;
        }
//        else{
//        	if(checkS=='double'){
//        		if(!digitDouble.test(value)){
//        			$(this).parent().css("border-color","red");
//        			checkRight=false;
//        		}
//        	}
//        }
    })
   
    return checkRight;
}

// https://tc39.github.io/ecma262/#sec-array.prototype.find
if (!Array.prototype.find) {
    Object.defineProperty(Array.prototype, 'find', {
      value: function(predicate) {
       // 1. Let O be ? ToObject(this value).
        if (this == null) {
          throw new TypeError('"this" is null or not defined');
        }
  
        var o = Object(this);
  
        // 2. Let len be ? ToLength(? Get(O, "length")).
        var len = o.length >>> 0;
  
        // 3. If IsCallable(predicate) is false, throw a TypeError exception.
        if (typeof predicate !== 'function') {
          throw new TypeError('predicate must be a function');
        }
  
        // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
        var thisArg = arguments[1];
  
        // 5. Let k be 0.
        var k = 0;
  
        // 6. Repeat, while k < len
        while (k < len) {
          // a. Let Pk be ! ToString(k).
          // b. Let kValue be ? Get(O, Pk).
          // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
          // d. If testResult is true, return kValue.
          var kValue = o[k];
          if (predicate.call(thisArg, kValue, k, o)) {
            return kValue;
          }
          // e. Increase k by 1.
          k++;
        }
  
        // 7. Return undefined.
        return undefined;
      }
    });
  }



//对Date的扩展，将 Date 转化为指定格式的String
//月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
//年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
//例子： 
//(new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
//(new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
Date.prototype.Format = function (fmt) { //author: meizz 
 var o = {
     "M+": this.getMonth() + 1, //月份 
     "d+": this.getDate(), //日 
     "h+": this.getHours(), //小时 
     "m+": this.getMinutes(), //分 
     "s+": this.getSeconds(), //秒 
     "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
     "S": this.getMilliseconds() //毫秒 
 };
 if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
 for (var k in o)
 if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
 return fmt;
}




//查看图片统一方法
function showBigImg(src){
	$("#showImgDivBg").show();
	$("#showImgDiv").show();
	$("#bigImgSrc").attr("src",src);
	
}
//关闭图片
function pictureColseIcon(){
	$("#showImgDivBg").hide();
	$("#showImgDiv").hide();
}

