//todo  白-ESM27定位获取 setId
//传感器数据采集返回时进行对象赋值更新   完成
//刷新带回事故点坐标   完成
//事故点位置变了，记得情况session的key
//火灾倒计时 完成
//todo 画点放到界面上
//查数据库t_gas_property把热辐射的气体类型添加上
//重质油品喷溅的界面同左侧灾害模拟的热传播速度一样写参考范围，让他自己填数字
//所有界面的风速、风向、大气稳定度单独提出去计算
//自动意泄露源定位的传感器位置要进行选择展示
//燃烧线速度,石油的
var ZHMNrswfxModelDivMt = 0.0781;
var ksmnTypeDatas=new HashMap();
var ksmnType='';
//超高压爆炸的扩散颜色 绿黄红
var iteamsColor=['#00FF30','#E4FF00','#FF0000','#ddd'];
var iteamsColorBZ=['#00FF30','#E4FF00','#df731b','#FF0000'];
//颗粒扩散分析颜色 灰色系
var iteamsColorKL=['#aaa','#eee','#414141'];
var sjzParams = {};
var sjzUrl = "";

function getnum(num)
{
	var s = num+"";
	return result = s.substring(0,s.indexOf(".")+3);
}
//获取实时风速风向大气稳定度的params
function getWeatherData(params){
	var fzdxModelFS = global_weatherData.windSpeed;
	var fzdxModelWD = global_weatherData.temperature;
	var fzdxModelSD = global_weatherData.humidity;
	var fzdxModelFX = global_weatherData.windDirection;
	params.as = getASValue(parseFloat(fzdxModelFS));
//	params.as = DQWDDValue(fzdxModelBW,fzdxModelQYY,fzdxModelFS);
	params.ws = fzdxModelFS;
	params.wd = fzdxModelFX;
	params.humidity = global_weatherData.humidity;
	params.cw = fzdxModelWD;
	return params;
}

//获取事故点坐标params
function getZhmnAccidentPoint(params){
    if(zhmnAccidentPoint.length>0){
    	params.sX = zhmnAccidentPoint[0];
    	params.sY = zhmnAccidentPoint[1];
    	return params;
    }else{
    	layer.msg("请选择准确事故点位置", {time: 1000});
    	return false;
    }
}

/*
 * 只获取实时数据
 * */
function getPublicParams(params){
	var pointParams = getZhmnAccidentPoint(params);
	if(pointParams){
		params = pointParams;
	}else{
		layer.msg('请先确认事故点', {icon: 1, time: 1000});
		return false;
	}
	params = getWeatherData(params);
	return params;
}


//获取气象模拟数据
function getCommonSimulateData(params){
	params.ws = $("#commonTQFS").html()? $("#commonTQFS").html():3;
	params.cw = $("#commonTQWD").html()?$("#commonTQWD").html():20;
	params.humidity = $("#commonTQSD").html()?$("#commonTQSD").html():80;
	params.wd = $("#commonTQFXValue").val()?$("#commonTQFXValue").val():0;
	params.pressure = $("#commonTQYQ").html()?$("#commonTQYQ").html():1000;
	params.as = $("#commonAs").html()?$("#commonAs").html():2;	
	return params;
}
/*获取实时数据或者模拟数据
 * isRealTime==1 实时，0模拟
 * */
function getRealTimeOrSimulateData(isRealTime,params){
	if(isRealTime==1){
		if(global_weatherData.windSpeed){
			var params = {};
			var publicParams = getPublicParams(params);
			if(publicParams){
				params = publicParams;
			}else{
				return false;
			}
		}else{
			layer.msg("检测设备未就绪,无实时数据", {time: 1000});
		}
	}else{
		params = getCommonSimulateData(params);	
		params= getZhmnAccidentPoint(params);
	}
	return params;
}

//开启关闭时间轴
function hideShowTime(oid){
	$("#zhmnKSSJDivOutSyn").hide();
	$("#zhmnKSSJDivOut").hide();
	$("#zhmnKSSJDivUl li").removeClass();
	$("#zhmnKSSJDivUlSyn li").removeClass();
	$("#"+oid).show();
}
//获取实时数据不包含事故点
function getRealTimeOrSimulateDataFS(isRealTime,params){
	if(isRealTime==1){
		if(global_weatherData.windSpeed){
			var params = {};
			var publicParams = getWeatherData(params);
			if(publicParams){
				params = publicParams;
			}else{
				return false;
			}
		}else{
			layer.msg("检测设备未就绪,无实时数据", {time: 1000});
		}
	}else{
		params = getCommonSimulateData(params);	
	}
	return params;
}

$(document).ready(function(){
});
//启动倒计时
var timeInterval = 	null;
/*
 * @parm time 秒
 * */
function startTimeInterval(time){
	_stopTimer = false;
	//开始循环
	timeIntervalEnd();
	timeInterval = 	setInterval(function(){
		time = parseInt(time)-1;
			if(time>0){
				$("#surplusTime").html(getDateTimeT(time));
			}
			//为0停止
			if(time==0 || time<0){
				timeIntervalEnd();
			}
		},1000);
}
//暂停喷溅
function timeIntervalEnd(){
	if(timeInterval){
		clearInterval(timeInterval);
		timeInterval = 	null;
		_stopTimer = true;
	}
}
/**
 * 根据多点生成图形
 * @params coords点集合,isBorder需要边框,borderColor边框颜色,fillColor填充颜色,transparency填充透明度（不需要填充0）
 * */
function gasDiffusionDraw(pointsObj){
		buildGeometryByCoords(pointsObj.coords);
//		buildGeometryByCoords(pointsObj.coords,pointsObj.isBorder,pointsObj.borderColor,pointsObj.fillColor,pointsObj.fillTransparency);
}


//燃烧物分析
function rswfxTubiaoChartShow3(isRealTime,coRate,co2Rate,so2Rate,noRate,no2Rate){
	var html="";
	html+="<li onclick='zhmn_calculationRate("+false+","+isRealTime+",\"co\","+getnum(coRate)+",\"0\",\"1.25\",\"0\")' class='rswBgColor'><span class='rswName'>1:一氧化碳(CO)</span><span class='rswNumber'>"+getnum(coRate)+"(kg/s)</span></li>";
	html+="<li onclick='zhmn_calculationRate("+false+","+isRealTime+",\"co2\","+getnum(co2Rate)+",\"1\",\"1.97\",\"0\")'><span class='rswName'>2:二氧化碳(CO2)</span><span class='rswNumber'>"+getnum(co2Rate)+"(kg/s)</span></li>";
	html+="<li onclick='zhmn_calculationRate("+false+","+isRealTime+",\"so2\","+getnum(so2Rate)+",\"1\",\"1.2.93\",\"0\")'class='rswBgColor'><span class='rswName'>3:二氧化硫(SO2)</span><span class='rswNumber'>"+getnum(so2Rate)+"(kg/s)</span></li>";
	html+="<li onclick='zhmn_calculationRate("+false+","+isRealTime+",\"no\","+getnum(noRate)+",\"0\",\"1.27\",\"0\")'><span class='rswName'>4:一氧化氮(NO)</span><span class='rswNumber'>"+getnum(noRate)+"(kg/s)</span></li>";
	html+="<li onclick='zhmn_calculationRate("+false+","+isRealTime+",\"no2\","+getnum(no2Rate)+",\"1\",\"2.05\",\"0\")'class='rswBgColor'><span class='rswName'>5:二氧化氮(NO2)</span><span class='rswNumber'>"+getnum(no2Rate)+"(kg/s)</span></li>";
	$("#zhmnRSWFXDivList").html(html);
	$("#zhmnRSWFXDivOut").show(); 
}
//燃烧物分析
function rswfxTubiaoChartShow2(coRate,co2Rate,so2Rate,noRate,no2Rate){
	var html="";
	html+="<li onclick='zhmn_calculationRate("+true+",\"1\",\"co\","+getnum(coRate)+",\"0\",\"1.25\",\"1\")' class='rswBgColor'><span class='rswName'>1:一氧化碳(CO)</span><span class='rswNumber'>"+getnum(coRate)+"(kg/s)</span></li>";
	html+="<li onclick='zhmn_calculationRate("+true+",\"1\",\"co2\","+getnum(co2Rate)+",\"1\",\"1.97\",\"1\")'><span class='rswName'>2:二氧化碳(CO2)</span><span class='rswNumber'>"+getnum(co2Rate)+"(kg/s)</span></li>";
	html+="<li onclick='zhmn_calculationRate("+true+",\"1\",\"so2\","+getnum(so2Rate)+",\"1\",\"1.2.93\",\"1\")'class='rswBgColor'><span class='rswName'>3:二氧化硫(SO2)</span><span class='rswNumber'>"+getnum(so2Rate)+"(kg/s)</span></li>";
	html+="<li onclick='zhmn_calculationRate("+true+",\"1\",\"no\","+getnum(noRate)+",\"0\",\"1.27\",\"1\")'><span class='rswName'>4:一氧化氮(NO)</span><span class='rswNumber'>"+getnum(noRate)+"(kg/s)</span></li>";
	html+="<li onclick='zhmn_calculationRate("+true+",\"1\",\"no2\","+getnum(no2Rate)+",\"1\",\"2.05\",\"1\")'class='rswBgColor'><span class='rswName'>5:二氧化氮(NO2)</span><span class='rswNumber'>"+getnum(no2Rate)+"(kg/s)</span></li>";
	$("#zhmnRSWFXDivList").html(html);
	$("#zhmnRSWFXDivOut").show(); 
}


//显示气体扩散图例
//CO   35PPM 国标50PPM
//CO2  1%VOL   1*10-2
//SO2  2PPM    2*10-6
//NO   10PPM   10*10-6
//NO2  2PPM    2*10-6

function shouTuLiKuoSan(ctype){
	$("#ysslDiv").show();
	var type = ctype.toLowerCase();
	if(type == 'co'){
		$("#ysslDivHong").html('35PPM以上');
		$("#ysslDivHuang").html('25-35PPM');
		$("#ysslDivLv").html('0-25PPM');
	}else if(type == 'co2'){
		$("#ysslDivHong").html('2PPM以上');
		$("#ysslDivHuang").html('0.5-2PPM');
		$("#ysslDivLv").html('0-0.5PPM');
	}else if(type == 'so2'){
		$("#ysslDivHong").html('2PPM以上');
		$("#ysslDivHuang").html('0.5-2PPM');
		$("#ysslDivLv").html('0-0.5PPM');
	}else if(type == 'no'){
		$("#ysslDivHong").html('10PPM');
		$("#ysslDivHuang").html('5-10PPM');
		$("#ysslDivLv").html('0-5PPM');
	}else if(type == 'no2'){
		$("#ysslDivHong").html('2PPM以上');
		$("#ysslDivHuang").html('1-2PPM');
		$("#ysslDivLv").html('0-1PPM');
	}else if(type == 'nh3'){
		$("#ysslDivHong").html('25PPM以上');
		$("#ysslDivHuang").html('20-25PPM');
		$("#ysslDivLv").html('0-20PPM');
	}else if(type == 'ch4'){
		$("#ysslDivHong").html('25%LEL以上');
		$("#ysslDivHuang").html('10-25%LEL');
		$("#ysslDivLv").html('0-10%LEL');
	}else if(type == 'hf'){
		$("#ysslDivHong").html('25PPM以上');
		$("#ysslDivHuang").html('10-25PPM');
		$("#ysslDivLv").html('0-10PPM');
	}else if(type == 'cl'){
		$("#ysslDivHong").html('1PPM以上');
		$("#ysslDivHuang").html('0.5-1PPM');
		$("#ysslDivLv").html('0-0.5PPM');
	}else if(type == 'h2s'){
		$("#ysslDivHong").html('15PPM以上');
		$("#ysslDivHuang").html('10-15PPM');
		$("#ysslDivLv").html('0-10PPM');
	}else{
		$("#ysslDivHong").html('2PPM以上');
		$("#ysslDivHuang").html('0.5-2PPM');
		$("#ysslDivLv").html('0-0.5PPM');
	}
}


//灾害模拟，重轻质扩散
function zhmn_calculationRate(isLeft,isRealTime,type,rate,is_heavy,gd,height){
	if(rate<0.03){
		layer.msg("泄漏率太小,污染在安全范围内", {time: 3000});
		return false;
	}
	//清除燃烧分析
	cleanKuoSanDrawLayer();
	ksmnType= type;
	$("#zhmnKSSJContent").html(type+"("+rate+")");
	var titleTime=type+"("+rate+")";

	var params = {};
	
	var publicParams = getRealTimeOrSimulateData(isRealTime,params);
	if(publicParams){
		params = publicParams;
	}else{
		return false;
	}

	//轻质气体
	if(is_heavy=='0'){
			var url=baseUrl+'/webApi/math/gaussionModelParameters.jhtml';
			params.gasType = type;
			if(height==0){
				params.h = height;
			}else{
				params.h = $("#ZHMNrfsModelXLDJDMGD").val();
			}
			params.fCurrentTime = parseInt($("#zhmnRSFSSJ").val())*60;
			params.dq = rate;
			sjzParams = params;
			sjzUrl = url;
			mathAjax(params,url,function(data){
					var obj = data.data;
					var layer = layerIsExist('buildGeometryLayer',global.map);
					if(layer){
						clearBuildGeometryLayer();
					}
					var pointDataList = obj.mathGranularVoLsit;
					var pointsObj = {};
					pointsObj.sjzUrl = sjzUrl;
					pointsObj.sjzParams = sjzParams;
					pointsObj.pointDataList=pointDataList;
					for(var i=0;i<pointDataList.length;i++){
						pointsObj.coords = eval('(' + pointDataList[i] + ')');
						pointsObj.isBorder = false;
						pointsObj.borderColor = '';
						pointsObj.fillColor = iteamsColor[i];
						pointsObj.fillTransparency = '0.6';
						buildGeometryByCoords(pointsObj.coords,pointsObj.isBorder,pointsObj.borderColor,pointsObj.fillColor,pointsObj.fillTransparency);
					}
					if(isLeft){
						hideShowTime("zhmnKSSJDivOutSyn");
						var paramsSyn = {};
						pointsObj.titleTime = titleTime;
						pointsObj.point = zhmnAccidentPoint;
						paramsSyn.accType = "zhmn_qzks";
						paramsSyn.content = pointsObj;
						send(JSON.stringify({"evetype":"zhmn","val":paramsSyn}));
					}else{
						hideShowTime("zhmnKSSJDivOut");
					}
					drawHighLine('drawHighLine',zhmnAccidentPoint);
					gotoPointView(zhmnAccidentPoint[0],zhmnAccidentPoint[1],18);
			});
	}else{
	//重质气体计算
		var lonlat = ol.proj.transform(zhmnAccidentPoint, 'EPSG:3857', 'EPSG:4326');
		params.lng=lonlat[0];
		params.lat=lonlat[1];
		params.dWindSpeed=params.ws;
		params.dT = params.cw;
		params.humidity = params.humidity;
		params.windWane = params.wd;
		params.RType = 2;
		params.h2scon= 4;
		params.pipepress=3;
		params.dQ=rate*1000;
		var tokenV = getTime();
		params.wtoken=tokenV;
		params.windWane=global_weatherData.windDirection;
		params.isLeft = isLeft;
		$("#fzdxModelDiv").hide();
		gasSpreadController(params);
	}
	shouTuLiKuoSan(type);
}


//燃烧物分析
function rswfxTubiaoChartShow1(coRate,co2Rate,so2Rate,noRate,no2Rate){
	$("#rswfxTubiaoDivOut").show();
	var myChart = echarts.init(document.getElementById("rswfxTubiaoDiv"));
	var app = {};
	option = null;
	option = {
	    title : {
	        text: '燃烧物有毒气体',
	        x:'center'
	    },
	    legend: {
	        x : 'center',
	        y : 'bottom',
	        data:['coRate','co2Rate','so2Rate','noRate','no2Rate']
	    },
	    calculable : true,
	    series : [
	        
	        {
	            name:'面积模式',
	            type:'pie',
	            radius : [5, 50],
	            center : ['35%', '50%'],
	            roseType : 'area',
	            labelLine: {
	                normal: {
	                    smooth: 0.4,
	                    length: 5,
	                    length2: 5
	                },emphasis: {
	                    show: true
	                }
	            },
	            data:[
	                {value:coRate, name:'coRate'},
	                {value:co2Rate, name:'co2Rate'},
	                {value:so2Rate, name:'so2Rate'},
	                {value:noRate, name:'noRate'},
	                {value:no2Rate, name:'no2Rate'}
	            ]
	        }
	    ]
	};
	;
	if (option && typeof option === "object") {
	    myChart.setOption(option, true);
	}
	myChart.on('click', function(param) {
//	     console.log(param.data.name);//重要的参数都在这里！
	 })
}

function hzmyModelSubmit(){
	var isRealTime = $("input[name='hzmyModelWeatherData']:checked").val();
	var url=baseUrl+'/webApi/math/getFireSpreadRegion.jhtml';
	var params = {};
	var publicParams = getRealTimeOrSimulateData(isRealTime,params);
	if(publicParams){
		params = publicParams;
	}else{
		return false;
	}
	params.fCurrentTime = 1;
	sjzParams = params;
	sjzUrl = url;
	  $.ajax({
			type : "post",
			url : url,
			data: JSON.stringify(params),
			dataType:"json",
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {
				if(data.httpCode==200){
					var obj =  data.data;
//					var obj =  data.data.pointData;
					var pointDataList = obj.mathGranularVoLsit;
					for(var i=0;i<pointDataList.length;i++){
						var pointsObj = {};
						pointsObj.coords = eval('(' + pointDataList[i] + ')');
						pointsObj.isBorder = false;
						pointsObj.borderColor = '';
						pointsObj.fillColor = iteamsColor[i];
						pointsObj.fillTransparency = '0.6';
						buildGeometryByCoords(pointsObj.coords,pointsObj.isBorder,pointsObj.borderColor,pointsObj.fillColor,pointsObj.fillTransparency);
					}
					gotoPointView(zhmnAccidentPoint[0],zhmnAccidentPoint[1],18);
					//gotoPointView(zhmnAccidentPoint[0],zhmnAccidentPoint[1],0);   张修改
					$("#hzmyModelDiv").hide();
					$("#zhmnKSSJDivOut").show();
				}else{
					layer.msg(systemAbnormalityMsg, {time: 2000});
				}
				
			},error: function(request) {
				layer.msg(networkErrorMsg, {time: 2000});
            }
		});
}

function disasterOnclick(type,e){
	$("#disasterOnclickUl li").attr("class","");
	$(e).attr("class","disasterMenuActive");
	$("#klwcjBTN").hide();
	$("#xlksBTN").hide();
	if(type=='xlks'){
		 $("#xlksBTN").show();
		 $("#xlksModelDiv").show();
	}else if(type=='fzdx'){
		//不需要反复调用，60文件
		$("#fzdxModelDiv").show();
	}else if(type=='gjfs'){
		//60文件
		//泄露率  调用一次
		 $("#xlydwModelDivHandleTitle").html("高级反算");
		 $("#xlydwModelDiv").show();
		 $("#xlydwZhqtSelect").show();
		 $("#gjfsModelSubmitButton").show();
		 $("#xlydwModelSubmitButton").hide();
	}else if(type=='xlydw'){
		//调用一次
		 $("#xlydwModelDivHandleTitle").html("泄露源定位");
		 $("#xlydwModelDiv").show();
		 $("#xlydwZhqtSelect").show();
		 $("#gjfsModelSubmitButton").hide();
		 $("#xlydwModelSubmitButton").show();
	}else if(type=='rswfx'){
		//so2，no2的比例
		//调用轻质方法
		 $("#rswfxModelDiv").show();
	}else if(type=='nhjx'){
		//你是给我一个时
		//反复计时器减少时间
		showNhjxType();
		$("#nhjxModelDiv").show();
	}else if(type=='cgybz'){
		//一次
		$("#cgybzModelDiv").show();
	}else if(type=='yygqfy'){
		//你是给我一个时间
		//反复计时器减少时间
		$("#yygqfyModelDiv").show();
	}else if(type=='mhj'){
        $("#mhjModelDiv").show();
    }else if(type=='gqmgltj'){
	        $("#gqmhjTJModelDiv").show();
	}else if(type=='tqxx'){
	        $("#tqxxModelDiv").show();
	}else if(type=='zdyyc'){
		//删除公式列表
		$("#exprlist dd").remove();
		$.ajax({
			type:'POST',
			url: '/webApi/expr/qryList.jhtml',
			headers: {"Content-Type": "application/json;charset=utf-8"},
			dataType:'JSON',
			success: function(data){
				$.each(data.data,function(index,element){
					var html = '<dd onclick=\'exprdetail("'+ element.id_ +'","'+element.name+'","'+element.describtion+'")\'><span>' + element.name + '</span><i class=""></i></dd>';
					$("#exprlist").append(html);
					//选中当前公式
					$('#exprlist').on('click','dd',function(){
						$(this).addClass("onExprlist").siblings().removeClass("onExprlist");
					});
				});
			}
		});
		$("#formulaCalculationDiv").show();
	}else if(type=='rfs'){
		//一次
		$("#rfsModelDiv").show();
	}else if(type=='rfsr'){
		$("#rfsModelDiv").show();
	}else if(type=='sz'){
		//速录
		//调用轻质方法
		$("#szModelDiv").show();
	}else if(type=='fysj'){
		//
		$("#zhyppjModelDiv").show();
	}else if(type=='klwcj'){
		//反复算567
		//一圈一圈的线
		$("#klwcjBTN").show();
		$("#klwcjModelDiv").show();
	}else if(type=='gdxl'){
		//泄露率
		//后台自己泄漏一小时计算结果出现列表
		$("#gdxlModelDiv").show();
	}else if(type=='cs'){
		var json = {"type":"Feature","id":"106f-1533441628355-31485"};
		var data1=[];
		var data= [[11518384.023410,3571012.185491],[11518384.023441,3569597.971929],[11517676.916675,3568890.865132],[11516969.809909,3568183.758335],[11516262.703144,3567476.651539],[11514848.489581,3567476.651508],[11514141.382785,3568183.758274],[11514141.382754,3569597.971836],[11514141.382723,3571012.185398],[11514848.489489,3571719.292195],[11515555.596254,3572426.398992],[11516969.809817,3572426.399023],[11518384.023410,3571012.185491]];
		var geometry1 = {"type":"LineString"};
		geometry1.coordinates = data1;
		json.geometry = geometry1;
		var properties1 = {"type":"Curve","params":{"isSyn":true,"isSave":true,"recordType":"2","action":"会商绘标","type":"poly","plotType":"Curve"},"plotType":"Curve"};
		properties1.points=data;
		json.properties = properties1;
		positionSynMark(json);
		global.map.getView().animate({center: [11518384.023410,3571012.185491]}, {zoom: 13});
//		global.map.getView().fit([[5.515433,4.384062,0.000000],[4.101219,1.555635,0.000000],[1.979899,0.848528,0.000000],[4.101219,5.798276,0.000000]],{size:map.getSize(),duration:1000});
	}else if(type=="hzmy"){
		$("#hzmyModelDiv").show();
	}
}

function drawC(data,num){
	var json = {"type":"Feature","id":"106f-1533441628355-31485"+num};
//	var data= [[11518384.023410,3571012.185491],[11518384.023441,3569597.971929],[11517676.916675,3568890.865132],[11516969.809909,3568183.758335],[11516262.703144,3567476.651539],[11514848.489581,3567476.651508],[11514141.382785,3568183.758274],[11514141.382754,3569597.971836],[11514141.382723,3571012.185398],[11514848.489489,3571719.292195],[11515555.596254,3572426.398992],[11516969.809817,3572426.399023],[11518384.023410,3571012.185491]];
	var geometry1 = {"type":"LineString"};
	geometry1.coordinates = data;
	json.geometry = geometry1;
	var properties1 = {"type":"Curve","params":{"isSyn":true,"isSave":true,"recordType":"2","action":"会商绘标","type":"poly","plotType":"Curve"},"plotType":"Curve"};
	properties1.points=data;
	json.properties = properties1;
	positionSynMark(json);
	global.map.getView().animate({center: [11518384.023410,3571012.185491]}, {zoom: 13});
}


function drawBZ(oid,items,point,isFill,showDistance){
//	var point =global.esmUtil.fromLonLat([px,py]);
//	 var items = [{distance:data,color:'#FF0000'}];
//	 var items = [{distance:data[0],color:'#FF0000'},{distance:data[1],color:'#E4FF00'},{distance:data[2],color:'#00FF30'}];
	if(!point[0] || point[0]!="undefind"){
		clearBzy('cgybz'); 
		var image = baseUrl+"/images/start.png";
		addBzy(oid,point,items,image,isFill,showDistance,getWindDirection());
	}else{
		layer.msg("没有事故点信息，无法在地图上展示扩散范围！请设置事故点。", {time: 1000});
		return false;
	}
}

function closePanelMath(oid){
	$("#"+oid).hide();
	$("#disasterOnclickUl li").attr("class","");
}

//复杂地形 
function fzdxModelsSubmit(){
	if(!checkIsEmpty('fzdxModelDiv')){
		layer.confirm('请检查各个输入项是否输入!', {
	         btn: ['确定'] //按钮
	    },function(){
	    	layer.closeAll('dialog');
	    });
		return;
	}
	if($("#fzdxModelGDYQ").val()=='0'){
		layer.confirm('管道压强不能为0!', {
	         btn: ['确定'] //按钮
	    },function(){
	    	layer.closeAll('dialog');
	    });
		return;
	}
	if($("#fzdxModelWRQTND").val()=='0'){
		layer.confirm('污染气体浓度不能为0!', {
	         btn: ['确定'] //按钮
	    },function(){
	    	layer.closeAll('dialog');
	    });
		return;
	}
	if($("#fzdxModelXLKZJ").val()=='0'){
		layer.confirm('泄露孔直径不能为0!', {
	         btn: ['确定'] //按钮
	    },function(){
	    	layer.closeAll('dialog');
	    });
		return;
	}
	if($("#fzdxModelXLDJDMGD").val()=='0'){
		layer.confirm('泄漏源高度不能为0!', {
	         btn: ['确定'] //按钮
	    },function(){
	    	layer.closeAll('dialog');
	    });
		return;
	}
	
	var isRealTime = $("input[name='fzdxModelWeatherData']:checked").val();
	var params = {};
	params = getRealTimeOrSimulateData(isRealTime,params);
	if(!params){
		layer.confirm('请先确认事故点!', {
	         btn: ['确定'] //按钮
	    },function(){
	    	layer.closeAll('dialog');
	    });
		return;
	}
	var tempRType = $("#fzdxModelXLKZJ").val();
	params.dHeight = $("#fzdxModelXLDJDMGD").val();
	if(tempRType<20){
		params.RType = 0;
	}else if(tempRType<50){
		params.RType = 1;
	}else{
		params.RType = 2;
	}
	params.h2scon= Getnongdu($("#fzdxModelWRQTND").val());
	params.pipepress=Getyaqiang($("#fzdxModelGDYQ").val());
	var lonlat = ol.proj.transform(zhmnAccidentPoint, 'EPSG:3857', 'EPSG:4326');
	params.lng=lonlat[0];
	params.lat=lonlat[1];

	if(global_lonMin>params.lng || params.lng>global_lonMax){
//		layer.msg('事故点经度不在'+global_lonMin+'-'+global_lonMax+'范围内，没有地形数据！', {icon: 1, time: 1000});
		layer.confirm('事故点经度不在'+global_lonMin+'-'+global_lonMax+'范围内，没有地形数据！', {
	         btn: ['确定'] //按钮
	    },function(){
	    	layer.closeAll('dialog');
	    });
		return;
	}else if(global_latMin>params.lat || params.lat>global_latMax){
//		layer.msg('事故点伟度不在'+global_latMin+'-'+global_latMax+'范围内，没有地形数据！', {icon: 1, time: 1000});
		layer.confirm('事故点伟度不在'+global_latMin+'-'+global_latMax+'范围内，没有地形数据！', {
	         btn: ['确定'] //按钮
	    },function(){
	    	layer.closeAll('dialog');
	    });
		return;
	}
	zhmnAccidentPoint = ol.proj.transform(lonlat, 'EPSG:4326', 'EPSG:3857');
	if($("#fzdxModelXLL").val()!=""){
		params.dQ=parseFloat($("#fzdxModelXLL").val())*1000;
	}else{
		params.dQ=-1;
	}
	var tokenV = getTime();
	params.wtoken=tokenV;
	params.windWane="90";
	params.isLeft = false;
	$("#fzdxModelDiv").hide();
	gasSpreadController(params);
	var vaCWND = $("#fzdxModelWRQQCWND").val();
	if(vaCWND==0){
		shouTuLiKuoSan('H2S');
	}else if(vaCWND==1){
		shouTuLiKuoSan('SO2');
	}else if(vaCWND==2){
		shouTuLiKuoSan('CL');
	}else if(vaCWND==3){
		shouTuLiKuoSan('NO2');
	}else if(vaCWND==4){
		shouTuLiKuoSan('HF');
	}
}

function getTime(){
	var myDate = new Date();
    var TimeString = myDate.getFullYear() + "-" + (myDate.getMonth() + 1) + "-" + myDate.getDate() + "-" + myDate.getHours() + "-" + myDate.getMinutes() + "-" + myDate.getSeconds();    
    return TimeString;
}

function Getnongdu(nongdu) {
	var Num = 0;
	if (nongdu == 0) {
		Num = 0;
	}
	if (nongdu > 0 && nongdu <= 10) {
		Num = 1;
	}
	if (nongdu > 10 && nongdu <= 15) {
		Num = 2;
	}
	if (nongdu > 15 && nongdu <= 80) {
		Num = 3;
	}
	if (nongdu > 80 && nongdu <= 100) {
		Num = 4;
	}
	return Num;
}
function Getyaqiang(Num) {
    if (Num == 2) {
        Num = 0;
    }
    if (Num > 2 && Num <= 5) {
        Num = 1;
    }
    if (Num > 5 && Num <= 9) {
        Num = 2;
    }
    if (Num > 9) {
        Num = 3;
    }
    return Num;
}

/**
* 灭火剂用量计算
* @returns
*/
function mhjModelDivSubmit(){
	if(!checkIsEmpty('solidDiv')){
		layer.msg("请检查各个输入项是否输入", {time: 2000});
		return false;
	}
	if($("#mhjrsmj").val()=='0'){
		layer.msg("燃烧面积不能为0", {time: 2000});
		return false;
	}
	var params = {};
	var sidParam ="sid=solid_fires&areas=10&material=1";
	var url =gasUrl+"GasEmission.ashx";
	params.areas = $("#mhjrsmj").val();
    params.material = $("#mhjrsw").val();
    params.sid="solid_fires";
	$.ajax({
		type : "get",
		url : gasUrl+"GasEmission.ashx",
		dataType:"text",
		data: params,
		success : function(data) {	
			$("#grkrwResult").text(data);
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
		}
	});


}
/**
* 液化石油气用量计算
* @returns
*/
function mhjWarpFireSubmit(){
	if(!checkIsEmpty('liquefyingDiv')){
		layer.msg("请检查各个输入项是否输入", {time: 2000});
		return false;
	}
    var params = {};
	if($("#zhgzj").val()=='0'){
		layer.msg("着火罐直径不能为0", {time: 2000});
		return false;
	}
	if($("#ljgzj").val()=='0'){
		layer.msg("邻近罐直径不能为0", {time: 2000});
		return false;
	}
	if($("#ljgsl").val()=='0'){
		layer.msg("邻近罐数量不能为0", {time: 2000});
		return false;
	}
    params.diameter1 = $("#zhgzj").val();
    params.diameter2 = $("#ljgzj").val();
    params.nums = $("#ljgsl").val();
    params.sid="lpg_fires";
    var url =gasUrl+"GasEmission.ashx";
	$.ajax({
		type : "get",
		url : gasUrl+"GasEmission.ashx",
		dataType:"text",
		data: params,
		success : function(data) {	
			$("#yhsyqResult").text(data);
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
		}
	});
}

/**
* 油罐区计算
* @returns
*/
function mhjYgqDivSubmit(){
    var params = {};
    params.areas = $("#ygqdmrsmj").val();
    params.gTypes = $("#ygqzhglx").val();
    params.materialType = $("#ygqrswlx").val();
    params.gDiameters = $("#ygqzhgzj").val();
    params.gNums = $("#ygqzhgsl").val();
    params.cTypes = $("#ygqljglx").val();
    params.cDiameters = $("#ygqljgzj").val();
    params.cNums = $("#ygqljgsl").val();    
    params.sid="oil_fires";
    var url =gasUrl+"GasEmission.ashx";
	$.ajax({
		type : "get",
		url : gasUrl+"GasEmission.ashx",
		dataType:"text",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		data:params,
		success : function(data) {
			var resultP="泡沫"+data.split(";")[0];
			var resultX="消防水"+data.split(";")[1];
			$("#ygqReturn").text(resultP+";"+resultX);
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
		}
	});
}

//超高压爆炸模型
function cgybzModelDivSubmit(){
	var url=baseUrl+'/webApi/math/explosionHurt.jhtml.jhtml';
	var params = {};
	params.type1 = $("#cgybzModelBZLX").val();
	params.name1 = $("#cgybzModelBZWMC").val();
	params.pressure = $("#cgybzModelBZWYL").val();
    params.volume = $("#cgybzModelBZWTJ").val();
	params.r = $("#cgybzModelDistance").val();
	$("#rswfxModelDiv").hide();
	mathAjax(params,url,function(data){
		 var obj = data.data;
		 var heatFluxDensity = obj.heatFluxDensity;
	//	 alert(heatFluxDensity);
	});
}

var isFlat=true;

function cgybzModelDivSubmit2(){
	if(!checkIsEmpty('cgybzModelDiv')){
		layer.msg("请检查各个输入项是否输入", {time: 2000});
		return false;
	}
	if($("#cgybzModelBZWYL").val()=='0'){
		layer.msg("爆炸物的压力不能为0", {time: 2000});
		return false;
	}
	if($("#cgybzModelBZWTJ").val()=='0'){
		layer.msg("爆炸物体积不能为0", {time: 2000});
		return false;
	}
	var url=baseUrl+'/webApi/math/explosionHurtR.jhtml';
	var params = {};
	var type=$("#cgybzModelBZLX").val();
	if(type==5){
		params.type1 = 1;
		params.name1 = $("#cgybzModelBZWMC").val();
		params.pressure = $("#cgybzModelBZWYL5").val();
		var r = $("#cgybzModelBZWTJ5").val();
		var volume = 3.14*(parseFloat(r)/100)*(parseFloat(r)/100)*2;
		params.volume = volume.toFixed(1);
//		params.r = $("#cgybzModelDistance").val(); 
		params.hurt = 2;
	}else{
		params.type1 = $("#cgybzModelBZLX").val();
		params.name1 = $("#cgybzModelBZWMC").val();
		params.pressure = $("#cgybzModelBZWYL").val();
		params.volume = $("#cgybzModelBZWTJ").val();
//		params.r = $("#cgybzModelDistance").val(); 
		params.hurt = 2;
	}
	
	if(zhmnAccidentPoint.length<1){
		layer.msg("没有事故点信息，无法在地图上展示扩散范围！请设置事故点。", {time: 1000});
		return false;
	}else{
		mathAjax(params,url,function(data){
			$("#damagePeopleDiv").show();
			var obj = data.data;
			var pointDatas = obj.pointData;
			var items = [];
			for(var i=0;i<pointDatas.length;i++){
				var obj = {};
				obj.distance = pointDatas[i];
				obj.color = iteamsColor[i];
				items[i]=obj;
			}
				var point = [zhmnAccidentPoint[0],zhmnAccidentPoint[1]];
				drawBZ('cgybz',items,point,1,true);
				if(isFlat){
					point = [zhmnAccidentPoint[0],zhmnAccidentPoint[1]];
					centerAndZoom(point,17);
					isFlat = true;
				}
				$("#cgybzModelDiv").hide();
		});
	}
}

//燃烧物分析
function rswfxModelDivSubmit(){
	$("#closeZhmnRSWFXDivOut").show();
	if(!checkIsEmpty('rswfxModelDiv')){
		layer.msg("请检查各个输入项是否输入", {time: 2000});
		return false;
	}
	var isRealTime = $("input[name='rswfxModelWeatherData']:checked").val();
	var url=baseUrl+'/webApi/math/calcEmissionRate.jhtml';
	var params = {};
	 params = getRealTimeOrSimulateData(isRealTime,params);
	 if(!params){
			layer.msg('请先确认事故点', {icon: 1, time: 1000});
			return false;
		}
	 
	 if($("#rswfxModelDivCs").val()=='0'){
		 layer.msg('石油含硫率不能为0', {icon: 1, time: 1000});
			return false;
	 }
	 if($("#rswfxModelDivCn").val()=='0'){
		 layer.msg('石油含氮率不能为0', {icon: 1, time: 1000});
			return false;
	 }
	 
	 params.mt = $("#rswfxModelRSMJ").val()*ZHMNrswfxModelDivMt;
	 params.Cc = $("#rswfxModelDivCc").val();
	 params.Cs = $("#rswfxModelDivCs").val();
	 params.Cn = $("#rswfxModelDivCn").val();
	 $("#rswfxModelDiv").hide();
	 mathAjax(params,url,function(data){
		 var obj = data.data;
		 co2Rate = obj.co2Rate;
		 coRate = obj.coRate;
		 no2Rate = obj.no2Rate;
		 noRate = obj.noRate;
		 so2Rate = obj.so2Rate;
		 rswfxTubiaoChartShow3(isRealTime,coRate,co2Rate,no2Rate,noRate,so2Rate);
	 });
}

//热辐射,计算半径
function rfsModelSubmit(){
	/*var url=baseUrl+'/webApi/math/heatHurtCalParameters.jhtml';
	var params = {};
	params.S  = $("#rfsModelHZMJ").val();
	params.T0 = $("#rfsModelHJWD").val();
	params.time = $("#rfsModelSJ").val();
	params.Hc = $("#rfsModelYPMC").find("option:selected").attr("bh");
	params.Hv = $("#rfsModelYPMC").find("option:selected").attr("eh");
	params.cp = $("#rfsModelYPMC").find("option:selected").attr("sh");
	params.Tb = $("#rfsModelYPMC").find("option:selected").attr("tb");
	*/
	if(!checkIsEmpty('rfsshfw')){
		layer.msg("请检查各个输入项是否输入", {time: 2000});
		return false;
	}
	if($("#rswfxModelRSMJ").val()=='0'){
		 layer.msg('燃烧面积不能为0', {icon: 1, time: 1000});
			return false;
	 }
	 
	 if($("#rswfxModelDivCc").val()=='0'){
		 layer.msg('石油含碳率不能为0', {icon: 1, time: 1000});
			return false;
	 }
	var T0 = parseFloat($("#rfsModelHJWD").val());
	var Tb = parseFloat($("#rfsModelYPMC").find("option:selected").attr("tb"));
	var Hc = parseFloat($("#rfsModelYPMC").find("option:selected").attr("bh"));
	var Hv = parseFloat($("#rfsModelYPMC").find("option:selected").attr("eh"));
	var cp = parseFloat($("#rfsModelYPMC").find("option:selected").attr("sh"));

	var mf = 0;
	if (Tb > T0)
	{
		mf = 0.001 * Hc / (cp * (Tb - T0) + Hv);
	}
	else
	{
		mf = 0.001 * Hc / Hv;
	}
	
	fire_area = parseFloat($("#rfsModelHZMJ").val());		// 燃烧面积
	fire_speed = mf;	// 石油最大燃烧速度
	fire_Hc = Hc;	// 石油燃烧热
//	防护服热伤害计算
	zhmnQxzs_FHFjs();

	var url=baseUrl+'/webApi/math/hotTimeRadiusLine.jhtml';
	var params = {};
	params.area  = fire_area;
	params.Hc = Hc;
	params.speed = mf;
// 无防火服的曲线距离
	 mathAjax(params,url,function(data){
		 $("#rfsModelDiv").show();
		 var obj = data.data;
		 var mathGranularDLsit = obj.mathGranularDLsit;
		 zhmn_rsfxRbl(mathGranularDLsit[0],mathGranularDLsit[1],mathGranularDLsit[2]);
		 var paramsSyn = {};
		 paramsSyn.accType = "zhmn_rfsfx";
		 paramsSyn.content = obj;
		 send(JSON.stringify({"evetype":"zhmn","val":paramsSyn}));
	 });

}

/*
 * 地图结果显示隐藏
 * */
function zhmnQxzs_FHFjsShow(){
	if($("#zhmnQxzs_FHFjsShowR").prop('checked')){
		var zhmnQxzs_aqjl = $("#zhmnQxzs_aqjl").html();
		if(zhmnQxzs_aqjl==""){
			zhmnQxzs_aqjl = 0;
		}else{
			zhmnQxzs_aqjl = parseFloat(zhmnQxzs_aqjl).toFixed(2);
		}
		if(zhmnAccidentPoint.length<1){
			layer.msg("没有事故点信息，无法在地图上展示扩散范围！请设置事故点。", {time: 1000});
			return false;
		}else{
			 //防护服划线
			 var point = [zhmnAccidentPoint[0],zhmnAccidentPoint[1]];
			 var items=[{distance:zhmnQxzs_aqjl,color:'#FF0000'}];
			 var rotation = 0;
			 if(global_weatherData.windDirection){
				rotation = parseInt(global_weatherData.windDirection*64/360);
			 }
			 drawBZ('cgybz',items,point,1,true,rotation);
			 gotoPointView(zhmnAccidentPoint[0],zhmnAccidentPoint[1],18);
		}
	}else{
		clearBzy('cgybz'); 
	}
}

/*
 * 防护服热伤害计算
 * */
var fire_area = 0;		// 燃烧面积
var fire_speed = 0;		// 燃烧速度
var fire_Hc = 0;		// 燃烧热

function zhmnQxzs_FHFjs(){
	var url=baseUrl+'/webApi/math/hotTimeRadiusLineFFSF.jhtml';
	var params = {};
	params.tpp  = $("#zhmnQxzs_fhfTPP").val();
	params.time  = $("#zhmnQxzs_ycjgsj").val();
	params.area  = fire_area;
	params.speed  = fire_speed;
	params.Hc  = fire_Hc;
	if(zhmnAccidentPoint.length<1){
		layer.msg("没有事故点信息，无法在地图上展示扩散范围！请设置事故点。", {time: 1000});
		return false;
	}else{
		mathAjax(params,url,function(data){
			$("#zhmnQxzsOutDiv").show();
			var obj = data.data;
			var dieRadius = obj.dieRadius.toFixed(2);
			obj.dieRadius = dieRadius;
			$("#zhmnQxzs_aqjl").html(dieRadius);
			//防护服划线
			var point = [zhmnAccidentPoint[0],zhmnAccidentPoint[1]];
			obj.point = point;
			var items=[{distance:dieRadius,color:'#FF0000'}];
			var rotation = 0;
			if(global_weatherData.windDirection){
				rotation = parseInt(global_weatherData.windDirection*64/360);
			}
			obj.rotation = rotation;
			drawBZ('cgybz',items,point,1,true,rotation);
			gotoPointView(zhmnAccidentPoint[0],zhmnAccidentPoint[1],18);
			var paramsSyn = {};
			paramsSyn.accType = "zhmn_rfsfhf";
			paramsSyn.content = obj;
			send(JSON.stringify({"evetype":"zhmn","val":paramsSyn}));
		});
	}
}

//传参数（计算热辐射伤害），后 4 个参数查数据库
// S: 火灾面积，单位平方米(m2)
// T0: 环境温度，单位开氏度(K)
// Hc: 物质的燃烧热，单位焦耳每公斤(J/Kg)
// Hv: 物质的蒸发热，单位焦耳每公斤(J/Kg)
// cp: 物质的定压比热容，单位焦耳(每公斤每华氏度)(J/(Kg*K))
// Tb: 物质的沸点，单位开氏度(K)
// 返回值: true成功，false失败
//伤害半径
function rfsModelSubmitR(){
	if(!checkIsEmpty('rfsjssssh')){
		layer.msg("请检查各个输入项是否输入", {time: 2000});
		return false;
	}
	
	if($("#rfsModelJL").val()=='0'){
		layer.msg("离火场的距离不能为0", {time: 2000});
		return false;
	}
	
	if($("#rfsModelSJ").val()=='0'){
		layer.msg("受到伤害的时间不能为0", {time: 2000});
		return false;
	}
	var url=baseUrl+'/webApi/math/heatHurtParameters.jhtml';
	var params = {};
	var ss = $("#rfsModelHZMJ").val();
	params.S  = ss;
	params.T0 = $("#rfsModelHJWD").val();
	params.Hc = $("#rfsModelYPMC").find("option:selected").attr("bh");
	params.Hv = $("#rfsModelYPMC").find("option:selected").attr("eh");
	params.cp = $("#rfsModelYPMC").find("option:selected").attr("sh");
	params.Tb = $("#rfsModelYPMC").find("option:selected").attr("tb");
	params.time = $("#rfsModelSJ").val();
	var radius = $("#rfsModelJL").val();
	var rr = Math.sqrt(4 * ss / 3.1415926)/2;
	if(rr>radius){
		layer.open({
			  title: '危险提示',
			  content: '您的位置在火焰中，很危险!'
		});   
		return false;
	}	 
	params.length=radius;
	 mathAjax(params,url,function(data){
		 var obj = data.data;
		 dieRadius = obj.dieRadius;
		 secondLevelRadius = obj.secondLevelRadius;
		 oneLevelRadius = obj.oneLevelRadius;
		 //todo 调用三维画圆圈方法
		 var dieRadiusStr = "轻伤";
		 if(dieRadius>4.9){
			 dieRadiusStr = "致死";
		 }else if(secondLevelRadius>4.9){
			 dieRadiusStr = "重伤";
		 }else if(oneLevelRadius>4.9){
			 dieRadiusStr = "轻伤";
		 }else{
			 dieRadiusStr = "没有伤害"; 
		 }
		 $("#rfsModelJGZS").html(dieRadiusStr);
		 $("#rfsModelDivJsjg").show();
		 $("#rfsModelDivCont").hide();
	 });
	
}

//闪蒸
function szModelSubmit(){
	// Sh: 舍伍德数
	var url=baseUrl+'/webApi/math/calcEvaporate1.jhtml';
	var params = {};
	params.Cp  = $("#szModelDYBL").val();
	params.Hv =  $("#szModelZFR").val();
	params.wd = $("#szModelWD").val();
	params.Tb = $("#szModelYTFD").val();
	params.m = $("#szModelXLZL").val();
	params.t = $("#szModelSZSJ").val();
	params.A1 = $("#szModelYCMJ").val();
	params.T0 = $("#szModelHJWD").val();
	params.L = $("#szModelYCCD").val();
	params.K = $("#szModelDMDRXS").val();
	params.a = $("#szModelRKSXS").val();
	params.T = $("#szModelZFSJ").val();
	params.Nu = $("#szModelNSES").val();
	params.alpha = $("#szModelFZKSXS").val();
	params.Sh = "2.3";
	params.A = $("#szModelYCMJ").val();
	params.rou = $("#szModelYTMD").val();
	mathAjax(params,url,function(data){
		 var obj = data.data;
		 rate = obj.rate;
		 //todo 调用三维画圆圈方法
		// alert(rate);
	 });
}

/*
 * 获取泄漏源定位的实时数据
 * */
function changeXlydModelWeatherData(isRealTime){
	if(isRealTime==1){
		$("#xlydModelWeatherDataW").hide();
	}else{
		$("#xlydModelWeatherDataW").show();
	}
}

//泄露源定位
//223.557774995983,107.9671884,31.35408426,1.2,56.9063379124574,107.9674636,31.35398703,1.2,56.9063379124574,107.967364,31.35380759,1.2,223.557774995983,107.9670953,31.35391145,1.2
function xlydwModelSubmit(flag){
	if(!checkIsEmpty('xlydwModelDiv')){
		layer.msg("请检查各个输入项是否输入", {time: 3000});
		return false;
	}
	var isRealTime = $("input[name='xlydModelWeatherData']:checked").val();
	var url=baseUrl+'/webApi/math/leakSource.jhtml';
	var params = {};
	params = getRealTimeOrSimulateDataFS(isRealTime,params);
	if(!params){
		layer.msg('实时数据加载错误', {icon: 1, time: 3000});
		return false;
	}
	//获取界面数据
	var dis1 = "";
	if(isRealTime!=1){
		var cgq1Rate=$("#cgq1Rate").val();
		var cgq2Rate=$("#cgq2Rate").val();
		var cgq3Rate=$("#cgq3Rate").val();
		var cgq4Rate=$("#cgq4Rate").val();		
		var cgq1Lon=$("#cgq1Lon").val();
		var cgq2Lon=$("#cgq2Lon").val();
		var cgq3Lon=$("#cgq3Lon").val();
		var cgq4Lon=$("#cgq4Lon").val();

		var checkFlag= checkFourEsm27Lon(cgq1Rate,cgq2Rate,cgq3Rate,cgq4Rate,cgq1Lon,cgq2Lon,cgq3Lon,cgq4Lon);
		
		if(!checkFlag){
			return false;
		}
		
		var xlydwZhqtSelect = $("#xlydwZhqtSelect").val();
		if(xlydwZhqtSelect==0){
			rateSelect = 46;
		}else if(xlydwZhqtSelect==1){
			rateSelect = 34;
		}else if(xlydwZhqtSelect==2){
			rateSelect = 16;
		}else if(xlydwZhqtSelect==3){
			rateSelect = 28;
		}else if(xlydwZhqtSelect==4){
			rateSelect = 16;
		}else if(xlydwZhqtSelect==5){
			rateSelect = 20;
		}
		cgq1Rate = ppm_to_gm3(cgq1Rate,rateSelect);
		cgq2Rate = ppm_to_gm3(cgq2Rate,rateSelect);
		cgq3Rate = ppm_to_gm3(cgq3Rate,rateSelect);
		cgq4Rate = ppm_to_gm3(cgq4Rate,rateSelect);
		
		params.data  = cgq1Rate+","+cgq1Lon+",1.2," +
		               cgq2Rate+","+cgq2Lon+",1.2," +
		               cgq3Rate+","+cgq3Lon+",1.2," +
		               cgq4Rate+","+cgq4Lon+",1.2";
	}else{
		/**
		 * 自动获取数据
		 * **/
//		var resultN = getWeatherData();
//		if(!resultN){
//			return false;
//		}else{
//			params.data = resultN;
//		}
		var resultN = getRealtimeDataParams();
		if(!resultN){
			return false;
		}else{
			params.data = resultN;
		}
	}

	$("#xlydwModelDivContent").hide();
	$("#xlydwModelDivHeadBox span").text("展开");
	$("#xlydwModelDivHeadBox i").addClass("stopUpIconDown");
	$("#xlydwModelDivJsjg").show();
	$("#xlydwxlydModelJSJGXLL").html("计算中......");
	mathAjax(params,url,function(data){
		 var obj = data.data;
		 var sX = obj.sX;
		 var sY = obj.sY;
		 var sH = obj.sH;
		 var lsQ = obj.lsQ;
		 var lsQt = (lsQ/1000).toFixed(2);
		 $("#xlydwxlydModelJSJGXLL").html(lsQt+"(KG/S)");	 
		 if(flag=="dw"){
			 //泄露源定位 
//			 var point = ol.proj.transform([sX,sY], 'EPSG:4326', 'EPSG:3857');    //经纬度转米
//			 gotoPointView(point[0],point[1],18);
			 zhmnAccidentPoint = ol.proj.transform([sX,sY], 'EPSG:4326', 'EPSG:3857');
			 gotoPointView(zhmnAccidentPoint[0],zhmnAccidentPoint[1],17);
			 clearPointByXyLayer();
			 addPointByXy(zhmnAccidentPoint,12345,'/mapIcon/mapFixedIcon/sgd_dw.png');
		 }
	 },function(){
		 $("#xlydwxlydModelJSJGXLL").html("数据获取失败...");
		 $("#xlydwModelDivJsjg").show();
	 });	
	
}

/*
 * 自动获取4个传感器的数据
 * @返回params.data
 * */
function getRealtimeDataParams(){
		var resultN = '';  
		/**
		 * 判断是不是四个以上传感器
		 * **/
		if(global_realtimeData.size<4){
			layer.msg("至少需要接入 4 个气体检测仪", {time: 1000});
			return false;
		}
		if(global_gasIsAlertNameData == ""){
			layer.msg("气体无告警", {time: 1000});
			return false;
		}
		/**
		 * 判断有无告警
		 * **/
//		var location1 = "";
//		var location2 = "";
//		var location3 = "";
//		var location4 = "";
//		var location5 = "";
//		var location6 = "";
//		var location7 = "";
//		var location8 = "";
//		var indexL=0;
		
		var cgqs = [];
		
		global_realtimeData.forEach(function (element, index, array) {
		    // element: 指向当前元素的值
		    // index: 指向当前索引
		    // array: 指向Array对象本身
			
			var cgq = new Object();
			cgq.lng = element.pointLL[0];
			cgq.lat = element.pointLL[1];
			var gasIsAlertData = element.realDataList;
			var gasIsAlertDataLength = gasIsAlertData.length;
			if(gasIsAlertDataLength>0){
				for(var i=0;i<gasIsAlertDataLength;i++){
					var gasName = gasIsAlertData[i].gasName;
					if(gasName==global_gasIsAlertNameData){
						cgq.rate=gasIsAlertData[i].gasValue;
					}
				}
				cgqs.push(cgq);
			}else{
				return false;
			} 
			
//			if(indexL==0){
//				location1=element.pointLL[0];
//				location2=element.pointLL[1];
//				var gasIsAlertData = element.realDataList;
//				  var gasIsAlertDataLength = gasIsAlertData.length;
//				  if(gasIsAlertDataLength>0){
//					  for(var i=0;i<gasIsAlertDataLength;i++){
//						  var gasName = gasIsAlertData[i].gasName;
//						  if(gasName==global_gasIsAlertNameData){
//							  cgq1Rate=gasIsAlertData[i].gasValue;
//						  }
//					  }
//				  }else{
//					  return false;
//				  }
//			}else if(indexL==1){
//				location3=element.pointLL[0];
//				location4=element.pointLL[1];
//				var gasIsAlertData = element.realDataList;
//				  var gasIsAlertDataLength = gasIsAlertData.length;
//				  if(gasIsAlertDataLength>0){
//					  for(var i=0;i<gasIsAlertDataLength;i++){
//						  var gasName = gasIsAlertData[i].gasName;
//						  if(gasName==global_gasIsAlertNameData){
//							  cgq2Rate=gasIsAlertData[i].gasValue;
//						  }
//					  }
//				  }else{
//					  return false;
//				  }
//			}else if(indexL==2){
//				location5=element.pointLL[0];
//				location6=element.pointLL[1];
//				var gasIsAlertData = element.realDataList;
//				  var gasIsAlertDataLength = gasIsAlertData.length;
//				  if(gasIsAlertDataLength>0){
//					  for(var i=0;i<gasIsAlertDataLength;i++){
//						  var gasName = gasIsAlertData[i].gasName;
//						  if(gasName==global_gasIsAlertNameData){
//							  cgq3Rate=gasIsAlertData[i].gasValue;
//						  }
//					  }
//				  }else{
//					  return false;
//				  }
//			}else if(indexL==3){
//				location7=element.pointLL[0];
//				location8=element.pointLL[1];
//				var gasIsAlertData = element.realDataList;
//				  var gasIsAlertDataLength = gasIsAlertData.length;
//				  if(gasIsAlertDataLength>0){
//					  for(var i=0;i<gasIsAlertDataLength;i++){
//						  var gasName = gasIsAlertData[i].gasName;
//						  if(gasName==global_gasIsAlertNameData){
//							  cgq4Rate=gasIsAlertData[i].gasValue;
//						  }
//					  }
//				  }else{
//					  return false;
//				  }
//			}
//			indexL++;
		});
		
	var checkFlag= checkFourEsm27Lon(cgqs);
	if(!checkFlag){
		return false;
	}
	
	var global_gasIsAlertM = getRealTimeGlobal_gasIsAlertM();
	
	for(var i=0; i < cgqs.length; i++){
		cgqs[i].rate = ppm_to_gm3(cgqs[i].rate,global_gasIsAlertM);
	}
	
	for(var i=0; i < cgqs.length; i++){
		if(i != cgqs.length - 1){
			resultN += cgqs[i].rate + "," + cgqs[i].lng + "," + cgqs[i].lat + ",1.2,"
		}else{
			resultN += cgqs[i].rate + "," + cgqs[i].lng + "," + cgqs[i].lat + ",1.2"
		}
		
	}
	
//    var cgq1Lon = location1+","+location2;
//    var cgq2Lon = location3+","+location4;
//    var cgq3Lon = location5+","+location6;
//    var cgq4Lon = location7+","+location8;
//	var checkFlag= checkFourEsm27Lon(cgq1Rate,cgq2Rate,cgq3Rate,cgq4Rate,cgq1Lon,cgq2Lon,cgq3Lon,cgq4Lon);
//	
//	if(!checkFlag){
//		return false;
//	}
//	
//	cgq1Rate = ppm_to_gm3(cgq1Rate,global_gasIsAlertM);
//	cgq2Rate = ppm_to_gm3(cgq2Rate,global_gasIsAlertM);
//	cgq3Rate = ppm_to_gm3(cgq3Rate,global_gasIsAlertM);
//	cgq4Rate = ppm_to_gm3(cgq4Rate,global_gasIsAlertM);
//	
//	resultN = cgq1Rate+","+cgq1Lon+",1.2," +
//	               cgq2Rate+","+cgq2Lon+",1.2," +
//	               cgq3Rate+","+cgq3Lon+",1.2," +
//	               cgq4Rate+","+cgq4Lon+",1.2";
	return resultN;
}

/*
 * 判断4个传感器的位置和读数
 * */
function checkFourEsm27Lon(cgqs){
	
	for(var i=0; i < cgqs.length; i++){
		if(cgqs[i].rate === ''){
			layer.msg("传感器100"+ (i+1) +"浓度不能为空", {time: 1000});
			return false;
		}
	}
	
//	if(cgq1Rate===''){
//		layer.msg("传感器1001浓度不能为空", {time: 1000});
//		return false;
//	}
//	if(cgq2Rate===''){
//		layer.msg("传感器1002浓度不能为空", {time: 1000});
//		return false;
//	}
//	if(cgq3Rate===''){
//		layer.msg("传感器1003浓度不能为空", {time: 1000});
//		return false;
//	}
//	if(cgq4Rate===''){
//		layer.msg("传感器1004浓度不能为空", {time: 1000});
//		return false;
//	}
//	var location1 =cgq1Lon.split(",")[0];
//	var location2 =cgq1Lon.split(",")[1];
//	var location3 =cgq2Lon.split(",")[0];
//	var location4 =cgq2Lon.split(",")[1];
//	var location5 =cgq3Lon.split(",")[0];
//	var location6 =cgq3Lon.split(",")[1];
//	var location7 =cgq4Lon.split(",")[0];
//	var location8 =cgq4Lon.split(",")[1];
	
	for(var i=0; i < cgqs.length; i++){	
		if(i != cgqs.length-1 ){
			for(var j = i+1; j < cgqs.length; j++){
				var dis = getShortDistance(cgqs[i].lng,cgqs[i].lat,cgqs[j].lng,cgqs[j].lat);
				console.log(i+","+i+","+j+","+j+";;"+dis);
				console.log(cgqs[i].lng,cgqs[i].lat,cgqs[j].lng,cgqs[j].lat);
				if(10>dis||dis>1000){
					layer.msg('传感器之前得距离要在10到1000米之间', {icon: 1, time: 2000});
					return false;
				}
			}
		}
	}
//	var dis1 = getShortDistance(location1,location2,location3,location4);
//	if(10>dis1||dis1>1000){
//		layer.msg('传感器之前得距离要在10到1000米之间', {icon: 1, time: 2000});
//		return false;
//	}
//	var dis2 =getShortDistance(location1,location2,location5,location6);
//	if(10>dis2||dis2>1000){
//		layer.msg('传感器之前得距离要在10到1000米之间', {icon: 1, time: 2000});
//		return false;
//	}
//	var dis3 =getShortDistance(location1,location2,location7,location8);
//	if(10>dis1||dis1>1000){
//		layer.msg('传感器之前得距离要在10到1000米之间', {icon: 1, time: 2000});
//		return false;
//	}
//	var dis4 =getShortDistance(location3,location4,location5,location6);
//	if(10>dis1||dis1>1000){
//		layer.msg('传感器之前得距离要在10到1000米之间', {icon: 1, time: 2000});
//		return false;
//	}
//	var dis5 =getShortDistance(location3,location4,location7,location8);
//	if(10>dis1||dis1>1000){
//		layer.msg('传感器之前得距离要在10到1000米之间', {icon: 1, time: 2000});
//		return false;
//	}
//	var dis6 =getShortDistance(location5,location6,location7,location8);
//	if(10>dis1||dis1>1000){
//		layer.msg('传感器之前得距离要在10到1000米之间', {icon: 1, time: 2000});
//		return false;
//	}
	return true;
}
/**
 * 天气信息输入提交事件
 * @returns
 */
function commonTqSubmit(){
	if(!checkIsEmpty('tqxxModelDiv')){
		layer.msg("请检查各个输入项是否输入", {time: 1000});
		return false;
	}
	var tqxxModelBW = $("#tqxxModelBW").val();
	var tqxxModelQYY = $("#tqxxModelQYY").val();
	var tqxxFS = $("#tqxxFS").val();
	var tqxxFX = $("#tqxxFX").val();
	var tqxxSD = $("#tqxxSD").val();
	var tqxxWD = $("#tqxxWD").val();
	var tqxxYQ = $("#tqxxYQ").val();
	if(tqxxYQ == '0'){
		layer.msg("压强不能为0", {time: 1000});
		return false;
	}
	$("#commonTQFS").text(tqxxFS);
	var as = DQWDDValue(tqxxModelBW, tqxxModelQYY, parseFloat(tqxxFS));
	$("#commonAs").text(as);

	var fx = parseFloat(tqxxFX);
	if(fx <= 11.25){
		$("#commonTQFX").text("北");
	}else if(fx <= 33.75){
		$("#commonTQFX").text("北北东");
	}else if(fx <= 56.25){
		$("#commonTQFX").text("东北");
	}else if(fx <= 78.75){
		$("#commonTQFX").text("东东北");
	}else if(fx <= 101.25){
		$("#commonTQFX").text("东");
	}else if(fx <= 123.75){
		$("#commonTQFX").text("东东南");
	}else if(fx <= 146.25){
		$("#commonTQFX").text("东南");
	}else if(fx <= 168.75){
		$("#commonTQFX").text("南南东");
	}else if(fx <= 191.25){
		$("#commonTQFX").text("南");
	}else if(fx <= 213.75){
		$("#commonTQFX").text("南南西");
	}else if(fx <= 236.25){
		$("#commonTQFX").text("西南");
    }else if(fx <= 258.75){
		$("#commonTQFX").text("西西南");
	}else if(fx <= 281.25){
		$("#commonTQFX").text("西");
	}else if(fx <= 303.75){
		$("#commonTQFX").text("西西北");
	}else if(fx <= 326.25){
		$("#commonTQFX").text("西北");
	}else if(fx <= 348.75){
		$("#commonTQFX").text("北北西");
	}else{
		$("#commonTQFX").text("北");
	}

	$("#commonTQFXValue").val(tqxxFX);
	$("#commonTQWD").text(tqxxWD);
	$("#commonTQSD").text(tqxxSD);
	$("#commonTQYQ").text(tqxxYQ);
	
	$("#tqxxModelDiv").hide();
	$("#weatherMsgModelDiv").show();
}

function editTqxx(){
  $("#tqxxModelDiv").show();
}

//扩散
//as: 大气稳定度，整型，取值 1,2,3,4,5,6 与大气稳定度取值表的对应关系 1=A, 2=B, 3=C, 4=D, 5=E, 6=F
// ws: 环境风速，一般取地面 10m 高处的平均风速，单位m/s
// wd: 风向，单位度
// gd: 污染气体在常温下的浓度，单位 kg/m3
// h: 泄漏点离地面的高度
// dq: 泄漏源强度，kg/s
// 返回值: true成功，false失败 
var  numTime = 70;
var xlkInterval = null;
function xlksModelsSubmit(){
	if(!checkIsEmpty('xlksModelDiv')){
		layer.msg("请检查各个输入项是否输入", {time: 1000});
		return false;
	}
	if($("#rfsModelXLDJDMGD").val()=='0'){
		layer.msg("泄漏源高度不能为0", {time: 1000});
		return false;
	}
	if($("#rfsModelXLL").val()=='0'){
		layer.msg("泄漏率不能为0", {time: 1000});
		return false;
	}
	if($("#rfsModelZHFSSJ").val()=='0'){
		layer.msg("发生时间不能为0", {time: 1000});
		return false;
	}
	var time = $("#rfsModelZHFSSJ").val();
	setXlksModelsSubmit(time);
	$("#xlksModelDiv").hide();
}
function cleanxlkInterval(){
	clearInterval(xlkInterval);
}
function ZHMNClosePanel(oid){
	$("#"+oid).hide();
	cleanxlkInterval();
}
function setXlksModelsSubmit(time){
	numTime = time;
	var url=baseUrl+'/webApi/math/gaussionModelParameters.jhtml';
	var isRealTime = $("input[name='rfsModelWeatherData']:checked").val();
	var params = {};
	var rfsModelFS = "";
	params = getRealTimeOrSimulateData(isRealTime,params);
	if(!params){
		layer.msg('请先确认事故点', {icon: 1, time: 1000});
		return false;
	}
	params.isRealTime = isRealTime;
	let value = $("#rfsModelWRQQCWND").find("option:selected").attr("value");
	params.gasType = $("#rfsModelWRQQCWND").find("option:selected").attr("che");
	if(value==3){
		shouTuLiKuoSan("HF");	
	}else{
		shouTuLiKuoSan(params.gasType);	
	}
	params.h = $("#rfsModelXLDJDMGD").val();
	params.dq =$("#rfsModelXLL").val();
	params.fCurrentTime=$("#rfsModelZHFSSJ").val()*60;
	sjzParams = params;
	sjzUrl = url;
	mathAjax(params,url,function(data){
			var obj = data.data;
			var layer = layerIsExist('buildGeometryLayer',global.map);
			if(layer){
				clearBuildGeometryLayer();
			}
			var pointDataList = obj.mathGranularVoLsit;
			for(var i=0;i<pointDataList.length;i++){
				var pointsObj = {};
				pointsObj.coords = eval('(' + pointDataList[i] + ')');
				pointsObj.isBorder = false;
				pointsObj.borderColor = '';
				pointsObj.fillColor = iteamsColor[i];
				pointsObj.fillTransparency = '0.6';
				buildGeometryByCoords(pointsObj.coords,pointsObj.isBorder,pointsObj.borderColor,pointsObj.fillColor,pointsObj.fillTransparency);
			}
			$("#zhmnKSSJDivOut").show();
			ksmnTypeDatas.put(params.gasType+params.fCurrentTime,obj);
	 });
	drawHighLine('drawHighLine',zhmnAccidentPoint);
	gotoPointView(zhmnAccidentPoint[0],zhmnAccidentPoint[1],18);
	//gotoPointView(zhmnAccidentPoint[0],zhmnAccidentPoint[1],0);  张修改
}
//实时数据获取大气稳定度
function getASValue(fs){
	var as = 2;
	var myDate = new Date();
	var hour= myDate.getHours();  
	if(8<=hour && hour<=19){
		if (fs <= 2){
			as = 1;
		}
	}
	return as;
}

//大气稳定度获取
function DQWDDValue(rfsModelBW,rfsModelQYY,rfsModelFS){
	
	var as = 2;
	if (rfsModelBW == 0 && rfsModelFS <= 2){
		as = 1;
	}
	return as;
	
	var ret = 1;
	if (rfsModelBW == 1)	//晚上
	{
		if (rfsModelQYY == 2)	// 阴天
			ret = 4;
		else
		{
			if (rfsModelFS < 2)
				ret = 6;
			else if (rfsModelFS < 3)
				ret = 5;
			else
				ret = 4;
		}
	}
	else if (rfsModelBW == 0)	//白天
	{
		if (rfsModelQYY == 2)	// 阴天
			ret = 4;
		else if (rfsModelQYY == 1)	// 多云
		{
			if (rfsModelFS < 2)
				ret = 2;
			else if (rfsModelFS < 5)
				ret = 3;
			else
				ret = 4;
		}
		else if (rfsModelQYY == 0)	// 晴天
		{
			if (rfsModelFS < 2)
				ret = 1;
			else if (rfsModelFS < 5)
				ret = 2;
			else
				ret = 3;
		}
	}
	return ret;
}

//管道泄漏模型
function gdxlModelDivSubmit(num){
//泄漏率计算
	var url=baseUrl+'/webApi/math/calcLiquidLeak.jhtml';
	if(num==3){
		url=baseUrl+'/webApi/math/calcLiquidLeakArea2.jhtml';
	}
	var params = {};
	params.rou  = $("#gdxlModelYTMD").val();
	params.s  = $("#gdxlModelYLKMJ").val();
	params.p  = $("#gdxlModelRQNYQ").val();
	params.h  = $("#gdxlModelYYGD").val();
	params.m  = $("#gdxlModelXLZL").val();
	params.t  = $("#gdxlModelXLSJ2").val();
	mathAjax(params,url,function(data){
		 var obj = data.data;
		 var radius = obj.radius;
		 var rate = obj.rate;
		 //todo 调用三维画圆圈方法
		 // 半径R*π*R 计算面积
		// alert(radius+":"+rate);
	 });
}

var DEF_PI = 3.14159265359; // PI
var DEF_2PI = 6.28318530712; // 2*PI
var DEF_PI180 = 0.01745329252; // PI/180.0
var DEF_R = 6370693.5; // radius of earth
function getShortDistance(lon1, lat1, lon2, lat2) {
    var ew1, ns1, ew2, ns2;
    var dx, dy, dew;
    var distance;
    // 角度转换为弧度
    ew1 = lon1 * DEF_PI180;
    ns1 = lat1 * DEF_PI180;
    ew2 = lon2 * DEF_PI180;
    ns2 = lat2 * DEF_PI180;
    // 经度差
    dew = ew1 - ew2;
    // 若跨东经和西经180 度，进行调整
    if (dew > DEF_PI)
        dew = DEF_2PI - dew;
    else if (dew < -DEF_PI)
        dew = DEF_2PI + dew;
    dx = DEF_R * Math.cos(ns1) * dew; // 东西方向长度(在纬度圈上的投影长度)
    dy = DEF_R * (ns1 - ns2); // 南北方向长度(在经度圈上的投影长度)
    // 勾股定理求斜边长
    distance = Math.sqrt(dx * dx + dy * dy).toFixed(0);
  //  alert(distance)
    return distance;
}

function changegJfsxlydModelWeatherData(isRealTime){
	if(isRealTime==1){
		$("#gjfsxlydModelWeatherDataW").hide();
	}else{
		$("#gjfsxlydModelWeatherDataW").show();
	}
}

var yygqfyModel = {};
yygqfyModel.YYRCBSD='';
yygqfyModel.RSXSD='';
//原油罐区喷溅时间
function yygqfyModelDivSubmit(){
	if(!checkIsEmpty('yygqfyModelDiv')){
		layer.msg("请检查各个输入项是否输入", {time: 2000});
		return false;
	}
	if($("#yygqfyModelCGZYMGD").val()=='0'){
		layer.msg("页面高度不能为0", {time: 2000});
		return false;
	}
	if($("#yygqfyModelHSL").val()=='0'){
		layer.msg("含水率不能为0", {time: 2000});
		return false;
	}
	if($("#yygqfyModelCYZSDCGD").val()=='0'){
		layer.msg("水垫层高度不能为0", {time: 2000});
		return false;
	}
	if($("#yygqfyModelGTZJ").val()=='0'){
		layer.msg("罐体直径不能为0", {time: 2000});
		return false;
	}
	if($("#yygqfyModelYYRCBSD").val()=='0'){
		layer.msg("热波传播速度不能为0", {time: 2000});
		return false;
	}
	if($("#yygqfyModelRSXSD").val()=='0'){
		layer.msg("燃烧的线速度不能为0", {time: 2000});
		return false;
	}
	
	
	var r = parseFloat($("#yygqfyModelGTZJ").val())/2;
	var H = $("#yygqfyModelCGZYMGD").val();
	var hsl = $("#yygqfyModelHSL").val();
	var h = hsl*H/100;

	$("#yygqfyModelCYZSDCGD").val(h.toFixed(3));
	
	var addressID = $("input[name='yygqfyModelSbradio']:checked").val();
	if(H>h){
		var shortTime=0;
		var hs = $("#yygqfyModelYYRCBSD").val();
		var ss = $("#yygqfyModelRSXSD").val();
		shortTime = time_qzyy(H,h,parseFloat(hs),parseFloat(ss),addressID);
		$("#yygqfyModelYJFSFTSJ").html(shortTime.toFixed(2));
		$("#yygqfyModelDivJsjg").show();
	}else{
		layer.msg("水垫层的高度不能大于液面高度", {time: 3000});
	}
	    $("#yygqfyModelDivConTent").hide();
	
}

function yygqfyModelCGZYMGDChange()
{
	var H = $("#yygqfyModelCGZYMGD").val();
	var hsl = $("#yygqfyModelHSL").val();
	var h = hsl*H/100;
	$("#yygqfyModelCYZSDCGD").val(h.toFixed(3));
}

function yygqfyModelHSLChange()
{
	var hsl = $("#yygqfyModelHSL").val();
	if(hsl>0.5){
		$("#errorCheck").show();
	}
	var speed = rb_speed(hsl);
	speed = speed * 2;
	$("#yygqfyModelYYRCBSD").val(speed);

	var H = $("#yygqfyModelCGZYMGD").val();
	var h = hsl*H/100;
	$("#yygqfyModelCYZSDCGD").val(h.toFixed(3));
}

function changeYygqfyModelYPMC(divName1,divName2,divName3){
	var flage = $("#"+divName1).val();
	if(flage==0){
		$("#"+divName2).val("0.38-0.90");
		$("#"+divName3).val("0.10-0.46");
	}else if(flage==1){
		$("#"+divName2).val("0.43-0.127");
		$("#"+divName3).val("0.10-0.46");
	}else if(flage==2){
		$("#"+divName2).val("0.50-0.75");
		$("#"+divName3).val("0.075-0.13");
	}else if(flage==3){
		$("#"+divName2).val("0.03-0.127");
		$("#"+divName3).val("0.075-0.13");
	}else if(flage==4){
		$("#"+divName2).val("0");
		$("#"+divName3).val("0.125-0.20");
	}else if(flage==5){
		$("#"+divName2).val("0");
		$("#"+divName3).val("0.15-0.30");
	}
}

function changeYygqfyModelYPMC1(divName1,divName2,divName3){
	var flage = $("#"+divName1).val();
	var mixValue='';
	var mixValue2='';
	if(flage==0){
		$("#"+divName2).html("0.38-0.90");
		$("#"+divName3).html("0.10-0.46");
		mixValue = 0.38;
		mixValue2 = 0.10;
	}else if(flage==1){
		$("#"+divName2).html("0.43-0.127");
		$("#"+divName3).html("0.10-0.46");
		mixValue = 0.43;
		mixValue2 = 0.10;
	}else if(flage==2){
		$("#"+divName2).html("0.50-0.75");
		$("#"+divName3).html("0.075-0.13");
		mixValue = 0.5;
		mixValue2 = 0.075;
	}else if(flage==3){
		$("#"+divName2).html("0.03-0.127");
		$("#"+divName3).html("0.075-0.13");
		mixValue = 0.03;
		mixValue2 = 0.075;
	}else if(flage==4){
		$("#"+divName2).html("0");
		$("#"+divName3).html("0.125-0.20");
		mixValue = 0;
		mixValue2 = 0.125;
	}else if(flage==5){
		$("#"+divName2).html("0");
		$("#"+divName3).html("0.15-0.30");
		mixValue = 0;
		mixValue2 = 0.15;
	}
	$("#ZHMNyygqfyModelYYRCBSD").val(mixValue);
	$("#ZHMNyygqfyModelRSXSD").val(mixValue2);
}
var klwcjInterval = null;
function klwcjModelSubmit(){
	if(!checkIsEmpty('klwcjModelDiv')){
		layer.msg("请检查各个输入项是否输入", {time: 2000});
		return false;
	}
	var isRealTime = $("input[name='klwcjModelWeatherData']:checked").val();
	var url=baseUrl+'/webApi/math/calcEmissionRate.jhtml';
	var params = {};
	 params = getRealTimeOrSimulateData(isRealTime,params);
	 if(!params){
			layer.msg('请先确认事故点', {icon: 1, time: 1000});
			return false;
		}
	var time = $("#klwcjModelFSSC").val();
	setKlwcjModelSubmit($("#klwcjModelFSSC").val());
	$("#klwcjModelDiv").hide();
}

function cleanklwcjInterval(){
	$("#klwcjBTN").hide();
	clearInterval(klwcjInterval);
}

//颗粒沉降物
function setKlwcjModelSubmit(fCurrentTime){
	numTime = fCurrentTime;
	// 传参数
	// as: 大气稳定度，整型，取值 1,2,3,4,5,6 与大气稳定度取值表的对应关系 1=A, 2=B, 3=C, 4=D, 5=E, 6=F
//	var url=baseUrl+'/webApi/math/granularPrecipitate';
	var url=baseUrl+'/webApi/math/granularPrecipitate.jhtml';
	var isRealTime = $("input[name='klwcjModelWeatherData']:checked").val();
	var params = {};
	params = getRealTimeOrSimulateData(isRealTime,params);
	
	params.h = $("#klwcjModelGD").val();
	params.dq = $("#klwcjModelKLWYQD").val();
	params.gd = 1;
	//最佳值2,8,2,12,2,5,0.0001,0.001,0.1
	//3个等高线一起传
//	params.t = $("#klwcjModelT").val();
	params.t = "0.0001,0.01,0.1";
	//fCurrentTime 单位秒
	params.fCurrentTime = fCurrentTime*60;
	mathAjax(params,url,function(data){
			var obj = data.data;
			$("#jisuanjieguo").html("...");
			var layer = layerIsExist('buildGeometryLayer',global.map);
			if(layer){
				clearBuildGeometryLayer();
			}
			var pointDataList = obj.mathGranularVoLsit;
			for(var i=0;i<pointDataList.length;i++){
				var pointsObj = {};
				pointsObj.coords = eval('(' + pointDataList[i] + ')');
				pointsObj.isBorder = false;
				pointsObj.borderColor = '';
				pointsObj.fillColor = iteamsColorKL[i];
				pointsObj.fillTransparency = '0.6';
				buildGeometryByCoords(pointsObj.coords,pointsObj.isBorder,pointsObj.borderColor,pointsObj.fillColor,pointsObj.fillTransparency);
			}
			drawHighLine('drawHighLine',zhmnAccidentPoint);
			gotoPointView(zhmnAccidentPoint[0],zhmnAccidentPoint[1],18);
	 });
}

//同步获取topic方法
function disasterOnclickTest(){
	$.ajax({
		type : "post",
		url : baseUrl+"/webApi/math/gaussionModelParametersSyn.jhtml",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {					
			alert("成功开启实时检测，若无实时数据，请检查设备配置");
		},error: function(request) {
			alert("网络错误");
		}
	});
}

//画圆的方法
/**
 * 爆炸接口显示
 * @param id 主键
 * @param point[x,y]
 * @param items[{distance:1000,color:'#FF0000'},..]
 * @param image 中心点的图片
 * @param isFill 是否需要填充色
 * @param showDistance 是否显示半径距离
 * */
function drawHighLine(oid,point){
	var items = [];
	for(var i=100;i<1001;i+=100){
		items.push({distance:i,color:'#525756'});
	}
//	 var items = [{distance:data,color:'#FF0000'}]; 69ffe4
//	 var items = [{distance:data[0],color:'#FF0000'},{distance:data[1],color:'#E4FF00'},{distance:data[2],color:'#00FF30'}];
	 var image = baseUrl+"/images/start.png";
	addBzy(oid,point,items,image,1,true,getWindDirection());
}

//灾害分析公共方法
function mathAjax(params,url,callback,errorCallBack){
	doCallback = function (fn, args) {
		return fn.apply(this, args);
	}
	params.type=params.recordType;
	$.ajax({
		type:'POST',
		url: url,
		headers: {"Content-Type": "application/json;charset=utf-8"},
		data: JSON.stringify(params),
		dataType:'JSON',
		success: function(data){
			if(data.httpCode==200){
				if(callback){
					doCallback(callback,[data]);
				}
			}else{
				layer.msg("初始化失败", {time: 1000});
				if(errorCallBack){
					doCallback(errorCallBack);
				}
			}
		},
		erro:function(){
			if(errorCallBack){
				doCallback(errorCallBack);
			}
		}
	});
}

//公式计算
function exprdetail(id,name,desc){
	$("#originalexpr").text("");
	$("#exprval").html("");
	$("#exprresult").text("");
	$("#contentFormulaName").text(name);
	$("#spreadTipsText").text(desc)
	$.ajax({
		type : "post",
		url : baseUrl+"/webApi/expr/qryDetail.jhtml",
		data: JSON.stringify(id),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				$("#originalexpr").text("");
				$("#exprval").html("");
				$("#originalexpr").text(data.data.expr);
				$("#exprhidden").val(data.data.expr);
				$("#scalehidden").val(data.data.scale);
				$("#unithidden").val(data.data.unit);
				$("#dressingMethodhidden").val(data.data.dressingMethod);
				
				$.each(data.data.exprParams,function(index,ele){
					var html = '<div class="ptContBar ca cf" style="margin-left:100px;">';
						html+= '<label class="ptContA fl">' + ele.name + ' ( ' + ele.value + ' ) ' + ': </label>';
						html+='<div class="ptContIn fl" style="width:90px;" >';
						if(ele.type==2){
							if(ele.childOption&&ele.childOption.length>0){
								html+='<select class="select_class" name="' + ele.name + '(' + ele.value + ')'+'" empty="' + ele.empty +'" expr="' + ele.value +'" >';
								html+='<option value="" >请选择</option>';
								for(var i=0;i<ele.childOption.length;i++){
									var curr =ele.childOption[i];
									if(curr.defaultVal==1){
										html+='<option value="' + curr.optionVal + '" selected = "selected">' + curr.attr + '</option>';
									}else{
										html+='<option value="' + curr.optionVal + '" >' + curr.attr + '</option>';
									}
									
								}
								html+='</select>';
							}
						}else{
							html+='<input class="select_class" name="' + ele.name + '(' + ele.value + ')'+'"  empty="' + ele.empty +'" expr="' + ele.value +'" type="text" style="width:70px"/>';
						}
						
						html+='</div>';
						if(ele.realTime==1){
							html+= '<input name="' + ele.value + '" onclick="exprselect(\'' + ele.value + '\')" type="button" value="选择" class="ptContBtn fl"/>';
						}
						
						html+='</div>';
					$("#exprval").append(html);
				});
				
				
			}else{
				layer.msg("系统错误", {time: 1000});
			}
		},error: function(request) {
			layer.msg("网络错误", {time: 1000});
        }
	})
}

function calculate(){	
	if($("#exprhidden").val() == null || $("#exprhidden").val() == ''){
		layer.msg("请选择公式再进行计算", {time: 2000});
		return;
	}
	var inputs = $("#exprval .select_class");
	var isBlank = false;
	var isRealTime = false;
	$.each(inputs,function(index,ele){
		if($(ele).attr('empty')==2&&($(ele).val() == null || $(ele).val() == '')){
			layer.msg("请填写"+$(ele).attr('name')+"参数值再进行计算", {time: 2000});
			isBlank = true;
			return false;
		}
		if($(ele).val().indexOf("#") != -1 ){
			isRealTime = true;
		}
	});
	if(isBlank){
		return;
	}
	//实时数据参与计算
	if(isRealTime){
		updateExprResult();
	}else{
		//实时数据不参与计算
		var data = new Object();
		data.expr = $("#exprhidden").val();
		var params = new Array() 
		$.each(inputs,function(index,ele){
			if($(ele).val() != null || $(ele).val() != ''){
				var e = new Object();
				e.name = $(ele).attr('expr');
				e.value = $(ele).val();
				params.push(e);
			}
		});
		data.exprParams = params;
		$.ajax({
			type : "post",
			url : baseUrl+"/webApi/expr/calculate.jhtml",
			data: JSON.stringify(data),
			dataType:"json",
			headers: {"Content-Type": "application/json;charset=utf-8"},
	        success:function(data){
	        	if(data.httpCode==200){
					var dressingMethodhidden =$("#dressingMethodhidden").val();
					var dataMath=data.data;
					if(dressingMethodhidden==1){
						//四舍五入
						dataMath=keepTwoDecimal(dataMath,$("#scalehidden").val());						
					}else if(dressingMethodhidden==2){						
						//向上取整
						dataMath=Math.ceil(dataMath);						
					}else if(dressingMethodhidden==3){
						//向下取整
						dataMath=Math.floor(dataMath);
					}
	        		$("#exprresult").text(dataMath+$("#unithidden").val());
	        	}else{
					layer.msg("计算错误,请检查原始公式语法", {time: 1000});
				}
	        },
	        error:function(data){
	        	layer.msg("网络错误", {time: 1000});
	        }
	    });
	}
}

//数字四舍五入（保留n位小数）
function keepTwoDecimal(number, n) { 
  n = n ? parseInt(n) : 0; 
  if (n <= 0) return Math.round(number); 
  number = Math.round(number * Math.pow(10, n)) / Math.pow(10, n); 
  return number; 
};



// 更新公式计算结果
function updateExprResult(){
	if($("#formulaCalculationDiv").css("display") == "none"){
		return;
	}
	var inputs = $("#exprval :text");
	var data = new Object();
	data.expr = $("#exprhidden").val();
	var params = new Array() 
	if(inputs != null && inputs.length > 0){
		$.each(inputs,function(index,ele){
			if($(ele).val() != null || $(ele).val() != ''){
				var e = new Object();
				e.name = $(ele).attr('expr');
				e.value = $(ele).val();
				$.each(rtData,function(index,ele){
					if(ele.name == e.value){
						e.value = ele.value;
					}
				});
				params.push(e);
			}
		});
	}
	
	data.exprParams = params;
	$.ajax({
		type : "post",
		url : baseUrl+"/webApi/expr/calculate.jhtml",
		data: JSON.stringify(data),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
        success:function(data){
        	if(data.httpCode==200){
        		var dressingMethodhidden =$("#dressingMethodhidden").val();
				var dataMath=data.data;
				if(dressingMethodhidden==1){
					//四舍五入
					dataMath=keepTwoDecimal(dataMath,$("#scalehidden").val());						
				}else if(dressingMethodhidden==2){						
					//向上取整
					dataMath=Math.ceil(dataMath);						
				}else if(dressingMethodhidden==3){
					//向下取整
					dataMath=Math.floor(dataMath);
				}
        		$("#exprresult").text(dataMath+$("#unithidden").val());
        	}else{
				layer.msg("计算错误,请检查原始公式语法", {time: 1000});
			}
        },
        error:function(data){
        	layer.msg("网络错误", {time: 1000});
        }
    });
}

function exprselect(name){	
	$("#currV").val(name);
	$("#gsjsxzDiv").show();	
}

function selectV(){
	$("input[expr='" + $("#currV").val() + "']").val($("input[name='cgradio']:checked").val());
	$("#gsjsxzDiv").hide();	
}

//获取实时数据方法
function setPublicModelWeatherData(showId,hideId){
	$("#"+hideId).hide();
	$("#"+showId).show();
}


//水垫层高度
function setZHMNyygqfyModelCYZSDCGD(){
	
}

//判断含水率的
function ZHMNyygqfyModelChange(){ 
	var hsl = $("#ZHMNyygqfyModelCYZSDCGD").val();
	if(hsl>0.5){
		$("#errorCheck").show();
	}
	var speed = rb_speed(hsl);
	//张加
	speed=speed*2;
	$("#ZHMNyygqfyModelYYRCBSD").val(speed);
}

//修改结果
function rfsModelHZLXValue(){
	var rfsModelHZLX = $("#rfsModelHZLX").val();
	var d = $("#ZHMNrswfxModelGTZJ").val();
	var PI = 3.1415926;
	if(rfsModelHZLX==0){
		var num = PI * d/2 * d/2 - PI * (d/2-1.2) * (d/2 - 1.2);
		num = num.toFixed(2); 
		$("#ZHMNrfsModelHZMJ").val(a);
	}else{
		var num1 = PI * d/2 * d/2;
		num1 = num1.toFixed(2); 
		$("#ZHMNrfsModelHZMJ").val(a);
		$("#ZHMNrfsModelHZMJ").val(a);
	}
}

//计算
function zhmn_clickCalculation(){

	cleanKuoSanDrawLayer();
	var zhmnSglx = $("#zhmnSglx").val();
	if(!checkIsEmpty('zhmnSglxOut0Div')){
		layer.msg("请检查各个输入项是否输入", {time: 1000});
		return false;
	}
	if($("#zhmnRSFSSJ").val()=='0'){
		layer.msg("灾害已发生时间不能为0",{time:1000});
		return false;
	}
	if($("#ZHMNyygqfyModelCGZYMGD").val()=='0'){
		layer.msg("储罐中液面高度不能为0",{time:1000});
		return false;
	}
	
	if($("#ZHMNrswfxModelGTZJ").val()=='0'){
		layer.msg("罐体直径不能为0",{time:1000});
		return false;
	}
	
	if($("#ZHMNyygqfyModelCYZSDCGD").val()=='0'){
		layer.msg("油品含水率不能为0",{time:1000});
		return false;
	}
	
	if($("#ZHMNyygqfyModelYYRCBSD").val()=='0'){
		layer.msg("原油热播传播速度不能为0",{time:1000});
		return false;
	}
	
	if($("#ZHMNyygqfyModelRSXSD").val()=='0'){
		layer.msg("燃烧的线速度不能为0",{time:1000});
		return false;
	}
	
	if($("#ZHMNrswfxModelDivCc").val()=='0'){
		layer.msg("石油中含碳的质量比不能为0",{time:1000});
		return false;
	}
	
	if($("#ZHMNrswfxModelDivCs").val()=='0'){
		layer.msg("石油中含硫的质量比不能为0",{time:1000});
		return false;
	}
	
	if($("#ZHMNrswfxModelDivCn").val()=='0'){
		layer.msg("石油中含氮的质量比不能为0",{time:1000});
		return false;
	}
	if($("#ZHMNrfsModelHZMJ").val()=='0'){
		layer.msg("火灾面积不能为0",{time:1000});
		return false;
	}
	
	if($("#ZHMNrfsModelXLDJDMGD").val()=='0'){
		layer.msg("灾害点离地面的高度不能为0",{time:1000});
		return false;
	}
	
	if(!global_weatherData.windSpeed){
		layer.msg("实时数据没有开启请使用模拟数据进行测试", {time: 1000});
		return false;
	}
	var params = {};
	var publicParams = getPublicParams(params);
	if(publicParams){
		params = publicParams;
	}else{
		layer.msg("请先选择事故点", {time: 1000});
		return false;
	}

	fire_area = parseFloat($("#ZHMNrfsModelHZMJ").val());		// 燃烧面积
	fire_speed = 0.0781;	// 石油最大燃烧速度
	fire_Hc = 41816000;	// 石油燃烧热

//	防护服热伤害计算
	zhmnQxzs_FHFjs();
	
	params.area = fire_area;
	params.speed = fire_speed;
	params.Hc = fire_Hc;
	//半径曲线
	var rfsUrl = baseUrl+"/webApi/math/hotTimeRadiusLine.jhtml";
	mathAjax(params,rfsUrl,function(data){
		 var obj = data.data;
		 var mathGranularDLsit = obj.mathGranularDLsit;
		 zhmn_rsfxRbl(mathGranularDLsit[0],mathGranularDLsit[1],mathGranularDLsit[2]);
		 //画曲线，R1红色
		 var paramsSyn = {};
		 paramsSyn.accType = "zhmn_rfsfx";
		 paramsSyn.content = obj;
		 send(JSON.stringify({"evetype":"zhmn","val":paramsSyn}));
	});
	//喷溅时间,池火灾不要
	if(zhmnSglx==0){
		var H = $("#ZHMNyygqfyModelCGZYMGD").val();
		zhmn_yppjc(H);
		//喷溅范围
		zhmn_zzyppjfw();
	}else{
		$("#zhmnZZYPPJDiv").hide();
		
	}
	//燃烧物分析
	var url=baseUrl+'/webApi/math/calcEmissionRate.jhtml';
	 params.mt = $("#ZHMNrfsModelHZMJ").val()*ZHMNrswfxModelDivMt;
	 params.Cc = $("#ZHMNrswfxModelDivCc").val();
	 params.Cs = $("#ZHMNrswfxModelDivCs").val();
	 params.Cn = $("#ZHMNrswfxModelDivCn").val();
	 $("#closeZhmnRSWFXDivOut").hide();
	 mathAjax(params,url,function(data){
		 $("#zhmnRSWFXDivOut").show();
		 var obj = data.data;
		 var co2Rate = obj.co2Rate;
		 var coRate = obj.coRate;
		 var no2Rate = obj.no2Rate;
		 var noRate = obj.noRate;
		 var so2Rate = obj.so2Rate;
		 rswfxTubiaoChartShow2(coRate,co2Rate,no2Rate,noRate,so2Rate);
		 var paramsSyn = {};
		 paramsSyn.accType = "zhmn_rsfx";
		 paramsSyn.content = obj;
		 send(JSON.stringify({"evetype":"zhmn","val":paramsSyn}));
		 
	 });
	
	//界面显示隐藏
	 var paramsSyn = {};
	 paramsSyn.accType = "isShwo";
	 paramsSyn.content = zhmnSglx;
	 send(JSON.stringify({"evetype":"zhmn","val":paramsSyn}));
}

function zhmn_zzyppjfw(){
	var params = {};
	var publicParams = getPublicParams(params);
	if(params.cw==undefined){
		alert("实时数据或模拟数据未开启");
	}
	if(publicParams){
		params = publicParams;
	}else{
		alert("请先选择事故点");
		return false;
	}
	if($("#zhmn_pjfwxs").prop('checked')){
		//计算重质油品喷溅范围
		var url=baseUrl+'/webApi/math/getPJRegion.jhtml';
		 params.r = $("#ZHMNrswfxModelGTZJ").val()/2;
		 mathAjax(params,url,function(data){
			 var obj = data.data;
			 var layer = layerIsExist('buildGeometryLayer',global.map);
				if(layer){
					clearBuildGeometryLayer();
				}
				var pointDataList = obj.mathGranularVoLsit;
				var pointsObj = {};
				pointsObj.pointDataList=pointDataList;
				for(var i=0;i<pointDataList.length;i++){
					pointsObj.coords = eval('(' + pointDataList[i] + ')');
					pointsObj.isBorder = false;
					pointsObj.borderColor = '';
					pointsObj.fillColor = iteamsColor[i];
					pointsObj.fillTransparency = '0.6';
					if(pointsObj.coords.length>1){
						buildGeometryByCoords(pointsObj.coords,pointsObj.isBorder,pointsObj.borderColor,pointsObj.fillColor,pointsObj.fillTransparency);
					}
				}
				gotoPointView(zhmnAccidentPoint[0],zhmnAccidentPoint[1],18);
			 var paramsSyn = {};
			 paramsSyn.accType = "zhmn_pjfw";
			 paramsSyn.content = obj;
			 send(JSON.stringify({"evetype":"zhmn","val":paramsSyn}));
		 });
	}else{
		 var layer = layerIsExist('buildGeometryLayer',global.map);
			if(layer){
				clearBuildGeometryLayer();
			}
	}
	
}

//计算油品喷溅时间
//H页面高度
function zhmn_yppjc(H){
	//喷溅时间,池火灾不要
	var h = parseFloat($("#ZHMNyygqfyModelCYZSDCGD").val())*H/100;
	var addressID = $("input[name='ZHMNyygqfyModelSbradio']:checked").val();
	var flage = $("#ZHMNyygqfyModelYPMC").val();
	var hs = $("#ZHMNyygqfyModelYYRCBSD").val();
	var ss = $("#ZHMNyygqfyModelRSXSD").val();
	var shortTime=0;
	var longTime=0;
	shortTime = time_qzyy(H,h,parseFloat(hs),parseFloat(ss),addressID);
	startTimeInterval(shortTime*60*60);
	$("#zhmnZZYPPJDiv").show();
//		$("#jieguo").val("预计时间："+shortTime);
	var paramsSyn = {};
	paramsSyn.accType = "zhmn_pjsj";
	paramsSyn.content = shortTime*60*60;
	send(JSON.stringify({"evetype":"zhmn","val":paramsSyn}));
}

//修正喷溅时间
function zhmn_updateXzSurplusTime1(){
	var gd = $("#rbgdSurplusTime").val();
	zhmn_yppjc(gd);
}

//修正喷溅时间
function zhmn_updateXzSurplusTime2(){
	var newTime = $("#xzSurplusTime").val();
	startTimeInterval(newTime*60);
	var paramsSyn = {};
	paramsSyn.accType = "zhmn_pjsj";
	paramsSyn.content = newTime*60;
	send(JSON.stringify({"evetype":"zhmn","val":paramsSyn}));
}
//燃烧分析比例
function zhmn_rsfxRbl(dataHurt1,dataHurt2,dataHurt3){
	var dataTime=[];
	for(var t = 1; t < 10; t++){
		dataTime.push(t*2);
	}
	$("#zhmnQxzsOutDiv").show();
	// 基于准备好的dom，初始化echarts实例
	var dom = document.getElementById("zhmnQxzs");
	var myChart = echarts.init(dom);
	var app = {};
	option = null;
	var colors = ['#1fc17b', '#fae242', '#fa4242'];
	option = {
//		    title: {
//		        text: '折线图堆叠'
//		    },
		    tooltip: {
		        trigger: 'axis',
		        backgroundColor:'rgba(255,255,255,0.8)',
		        borderColor:'gray',
		        	textStyle:{
		        		color:'black',
		        	}
		    },
		    legend: {
		        data:['轻伤','重伤','致死'],
		        textStyle: {
	                color: '#fff'
	            }
		    },
	
		    xAxis: {
		        type: 'category',
		        boundaryGap: false,
		        data: dataTime,
		        axisPointer: {
	                label: {
	                    formatter: function (params) {
	                        return '时间' + params.value+'分';
	                    }
                    }
                },
                axisLabel: {
                    textStyle: {
                        color: '#fff'
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: '#fff'
                    }  
                },
                name: '时间(分)'
		    },
		    yAxis: {
		        type: 'value',
		        name: '距离',
		        axisLabel: {
		            textStyle: {
		                color: '#fff'
		            }
		        },
		        axisLine: {
                    lineStyle: {
                        color: '#fff'
                    }  
                }
		    },
		    series: [
		        {
		            name:'致死',
		            type:'line',
		            smooth: true,
		            data: dataHurt1,
		            color:colors[2],
		        },
		        {
		            name:'重伤',
		            type:'line',
		            smooth: true,
		            data: dataHurt2,
		            color:colors[1],
		        },
		        {
		            name:'轻伤',
		            type:'line',
		            smooth: true,
		            data: dataHurt3,
		            color:colors[0],
		        }
		        
		    ]
		};
	if (option && typeof option === "object") {
	    myChart.setOption(option, true);
	}
}

function eeetest(){
	var url = "http://192.168.0.138:8018/GasEmission.ashx";
	var url1 = "http://192.168.0.138:8018/GasEmission.ashx?sid=create3Dpollutlayer&timeSpan=60&timeInterval=&dWindSpeed=1.5&dT=26&windStab=&humidity=80&pressure=1000&RType=0&h2scon=1&pipepress=2&lng=107.96755&lat=31.353538&wtoken=a22dmin32018-8-13-19-5022-35&windWane=90";
	var url2 = "timeSpan=60" + //时间
			"&timeInterval=" + //空
			"&dWindSpeed=1.5" + //风速
			"&dT=26" + //温度
			"&windStab=" + //空
			"&humidity=80" + //湿度
			"&pressure=1000" + //大气压强
			"&RType=0" + //空
			"&h2scon=1" + //空
			"&pipepress=2" + //空
			"&lng=107.96755" + //精度
			"&lat=31.353538" + //维度
			"&wtoken=a22dmin32018-8-13-19-5022-35" + //文件夹名称，用户名+时间
			"&windWane=90"; // 风向
	var params= {};
	params.sid = 'create3Dpollutlayer';
	params.timeSpan = '60';
	params.timeInterval = '1';
	params.dWindSpeed = '1.5';
	params.dT = '26';
	params.windStab = '26';
	params.humidity = '26';
	params.pressure = '26';
	params.RType = '26';
	params.h2scon = '26';
	params.pipepress = '26';
	params.lng = '26';
	params.lat = '26';
	params.wtoken = '26';
	params.windWane = '26';
	$.ajax({
		type : "get",
//		url : baseUrl+"/webApi/upScenebuilding.jhtml",
		url :url,
		data: JSON.stringify(params),
		dataType:"text",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
				layer.msg("保存成功");
		},error: function(request) {
			alert("网络错误");
	    }
	})
}
function jsonpCallback(res){
    //alert(JSON.stringify(res))
}

//<select id="zhmnSglx" class="typeSelect-A fl" onchange="zhmn_SglxSelect()">
//<option value="0">油罐火灾</option>
//<option value="1">池火灾</option>
//<option value="2">气体泄露</option>
//<option value="3">爆炸</option>
//</select>

//修改灾害类型
function zhmn_SglxSelect(){
	var zhmnSglx = $("#zhmnSglx").val();
	setDefaultValue(zhmnSglx);
	if(zhmnSglx==0){
		$("#isYKHZDiv1").show();
		$("#isYKHZDiv2").show();
		$("#zhmnSglxOut0Div").show();
		$("#zhmnSglxOut1Div").hide();
		$("#zhmnSglxOut2Div").hide();
	}else if(zhmnSglx==1){
		$("#isYKHZDiv1").hide();
		$("#isYKHZDiv2").hide();
		$("#zhmnSglxOut0Div").show();
		$("#zhmnSglxOut1Div").hide();
		$("#zhmnSglxOut2Div").hide();
	}else if(zhmnSglx==2){
		$("#zhmnSglxOut0Div").hide();
		$("#zhmnSglxOut1Div").show();
		$("#zhmnSglxOut2Div").hide();
		$("#isYKHZDiv1").hide();
	}else if(zhmnSglx==3){
		$("#zhmnSglxOut0Div").hide();
		$("#zhmnSglxOut1Div").hide();
		$("#zhmnSglxOut2Div").show();
		$("#isYKHZDiv1").show();
	}
	
}

var ZHMNXLV = '';
//泄露源定位
//223.557774995983,107.9671884,31.35408426,1.2,56.9063379124574,107.9674636,31.35398703,1.2,56.9063379124574,107.967364,31.35380759,1.2,223.557774995983,107.9670953,31.35391145,1.2
function zhmn_clickXL1Calculation(){
	ZHMNXLV='';
	if(!checkIsEmpty('zhmnSglxOut1Div')){
		layer.msg("请检查各个输入项是否输入", {time: 1000});
		return false;
	}
	/**
	 * @注释：自动获取数据
	 * **/
	var url=baseUrl+'/webApi/math/leakSource.jhtml';
	var params = {};
	params = getRealTimeOrSimulateDataFS(1,params);
	if(!params){
		layer.msg('数据加载错误', {icon: 1, time: 1000});
		return false;
	}
	var resultN = getRealtimeDataParams();
	if(!resultN){
		return false;
	}else{
		params.data = resultN;
	}
	mathAjax(params,url,function(data){
		 var obj = data.data;
		 var sX = obj.sX;
		 var sY = obj.sY;
		 var sH = obj.sH;
		 var lsQ = obj.lsQ;
		 lsQ = (lsQ/1000).toFixed(2);
		 ZHMNXLV = lsQ;
		 $("#ZHMNrfsModelXLDJDXLV").val(lsQ);
		 zhmnAccidentPoint = ol.proj.transform([sX,sY], 'EPSG:4326', 'EPSG:3857');
		 gotoPointView(zhmnAccidentPoint[0],zhmnAccidentPoint[1],17);
		 addPointByXy(zhmnAccidentPoint,12345,'/mapIcon/mapFixedIcon/sgd_dw.png');
		 var paramsSyn = {};
		 paramsSyn.accType = "isShwo";
		 paramsSyn.content = 5;
		 send(JSON.stringify({"evetype":"zhmn","val":paramsSyn}));
	 });
	showZNTJWHPQT();
}

//实时灾害模拟泄漏源定位
function zhmn_ZHMNXLYDWCalculation(){
	if(!checkIsEmpty('zhmnSglxOut1Div')){
		layer.msg("请检查各个输入项是否输入", {time: 1000});
		return false;
	}
	//轻质气体
	var isRealTime = $("input[name='rfsModelWeatherData']:checked").val();
	var params = {};
	params = getRealTimeOrSimulateData(isRealTime,params);
	if(!params){
		layer.msg('数据加载错误', {icon: 1, time: 1000});
		return false;
	}
	
	params.h = $("#ZHMNrfsModelXLDJDMGDBZXL").val();
	var dq = $("#ZHMNrfsModelXLDJDXLV").val();
	var rateUrl = baseUrl+'/webApi/math/leakSource.jhtml';
	params.fCurrentTime = parseInt($("#zhmnRSFSSJ").val())*60;
	params.gasType=global_gasIsAlertNameData;
	if(dq!=""){
		params.dq =$("#ZHMNrfsModelXLDJDXLV").val();
		zhmn_ZHMNXLYDWCalculation_kuosan(params);
	}else if(ZHMNXLV){
		params.dq =ZHMNXLV;
		zhmn_ZHMNXLYDWCalculation_kuosan(params);
	}else{
		var resultN = getRealtimeDataParams();
		if(!resultN){
			return false;
		}else{
			params.data = resultN;
		}
		mathAjax(params,rateUrl,function(data){
			var obj = data.data;
			 var lsQ = obj.lsQ;
			 lsQ = (lsQ/1000).toFixed(2);
			 ZHMNXLV = lsQ;
			 params.dq = lsQ;
			 $("#ZHMNrfsModelXLDJDXLV").val(lsQ);
			 zhmn_ZHMNXLYDWCalculation_kuosan(params);
		});
	}
	showZNTJWHPQT();
}

function zhmn_ZHMNXLYDWCalculation_kuosan(params){
	 if(global_gasIsAlertM<29){
		 var url=baseUrl+'/webApi/math/gaussionModelParameters.jhtml';
		 sjzParams = params;
		 sjzUrl = url;
		 //轻质气体
		 mathAjax(params,url,function(data){
			 hideShowTime("zhmnKSSJDivOutSyn");
			 var obj = data.data;
			 var layer = layerIsExist('buildGeometryLayer',global.map);
			 if(layer){
				 clearBuildGeometryLayer();
			 }
			 var pointsObj = {};
			 var pointDataList = obj.mathGranularVoLsit;
			 pointsObj.pointDataList = pointDataList;
			 pointsObj.sjzUrl = sjzUrl;
			 pointsObj.sjzParams = sjzParams;
			 for(var i=0;i<pointDataList.length;i++){
				 pointsObj.coords = eval('(' + pointDataList[i] + ')');
				 pointsObj.isBorder = false;
				 pointsObj.borderColor = '';
				 pointsObj.fillColor = iteamsColor[i];
				 pointsObj.fillTransparency = '0.6';
				 if(pointsObj.coords.length>1){
					 buildGeometryByCoords(pointsObj.coords,pointsObj.isBorder,pointsObj.borderColor,pointsObj.fillColor,pointsObj.fillTransparency);
				 }
			 }
			 drawHighLine('drawHighLine',zhmnAccidentPoint);
			 gotoPointView(zhmnAccidentPoint[0],zhmnAccidentPoint[1],18);
			 var paramsSyn1 = {};
			 pointsObj.titleTime = "";
			 pointsObj.point = zhmnAccidentPoint;
			 paramsSyn1.accType = "zhmn_qzks";
			 paramsSyn1.content = pointsObj;
			 send(JSON.stringify({"evetype":"zhmn","val":paramsSyn1}));
		 });
	 }else{
		 //重质气体
		 	var lonlat = ol.proj.transform(zhmnAccidentPoint, 'EPSG:3857', 'EPSG:4326');
		 	params.lng=lonlat[0];
			params.lat=lonlat[1];
			params.dWindSpeed=params.ws;
			params.dT = params.cw;
			params.humidity = params.humidity;
			params.windWane = params.wd;
			params.RType = 2;
			params.h2scon= 4;
			params.pipepress=3;
			var rate = $("#ZHMNrfsModelXLDJDXLV").val();
			params.dQ=rate*1000;
			var tokenV = getTime();
			params.wtoken=tokenV;
			params.windWane=global_weatherData.windDirection;
			params.isLeft = true;
			$("#fzdxModelDiv").hide();
			gasSpreadController(params);
			
			 var paramsSyn1 = {};
			 paramsSyn1.accType = "zhmn_zzks";
			 paramsSyn1.content = params;
			 send(JSON.stringify({"evetype":"zhmn","val":paramsSyn1}));
			
	 }
}
//灾害模拟泄漏源定位
function zhmn_ZHMNXLYDWCalculation1(){
	if(!checkIsEmpty('zhmnSglxOut1Div')){
		layer.msg("请检查各个输入项是否输入", {time: 1000});
		return false;
	}
	//轻质气体
	var isRealTime = $("input[name='rfsModelWeatherData']:checked").val();
	var params = {};
	params = getRealTimeOrSimulateData(isRealTime,params);
	if(!params){
		layer.msg('数据加载错误', {icon: 1, time: 1000});
		return false;
	}
	
	params.h = $("#ZHMNrfsModelXLDJDMGDBZXL").val();
	var dq = $("#ZHMNrfsModelXLDJDXLV").val();
	var url=baseUrl+'/webApi/math/gaussionModelParameters.jhtml';
	var rateUrl = baseUrl+'/webApi/math/leakSource.jhtml';
	if(ZHMNXLV){
		params.dq =ZHMNXLV;
	}else if(dq!=""){
		params.dq =$("#ZHMNrfsModelXLDJDXLV").val();
	}else{
		var resultN = getRealtimeDataParams();
		if(!resultN){
			return false;
		}else{
			params.data = resultN;
		}
		url=baseUrl+'/webApi/math/getRateGaussionModelParameters.jhtml';
	}
	params.fCurrentTime = parseInt($("#zhmnRSFSSJ").val())*60;
	params.gasType='CO';
	sjzParams = params;
	sjzUrl = url;
	//todo 计算轻重气体
	mathAjax(params,url,function(data){
			$("#zhmnKSSJDivOut").show();
			var obj = data.data;
			var layer = layerIsExist('buildGeometryLayer',global.map);
			if(layer){
				clearBuildGeometryLayer();
			}
			var pointDataList = obj.mathGranularVoLsit;
			for(var i=0;i<pointDataList.length;i++){
				var pointsObj = {};
				pointsObj.coords = eval('(' + pointDataList[i] + ')');
				pointsObj.isBorder = false;
				pointsObj.borderColor = '';
				pointsObj.fillColor = iteamsColor[i];
				pointsObj.fillTransparency = '0.6';
				if(pointsObj.coords.length>1){
					buildGeometryByCoords(pointsObj.coords,pointsObj.isBorder,pointsObj.borderColor,pointsObj.fillColor,pointsObj.fillTransparency);
				}
			}
			drawHighLine('drawHighLine',zhmnAccidentPoint);
			gotoPointView(zhmnAccidentPoint[0],zhmnAccidentPoint[1],18);
	 });
}

//火灾计算
function zhmn_clickXL2Calculation(){
	var url=baseUrl+'/webApi/math/calcHrLine.jhtml';
	var params = {};
	params.wd  = global_weatherData.windDirection;
	params.ws = global_weatherData.windSpeed;
	params.area = $('#ZHMNcgybzModelBZWTJ').val();
	mathAjax(params,url,function(data){
		var obj = data.data;
		var layer = layerIsExist('buildGeometryLayer',global.map);
		if(layer){
			clearBuildGeometryLayer();
		}
		var pointDataList = obj.mathGranularVoLsit;
		for(var i=0;i<pointDataList.length;i++){
			var pointsObj = {};
			pointsObj.coords = eval('(' + pointDataList[i] + ')');
			pointsObj.isBorder = false;
			pointsObj.borderColor = '';
			pointsObj.fillColor = iteamsColor[i];
			pointsObj.fillTransparency = '0.6';
			buildGeometryByCoords(pointsObj.coords,pointsObj.isBorder,pointsObj.borderColor,pointsObj.fillColor,pointsObj.fillTransparency);
		}
		gotoPointView(11518384.023410,3571012.185491,18);
//		gotoPointView(zhmnAccidentPoint[0],zhmnAccidentPoint[1]);
	 });
}
//超高压爆炸
function zhmn_clickXL3Calculation(){
	if(!checkIsEmpty('zhmnSglxOut2Div')){
		layer.msg("请检查各个输入项是否输入", {time: 1000});
		return false;
	}
	
	var url=baseUrl+'/webApi/math/explosionHurtR.jhtml';
	var params = {};
	params.type1 = $("#ZHMNcgybzModelBZLX").val();
	params.name1 = $("#ZHMNcgybzModelBZWMC").val();
	params.pressure = $("#ZHMNcgybzModelBZWYL").val();
	params.volume = $("#ZHMNcgybzModelBZWTJ").val();
//	params.r = $("#ZHMNcgybzModelDistance").val();
	params.hurt = 2;
	if(zhmnAccidentPoint.length<1){
		layer.msg("没有事故点信息，无法在地图上展示扩散范围！请设置事故点。", {time: 1000});
		return false;
	}else{
		mathAjax(params,url,function(data){
			var obj = data.data;
			var pointDatas = obj.pointData;
			var items = [];
			for(var i=0;i<pointDatas.length;i++){
				var obj = {};
				obj.distance = pointDatas[i];
				obj.color = iteamsColorBZ[i];
				items[i]=obj;
			}
			var point =[zhmnAccidentPoint[0],zhmnAccidentPoint[1]];
			drawBZ('cgybz',items,point,1,true);
			//$("#damageWaveDiv").show();
			$("#damagePeopleDiv").show();
			if(isFlat){
				centerAndZoom(point,17);
				isFlat = true;
			}
			var paramsSyn1 = {};
			paramsSyn1.accType = "zhnm_bz";
			paramsSyn1.content = pointDatas;
			send(JSON.stringify({"evetype":"zhmn","val":paramsSyn1}));
			
		});
	}
}

//清楚爆炸图
function zhmn_clean3Calculation(){
	clearBzy('cgybz');
	$("#damageWaveDiv").hide();
	$("#damagePeopleDiv").hide();
	
}

//扩散模拟
function zhmn_showKSFWzhm(isSyn,time){
	$("#zhmnKSSJDivUl li").removeClass();
	$("#zhmnKSSJDivUlSyn li").removeClass();
	if(isSyn==0){
		$("#zhmnTime"+time).addClass("currentTime");
	}else{
		$("#zhmnSynTime"+time).addClass("currentTime");
	}
	var key = zhmnSglx+"hzmnks"+time;
	if(false){
//		if(ksmnTypeDatas.containsKey(key)){
//		var obj = ksmnTypeDatas.get(key);
//		var layer = layerIsExist('buildGeometryLayer',global.map);
//		if(layer){
//			clearBuildGeometryLayer();
//		}
//		var pointDataList = obj.mathGranularVoLsit;
//		var pointsObj = {};
//		pointsObj.pointDataList=pointDataList;
//		for(var i=0;i<pointDataList.length;i++){
//			pointsObj.time = time;
//			pointsObj.liId = "zhmnSynTime"+time;
//			pointsObj.coords = eval('(' + pointDataList[i] + ')');
//			pointsObj.isBorder = false;
//			pointsObj.borderColor = '';
//			pointsObj.fillColor = iteamsColor[i];
//			pointsObj.fillTransparency = '0.6';
//			buildGeometryByCoords(pointsObj.coords,pointsObj.isBorder,pointsObj.borderColor,pointsObj.fillColor,pointsObj.fillTransparency);
//		}
//		//同步
//		if(isSyn==1){
//			var paramsSyn = {};
//			paramsSyn.accType = "zhmn_qzksTime";
//			paramsSyn.content = pointsObj;
//			send(JSON.stringify({"evetype":"zhmn","val":paramsSyn}));
//		}
//		//选中定位，则调用定位方法
//		if($('#isZoomCheck').prop('checked')){
//			gotoPointView(zhmnAccidentPoint[0],zhmnAccidentPoint[1]);
//		}
//		if($('#isZoomCheckSyn').prop('checked')){
//			gotoPointView(zhmnAccidentPoint[0],zhmnAccidentPoint[1]);
//		}
		
	}else{
		$("#zhmnHZMYFXDivOut").hide();
		isRealTime = 1;
//		var url=baseUrl+'/webApi/math/gaussionModelParameters.jhtml';
		var url = sjzUrl;
		var isRealTime = $("input[name='rfsModelWeatherData']:checked").val();
		params = sjzParams;
		
		if(url.indexOf("GasEmission.ashx")!=-1){
			params.fCurrentTime=time-1;
			isTimeEnd = true;
			if(isSyn==1){
				showSpreadInfo(zhmnAccidentPoint[0],zhmnAccidentPoint[1],true,gasResultUrl+params.wtoken,params.fCurrentTime);
			}else{
				showSpreadInfo(zhmnAccidentPoint[0],zhmnAccidentPoint[1],false,gasResultUrl+params.wtoken,params.fCurrentTime);
			}
		}else if(url.indexOf("getFireSpreadRegion.jhtml")!=-1){
			params.fCurrentTime=time*60;
			mathAjax(params,url,function(data){
				var obj = data.data;
				var layer = layerIsExist('buildGeometryLayer',global.map);
				if(layer){
					clearBuildGeometryLayer();
				}
				var pointDataList = obj.mathGranularVoLsit;
				var pointsObj = {};
				pointsObj.pointDataList=pointDataList;
				for(var i=0;i<pointDataList.length;i++){
					pointsObj.time = time;
					pointsObj.liId = "zhmnSynTime"+time;
					pointsObj.coords = eval('(' + pointDataList[i] + ')');
					pointsObj.isBorder = false;
					pointsObj.borderColor = '';
					pointsObj.fillColor = iteamsColor[i];
					pointsObj.fillTransparency = '0.6';
					buildGeometryByCoords(pointsObj.coords,pointsObj.isBorder,pointsObj.borderColor,pointsObj.fillColor,pointsObj.fillTransparency);
				}
				//同步
				if(isSyn==1){
					var paramsSyn = {};
					paramsSyn.accType = "zhmn_qzksTime";
					paramsSyn.content = pointsObj;
					pointsObj.point = zhmnAccidentPoint;
					send(JSON.stringify({"evetype":"zhmn","val":paramsSyn}));
				}
				$("#zhmnHZMYFXDivOut").show();
				$("#zhmnHZMYFX_sf").html(obj.pointData[0].toFixed(2));
				$("#zhmnHZMYFX_cf").html(obj.pointData[1].toFixed(2));
				$("#zhmnHZMYFX_xf").html(obj.pointData[2].toFixed(2));
			});
		}else{
			params.fCurrentTime=time*60;
			mathAjax(params,url,function(data){
				var obj = data.data;
				var layer = layerIsExist('buildGeometryLayer',global.map);
				if(layer){
					clearBuildGeometryLayer();
				}
				var pointDataList = obj.mathGranularVoLsit;
				var pointsObj = {};
				pointsObj.pointDataList=pointDataList;
				for(var i=0;i<pointDataList.length;i++){
					pointsObj.time = time;
					pointsObj.liId = "zhmnSynTime"+time;
					pointsObj.coords = eval('(' + pointDataList[i] + ')');
					pointsObj.isBorder = false;
					pointsObj.borderColor = '';
					pointsObj.fillColor = iteamsColor[i];
					pointsObj.fillTransparency = '0.6';
					buildGeometryByCoords(pointsObj.coords,pointsObj.isBorder,pointsObj.borderColor,pointsObj.fillColor,pointsObj.fillTransparency);
				}
				//同步
				if(isSyn==1){
					var paramsSyn = {};
					paramsSyn.accType = "zhmn_qzksTime";
					paramsSyn.content = pointsObj;
					send(JSON.stringify({"evetype":"zhmn","val":paramsSyn}));
				}
			});
		}
	    //选中定位，则调用定位方法
	 	if($('#isZoomCheck').prop('checked')){
			gotoPointView(zhmnAccidentPoint[0],zhmnAccidentPoint[1],18);
		}
	 	if($('#isZoomCheckSyn').prop('checked')){
	 		gotoPointView(zhmnAccidentPoint[0],zhmnAccidentPoint[1],18);
	 	}
	}
}

//生成燃烧的时间列表
function zhmn_changeKSMNList(){
	var startTime = parseInt($("#zhmnRSFSSJ").val());
	var html = '';
	for(var i=0;i<12;i++){
		var showTime = startTime+5*i;
		html += '<li onclick="zhmn_showKSFWzhm(0,'+showTime+')">'+showTime+'分钟</li>';
		
	}
	$("#zhmnKSSJDivUl").html(html);
}
//爆炸联动
//type: 爆炸类型(1-3是物理爆炸, 4是化学爆炸), 1 压缩气体容器爆炸, 2 高压液体容器爆炸, 3 过热液体容器爆炸, 4 蒸气云爆炸
//idx: 爆炸物索引
//    1 压缩气体容器爆炸:空气(1),氮(2),氧(3),氢(4),甲烷(5),乙烷(6),乙烯(7),丙烷(8),一氧化碳(9),二氧化碳(10),一氧化氮(11),二氧化氮(12),氨气(13),氯气(14),过热蒸气(15),干饱和蒸气(16),氢氯酸(17)
//    4 蒸气云爆炸:氢气(1),氨气(2),苯(3),一氧化碳(4),硫化氢1(生成SO2)(5),硫化氢2(生成SO3)(6),甲烷(7),乙烷(8),乙烯(9),乙炔(10),丙烷(11),丙烯(12),正丁烷(13),异丁烷(14),丁烯(15)
var ysMap = new HashMap();
ysMap.put(1,'空气');
ysMap.put(2,'氮');
ysMap.put(3,'氧');
ysMap.put(4,'氢');
ysMap.put(5,'甲烷');
ysMap.put(6,'乙烷');
ysMap.put(7,'乙烯');
ysMap.put(8,'丙烷');
ysMap.put(9,'一氧化碳');
ysMap.put(10,'二氧化碳');
ysMap.put(11,'一氧化氮');
ysMap.put(12,'二氧化氮');
ysMap.put(13,'氨气');
ysMap.put(14,'氯气');
ysMap.put(15,'过热蒸气');
ysMap.put(16,'干饱和蒸气');
ysMap.put(17,'氢氯酸');
var zqyMap = new HashMap();
zqyMap.put(1,'氢气');
zqyMap.put(1,'氢气');
zqyMap.put(2,'氨气');
zqyMap.put(3,'苯');
zqyMap.put(4,'一氧化碳');
zqyMap.put(5,'硫化氢1(生成SO2)');
zqyMap.put(6,'硫化氢2(生成SO3)');
zqyMap.put(7,'甲烷');
zqyMap.put(8,'乙烷');
zqyMap.put(9,'乙烯');
zqyMap.put(10,'乙炔');
zqyMap.put(11,'丙烷');
zqyMap.put(12,'丙烯');
zqyMap.put(13,'正丁烷');
zqyMap.put(14,'异丁烷');
zqyMap.put(15,'丁烯');
function setcgybzModelBZWMCValue(parentId,oid){
	 var type=$("#"+parentId).val();
	 var keys = "";
	 var tempMap = "";
	 $("#cgybzModelBZWYL").val("");
	 $("#cgybzModelBZWTJ").val("");
	 $("#cgybzModelBZWTJ5").val("");
	 $("#cgybzModelBZWYL5").val("");
	 if(type==1 || type==2 ||type==3 ||type==4 ){
			$("#bzjstyep5").hide();
			$("#bzjstyep1234").show();
	 }else if(type==5){
		$("#bzjstyep1234").hide();
		$("#bzjstyep5").show();
	 }
	 if(type==1 || type==2 ||type==3 ||type==5 ){
		keys =  ysMap.keys();
		tempMap = ysMap;
	 }else if(type==4){
		keys =  zqyMap.keys();
		tempMap = zqyMap;
	 }
	 var html ="";
	 for(var i=0;i<keys.length;i++){
		 html+='<option value="'+keys[i]+'">'+tempMap.get(keys[i])+'</option>' ;
	 }
	 $("#"+oid).html(html);
}

//放大收缩热辐射分析
//var qxzsIsopen = true;
//var zzyppjIsopen = true;
//var rsxfxIsopen = true;
function zhmnQxzsOpenCloseDiv(){
	if(qxzsIsopen){
		qxzsIsopen = false;
	}else{
		
	}
}

//热辐射分析收起内容
function rfsHideBox(){ 
	$("#zhmnQxzsOutDiv").hide();
	$("#hotRadiation").css('display','block');
//	var vv=$("#outzhmn1").text();
//	if(vv=="收起"){
//		$("#zhmnQxzsOutDiv").css("width","220px");
//		$("#rfsfxShow").hide();
//		$("#outzhmn1").text("展开");
//		$("#rfsArrowIcon").addClass("retractOut");
//	}else{
//		$("#zhmnQxzsOutDiv").css("width","620px");
//		$("#rfsfxShow").show();
//		$("#outzhmn1").text("收起");
//		$("#rfsArrowIcon").removeClass("retractOut");
//	}
}

//喷溅收起内容
function pjHideBox(){ 
	$("#zhmnZZYPPJDiv").hide();
	$("#expectTime").css('display','block');
//	var vv=$("#outzhmn2").text();
//	if(vv=="收起"){
//		$("#zhmnZZYPPJDiv").css("width","220px");
//		$("#timeWarpBox").hide();
//		$("#outzhmn2").text("展开");
//		$("#pjArrowIcon").addClass("retractOut");
//	}else{
//		$("#zhmnZZYPPJDiv").css("width","320px");
//		$("#timeWarpBox").show();
//		$("#outzhmn2").text("收起");
//		$("#pjArrowIcon").removeClass("retractOut");
//	}
}


//燃烧物分析收起内容
function rswfxHideBox(){ 
	$("#zhmnRSWFXDivOut").hide();
	$("#burnAnalyse").css('display','block');
//	var vv=$("#outzhmn3").text();
//	if(vv=="收起"){
//		$("#zhmnRSWFXDivOut").css("width","200px");
//		$("#zhmnRSWFXDivMain").hide();
//		$("#outzhmn3").text("展开");
//		$("#rswArrowIcon").addClass("retractOut");
//	}else{
//		$("#zhmnRSWFXDivOut").css("width","300px");
//		$("#zhmnRSWFXDivMain").show();
//		$("#outzhmn3").text("收起");
//		$("#rswArrowIcon").removeClass("retractOut");
//	}
}
function onHotRadiationClick(){
	$("#zhmnQxzsOutDiv").show();
	$("#hotRadiation").css('display','none');
}
function onExpectTimeClick(){
	$("#zhmnZZYPPJDiv").show();
	$("#expectTime").css('display','none');
}
function onBurnAnalyse(){
	$("#zhmnRSWFXDivOut").show();
	$("#burnAnalyse").css('display','none');
}

//消防力量收起内容 
function fireDosageDivHideBox(){ 
	var vv=$("#outfireDosage3").text();
	if(vv=="收起"){
		$("#fireDosageDiv").css("width","250px");
		$("#fireDosageDivList").hide();
		$("#outfireDosage3").text("展开");
		$("#fireDosageArrowIcon").addClass("retractOut");
	}else{
		$("#fireDosageDiv").css("width","750px");
		$("#fireDosageDivList").show();
		$("#outfireDosage3").text("收起");
		$("#fireDosageArrowIcon").removeClass("retractOut");
	}
}


//初始化默认值
function setDefaultValue(type){
	if(type==0){
		$("#ZHMNyygqfyModelCGZYMGD").val("17");
		$("#ZHMNyygqfyModelCYZSDCGD").val("0.2");
		$("#ZHMNrswfxModelGTZJ").val("80");
		$("#ZHMNrswfxModelDivCc").val("85");
		$("#ZHMNrswfxModelDivCs").val("0.07");
		$("#ZHMNrswfxModelDivCn").val("1");
		$("#ZHMNrfsModelHZMJ").val("5026");
		$("#ZHMNrfsModelXLDJDMGD").val("21.8");
	}else if(type==1){
		$("#ZHMNrfsModelHZMJ").val("200");
		$("#ZHMNrfsModelXLDJDMGD").val("1");
	}else if(type==2){
		$("#ZHMNrfsModelXLDJDMGDBZXL").val("0.5");
	}else if(type==3){
		$("#ZHMNcgybzModelBZWYL").val("10");
		$("#ZHMNcgybzModelBZWTJ").val("400");
		$("#ZHMNcgybzModelDistance").val("0.5");
		
	}
}

//隐藏图层面板
function hidePanl(){
	//zhmnKSSJDivOut
	closePanel('zhmnQxzsOutDiv');
	closePanel('zhmnKSSJDivOut');
	closePanel('zhmnKSSJDivOutSyn');
	$("#zhmnKSSJDivUl li").removeClass();
	$("#zhmnKSSJDivUlSyn li").removeClass();
	closePanel('zhmnRSWFXDivOut');
	closePanel('zhmnZZYPPJDiv');
	ZHMNClosePanel('zhmnZZYPPJDiv');
	closePanel('zhmnKSSJDivOut');
	clearBzy('cgybz');
	$("#damageWaveDiv").hide();
	$("#damagePeopleDiv").hide();
	cleanKuoSanDrawLayer();
}
//清空所有图层的方法
function cleanKuoSanDrawLayer(){
	clearSpreadLayer();
	clearLayerByIds( ['buildGeometryLayer','cgybzbzyLayer','drawHighLinebzyLayer'],global.map);
	clearBzy('cgybz'); 
	clearBuildGeometryLayer('buildGeometryLayer');
	clearBzy('drawHighLine'); 
//	closePanel('zhmnQxzsOutDiv');
	closePanel('zhmnKSSJDivOut');
	closePanel('zhmnKSSJDivOutSyn');
	$("#zhmnKSSJDivUl li").removeClass();
	$("#zhmnKSSJDivUlSyn li").removeClass();
	$("#ysslDiv").hide();
	$("#hotRadiation").hide();
	$("#expectTime").hide();
	$("#burnAnalyse").hide();
	if(spreadTimer!=null){
		clearTimeout(spreadTimer);
		spreadTimer = null;
		isTimeEnd = true;
	}
}

//重质气体扩散
function gasSpreadController(params){
	params.sid="create3Dpollutlayer";
	params.timeSpan="60";
	params.timeInterval="";
	params.windStab="";
	sjzParams = params;
	sjzUrl =  gasUrl+"GasEmission.ashx";
	params.sjzUrl = sjzUrl;
	$.ajax({
		type : "get",
		url : gasUrl+"GasEmission.ashx",
		dataType:"text",
		jsonp: "callbackparam",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		data: params,
		success : function(data) {					
			centerAndZoom(zhmnAccidentPoint,17);
			spread(zhmnAccidentPoint[0],zhmnAccidentPoint[1],params.isLeft,params.wtoken,0);
			//如果是左侧的就需要同步
			if(params.isLeft){
				 var paramsSyn = {};
				 paramsSyn.accType = "zhmn_zzks";
				 paramsSyn.content = params;
				 send(JSON.stringify({"evetype":"zhmn","val":paramsSyn}));
			}
			drawHighLine('drawHighLine',zhmnAccidentPoint);
			$("#zhmnKSSJDivOut").show();
		},error: function(request) {
			alert("网络错误");
		}
	});
}

//计算轻质气体扩散
function gasSpreadLightController(params){
	isTimeEnd = true;
	var url=baseUrl+'/webApi/math/gaussionModelParameters.jhtml';
	sjzParams = params;
	sjzUrl = url;
	mathAjax(params,url,function(data){
			var obj = data.data;
			var layer = layerIsExist('buildGeometryLayer',global.map);
			if(layer){
				clearBuildGeometryLayer();
			}
			var pointDataList = obj.mathGranularVoLsit;
			for(var i=0;i<pointDataList.length;i++){
				var pointsObj = {};
				pointsObj.coords = eval('(' + pointDataList[i] + ')');
				pointsObj.isBorder = false;
				pointsObj.borderColor = '';
				pointsObj.fillColor = iteamsColor[i];
				pointsObj.fillTransparency = '0.6';
				if(pointsObj.coords.length>1){
					buildGeometryByCoords(pointsObj.coords,pointsObj.isBorder,pointsObj.borderColor,pointsObj.fillColor,pointsObj.fillTransparency);
				}
			}
			$("#zhmnKSSJDivOut").show();
			 gotoPointView(zhmnAccidentPoint[0],zhmnAccidentPoint[1],18);
	 });
}

//获取传感器坐标
function getCgqLON(oid,name){
	var params ={};
	params.oid=oid;
	params.catecd="sensor";
	params.clusterType='esm27q';
	params.imagePath="/mapIcon/mapFixedIcon/qtIcon.png";
	params.isCluster=false;
	params.clusterImagePath="/mapIcon/iconCluster/qtIcon_c.png";
	params.isSave=true;
	params.name = name;//设备名称
	params.recordType='sensor';
	params.action="传感器位置";
	addMakerEngine(params);
}

//罐区灭火剂用量计算

// 立式油罐
// volume 容积, diameter 直径, height 高度, perimeter 周长, sectional_area 截面积, circle_area 环状面积
var vertical_tanks = [
{ "volume":"100" , "diameter":"5.17" , "height":"5.3" , "perimeter":"" , "sectional_area":"" , "circle_area":"" },
{ "volume":"200" , "diameter":"6.62" , "height":"6.47" , "perimeter":"" , "sectional_area":"" , "circle_area":"" },
{ "volume":"300" , "diameter":"7.75" , "height":"7.07" , "perimeter":"" , "sectional_area":"" , "circle_area":"" },
{ "volume":"400" , "diameter":"8.29" , "height":"8.24" , "perimeter":"" , "sectional_area":"" , "circle_area":"" },
{ "volume":"500" , "diameter":"9.98" , "height":"8.81" , "perimeter":"" , "sectional_area":"" , "circle_area":"" },
{ "volume":"700" , "diameter":"10.26" , "height":"9.41" , "perimeter":"" , "sectional_area":"" , "circle_area":"" },
{ "volume":"1000" , "diameter":"11.58" , "height":"10.58" , "perimeter":"" , "sectional_area":"" , "circle_area":"" },
{ "volume":"2000" , "diameter":"15.78" , "height":"11.37" , "perimeter":"" , "sectional_area":"" , "circle_area":"" },
{ "volume":"3000" , "diameter":"18.99" , "height":"11.76" , "perimeter":"" , "sectional_area":"" , "circle_area":"" },
{ "volume":"5000" , "diameter":"23.76" , "height":"12.52" , "perimeter":"" , "sectional_area":"" , "circle_area":"" },
{ "volume":"10000" , "diameter":"31.29" , "height":"14.46" , "perimeter":"" , "sectional_area":"" , "circle_area":"" },
{ "volume":"20000" , "diameter":"40.5" , "height":"15.85" , "perimeter":"" , "sectional_area":"" , "circle_area":"148" },
{ "volume":"50000" , "diameter":"60" , "height":"19.35" , "perimeter":"" , "sectional_area":"" , "circle_area":"222" },
{ "volume":"100000" , "diameter":"80" , "height":"22" , "perimeter":"" , "sectional_area":"" , "circle_area":"211" },
{ "volume":"150000" , "diameter":"96" , "height":"22" , "perimeter":"" , "sectional_area":"" , "circle_area":"265" }
];

// 罐周长
var tank_perimeter = 16.24;
// 截面积
var sectional_area = 20.99;

// 灭火供给强度
var mix_liquid_supply_q_total = 0;
var mix_liquid_supply_q_1 = 0;
// 冷却供给强度
var cooling_water_supply_q_total = 0;
var cooling_water_supply_q_1 = 0;
// 冷却邻近罐供给强度
// 同样的邻近罐
var adj_cooling_water_supply_q_total = 0;
var adj_cooling_water_supply_q_1 = 0;
//邻近罐数量
var adjacent_num = 0;
// 8种不同邻近罐
var array_adj_cooling_water_supply_q_total = [0,0,0,0,0,0,0,0];
var array_adj_cooling_water_supply_q_1 = [0,0,0,0,0,0,0,0];
var array_adj_tank_num = [0,0,0,0,0,0,0,0];
var array_adj_total = 0;

// 是否简单计算界面
var is_show_jdDiv = true;

// 着火罐容积或者直径测量值发生变化, 重新计算几个参数
function tank_params_changed ()
{
	var volume = $("#tank_volume_select").val();
	var measure_diameter = $("#measure_diameter_id").val();
	var diameter = parseFloat(measure_diameter);
	for (var i = 0; i < vertical_tanks.length; i++)
	{
		if (volume == vertical_tanks[i].volume)
		{
			$("#span_diameter_id").html(vertical_tanks[i].diameter);
			$("#span_height_id").html(vertical_tanks[i].height);
			if (diameter == 0)
			{
				diameter = parseFloat(vertical_tanks[i].diameter);
			}
			break;
		}
	}
	
	tank_perimeter = 3.141592654 * diameter;
	sectional_area = 3.141592654 * diameter * diameter / 2 / 2;

	$("#span_perimeter_id").html(tank_perimeter.toFixed(2));
	$("#span_sectional_area_id").html(sectional_area.toFixed(2));
}

// 计算着火罐的泡沫灭火剂和水的供给强度
// tank_type 储罐类型
// oil_type 介质
// if_redundant 是否考虑供给保全值
function foam_foamite (tank_type, oil_type, if_redundant)
{
	// 混合液供给强度
	var mix_liquid_supply_q = 1 * 60 / 6.25;
	if (oil_type == "有机溶剂")
	{
		mix_liquid_supply_q = 12;
	}
	if (if_redundant)
	{
		mix_liquid_supply_q = mix_liquid_supply_q * 1.2;
	}

	var mix_liquid_q = mix_liquid_supply_q * sectional_area;
	var foam = mix_liquid_q * 0.06;
	var foam_water = mix_liquid_q * 0.94;

	var cooling_water_supply = 0.8;
	if (tank_type == "固定顶")
	{
		cooling_water_supply = 1.0;
	}
	var cooling_water = cooling_water_supply * tank_perimeter;

	return {
		'foam': foam,	// 泡沫原液供给强度
		'foam_water': foam_water,	// 灭火用水供给强度
		'cooling_water': cooling_water	// 冷却用水供给强度
	}; 
}

// 计算临近罐冷却用水量供给强度
function cooling_adjacent_tank (tank_type, perimeter)
{
	var cooling_water_supply = 0.6;
	if (tank_type == "固定顶")
	{
		cooling_water_supply = 0.8;
	}
	var cooling_water = cooling_water_supply * perimeter / 2;

	return cooling_water;
}

// 重新计算消防力量
function reset_fireHydrant()
{
	for (var i = 1; i <= 5; i++)
	{
		$("#outfire_num_"+i).html("0");
		$("#cooling_num_"+i).html("0");
		if (is_show_jdDiv)
		{
			$("#adj_num_"+i).html("0");
		}
		else
		{
			for(var j = 1; j <= array_adj_total; j++)
			{
				$("#adj_num_"+i+'_'+j).html("0");
			}
		}
	}

	mix_liquid_supply_q_1 = mix_liquid_supply_q_total;
	cooling_water_supply_q_1 = cooling_water_supply_q_total;

	var strq = $("#FireHydrant_type_select_1").val();
	var q = parseFloat(strq);
	if(q==0){
		$("#outfire_num_1").html("0");
		$("#cooling_num_1").html("0");
	}else{
		var outfire_num1 = mix_liquid_supply_q_total / q;
		var cooling_num1 = cooling_water_supply_q_total / q;
	
		$("#outfire_num_1").html(Math.ceil(outfire_num1));
		$("#cooling_num_1").html(Math.ceil(cooling_num1));
	}

	if (is_show_jdDiv)
	{
		adj_cooling_water_supply_q_1 = adj_cooling_water_supply_q_total;
		if(q==0){
			$("#adj_num_1").html("0");
		}else{
			var adj_num1 = adj_cooling_water_supply_q_1 / q;
			$("#adj_num_1").html(Math.ceil(adj_num1)*adjacent_num);
		}
		
	}
	else
	{
		for(var j = 1; j <= array_adj_total; j++)
		{
			array_adj_cooling_water_supply_q_1[j-1] = array_adj_cooling_water_supply_q_total[j-1];
			if(q==0){
				$("#adj_num_1_"+j).html("0");
			}else{
				var fire_num = array_adj_cooling_water_supply_q_1[j-1] / q;
				$("#adj_num_1_"+j).html(Math.ceil(fire_num)*array_adj_tank_num[j-1]);
			}
		}
	}
}

//点击事件
function click_calc_foam_foamite()
{
	if(!checkIsEmpty('gqmhjtjCs')){
		layer.msg("请检查各个输入项是否输入", {time: 2000});
		return false;
	}
	if($("#outfire_time").val()=='0'){
		layer.msg("一次进攻时间不能为0", {time: 2000});
		return false;
	}
	
//	if(is_show_jdDiv){
//		if($("#adjacent_tank_num").val()=='0'){
//			layer.msg("临近罐数量不能为0", {time: 2000});
//			return false;
//		}
//	}
	
	$("#click_calc_foam_foamiteDiv").show();
	drawGqmhjJSTable();
	var tank_type = $("#tank_type_select").val();
	var oil_type = $("#oil_type_select").val();
	var if_redundant = $("#if_redundant").prop('checked');
	var str_outfire_time = $("#outfire_time").val();
	var fire_tank_num = 1;//$("#fire_tank_num").val();
	var adjacent_tank_num = $("#adjacent_tank_num").val();
	calc_foam_foamite(tank_type, oil_type, if_redundant, str_outfire_time, fire_tank_num, adjacent_tank_num);
	$("#gqmhjtjCs").hide();
}

// 计算灭火剂用量
function calc_foam_foamite(tank_type, oil_type, if_redundant, str_outfire_time, fire_tank_num, adjacent_tank_num)
{
	// 灭火时间, 秒
	var outfire_time = parseFloat(str_outfire_time);
	// 着火罐数量
	var fire_num = parseInt(fire_tank_num);

	// 着火罐的泡沫灭火剂和水的供给强度
	var ret = foam_foamite(tank_type, oil_type, if_redundant);
	ret.foam = ret.foam * fire_num;
	ret.foam_water = ret.foam_water * fire_num;
	ret.cooling_water = ret.cooling_water * fire_num;

	// 灭火泡沫
	$("#span_foam_q").html(ret.foam.toFixed(2));
	$("#span_foam_amount").html((ret.foam * outfire_time / 1000).toFixed(2));
	// 灭火用水
	$("#span_foam_water_q").html(ret.foam_water.toFixed(2));
	$("#span_foam_water_amount").html((ret.foam_water * outfire_time / 1000).toFixed(2));
	// 冷却着火罐用水
	$("#span_cooling_water_q").html(ret.cooling_water.toFixed(2));
	$("#span_cooling_water_amount").html((ret.cooling_water * outfire_time * 60 / 1000).toFixed(2));

	// 计算灭火器具
	mix_liquid_supply_q_total = (ret.foam + ret.foam_water) / 60;
	cooling_water_supply_q_total = ret.cooling_water;

	if (is_show_jdDiv)
	{
		// 邻近罐数量
		adjacent_num = parseInt(adjacent_tank_num);

		// 临近罐冷却用水量供给强度
		var adjacent_cooling_water = cooling_adjacent_tank(tank_type, tank_perimeter);
		var adjacent_cooling_water_amount = adjacent_cooling_water * adjacent_num;

		$("#span_adjacent_cooling_water_q").html(adjacent_cooling_water_amount.toFixed(2));
		$("#span_adjacent_cooling_water_amount").html((adjacent_cooling_water_amount * outfire_time * 60 / 1000).toFixed(2));
		adj_cooling_water_supply_q_total = adjacent_cooling_water;
	}
	else
	{
		for(var k = 0; k < 8; k++)
		{
			array_adj_cooling_water_supply_q_total[k] = 0;
			array_adj_cooling_water_supply_q_1[k] = 0;
			array_adj_tank_num[k] = 0;
		}

		var amount = 0;
		var adjacent_cooling_water_q = 0;
		for(var j = 1; j <= 8; j++)
		{
			var adja_num = parseInt($("#adjacent_num_" + j).val());
			
			if (adja_num > 0)
			{
				array_adj_tank_num[amount] = adja_num;
				var diameter = 0;
				var cooling_adj_volume = $("#adj_tank_volume_select_" + j).val();
				for (var i = 0; i < vertical_tanks.length; i++)
				{
					if (cooling_adj_volume == vertical_tanks[i].volume)
					{
						diameter = parseFloat(vertical_tanks[i].diameter);
						break;
					}
				}
				var perimeter = 3.141592654 * diameter;
				var adjacent_cooling_water = cooling_adjacent_tank(tank_type, perimeter);
				array_adj_cooling_water_supply_q_total[amount] = adjacent_cooling_water;
				
				adjacent_cooling_water_q = adjacent_cooling_water_q + adjacent_cooling_water * adja_num;
				
				amount++;
			}
		}

		$("#span_adjacent_cooling_water_q").html(adjacent_cooling_water_q.toFixed(2));
		$("#span_adjacent_cooling_water_amount").html((adjacent_cooling_water_q * outfire_time * 60 / 1000).toFixed(2));
	}

	reset_fireHydrant();
	calc_total_fireHydrant();
}

// 灭火器具减一
function calc_outfire_decrease(index)
{
	var outfire_cur = parseInt($("#outfire_num_" + index).html());
	if (outfire_cur == 0)
	{
		return;
	}
	var outfire_num = outfire_cur - 1;
	$("#outfire_num_" + index).html(outfire_num);
	var outfire_q_cur = parseFloat($("#FireHydrant_type_select_" + index).val());
	mix_liquid_supply_q_1 = mix_liquid_supply_q_1 + outfire_q_cur;
	var outfire_q_1 = parseFloat($("#FireHydrant_type_select_1").val());
	if(outfire_q_1==0){
		$("#outfire_num_1").html("0");
	}else{
		var outfire_1 = mix_liquid_supply_q_1 / outfire_q_1;
		$("#outfire_num_1").html(Math.ceil(outfire_1));
	}
	
	calc_total_fireHydrant()
}
// 灭火器具加一
function calc_outfire_increase(index)
{
	var outfire_q_cur = parseFloat($("#FireHydrant_type_select_" + index).val());
	var outfire_cur = parseInt($("#outfire_num_" + index).html());

	var new_mix_liquid_supply_q_1 = mix_liquid_supply_q_1 - outfire_q_cur;
	if (new_mix_liquid_supply_q_1 > 0)
	{
		var outfire_q_1 = parseFloat($("#FireHydrant_type_select_1").val());
		if(outfire_q_1==0){
			$("#outfire_num_1").html("0");
		}else{
			$("#outfire_num_1").html(Math.ceil(new_mix_liquid_supply_q_1 / outfire_q_1));
		}
		$("#outfire_num_" + index).html(outfire_cur + 1);
		mix_liquid_supply_q_1 = new_mix_liquid_supply_q_1;
	}
	
	calc_total_fireHydrant()
}
// 冷却器具减一
function calc_cooling_decrease(index)
{
	var cooling_cur = parseInt($("#cooling_num_" + index).html());
	if (cooling_cur == 0)
	{
		return;
	}
	var cooling_num = cooling_cur - 1;
	document.getElementById('cooling_num_' + index).innerText = cooling_num;
	var cooling_q_cur = parseFloat($("#FireHydrant_type_select_" + index).val());
	cooling_water_supply_q_1 = cooling_water_supply_q_1 + cooling_q_cur;
	var cooling_q_1 = parseFloat($("#FireHydrant_type_select_1").val());
	if(cooling_q_1==0){
		$("#cooling_num_1").html("0");
	}else{
		var cooling_1 = cooling_water_supply_q_1 / cooling_q_1;
		$("#cooling_num_1").html(Math.ceil(cooling_1));
	}
	
	calc_total_fireHydrant()
}
// 冷却器具加一
function calc_cooling_increase(index)
{
	var cooling_q_cur = parseFloat($("#FireHydrant_type_select_" + index).val());
	var cooling_cur = parseInt($("#cooling_num_" + index).html());

	if (cooling_water_supply_q_1 > 0)
	{
		var new_cooling_water_supply_q_1 = cooling_water_supply_q_1 - cooling_q_cur;
		//if (new_cooling_water_supply_q_1 > 0)
		{
			var cooling_q_1 = parseFloat($("#FireHydrant_type_select_1").val());
			var new_num = Math.ceil(new_cooling_water_supply_q_1 / cooling_q_1);
			if (new_num < 0)
			{
				new_num = 0;
			}
			$("#cooling_num_1").html(new_num);
			$("#cooling_num_" + index).html(cooling_cur + 1);
			cooling_water_supply_q_1 = new_cooling_water_supply_q_1;
		}
	}
	
	calc_total_fireHydrant()
}

// 冷却邻近罐器具减一
function calc_adj_decrease(index)
{
	var adj_cur = parseInt($("#adj_num_" + index).html());
	if (adj_cur == 0)
	{
		return;
	}

	if (is_show_jdDiv)
	{
		$("#adj_num_" + index).html(adj_cur - adjacent_num);
		var adj_q_cur = parseFloat($("#FireHydrant_type_select_" + index).val());
		adj_cooling_water_supply_q_1 = adj_cooling_water_supply_q_1 + adj_q_cur;
		var adj_q_1 = parseFloat($("#FireHydrant_type_select_1").val());
		var adj_1 = adj_cooling_water_supply_q_1 / adj_q_1;
		$("#adj_num_1").html(Math.ceil(adj_1)*adjacent_num);
	}
	else
	{
		var strs = index.split("_");
		var j = parseInt(strs[1]);

		$("#adj_num_"+index).html((adj_cur-array_adj_tank_num[j-1]));
		var adj_q_cur = parseFloat($("#FireHydrant_type_select_" + strs[0]).val());
		array_adj_cooling_water_supply_q_1[j-1] = array_adj_cooling_water_supply_q_1[j-1] + adj_q_cur;
		var adj_q_1 = parseFloat($("#FireHydrant_type_select_1").val());
		if(adj_q_1==0){
			$("#adj_num_1_"+j).html("0");
		}else{
			var adj_1 = array_adj_cooling_water_supply_q_1[j-1] / adj_q_1;
			$("#adj_num_1_"+j).html(Math.ceil(adj_1)*array_adj_tank_num[j-1]);
		}
	}
	
	calc_total_fireHydrant()
}
// 冷却邻近罐器具加一
function calc_adj_increase(index)
{
	if (is_show_jdDiv)
	{
		var adj_q_cur = parseFloat($("#FireHydrant_type_select_" + index).val());
		if (adj_cooling_water_supply_q_1 > 0)
		{
			var new_adj_cooling_water_supply_q_1 = adj_cooling_water_supply_q_1 - adj_q_cur;
			//if (new_adj_cooling_water_supply_q_1 > 0)
			{
				var adj_q_1 = parseFloat($("#FireHydrant_type_select_1").val());
				var new_num = Math.ceil(new_adj_cooling_water_supply_q_1 / adj_q_1)*adjacent_num;
				if (new_num < 0)
				{
					new_num = 0;
				}
				$("#adj_num_1").html(new_num);
				var adj_cur = parseInt($("#adj_num_" + index).html());
				$("#adj_num_" + index).html((adj_cur + adjacent_num));
				adj_cooling_water_supply_q_1 = new_adj_cooling_water_supply_q_1;
			}
		}
	}
	else
	{
		var strs = index.split("_");
		var j = parseInt(strs[1]);
		var adj_q_cur = parseFloat($("#FireHydrant_type_select_" + strs[0]).val());
		if (array_adj_cooling_water_supply_q_1[j-1] > 0)
		{
			var new_array_adj_cooling_water_supply_q_1 = array_adj_cooling_water_supply_q_1[j-1] - adj_q_cur;
			//if (new_array_adj_cooling_water_supply_q_1 > 0)
			{
				var adj_q_1 = parseFloat($("#FireHydrant_type_select_1").val());
				var adj_1 = new_array_adj_cooling_water_supply_q_1 / adj_q_1;
				var new_num = Math.ceil(adj_1)*array_adj_tank_num[j-1];
				if (new_num < 0)
				{
					new_num = 0;
				}
				$("#adj_num_1_"+j).html(new_num);
				var adj_cur = parseInt($("#adj_num_" + index).html());
				$("#adj_num_"+index).html((adj_cur+array_adj_tank_num[j-1]));
				array_adj_cooling_water_supply_q_1[j-1] = new_array_adj_cooling_water_supply_q_1;
			}
		}
	}
	
	calc_total_fireHydrant()
}
// 灭火冷却器具发生变化
function fireHydrant_type_changed(index)
{
	if (index == 1)
	{
		reset_fireHydrant();
	}
	else
	{
		var outfire_cur = parseInt($("#outfire_num_" + index).html());
		var outfire_q_cur = parseFloat($("#FireHydrant_type_select_" + index).val());
		mix_liquid_supply_q_1 = mix_liquid_supply_q_1 + outfire_q_cur * outfire_cur;
		var outfire_q_1 = parseFloat($("#FireHydrant_type_select_1").val());
		if(outfire_q_1==0){
			$("#outfire_num_1").html("0");
		}else{
			var outfire_1 = mix_liquid_supply_q_1 / outfire_q_1;
			$("#outfire_num_1").html(Math.ceil(outfire_1));
		}
		$("#outfire_num_" + index).html("0");

		var cooling_cur = parseInt($("#cooling_num_" + index).html());
		var cooling_q_cur = parseFloat($("#FireHydrant_type_select_" + index).val());
		cooling_water_supply_q_1 = cooling_water_supply_q_1 + cooling_q_cur * cooling_cur;
		var cooling_q_1 = parseFloat($("#FireHydrant_type_select_1").val());
		if(cooling_q_1==0){
			$("#cooling_num_1").html("0");
		}else{
			var cooling_1 = cooling_water_supply_q_1 / cooling_q_1;
			$("#cooling_num_1").html(Math.ceil(cooling_1));
		}
		
		$("#cooling_num_" + index).html("0");

		if (is_show_jdDiv)
		{
			var adj_cur = parseInt($("#adj_num_" + index).html());
			if (adjacent_num > 0){
				adj_cur = adj_cur / adjacent_num;
			}
			var adj_q_cur = parseFloat($("#FireHydrant_type_select_" + index).val());
			adj_cooling_water_supply_q_1 = adj_cooling_water_supply_q_1 + adj_q_cur * adj_cur;
			var adj_q_1 = parseFloat($("#FireHydrant_type_select_1").val());
			var adj_1 = adj_cooling_water_supply_q_1 / adj_q_1;
			$("#adj_num_1").html(Math.ceil(adj_1)*adjacent_num);
			$("#adj_num_" + index).html("0");
		}
		else
		{
			for(var j = 1; j <= array_adj_total; j++)
			{
				var adj_cur = parseInt($("#adj_num_" + index + "_" + j).html());
				adj_cur = adj_cur / array_adj_tank_num[j-1];
				var adj_q_cur = parseFloat($("#FireHydrant_type_select_" + index).val());
				array_adj_cooling_water_supply_q_1[j-1] = array_adj_cooling_water_supply_q_1[j-1] + adj_q_cur * adj_cur;
				var adj_q_1 = parseFloat($("#FireHydrant_type_select_1").val());
				if(adj_q_1==0){
					$("#adj_num_1_"+j).html("0");
				}else{
					var adj_1 = array_adj_cooling_water_supply_q_1[j-1] / adj_q_1;
					$("#adj_num_1_"+j).html(Math.ceil(adj_1)*array_adj_tank_num[j-1]);	
				}
				$("#adj_num_"+index+"_"+j).html("0");
			}
		}
	}
	calc_total_fireHydrant();
}

// 计算合计
function calc_total_fireHydrant()
{
	for (var i = 1; i <= 5; i++)
	{
		var outfire_cur = parseInt($("#outfire_num_" + i).html());
		var cooling_cur = parseInt($("#cooling_num_" + i).html());

		var total = outfire_cur + cooling_cur;

		if (is_show_jdDiv)
		{
			if (adjacent_num > 0){
				var adj_cur = parseInt($("#adj_num_" + i).html());
				total = total + adj_cur;
			}
		}
		else
		{
			for(var j = 1; j <= array_adj_total; j++)
			{
				var adj_cooling = parseInt($("#adj_num_"+i+'_'+j).html());
				total = total + adj_cooling;
			}
		}
		
		$("#fireHydrant_total_"+i).html(total);
	}
}

//罐区灭火剂计算界面
function drawGqmhjJSTable(){
	var selectList = [];
	$("select[name='ljgslSelect']").each(  
		function(){  
			if($(this).val()!=0){
				selectList.push($(this).attr("tg"));
			}
		}  
	);
	
	array_adj_total = selectList.length;
	
	var htmlOp = "";
	for(var j=0;j<waterSupplyEquipment.length;j++){
		var obj = waterSupplyEquipment[j];
		//foamAvailable=1 是喷泡沫,2是喷水
		htmlOp += '<option value="'+obj.flow+'" tg='+obj.foamAvailable+'>'+obj.name+'('+obj.flow+'L/s)</option>';
	}
	var html1="<thead><tr><th rowspan='2'>序号</th>	<th rowspan='2'>灭火器具</th><th colspan='2'>着火罐的灭火剂用量</th>";

	// 简单计算  
	if (is_show_jdDiv){
		var adjacent_tank_num = parseInt($("#adjacent_tank_num").val());
		if(adjacent_tank_num>0){
			html1+="<th rowspan='2'>临近罐的灭火剂用量</th>";
		}

		html1+="<th rowspan='2'>总计</th></tr>";
		var html="<tr><th>灭火</th><th>冷却</th>";
		html+="</thead><tbody>";
		var html2 = "";
		for(var x = 1;x<6;x++){
			//alert(333);
			html2+='<tr><td>'+x+'</td><td>';
			if (x==1){
				html2+='<select class="dTab1_select tst216" id="FireHydrant_type_select_'+x+'" onchange="fireHydrant_type_changed('+x+');">';
			}
			else{
				html2+='<select class="dTab1_select tst216" id="FireHydrant_type_select_'+x+'" onClick="fireHydrant_type_changed('+x+');">';
			}
			html2+=htmlOp;
			html2+='</select>';
			html2+='</td><td>';
			if(x!=1){
				html2+='<input type="button" value="-" onclick="calc_outfire_decrease(\''+x+'\');" class="adddTo fl">';
			}
			html2+='<span id="outfire_num_'+x+'" class="zreo fl">0</span>';
			if(x!=1){
				html2+='<input type="button" value="+" onclick="calc_outfire_increase(\''+x+'\');" class="reduceTo fl">';
			}
			html2+='</div>';
			html2+='</td>';
			html2+='<td>';
			html2+='<div class="fireNumber ca cf">';
			if(x!=1){
				html2+='<input type="button" value="-" onclick="calc_cooling_decrease(\''+x+'\');" class="adddTo fl">';
			}
			html2+='<span id="cooling_num_'+x+'" class="zreo fl">0</span>';
			if(x!=1){
				html2+='<input type="button" value="+" onclick="calc_cooling_increase(\''+x+'\');" class="reduceTo fl">';
			}
			html2+='</div>';
			html2+='</td>';

			if (adjacent_tank_num > 0){
				html2+='<td>';
				html2+='<div class="fireNumber ca cf">';
				if(x!=1){
					html2+='<input type="button" value="-" onclick="calc_adj_decrease(\''+x+'\');" class="adddTo fl">';
				}
				html2+='<span id="adj_num_'+x+'" class="zreo fl">0</span>';
				if(x!=1){
					html2+='<input type="button" value="+" onclick="calc_adj_increase(\''+x+'\');" class="reduceTo fl">';
				}
				html2+='</div>';
				html2+='</td>';
			}
			html2+='<td class="redColor" id="fireHydrant_total_'+x+'">0</td></tr>';
		}
	}
	else{	//高级计算
		if(selectList.length>0){
			html1+="<th colspan='"+selectList.length+"'>临近罐的灭火剂用量</th>";
		}
		
		html1+="<th rowspan='2'>总计</th></tr>";
		var html="<tr><th>灭火</th><th>冷却</th>";
		for(var i=0;i<selectList.length;i++){
			var flag = selectList[i];
			if(flag=="nw"){
				html+="<th>西北</th>";
			}else if(flag=="n"){
				html+="<th>北</th>";
			}else if(flag=="ne"){
				html+="<th>东北</th>";
			}else if(flag=="e"){
				html+="<th>东</th>";
			}else if(flag=="w"){
				html+="<th>西</th>";
			}else if(flag=="sw"){
				html+="<th>西南</th>";
			}else if(flag=="s"){
				html+="<th>南</th>";
			}else if(flag=="se"){
				html+="<th>东南</th>";
			}
		}
		html+="</thead><tbody>";
		var html2 = "";
		for(var x = 1;x<6;x++){
			if(x==1){
				html2+='<tr><td>'+x+'</td><td>';
				html2+='<select class="dTab1_select tst216" id="FireHydrant_type_select_'+x+'" onchange="fireHydrant_type_changed('+x+');">';
				html2+=htmlOp;
				html2+='</select></td><td>';
				html2+='<div class="fireNumber ca cf">';
				html2+='<span id="outfire_num_'+x+'" class="zreo fl">0</span>';
				html2+='</div>';
				html2+='</td>';
				html2+='<td>';
				html2+='<div class="fireNumber ca cf">';
				html2+='<span id="cooling_num_'+x+'" class="zreo fl">0</span>';
				html2+='</div>';
				html2+='</td>';
				//循环选中的邻近罐，设置td的列 
				for(var c=0;c<selectList.length;c++){
					html2+='<td>';
					html2+='<div class="fireNumber ca cf">';
					html2+='<span id="adj_num_'+x+'_'+(c+1)+'" class="zreo fl">0</span>';
					html2+='</div>';
					html2+='</td>';
				}
				html2+='<td class="redColor" id="fireHydrant_total_'+x+'">0</td></tr>';
			}else{
				html2+='<tr><td>'+x+'</td><td>';
				html2+='<select class="dTab1_select tst216" id="FireHydrant_type_select_'+x+'" onClick="fireHydrant_type_changed('+x+');">';
				html2+=htmlOp;
				html2+='</select></td><td>';
				html2+='<div class="fireNumber ca cf">';
				html2+='<input type="button" value="-" onclick="calc_outfire_decrease(\''+x+'\');" class="adddTo fl">';
				html2+='<span id="outfire_num_'+x+'" class="zreo fl">0</span>';
				html2+='<input type="button" value="+" onclick="calc_outfire_increase(\''+x+'\');" class="reduceTo fl">';
				html2+='</div>';
				html2+='</td>';
				html2+='<td>';
				html2+='<div class="fireNumber ca cf">';
				html2+='<input type="button" value="-" onclick="calc_cooling_decrease(\''+x+'\');" class="adddTo fl">';
				html2+='<span id="cooling_num_'+x+'" class="zreo fl">0</span>';
				html2+='<input type="button" value="+" onclick="calc_cooling_increase(\''+x+'\');" class="reduceTo fl">';
				html2+='</div>';
				html2+='</td>';
				//循环选中的邻近罐，设置td的列
				for(var c=0;c<selectList.length;c++){
					html2+='<td>';
					html2+='<div class="fireNumber ca cf">';
					html2+='<input type="button" value="-" onclick="calc_adj_decrease(\''+x+'_'+(c+1)+'\');" class="adddTo fl">';
					html2+='<span id="adj_num_'+x+'_'+(c+1)+'" class="zreo fl">0</span>';
					html2+='<input type="button" value="+" onclick="calc_adj_increase(\''+x+'_'+(c+1)+'\');" class="reduceTo fl">';
					html2+='</div>';
					html2+='</td>';
				}
				html2+='<td class="redColor" id="fireHydrant_total_'+x+'">0</td></tr>';
			}
		}
	}
	var indexHtml=html1+html+html2+"</tbody>";
	$("#gqmhjJSTable").html(indexHtml);
}

function changeShowGqmhjDiv(showId,hideId){
	if (showId == "gqmhjyljs_jdDiv")
	{
		is_show_jdDiv = true;
		$("#gqmhjyljsTab1").attr("class","activeLink");
		$("#gqmhjyljsTab2").attr("class","");
	}
	else
	{
		is_show_jdDiv = false;
		$("#gqmhjyljsTab2").attr("class","activeLink");
		$("#gqmhjyljsTab1").attr("class","");
	}
	$("#"+showId).show();
	$("#"+hideId).hide();
}

//重质油品喷溅时间计算结束后收起参数设置
function zhyppjShouqiSrcs(){	
	$("#yygqfyModelDivConTent").toggle();
	var text=$("#yygqfyModelDivHeadBox span").text();
	if(text=="展开"){
		$("#yygqfyModelDivHeadBox span").text("收起");
		$("#yygqfyModelDivHeadBox i").removeClass("stopUpIconDown");
	}else{
		$("#yygqfyModelDivHeadBox span").text("展开");
		$("#yygqfyModelDivHeadBox i").addClass("stopUpIconDown");
	}	
}

//耐火极限计算结束后收起参数设置
function nhjxjShouqiSrcs(){	
	$("#nhjxModelDivCont").toggle();
	var text=$("#nhjxModelDivHeadBox span").text();
	if(text=="展开"){
		$("#nhjxModelDivHeadBox span").text("收起");
		$("#nhjxModelDivHeadBox i").removeClass("stopUpIconDown");
	}else{
		$("#nhjxModelDivHeadBox span").text("展开");
		$("#nhjxModelDivHeadBox i").addClass("stopUpIconDown");
	}	
}

//热辐射计算结束后收起参数设置
function rfsShouqiSrcs(){	
	$("#rfsModelDivCont").toggle();
	var text=$("#rfsModelDivDivHeadBox span").text();
	if(text=="展开"){
		$("#rfsModelDivDivHeadBox span").text("收起");
		$("#rfsModelDivDivHeadBox i").removeClass("stopUpIconDown");
	}else{
		$("#rfsModelDivDivHeadBox span").text("展开");
		$("#rfsModelDivDivHeadBox i").addClass("stopUpIconDown");
	}	
}
//罐区灭火剂计算结束后收起参数设置
function shouqiSrcs(){	
	$("#gqmhjtjCs").toggle();
	var text=$("#countHeadBox span").text();
	if(text=="展开"){
		$("#countHeadBox span").text("收起");
		$("#countHeadBox i").removeClass("stopUpIconDown");
	}else{
		$("#countHeadBox span").text("展开");
		$("#countHeadBox i").addClass("stopUpIconDown");
	}	
}

//高级反算计算结束后收起参数设置
function gjfsShouqiSrcs(){	
	$("#gjfsModelDivContent").toggle();
	var text=$("#gjfsModelDivHeadBox span").text();
	if(text=="展开"){
		$("#gjfsModelDivHeadBox span").text("收起");
		$("#gjfsModelDivHeadBox i").removeClass("stopUpIconDown");
	}else{
		$("#gjfsModelDivHeadBox span").text("展开");
		$("#gjfsModelDivHeadBox i").addClass("stopUpIconDown");
	}	
}

//泄漏源定位计算结束后收起参数设置
function xlydwShouqiSrcs(){	
	$("#xlydwModelDivContent").toggle();
	var text=$("#xlydwModelDivHeadBox span").text();
	if(text=="展开"){
		$("#xlydwModelDivHeadBox span").text("收起");
		$("#xlydwModelDivHeadBox i").removeClass("stopUpIconDown");
	}else{
		$("#xlydwModelDivHeadBox span").text("展开");
		$("#xlydwModelDivHeadBox i").addClass("stopUpIconDown");
		
	}	
}

function openXFSSDetailForMark(oid,flow,e){
	var flag = $(e).val();
	if(typeof(flow)==undefined || flow=='undefined'){
		flow=0;
	}
	flow = parseFloat(flow);
	var zddwCQZLKYLL = parseFloat($("#zddwCQZLKYLL").html());
	if(zddwCQZLKYLL<flow){
		layer.confirm('厂区供水能力已不足，继续开启流量只有'+zddwCQZLKYLL+'(L/S)', {
	        btn: ['确定','取消'] //按钮
	   },function(){
		   layer.closeAll('dialog');
		   flow = zddwCQZLKYLL;
		   var setCustomValueXFSYKQ = parseFloat($("#setCustomValueXFSYKQ").html());
			$("#setCustomValueXFSYKQ").text(flow);
			var zddwCQZL = parseFloat($("#zddwCQZL").html());
			var zddwCQZLXFLL = parseFloat($("#zddwCQZLXFLL").html());
			if(flag=='全开'){
				if(zddwCQZL>0){
					zddwCQZLXFLL = zddwCQZLXFLL+flow-setCustomValueXFSYKQ;
					$("#zddwCQZLXFLL").html(zddwCQZLXFLL);
					$("#zddwCQZLKYLL").html(zddwCQZL-zddwCQZLXFLL);
				}
				if(global.changeFeature) {
					global.changeFeature.setProperties({'catecd':'xfsyhui','type':'xfsyhui'});
					global.changeFeature.changed();
				}
				
			}else if(flag=='半开'){
				if(zddwCQZL>0){
					zddwCQZLXFLL = zddwCQZLXFLL+flow-setCustomValueXFSYKQ;
					$("#zddwCQZLXFLL").html(zddwCQZLXFLL);
					$("#zddwCQZLKYLL").html(zddwCQZL-zddwCQZLXFLL);
				}
				if(global.changeFeature) {
					global.changeFeature.setProperties({'catecd':'xfsyhui','type':'xfsyhui'});
					global.changeFeature.changed();
				}
			}else{
				if(zddwCQZL>0){
					zddwCQZLXFLL = zddwCQZLXFLL-flow-setCustomValueXFSYKQ;
					$("#zddwCQZLXFLL").html(zddwCQZLXFLL);
					$("#zddwCQZLKYLL").html(zddwCQZL+zddwCQZLXFLL);
				}
				if(global.changeFeature) {
					global.changeFeature.setProperties({'catecd':'xfsy','type':'xfsy'});
					global.changeFeature.changed();
				}
			}
	   }, function(){
	   		layer.msg('已取消', {icon: 1, time: 1000});
	   		return;
	   });
	}else{
		var setCustomValueXFSYKQ = parseFloat($("#setCustomValueXFSYKQ").html());
		$("#setCustomValueXFSYKQ").text(flow);
//		var setCustomValueXFS = $("#setCustomValueXFS").val();
//		if(setCustomValueXFS!=""){
//			var flow1 = parseFloat($("#setCustomValueXFS").val());
//			if(flow1>flow){
//				layer.msg("请输入合理流量值", {time: 1000});
//				return false;
//			}
//			flow = flow1;
//		}
		var zddwCQZL = parseFloat($("#zddwCQZL").html());
		var zddwCQZLXFLL = parseFloat($("#zddwCQZLXFLL").html());
		if(flag=='全开'){
			if(zddwCQZL>0){
				zddwCQZLXFLL = zddwCQZLXFLL+flow-setCustomValueXFSYKQ;
				$("#zddwCQZLXFLL").html(zddwCQZLXFLL);
				$("#zddwCQZLKYLL").html(zddwCQZL-zddwCQZLXFLL);
			}
			if(global.changeFeature) {
				global.changeFeature.setProperties({'catecd':'xfsyhui','type':'xfsyhui'});
				global.changeFeature.changed();
			}
			
		}else if(flag=='半开'){
			if(zddwCQZL>0){
				zddwCQZLXFLL = zddwCQZLXFLL+flow-setCustomValueXFSYKQ;
				$("#zddwCQZLXFLL").html(zddwCQZLXFLL);
				$("#zddwCQZLKYLL").html(zddwCQZL-zddwCQZLXFLL);
			}
			if(global.changeFeature) {
				global.changeFeature.setProperties({'catecd':'xfsyhui','type':'xfsyhui'});
				global.changeFeature.changed();
			}
		}else{
			if(zddwCQZL>0){
				zddwCQZLXFLL = zddwCQZLXFLL-flow-setCustomValueXFSYKQ;
				$("#zddwCQZLXFLL").html(zddwCQZLXFLL);
				$("#zddwCQZLKYLL").html(zddwCQZL+zddwCQZLXFLL);
			}
			if(global.changeFeature) {
				global.changeFeature.setProperties({'catecd':'xfsy','type':'xfsy'});
				global.changeFeature.changed();
			}
		}
	}
	
//	if(flag=='开启'){
//		$(e).val('关闭');
//	}else{
//		$(e).val('开启');
//	}
//	global.changeFeature.get('params').imagePath ='https://b3.hoopchina.com.cn/images/logo2017/v1/hp_logo_sports.png';
//	global.changeFeature.changed();//调用后刷新生效
}



//热播传播速度
//石油热波传播速度
//x 石油含水率
function rb_speed(x)
{
	var speed = 3.62 + 2.58 * x - 1.49 * x * x + 0.49 * x * x * x - 0.07 * x * x *x * x;	// 毫米/分钟
	speed = speed * 60 / 1000;	// 米/小时
	return speed.toFixed(3);
}

/**
 * 消防服热辐射范围
 * @param t 分钟 tpp 防火服 tpp 值	cal/cm2
 * */
function calc_fhf_r(tpp, t)
{
	var k = tpp * 10000;	// 1平方米面积接受的热量
	var j = k * 4.184;		// 1卡=4.184焦耳
	var w = j / (t * 60);	// 1平方米面积上的功率，瓦
	var kw = w / 1000;		// 1平方米面积上的功率，千瓦
	return kw;
}

/**
 * 防火服热辐射范围
 * @param t 一次进攻时间 单位分钟
 * */
function changeZhmnQxzsR(flag){
	//防辐射服
	if(flag==1){
		$("#zhmnQxzsFHFDiv").show();
		$("#zhmnQxzs").hide();
	}else{
	//无防辐射服
		$("#zhmnQxzsFHFDiv").hide();
		$("#zhmnQxzs").show();
	}
}

// jianzhou 2018-12-02
// 浓度 单位 ppm 转换为 g/m3（克每立方米）
// 参数：ppm 气体检测仪测得的浓度值
//     m 气体的分子量：S2H-34, SO2-64, SO-48, CO2-44, NH3-17
//     rateSum 选择的气体类型
// 返回值：单位为  g/m3 的浓度值
function ppm_to_gm3(ppm,m){
	var gm3 = m * ppm / 22.4 * 0.001;
	return gm3;
}

//
function showNhjxType(){
	var url=baseUrl+'/webApi/fireLimit/qryList.jhtml';
	var params = null;
	var pid='';
	$.ajax({
		type:'POST',
		url: url,
		headers: {"Content-Type": "application/json;charset=utf-8"},
		data: JSON.stringify(params),
		dataType:'JSON',
		success: function(data){
			if(data.httpCode==200){
				var objList= data.data;
				var htmlOption = '';
				for(var i=0;i<objList.length;i++){
					var obj = objList[i];
					if(i==0){
						pid=obj.id;
					}
					htmlOption+='<option value="'+objList[i].id+'">'+objList[i].materialType+'</option>';
				}
				$("#nhjxModelDiv_lx").html(htmlOption);
				if(pid!=''){
					getChildrenList(pid);
				}
			}else{
				layer.msg("初始化失败", {time: 1000});
			}
		}
	});
}


function getChildrenListSelect(){
	var pid = $("#nhjxModelDiv_lx").val();
	getChildrenList(pid);
}

function calculNhjxModel(){
	if(!checkIsEmpty('nhjxModelDiv')){
		layer.msg("请检查各个输入项是否输入", {time: 2000});
		return false;
	}
	var nhjx_time = $("#nhjx_time").html();
	var nhjx_jl = $("#nhjx_jl").val();
	var nhjxModelDiv_mc = $("#nhjxModelDiv_mc").val();
	var nhjx_zhmj = $("#nhjx_zhmj").val();
	
	if(nhjx_jl=='0'){
		layer.msg("距离不能为0", {time: 2000});
		return false;
	}
	if(nhjx_zhmj=='0'){
		layer.msg("着火面积不能为0", {time: 2000});
		return false;
	}
	
	var url=baseUrl+'/webApi/math/buildingsFlameTime.jhtml';
	var params = {len:nhjx_jl,time:nhjx_time,area:nhjx_zhmj};
	$.ajax({
		type:'POST',
		url: url,
		headers: {"Content-Type": "application/json;charset=utf-8"},
		data: JSON.stringify(params),
		dataType:'JSON',
		success: function(data){
			if(data.httpCode==200){
				var time = data.data.time;
				var dieRadius = data.data.dieRadius;
				$("#nhjx_aqjl").html(dieRadius.toFixed(2)+"(米)");
				$("#nhjx_nhsj").html(time.toFixed(2)+"(小时)");
				$("#nhjxModelDivJsjg").show();
			}else{
				layer.msg("初始化失败", {time: 1000});
				$("#nhjxModelDivCont").hide();
			}
		}
	});
}

function changeGetChildrenList(){
	$("#nhjx_time").html($("#nhjxModelDiv_mc").val());
}

function getChildrenList(ppid){
	var url=baseUrl+'/webApi/fireLimit/qryListByPidId.jhtml';
	var params = ppid;
	$.ajax({
		type:'POST',
		url: url,
		headers: {"Content-Type": "application/json;charset=utf-8"},
		data: JSON.stringify(params),
		dataType:'JSON',
		success: function(data){
			if(data.httpCode==200){
				var objList= data.data;
				var htmlOption = '';
				for(var i=0;i<objList.length;i++){
					var obj = objList[i];
					if(i==0){
						$("#nhjx_time").html(objList[i].fireResistanceH);
					}
					htmlOption+='<option id="'+objList[i].id+'type" value="'+objList[i].fireResistanceH+'" >'+objList[i].materialName+'</option>';
				}
				$("#nhjxModelDiv_mc").html(htmlOption);
			}else{
				layer.msg("初始化失败", {time: 1000});
			}
		}
	});
}

//获取随风方向的数值
function getWindDirection(){
	var rotation = 0;
	if(global_weatherData.windDirection){
		rotation = 32+parseInt(global_weatherData.windDirection*64/360);
		if (rotation > 64){
			rotation = rotation - 64;
		}
	}
	return rotation;
}
//演示用模拟数据
function changeMonishuju(){
	if($("#monishuju").prop('checked')){
		changeMonishujuData();
		$('#monishuju').attr("checked", true);
	}else{
		$('#monishuju').attr("checked", false);
	}
}

