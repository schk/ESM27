function keyLogin(){
    if (event.keyCode==13){ //回车键的键值为13 
　　　　loginEsm();
    }
}  

function loginEsm(){
	var userName = $("#userName").val();
	var userPassword= $("#userPass").val();
	$.ajax({
		type : "post",
		url : baseUrl+'/webApi/doLoginEsm.jhtml',
		data: JSON.stringify({username:userName,password:userPassword}),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				$('.error.login span').text('');
				window.location.href="index.jhtml";
			}else if(data.httpCode==303){
				$('.error.login span').text(data.msg);
				//layer.msg("用户密码错误", {time: 1000});
			}
		},error: function(request) {
			//layer.msg("网络错误", {time: 1000});
			$('.error.login span').text('用户名密码输入错误，请重新输入');
        }
	});
}

function logoutEsm(){
	layer.confirm('是否退出登录？', {
        btn: ['确定','取消'] //按钮
   },function(){
		$.ajax({
			type : "post",
			url : baseUrl+'/webApi/logoutEsm.jhtml',
			dataType:"json",
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {	
				if(data.httpCode==200){
					window.location.href="login.jhtml";
				}else{
					layer.msg("退出失败", {time: 1000});
				}
			},error: function(request) {
				//layer.msg("网络错误", {time: 1000});
	        }
		});
   }, function(){
   		layer.msg('已取消', {icon: 1, time: 1000});
   });
}
// 下载插件
function downloadPlugInUnit(){
	var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串  

	var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1; //判断是否IE<11浏览器  

	if(isIE){

	//如果为IE 构造一个虚拟的a标签 以便于跳转到对应的URL

	   var gotolink = document.createElement('a');
	   gotolink.href = '/plugInFile/flexpaper.reg';
	   gotolink.setAttribute("target", "_blank");
	   document.body.appendChild(gotolink);
	   gotolink.click();
	}
	else{
		window.open('/plugInFile/flexpaper.reg');
	}
}


$(document).ready(function(){
	//$('.error.login').hide();
	$('.logout').on("click",function(){
		logoutEsm();
	});
	
	//登录适应屏幕事件
	var bodyHeight = $( $(document.body).height).height();
	var logBoxHeight = $(".logBox").height();
	//$(".logBox").css("marginTop",0-(bodyHeight-logBoxHeight)/2+"px");
	var outImageHeight = (bodyHeight-logBoxHeight)/2+logBoxHeight*0.75;
	$(".topBg").css("height",outImageHeight+"px");
});



