ESM.ContextMenu = function(map, options) {

	/**
	 * 公用工具类对象
	 * @type {ESM}
	 */
	this.utils_ = new ESM.Utils();

	/**
	 * 默认的参数
	 * @type {Object}
	 */
	var defaults = {width:150, default_items:true};
	/**
	 * 右键菜单的参数，用户参数与默认参数合并后的参数
	 * @type {object}
	 */
	this.options_ = this.utils_.mergeObjects(defaults, options);

	/**
	 * 菜单的DOM容器对象
	 * @type {DOM Node}
	 */
	this.container_ = this.createContainer();

	/**
	 * 菜单项数组
	 * @type {Array}
	 */
	this.menuItems_ = [];

	/**
	 * 右击时间发生在地图上的坐标点
	 * @type {ol.coordinate}
	 */
	this.coordinateClicked_ = null;

	ol.control.Control.call(this, {
		element:this.container_
	});

  /**
   * 要添加右键菜单的地图对象
   * @private
   * @type {ol.Map} 
   */
  this.map_ = map;
  
	this.init();
	
};

ol.inherits(ESM.ContextMenu, ol.control.Control);

//默认菜单项定义 var center = view.calculateCenterZoom(resolution, opt_anchor);
ESM.ContextMenu.defaultItems = [];

/**
 * 初始化函数
 */
ESM.ContextMenu.prototype.init = function(){
	if(this.options_.default_menus){
		this.menuItems_ = ESM.ContextMenu.defaultItems;
	}
	if(this.options_.items)
		this.menuItems_ = this.options_.items.concat(this.menuItems_);
	if(this.menuItems_.length){
		this.refreshMenuItems();
		this.setListeners_();
	}
	this.map_.addControl(this);
}

/**
 * 创建菜单容器并返回
 * @return {DOM Node} 新创建的菜单DOM容器对象
 */
ESM.ContextMenu.prototype.createContainer = function(){
	var container = document.createElement('ul');
	container.className = 'ol-contextmenu ol-unselectable hidden';
	container.style.width = parseInt(this.options_.width, 10) + 'px';
    return container;
}

/**
 * 添加时间响应
 */
ESM.ContextMenu.prototype.setListeners_ = function() {
  var map = this.map_,
    canvas = map.getTargetElement(),
    this_ = this;
    menu = function(evt){
      evt.stopPropagation();
      evt.preventDefault();
      this_.showMenu(map.getEventPixel(evt));
      this_.coordinateClicked_ = map.getEventCoordinate(evt);
      //one-time fire
      canvas.addEventListener('mousedown', {
        handleEvent: function (evt) {
          this_.hideMenu();
          canvas.removeEventListener(evt.type, this, false);
        }
      }, false);
    }
  ;
  canvas.addEventListener('contextmenu', menu, false);

}

/**
 * 根据菜单项参数生成菜单html dom对象，并添加到容器中
 */
ESM.ContextMenu.prototype.refreshMenuItems = function(){
	for (var i = 0; i< this.menuItems_.length; i++) {
		this.container_.appendChild(this.menuItemHtml(this.menuItems_[i], i));
	};
}

/**
 * 生成单个菜单项的html dom对象
 * @param  {object} item  菜单项对象
 * @param  {Integer} index 菜单项序号
 * @return {DOM Node}       菜单项的html dom对象
 */
ESM.ContextMenu.prototype.menuItemHtml = function(item, index){
	  var classname, style = '', html = '', this_=this;
	  if(item.id=="around"){
		  html = '<li><div class="cmitem_in ca cf"><input id="around" type="text" class="inAddress fl" value="5000"><span class="meter">(m)</span><div><li>';
	  }else{
		  classname = item.classname ? ' class="' + item.classname + '"' : '';
		  html = '<li id="index' + index + '"' + style + classname + '>' + item.text + '</li>';
	  }
	  
      //生成文档结点并返回
      var frag = document.createDocumentFragment(),
        	temp = document.createElement('div');

      temp.innerHTML = html;
      while (temp.firstChild) {
        frag.appendChild(temp.firstChild);
      }

      var child = [].slice.call(frag.childNodes, 0)[0];

      child.addEventListener('click', function(evt){
      	if(typeof item.callback === 'function'&&item.id!="around"){
      		var param = {};
      		param.coordinate = this_.coordinateClicked_;
      		param.distance = $("#around").val();
      		item.callback(param, this_.map_,this_.container_);
      		this_.hideMenu();
      	}
      }, false);
      
      return child;
}

/**
 * 在右键点击位置显示右键弹出菜单
 * @param  {ol.Pixel} pixel 屏幕像素坐标点
 */
ESM.ContextMenu.prototype.showMenu = function(pixel){
	var map_size = this.map_.getSize(),
		menu_size = [this.container_.offsetWidth, this.container_.offsetHeight],
		map_width = map_size[0],
		map_height = map_size[1],
		menu_width = menu_size[0],
		menu_height   = menu_size[1],
        height_left   = map_height - pixel[1],
        width_left    = map_width - pixel[0],
        top = (height_left >= menu_height) ? pixel[1] - 10 : pixel[1] - menu_height - 10,
        left = (width_left >= menu_width) ? pixel[0] + 5 : pixel[0] - menu_width - 5;

    this.container_.className = this.container_.className.replace(/hidden/g, '').trim();
    this.container_.style.left = left + 'px';
    this.container_.style.top = top + 'px';
}

/**
 * 隐藏右键菜单
 */
ESM.ContextMenu.prototype.hideMenu = function(){
	$("#around").val(5000);
	this.container_.className += ' hidden';
}




