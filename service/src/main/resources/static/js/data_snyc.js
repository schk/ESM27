
//同步jar包只在本地部署，服务器上不部署.
//ftp装服务器上本地不装

var cloneUrl = "http://127.0.0.1:8001/dataSync";

//基础数据同步（服务器上得数据下载到本地）
var tableNames0=new Array("t_accident_case","t_accident_case_file","t_danger_type","t_dangers","t_dangers_catalog"
		,"t_department","t_dictionary","t_duty_equip_use_record","t_emergency_material","t_emergency_material_type"
		,"t_equipment","t_equipment_doc","t_equipment_doc_catalog","t_expert","t_equipment_type"
		,"t_expert_type","t_expr","t_expr_param","t_expr_param_option","t_facilities","t_fire_brigade"
		,"t_fire_engine","t_fire_engine_equipment","t_fire_water_source","t_geographic_edit","t_key_parts"
		,"t_key_parts_dangers","t_key_unit","t_key_unit_accident","t_key_unit_dangers","t_key_unit_dangers_catalog"
		,"t_key_unit_img","t_plans","t_plans_catalog","t_plotting","t_reserve_point","t_permission","t_role"
		,"t_role_permission_ref","t_special_duty_equip","t_teams","t_user","t_user_brigade_ref","t_user_role")

//场景数据同步（服务器和上得数据下载到本地）
var tableNames1=new Array("t_resource")
var tableNames2=new Array("t_accident","t_room","t_incident_record","t_incident_record_file_ref"
		,"t_emergency_material_use_record","t_fire_engine_use_record")
var param = {
	//服务器
	"srcDataSourceParams":"jdbc:mysql://39.152.50.253:3306/esm27fsdata?user=root&password=root&characterEncoding=utf8&serverTimezone=UTC&zeroDateTimeBehavior=convertToNull&allowMultiQueries=true&serverTimezone=PRC&useSSL=false",
	//本地
	"destDataSourceParams":"jdbc:mysql://127.0.0.1:3306/esm27fsdata?user=root&password=root&characterEncoding=utf8&serverTimezone=UTC&zeroDateTimeBehavior=convertToNull&allowMultiQueries=true&serverTimezone=PRC&useSSL=false",
	"syncMode":"double",
	"srcFtpHost":"39.152.50.253",
	"srcFtpPort":"21",
	"srcFtpLoginName":"sczc",
	"srcFtpLoginPassword":"sczc",
	"destPath":"D:/ESM27/tomact/upload",   //本地文件存放路径
	"token":ip,
	"syncTables":[{"tableName":"t_resource","syncUpdateColumnName":"update_time","snycFileColumnNames":["resource_path"]}
	             ]
}
function dataSycn() {
	param.syncMode="centerTolocal";
	param.syncTables=[		
		{"tableName":"t_accident_case","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},
		{"tableName":"t_accident_case_file","syncUpdateColumnName":"update_time","snycFileColumnNames":["path_","swf_path"]},
        {"tableName":"t_danger_type","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},
        {"tableName":"t_dangers","syncUpdateColumnName":"update_time","snycFileColumnNames":["msds_path","swf_path"]},
        {"tableName":"t_dangers_catalog","syncUpdateColumnName":"create_time","snycFileColumnNames":["msds_path","swf_path"]},
        {"tableName":"t_department","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},
        {"tableName":"t_dictionary","syncUpdateColumnName":"update_time","snycFileColumnNames":["path_","dt_path","jh_path"]},
        {"tableName":"t_duty_equip_use_record","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},
        {"tableName":"t_emergency_material","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},
        {"tableName":"t_emergency_material_type","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},       
        {"tableName":"t_equipment","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},
        {"tableName":"t_equipment_doc","syncUpdateColumnName":"update_time","snycFileColumnNames":["path_","swf_path"]},
        {"tableName":"t_equipment_doc_catalog","syncUpdateColumnName":"create_time","snycFileColumnNames":[]},
        {"tableName":"t_equipment_type","syncUpdateColumnName":"update_time","snycFileColumnNames":["dt_path"]},
        {"tableName":"t_expert","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},
        {"tableName":"t_expert_type","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},
        {"tableName":"t_expr","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},
        {"tableName":"t_expr_param","syncUpdateColumnName":"create_time","snycFileColumnNames":[]},
        {"tableName":"t_expr_param_option","syncUpdateColumnName":"create_time","snycFileColumnNames":[]},
        {"tableName":"t_facilities","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},
        {"tableName":"t_fire_brigade","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},
        {"tableName":"t_fire_engine","syncUpdateColumnName":"update_time","snycFileColumnNames":["path_"]},
        {"tableName":"t_fire_engine_equipment","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},       
        {"tableName":"t_fire_water_source","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},
        {"tableName":"t_geographic_edit","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},
        {"tableName":"t_key_parts","syncUpdateColumnName":"update_time","snycFileColumnNames":["path_","process_flow"]},
        {"tableName":"t_key_parts_dangers","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},
        {"tableName":"t_key_unit","syncUpdateColumnName":"update_time","snycFileColumnNames":["dt_path"]},
        {"tableName":"t_key_unit_accident","syncUpdateColumnName":"update_time","snycFileColumnNames":["path_","swf_path"]},
        {"tableName":"t_key_unit_dangers","syncUpdateColumnName":"update_time","snycFileColumnNames":["msds_path","swf_path","img_path"]},
        {"tableName":"t_key_unit_dangers_catalog","syncUpdateColumnName":"create_time","snycFileColumnNames":["msds_path","swf_path"]},
        {"tableName":"t_key_unit_img","syncUpdateColumnName":"update_time","snycFileColumnNames":["path_"]},
        {"tableName":"t_plans","syncUpdateColumnName":"update_time","snycFileColumnNames":["path_","swf_path"]},
        {"tableName":"t_plans_catalog","syncUpdateColumnName":"create_time","snycFileColumnNames":[]},
        {"tableName":"t_plotting","syncUpdateColumnName":"update_time","snycFileColumnNames":["path_","jh_path","dt_path"]},
        {"tableName":"t_reserve_point","syncUpdateColumnName":"update_time","snycFileColumnNames":["dt_path"]},
        {"tableName":"t_permission","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},
        {"tableName":"t_role","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},
        {"tableName":"t_role_permission_ref","syncUpdateColumnName":"create_time","snycFileColumnNames":[]},
        {"tableName":"t_special_duty_equip","syncUpdateColumnName":"update_time","snycFileColumnNames":["technical_param_file","system_rules"]},
        {"tableName":"t_teams","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},
        {"tableName":"t_user","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},
        {"tableName":"t_user_brigade_ref","syncUpdateColumnName":"create_time","snycFileColumnNames":[]},
        {"tableName":"t_user_role","syncUpdateColumnName":"create_time","snycFileColumnNames":[]}
         ];

    $.ajax({
		type : "post",
		url : cloneUrl,
		data: JSON.stringify(param),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
            if(data.code==1){ // 同步数据开始，用于传递需要同步的数据表
            	$('#sync_data_of_table1').html("");
            	var html = "";
            	// 创建需要同步表的进度显示
            	for(var i= 0;i<tableNames0.length;i++){
    				html+='<li class="ca cf activeData">';
    				html+='<div class="fl dataName ellipsis">'+tableNames0[i]+'</div>';
    				html+='<div class="fl dataProgress" ><img table_num="table_num_'+tableNames0[i]+'" src="/images/scheduleImg-02.png"  alt="" /></div>';
    				html+='<div class="fl dataName ellipsis">'+tableNames0[i]+'</div>';
    				html+='<div table_tit="table_img_'+tableNames0[i]+'"></div>';
    				html+='</li>';
    			}
            	$('#sync_data_of_table1').html(html);
            }else {
				layer.open({
        		  title: '信息提示',
        		  content: "同步数据异常:" + data.message
        		}); 
			}

		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	});
    
}

//场景数据同步（互相同步）
function dataSycnScene() {
	param.syncMode="double";
	param.syncTables=[{"tableName":"t_resource","syncUpdateColumnName":"update_time","snycFileColumnNames":["resource_path"]}
    ];
    $.ajax({
		type : "post",
		url : cloneUrl,
		data: JSON.stringify(param),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
            if(data.code==1){ // 同步数据开始，用于传递需要同步的数据表
            	$('#sync_data_of_table2').html("");
            	var html = "";
            	// 创建需要同步表的进度显示
            	for(var i= 0;i<tableNames1.length;i++){
    				html+='<li class="ca cf activeData">';
    				html+='<div class="fl dataName ellipsis">'+tableNames1[i]+'</div>';
    				html+='<div class="fl dataProgress" ><img table_num="table_num_'+tableNames1[i]+'" src="/images/scheduleImg-02.png"  alt="" /></div>';
    				html+='<div class="fl dataName ellipsis">'+tableNames1[i]+'</div>';
    				html+='<div table_tit="table_img_'+tableNames1[i]+'"></div>';
    				html+='</li>';
    			}
            	$('#sync_data_of_table2').html(html);
            }else {
				layer.open({
        		  title: '信息提示',
        		  content: data.message
        		}); 
			}

		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	});
}

//救援记录数据同步(本地数据上传到服务器)
function dataSycnIncidentRecord() {
	param.syncMode="localToCenter";
	param.syncTables=[{"tableName":"t_accident","syncUpdateColumnName":"create_time","snycFileColumnNames":[]},
        {"tableName":"t_room","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},
        {"tableName":"t_incident_record","syncUpdateColumnName":"create_time","snycFileColumnNames":[]},
        {"tableName":"t_incident_record_file_ref","syncUpdateColumnName":"create_time","snycFileColumnNames":["path_"]},
        {"tableName":"t_emergency_material_use_record","syncUpdateColumnName":"update_time","snycFileColumnNames":[]},
        {"tableName":"t_fire_engine_use_record","syncUpdateColumnName":"update_time","snycFileColumnNames":[]}
      
        ];
    $.ajax({
		type : "post",
		url : cloneUrl,
		data: JSON.stringify(param),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
            if(data.code==1){ // 同步数据开始，用于传递需要同步的数据表
            	$('#sync_data_of_table3').html("");
            	var html = "";
            	// 创建需要同步表的进度显示
            	for(var i= 0;i<tableNames2.length;i++){
    				html+='<li class="ca cf activeData">';
    				html+='<div class="fl dataName ellipsis">'+tableNames2[i]+'</div>';
    				html+='<div class="fl dataProgress" ><img table_num="table_num_'+tableNames2[i]+'" src="/images/scheduleImg-02.png"  alt="" /></div>';
    				html+='<div class="fl dataName ellipsis">'+tableNames2[i]+'</div>';
    				html+='<div table_tit="table_img_'+tableNames2[i]+'"></div>';
    				html+='</li>';
    			}
            	$('#sync_data_of_table3').html(html);
            }else {
				layer.open({
        		  title: '信息提示',
        		  content: "同步数据异常:" + data.message
        		}); 
			}

		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	});
    
}


$(document).ready(function() {
    if(window.WebSocket) {
        var client, destination;
        var url = 'ws://39.152.50.253:61614';   //服务器得mq地址
        var login = 'admin';
        var pw = 'admin';
        destination = '/topic/esm27.sync.'+ip;
        client = Stomp.client(url);
        client.connect(login, pw, function(frame) {
          client.subscribe(destination, function(result) {
            var datas = $.parseJSON(result.body);
         // 单个表的同步开始，用于传递表明和需要同步的数据量 分为三个状态（成功、失败、开始）
            if(datas.state=="starting"){ 
            	$('[table_num="table_num_'+datas.tableName+'"]').attr("src",baseUrl+"/images/scheduleGif.gif");
            }else if(datas.state=="complete"){ // 每添加一条数据，便会触发一次此事件
            	$('[table_num="table_num_'+datas.tableName+'"]').attr("src",baseUrl+"/images/scheduleImg.png");
            	$('[table_tit="table_img_'+datas.tableName+'"]').addClass("fl dataSuccess");
            }else if(datas.state=="error"){ // 完成所有同步，便会触发一次此事件
            	layer.open({
          		  title: '信息提示',
          		  content: datas.message?datas.message:datas.tableName+"同步失败"
          		});   
            	//$('[table_num="table_num_'+datas.tableName+'"]').attr("src",baseUrl+"/images/scheduleImg-02.png");
              }else if(datas.state=="all_complete"){ // 完成所有同步，便会触发一次此事件
            	layer.open({
        		  title: '信息提示',
        		  content: '数据同步已完成'
        		});   
            }
          });
        });
    }
    
  
    //数据同步按钮切换事件
    $("#myDateDiv").find("ul li").click(function(){
    	   $(this).addClass("onMyDate").siblings().removeClass("onMyDate");//切换li选中高亮
    	   var tagT = $(this).attr("dataSync");
    	   if(tagT==1){
    		   $("#dataSync1").show();
    		   $("#dataSync2").hide();
    		   $("#dataSync3").hide();
    	   }else if(tagT==2){
    		   $("#dataSync1").hide();
    		   $("#dataSync2").show();
    		   $("#dataSync3").hide();
    	   }else if(tagT==3){
    		   $("#dataSync1").hide();
    		   $("#dataSync2").hide();
    		   $("#dataSync3").show();
    	   }
 	});
    
    
  });