/**
 * 
 */
ESM.Overlay = function(options){
	this.map = options.map;
	this.content = options.content;
	this.id = options.id;
	this.point = options.point;
	
	var container = document.createElement('div');;
    container.className = 'esm-ol-popup';

    var closer = document.createElement('a');
    closer.className = 'esm-ol-popup-closer';
    closer.href = '#';
    container.appendChild(closer);
	
    var that = this;
    closer.addEventListener('click', function(evt) {
       that.map.removeOverlay(that.overlay);
       evt.preventDefault();
    }, false);

    var content_ = document.createElement('div');
    
    if(this.content instanceof HTMLElement){
    	content_.appendChild(this.content);
    }else{
    	content_.innerHTML = this.content;
    }
    
    container.appendChild(content_);

	this.overlay = new ol.Overlay({
		id: this.id,
		element:container,
		offset:[0,5],
		position:this.point,
		insertFirst:false
	});
	
	this.map.addOverlay(this.overlay);
}

ESM.Overlay.prototype.close = function(){
	this.map.removeOverlay(this.overlay);
}