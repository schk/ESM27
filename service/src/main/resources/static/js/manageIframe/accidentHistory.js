//var baseUrl =window.location.protocol+"//"+window.location.host+"/";
var ext = [20037508.3427892,20037508.3427892,-20037508.3427892,-20037508.3427892];

var resourceType = -1;

//行政图和卫星图的图片数据加载list
var xzImageResourceList=[];
var wxImageResourceList=[];
//百度地图层
var baiduMapLayer,baiduWeiXinMapLayer,cva_wLayer;
$(document).ready(function(){
		// 判断加载地图的类型
	    if (loadMapType=="b"){
	    	//行政图
	        baiduMapLayer = new ol.layer.Tile({
	       	    source: new ol.source.BaiduMap()
	       });
	        //卫星图
	        baiduWeiXinMapLayer = new ol.layer.Tile({
	    	    source: new ol.source.BaiduMap({"mapType":"sat"}),
	            projection: 'EPSG:3857'
	        })
	    }else{
	    	//行政图
	        baiduMapLayer = new ol.layer.Tile({
	       	    source: new ol.source.TianMap()
	        });
	        
	        if(mapModel == 1){
	       	// 天地图的lable
	            cva_wLayer = new ol.layer.Tile({
	           	    source: new ol.source.TianMap({"mapType":"label"})
	            });
	        }
	        //卫星图
	        baiduWeiXinMapLayer = new ol.layer.Tile({
	    	    source: new ol.source.TianMap({"mapType":"sat"}),
	            projection: 'EPSG:3857'
	        })
	    }
		
		map = new ol.Map({
        target: 'map',
        layers: loadMapType=="b" ? [baiduMapLayer,baiduWeiXinMapLayer] 
		: mapModel == 1 ? [baiduMapLayer,cva_wLayer,baiduWeiXinMapLayer]:[baiduMapLayer,baiduWeiXinMapLayer],
        view: new ol.View({
          // 设置成都为地图中心
          center: ol.proj.transform([104.094752, 30.667451], 'EPSG:4326', 'EPSG:3857'),
          zoom: 8,
          minZoom: 3,
          maxZoom: 18,
        })
      });
	  
	  map_plot = new olPlot(map, {
        zoomToExtent: true,
        isClear: true
    })
	  
	  baiduMapLayer.setVisible(true); //显示行政
	  baiduWeiXinMapLayer.setVisible(false); //关闭卫星

	  var currentRoomId =$("#accidentId").text();
	  var params="";
	  var html="";
	  if(currentRoomId){
			params=currentRoomId;// 当前房间id todo （这是个动态id）
	  }
	  $.ajax({
			type : "post",
			url : baseUrl+"/webApi/getIncidentRecordByAccident.jhtml",
			data: JSON.stringify(params),
			dataType:"json",
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {
				if(data.data.length<1){
					html = "该事故没有处置记录！";
					$("#dilixinxibianjiList").append(html);	
				}
				for(j = 0,len=data.data.length; j < len; j++) {
					html = getIncidentRecordHtml(data.data[j],j);
					$("#dilixinxibianjiList").append(html);	
					//注册事件
					(function(data,index){
						$("#list_"+index).on("click",function(){
							getIncidentRecordDetail(data,index);
						})
					})(data.data[j],j)
				}
			},error: function(request) {
// layer.msg("网络错误", {time: 1000});
         }
	  })

	  getClusterDataList();
	  //初始化获取道路列表
	  getRoadList();
	  //初始化场景数据(t_resource表)
	  getResourceList();
	  gertInitCoordinate();
      
});


function showWeiXingTu(){
	resourceType=2;
	
	for(j = 0,len=xzImageResourceList.length; j < len; j++) {
		var oid=xzImageResourceList[j].id;
		removeImagesByid(oid);
	}
	 baiduMapLayer.setVisible(false); //显示卫星
     baiduWeiXinMapLayer.setVisible(true); //关闭行政
     getAllImageMap();
}

function showDiTu(){
	resourceType=1;
	for(j = 0,len=wxImageResourceList.length; j < len; j++) {
		var oid=wxImageResourceList[j].id;
		removeImagesByid(oid);
	}
	baiduMapLayer.setVisible(true); //显示行政
    baiduWeiXinMapLayer.setVisible(false); //关闭卫星
    getAllImageMap();
}

//获取初始化场景中地图加载的图片
function getAllImageMap(_data){
	var params = {type:resourceType};
	$.ajax({
		type : "post",
		url : baseUrl+"/accidentResource/qryResourceListInMap.jhtml",
		data: JSON.stringify(params),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {
			var addtomap=[];
			if(resourceType==1){
				xzImageResourceList=data.data;
			}else if(resourceType==2){
				wxImageResourceList=data.data;
			}
			for(j = 0,len=data.data.length; j < len; j++) {
				var d = data.data[j];
				if(d.position && d.position!="")
				{
					addtomap.push({url:d.resourcePath,extent:d.position,id:d.id,imageId:d.id});
				}
			}
			
			addImagesToMap(addtomap);
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
	    }
	});
}

function clear(){
	map.getLayers().forEach(function(layer) {
		if (layer.get('id') == 'historyLayer') {
			layer.getSource().clear();
		}
	});
	ext = [20037508.3427892,20037508.3427892,-20037508.3427892,-20037508.3427892];
	map.getView().animate({center:ol.proj.transform([104.094752, 30.667451], 'EPSG:4326', 'EPSG:3857'),zoom:8});
}

/**
 * 获取初始化地图数据(wzcbd,zddw,xfsy,yjdw,sczz,xfdw)等数据
 */
function getClusterDataList(){
	var urls = baseUrl+"/api/geographicEdit/qryListAll.jhtml";
	var params = null
	executeAjax(urls,params,function(data){
		if(data.httpCode==200){
			if(data){
				clusterDo(data);
			}
		}else {
			layer.msg(data.msg, {time: 1000});
		}
	});
}

function gertInitCoordinate(){
	//获取地图初始化中心点坐标
    var urls =baseUrl+"/api/gis/getMapInitCenter.jhtml";
    var params = null
    executeAjax(urls,params,function(data){
		if(data.httpCode==200){
//			console.log(JSON.stringify(data))
    		if(data.data){
    			//定位中心点	
    			map.getView().animate({zoom: 8}, {center: ol.proj.transform([data.data.lng,data.data.lat], 'EPSG:4326', 'EPSG:3857')});
    		}
    	}else {
    		layer.msg(data.msg, {time: 1000});
    	}
    });
}

/**
 * 获取道路列表
 * @returns
 */
function getRoadList(){
	var urls =baseUrl+"/api/geographicEdit/getRoadList.jhtml";
    var params = null
    executeAjax(urls,params,function(data){
		if(data.httpCode==200){
    		if(data.data&&data.data.length>0){
    			for(j = 0,len=data.data.length; j < len; j++) {
					if(data.data[j].content &&data.data[j].content!=""){
						var json = JSON.parse(data.data[j].content);
						var feature = plot.plotUtils.toFeature(json);
						addFeatureToMap(feature,data.data[j].id);
    				}
				}
    		}
    	}else {
    		layer.msg(data.msg, {time: 1000});
    	}
    });
}
/**
 * 初始化场景数据
 * @returns
 */
function getResourceList(){
	var urls = baseUrl+"/api/geographicEdit/queryResource.jhtml";
    var params = null
    executeAjax(urls,params,function(data){
		if(data.httpCode==200){
    		if(data.data&&data.data.length>0){
    			var addtomap=[];
    			for(j = 0,len=data.data.length; j < len; j++) {
    				var d = data.data[j];
    				if(d.position && d.position!=""){
    					addtomap.push({url:d.resourcePath,extent:d.position,id:d.id,imageId:d.id});
    				}
    			}
    			addImagesToMap(addtomap);
    		}
    	
    	}else {
    		layer.msg(data.msg, {time: 1000});
    	}
    });
}
/**
 * 聚合接口
 * */
function clusterDo(data){
	var fmap = new HashMap();
	var format = new ol.format.GeoJSON();
	var features = format.readFeatures(data.data);
	features.forEach(function(feature){
		feature.set('cluster',true)
		var prop = feature.getProperties();
		var catecd = prop.catecd;
		if(fmap.containsKey(catecd)){
			fmap.get(catecd).push(feature);
		}else{
			var fs = [];
			fs.push(feature);
			fmap.put(catecd,fs);
		}
	});
	var keys =  fmap.keys();
	for(var i=0;i<keys.length;i++){
		var layerId = keys[i]+"ClusterLayer";
		var layer = layerIsExist(layerId,map);
		if(!layer){
			layer = createClusterLayer(layerId,keys[i]);
			map.addLayer(layer);
		}
		addFeatureToLayer(fmap.get(keys[i]),layer,true)
	}
}
/**
 * 标注回显功能
 * */
function addFeatureToMap(feature,id){
	feature.setId(id);
	var params = feature.get("params");
	feature.set("name",params.name);
	feature.set("desc",params.desc);
	addFeature(feature);
}

/**
 * 显示场景搭建历史
 * */
function addImagesToMap(images){
	var group = layerIsExist('imagesGroup',map);
	if(!group){
		group = createGroupLayer('imagesGroup');
		map.addLayer(group);
	}
	for(var i=0;i<images.length;i++){
		var image = images[i];
		var layer = new ol.layer.Image({
			source: new ol.source.ImageStatic({
				url:baseFileUrl+image.url,
				crossOrigin: '',
				projection: 'EPSG:3857',
	            imageExtent: image.extent.split(",")
			})
		});
		if(image.id){
			layer.set('id',image.id);
			removeImagesByid(image.id);
		}
		group.getLayers().push(layer);
	}
}

/**
 * 删除场景搭建历史
 * */
function removeImagesByid(id){
	var layer;
	var group = layerIsExist('imagesGroup',map);
	group.getLayers().forEach(function(lay){
		if(lay.get('id')==id){
			layer = lay;
		}
	});
	if(layer){
		group.getLayers().remove(layer);
	}
}

/**
 * 判断图层是否存在
 * 
 * @param layerId 图层ID
 * @param map 地图
 */
function layerIsExist(layerId, map) {
	var lay = null;
	map.getLayers().forEach(function(layer) {
		
//		console.log(layer.get('id'));
		if (layer.get('id') == layerId) {
			lay = layer;
		}
	});
	return lay;
}
/**
 * 删除标绘
 */
function remove (featureid) {
	var historyLayer;
	map.getLayers().forEach(function(layer) {
		if (layer.get('id') == 'historyLayer') {
			historyLayer = layer;
		}
	});
	//historyLayer.getSource().removeFeature(feature);
	var feature = historyLayer.getSource().getFeatureById(featureid);
	historyLayer.getSource().removeFeature(feature);
}

function addFeatures(features){
	var historyLayer;
	map.getLayers().forEach(function(layer) {
		if (layer.get('id') == 'historyLayer') {
			historyLayer = layer;
		}
	});
	if(!historyLayer){
		historyLayer = createLayer('historyLayer');
	}
	map.addLayer(historyLayer);
	historyLayer.getSource().clear();
	historyLayer.getSource().addFeatures(features);
	for(var i=0;i<features.length;i++){
		var feature = features[i];
		var extent = feature.getGeometry().getExtent();
		if(extent[0]<ext[0]){
			ext[0] = extent[0];
		}
		if(extent[1]<ext[1]){
			ext[1] = extent[1];
		}
		if(extent[2]>ext[2]){
			ext[2] = extent[2];
		}
		if(extent[3]>ext[3]){
			ext[3] = extent[3];
		}
	}
	map.getView().fit(ext,{size:map.getSize(),duration:1000});
}

function addFeature(feature){
	var historyLayer;
	map.getLayers().forEach(function(layer) {
		if (layer.get('id') == 'historyLayer') {
			historyLayer = layer;
		}
	});
	if(!historyLayer){
		historyLayer = createLayer('historyLayer');
		map.addLayer(historyLayer);
	}
	//
	//historyLayer.getSource().clear();
	historyLayer.getSource().addFeature(feature);
	var extent = feature.getGeometry().getExtent();
	if(extent[0]<ext[0]){
		ext[0] = extent[0];
	}
	if(extent[1]<ext[1]){
		ext[1] = extent[1];
	}
	if(extent[2]>ext[2]){
		ext[2] = extent[2];
	}
	if(extent[3]>ext[3]){
		ext[3] = extent[3];
	}
	map.getView().fit(ext,{size:map.getSize(),duration:1000});
}

function createLayer(id){
	var historyLayer = new  ol.layer.Vector({
		source : new ol.source.Vector({
			features : []
		}),
		style : function(feature) {
			var image = feature.get('params').imagePath?feature.get('params').imagePath:'/mapIcon/mapFixedIcon/sczz.png';
			var name = feature.get('name')?feature.get('name'):'';
			var scolor = feature.get('scolor')?feature.get('scolor'):'#4781d9';
			var fcolor = feature.get('fcolor')?feature.get('fcolor'):'rgba(67, 110, 238, 0.4)';
			var geoType = feature.getGeometry().getType();
			var style;
			if(geoType=="Point"){
				style = new ol.style.Style({
					image : new ol.style.Icon({
						src : baseFileUrl+image,
						anchor : [ 0.5, 0.5 ]
					}),
					text : new ol.style.Text({
						text : name,
						offsetY : 20,
						font : 'bold 12px sans-serif',
						fill : new ol.style.Fill({
							color : '#84C1FF'
						}),
						stroke : new ol.style.Stroke({
							color : '#ECF5FF',
							width : 3
						})
					})
				})
			}else if(geoType=="LineString"){
				style = new ol.style.Style({
					stroke:new ol.style.Stroke({
						width:2,
						color: scolor
					})
				});
			}else if(geoType=="Polygon"){
				style = new ol.style.Style({
					stroke:new ol.style.Stroke({
						width:2,
						color: scolor
					}),
					fill: new ol.style.Fill({
						color:fcolor
					})
				})
			}
			return style;
		},
		zIndex:5
	});
	historyLayer.set('id',id);
	return historyLayer;
}

/**
 * 判断是否为空
 * 
 * @param value
 * @returns
 */
function isEmptyOrNo(value){
	if(value){
		return value;
	}else{
		return "";
	}
}
function timestampToTime(timestamp) {
// var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
	var date = new Date(timestamp);
	Y = date.getFullYear() + '-';
  M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
  D = date.getDate() + ' ';
  h = date.getHours() + ':';
  m = date.getMinutes() + ':';
  s = date.getSeconds();
  return Y+M+D+h+m+s;
}


/* 救援记录拼接html */
function getIncidentRecordHtml(obj,num){
	  if(obj.content){
		 var content = eval('(' + obj.content + ')');
	  }
      var html="<div class='succor' id='list_"+num+"'  timeDifference='"+isEmptyOrNo(obj.timeDifference)+"'>"; 
      html+="<div class='succorHd'><i></i>步骤:"+(num+1)+"</div>";
      html+="<div class='sucMsg'>"; 
      html+="<div class='ca cf sucMsgli'><span class='yearIcon'></span><div class='yearTime'>"+isEmptyOrNo(timestampToTime(obj.createTime))+"</div></div>";
      html+="<div class='ca cf sucMsgli'>";		
      html+="<div class='sucMsgTxt'>";
      if(obj.type==1){			
			if(obj.content){
				html+="<p><span class='green'>"+content.previousName+"</span>把指挥权移交给<span class='green'>"+content.name+"</span></p>";
			}
		}else if(obj.type==2 && typeof obj.isDelete != 'undefined' && obj.isDelete==0){
			if(obj.content){
				html+="<p><span class='green'>"+obj.director+"</span> 删除<i class='red'>标会</i>命令</p>"+obj.action;														
			}
		}else if(obj.type==2){
			if(obj.content){
				html+="<p><span class='green'>"+obj.director+"</span> 执行<i class='red'>标会</i>命令</p>"+obj.action;														
			}
		}else if(obj.type==3){
			if(obj.content){
				html+="<p><span class='green'>"+obj.director+"</span> 加入了房间<i class='yellow'>"+obj.room+"</i>语音组</p>";
			}
		}else if(obj.type==4){ 
			    html+="<p><span class='green'>"+obj.director+"</span> "+obj.actionDesc+"</p>";	
		}else if(obj.type==5 && typeof obj.isDelete != 'undefined' && obj.isDelete==0){
			if(obj.content){
				html+="<p><span class='green'>"+obj.director+"</span> 退出了 "+content.roomname+"房间</p>";												
			}
		}else if(obj.type==5){ 
			if(obj.content){
				html+="<p><span class='green'>"+obj.director+"</span> 加入了房间<i class='yellow'>"+content.roomname+"</i></p>";
			}
		}else if(obj.type==6){ 
			if(typeof obj.isDelete != 'undefined' && obj.isDelete==0){
				html+="<p><span class='green'>"+obj.director+"</span>删除事故点：在GIS地里信息系统经纬度坐标：<i class='brown'>"+content.geometry.coordinates.join(',')+"</i></p>";
				if(content.properties.params.groundHeight){
					html+="<p>距地面高度: <i class='brown'>"+content.properties.params.groundHeight+"</i></p>";
					html+="<p>事故类型: <i class='brown'>"+content.properties.params.accidentType+"</i></p>";
				}
			}else{
				html+="<p>事故点已确认，在GIS地里信息系统经纬度坐标：<i class='brown'>"+content.geometry.coordinates.join(',')+"</i></p>";
				if(content.properties.params.groundHeight){
					html+="<p>距地面高度: <i class='brown'>"+content.properties.params.groundHeight+"</i></p>";
					html+="<p>事故类型: <i class='brown'>"+content.properties.params.accidentType+"</i></p>";
				}
			}
			
		}else if(obj.type==7){ 
			if(obj.content){
				html+="<p><span class='green'>"+obj.director+"</span>向作战人员发布消息命令"+content.command+"</p>";	
			}
		}else if(obj.type==8 && typeof obj.isDelete != 'undefined' && obj.isDelete==0){
			if(obj.content){
				html+="<p><span class='green'>"+obj.director+"</span> 删除一辆消防车："+content.properties.params.plateNumber+"</p>";												
			}
		}else if(obj.type==8){ 
			if(obj.content){
				html+="<p><span class='green'>"+obj.director+"</span> 摆放一辆消防车："+content.properties.params.plateNumber+"</p>";	
			}
		}else if(obj.type==9){ 
			if(obj.content){
				html+="<p><span class='green'>"+obj.director+"</span> 执行一个标注："+content.properties.params.name+"</p>";	
			}
	   }else if(obj.type==10){ 
			if(obj.content){
				html+="<p><span class='green'>"+obj.director+"</span> 标注了一条："+content.properties.params.name+" 路线</p>";												
			}
	    }else if(obj.type==13){ 
			if(obj.content){
				html+="<p><span class='green'>"+obj.publisher+"</span><i class='red'>发布任务：</i></p>"+obj.actionDesc;	
				if(obj.status==2){
					html+="<p><span class='green'>"+obj.feedback+"</span>对该任务进行了反馈： <i class='red'>"+obj.feedbackDesc+"</i></p>";
				}
				
			}
	    }
	    html+="</div>";
		html+="</div>";
		html+="</div>";
	return html;
}

//绘制每个事件
function getIncidentRecordDetail(data,index){
	var btnplay =  $('#btnplay');
	var _getIncidentRecordDetail = function(){
		//添加样式
		$('#list_'+index).css("border","1px solid #15a4fa").siblings().css("border","1px solid #ebeceb");
		
		//点到第几个了
		btnplay.attr("playindex",index);
		//如果是最后一个就暂停
		if($("#dilixinxibianjiList .succor:last").attr("id")=='list_'+index){
			$('#btnplay').attr("isplay",1);
			historyplay();
		}
		var content ;
		if(data.content){
			 content = eval('(' + data.content + ')');
		}
		if(typeof data.isDelete != 'undefined' && data.isDelete==0&&data.type!=5){
			try{
				play[data.type].remove(data,content);
			}catch(e){}
		}
		else{
			try{
				play[data.type].init(data,content);
			}catch(e){}
		}
	}
	//判断是否是正在播放 播放时保留 地图图层，暂停时 清除
	var isplay = btnplay.attr("isplay");
	//保留
	if(isplay && parseInt(isplay)==1){
		_getIncidentRecordDetail();
	}
	//清除 可以点肯定等于0 向上展示出来，向下都清除
	else{
		var callbacks = $.Callbacks('once unique');
		callbacks.add(function(){
			btnplay.attr("isplay",1);
			$('.payTo').remove();
			clear();
		})
		for(var i=0;i<=index;i++){
			(function(_index){
				callbacks.add(function(){
					$('#list_'+_index).click();
				})
			})(i)
		}
		callbacks.add(function(){
			btnplay.attr("isplay",0);
		})
		callbacks.fire(); 
	}
}

//根据类型执行不同步效果
var play = {
	//移交指挥权
	1:{
		//绘制
		init:function(data,content){
			var html = "";
			var htmlid = 'show_1_'+data.id;
			if($('#'+htmlid).length>0){
				return;
			}
			var index = $('.payTo.left').length;
			html +='<div index="'+index+'" id="'+htmlid+'" class="payTo left" style="left: auto; margin-left: 20px; margin-top: 10px; top: auto;position: static;">';
			html +='<div class="payToWarp arrowAfter">';
			html +='<div class="payToUser">';
			html +='<div class="Operator sucMsgTxt historyMsg">';
			html +='<p><span class="yellow">'+content.previousName+"</span>把指挥权移交给<span class='yellow'>"+content.name+'</span></p>';
			html +='</div>';
			html +='</div>';
			html +='</div>';
			html +='</div>';
			$("#leftpanel").append(html);
			var gettop = function(){
				var _index = $('.payTo.left').length;
				if(_index==0){
					return '2%';
				}
				else{
					var up = $('div[index="'+(_index-1)+'"]');
					return (up.offset().top+up.height()+10);
				}
			}
			//$('#'+htmlid).css('top',gettop());
			$('#'+htmlid).slideDown(200,function(){
				//$('#'+htmlid).attr('index',$('.payTo').length);
				//$('#'+htmlid).css('top',gettop())
			});
		},
		//清除
		remove:function(){}
	},
	//会标
	2:{
		init:function(data,content){
			if(content && content!=''){
				var feature = map_plot.plotUtils.toFeature(content);
				addFeature(feature);
			}
		},
		remove:function(data,content){
			if(content && content!=''){
				var feature = map_plot.plotUtils.toFeature(content);
				remove(feature.getId());
			}
		}
	},
	//语音
	3:{
		init:function(){},
		remove:function(){}
	},
	//物资投放
	4:{
		init:function(data,content){
			if(content && content!=''){
				var feature = map_plot.plotUtils.toFeature(content);
				addFeature(feature);
			}
		},
		remove:function(data,content){
			if(content && content!=''){
				var feature = map_plot.plotUtils.toFeature(content);
				remove(feature.getId());
			}
		}
	},
	//加入房间
	5:{
		init:function(data,content){
			var html = "";
			var htmlid = 'show_5_'+data.id;
			if($('#'+htmlid).length>0){
				return;
			}
			var index = $('.payTo.left').length;
			html +='<div index="'+index+'" id="'+htmlid+'" class="payTo left" style="left: auto; margin-left: 20px; margin-top: 10px; top: auto;position: static;">';
			html +='<div class="payToWarp arrowAfter">';
			html +='<div class="payToUser">';
			html +='<div class="Operator sucMsgTxt">';
			if(typeof data.isDelete != 'undefined' && data.isDelete==0){
				html +='<p><span class="green">'+content.name+"</span> 退出房间<i class='yellow'>"+content.roomname+'</i></p>';
			}else{
				html +='<p><span class="green">'+content.name+"</span> 加入了房间<i class='yellow'>"+content.roomname+'</i></p>';
			}
			html +='</div>';
			html +='</div>';
			html +='</div>';
			html +='</div>';
			$("#leftpanel").append(html);
			var gettop = function(){
				var _index = $('.payTo.left').length;
				if(_index==0){
					return '2%';
				}
				else{
					var up = $('div[index="'+(_index-1)+'"]');
					if(up && up.length!=0){
						return (up.offset().top+up.height()+10);
					}
				}
			}
			//$('#'+htmlid).css('top',gettop());
			$('#'+htmlid).slideDown(200,function(){
				//$('#'+htmlid).attr('index',$('.payTo.right').length);
				//$('#'+htmlid).css('top',gettop())
			});
		},
		remove:function(){}
	},
	//事故点
	6:{
		init:function(data,content){
			if(content && content!=''){
				var feature = map_plot.plotUtils.toFeature(content);
				addFeature(feature);
			}
		},
		remove:function(data,content){
			if(content && content!=''){
				var feature = map_plot.plotUtils.toFeature(content);
				remove(feature.getId());
			}
		}
	},
	//命令
	7:{
		init:function(data,content){
			//显示命令窗口
			var html = "";
			html +='<div id="commandshow" class="payTo" style="right:1%;margin-left:20px;margin-top:0px;left: auto;bottom: 1%;top:auto;width:355px;">';
			html +='<div class="chatContNav ca cf" >';
			html +='<ul>';
			html +='<li id="mlLi" onclick="chatContNavSwitch(1)"  class="activeLink">命令</li>';
			html +='<li id="rwLi" onclick="chatContNavSwitch(2)">发布任务</li>';
			html +='</ul>';
			html +='</div>';
			html +='<div class="chat" id="mlxxDiv">';
			html +='<div class="chatHd"><span>命令消息</span>';
			html +='</div>';
			html +='<div class="chatBox">';
			html +='<div class="chatBoxTxt">';
			html +='</div>';
			html +='</div>';
			html +='</div>';
			//发布任务列表
			html +='<div class="chat" id="fbrwDiv" style="display:none;">';
			html +='<div class="chatHd"><span>发布任务</span>';
			html +='</div>';
			html +='<div class="layim-chat-main missionTable">';
			html +='<table cellpadding="0" cellspacing="0" border="0" width="100%">';
			html +='<thead>';
			html +='<tr>';
			html +='<th>任务信息</th>';
			html +='<th>发布者</th>';
			html +='<th>状态</th>';
			html +='<th>反馈人</th>';
			html +='<th>反馈情况</th>';
			html +='</tr>';
			html +='</thead>';
			html +='<tbody id="chatRenwuListInfo">';
			
			html +='</tbody>';
			html +='</table>';
			html +='</div>';
			html +='</div>'; 
			html +='</div>';
			html +='</div>';
			
			if($('#commandshow').length==0){
				$(".ant-card.ant-card-wider-padding").after(html);
				$('#commandshow').slideDown(200);
			}
			var htmlid = 'show_7_'+data.id;
			if($('#'+htmlid).length>0){
				return;
			}
			var msghtml = '<p id="'+htmlid+'"><span class="adminUser">'+data.director+"("+isEmptyOrNo(timestampToTime(data.createTime))+")</span></p>";
		    msghtml += "<p class='adminContent'>"+content.command+'</p>';
			$('#commandshow .chatBoxTxt').append(msghtml);
		},
		remove:function(){}
	},
	//任务 
	13:{
		init:function(data,content){
			//显示命令窗口
			var html = "";
			html +='<div id="commandshow" class="payTo" style="right:1%;margin-left:20px;margin-top:0px;left: auto;bottom: 1%;top:auto;width:355px;">';
			html +='<div class="chatContNav ca cf">';
			html +='<ul>';
			html +='<li id="mlLi" onclick="chatContNavSwitch(1)" class="activeLink">命令</li>';
			html +='<li id="rwLi" onclick="chatContNavSwitch(2)">发布任务</li>';
			html +='</ul>';
			html +='</div>';
			html +='<div class="chat" id="mlxxDiv">';
			html +='<div class="chatHd"><span>命令消息</span>';
			html +='</div>';
			html +='<div class="chatBox">';
			html +='<div class="chatBoxTxt">';
			
			html +='</div>';
			html +='</div>';
			html +='</div>';
			//发布任务列表
			html +='<div class="chat" id="fbrwDiv" style="display:none;">';
			    html +='<div class="chatHd"><span>发布任务</span></div>';
				html +='<div class="layim-chat-main missionTable">';
					html +='<table cellpadding="0" cellspacing="0" border="0" width="100%">';
					html +='<thead>';
					html +='<tr>';
					html +='<th>任务信息</th>';
					html +='<th>发布者</th>';
					html +='<th>状态</th>';
					html +='<th>反馈人</th>';
					html +='<th>反馈情况</th>';
					html +='</tr>';
					html +='</thead>';
					html +='<tbody id="chatRenwuListInfo">';
					
					html +='</tbody>';
					html +='</table>';
					html +='</div>';			
			html +='</div>';
			
			html +='</div>';
			html +='</div>';
			
			if($('#commandshow').length==0){
				$("#mapHistory").append(html);
				$('#commandshow').slideDown(200);
			}
			var htmlid = 'show_13_'+data.id;
			if($('#'+htmlid).length>0){
				return;
			}
			var status = data.status==1?'未反馈':'已反馈';
		    var msghtml ='<tr id="'+htmlid+'"><td>'+data.actionDesc+'</td><td>'+data.publisher+'</td><td>'+status+'</td><td>'+data.feedback+'</td><td>'+data.feedbackDesc+'</td></tr>';
			$('#chatRenwuListInfo').append(msghtml);
		},
		remove:function(){}
	},
	
	//消防车
	8:{
		init:function(data,content){
			if(content && content!=''){
				var feature = map_plot.plotUtils.toFeature(content);
				addFeature(feature);
			}
		},
		remove:function(data,content){
			if(content && content!=''){
				var feature = map_plot.plotUtils.toFeature(content);
				remove(feature.getId());

				//deleteAccidentFeatureByid(feature.getId());
			}
		}
	},
	9:{
		init:function(data,content){
			if(content && content!=''){
				var feature = map_plot.plotUtils.toFeature(content);
				addFeature(feature);
			}
		},
		remove:function(data,content){
			if(content && content!=''){
				var feature = map_plot.plotUtils.toFeature(content);
				remove(feature.getId());
			}
		}
	},
	//救援路线的显示
	10:{
		init:function(data,content){
			if(content && content!=''){
				var feature = map_plot.plotUtils.toFeature(content);
				addFeature(feature);
			}
		},
		remove:function(data,content){
			if(content && content!=''){
				var feature = map_plot.plotUtils.toFeature(content);
				remove(feature.getId());
			}
		}
	}
}

//开始播放
function historyplay(){
	//为btnplay按钮标记isplay开始播放为1 暂停(没有播放)为0
	var btnplay =  $('#btnplay');
	var btnsuspend =  $('#btnsuspend');
	var isplay = btnplay.attr("isplay");
	//播放集合
	if(!window.listfun){
		window.listfun = [];
	}
	//暂停
	if(isplay && parseInt(isplay)==1){
		btnplay.attr("isplay",0);
		btnsuspend.addClass("disables");
		btnplay.removeClass("disables");
		//btnplay.val("开始播放");
		//清空集合
		for(var i=0;i<window.listfun.length;i++){
    	    clearTimeout(window.listfun[i]);
		}
		window.listfun = [];
		//关闭遮罩
		$('#dilixinxibianjiList_mask').remove();
	}else{
		btnplay.attr("isplay",1);
		//btnplay.val("暂停");
		btnsuspend.removeClass("disables");
		btnplay.addClass("disables");

		//遮罩
		var main = $('#dilixinxibianjiList');
		$('#dilixinxibianjiList_mask').remove();
		var mask = $('<div id="dilixinxibianjiList_mask" style="position: absolute;background: rgb(255, 255, 255,.1);z-index: 10000;"></div>');
		mask.css('width',main.width());
		mask.css('height',main.height());
		mask.css('top',main.offset().top);
		main.after(mask);

		//找到当前播放到第几个了
		var playindex = btnplay.attr("playindex");
		if(!playindex || playindex<=0){
			playindex = 0;
		}

		//开始注册播放事件
		var list = $('#list_'+playindex);
		//延迟
		var timeDifference = list.attr("timeDifference");
		if(!timeDifference || timeDifference==""){
			timeDifference = 0;//parseInt
		}
		while(list.length>=1){
			//注册方法
			(function(_playindex,_timeDifference){
				var fun = function(_playindex_,_timeDifference_){
					return	setTimeout(function(){
						//执行滚动动画
						$("#dilixinxibianjiList").animate({ 
							scrollTop: $("#dilixinxibianjiList").scrollTop() + $('#list_'+_playindex_).offset().top - $("#dilixinxibianjiList").offset().top+10 
						}, 1000);
						$('#list_'+_playindex_).click();
					},_timeDifference_);
				};
				(function(_fun){
					window.listfun.push(_fun);
				})(fun(_playindex,_timeDifference));
			})(playindex,parseInt(timeDifference)*1000);

			//计数器
			playindex++;
			list = $('#list_'+playindex);

			var thistime = list.attr("timeDifference");
			if(!thistime || thistime==""){
				timeDifference += 3;
			}else{
				timeDifference = (parseInt(timeDifference)+ parseInt(thistime));
			}

		}
		for(var i=0;i<window.listfun.length;i++){
    	    //window.listfun[i]();
		}
	}

}
//命令任务切换事件
function chatContNavSwitch(index){
	 if (index==1) {
	 	$("#mlxxDiv").show();
	 	$("#fbrwDiv").hide();
	 	$("#mlLi").addClass("activeLink");
	 	$("#rwLi").removeClass("activeLink")
	 }else if(index==2){
	 	$("#mlxxDiv").hide();
	 	$("#fbrwDiv").show();
	 	$("#mlLi").removeClass("activeLink");
	 	$("#rwLi").addClass("activeLink")
	 }
}
