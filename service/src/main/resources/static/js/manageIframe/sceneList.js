/**
 * 绘制
 */
//var baseUrl =window.location.protocol+"//"+window.location.host+"/";
var map,plot,images,plotType,utils;
var changeFeature = {};
var ESM = {};
var resourceType = -1;

//行政图和卫星图的图片数据加载list
var xzImageResourceList=[];
var wxImageResourceList=[];
var baiduMapLayer,baiduWeiXinMapLayer,cva_wLayer;
$(document).ready(function(){
	utils = new ESM.Utils();
	// 判断加载地图的类型
    if (loadMapType=="b"){
    	//行政图
        baiduMapLayer = new ol.layer.Tile({
       	    source: new ol.source.BaiduMap()
       });
        //卫星图
        baiduWeiXinMapLayer = new ol.layer.Tile({
    	    source: new ol.source.BaiduMap({"mapType":"sat"}),
            projection: 'EPSG:3857'
        })
    }else{
    	//行政图
        baiduMapLayer = new ol.layer.Tile({
       	    source: new ol.source.TianMap()
        });
        
        if(mapModel == 1){
       	// 天地图的lable
            cva_wLayer = new ol.layer.Tile({
           	    source: new ol.source.TianMap({"mapType":"label"})
            });
        }
        //卫星图
        baiduWeiXinMapLayer = new ol.layer.Tile({
    	    source: new ol.source.TianMap({"mapType":"sat"}),
            projection: 'EPSG:3857'
        })
    }
	
	map = new ol.Map({
		target: 'map',
		layers: loadMapType=="b" ? [baiduMapLayer,baiduWeiXinMapLayer] 
		: mapModel == 1 ? [baiduMapLayer,cva_wLayer,baiduWeiXinMapLayer]:[baiduMapLayer,baiduWeiXinMapLayer],
		view: new ol.View({
			center: ol.proj.transform([104.094752, 30.667451], 'EPSG:4326', 'EPSG:3857'),
			zoom: 8,
			minZoom: 3,
			maxZoom: 18
		})
	});
    //地图点击事件
    map.on('click', onClickHandler);
	plot = new olPlot(map, {
        zoomToExtent: true,
        isClear: true
    })
	
	plot.plotDraw.on('drawEnd', onDrawEnd)
	
	baiduMapLayer.setVisible(true); //显示行政
    baiduWeiXinMapLayer.setVisible(false); //关闭卫星
	
    getAllImageMap();
    //初始化场景数据(t_resource表)
    getResourceList();
	gertInitCoordinate();
	
	getClusterDataList();
    //初始化获取道路列表
    getRoadList();
});

function showWeiXingTu(){
	resourceType=2;
	
	for(j = 0,len=xzImageResourceList.length; j < len; j++) {
		var oid=xzImageResourceList[j].id;
		removeImagesByid(oid);
	}
	 baiduMapLayer.setVisible(false); //显示卫星
     baiduWeiXinMapLayer.setVisible(true); //关闭行政
     getAllImageMap();
}

function showDiTu(){
	resourceType=1;
	for(j = 0,len=wxImageResourceList.length; j < len; j++) {
		var oid=wxImageResourceList[j].id;
		removeImagesByid(oid);
	}
	baiduMapLayer.setVisible(true); //显示行政
    baiduWeiXinMapLayer.setVisible(false); //关闭卫星
    getAllImageMap();
}

//获取初始化场景中地图加载的图片
function getAllImageMap(_data){
	var params = {type:resourceType};
	$.ajax({
		type : "post",
		url : baseUrl+"/accidentResource/qryResourceListInMap.jhtml",
		data: JSON.stringify(params),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {
			var addtomap=[];
			if(resourceType==1){
				xzImageResourceList=data.data;
			}else if(resourceType==2){
				wxImageResourceList=data.data;
			}
			for(j = 0,len=data.data.length; j < len; j++) {
				var d = data.data[j];
				if(d.position && d.position!="")
				{
					addtomap.push({url:d.resourcePath,extent:d.position,id:d.id,imageId:d.id});
				}
			}
			
			addImagesToMap(addtomap);
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
	    }
	});
}

function gertInitCoordinate(){
	//获取地图初始化中心点坐标
    var urls =baseUrl+"/api/gis/getMapInitCenter.jhtml";
    var params = null
    executeAjax(urls,params,function(data){
		if(data.httpCode==200){
    		if(data.data){
    			//定位中心点	
    			map.getView().animate({zoom: 8}, {center: ol.proj.transform([data.data.lng,data.data.lat], 'EPSG:4326', 'EPSG:3857')});
    		}
    	}else {
    		layer.msg(data.msg, {time: 1000});
    	}
    });
}

/**
 * 获取初始化地图数据(wzcbd,zddw,xfsy,yjdw,sczz,xfdw)等数据
 */
function getClusterDataList(){
	var urls = baseUrl+"/api/geographicEdit/qryListAll.jhtml";
	var params = null
	executeAjax(urls,params,function(data){
		if(data.httpCode==200){
			if(data){
				clusterDo(data);
			}
		}else {
			layer.msg(data.msg, {time: 1000});
		}
	});
}

/**
 * 聚合接口
 * */
function clusterDo(data){
	var fmap = new HashMap();
	var format = new ol.format.GeoJSON();
	var features = format.readFeatures(data.data);
	features.forEach(function(feature){
		feature.set('cluster',true)
		var prop = feature.getProperties();
		var catecd = prop.catecd;
		if(fmap.containsKey(catecd)){
			fmap.get(catecd).push(feature);
		}else{
			var fs = [];
			fs.push(feature);
			fmap.put(catecd,fs);
		}
	});
	var keys =  fmap.keys();
	for(var i=0;i<keys.length;i++){
		var layerId = keys[i]+"ClusterLayer";
		var layer = layerIsExist(layerId,map);
		if(!layer){
			layer = createClusterLayer(layerId,keys[i]);
			map.addLayer(layer);
		}
		addFeatureToLayer(fmap.get(keys[i]),layer,true)
	}
}

/**
 * 初始化场景数据
 * @returns
 */
function getResourceList(){
	var html="";
	var urls = baseUrl+"/api/geographicEdit/queryResource.jhtml";
    var params = null
    executeAjax(urls,params,function(data){
		if(data.httpCode==200){
    		if(data.data&&data.data.length>0){
    			var addtomap=[];
    			for(j = 0,len=data.data.length; j < len; j++) {
    				var d = data.data[j];
    				if(d.position && d.position!=""){
    					addtomap.push({url:d.resourcePath,extent:d.position,id:d.id,imageId:d.id});
    				}
    			}
    			addImagesToMap(addtomap);
    		}
    	
    	}else {
    		layer.msg(data.msg, {time: 1000});
    	}
    });
}

/**
 * 获取道路列表
 * @returns
 */
function getRoadList(){
	var urls =baseUrl+"/api/geographicEdit/getRoadList.jhtml";
    var params = null
    executeAjax(urls,params,function(data){
		if(data.httpCode==200){
    		if(data.data&&data.data.length>0){
    			for(j = 0,len=data.data.length; j < len; j++) {
					if(data.data[j].content &&data.data[j].content!=""){
						var json = JSON.parse(data.data[j].content);
						var feature = plot.plotUtils.toFeature(json);
						addFeatureToMap(feature,data.data[j].id);
    				}
				}
    		}
    	}else {
    		layer.msg(data.msg, {time: 1000});
    	}
    });
}
/**
 * 标注回显功能
 * */
function addFeatureToMap(feature,id){
	feature.setId(id);
	var params = feature.get("params");
	feature.set("name",params.name);
	feature.set("desc",params.desc);
	addFeature(feature);
}
/**
 * 图标点击事件
 * */
function onClickHandler(event){
	var coord = event.coordinate;//地图点击位置的经纬度数值
	feature = map.forEachFeatureAtPixel(event.pixel, function (feature) {
		if(feature.get('features')){
			if(feature.get('features').length>1){
				var coordinate_ = feature.getGeometry().getCoordinates();
				map.getView().animate({zoom: map.getView().getZoom() + 1},{center: coordinate_},{duration:1000});
				return null;
			}else{
				return feature.get("features")[0];
			}
		}else{
			return feature
		}
	})
	
	if (feature && feature.get('isPlot') && !plot.plotDraw.isDrawing()) {
		plot.plotEdit.activate(feature);
	} else {
		plot.plotEdit.deactivate();
	}
	
	if(feature&&feature.get('imageClose')){
		console.info(feature.get('id'));
		var imageId = feature.get('id');
		removeImageByid(imageId);
		$("#"+imageId+"Radio").show();
		$("#"+imageId+"RadioInput").attr("checked",false);
	}
}

/**
 * 第4个功能点
 * */
function changeStyle(feature){
	changeFeature = feature;
	openInfoWindowByFeature(feature);
}

/**
 * 绘制功能结束的回调
 * */
function onDrawEnd(event){
	  var feature = event.feature;
	    var params = feature.get("params");
	    var fid = params.oid?params.oid:createRandomId();
	    feature.setId(createRandomId());
		feature.set("name",params.name);
		var iconType = params.recordType;
		if(params.type=="imageLocation"){
	    	var extent = feature.getGeometry().getExtent();
	    	var imageUrl = feature.get('params').url;
	    	var imageId = feature.get('params').imageId;
	    	var id = addImageToMap(extent,imageUrl,imageId,imageId);
	    	var layer = layerIsExist('plotImageLayer',map);
	 		if(!layer){
	 			layer = createImageExtentLayer('plotImageLayer');
	 			map.addLayer(layer);
	 		}
	 		// 根据imageId 禁用并改为未选中。
//	 		$("input[value='"+imageId+"']").attr("disabled",true);
//	 		$("input[value='"+imageId+"']").attr("checked",false);
	 		
	    	feature.set('plotType',plotType);
	    	feature.on("change",imageExtentChange);
	    	feature.setId(id);
	    	
	    	var closeFeature = new ol.Feature({
	    		geometry:new ol.geom.Point(ol.extent.getTopRight(extent))
	    	});
	    	closeFeature.set('imageClose',true);
	    	closeFeature.set('id',id);
	    	var closeId = 'close' + id;
	    	closeFeature.setId(closeId);
	    	layer.getSource().addFeature(feature);
	    	layer.getSource().addFeature(closeFeature);
	    	//左侧列表逻辑，设置图片不可用
	    	$("#"+imageId+"Radio").hide();
	    }
		
		changeFeature = feature;
		plot.plotEdit.activate(feature);
}

/**
 * 窗口弹出
 * */
function openPopup(point,html){
	setContent(html);
	setPopupPosition(point);
}

/**
 * 关闭弹出窗口
 * */
function close(){
	var overlay = map.getOverlayById('overlay');
	overlay.setPosition(undefined);
    $("#popup-closer").blur();
    $("#popup-content").html('');
    return false;
}

/**
 * 设置弹出内容
 * */
function setContent(html){
	$("#popup-content").html(html);
}

/**
 * 在坐标点位置打开窗口
 * */
function setPopupPosition(coord){
	var overlay = map.getOverlayById('overlay');
	overlay.setPosition(coord);
}

/**
 * 标注回显功能
 * */
function addJsonToMap(id,json){
	var feature = format.readFeature(json);
	feature.setId(id);
	addFeature(feature);
}

/**
 * 添加标绘要素
 * */
function addFeature(feature){
	var params = feature.get('params');
	if(params){
		
	}
	var layer = layerIsExist('geographicEditLayer',map);
	if(!layer){
		layer = createMarkLayer('geographicEditLayer');
		map.addLayer(layer);
	}
	layer.getSource().addFeature(feature);
}

/**
 * 激活绘制功能
 * */
function activate(type,params){
	plot.plotEdit.deactivate();
    plot.plotDraw.active(type, params);
}

/**
 * 添加图片到地图
 * @param extent 经纬度坐标范围[minx,miny,maxx,maxy]
 * @param image 图片的url路径 ‘images/chengdu.png’
 * */
function addImageToMap(extent,image,id,imageId){
	var group = layerIsExist('imageGroup',map);
	if(!group){
		group = createGroupLayer('imageGroup');
		map.addLayer(group);
	}
	if(id){
		var layer;
		group.getLayers().forEach(function(lay){
			if(lay.get('id')==id){
				layer = lay;
			}
		});
		if(layer&&extent){
			var source = new ol.source.ImageStatic({
				url:image,
				crossOrigin: '',
				projection: 'EPSG:3857',
	            imageExtent: extent
			})
			layer.setSource(source);
		}else{
			var layer = new ol.layer.Image({
				source: new ol.source.ImageStatic({
					url:image,
					crossOrigin: '',
					projection: 'EPSG:3857',
		            imageExtent: extent
				})
			});
			layer.set('id',id);
			group.getLayers().push(layer);
		}
	}else{
		var layer = new ol.layer.Image({
			source: new ol.source.ImageStatic({
				url:image,
				crossOrigin: '',
				projection: 'EPSG:3857',
	            imageExtent: extent
			})
		});
		id = createRandomId();
		layer.set('id',id);
		group.getLayers().push(layer);
	}
	var imageInfo = {};
	imageInfo.extent = extent.toString();
	imageInfo.url = image;
	imageInfo.imageId = imageId;
	if(images){
		if(images.containsKey(id)){
			images.remove(id);
		}
	}else{
		images = new HashMap();
	}
	images.put(id,imageInfo);
	return id;
}

/**
 * 删除场景搭建
 * */
function removeImageByid(id){
	var layer;
	var group = layerIsExist('imageGroup',map);
	group.getLayers().forEach(function(lay){
		if(lay.get('id')==id){
			layer = lay;
		}
	});
	if(layer){
		group.getLayers().remove(layer);
	}
	
	var plotLayer = layerIsExist('plotImageLayer',map);
	if(plotLayer){
		var feature = plotLayer.getSource().getFeatureById(id);
		plotLayer.getSource().removeFeature(feature);
		
		var closeFeature = plotLayer.getSource().getFeatureById('close'+id);
		plotLayer.getSource().removeFeature(closeFeature);
	}
	
	if(images&&images.containsKey(id)){
		images.remove(id);
	}
}


/**
 * 显示场景搭建历史
 * */
function addImagesToMap(images){
	var group = layerIsExist('imagesGroup',map);
	if(!group){
		group = createGroupLayer('imagesGroup');
		map.addLayer(group);
	}
	for(var i=0;i<images.length;i++){
		var image = images[i];
		var layer = new ol.layer.Image({
			source: new ol.source.ImageStatic({
				url:baseFileUrl+image.url,
				crossOrigin: '',
				projection: 'EPSG:3857',
	            imageExtent: image.extent.split(",")
			})
		});
		if(image.id){
			layer.set('id',image.id);
			removeImagesByid(image.id);
		}
		layer.setZIndex(2);
		group.getLayers().push(layer);
	}
}


function clearImagesGroup(){
	var group = layerIsExist('imagesGroup',map);
	if(group){
		map.removeLayer(group);
	}
}


//生成随机数
function createRandomId() {
    return (Math.random()*10000000).toString(16).substr(0,4)+'-'+(new Date()).getTime()+'-'+Math.random().toString().substr(2,5);
}

//标点保存
function saveImageActivate(){
	var id = $('#scenebuildingList input[name="sbradio"]:checked ').val();
	var ex = getImageExtent();
	if(!id){
		layer.msg('请选择一个场景', {icon: 0});
		return;
	}else if(ex.length == 0){
		layer.msg('未绘制标点', {icon: 0});
		return;
	}
	var params= ex;
//	for(var i=0;i<params.length;i++){
//		params[i].url=params[i].url.replace(baseUrl,"");
//	}
	$.ajax({
		type : "post",
		url : baseUrl+"/accidentResource/updateResource.jhtml",
		data: JSON.stringify(params),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
				var addtomap=[];
				for(j = 0,len=data.data.length; j < len; j++) {
					var d = data.data[j];
					if(d.position && d.position!=""){
						addtomap.push({url:d.resourcePath,extent:d.position,id:d.id,imageId:d.id});
					}
				}
				addImagesToMap(addtomap);
				layer.msg("保存成功");
		},error: function(request) {
			alert("网络错误");
	    }
	})
} 

//开始绘制标点
function setImageActivate(){
	var id = $('#scenebuildingList input[name="sbradio"]:checked ').val();
	if(!id){
		layer.msg('请选择一个场景', {icon: 0});
		return;
	}
	
	var scr = $('#scenebuildingList input[name="sbradio"]:checked ').parents('li').find('.sceneImg img').attr('src');
	var imageId = $('#scenebuildingList input[name="sbradio"]:checked ').parents('li').attr("id");
	locationImageActivate(scr,imageId);
}

$("body").delegate('#scenebuilding_FileUpload', 'change', function(){
	//$('#scenebuilding_FileUpload').on('change',(function(){
		return scenebuilding_FileUpload_onselect();
	});

//上传
function scenebuilding_FileUpload_onselect(){
	var _File = document.getElementById("scenebuilding_FileUpload").files[0];
	if(!/image\/\w+/.test(_File.type)) {
		layer.msg('请上传图片文件', {icon: 1,time: 2000});
		return false;
	}
	var formData = new FormData();
	formData.append("file",_File);
	formData.append("enctype","multipart/form-data");
	$.ajax({
		type: 'POST',
		url: baseUrl+'/common/uploadImg.jhtml',
		data: formData,
		processData: false,
		contentType: false,
		success:function(data){
			var params={
				filePath: data.data.url,
				fileName :data.data.name,
				type :1
			}
			$.ajax({
				type : "post",
				url : baseUrl+"/accidentResource/addResource.jhtml",
				data: JSON.stringify(params),
				dataType:"json",
				headers: {"Content-Type": "application/json;charset=utf-8"},
				success : function(data) {
					layer.msg('上传成功', {icon: 1,time: 1000});
//						getDataByParam("场景搭建");
					// 动态追加 新增的图片信息
					if (data.data) {
						var imgHtml = getScenebuildingHtml(data.data);
						$("#scenebuildingList ul").append(imgHtml);
						
						//注册鼠标经过事件 显示删除图层
						$("#scenebuildingList ul").find('.sceneDel').hide();
						$("#scenebuildingList ul").find('li').mouseenter(function(){
							if($(this).find(".sceneDel").css("display")=="none"){
								$(this).find(".sceneDel").fadeIn("fast");
							}
						})
						$("#scenebuildingList ul").find('li').mouseleave(function(){
							if($(this).find(".sceneDel").css("display")!="none"){
								$(this).find(".sceneDel").fadeOut("fast");
							}
						})
						
						//注册删除事件
						$("#scenebuildingList ul").find('.sceneDel').click(function(){

							var id = $(this).attr("data");
							layer.confirm('是否删除该信息？', {
						         btn: ['确定','取消'] //按钮
						    },function(){
								delscenebuildingByid(id);
						    }, function(){
						    	layer.msg('已取消', {icon: 1, time: 1000});
						    });
						})
						
					} else {
						layer.msg("上传失败！", {time: 1000});
					}
						
				},error: function(request) {
					layer.msg("上传失败！", {time: 1000});
				}
			})
			
		}
	});
}


//获取初始化场景中地图加载的图片
function getAllImageMap(_data){
	var params = {};
	var html = "";
	$.ajax({
		type : "post",
//		url : baseUrl+"/webApi/qrySceneBuildingList.jhtml",
		url : baseUrl+"/accidentResource/qryResourceListInMap.jhtml",
		data: JSON.stringify(params),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {
			var addtomap=[];
			for(j = 0,len=data.data.length; j < len; j++) {
				var d = data.data[j];
				if(d.position && d.position!="")
				{
					addtomap.push({url:d.resourcePath,extent:d.position,id:d.id,imageId:d.id});
				}
				html+=getScenebuildingHtml(data.data[j]);
			}
			$("#scenebuildingList ul").find("li").remove();
			$("#scenebuildingList ul").append(html);
			addImagesToMap(addtomap);
			allEventSceneUl();
		},error: function(request) {
			alert("网络错误");
	    }
	});
}



function allEventSceneUl(){
		//注册鼠标经过事件 显示删除图层
		$("#scenebuildingList ul").find('.sceneDel').hide();
		$("#scenebuildingList ul").find('li').mouseenter(function(){
			if($(this).find(".sceneDel").css("display")=="none"){
				$(this).find(".sceneDel").fadeIn("fast");
			}
		})
		$("#scenebuildingList ul").find('li').mouseleave(function(){
			if($(this).find(".sceneDel").css("display")!="none"){
				$(this).find(".sceneDel").fadeOut("fast");
			}
		})
		
		//注册删除事件
		$("#scenebuildingList ul").find('.sceneDel').click(function(){

			var id = $(this).attr("data");
			layer.confirm('是否删除该信息？', {
		         btn: ['确定','取消'] //按钮
		    },function(){
				delscenebuildingByid(id);
		    }, function(){
		    	layer.msg('已取消', {icon: 1, time: 1000});
		    });
		})
}

/*场景的详情*/
function getScenebuildingHtml(obj){
	var html='';
	html+='<li id="'+obj.id+'">';
	html+='	<div class="sceneImg">';
	html+='	  <img src="'+baseFileUrl+obj.resourcePath+'" alt="" />';
	html+='   <div class="sceneDel" data="'+obj.id+'"><span><i></i></span></div>';
	html+=' </div>';
	html+=' <div class="sceneTxt">';
	html+='	  <span>'+obj.resourceName+'</span>';
	html+='	  <label class="custom-checkbox"><input class="check-zhengshe" style="display:none;" type="radio" name="sbradio" value="'+obj.id+'"><i></i></label>';
	html+=' </div>';
	html+='</li>';
	
	return html;
}

//通过ID删除场景
function delscenebuildingByid(id){	
	params = {"id":id};
	$.ajax({
		type : "post",
		url : baseUrl+"/accidentResource/delResourceById.jhtml",
		data: JSON.stringify(params),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {
			if(data.data==1){
				layer.msg('删除成功', {icon: 1, time: 1000});
				getAllImageMap();
				removeImagesByid(id);
				$("#scenebuilding_FileUpload").val("");
			}else{
				layer.msg('删除失败', {icon: 1, time: 1000});
			}
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
		}
	})
}


/**
 * 删除场景搭建历史
 * */
function removeImagesByid(id){
	var layers = [];
	var group = layerIsExist('imagesGroup',map);
	group.getLayers().forEach(function(lay){
		if(lay.get('id')==id){
			layers.push(lay);
		}
	});
	
	if(layers&&layers.length>0){
		for(var j = 0,len = layers.length; j < len; j++){
			group.getLayers().remove(layers[j]);
		}
	}
	
//	var plotLayer = layerIsExist('plotImageLayer',map);
//	if(plotLayer){
//		var feature = plotLayer.getSource().getFeatureById(id);
//		plotLayer.getSource().removeFeature(feature);
//	}
}


function imageExtentChange(event){
	var extent = event.target.getGeometry().getExtent();
	var image = event.target.get('params').url;
	var id = event.target.getId();
	var imageId = event.target.get('params').imageId;
	addImageToMap(extent,image,id,imageId);
	
	var plotLayer = layerIsExist('plotImageLayer',map);
	if(plotLayer){
		var closeId = 'close' + id;
		var feature = plotLayer.getSource().getFeatureById(closeId);
		if(feature){
			feature.setGeometry(new ol.geom.Point(ol.extent.getTopRight(extent)));
		}
	}
}

/**
 * 删除获取图片四角坐标，并清空地图
 * */
function getImageExtent(){
	var result = [];
	var layer = layerIsExist('plotImageLayer',map);
	var imageLayer = layerIsExist('imageGroup',map);
	if(imageLayer){
		if(images){
			result = images.values();
		}
	}
	images = null;
	clearImageLayer();
	return result;
}

/**
 * 清空场景搭建图层
 * */
function clearImageLayer(){
	var layer = layerIsExist('plotImageLayer',map);
	var imageLayer = layerIsExist('imageGroup',map);
	if(imageLayer){
		map.removeLayer(imageLayer);
		layer.getSource().clear();
		plot.plotEdit.deactivate();
		
	}
}

function locationImageActivate(url,imageId){
	var params = {type:"imageLocation",url:url,noSocketSend:true,imageId:imageId};
	plotType = "RectAngle";
	activate("RectAngle",params);
}