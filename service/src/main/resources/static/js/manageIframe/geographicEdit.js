/**
 * 绘制
 */
//var baseUrl =window.location.protocol+"//"+window.location.host+"/";
var map,plot,utils;
var changeFeature = {};
var beforeFeature;
var ESM = {};
var resourceType = -1;
//行政图和卫星图的图片数据加载list
var xzImageResourceList=[];
var wxImageResourceList=[];
//百度地图层
var baiduMapLayer,baiduWeiXinMapLayer,cva_wLayer;
$(document).ready(function(){
	// 判断加载地图的类型
    if (loadMapType=="b"){
    	//行政图
        baiduMapLayer = new ol.layer.Tile({
       	    source: new ol.source.BaiduMap()
       });
        //卫星图
        baiduWeiXinMapLayer = new ol.layer.Tile({
    	    source: new ol.source.BaiduMap({"mapType":"sat"}),
            projection: 'EPSG:3857'
        })
    }else{
    	//行政图
        baiduMapLayer = new ol.layer.Tile({
       	    source: new ol.source.TianMap()
        });
        
        if(mapModel == 1){
       	// 天地图的lable
            cva_wLayer = new ol.layer.Tile({
           	    source: new ol.source.TianMap({"mapType":"label"})
            });
        }
        //卫星图
        baiduWeiXinMapLayer = new ol.layer.Tile({
    	    source: new ol.source.TianMap({"mapType":"sat"}),
            projection: 'EPSG:3857'
        })
    }
	
	utils = new ESM.Utils();
	map = new ol.Map({
		target: 'map',
		layers: loadMapType=="b" ? [baiduMapLayer,baiduWeiXinMapLayer] 
		: mapModel == 1 ? [baiduMapLayer,cva_wLayer,baiduWeiXinMapLayer]:[baiduMapLayer,baiduWeiXinMapLayer],
		view: new ol.View({
			center: ol.proj.transform([104.094752, 30.667451], 'EPSG:4326', 'EPSG:3857'),
			zoom: 8,
			minZoom: 3,
			maxZoom: 18
		})
	});
	
    initPopup(map);
    
    //地图点击事件
    map.on('singleclick', onClickHandler);
    map.on('moveend', onMoveEndHandler);
    
    baiduMapLayer.setVisible(true); //显示行政
    baiduWeiXinMapLayer.setVisible(false); //关闭卫星
    
	plot = new olPlot(map, {
        zoomToExtent: true,
        isClear: true
    })
	
	plot.plotDraw.on('drawEnd', onDrawEnd)
	
	/**
	 * 获取初始化地图数据(wzcbd,zddw,xfsy,yjdw,sczz,xfdw)等数据
	 */
	var urls = baseUrl+"/api/geographicEdit/qryListAll.jhtml";
    var params = null
    executeAjax(urls,params,function(data){
		if(data.httpCode==200){
    		if(data){
				clusterDo(data);
			}
    	}else {
    		layer.msg(data.msg, {time: 1000});
    	}
    });
    
    //初始化获取道路列表
    getRoadList(true);
    //初始化场景数据(t_resource表)
    getResourceList();
    gertInitCoordinate();
});

function gertInitCoordinate(){
	//获取地图初始化中心点坐标
    var urls =baseUrl+"/api/gis/getMapInitCenter.jhtml";
    var params = null
    executeAjax(urls,params,function(data){
		if(data.httpCode==200){
    		if(data.data){
    			//定位中心点	
    			map.getView().animate({zoom: 8}, {center: ol.proj.transform([data.data.lng,data.data.lat], 'EPSG:4326', 'EPSG:3857')});
    		}
    	}else {
    		layer.msg(data.msg, {time: 1000});
    	}
    });
}

function onMoveEndHandler(event){
//	console.log("moveend");
}


function showWeiXingTu(){
	resourceType=2;
	
	for(j = 0,len=xzImageResourceList.length; j < len; j++) {
		var oid=xzImageResourceList[j].id;
		removeImagesByid(oid);
	}
	 baiduMapLayer.setVisible(false); //显示卫星
     baiduWeiXinMapLayer.setVisible(true); //关闭行政
     getAllImageMap();
}

function showDiTu(){
	resourceType=1;
	for(j = 0,len=wxImageResourceList.length; j < len; j++) {
		var oid=wxImageResourceList[j].id;
		removeImagesByid(oid);
	}
	baiduMapLayer.setVisible(true); //显示行政
    baiduWeiXinMapLayer.setVisible(false); //关闭卫星
    getAllImageMap();
}

//获取初始化场景中地图加载的图片
function getAllImageMap(_data){
	var params = {type:resourceType};
	$.ajax({
		type : "post",
		url : baseUrl+"/accidentResource/qryResourceListInMap.jhtml",
		data: JSON.stringify(params),
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {
			var addtomap=[];
			if(resourceType==1){
				xzImageResourceList=data.data;
			}else if(resourceType==2){
				wxImageResourceList=data.data;
			}
			for(j = 0,len=data.data.length; j < len; j++) {
				var d = data.data[j];
				if(d.position && d.position!="")
				{
					addtomap.push({url:d.resourcePath,extent:d.position,id:d.id,imageId:d.id});
				}
			}
			
			addImagesToMap(addtomap);
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
	    }
	});
}

/**
 * 获取道路列表
 * @returns
 */
function getRoadList(isDrawMap){
	$("#roadList").html("");
	var html="";
	var urls =baseUrl+"/api/geographicEdit/getRoadList.jhtml";
    var params = null
    executeAjax(urls,params,function(data){
		if(data.httpCode==200){
    		if(data.data&&data.data.length>0){
    			for(j = 0,len=data.data.length; j < len; j++) {
					html+=getRoadHtml(data.data[j]);
					//本页面删除、修改、新增不需要重新绘制地图
					if(isDrawMap){
						if(data.data[j].content &&data.data[j].content!=""){
							var json = JSON.parse(data.data[j].content);
							var format = new ol.format.GeoJSON();
							var feature = plot.plotUtils.toFeature(json);
							addFeatureToMap(feature,data.data[j].id);
						}
					}
				}
    		}
    		$("#roadList").append(html);
    	}else {
    		layer.msg(data.msg, {time: 1000});
    	}
    });
}

/**
 * 初始化场景数据
 * @returns
 */
function getResourceList(){
	var html="";
	var urls = baseUrl+"/api/geographicEdit/queryResource.jhtml";
    var params = null
    executeAjax(urls,params,function(data){
		if(data.httpCode==200){
    		if(data.data&&data.data.length>0){
    			var addtomap=[];
    			for(j = 0,len=data.data.length; j < len; j++) {
    				var d = data.data[j];
    				if(d.position && d.position!=""){
    					addtomap.push({url:d.resourcePath,extent:d.position,id:d.id,imageId:d.id});
    				}
    			}
    			addImagesToMap(addtomap);
    		}
    	
    	}else {
    		layer.msg(data.msg, {time: 1000});
    	}
    });
}

/*道路拼接html*/
function getRoadHtml(obj){
	var html='<li onclick="centerAt(\''+obj.id+'\')">'; 
		html+="<h2><i class='environment'></i>"+obj.name+"</h2>";
		html+="<p>"+obj.desc+"</p>";
		html+="</li>";
    return html;
}

/*左侧点击道路定位并弹出详情*/
function gotoPointLocation(obj){
	var json = JSON.parse(obj.content);
	var format = new ol.format.GeoJSON();
	var feature = plot.plotUtils.toFeature(json);
	addFeature(feature);
}

/**
 * 聚合接口
 * */
function clusterDo(data){
	var fmap = new HashMap();
	var format = new ol.format.GeoJSON();
	var features = format.readFeatures(data.data);
	features.forEach(function(feature){
		feature.set('cluster',true)
		var prop = feature.getProperties();
		var catecd = prop.catecd;
		if(fmap.containsKey(catecd)){
			fmap.get(catecd).push(feature);
		}else{
			var fs = [];
			fs.push(feature);
			fmap.put(catecd,fs);
		}
	});
	var keys =  fmap.keys();
	for(var i=0;i<keys.length;i++){
		var layerId = keys[i]+"ClusterLayer";
		var layer = layerIsExist(layerId,map);
		if(!layer){
			layer = createClusterLayer(layerId,keys[i]);
			map.addLayer(layer);
		}
		addFeatureToLayer(fmap.get(keys[i]),layer,true)
	}
}

/**
 * 图标点击事件
 * */
function onClickHandler(event){
	var coord = event.coordinate;//地图点击位置的经纬度数值
	feature = map.forEachFeatureAtPixel(event.pixel, function (feature) {
		if(feature.get('features')){
			if(feature.get('features').length>1){
				var coordinate_ = feature.getGeometry().getCoordinates();
				map.getView().animate({zoom: map.getView().getZoom() + 1},{center: coordinate_},{duration:1000});
				return null;
			}else{
				return feature.get("features")[0];
			}
		}else{
			return feature
		}
	})
	if (feature && feature.get('isPlot') && !plot.plotDraw.isDrawing()) {
		plot.plotEdit.activate(feature);
		changeStyle(feature);
	} else {
		plot.plotEdit.deactivate();
	}
	 /**
	 * mark点击功能
	 */
	if(feature&&feature.get('cluster')){
		return;
	}
}

/**
 * 第4个功能点
 * */
function changeStyle(feature){
	changeFeature = feature;
	openInfoWindowByFeature(feature);
}

//图层面板扎图标
function makerFireEngine(dtPath,id,iconType,name,jhPath,clusterType){
	var params ={};
	params.clusterType='accident';
	params.imagePath="/mapIcon/mapPanel/mee-12.png";
	params.isCluster=true;
	params.clusterImagePath="/mapIcon/mapPanel/mee-12.png";
	params.isSyn=false;
	params.isSave=false;
	params.recordType='road';
	params.action="编辑道路";
	params.name=name;
	addRoadLineEvent("dlhz",params);
}

/**
* 调用marker标绘
* */
function addMakerEngine(params){
	params.type = 'marker';
    activate('Point',params);
}

/**
 * 绘制功能结束的回调
 * */
function onDrawEnd(event){
	var feature = event.feature;
    var params = feature.get("params");
    var fid = params.oid?params.oid:createRandomId();
    feature.setId(createRandomId());
	feature.set("name",params.name);
	feature.set("scolor","#ff00ff");
	var iconType = params.recordType;
	if(iconType=="road"){
		var layer = layerIsExist('geographicEditLayer',map);
 		if(!layer){
 			layer = createMarkLayer('geographicEditLayer');
 			map.addLayer(layer);
 		}
    	feature.set('plotType',"geographicEdit");
    	layer.getSource().addFeature(feature);
		openInfoWindowByFeature(feature);
    }
	changeFeature = feature;
	feature.scolor = "#000";
	plot.plotEdit.activate(feature);
}

/**
 * 标绘结束后的弹框
 * 根据feature 弹出弹出框
 *
 */
function openInfoWindowByFeature(feature){
	var point = null;
	if(feature.getGeometry().getType()=="Point"){
		point = feature.getGeometry().getCoordinates();
	}else if(feature.getGeometry().getType()=="Polygon"){
		point = feature.getGeometry().getInteriorPoint().getCoordinates();
	}else if(feature.getGeometry().getType()=="LineString"){
		point = feature.getGeometry().getFirstCoordinate();
	}
	var id  = '#markerText';
	//type 为20的是需要输入是输入名字和数量，其他为需要输入数量
	var obj = feature.get('params');
	var value = obj.name?obj.name:'';
	var desc = obj.desc?obj.desc:'';
	var iconType = obj.recordType;
	var content='';
	if(iconType=='road'){
		content = '<div class="olmatter">'
			+'<div class="ca cf"><span class="fl olName">道路名称2：</span><span class="fl olInput"><input id="roadName" type="text" value="'+value+'" /></span></div>'
			+'<div class="ca cf"><span class="fl olName">道路描述：</span><span class="fl olInput h40"><textarea name="roadDesc" class="textarea" id="roadDesc" type="text" rows="">'+desc+'</textarea></span></div>'
			+'<div class="olbtn"><span class="fl olName">&nbsp;&nbsp;</span><span class="fl"><input type="button" value="确定" onClick="saveRoad(\''+feature.getId()+'\')" class="olSure fl">'
			+'&nbsp<input type="button" value="删除" onClick="removeFeature(\''+feature.getId()+'\',\'road\')" class="olCancel fl"></span></div></div>';
	}
	/**
	 * 弹出道路信息框
	 */
	openPopup(point,content);
}

/**
 * 保存画的道路
 * @returns
 */
function saveRoad(oid){
	var name = $("#roadName").val();
	var desc = $("#roadDesc").val();
	if(name.length<1){
		layer.msg("请填写道路名称!", {
   		  time: 1000 
   		});
		return false;
	}
	if(desc.length<1){
		layer.msg("请填写道路描述!", {
   		  time: 1000 
   		});
		return false;
	}
	
	var obj = changeFeature.get('params');
	var layer = layerIsExist('geographicEditLayer',map);
	var fid = changeFeature.getId();	
	var feature = layer.getSource().getFeatureById(fid);
	var json = plot.plotUtils.fromFeature(feature);
	var params = obj;
	params.name = name;
	params.desc = desc;
	changeFeature.set('params',params);
	var paramsRoad={};
	paramsRoad.content = JSON.stringify(json);
	paramsRoad.name=name;
	paramsRoad.desc=desc;
	if(oid.indexOf("-")!=-1){
	}else{
		paramsRoad.id=oid;
	}
	$.ajax({
		type:'POST',
		url: baseUrl+'/api/geographicEdit/create',
		headers: {"Content-Type": "application/json;charset=utf-8"},
		data: JSON.stringify(paramsRoad),
		dataType:'JSON',
		success: function(data){
			if(data.httpCode==200){		
				changeFeature.setId(data.data.id);
				close();
				getRoadList(false);
			}
		}
	});
}

/**
 * 初始化弹出窗口
 * */
function initPopup(map){
	var element = document.createElement('div');
	element.className='ol-popup';
	element.id='popup';
	var child =  '<a href="#" id="popup-closer" class="ol-popup-closer"></a>';
	child += '<div id="popup-content"></div>';
	element.innerHTML += child;
	var overlay = new ol.Overlay({
		id: 'overlay',
		element: element,
		offset:[0,5],
		autoPan: true,
		positioning:'top-center',
		autoPanAnimation: {
			duration: 250
        }
	});
	map.addOverlay(overlay);
	$("#popup-closer").click(function(event){
		close();
	})
}

/**
 * 窗口弹出
 * */
function openPopup(point,html){
	setContent(html);
	setPopupPosition(point);
}

/**
 * 关闭弹出窗口
 * */
function close(){
	var overlay = map.getOverlayById('overlay');
	overlay.setPosition(undefined);
    $("#popup-closer").blur();
    $("#popup-content").html('');
    return false;
}

/**
 * 设置弹出内容
 * */
function setContent(html){
	$("#popup-content").html(html);
}

/**
 * 在坐标点位置打开窗口
 * */
function setPopupPosition(coord){
	var overlay = map.getOverlayById('overlay');
	overlay.setPosition(coord);
}

/**
 * 标注回显功能
 * */
function addFeatureToMap(feature,id){
	feature.setId(id);
	var params = feature.get("params");
	feature.set("name",params.name);
	feature.set("desc",params.desc);
	addFeature(feature);
}


/**
 * 楼块标绘
 * */
function addPolygonEngine(params){
	activate('Polygon',params);
}

function buffer(geometry){
	var parser = new jsts.io.OL3Parser();
	var jstsGeom = parser.read(geometry);
	var buffered = jstsGeom.buffer(40);
	var bufferGeom = parser.write(buffered);
	var feature = new ol.Feature({
		geometry:bufferGeom
	});
	return feature;
}
/**
 * 激活绘制功能
 * */
function activate(type,params){
	plot.plotEdit.deactivate();
    plot.plotDraw.active(type, params);
}

//救援路线
function makerEscapeLineEngine(oid,iconType,name){
	var params ={};
	params.oid=oid;
	params.name=name;
	params.isSyn=true;
	params.isSave=true;
	params.recordType="4";
	params.action="救援路线";
	params.scolor="red";
	addSpecialLineEvent('jylx',params);
}

/**
* 应急路线
* @param type 应急路线类型
* @param color 路线颜色
* */
function addSpecialLineEvent(type,params){
	params.type=type;
	params.isSpecial=true;
	activate("Polyline",params);
}

/**
 * 绘制道路
 * */
function addRoadLineEvent(type,params){
	params.type = type;
	params.isSpecial = false;
	activate("Polyline",params);
}

/**
 * 添加罐体
 * */
function addOilcanFeature(feature){
	var layer = layerIsExist('oilcanMakerLayer',map);
	if(!layer){
		layer = createNoStyleLayer('oilcanMakerLayer');
		map.addLayer(layer);
	}
	var params = feature.get(params);
	var image = params.image;
	var id = params.id;
	var style = new ol.style.Style({
		image : new ol.style.Icon({
			src : baseFileUrl+image,
			anchor : [ 0.5, 0.5 ]
		}),
		text : new ol.style.Text({
			text : params.name,
			offsetY : 10,
			font : 'bold 12px sans-serif',
			fill : new ol.style.Fill({
				color : '#84C1FF'
			}),
			stroke : new ol.style.Stroke({
				color : '#ECF5FF',
				width : 3
			})
		})
	});
	feature.setStyle(style);
	feature.setId(id);
	layer.getSource().addFeature(feature);
	//添加罐体外围图形
	var coord = feature.getGeometry().getCoordinates();
	addOilcanOuterFeature(coord,params.distance);
}

/**
 * 更新罐体数据
 * */
function updateOilcanFeature(id,distance){
	var layer = layerIsExist('oilcanMakerLayer',map);
	if(layer){
		var feature = layer.getSource().getFeatureById(id);
		if(feature){
			var params = feature.get('params');
			params.distance = distance;
			updateOilcanOuterFeature(id,distance,center);
		}
	}
}

/**
 * 删除罐体图标
 * */
function removeOilcanFeature(id){
	var layer = layerIsExist('oilcanMakerLayer',map);
	if(layer){
		var feature = layer.getSource().getFeatureById(id);
		if(feature){
			layer.getSource().removeFeature(feature);
			removeOilcanOuterFeature(id);
			var center = feature.getGeometry().getCoordinates();
			
		}
	}
}

/**
 * 添加罐体的外围图形
 * */
function addOilcanOuterFeature(center,distance){
	var layer = layerIsExist('markerOuterLayer',map);
	if(!layer){
		layer = createNoStyleLayer('markerOuterLayer');
		map.addLayer(layer);
	}
	var id = params.id;
	var distance = params.distance;
	var point = utils.fromLonLat(utils.calculatePoint(utils.toLonLat(center),distance,1.57));
	var radius = utils.calculateRadius(center,point);
	var circle = new ol.geom.Circle(center,radius);
	var feature = new ol.Feature({
		geometry: circle
	});
	var style = new ol.style.Style({
		fill:null,
		stroke: new ol.style.Stroke({
			color:'#ff00ff',
			width: 1
		})
	});
	feature.setStyle(style);
	feature.setId(id);
	layer.getSource().addFeature(feature);
}
/**
 * 根据id删除罐体外围图形
 * */
function removeOilcanOuterFeature(id){
	var layer = layerIsExist('markerOuterLayer',map);
	if(layer){
		var feature = layer.getSource().getFeatureById(id);
		if(feature){
			layer.getSource.removeFeature(feature);
		}
	}
}
/**
 * 更新
 * */
function updateOilcanOuterFeature(id,distance,center){
	var point = utils.fromLonLat(utils.calculatePoint(utils.toLonLat(center),distance,1.57));
	var radius = utils.calculateRadius(center,point);
	var circle = new ol.geom.Circle(center,radius);
	var layer = layerIsExist('markerOuterLayer',map);
	if(layer){
		var feature = layer.getSource().getFeatureById(id);
		if(feature){
			feature.setGeometry(circle);
		}
	}
}

/**
 * 添加标绘要素
 * */
function addFeature(feature){
	var params = feature.get('params');
	if(params){
		
	}
	var layer = layerIsExist('geographicEditLayer',map);
	if(!layer){
		layer = createMarkLayer('geographicEditLayer');
		map.addLayer(layer);
	}
	layer.getSource().addFeature(feature);
}

/**
 * 
 * 根据ID删除要素
 * */
function removeFeature(id,type){
	var layer = layerIsExist('geographicEditLayer',map);
	if(layer){
		var feature = layer.getSource().getFeatureById(id);
		layer.getSource().removeFeature(feature);
		close();
	}
	if(type=='road'){
		var urls = baseUrl+"/api/geographicEdit/remove.jhtml";
	    var params = id;
	    executeAjax(urls,params,function(data){
			if(data.httpCode==200){
				getRoadList(false);
	    	}else {
	    		layer.msg(data.msg, {time: 1000});
	    	}
	    });
	}
}
/**
 * 清空图层
 * */
function clearLayer(){
	clearLayerByIds(['plotLayer']);
}

function remove(){
	var feature = plot.plotEdit.getFeature();
	var layer = layerIsExist('plotLayer',map);
	if(layer){
		layer.getSource().removeFeature(feature);
	}
	plot.plotEdit.deactivate();
}

/**
 * 显示场景搭建历史
 * */
function addImagesToMap(images){
	var group = layerIsExist('imagesGroup',map);
	if(!group){
		group = createGroupLayer('imagesGroup');
		map.addLayer(group);
	}
	for(var i=0;i<images.length;i++){
		var image = images[i];
		var layer = new ol.layer.Image({
			source: new ol.source.ImageStatic({
				url:baseFileUrl+image.url,
				crossOrigin: '',
				projection: 'EPSG:3857',
	            imageExtent: image.extent.split(",")
			})
		});
		if(image.id){
			layer.set('id',image.id);
			removeImagesByid(image.id);
		}
		group.getLayers().push(layer);
	}
}

/**
 * 删除场景搭建历史
 * */
function removeImagesByid(id){
	var layer;
	var group = layerIsExist('imagesGroup',map);
	group.getLayers().forEach(function(lay){
		if(lay.get('id')==id){
			layer = lay;
		}
	});
	if(layer){
		group.getLayers().remove(layer);
	}
}

function clearImagesGroup(){
	var group = layerIsExist('imagesGroup',map);
	if(group){
		map.removeLayer(group);
	}
}

/**
* 保存操作记录的公共方法
* @param params t_incident_record 对象 操作人director 类型type 房间号roomId 子动作action 动作描述actionDesc 动作内容content
* @param callback 回调参数
* */
function saveRecord(params,callback){
	doCallback = function (fn, args) {
		return fn.apply(this, args);
	}
	if(!params.type){
		params.type=params.recordType;
	}
	var url = baseUrl+'/webApi/insertIncidentRecord.jhtml';
	$.ajax({
		type:'POST',
		url: url,
		headers: {"Content-Type": "application/json;charset=utf-8"},
		data: JSON.stringify(params),
		dataType:'JSON',
		success: function(data){
			if(data){
				if(callback){
				  doCallback(callback,[data]);
				}
			}
		}
	});
}
function updateRecord(params,callback){
	doCallback = function (fn, args) {
		return fn.apply(this, args);
	}
	params.type=params.recordType;
	var url = baseUrl+'/webApi/updateIncidentRecord.jhtml';
	$.ajax({
		type:'POST',
		url: url,
		headers: {"Content-Type": "application/json;charset=utf-8"},
		data: JSON.stringify(params),
		dataType:'JSON',
		success: function(data){
			if(data){
				if(callback){
					doCallback(callback,[data]);
				}
			}
		}
	});
}

/**
 * svg转png
 * */
function getVectorImage(src, width, height) {
	var canvas = document.createElement('canvas');
	canvas.setAttribute("width", width);
	canvas.setAttribute("height", height);
	var ctx = canvas.getContext("2d");
	var img = new Image();
	img.onload = function() {
		ctx.drawImage(img, 0, 0, width, height);
	}
	img.src = src;
	return canvas;
}


/**
 * 定位功能
 * */
function centerAt(id){
	var layer = layerIsExist('geographicEditLayer',map);
	if(layer){
		if(beforeFeature){
			var bf = layer.getSource().getFeatureById(beforeFeature.id);
			bf.set('scolor',beforeFeature.scolor);
		}
		var feature = layer.getSource().getFeatureById(id);
		if(feature){
			var point;
			var geometry = feature.getGeometry();
			var type = geometry.getType();
			if(type=="Point"){
				point = geometry.getFirstCoordinate();
			}else if(type=="LineString"){
				beforeFeature = null;
				beforeFeature = {};
				beforeFeature.id = feature.getId();
				beforeFeature.scolor = feature.get('scolor');
				feature.set('scolor','#00ff00');
				point = geometry.getFirstCoordinate();
			}else if(type=="Polygon"){
				point = geometry.getInteriorPoint();
			}
			centerAndZoom(point,map.getView().getZoom());
		}
	}
}

/**
 * 定位
 * */
function centerAndZoom(point,zoom){
	if(zoom){
		map.getView().animate({center: point}, {zoom: zoom});
	}else{
		map.getView().animate({center: point});
	}
}

//生成随机数
function createRandomId() {
    return (Math.random()*10000000).toString(16).substr(0,4)+'-'+(new Date()).getTime()+'-'+Math.random().toString().substr(2,5);
}