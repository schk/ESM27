//todo 修改获取方式
var currentUserName = "ceshi";
var currentUserId ="878827689403076608";
var currentRoomId= "1146317892350750722";
var currentRoomName = "4689";
var accidentMsg={};
accidentMsg.yqm=currentRoomName;
accidentMsg.zhr="王某";
accidentMsg.sfd="王某";

var isCollect = true;
var socket;

var stompClient;
//传感器，气象张缓存数据
var rtData = new Array();
//初始化数据
rtData.push({name:'#0_FS#',value:0});
rtData.push({name:'#0_FX#',value:0});
rtData.push({name:'#0_PM#',value:0});
rtData.push({name:'#0_WD#',value:0});
rtData.push({name:'#0_QY#',value:0});
rtData.push({name:'#0_SD#',value:0});
rtData.push({name:'#0_ZX#',value:0});


$(document).ready(function(){
	initRomomInfo();
	initHistory();
	initQryGasDataList();
});

//打开web界面后初始化参数
function setParams(params){
	currentUserName = params.account;
	currentUserId =params.userId;
	currentRoomName = params.roomCode;
	currentRoomId = params.roomId;
	accidentMsg.zhr = params.accidentConductor;
	accidentMsg.sfd = params.accidentLocation;
	accidentMsg.yqm = params.roomCode;
	
	initRomomInfo();
}

connect();
/**
 * 连接socket
 */

function connect(){
	
	socket = new SockJS(baseUrl+'/room');
	stompClient = Stomp.over(socket);
	stompClient.connect({}, function (frame) {
		if(currentRoomName != null && currentRoomName != ""){
			haveRoomEvent(currentRoomName);
			if(isCollect == true){
				subCollect = stompClient.subscribe('/topic/collect/' + currentRoomName, function (mes) {
					 handleData(mes);
					 
				});
			}
		}else{
			if(isCollect == true){
				subCollect = stompClient.subscribe('/topic/collect/' + currentUserId, function (mes) {
					 handleData(mes);
				});
			}
		}
	},errorCallback);  	
}

/**
 * 断线回调
 */
var errorCallback = function(error){
//	console.log("断线");
	connect();
}


// 测试
//$(document).ready(function(){
//	addDeviceToMap('1234',104.094752,30.667451,{"deviceAddress":"1234","realData":[{"gasName":"氢气","gasValue":1}]});
//});
/**
 * 添加设备到地图
 * @param lat
 * @param lng
 * @returns
 */
function addDeviceToMap(deviceCode,lat,lng,obj){
	var coordinate = ol.proj.transform([lat,lng], 'EPSG:4326', 'EPSG:3857');
	var params= {
		"type": "Feature",
		"id": "model-"+deviceCode,
		"geometry": {
			"type": "Point",
			"coordinates": [coordinate[0],coordinate[1]]
		},
		"properties": {
			"type": "Point",
			"points": [[coordinate[0],coordinate[1]]],
			"params": {
				"type": "marker",
				"oid": deviceCode,
				"name": deviceCode,
				"recordType": "mobileesm27",
				"clusterType": "esm27q",
				"catecd": "40",
				"imagePath": "/mapIcon/mapFixedIcon/qtIcon.png",
				"isCluster": false,
				"clusterImagePath": "/mapIcon/iconCluster/qtIcon_c.png",
				"isSyn": true,
				"isSave": true,
				"iconType": "mobileesm27",
				"action": "标注123",
				"isExist": true,
				"dataid": deviceCode
			},
			"name": obj,
			"plotType": "Marker"
		}
	}
	positionSynMark(params);
}

/**
 * 实时数据处理
 */
function handleData(mes){
	var returnData = JSON.parse(mes.body);
	if (returnData.returnCode =="realTimeData") {
		//returnData.deviceAddress(设备号)
		//returnData.latitude(纬度)
		//returnData.longitude(精度)
		addDeviceToMap(returnData.deviceAddress,returnData.latitude, returnData.longitude,returnData);
		var realData = returnData.realData;
		var alertNum = 0;
		for (var i = 0; i < realData.length; i++) {
			if (realData[i].gasIsAlert == 2) {
				alertNum++;
				$("#li_"+realData[i].id_).css('background','#ff0000');
			}else{
				$("#gas_"+realData[i].id_).parent('li').removeClass("yellow");
				$("#li_"+realData[i].id_).css('background','none');
			}
			$("#gas_"+realData[i].id_).text(realData[i].gasValue+" "+realData[i].gasUnit);
//			isBH = false;
//			$.each(rtData,function(index,ele){
//				if(ele.name == '#' + returnData.deviceAddress + '_' + realData[i].id_ + '#'){
//					rtData.splice(index,1,{name:'#' + returnData.deviceAddress + '_' + realData[i].id_ + '#',value:realData[i].gasValue});
//					isBH = true;
//					return false;
//				}
//			});
//			if(!isBH)
//				rtData.push({name:'#' + returnData.deviceAddress + '_' + realData[i].id + '#',value:realData[i].gasValue});
		}
		
		if(alertNum>0){
			$("#deviceSpan_"+returnData.deviceAddress).css('background','#ff0000');
			$('#reamTimeMsgBtn').attr('alarm','true');
		}else{
			$("#deviceSpan_"+returnData.deviceAddress).css('background','#1fc17b');
			$('#reamTimeMsgBtn').attr('alarm','false');
		}
		
		//$("#alertListUl").find("li").remove(); 
		$("#lastUpdateTime").text("最后更新时间    "+returnData.lastUpdateTime);
		if ( JSON.stringify(returnData.alertRealData) !== '[]' && returnData.alertRealData.length>0) {
			var alertData = returnData.alertRealData;
			var html="";
			for (var i = 0; i < alertData.length; i++) {
				html+='<li>';
				html+='<div class="bignumber">'+alertData[i].gasValue+'</div>';
				html+='<div class="device">';
				html+='<p class="deviceNanme">'+alertData[i].gasName+'</p>';
				html+='<p>'+returnData.deviceAddress?returnData.deviceAddress:""+'</p>';
				html+='</div>';
				html+='</li>';
			}
			$("#alertListUl>li:gt(2)").remove(); 
			$("#alertListUl").prepend(html);
			$("#alarm").show;
		}
		
		updateExprResult();
		
	 } 
	 if (returnData.returnCode =="serialPortData") {
		 var returnData = JSON.parse(mes.body);
		 var jsondata = returnData.returData;
		 var data = JSON.parse(jsondata);
		 var html="";
		 for (var i = 0; i < data.length; i++) {
			 html+='<li>'+data[i]+'</li>'
		 }
		 $("#allPortUl").append(html);
	 }
	 if (returnData.returnCode =="weatherData") {
		 var returnData = JSON.parse(mes.body);
		 var data = returnData.wsd;
		 data.humidity = parseFloat(data.humidity).toFixed(1)
		 weatherData = data;
		 $("#realTimeWindSpeed").text(data.windSpeed+"m/s");
		 $("#realTimeWindDirection").text(data.windDirection);
		 $("#realTimePmValue").text(data.pmValue);
		 $("#realTimeTemperature").text(parseFloat(data.temperature).toFixed(1)+"℃");
		 $("#realTimeHumidity").text(data.humidity+"%");
		 if(data.pressure<1){
			 data.pressure = 100.00;
		 }
		 $("#realTimePressure").text(data.pressure+"Pa");
		 $("#realTimeNoise").text(data.noise);
		 $('#WindVaneId').css('transform','rotate('+data.windDirectionValue+'deg)'); 
		 weatherData.windSpeed = data.windSpeed;
		 weatherData.windDirection = parseFloat(data.windDirectionValue).toFixed(1);
		 weatherData.temperature = parseFloat(data.temperature).toFixed(1);
		 weatherData.humidity = parseFloat(data.humidity).toFixed(1);
		 weatherData.pressure = parseFloat(data.pressure).toFixed(1);
		 $.each(rtData,function(index,ele){
			if(ele.name == '#0_FS#'){
				rtData.splice(index,1,{name:'#0_FS#',value:data.windSpeed});
			}
			if(ele.name == '#0_FX#'){
				rtData.splice(index,1,{name:'#0_FX#',value:data.windDirectionValue});
			}
			if(ele.name == '#0_PM#'){
				rtData.splice(index,1,{name:'#0_PM#',value:data.pmValue});
			}
			if(ele.name == '#0_WD#'){
				rtData.splice(index,1,{name:'#0_WD#',value:data.temperature});
			}
			if(ele.name == '#0_QY#'){
				rtData.splice(index,1,{name:'#0_QY#',value:data.pressure});
			}
			if(ele.name == '#0_SD#'){
				rtData.splice(index,1,{name:'#0_SD#',value:data.humidity});
			}
			if(ele.name == '#0_ZX#'){
				rtData.splice(index,1,{name:'#0_ZX#',value:data.noise});
			}
		 });	
		 updateExprResult();
	 }
	 if(returnData.returnCode =="gaussionModelParametersSyn"){
		 //todo 刘小蕾
		 var returnData = JSON.parse(mes.body);
	 }
	 if (returnData.returnCode =="AutoAlarm") {
			//returnData.deviceAddress(设备号)
				//returnData.latitude(纬度)
				//returnData.longitude(精度)
				var realData = returnData.realData;
				var address = returnData.deviceAddress;
				var alertNum = 0;
				for (var i = 0; i < realData.length; i++) {
					if (realData[i].gasIsAlert == 2) {
						alertNum++;
						$("#li_"+realData[i].id_).css('background','#ff0000');
					}else{
						$("#gas_"+realData[i].id_).parent('li').removeClass("yellow");
						$("#li_"+realData[i].id_).css('background','none');
					}
//					$("#gas_"+realData[i].id_).text(realData[i].gasValue+realData[i].gasUnit);
//					isBH = false;
//					$.each(rtData,function(index,ele){
//						if(ele.name == '#' + returnData.deviceAddress + '_' + realData[i].id_ + '#'){
//							rtData.splice(index,1,{name:'#' + returnData.deviceAddress + '_' + realData[i].id_ + '#',value:realData[i].gasValue});
//							isBH = true;
//							return false;
//						}
//					});
//					if(!isBH)
//						rtData.push({name:'#' + returnData.deviceAddress + '_' + realData[i].id_ + '#',value:realData[i].gasValue});
				}
				if(alertNum>0){
					$("#deviceSpan_"+address).css('background','#ff0000');
					$('#reamTimeMsgBtn').attr('alarm','true');
				}else{
					$("#deviceSpan_"+address).css('background','#1fc17b');
					$('#reamTimeMsgBtn').attr('alarm','false');
				}
				//$("#alertListUl").find("li").remove(); 
				$("#lastUpdateTime").text("最后更新时间    "+returnData.lastUpdateTime);
				if ( JSON.stringify(returnData.alertRealData) !== '[]' && returnData.alertRealData.length>0) {
					var alertData = returnData.alertRealData;
					var html="";
					for (var i = 0; i < alertData.length; i++) {
						html+='<li>';
						html+='<div class="device">';
						html+='<p class="deviceNanme">'+alertData[i].gasName+'</p>';
						html+='<p>'+returnData.deviceAddress?returnData.deviceAddress:""+'</p>';
						html+='</div>';
						html+='</li>';
					}
					$("#alertListUl>li:gt(2)").remove(); 
					$("#alertListUl").prepend(html);

					$("#starsDiv").css('right','70px');
					$("#alarm").show();
				}
			
			updateExprResult();
			
		 } 
}

//已有的房间所有操作 同步回显
function haveRoomEvent(accRoom){
	$("#talkAccident").val(currentRoomName+"房间命令");
	//如果等于空 重新注册
	
	//增加延迟， 避免正在断开注册失败
	setTimeout(function(){
	//创建房间编号并同步
//		subRoom = stompClient.subscribe('/topic/accident' + accRoom, function (mes) {
//		 var result = JSON.parse(mes.body);
//		 if(result.isdelete){
//			 console.log('删除'+JSON.stringify(result))
//			 synMarkerRemove(result);
//			 if(result.datatype==="6"){
//				 deleteAccidentFeatureByid(result.id);
//			 }
//			 if(result.datatype==="8"){
//				 $._IconBox.removedisableli(result.dataid);
//			 }
//		 }
//		 //推送修改指挥权
//		 else if(result.evetype && result.evetype=="changeLeaderName"){
//			$("#identConductor").text(result.val);
//			$("#changeLeadersDiv .Operator i").text(result.val);
//			layer.msg('修改成功', {icon: 1, time: 1000});
//			cancleChangeLeaderDiv();
//		 }
//		//推送场景搭建，地图展示场景图片
//		 else if(result.evetype && result.evetype=="changeSceneResource"){
//			var addtomap=[];
//			var obj = result.val;
//			obj.forEach(function(val){
//				addtomap.push({
//					url : val.url,
//					extent : val.extent,
//					id : val.imageId,
//					imageId : val.imageId
//				});
//			})
//			
//			addImagesToMap(addtomap);
//			
//		 //推送关闭事故，弹出事故报告 结束事故
//	 	}else if(result.evetype && $._Accident[result.evetype]){
//			$._Accident[result.evetype](result.val);
//		 }else if(result.evetype && $._IconBox[result.evetype]){
//			$._IconBox[result.evetype](result.val);
//		 }else if(result.evetype && result.evetype=="zhmn"){
//		// 事故模拟分析
//			 var zhmnObj = result.val;
//			 var zhmnType = zhmnObj.accType;
//			 var objData = zhmnObj.content;
//			 //zhmnFlag为fasle是被同步者，最小化展示
//			 if(!zhmnFlag){
//				 if(zhmnType=="zhmn_rfsfx"){
//					 //热辐射分析
//					 $("#zhmnQxzsOutDiv").show();
//					 var mathGranularDLsit = objData.mathGranularDLsit;
//					 zhmn_rsfxRbl(mathGranularDLsit[0],mathGranularDLsit[1],mathGranularDLsit[2]);
//				 }else if(zhmnType=="zhmn_pjsj"){
//					 //喷溅时间
//					 $("#zhmnZZYPPJDiv").show();
//					 startTimeInterval(objData*60*60);
//					 $("#jieguo").val("预计时间："+shortTime);
//				 }else if(zhmnType=="zhmn_rsfx"){
//					 //燃烧分析
//					 $("#zhmnRSWFXDivOut").show();
//					 var co2Rate = objData.co2Rate;
//					 var coRate = objData.coRate;
//					 var no2Rate = objData.no2Rate;
//					 var noRate = objData.noRate;
//					 var so2Rate = objData.so2Rate;
//					 rswfxTubiaoChartShow2(coRate,co2Rate,no2Rate,noRate,so2Rate);
//				 }
//			 }
//		 }
//		 else{
//			 positionSynMark(result);
//		 }
//	  });
		subRoom = stompClient.subscribe('/topic/accident' + accRoom, function (mes) {
			 var result = JSON.parse(mes.body);
			 if(result.isdelete){
				 synMarkerRemove(result);
				 if(result.datatype==="6"){
					 deleteAccidentFeatureByid(result.id);
				 }
				 if(result.datatype==="8"){
					 $._IconBox.removedisableli(result.dataid);
				 }
			 }
			 
			 
			 //推送修改指挥权
			 else if(result.evetype && result.evetype=="changeLeaderName"){
				if(typeof result.val == "undefined" || result.val == null || result.val == ""){
					$layer.msg('指挥权名称不能为空。', {icon: 2, time: 2000});
				}else{
					$("#identConductor").text(result.val);
					$("#changeLeadersDiv .Operator i").text(result.val);
					$layer.msg('修改成功', {icon: 1, time: 1000});
					cancleChangeLeaderDiv();
				}
			 }
			//推送场景搭建，地图展示场景图片
			 else if(result.evetype && result.evetype=="changeSceneResource"){
				var addtomap=[];
				var obj = result.val;
				obj.forEach(function(val){
					addtomap.push({
						url : val.url,
						extent : val.extent,
						id : val.imageId,
						imageId : val.imageId
					});
				})
				
				addImagesToMap(addtomap);
		 	}else if(result.evetype && result.evetype=="deleteSceneResource"){
				var addtomap=[];
				var id = result.id;
				removeImagesByid(id);
		 	}
			 else if(result.evetype && $._Accident[result.evetype]){ //推送关闭事故，弹出事故报告 结束事故
				$._Accident[result.evetype](result.val);
			 }else if(result.evetype && $._IconBox[result.evetype]){
				$._IconBox[result.evetype](result.val);
			 }else if(result.evetype && result.evetype=="zhmn"){
			// 事故模拟分析
				 var zhmnObj = result.val;
				 var zhmnType = zhmnObj.accType;
				 var objData = zhmnObj.content;
				
				 //zhmnFlag为fasle是被同步者，最小化展示
					 if(zhmnType=="zhmn_rfsfx"){
//						 $("#zhmnQxzsOutDiv").show();
						 cleanKuoSanDrawLayer();
						 //热辐射分析
						 if(qxzsIsopen){
							 $("#zhmnQxzsOutDiv").show();
						 }
						 var mathGranularDLsit = objData.mathGranularDLsit;
						 zhmn_rsfxRbl(mathGranularDLsit[0],mathGranularDLsit[1],mathGranularDLsit[2]);
					 }else if(zhmnType=="zhmn_rfsfhf"){
						 //防辐射服计算
//						 $("#zhmnQxzsOutDiv").show();
						 var point = objData.point;
						var items=[{distance:objData.dieRadius,color:'#FF0000'}];
						var rotation = 0;
						if($("#zhmnQxzs_FHFjsShowR").prop('checked')){
							drawBZ('cgybz',items,point,1,true,objData.rotation);
						}
					 }else if(zhmnType=="zhmn_pjsj"){
//						 $("#zhmnZZYPPJDiv").show();
//						 timeIntervalEnd();
						 //喷溅时间
						 if(zzyppjIsopen){
							 $("#zhmnZZYPPJDiv").show();
						 }
						 startTimeInterval(objData);
//						 $("#jieguo").val("预计时间："+shortTime);
					 }else if(zhmnType=="zhmn_rsfx"){
//						 $("#zhmnRSWFXDivOut").show();
						 //燃烧分析
						 if(rsxfxIsopen){
							 $("#zhmnRSWFXDivOut").show();
						 }
						 var co2Rate = objData.co2Rate;
						 var coRate = objData.coRate;
						 var no2Rate = objData.no2Rate;
						 var noRate = objData.noRate;
						 var so2Rate = objData.so2Rate;
						 rswfxTubiaoChartShow2(coRate,co2Rate,no2Rate,noRate,so2Rate);
					 }else if(zhmnType=="zhmn_qzks"){
//						 $("#zhmnKSSJDivOutSyn").show();
						 //扩散
						 var layer = layerIsExist('buildGeometryLayer',global.map);
						 if(layer){
							 clearBuildGeometryLayer();
						 }
						 buildGeometryByCoords(objData.coords,objData.isBorder,objData.borderColor,objData.fillColor,objData.fillTransparency);
						 drawHighLine('drawHighLine',objData.point);
					 }else if(zhmnType=="zhmn_qzksTime"){
//						 $("#zhmnHZMYFXDivOut").show();
						 //扩散
						 var layer = layerIsExist('buildGeometryLayer',global.map);
							if(layer){
								clearBuildGeometryLayer();
							}
						 $("#zhmnKSSJDivUl li").removeClass();
						 $("#zhmnKSSJDivUlSyn li").removeClass();
						 $("#"+objData.liId).addClass("currentTime");
						 var layer = layerIsExist('buildGeometryLayer',global.map);
						 if(layer){
							clearBuildGeometryLayer();
						 }
						 buildGeometryByCoords(objData.coords,objData.isBorder,objData.borderColor,objData.fillColor,objData.fillTransparency);
					 }else if(zhmnType=="zhmn_zzks"){
//						 $("#zhmnHZMYFXDivOut").show();
						 var result = objData;
						 var format = new ol.format.GeoJSON();
						 var features = format.readFeatures(result);
						 var layer = layerIsExist('spreadLayer',global.map);
						 if(!layer){
							 layer = createSpreadLayer('spreadLayer');
							 global.map.addLayer(layer);
						 }else{
							 layer.getSource().clear();
						 }
						 layer.getSource().addFeatures(features);
					 }else if(zhmnType=="isShwo"){
						 hidePanl();
						 if(objData==0){
							 $("#zhmnRSWFXDivOut").show();
							 $("#zhmnZZYPPJDiv").show();
							 $("#zhmnZZYPPJDiv").show();
						 }else if(objData==1){
							 $("#zhmnRSWFXDivOut").show();
							 $("#zhmnZZYPPJDiv").show();
							 $("#zhmnZZYPPJDiv").hide();
							 timeIntervalEnd();
						 }else if(objData==2){
							 $("#zhmnKSSJDivOut").show();
							 timeIntervalEnd();
						 }else if(objData==3){
							 $("#damagePeopleDiv").show();
							 timeIntervalEnd();
						 }
					 }
			 }
			 else{
				 if(result.properties.params.action==="事故点"){ // 如果传过来事故点的信息，就记录变量
					 zhmnAccidentPoint=result.geometry.coordinates;
				 }

				 positionSynMark(result);
			 }
		  });
	 //接到聊天数据
		subChat = stompClient.subscribe('/topic/chat/' + accRoom, function (mes) {	
		 var obj = eval('(' + mes.body + ')');
		 //显示在MINI聊天界面
		 if($("#chatPanel").css("display") == "none"){
			 $(".chatWarpCont").html('<span>'+ obj.user +'：</span>' + obj.val);
			 $("#chatMini").show();
			 
			 if(chatTimer){
				 clearTimeout(chatTimer);
				 chatTimer = null;
				 chatTimer = setTimeout(function(){
					 $("#chatMini").hide();
				 },3000);
			 }else{
				 chatTimer = setTimeout(function(){
					 $("#chatMini").hide();
				 },3000);
			 }
			
			 var html = "<li><div class='layim-chat-user'><cite style='left:0px'>" + obj.user + "<i>" + obj.createTime + "</i></cite></div><div class='layim-chat-text'>" + obj.val + "</div></li>"
			 $("#chatList").append(html);
		 }else{
			//显示正常聊天界面
			 var obj = eval('(' + mes.body + ')');
			 var html = "<li><div class='layim-chat-user'><cite style='left:0px'>" + obj.user + "<i>" + obj.createTime + "</i></cite></div><div class='layim-chat-text'>" + obj.val + "</div></li>"
			 $("#chatList").append(html);
		 }				
		 $("#chatListDiv").scrollTop($("#chatListDiv").prop('scrollHeight'));
		});
		
	},500);
}

/**
 * 加入房间后绘制历史中的绘标
 */
function initHistory(){
		$.ajax({
			type : "post",
			url : baseUrl+"/api/mobile/getIncidentRecordByAccident.jhtml",
			data: JSON.stringify(currentRoomId),
			dataType:"json",
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {
				//返回的数据中 有删除和新建的，如果新建后删除了就不调用绘制方法
	
				//需要绘制的类型
				var newjson =[2,4,6,8,9];
				//得到删除的ID
				var contents = data.data.map(function(m){
					if(typeof m.isDelete != 'undefined' && m.isDelete==0)
					{
						var content = eval("("+m.content+")");
						return content.id;
					}else{return '0';}
				});
				
				for(j = 0,len= data.data.length; j < len; j++){
					var jsoncontent =  data.data[j].content;
					var content = eval("("+jsoncontent+")");
					//如果需要绘制 并且不是删除的ID
					if(newjson.indexOf(data.data[j].type)>=0 && contents.indexOf(content.id)<0){
						positionSynMark(content);
//						if(data.data[j].type == 8){
//							//如果是消防车 记录已经扎上的消防车ID
//							if($._IconBox && $._IconBox.makerFireEngineoid){
//								$._IconBox.makerFireEngineoid.push(content.id);
//							}else{
//								$._IconBox.makerFireEngineoid=[];
//							}
//						}else if(data.data[j].type == 6){
//							//事故点位置赋值
//							zhmnAccidentPoint=content.properties.points[0];
//						}else if(data.data[j].type == 12){
//							//事故点位置赋值 todo刘小蕾
////							zhmnAccidentPoint=content.properties.points[0];
//						}
					}
				}
			}
		});
}

//开启聊天界面
function openChat(){
	$("#chatMini").hide();
	$("#chatPanel").show();
}

//关闭聊天界面
function closeChatPanel(){
	$("#chatPanel").hide();
}

//发送聊天信息
function sendChat(){
	if($("#chatCon").val()){
		var json = {user:currentUserName,val:$("#chatCon").val(),type:"mingling"}
		stompClient.send("/team/chat/" + currentRoomName,{},JSON.stringify(json));
		var params = {};
		params.id =getTimestamp();
		params.roomId = currentRoomId;
		params.action = "发布命令";
		params.actionDesc=$("#chatCon").val();
		params.content = JSON.stringify({"command": $("#chatCon").val()});
		params.type = 7;
		params.isDelete = 1;//删除0，添加1
		params.createBy = currentUserId; //修改等路人 todo
		params.director = currentUserName; //修改等路人 todo
		saveRecord(params);
		$("#chatCon").val('');
	}
}

/**  
/**
* 保存操作记录的公共方法
* @param params t_incident_record 对象 操作人director 类型type 房间号roomId 子动作action 动作描述actionDesc 动作内容content
* @param callback 回调参数
* */
function saveRecord(params,callback){
	doCallback = function (fn, args) {
		return fn.apply(this, args);
	}
	if(!params.type){
		params.type=params.recordType;
	}
	var url = baseUrl+'/api/mobile/insertIncidentRecordMobile.jhtml';
	$.ajax({
		type:'POST',
		url: url,
		headers: {"Content-Type": "application/json;charset=utf-8"},
		data: JSON.stringify(params),
		dataType:'JSON',
		success: function(data){
			if(data){
				if(callback){
				  doCallback(callback,[data]);
				}
			}
		}
	});
}

//打开关闭事故信息面板
function accidentMsgDivShow(){
	if($("#accidentMsgDiv").is(":hidden")){
        $("#accidentMsgDiv").show();
    }else{
        $("#accidentMsgDiv").hide();
    }
}
//事故房间信息出事故
function initRomomInfo(){
	$("#accidentYQM").html(accidentMsg.yqm);
	$("#accidentZHR").html(accidentMsg.zhr);
	$("#accidentSFD").html(accidentMsg.sfd);
	
	if(!currentRoomId){
		return;
	}
	
	$.ajax({
		  type : "post",
		  url : baseUrl+"/api/mobile/getRoom.jhtml",
		  data: currentRoomId,
		  dataType:"json",
		  headers: {"Content-Type": "application/json;charset=utf-8"},
		  success : function(data) {
			  //如果房间存在
			  if(data.httpCode == '200'){
				  	while(data.data.accidentLocation && typeof(data.data.accidentLocation)=="string"){
						data.data.accidentLocation = eval('('+data.data.accidentLocation+')');
					}
				  	var add =data.data.accidentLocation;
				  	setTimeout(function(){
					  	var point = ol.proj.transform([add.lon,add.lat], 'EPSG:4326', 'EPSG:3857');
						gotoPointView(point[0],point[1],14);
				  	},1000);
			  }else{
				  layer.msg("未找到房间", {time: 1000});
			  }
		  },error: function(request) {
			  layer.msg(networkErrorMsg, {time: 2000});
		  }
	  })
}

//显示实时数据信息界面
function showReamTimeMsg(){
	if($("#reamTimeMsgDiv").is(":hidden")){
		$("#reamTimeMsgBtn").attr("class","sidebar left");
        $("#reamTimeMsgDiv").show();
    }else{
    	$("#reamTimeMsgBtn").attr("class","sidebar sidebarRetract");
        $("#reamTimeMsgDiv").hide();
    }
}

//初始化气体设备数据
function initQryGasDataList(){
	$.ajax({
 		type : "post",
 		url : baseUrl+"/api/mobile/qryGasData.jhtml",
 		data: null,
 		dataType:"json",
 		headers: {"Content-Type": "application/json;charset=utf-8"},
 		success : function(data) {	
 			if(data.httpCode==200){
 				$('#reamTimeMsgDivList').html('');
 				$.each(data.data,function(index,item){
 					var html = '<div class="sideContent"><div class="flex sideHd"><div class="equal"><div class="flex"><span id="deviceSpan_'+item.deviceSerial+'" class="spot"></span>';
 					html += '<span class="lamp"></span>设备编号：<span>'+ item.deviceSerial +'</span></div>';
 					html += '</div><div class="arrowDown arrowUp"></div></div>';
 					html += '<div class="flex borderTop"><div class="equal explain">';
 					html += '<ul>';
 					for(var i=0;i<4;i++){
 						html += '<li class="flex" id="li_'+item.gasDetectorDataVo[i].id_+'"><span class="equal">'+ item.gasDetectorDataVo[i].gasName +'</span><span class="equal number" id="gas_'+ item.gasDetectorDataVo[i].id_ +'"></span></li>';
 					}
 					
 					html += '</ul>';
 						html += '</div>';
					html += '<div class="equal explain">';
 					html += '<ul>';
 					for(var i=4;i<7;i++){
 						html += '<li class="flex" id="li_'+item.gasDetectorDataVo[i].id_+'"><span class="equal">'+ item.gasDetectorDataVo[i].gasName +'</span><span class="equal number" id="gas_'+ item.gasDetectorDataVo[i].id_ +'"></span></li>';
 					}
 					
 					html += '</ul>';
 						html += '</div>';
 					html += '</div>';
 					html += '</div>';
 					//html += '<div class="last" id="lastUpdateTime"></div>';
 					$('#reamTimeMsgDivList').append(html);
 				});
 				$('#reamTimeMsgDivList').append('<div class="last" id="lastUpdateTime"></div>');
 			}
 			
 		},error: function(request) {
 			alert("网络错误");
 	     }
 	})
}
function updateExprResult(){
	
}
function showHistoryMobile(){
	$("#realtimeDataLi").removeClass("onlike");
	$("#hitoryDataLi").addClass("onlike");
	$("#target").removeClass("oldClass");
	$("#gasHistoryDataDivId").show();
	$("#reamTimeMsgDivList").hide();
	getHistoryGasDataFn();
}

function showRealTimeMobile(){
	$("#hitoryDataLi").removeClass("onlike");
	$("#realtimeDataLi").addClass("onlike");
	$("#gasHistoryDataDivId").hide();
	$("#reamTimeMsgDivList").show();
}

/*获取历史气体数据的信息*/
function getHistoryGasDataFn(){
	var startTime = new Date();
	startTime.setTime(startTime.getTime()-1000*60*60*24+1000*60);
	var endTime = new Date();
	var timeResult = cheackDate(startTime,endTime);/*0成功，1开始时间不能为空，2结束时间不能为空，3开始时间不能大于结束时间，4时间间隔不能超过5天*/
	if (timeResult == 1) {
		layer.msg("开始时间不能为空！");
	}else if(timeResult == 2){
		layer.msg("结束时间不能为空！");
	}else if(timeResult == 3){
		layer.msg("开始时间不能大于结束时间！");
	}else if(timeResult == 4){
		layer.msg("时间间隔不能超过24小时！");
	}else{
		var params = {};
	 	params.startTime=startTime;
	 	params.endTime=endTime;
//	 	params.deviceSerial=dNo;
	 	params.pageNum = 1;
	 	params.pageSize = 100;
	 	$.ajax({
	 		type : "post",
	 		url : baseUrl+"/webApi/realTimeData/queryHistoryGasData.jhtml",
	 		data: JSON.stringify(params),
	 		dataType:"json",
	 		headers: {"Content-Type": "application/json;charset=utf-8"},
	 		success : function(data) {	
	 			if(data.httpCode==200){
	 				if(data.data){
	 					var html1 = "";
	 					var html2 = "";
	 					var currentPage = Number(data.current);
						var totalPages = Number(data.pages);
//						if(currentPage>=1 && totalPages>1){
//							generatePagination(currentPage,totalPages,"historyGasDataPage");
//						}
	 					var list = data.data;
	 					if (list.length >0) {	 						
	 						var tableTrList = $('#historyGasDataTableTr2 > td');
	 						var tableTrIdList = [];
	 						for (var i = 0; i < tableTrList.length; i++) {
	 							tableTrIdList.push(tableTrList[i].id);
							}
							for (var i = 0; i < list.length; i++) {
								 var timeString = list[i].timeString.substring(0,list[i].timeString.length-2);
								 html1+='<tr>';
								 html1+='<td>'+timeString+'</td>';
								 html1+='<td>'+list[i].serial+'</td>';
								 html1+='</tr>';
								 html2+='<tr>';
								 var gasData = list[i].ghdList;
								 for (var j = 0; j < tableTrIdList.length; j++) {
									 var isHave = 0;
									 for (var z = 0; z < gasData.length; z++) {
										 var gasTitleValue = "gasTitle_"+gasData[z].gasId;
										 if (gasTitleValue == tableTrIdList[j]) {
											isHave = 1;
											html2+='<td>'+gasData[z].value+'</td>';
										 }
									 }
									 if (isHave==0) {
										 html2+='<td>&nbsp</td>';
									}
								 }
								 html2+='</tr>';
							}
						}
	 					$("#historyGasDataTableTr1").siblings().remove();
	 					$("#historyGasDataTableTr2").siblings().remove();
//	 					alert(html1);
//	 					alert(html2);
	 					$("#historyGasDataTable1").append(html1);
	 					$("#historyGasDataTable2").append(html2);
	 				}
	 			}
	 			
	 		},error: function(request) {
	 			alert("网络错误");
	 	     }
	 	})
	}
};

function cheackDate(startTime,endTime){/*0成功，1开始时间不能为空，2结束时间不能为空，3开始时间不能大于结束时间，4时间间隔不能超过5天*/
	var result = 0;
	if (startTime=="") {
		result = 1;
		return result;
	}
	if (endTime=="") {
		result = 2;
		return result;
	}
	var sTime = new Date(startTime).getTime();
	var eTime = new Date(endTime).getTime();
	if (sTime>eTime) {
		result = 3;
		return result;
	}
	if ((sTime+24*60*60*1000)<eTime) {
		result = 4;
		return result;
	}
	return result;
};