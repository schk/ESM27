 /*
  * #定义IP地址变量,打包部署修改
  */
  //服务器IP地址
 	//var global_serverIp = "39.152.50.253";
 	var global_serverIp = window.location.hostname;
	// var fileUrl = [[${fileUrl}]];
	// var ip = [[${ip}]];
	//流媒体服务器
	 var streamserver = global_serverIp+":1935";
	
	 //离线地图Url
	 var offLineMapUrl = "http://"+global_serverIp+":9092";
	 //气体扩散
	 var gasUrl= "http://"+global_serverIp+":9018/";
	 //图标的URL、静态文件URL
     var baseFileUrl =  "http://"+global_serverIp+":9092"; 
	 // "http://127.0.0.1:9092";
	 
  // 地图在线 or 离线  (0代表离线，1代表在线)
 	 var mapModel = 1;
 	 // 地图类型 百度(b) or 天地图(t)
 	 var loadMapType = "b";
 	// 摄像头类型（1连线，2代表4G）
 	 var videoType = 1;
 	 var video4gServerIp = "47.105.162.81";
 	 //var video4gServerIp = window.location.hostname;
 	 var video4gServerPort = 88;
 	 var video4gPort = 6604;
 	 //扩散模型取值地址
 	 var gasResultUrl ='D:/workspace/CDB/';
 	 //经纬度判断范围值
 	 var global_lonMin = 41;
	 var global_lonMax = 42;
	 var global_latMin = 123;
	 var global_latMax = 125;
 	 //访问后台地址，不修改
     var baseUrl =window.location.protocol+"//"+window.location.host; 
     //采集的url，不修改
	 var collectUrl = "http://127.0.0.1:9000";
/*
error msg
*/      
      var networkErrorMsg="网络错误,请刷新重试！";
      var systemAbnormalityMsg="系统异常,请联系管理员！"; 
   
      function getRealTimeGlobal_gasIsAlertM(){
    	  var rateSelect = 28;
    		if(global_gasIsAlertNameData=="二氧化氮"){
    			rateSelect = 46;
    		}else if(xlydwZhqtSelect=="硫化氢"){
    			rateSelect = 34;
    		}else if(xlydwZhqtSelect=="可燃气体"){
    			rateSelect = 16;
    		}else if(xlydwZhqtSelect=="一氧化碳"){
    			rateSelect = 28;
    		}else if(xlydwZhqtSelect=="VOC"){
    			rateSelect = 16;
    		}
    		return rateSelect;
        }
    //大庆
function changeMonishujuData(){
	global_weatherData.windSpeed =$("#commonTQFS").val()?$("#commonTQFS").val():$("#tqxxFS").val();
	global_weatherData.temperature = $("#commonTQWD").val()?$("#commonTQWD").val():$("#tqxxWD").val();
	global_weatherData.humidity = $("#commonTQSD").val()?$("#commonTQSD").val():$("#tqxxSD").val();
	global_weatherData.windDirection = $("#commonTQFXValue").val()?$("#commonTQFXValue").val():$("#tqxxFX").val();
	global_weatherData.realTimePressure = $("#commonTQYQ").val()?$("#commonTQYQ").val():$("#tqxxYQ").val();
	
	global_gasIsAlertNameData = "CO";
	global_gasIsAlertM=28;
	//设备1
	var realDataList = [];
	var esm27Date = {};
	var realDataObj = {};
	realDataObj.gasName = "H2S";
	realDataObj.gasValue = 25;
	realDataList.push(realDataObj);
	realDataObj = {};
	realDataObj.gasName = "VOC";
	realDataObj.gasValue = 35;
	realDataList.push(realDataObj);
	
	realDataObj = {};
	realDataObj.gasName = "O2";
	realDataObj.gasValue = 25;
	realDataList.push(realDataObj);
	
	realDataObj = {};
	realDataObj.gasName = "CO";
	realDataObj.gasValue = 30;
	realDataList.push(realDataObj);
	
	realDataObj = {};
	realDataObj.gasName = "CO2";
	realDataObj.gasValue = 9;
	realDataList.push(realDataObj);
	
	esm27Date.realDataList = realDataList;
	esm27Date.pointLL = [123.955854,41.885002];
	global_realtimeData.set("001",esm27Date);
	
	//设备2
	realDataList = [];
	esm27Date = {};
	var realDataObj = {};
	realDataObj.gasName = "H2S";
	realDataObj.gasValue = 40;
	realDataList.push(realDataObj);
	realDataObj = {};
	realDataObj.gasName = "VOC";
	realDataObj.gasValue = 45;
	realDataList.push(realDataObj);
	
	realDataObj = {};
	realDataObj.gasName = "O2";
	realDataObj.gasValue = 25;
	realDataList.push(realDataObj);
	
	realDataObj = {};
	realDataObj.gasName = "CO";
	realDataObj.gasValue = 40;
	realDataList.push(realDataObj);
	
	realDataObj = {};
	realDataObj.gasName = "CO2";
	realDataObj.gasValue = 9;
	realDataList.push(realDataObj);
	esm27Date.realDataList = realDataList;
	esm27Date.pointLL = [123.956755,41.885290];
	global_realtimeData.set("002",esm27Date);

	//设备3
	realDataList = [];
	esm27Date = {};
	var realDataObj = {};
	realDataObj.gasName = "H2S";
	realDataObj.gasValue = 30;
	realDataList.push(realDataObj);
	realDataObj = {};
	realDataObj.gasName = "VOC";
	realDataObj.gasValue = 40;
	realDataList.push(realDataObj);
	
	realDataObj = {};
	realDataObj.gasName = "O2";
	realDataObj.gasValue = 25;
	realDataList.push(realDataObj);
	
	realDataObj = {};
	realDataObj.gasName = "CO";
	realDataObj.gasValue = 40;
	realDataList.push(realDataObj);
	
	realDataObj = {};
	realDataObj.gasName = "CO2";
	realDataObj.gasValue = 9;
	realDataList.push(realDataObj);
	esm27Date.realDataList = realDataList;
	esm27Date.pointLL = [123.956584,41.884811];
	global_realtimeData.set("003",esm27Date);
	
	//设备4
	realDataList = [];
	esm27Date = {};
	var realDataObj = {};
	realDataObj.gasName = "H2S";
	realDataObj.gasValue = 40;
	realDataList.push(realDataObj);
	realDataObj = {};
	realDataObj.gasName = "VOC";
	realDataObj.gasValue = 49;
	realDataList.push(realDataObj);
	
	realDataObj = {};
	realDataObj.gasName = "O2";
	realDataObj.gasValue = 25;
	realDataList.push(realDataObj);
	
	realDataObj = {};
	realDataObj.gasName = "CO";
	realDataObj.gasValue = 30;
	realDataList.push(realDataObj);
	
	realDataObj = {};
	realDataObj.gasName = "CO2";
	realDataObj.gasValue = 9;
	realDataList.push(realDataObj);
	esm27Date.realDataList = realDataList;
	esm27Date.pointLL = [123.955951,41.884923];
	global_realtimeData.set("004",esm27Date);
	
	global_alertGrasZNTJ.set("硫化氢","硫化氢");
}