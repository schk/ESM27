ol.source.TianMap = function(options){
    var options = options ? options : {};
      var attributions;
      if(options.attributions !== undefined){
          attributions = option.attributions;
      }else{
          attributions = [ol.source.BaiduMap.ATTRIBUTION];
      }

     
    var url;
    if(mapModel==0){
    	// 离线
        if(options.mapType == "sat"){
            url = offLineMapUrl+"/map/wx/{z}/{x}/{y}.png";
        }else if(options.mapType == "satLabel"){
            url = "http://t{0-4}.tianditu.com/DataServer?T=cia_w&x={x}&y={y}&l={z}&tk=d0db17a1199d0ff0d904621da05f2271";
        }else if(options.mapType == "label"){
            url = "http://t{0-4}.tianditu.com/DataServer?T=cva_w&x={x}&y={y}&l={z}&tk=d0db17a1199d0ff0d904621da05f2271";
        }else{
            url = offLineMapUrl+"/map/xz/{z}/{x}/{y}.png";
        }
    }else{
    	// 在线
        if(options.mapType == "sat"){
            url = "http://t{0-4}.tianditu.com/DataServer?T=img_w&x={x}&y={y}&l={z}&tk=d0db17a1199d0ff0d904621da05f2271";
        }else if(options.mapType == "satLabel"){
            url = "http://t{0-4}.tianditu.com/DataServer?T=cia_w&x={x}&y={y}&l={z}&tk=d0db17a1199d0ff0d904621da05f2271";
        }else if(options.mapType == "label"){
            url = "http://t{0-4}.tianditu.com/DataServer?T=cva_w&x={x}&y={y}&l={z}&tk=d0db17a1199d0ff0d904621da05f2271";
        }else{
            url = "http://t{0-4}.tianditu.com/DataServer?T=vec_w&x={x}&y={y}&l={z}&tk=d0db17a1199d0ff0d904621da05f2271";
        }
    }
    
     ol.source.XYZ.call(this, {
        attributions: attributions,
      projection: mapModel==0?ol.proj.get('EPSG:4326'):ol.proj.get('EPSG:3857'),
        cacheSize: options.cacheSize,
        crossOrigin: 'anonymous',
        opaque: options.opaque !== undefined ? options.opaque : true,
        maxZoom: options.maxZoom !== undefined ? options.maxZoom : 19,
        reprojectionErrorThreshold: options.reprojectionErrorThreshold,
        tileLoadFunction: options.tileLoadFunction,
        url: url,
        wrapX: options.wrapX
      });
}
ol.inherits(ol.source.TianMap, ol.source.XYZ);
ol.source.TianMap.ATTRIBUTION = new ol.Attribution({
    html: ''
});


var tianMapRoadLayer = new ol.layer.Tile({
    title: "天地图路网",
    source: new ol.source.TianMap()
});
var tianMapLableLayer = new ol.layer.Tile({
    title: "天地图文字标注",
    source: new ol.source.TianMap({"mapType":"label"})
});
var tianMapSatLayer = new ol.layer.Tile({
    title: "天地图卫星",
    source: new ol.source.TianMap({"mapType":"sat"})
});

