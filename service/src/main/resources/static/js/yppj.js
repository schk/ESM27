﻿
//计算
//(热波传播速度+燃烧线速度)] - f*H;
function time_qzyy(H,h,hs,ss,f){
	var t = [(H-h)/(hs+ss)] - f*H;
	return t;
}
// 1、轻质原油
// 燃烧线速度：0.10~0.46m/h
// 热波传播速度：0.43~1.27m/h
// 最短时间=[(液面高度H-水垫层高度h)/(1.27+0.46)]-0.1*液面高度H
// 最长时间=[(液面高度H-水垫层高度h)/(0.43+0.10)]-0.1*液面高度H
// 含水率 0.3% 以上
function jet_time_qzyy1(H, h, f)	// 最短时间
{
	var t = [(H-h)/(1.27+0.46)] - f*H;
	return t;
}
function jet_time_qzyy2(H, h, f)	// 最长时间
{
	var t = [(H-h)/(0.43+0.10)] - f*H;
	return t;
}
// 含水率 0.3% 以下
// 热波传播速度：0.38~0.90m/h
function jet_time_qzyy3(H, h, f)	// 最短时间
{
	var t = [(H-h)/(0.90+0.46)] - f*H;
	return t;
}
function jet_time_qzyy4(H, h, f)	// 最长时间
{
	var t = [(H-h)/(0.38+0.10)] - f*H;
	return t;
}


// 2、重质原油
// 燃烧线速度：0.075~0.13m/h
// 热波传播速度：0.30~1.27m/h
// 最短时间=[(液面高度H-水垫层高度h)/(1.27+0.13)]-0.1*液面高度H
// 最长时间=[(液面高度H-水垫层高度h)/(0.30+0.075)]-0.1*液面高度H
// 含水率 0.3% 以上
function jet_time_zzyy1(H, h, f)	// 最短时间
{
	var t = [(H-h)/(1.27+0.13)] - f*H;
	return t;
}
function jet_time_zzyy2(H, h, f)	// 最长时间
{
	var t = [(H-h)/(0.30+0.075)] - f*H;
	return t;
}
// 含水率 0.3% 以下
// 热波传播速度：0.50~0.75m/h
function jet_time_zzyy3(H, h, f)	// 最短时间
{
	var t = [(H-h)/(0.75+0.13)] - f*H;
	return t;
}
function jet_time_zzyy4(H, h, f)	// 最长时间
{
	var t = [(H-h)/(0.50+0.075)] - f*H;
	return t;
}


// 3、煤油
// 燃烧线速度：0.125~0.20m/h
// 热波传播速度：0m/h
// 最短时间=[(液面高度H-水垫层高度h)/0.2]-0.1*液面高度H
// 最长时间=[(液面高度H-水垫层高度h)/0.125]-0.1*液面高度H

function jet_time_my1(H, h, f)	// 最短时间
{
	var t = [(H-h)/0.2] - f*H;
	return t;
}
function jet_time_my2(H, h, f)	// 最长时间
{
	var t = [(H-h)/0.125] - f*H;
	return t;
}


// 4、汽油
// 燃烧线速度：0.15~0.30m/h
// 热波传播速度：0m/h
// 最短时间=[(液面高度H-水垫层高度h)/0.3]-0.1*液面高度H
// 最长时间=[(液面高度H-水垫层高度h)/0.15]-0.1*液面高度H

function jet_time_qy1(H, h, f)	// 最短时间
{
	var t = [(H-h)/0.3] - f*H;
	return t;
}
function jet_time_qy2(H, h, f)	// 最长时间
{
	var t = [(H-h)/0.15] - f*H;
	return t;
}
