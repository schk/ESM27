/**
 * 画圆功能
 * @param coord 中心点
 * @param distance 距离
 * @param layer 画圆图层
 * @param map map对象
 * @param utils 工具类
 */
var drawCircle = function(coord,distance,layer,map,utils){
	
	var control_point = utils.fromLonLat(utils.calculatePoint(utils.toLonLat(coord),distance,1.57));
	var radius = utils.calculateRadius(coord,control_point);
	
	var marker = new ol.Feature({
		geometry: new ol.geom.Point(coord)
	});
	var marker_style = new ol.style.Style({
		image: new ol.style.Icon({
			src: baseUrl + '/images/end.png',
			offset:[0.5,0.5]
		})
	})
	marker.setStyle(marker_style);
	
	var c_marker = new ol.Feature({
		geometry: new ol.geom.Point(control_point)
	});
	var content = utils.formatLength(distance);
	var c_marker_style = new ol.style.Style({
		image: new ol.style.Icon({
			src: baseUrl + '/images/drag.svg',
			offset:[0.5,0.5]
		}),
		text: new ol.style.Text({
			text: content,
			offsetX:45,
			offsetY:-10,
			backgroundFill: new ol.style.Fill({color : '#fff'}),
			backgroundStroke: new ol.style.Stroke({
				color:'#333',
				width:1
			}),
			padding:[0,5,0,5]
		})
	})
	
	c_marker.set('drag',true);
	c_marker.set('isNear',true);
	c_marker.set('center',coord);
	c_marker.setStyle(c_marker_style);
	
	var circle = new ol.Feature({
		geometry: new ol.geom.Circle(coord,radius)
	})
	
	var circle_style = new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: '#3385FF',
            width: 1
        }),
        fill: new ol.style.Fill({
            color: 'rgba(51,133,255, 0.3)'
        })
    })
	
	circle.setStyle(circle_style);
	circle.setId('circle');
	layer.getSource().clear();
	layer.getSource().addFeatures([marker,c_marker,circle]);
	map.getView().fit(circle.getGeometry().getExtent(),map.getSize());
}

function drawCircleByDis(point,distance,color,ends,isFill,util){
	var coord = point;
	var fill_color = util.hex2Rgba(color,0.5);
	var center = util.fromLonLat(util.calculatePoint(util.toLonLat(coord),distance,1.57));
	var radius = util.calculateRadius(coord,center);
	var cgeom = new ol.geom.Circle(coord,radius);
	var geometry = ol.geom.Polygon.fromCircle(cgeom,64,0);
	if(ends){
		geometry.appendLinearRing(ends);
	}
	var circle = new ol.Feature({
		geometry: geometry
	})
	var circle_style;
	if(isFill==0){
		circle_style = new ol.style.Style({
	        stroke: null,
	        fill: new ol.style.Fill({
	            color: fill_color
	        })
	    })
	}else if(isFill==1){
		circle_style = new ol.style.Style({
			stroke: new ol.style.Stroke({
	            color: color,
	            width: 2
	        }),
	        fill: null
	    })
	}else if(isFill==2){
		circle_style = new ol.style.Style({
			stroke: new ol.style.Stroke({
	            color: color,
	            width: 2
	        }),
	        fill: new ol.style.Fill({
	            color: fill_color
	        })
	    })
	}
	
	circle.setStyle(circle_style);
	return circle
}