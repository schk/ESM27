var isCollect = false;
var alarmAudio;
/**
 * 实时数据相关方法
 */

/*以下为自动获取块方法*/
openAuto = function(){
	 $.ajax({
		 type : "post",
		 url : baseUrl+"/webApi/realTimeData/openAuto.jhtml",
		 timeout : 3000,
		 headers: {"Content-Type": "application/json;charset=utf-8"},
		 data: JSON.stringify({
			 'userId': currentUserId,
			 'roomId': currentRoomName
		 }),
		 success : function(data) {		
			 if(data.httpCode == 200){
				 $.ajax({
					 type : "post",
					 url : collectUrl+"/OpenSerialPort",
					 timeout : 3000,
					 headers: {"Content-Type": "application/json;charset=utf-8"},
					 data: JSON.stringify({
						 'userId': currentUserId,
						 'roomId': currentRoomName,
						 'serialPortName': data.data.serialPortName,
						 'autoPushSensorDataIntervalSeconds': data.data.autoPushSensorDataIntervalSeconds,
						 'deviceIds':data.data.deviceIds
					 }),
					 success : function(data) {		
//						 if(data.httpCode == 200 && data.data != ""){
//							
//							 var returnData = JSON.parse(data.data);
//							 if(returnData.isSuccess == true){
//								 if(subCollect != null){
//									 subCollect.unsubscribe();
//									 subCollect = null;
//								 }	 
//								 if(currentRoomName == null || currentRoomName == ""){
//									 //没加入房间，单机订阅接收
//									 subCollect = stompClient.subscribe('/topic/collect/' + currentUserId, function (mes) {
//										 handleData(mes);
//									 });
//								 }else{
//									 subCollect = stompClient.subscribe('/topic/collect/' + currentRoomName, function (mes) {
//										 handleData(mes);
//									 });
//								 } 
//								 layer.msg("成功开启实时检测，若无实时数据，请检查设备配置", {time: 1000});
//								 $("#openAuto").hide();
//								 $("#closeAuto").show();
//								 isCollect = true;
//							 }else{
//								 if(currentRoomName == null || currentRoomName == ""){
//									 //没加入房间，单机订阅接收
//									 subCollect = stompClient.subscribe('/topic/collect/' + currentUserId, function (mes) {
//										 handleData(mes);
//									 });
//								 }else{
//									 subCollect = stompClient.subscribe('/topic/collect/' + currentRoomName, function (mes) {
//										 handleData(mes);
//									 });
//								 } 
//							 }
//							 
//						 }else{
//							 //layer.msg("当前终端未连接硬件设备或网络异常，请稍候重试", {time: 1000});
//						 }
					 },error: function(error) {
						 //layer.msg("当前终端未连接硬件设备或网络异常，请稍候重试", {time: 1000});
					 }
				 });
			 }
			 if(subCollect != null){
				 subCollect.unsubscribe();
				 subCollect = null;
			 }	 
			 if(currentRoomName == null || currentRoomName == ""){
				 //没加入房间，单机订阅接收
				 subCollect = stompClient.subscribe('/topic/collect/' + currentUserId, function (mes) {
					 handleData(mes);
				 });
			 }else{
				 subCollect = stompClient.subscribe('/topic/collect/' + currentRoomName, function (mes) {
					 handleData(mes);
				 });
			 } 
			 $("#openAuto").hide();
			 $("#closeAuto").show();
			 isCollect = true; 
			 layer.msg("成功开启实时检测", {time: 1000});
		 },error: function(error) {
			 layer.msg("开启实时检测失败", {time: 1000});
		 }
	 });
//	 $.ajax({
//		 type : "post",
//		 url : baseUrl+"/webApi/realTimeData/openAuto.jhtml",
//		 timeout : 3000,
//		 headers: {"Content-Type": "application/json;charset=utf-8"},
//		 data: JSON.stringify({
//			 'userId': currentUserId,
//			 'roomId': currentRoomName
//		 }),
//		 success : function(data) {		
//			 if(data.httpCode == 200 && data.data != ""){
//				 var returnData = JSON.parse(data.data);
//				 if(returnData.isSuccess == true){
//					 if(subCollect != null){
//						 subCollect.unsubscribe();
//						 subCollect = null;
//					 }	 
//					 if(currentRoomName == null || currentRoomName == ""){
//						 //没加入房间，单机订阅接收
//						 subCollect = stompClient.subscribe('/topic/collect/' + currentUserId, function (mes) {
//							 handleData(mes);
//						 });
//					 }else{
//						 subCollect = stompClient.subscribe('/topic/collect/' + currentRoomName, function (mes) {
//							 handleData(mes);
//						 });
//					 } 
//					 layer.msg("成功开启实时检测，若无实时数据，请检查设备配置", {time: 1000});
//					 $("#openAuto").hide();
//					 $("#closeAuto").show();
//					 isCollect = true;
//				 }else{
//					 layer.msg("当前终端未连接硬件设备或网络异常，请稍候重试", {time: 1000});
//				 }
//				 
//			 }else{
//				 layer.msg("当前终端未连接硬件设备或网络异常，请稍候重试", {time: 1000});
//			 }
//		 },error: function(error) {
//			 layer.msg("当前终端未连接硬件设备或网络异常，请稍候重试", {time: 1000});
//		 }
//	 });
//	 $.ajax({
//		type : "post",
//		url : baseUrl+"/webApi/realTimeData/openAuto.jhtml",
//		headers: {"Content-Type": "application/json;charset=utf-8"},
//		success : function(data) {					
//			alert("成功开启实时检测，若无实时数据，请检查设备配置");
//			$("#openAuto").hide();
//			$("#closeAuto").show();
//		},error: function(request) {
//			alert("网络错误");
//		}
//	});
};

closeAuto = function(){

	$.ajax({
		 type : "post",
		 url : collectUrl+"/CloseSerialPort",
		 timeout : 1000,
		 headers: {"Content-Type": "application/json;charset=utf-8"},
		 data: JSON.stringify({
			 userId: currentUserId
		 }),
		 success : function(data) {	
//			 if(data.httpCode == 200 && data.data != ""){
//				 var returnData = JSON.parse(data.data);
//				 if(returnData.isSuccess == true){
//					 
//				 }else{
//					 layer.msg("当前终端未连接硬件设备或网络异常，请稍候重试", {time: 1000});
//				 }
//			 }else{
//				 layer.msg("当前终端未连接硬件设备或网络异常，请稍候重试", {time: 1000});
//			 } 
		 },error: function(error) {
			 //layer.msg("当前终端未连接硬件设备或网络异常，请稍候重试", {time: 1000});
		 }
	 });
	 if(subCollect != null){
		 subCollect.unsubscribe();
		 subCollect = null;
	 }
	 layer.msg("成功关闭实时检测", {time: 1000});
	 $("span[id^=deviceSpan_]").css('background','#a0acbf');
	 $("span[id^=gas_]").text("");
	 $("span[id^=battery_]").text("");
	 $("li[id^=li_]").css('background','');
	 $("#realTimeWindSpeed").text('');
	 $("#realTimeWindDirection").text('');
	 $("#realTimePmValue").text('');
	 $("#realTimeTemperature").text('');
	 $("#realTimeHumidity").text('');
	 $("#realTimePressure").text('');
	 $("#realTimeNoise").text('');
	 $("#openAuto").show();
	 $("#closeAuto").hide();
	 isCollect = false;
	 $('#alarmOpenMove').attr('alarm','false');
	
//	$.ajax({
//		type : "post",
//		url : baseUrl+"/webApi/realTimeData/closeAuto.jhtml",
//		data: "",
//		dataType:"json",
//		headers: {"Content-Type": "application/json;charset=utf-8"},
//		success : function(data) {					
//			alert("成功关闭实时检测");
//			$("#openAuto").show();
//			$("#closeAuto").hide();
//		},error: function(request) {
//			alert("网络错误");
//		}
//	});	
};

//分页
function generatePagination(cp,tp,keyId){
	this.currentPage = Number(cp);
	this.totalPages = Number(tp);
	var jump_prev=currentPage>1?(currentPage-1):currentPage;
	var jump_next=(currentPage<totalPages)?(currentPage+1):totalPages;
	var pageList ="<a name='findPage' pageNum='"+jump_prev+"' class='front'><span></span></a>";
	var page_start = Number(currentPage)/5;
	if(page_start>1){
		pageList = getPageHtml(page_start*5-2,page_start*5+3,totalPages,pageList);
	}else{
		pageList = getPageHtml(1,6,totalPages,pageList);
	}
	pageList +="<a name='findPage' pageNum='"+jump_next+"' class='after'><span></span></a>";
	$("#"+keyId).html(pageList);
	if(keyId=="historyWeatherDataPage"){
		$("#historyWeatherDataPage").find("[name=findPage]").on("click",function(){
		   pageNum=$(this).attr("pageNum");  //获取链接里的页码  
		   getHistoryWeatherDataFn();
		});  
	}else if(keyId=="historyGasDataPage"){
		$("#historyGasDataPage").find("[name=findPage]").on("click",function(){
		   pageNum=$(this).attr("pageNum");  //获取链接里的页码  
		   getHistoryGasDataFn();
		}); 
	}
	
}

function getPageHtml(start,end,totalPages,pageList){
	for(var i=start;i<=end;i++){
		if(i<=totalPages){
			var temp =""
			if(i==currentPage){
				temp="<a name='findPage' pageNum='"+i+"' class='onlink'>"+i+"</a>";
			}else{
				temp="<a name='findPage' pageNum='"+i+"'>"+i+"</a>";
			}
			pageList+=temp;
		}
	}
	return pageList;
}

/**
 * 实时数据处理
 */
function handleData(mes){
	var returnData = JSON.parse(mes.body);
	  if (returnData.returnCode =="realTimeData") {
		  //画设备坐标
		  var esm27Date={};
		  var esm27DateId = returnData.deviceAddress;
		  if(typeof returnData.longitude != '' && typeof returnData.latitude != '' && returnData.longitude!=0 && returnData.latitude!=0){
			  var coordinate = ol.proj.transform([returnData.longitude,returnData.latitude], 'EPSG:4326', 'EPSG:3857');
			  
//			  if(returnData.deviceAddress==1){
//				  coordinate = ol.proj.transform([104.1117451985677,30.676711400349934], 'EPSG:4326', 'EPSG:3857');
//			  }
//			  esm27Point.put(returnData.deviceAddress,[coordinate[0],coordinate[1]]);
			  esm27Date.point = [coordinate[0],coordinate[1]];
			  esm27Date.pointLL = [returnData.longitude,returnData.latitude];
		 var jsoncontent = '{"type":"Feature","id":"123'+returnData.deviceAddress+'","geometry":{"type":"Point","coordinates":['+coordinate[0]+','+coordinate[1]+']},"properties":{"type":"Point","points":[['+coordinate[0]+','+coordinate[1]+']],"params":{"name":"设备'+returnData.deviceAddress+'","desc":"2","groundHeight":"3","accidentType":"4","roomId":"1074270795046133762","type":"marker","clusterType":"esm27q","imagePath":"/mapIcon/mapFixedIcon/qtIcon.png","isCluster":true,"clusterImagePath":"/mapIcon/iconCluster/qtIcon_c.png","isSyn":true,"isSave":true,"recordType":"9","action":"设备","isExist":true,"dataid":1544961263221},"plotType":"Marker"}}';
		 var content = eval("("+jsoncontent+")");
			   positionSynMark(content); 
			  if(returnData.deviceAddress==1){
				  $("#pointJC1").show();
				  $("#pointJC1").attr("lat",coordinate[0]);
				  $("#pointJC1").attr("lon",coordinate[1]);
			  }else if(returnData.deviceAddress==2){
				  $("#pointJC2").show();
				  $("#pointJC2").attr("lat",coordinate[0]);
				  $("#pointJC2").attr("lon",coordinate[1]);
			  }else if(returnData.deviceAddress==3){
				  $("#pointJC3").show();
				  $("#pointJC3").attr("lat",coordinate[0]);
				  $("#pointJC3").attr("lon",coordinate[1]);
			  }else if(returnData.deviceAddress==4){
				  $("#pointJC4").show();
				  $("#pointJC4").attr("lat",coordinate[0]);
				  $("#pointJC4").attr("lon",coordinate[1]);
			  }
			 
		  }
			var realData = returnData.realData;
			var address = returnData.deviceAddress;
			var batteryVoltage = returnData.batteryVoltage;
			var pumpState = returnData.pumpState;
			var alertNum = 0;
			$("#battery_"+address).text(batteryVoltage);
			//电量报警
			if(batteryVoltage>6.8){
				$("#divBattery_"+address).css('background','none');
			}else{
				$("#divBattery_"+address).css('background','#ff0000');
			}
			if(pumpState==1){
				$("#pumpState_"+address).text("正常");
				$("#divPumpState_"+address).css('background','#1fc17b');
			}else{
				$("#pumpState_"+address).text("堵泵");
				$("#divPumpState_"+address).css('background','#ff0000');
			}
			var realDataList = [];
			for (var i = 0; i < realData.length; i++) {
				var realDataObj = {};
				var name = realData[i].gasName;
				realDataObj.gasName = realData[i].gasName;
				realDataObj.gasValue = realData[i].gasValue;
				realDataList.push(realDataObj);
				if (realData[i].gasIsAlert == 2) {
					alertNum++;
					$("#li_"+realData[i].id_).css('background','#fffd00');
					$("#li_"+realData[i].id_).css('color','#333');
					var name = realData[i].gasName.toUpperCase();
					if( name=="氧气" || name=="O2" ||  name=="GAMMA"){
						
					}else{
							global_gasIsAlertNameData = name;
							global_gasIsAlertM=realData[i].molecular;
						
						if(name=="硫化氢" || name.toUpperCase("H2S")){
							global_gasIsAlertNameData = name;
							global_gasIsAlertM=realData[i].molecular;
						}
					}
					if(alarmAudio.paused && $("#cb_alarm_audio").is(':checked')){
						alarmAudio.play();
					}
				}
				else if(realData[i].gasIsAlert == 3){
					alertNum++;
					$("#li_"+realData[i].id_).css('background','#ff0000');
					$("#li_"+realData[i].id_).css('color','#333');
					var name = realData[i].gasName.toUpperCase();
					global_alertGrasZNTJ.set(name,name);
					if( name=="氧气" || name=="O2" ||  name=="GAMMA"){
						
					}else{
							global_gasIsAlertNameData = name;
							global_gasIsAlertM=realData[i].molecular;
						
						if(name=="硫化氢" || name.toUpperCase("H2S")){
							global_gasIsAlertNameData = name;
							global_gasIsAlertM=realData[i].molecular;
						}
					}
					if(alarmAudio.paused && $("#cb_alarm_audio").is(':checked')){
						alarmAudio.play();
					}
				}
				else{
					$("#gas_"+realData[i].id_).parent('li').removeClass("yellow");
					$("#li_"+realData[i].id_).css('background','none');
					$("#li_"+realData[i].id_).css('color','#fff');
				}
				
				$("#gas_"+realData[i].id_).text(parseFloat(realData[i].gasValue).toFixed(1)+realData[i].gasUnit);
				isBH = false;
				$.each(rtData,function(index,ele){
					if(ele.name == '#' + returnData.deviceAddress + '_' + realData[i].id_ + '#'){
						rtData.splice(index,1,{name:'#' + returnData.deviceAddress + '_' + realData[i].id_ + '#',value:parseFloat(realData[i].gasValue).toFixed(1)});
						isBH = true;
						return false;
					}
				});
				if(!isBH)
					rtData.push({name:'#' + returnData.deviceAddress + '_' + realData[i].id_ + '#',value:parseFloat(realData[i].gasValue).toFixed(1)});
			}
			/*
			 * 缓存当前告警气体
			 * */
			esm27Date.realDataList = realDataList;
			global_realtimeData.set(esm27DateId,esm27Date);
			if(alertNum>0){
				//智能推荐报警危化品气体
				showZNTJWHPQT();
				$("#deviceSpan_"+address).css('background','#ff0000');
				$('#alarmOpenMove').attr('alarm','true');
			}else{
				$("#deviceSpan_"+address).css('background','#1fc17b');
				$('#alarmOpenMove').attr('alarm','false');
			}
			$("#lastUpdateTime").text("最后更新时间    "+returnData.lastUpdateTime);
			deviceMap.set("deviceSpan_"+address,new Date().getTime());
		    updateExprResult();
		
	 } 
	 if (returnData.returnCode =="serialPortData") {
		 var returnData = JSON.parse(mes.body);
		 var jsondata = returnData.returData;
		 var data = JSON.parse(jsondata);
		 var html="";
		 for (var i = 0; i < data.length; i++) {
			 html+='<li>'+data[i]+'</li>'
		 }
		 $("#allPortUl").append(html);
	 }
	 if (returnData.returnCode =="weatherData") {
		 var returnData = JSON.parse(mes.body);
		 var data = returnData.wsd;
		 data.humidity = parseFloat(data.humidity).toFixed(1)
		 global_weatherData = data;
		 $("#realTimeWindSpeed").text(data.windSpeed+"m/s");
		 $("#realTimeWindDirection").text(data.windDirection);
		 $("#realTimePmValue").text(data.pmValue);
		 $("#realTimeTemperature").text(parseFloat(data.temperature).toFixed(1)+"℃");
		 $("#realTimeHumidity").text(data.humidity+"%");
		 if(data.pressure<1){
			 data.pressure = 100.00;
		 }
		 $("#realTimePressure").text(data.pressure+"Pa");
		 $("#realTimeNoise").text(data.noise);
		 $('#WindVaneId').css('transform','rotate('+data.windDirectionValue+'deg)'); 
		 global_weatherData.windSpeed = data.windSpeed;
		 global_weatherData.windDirection = parseFloat(data.windDirectionValue).toFixed(1);
		 global_weatherData.temperature = parseFloat(data.temperature).toFixed(1);
		 global_weatherData.humidity = parseFloat(data.humidity).toFixed(1);
		 global_weatherData.pressure =parseFloat(data.pressure).toFixed(1);
		 $.each(rtData,function(index,ele){
			if(ele.name == '#0_FS#'){
				rtData.splice(index,1,{name:'#0_FS#',value:data.windSpeed});
			}
			if(ele.name == '#0_FX#'){
				rtData.splice(index,1,{name:'#0_FX#',value:data.windDirectionValue});
			}
			if(ele.name == '#0_PM#'){
				rtData.splice(index,1,{name:'#0_PM#',value:data.pmValue});
			}
			if(ele.name == '#0_WD#'){
				rtData.splice(index,1,{name:'#0_WD#',value:data.temperature});
			}
			if(ele.name == '#0_QY#'){
				rtData.splice(index,1,{name:'#0_QY#',value:data.pressure});
			}
			if(ele.name == '#0_SD#'){
				rtData.splice(index,1,{name:'#0_SD#',value:data.humidity});
			}
			if(ele.name == '#0_ZX#'){
				rtData.splice(index,1,{name:'#0_ZX#',value:data.noise});
			}
		 });	
		 weatherMap.set("weatherDev",new Date().getTime());
		 updateExprResult();
	 }
	 if(returnData.returnCode =="gaussionModelParametersSyn"){
		 //todo 刘小蕾
		 var returnData = JSON.parse(mes.body);
	 }
	 if (returnData.returnCode =="AutoAlarm") {
			//returnData.deviceAddress(设备号)
				//returnData.latitude(纬度)
				//returnData.longitude(精度)
				var realData = returnData.realData;
				var address = returnData.deviceAddress;
				var alertNum = 0;
				for (var i = 0; i < realData.length; i++) {
					if (realData[i].gasIsAlert == 2) {
						alertNum++;
						$("#li_"+realData[i].id_).css('background','#ff0000');
					}else{
						$("#gas_"+realData[i].id_).parent('li').removeClass("yellow");
						$("#li_"+realData[i].id_).css('background','none');
					}
//					$("#gas_"+realData[i].id_).text(realData[i].gasValue+realData[i].gasUnit);
//					isBH = false;
//					$.each(rtData,function(index,ele){
//						if(ele.name == '#' + returnData.deviceAddress + '_' + realData[i].id_ + '#'){
//							rtData.splice(index,1,{name:'#' + returnData.deviceAddress + '_' + realData[i].id_ + '#',value:realData[i].gasValue});
//							isBH = true;
//							return false;
//						}
//					});
//					if(!isBH)
//						rtData.push({name:'#' + returnData.deviceAddress + '_' + realData[i].id_ + '#',value:realData[i].gasValue});
				}
				if(alertNum>0){
					$("#deviceSpan_"+address).css('background','#ff0000');
					$('#alarmOpenMove').attr('alarm','true');
				}else{
					$("#deviceSpan_"+address).css('background','#1fc17b');
					$('#alarmOpenMove').attr('alarm','false');
				}
				$("#lastUpdateTime").text("最后更新时间    "+returnData.lastUpdateTime);
			    updateExprResult();
		 } 
	 
}


//window.onbeforeunload = function (){
//	var node=$("#closeAuto");
//	if (node.is(':hidden')) {
//	}else{
//		$.ajax({
//			type : "post",
//			url : baseUrl+"/webApi/realTimeData/closeAuto.jhtml",
//			data: "",
//			dataType:"json",
//			headers: {"Content-Type": "application/json;charset=utf-8"},
//			success : function(data) {					
//				//alert("已关闭实时检测");
//				$("#openAuto").show();
//				$("#closeAuto").hide();
//			},error: function(request) {
//				//alert("网络错误");
//			}
//		});	
//	}
//	
//};
/*以下为数据配置块方法*/

function getAllProt(){
	$("#allPortUl").html('');
	$.ajax({
		type : "post",
		url : collectUrl+"/GetSystemSerialPortList",
		data: "",
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {		
			if(data != ""){
				var returnData = data;
				if(returnData.isSuccess == true){
					var html="";
					for (var i = 0; i < returnData.awsPostdata.length; i++) {
					  html+='<li>'+returnData.awsPostdata[i]+'</li>'
					}
					$("#allPortUl").append(html);
				}else{
					layer.confirm('当前终端未连接硬件设备或网络异常，请检查连接后重试！', {
				         btn: ['确定'] //按钮
				    },function(){
				    	layer.closeAll('dialog');
				    });
				}
			}else{
				layer.confirm('当前终端未连接硬件设备或网络异常，请检查连接后重试！', {
			         btn: ['确定'] //按钮
			    },function(){
			    	layer.closeAll('dialog');
			    });
			}
		},error: function(request) {
			layer.confirm('当前终端未连接硬件设备或网络异常，请检查连接后重试！', {
		         btn: ['确定'] //按钮
		    },function(){
		    	layer.closeAll('dialog');
		    });
		}
	});	
};

//function getAllProt(){
//	$("#allPortUl").html('');
//	$.ajax({
//		type : "post",
//		url : baseUrl+"/webApi/realTimeData/qrySerialPortList.jhtml",
//		data: "",
//		dataType:"json",
//		headers: {"Content-Type": "application/json;charset=utf-8"},
//		success : function(data) {		
//			if(data.httpCode == 200 && data.data != ""){
//				var returnData = JSON.parse(data.data);
//				if(returnData.isSuccess == true){
//					var html="";
//					for (var i = 0; i < returnData.awsPostdata.length; i++) {
//					  html+='<li>'+returnData.awsPostdata[i]+'</li>'
//					}
//					$("#allPortUl").append(html);
//				}else{
//					layer.msg("当前终端未连接硬件设备或网络异常，请稍候重试", {time: 1000});
//				}
//			}else{
//				 layer.msg("当前终端未连接硬件设备或网络异常，请稍候重试", {time: 1000});
//			}
//		},error: function(request) {
//			layer.msg("当前终端未连接硬件设备或网络异常，请稍候重试", {time: 1000});
//		}
//	});	
//};


function cancleSetUpInfo(obj){
	$("#pzjmPanel").hide();
};

function getDataConfigureEvent(){
	getDeviceConfigureEvent();
    $.ajax({
		type : "post",
		url : baseUrl+"/webApi/realTimeData/qryDataConfigure.jhtml",
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				if(data.data){
					$("#weatherStationNo").val(data.data.weatherStation[0].stationPort);
				}
			}
			
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	})  
 };
 
 function removeDetectorSerial(obj){
	 $(obj).parents("li").siblings().each(function(i){
         $(this).find("span").eq(0).html(i+1)
     })
	 $(obj).parent().remove();
 };
 
 function addSerialNoEvent(){
	var lockValue = $("#configureLockId").attr("class");
	if (lockValue == "shutIcon") {
		layer.msg("请先进行解锁！！！");
		return false;
	}
	var sno = $("#serialNoInput").val();
	if (sno=="") {
		layer.msg("气体检测仪编号不能为空！");
		return false;
	}
	var spanList=$("span[name='detectorSerialNo']");
	if (spanList.length>0) {
 	 	for (var i = 0; i < spanList.length; i++) {
 	 		if (spanList[i].textContent == sno) {
 	 			layer.msg("气体检测仪编号不能重复！");
 	 			return false;
			}
 		}
	}
//	var indexNum = $("span[name='detectorSerialNo']").length
//	var html='';
//	html+='<li>';
//	//html+='<span class="orderNumber">'+(indexNum+1)+'</span>';
//	html+='<span name="detectorSerialNo">'+sno+'</span>';
//	html+='<i onClick="removeDetectorSerial(this);"></i>';
//	html+='</li>';
//	$("#deviceDetectorListId").append(html);
	var params={"serial":sno}
	$.ajax({
		type : "post",
		url : baseUrl+"/webApi/realTimeData/addConfigure.jhtml",
		data: JSON.stringify(params), 
		dataType:"json",
		ladeview:false,
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				getDeviceConfigureEvent();
				$("#serialNoInput").val("");
			}
			
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
	     }
	})
	
	
 };
 
 
 function removeDevice(deviceId){
	var lockValue = $("#configureLockId").attr("class");
	if (lockValue == "shutIcon") {
		layer.msg("请先进行解锁！！！");
		return false;
	}
	var params=deviceId;
	$.ajax({
		type : "post",
		url : baseUrl+"/webApi/realTimeData/delConfigure.jhtml",
		data: JSON.stringify(params),
		dataType:"json",
		ladeview:false,
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				getDeviceConfigureEvent();
			}
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
	     }
	})
	
	
 };
 

 
 /*保存数据配置的信息*/
 function saveDataConfigureInfo(){
	var lockValue = $("#configureLockId").attr("class");
	if (lockValue == "shutIcon") {
		layer.msg("请先进行解锁！！！");
	}else{
		var params = {};
	 	var weatherStationNo=$("#weatherStationNo").val();
	 	if (weatherStationNo!="") {
	 		params.weatherStation = [{"stationPort":weatherStationNo}];
		}else{
			layer.msg("便携气象站不能为空！");
			return false;
		}
//	 	params.gasDetector = [];
//	 	var spanList=$("span[name='detectorSerialNo']");
//	 	if (spanList.length>0) {
//	 	 	for (var i = 0; i < spanList.length; i++) {
//	 			var entity = {
//	 					"serial":spanList[i].textContent
//	 			}
//	 			params.gasDetector.push(entity)
//	 		}
//		}else{
//			layer.msg("气体检测仪不能为0个！");
//			return false;
//		}
	 	
	 	var paramsGas = [];
	 	var gdId = $("#deviceDetectorId").val();
	 	var gasNUllData=0;
	 	for (var i = 1; i < 8; i++) {
	 		var entity = {
					"gasId":$("#tongdaoId_"+i).val(),
					"id":$("#gddId_"+i).val()
			}
	 		paramsGas.push(entity)
			if (entity.gasId=="") {
				gasNUllData = gasNUllData+1;
			}
		}	
	 	if (paramsGas.length==gasNUllData) {
	 		layer.msg("请配置通道气体!");
	 		return false;
		}
	 	params.gasDetectorData=paramsGas;
		$.ajax({
			type : "post",
			url : baseUrl+"/webApi/realTimeData/updateDataConfigure.jhtml",
			data: JSON.stringify(params),
			dataType:"json",
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {	
				if(data.httpCode==200){
					layer.msg("数据配置设置成功");
					$('#deviceC').html('');
	 				$.each(data.data,function(index,item){
	 					var html = '<div class="facDate fl">';
	 					html += '<div class="facHd" id="deviceTitle_' + item.deviceSerial + '" onclick="cancleDeviceDataInfo(\''+ item.deviceSerial +'\')"><span id="deviceSpan_'+item.deviceSerial+'" name="deviceIcon" class="lamp"></span>设备编号：<span name="deviceSerialEsmId">'+ item.deviceSerial +'</span><i class="rightArr"></i></div>';
	 					if(data.data.length>4){
	 						html += '<div class="ca cf botTop" id="deviceContent_' + item.deviceSerial + '" style="display:none">';
	 					}else{
	 						html += '<div class="ca cf botTop" id="deviceContent_' + item.deviceSerial + '" >';
	 					}
	 					html += '<div class="fl facList">';
	 					html += '<ol>';
	 					for(var i=0;i<4;i++){
	 						html += '<li class="yellow"><span class="explain">'+ item.gasDetectorDataVo[i].gasName +'</span><span class="dateNumber" id="gas_'+ item.gasDetectorDataVo[i].id_ +'"></span></li>';
	 					}
	 					
	 					html += '</ol>';
 						html += '</div>';
 						
 						html += '<div class="fr facList">';
	 					html += '<ol>';
	 					for(var i=4;i<7;i++){
	 						html += '<li class="yellow"><span class="explain">'+ item.gasDetectorDataVo[i].gasName +'</span><span class="dateNumber" id="gas_'+ item.gasDetectorDataVo[i].id_ +'"></span></li>';
	 					}
	 					
	 					html += '</ol>';
 						html += '</div>';
 						
	 					html += '</div>';
	 					html += '</div>';
	 					$('#deviceC').append(html);
	 				});
				}
				
			},error: function(request) {
				layer.msg(networkErrorMsg, {time: 2000});
		     }
		})
	}
 };
 
 function getTimeConfigureEvent(){
	    getAllProt();
	    getProtAndDeviceTime();
};

/*获取已配置的串口信息*/
function getProtAndDeviceTime(){
    $.ajax({
		type : "post",
		url : baseUrl+"/webApi/realTimeData/qrySerialPort.jhtml",
		dataType:"json",
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				if(data.data){
					$("#serialPortNo").text(data.data.serialPort);
					$("#timeSpaceInput").val(data.data.timeCell);
				}
			}
			
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	})  
};

/*保存时间间隔的信息*/
function saveTimeSpaceInfo(){
	var lockValue = $("#configureLockId").attr("class");
	if (lockValue == "shutIcon") {
		layer.msg("请先进行解锁！！！");
	}else{
		var timeCell = $("#timeSpaceInput").val();
		if (timeCell!="") {
			if (!isNaN(timeCell)) {
				if (Number(timeCell)<5) {
					layer.msg("时间间隔应大于5秒！");
					return false;
				}
			}else{
				layer.msg("请输入数字！");
				return false;
			}
		}else{
			layer.msg("时间间隔不能为空！");
			return false;
		}
		var serialPort = $("#serialPortNo").text();
		var params={"timeCell":timeCell,"serialPort":serialPort};
		$.ajax({
			type : "post",
			url : baseUrl+"/webApi/realTimeData/updateDeviceTimee.jhtml",
			data: JSON.stringify(params),
			dataType:"json",
			ladeview:false,
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {	
				if(data.httpCode==200){
					layer.msg("时间间隔设置成功");
				}
				
			},error: function(request) {
				layer.msg(networkErrorMsg, {time: 2000});
		     }
		})
	}
};

function getDeviceConfigureEvent(){
	$("#deviceDetectorListId").children().filter("li").remove();
    $.ajax({
		type : "post",
		url : baseUrl+"/webApi/realTimeData/qryDeviceConfigure.jhtml",
		dataType:"json",
		ladeview:false,
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				if(data.data){
					var html='';
					var gdId = "";
					if (!$.isEmptyObject(data.data.gasDetector) && data.data.gasDetector.length>0) {
						for (var i = 0; i < data.data.gasDetector.length; i++) {
							if (i==0) {
								gdId=data.data.gasDetector[i].id;
								html+='<input type="hidden" id="deviceDetectorId" value="'+data.data.gasDetector[i].id+'">';
								html+='<li class="selectOn">';
								html+='<div class="faScrollDiv"><span name="detectorSerialNo" id="'+data.data.gasDetector[i].id+'">'+data.data.gasDetector[i].serial+'</span><i></i></div>';
								html+='<em onClick="removeDevice(\''+data.data.gasDetector[i].id+'\');"></em>';
								html+='</li>';
							}else{
								html+='<li>';
								html+='<div class="faScrollDiv"><span name="detectorSerialNo" id="'+data.data.gasDetector[i].id+'">'+data.data.gasDetector[i].serial+'</span><i></i></div>';
								html+='<em onClick="removeDevice(\''+data.data.gasDetector[i].id+'\');"></em>';
								html+='</li>';
							}
						}
					}
					if (!$.isEmptyObject(data.data.gasDetectorDataVo) && data.data.gasDetectorDataVo.length>0) {
						var gddData = data.data.gasDetectorDataVo;
						for (var i = 0; i < gddData.length; i++) {
							if (gddData[i].gasDetectorId == gdId) {
								if (gddData[i].gasName!=null && gddData[i].gasName!=null != "") {
									$("#tongdaoValue_"+gddData[i].detectorPosition).text(gddData[i].gasName);
								}else{
									$("#tongdaoValue_"+gddData[i].detectorPosition).text("未设置");
								}
								$("#tongdaoId_"+gddData[i].detectorPosition).val(gddData[i].gasId);
								$("#gddId_"+gddData[i].detectorPosition).val(gddData[i].id);
							}
						}
					}
					$("#deviceDetectorListId").append(html);
				}
			}
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	})  
 };
 
 
 /*保存设备配置的信息*/
 function saveDeviceConfigureInfo(){
	var lockValue = $("#configureLockId").attr("class");
	if (lockValue == "shutIcon") {
		layer.msg("请先进行解锁！！！");
		return false;
	}else{
		var params = [];
	 	var gdId = $("#deviceDetectorId").val();
	 	var gasNUllData=0;
	 	for (var i = 1; i < 8; i++) {
	 		var entity = {
					"gasId":$("#tongdaoId_"+i).val(),
					"id":$("#gddId_"+i).val()
			}
	 		params.push(entity)
			if (entity.gasId=="") {
				gasNUllData = gasNUllData+1;
			}
		}	
	 	if (params.length==gasNUllData) {
	 		layer.msg("请配置通道气体!");
	 		return false;
		}
	 	$.ajax({
	 		type : "post",
	 		url : baseUrl+"/webApi/realTimeData/updateDeviceConfigure.jhtml",
	 		data: JSON.stringify(params),
	 		ladeview:false,
	 		dataType:"json",
	 		headers: {"Content-Type": "application/json;charset=utf-8"},
	 		success : function(data) {	
	 			if(data.httpCode==200){
	 				layer.msg("设备配置设置成功");
	 				$('#deviceC').html('');
	 				$.each(data.data,function(index,item){
	 					var html = '<div class="facDate fl">';
	 					html += '<div class="facHd" id="deviceTitle_' + item.deviceSerial + '" onclick="cancleDeviceDataInfo(\''+ item.deviceSerial +'\')"><span id="deviceSpan_'+item.deviceSerial+'" name="deviceIcon" class="lamp"></span>设备编号：<span name="deviceSerialEsmId">'+ item.deviceSerial +'</span><i class="rightArr"></i></div>';
	 					if(data.data.length>4){
	 						html += '<div class="ca cf botTop" id="deviceContent_' + item.deviceSerial + '" style="display:none">';
	 					}else{
	 						html += '<div class="ca cf botTop" id="deviceContent_' + item.deviceSerial + '">';
	 					}
	 					html += '<div class="fl facList">';
	 					html += '<ol>';
	 					for(var i=0;i<4;i++){
	 						html += '<li class="yellow"><span class="explain">'+ item.gasDetectorDataVo[i].gasName +'</span><span class="dateNumber" id="gas_'+ item.gasDetectorDataVo[i].id_ +'"></span></li>';
	 					}
	 					
	 					html += '</ol>';
 						html += '</div>';
 						
 						html += '<div class="fr facList">';
	 					html += '<ol>';
	 					for(var i=4;i<7;i++){
	 						html += '<li class="yellow"><span class="explain">'+ item.gasDetectorDataVo[i].gasName +'</span><span class="dateNumber" id="gas_'+ item.gasDetectorDataVo[i].id_ +'"></span></li>';
	 					}
	 					
	 					html += '</ol>';
 						html += '</div>';
 						
	 					html += '</div>';
	 					html += '</div>';
	 					$('#deviceC').append(html);
	 				});
	 			}
	 			
	 		},error: function(request) {
	 			layer.msg(networkErrorMsg, {time: 2000});
	 	     }
	 	})
	}
 };
 
function controlGasSelect(params){
	$("div[id^='tongdao_']").each(function(i){  
		var num = i+1;
		if(params!=num){
			var otherNode = $("#tongdao_"+num);
			otherNode.hide();
		}
	});
	var node=$("#tongdao_"+params);
	if (node.is(':hidden')) {
		node.show();
	}else{
		node.hide();
	}
};

function controlSerialPortSelect(){
	var node=$("#allPortDiv");
	if (node.is(':hidden')) {
		node.show();
	}else{
		node.hide();
	}
};


function configureLockOnclick(params){
	var lockValue = $("#configureLockId").attr("class");
	if (lockValue == "shutIcon") {
		$("#psdDiv1").show();
		$("#psdDiv2").show();
	}else{
		layer.msg("已锁定");
		$('#configureLockId').removeClass('openIcon').addClass('shutIcon');
	}
};

function psdCloseDivFn(){
	$("#psdDiv1").hide();
	$("#psdDiv2").hide();
	$("#psdErrorMsgP").hide();
	$("#psdInpntId").val("");
};

function openLockEvent(params){
	var params = $('#psdInpntId').val();
	if (params!="") {
		$.ajax({
			type : "post",
			url : baseUrl+"/webApi/realTimeData/openLock.jhtml",
			data: JSON.stringify(params),
			dataType:"json",
			ladeview:false,
			headers: {"Content-Type": "application/json;charset=utf-8"},
			success : function(data) {	
				if(data.httpCode==200){
					if (data.data==1) {
						layer.msg("解锁成功");
						$("#psdDiv1").hide();
						$("#psdDiv2").hide();
						$("#psdErrorMsgP").hide();
						$("#psdInpntId").val("");
						$('#configureLockId').removeClass('shutIcon').addClass('openIcon');
					}else{
						$("#psdInpntId").val("");
						$("#psdErrorMsgP").show();
					}
				}
			},error: function(request) {
				layer.msg(networkErrorMsg, {time: 2000});
		     }
		})
	}else{
		layer.msg("密码不能为空！");
		return false;
	}
};

/*以下为数据展示块方法*/
function cancleDeviceDataInfo(params){
	var node=$("#deviceContent_"+params);
	if (node.is(':hidden')) {
		node.show(); 
	}else{
		node.hide();
	}
};

/*以下为历史数据块方法*/

function controlHistoryGasDataDeviceSelect(data){
	var node=$("#historyGasDataDeviceSelectDiv");
	if (node.is(':hidden')) {
		node.show();
	}else{
		node.hide();
	}
};

function getStartCurrentDay(){
	var day = new Date();
	day.setTime(day.getTime());
	var s = day.getFullYear()+"-" + initSt((day.getMonth()+1)) + "-" + initSt(day.getDate())
		+" "+initSt((day.getHours()-1))+":"+initSt(day.getMinutes())+":"+initSt(day.getSeconds());
	return s;
};

function getEndCurrentDay(){
	var day = new Date();
	day.setTime(day.getTime());
	var s = day.getFullYear()+"-" + initSt((day.getMonth()+1)) + "-" + initSt(day.getDate())
	+" "+initSt(day.getHours())+":"+initSt(day.getMinutes())+":"+initSt(day.getSeconds());
	return s;
};
function initSt(m){
	if(m<10){
		return "0"+m;
	}
	return m;
}

function cheackDate(startTime,endTime){/*0成功，1开始时间不能为空，2结束时间不能为空，3开始时间不能大于结束时间，4时间间隔不能超过5天*/
	var result = 0;
	if (startTime=="") {
		result = 1;
		return result;
	}
	if (endTime=="") {
		result = 2;
		return result;
	}
	var sTime = new Date(startTime).getTime();
	var eTime = new Date(endTime).getTime();
	if (sTime>eTime) {
		result = 3;
		return result;
	}
	if ((sTime+24*60*60*1000)<eTime) {
		result = 4;
		return result;
	}
	return result;
};


function qryHistoryGasDataFn(){
	pageNum=1;
	getHistoryGasDataFn();
}

/*获取历史气体数据的信息*/
function getHistoryGasDataFn(){
	var startTime = $("#historyGasDataStartTime").val();
	var endTime = $("#historyGasDataEndTime").val();
	var roomId = $("#gasDataRoomId").val();
	var timeResult = cheackDate(startTime,endTime);/*0成功，1开始时间不能为空，2结束时间不能为空，3开始时间不能大于结束时间，4时间间隔不能超过5天*/
	if (timeResult == 1) {
		layer.msg("开始时间不能为空！");
	}else if(timeResult == 2){
		layer.msg("结束时间不能为空！");
	}else if(timeResult == 3){
		layer.msg("开始时间不能大于结束时间！");
	}else if(timeResult == 4){
		layer.msg("时间间隔不能超过24小时！");
	}else{
		var dNo = $("#historyGasDataDeviceSelectId").text();
		if(dNo=="全部"){
			dNo="";
		}
		var params = {};
	 	params.startTime=startTime;
	 	params.endTime=endTime;
	 	params.deviceSerial=dNo;
	 	params.pageNum = pageNum;
	 	params.pageSize = pageSize;
	 	params.room = roomId;
	 	$.ajax({
	 		type : "post",
	 		url : baseUrl+"/webApi/realTimeData/queryHistoryGasData.jhtml",
	 		data: JSON.stringify(params),
	 		dataType:"json",
	 		headers: {"Content-Type": "application/json;charset=utf-8"},
	 		success : function(data) {	
	 			if(data.httpCode==200){
	 				if(data.data){
	 					var html1 = "";
	 					var html2 = "";
	 					var currentPage = Number(data.current);
						var totalPages = Number(data.pages);
						if(currentPage>=1 && totalPages>1){
							generatePagination(currentPage,totalPages,"historyGasDataPage");
						}else{
							$("#historyGasDataPage").html("");
						}
	 					var list = data.data;
	 					if (list.length >0) {	 						
	 						var tableTrList = $('#historyGasDataTableTr2 > td');
	 						var tableTrIdList = [];
	 						for (var i = 0; i < tableTrList.length; i++) {
	 							tableTrIdList.push(tableTrList[i].id);
							}
							for (var i = 0; i < list.length; i++) {
								 var timeString = list[i].timeString.substring(0,list[i].timeString.length-2);
								 html1+='<tr>';
								 html1+='<td>'+timeString+'</td>';
								 html1+='<td>'+list[i].serial+'</td>';
								 html1+='</tr>';
								 html2+='<tr>';
								 var gasData = list[i].ghdList;
								 for (var j = 0; j < tableTrIdList.length; j++) {
									 var isHave = 0;
									 for (var z = 0; z < gasData.length; z++) {
										 if(gasData[z].gasId==8){
											 isHave = 1;
										 }else{
											 var gasTitleValue = "gasTitle_"+gasData[z].gasId;
											 if (gasTitleValue == tableTrIdList[j]) {
												isHave = 1;
												if (gasData[z].isAlert==2) {
													html2+='<td style="color:#F00;">'+gasData[z].value+'</td>';
												}else{
													html2+='<td>'+gasData[z].value+'</td>';
												}
												
												
											 }
										 }
									 }
									 if (isHave==0) {
										 html2+='<td>/</td>';
									}
								 }
								 html2+='</tr>';
							}
						}
	 					$("#historyGasDataTableTr1").siblings().remove();
	 					$("#historyGasDataTableTr2").siblings().remove();
	 					$("#historyGasDataTable1").append(html1);
	 					$("#historyGasDataTable2").append(html2);
	 				}
	 			}
	 			
	 		},error: function(request) {
	 			layer.msg(networkErrorMsg, {time: 2000});
	 	     }
	 	})
	}
};

/*下载历史气体数据的信息*/
function downLoadHistoryGasDataFn(){
	var startTime = $("#historyGasDataStartTime").val();
	var endTime = $("#historyGasDataEndTime").val();
	var roomId = $("#gasDataRoomId").val();
	var timeResult = cheackDate(startTime,endTime);/*0成功，1开始时间不能为空，2结束时间不能为空，3开始时间不能大于结束时间，4时间间隔不能超过5天*/
	if (timeResult == 1) {
		layer.msg("开始时间不能为空！");
	}else if(timeResult == 2){
		layer.msg("结束时间不能为空！");
	}else if(timeResult == 3){
		layer.msg("开始时间不能大于结束时间！");
	}else if(timeResult == 4){
		layer.msg("时间间隔不能超过24小时！");
	}else{
		var dNo = $("#historyGasDataDeviceSelectId").text();
		if(dNo=="全部"){
			dNo="";
		}
		var params = {};
	 	params.startTime=startTime;
	 	params.endTime=endTime;
	 	params.deviceSerial=dNo;
	 	params.pageNum = pageNum;
	 	params.pageSize = pageSize;
	 	params.room = roomId;
		window.open(baseUrl+"/webApi/realTimeData/downLoadGasDataEXL.jhtml?param="+encodeURIComponent(JSON.stringify(params)));
	}
};

/*获取历史气象图表的信息*/
function getHistoryGasChartFn(param){
	if (param=="qry") {
		param=$("#historyGasChartDl").children().get(1).textContent;
	}
	var startTime = $("#historyGasChartStartTime").val();
	var endTime = $("#historyGasChartEndTime").val();
	var roomId = $('#gasChartRoomId').val();
	var timeResult = cheackDate(startTime,endTime);/*0成功，1开始时间不能为空，2结束时间不能为空，3开始时间不能大于结束时间，4时间间隔不能超过5天*/
	if (timeResult == 1) {
		layer.msg("开始时间不能为空！");
	}else if(timeResult == 2){
		layer.msg("结束时间不能为空！");
	}else if(timeResult == 3){
		layer.msg("开始时间不能大于结束时间！");
	}else if(timeResult == 4){
		layer.msg("时间间隔不能超过24小时！");
	}else{
		var params = {};
	 	params.startTime=startTime;
	 	params.endTime=endTime;
	 	params.deviceSerial=param;
	 	params.room = roomId;
	 	$.ajax({
	 		type : "post",
	 		url : baseUrl+"/webApi/realTimeData/queryHistoryGasChart.jhtml",
	 		data: JSON.stringify(params),
	 		dataType:"json",
	 		headers: {"Content-Type": "application/json;charset=utf-8"},
	 		success : function(data) {	
	 			if(data.httpCode==200){
	 				if(data.data){
	 					var html = "";
	 					var chartData = data.data;
	 					// 基于准备好的dom，初始化echarts实例
	 			        var myChart = echarts.init(document.getElementById('historyGasChartImgDiv'));
	 			        option = {
	 			    		    title: {
	 			    		        text: chartData.title,
	 			    		        textStyle: {
		 			  	                color: '#fff',
		 			  	                fontSize:16,
		 			  	            },
		 			  	            padding: [10, 10]
	 			    		    },
	 			    		    tooltip: {
	 			    		        trigger: 'axis',
	 			    		        backgroundColor:'rgba(255,255,255,0.8)',
	 			  		            borderColor:'gray',
	 			  		        	textStyle:{
	 			  		        		color:'black',
	 			  		        	}
	 			    		    },
	 			    		    legend: {
	 			    		        data:chartData.legendData,
	 			    		        textStyle: {
	 			    		        	color: '#fff',
	 			    		        	fontSize:14
	 			    		        },
	 			    		       padding: [10, 10]
	 			    		    },
	 			    		    grid: {
	 			    		        left: '5%',
	 			    		        right: '5%',
	 			    		        bottom: '5%',
	 			    		        containLabel: true
	 			    		    },
	 			    		    xAxis: {
	 			    		        type: 'category',
	 			    		        boundaryGap: false,
	 			    		        data: chartData.xAxis.data,
	 			    		        axisLabel: {
	 			    		            rotate: 60,
	 			    		        },
	 			    		       axisLabel: {
	 			                      textStyle: {
	 			                          color: '#fff',
	 			                          fontSize:14
	 			                      }
	 			                  },
	 			                  axisLine: {
	 			                      lineStyle: {
	 			                          color: '#fff'	 			                          
	 			                      }  
	 			                  },
	 			    		    },
	 			    		    yAxis: {
	 			    		        type: 'value',
	 			    		        axisLabel: {
	 			  		            textStyle: {
	 			  		                color: '#fff',
	 			  		                fontSize:14,
	 			  		            }
	 			  		        },
	 			  		        axisLine: {
	 			                      lineStyle: {
	 			                          color: '#fff'
	 			                      }  
	 			                  }
	 			    		    },
	 			    		    series: chartData.seriesData
	 			    	};
	 			       // 使用刚指定的配置项和数据显示图表。
	 			        myChart.setOption(option);
	 				}
	 			}
	 			
	 		},error: function(request) {
	 			layer.msg(networkErrorMsg, {time: 2000});
	 	     }
	 	})
	}
};


function qryHistoryWeatherDataFn(){
	pageNum=1;
	getHistoryWeatherDataFn();
}

/*获取历史气象数据的信息*/
function getHistoryWeatherDataFn(){
	var startTime = $("#historyWeatherDataStartTime").val();
	var endTime = $("#historyWeatherDataEndTime").val();
	var roomId = $("#weatherDataRoomId").val();
	var timeResult = cheackDate(startTime,endTime);/*0成功，1开始时间不能为空，2结束时间不能为空，3开始时间不能大于结束时间，4时间间隔不能超过5天*/
	if (timeResult == 1) {
		layer.msg("开始时间不能为空！");
	}else if(timeResult == 2){
		layer.msg("结束时间不能为空！");
	}else if(timeResult == 3){
		layer.msg("开始时间不能大于结束时间！");
	}else if(timeResult == 4){
		layer.msg("时间间隔不能超过24小时！");
	}else{
		var params = {};
	 	params.startTime=startTime;
	 	params.endTime=endTime;
	 	params.pageNum = pageNum;
	 	params.pageSize = pageSize;
	 	params.room = roomId;
	 	$.ajax({
	 		type : "post",
	 		url : baseUrl+"/webApi/realTimeData/queryHistoryWeatherData.jhtml",
	 		data: JSON.stringify(params),
	 		dataType:"json",
	 		headers: {"Content-Type": "application/json;charset=utf-8"},
	 		success : function(data) {	
	 			if(data.httpCode==200){
	 				if(data.data){
	 					var html = "";
	 					var currentPage = Number(data.current);
						var totalPages = Number(data.pages);
						if(currentPage>=1 && totalPages>1){
							generatePagination(currentPage,totalPages,"historyWeatherDataPage");
						}else{
							$("#historyWeatherDataPage").html("");
						}
	 					var list = data.data;
	 					if (list.length >0) {
							for (var i = 0; i < list.length; i++) {
								 var timeString = list[i].timeString.substring(0,list[i].timeString.length-2);
								 html+='<tr>';
								 html+='<td>'+list[i].stationPort+'</td>';
								 html+='<td>'+timeString+'</td>';
								 html+='<td>'+list[i].windSpeed+'m/s</td>';
								 html+='<td>'+list[i].windDirection+'</td>';
								 html+='<td>'+list[i].pmValue+'</td>';
								 html+='<td>'+list[i].temperature+'℃</td>';
								 html+='<td>'+list[i].humidity+'%</td>';
								 html+='<td>'+list[i].pressure+'Pa</td>';
								 html+='<td>'+list[i].noise+'</td>';
								 html+='</tr>';
							}
						}
	 					$("#historyWeatherDataTableTr").siblings().remove();
	 					$("#historyWeatherDataTable").append(html);
	 				}
	 			}
	 			
	 		},error: function(request) {
	 			layer.msg(networkErrorMsg, {time: 2000});
	 	     }
	 	})
	}
};

/*获取历史气象数据的信息*/
function downLoadHistoryWeatherDataFn(){
	var startTime = $("#historyWeatherDataStartTime").val();
	var endTime = $("#historyWeatherDataEndTime").val();
	var roomId = $("#weatherDataRoomId").val();
	var timeResult = cheackDate(startTime,endTime);/*0成功，1开始时间不能为空，2结束时间不能为空，3开始时间不能大于结束时间，4时间间隔不能超过5天*/
	if (timeResult == 1) {
		layer.msg("开始时间不能为空！");
	}else if(timeResult == 2){
		layer.msg("结束时间不能为空！");
	}else if(timeResult == 3){
		layer.msg("开始时间不能大于结束时间！");
	}else if(timeResult == 4){
		layer.msg("时间间隔不能超过24小时！");
	}else{
		var params = {};
	 	params.startTime=startTime;
	 	params.endTime=endTime;
	 	params.pageNum = pageNum;
	 	params.pageSize = pageSize;
	 	params.room = roomId;
	 	window.open(baseUrl+"/webApi/realTimeData/downLoadWeatherDataEXL.jhtml?param="+encodeURIComponent(JSON.stringify(params)));
	}
};

/*获取历史气象图表的信息*/
function getHistoryWeatherChartFn(){
	var startTime = $("#historyWeatherChartStartTime").val();
	var endTime = $("#historyWeatherChartEndTime").val();
	var roomId = $("#weatherChartRoomId").val();
	var timeResult = cheackDate(startTime,endTime);/*0成功，1开始时间不能为空，2结束时间不能为空，3开始时间不能大于结束时间，4时间间隔不能超过5天*/
	if (timeResult == 1) {
		layer.msg("开始时间不能为空！");
	}else if(timeResult == 2){
		layer.msg("结束时间不能为空！");
	}else if(timeResult == 3){
		layer.msg("开始时间不能大于结束时间！");
	}else if(timeResult == 4){
		layer.msg("时间间隔不能超过24小时！");
	}else{
		var params = {};
	 	params.startTime=startTime;
	 	params.endTime=endTime;
	 	params.room = roomId;
	 	$.ajax({
	 		type : "post",
	 		url : baseUrl+"/webApi/realTimeData/queryHistoryWeatherChart.jhtml",
	 		data: JSON.stringify(params),
	 		dataType:"json",
	 		headers: {"Content-Type": "application/json;charset=utf-8"},
	 		success : function(data) {	
	 			if(data.httpCode==200){
	 				if(data.data){
	 					var html = "";
	 					var chartData = data.data;
	 					// 基于准备好的dom，初始化echarts实例
	 			        var myChart = echarts.init(document.getElementById('historyWeacherChartImgDiv'));
	 			        option = {
	 			    		    title: {
	 			    		        text: chartData.title,
	 			    		        textStyle: {
		 			  	                color: '#fff',
		 			  	                fontSize:16,
		 			  	            },
	 			                    padding: [10, 10]
	 			    		    },
	 			    		    tooltip: {
	 			    		        trigger: 'axis',
	 			    		       backgroundColor:'rgba(255,255,255,0.8)',
	 			  		           borderColor:'gray',
	 			  		        	textStyle:{
	 			  		        		color:'black',
	 			  		        	}
	 			    		    },
	 			    		    legend: {
	 			    		        data:chartData.legendData,
	 			    		        textStyle: {
		 			  	                color: '#fff',
		 			  	                fontSize:14,
		 			  	            },
		 			  	            padding: [10, 10]
	 			    		    },
	 			    		    grid: {
	 			    		        left: '3%',
	 			    		        right: '4%',
	 			    		        bottom: '3%',
	 			    		        containLabel: true
	 			    		    },
	 			    		    xAxis: {
	 			    		        type: 'category',
	 			    		        boundaryGap: false,
	 			    		        data: chartData.xAxis.data,
	 			    		        axisLabel: {
	 			    		            rotate: 60,
	 			    		        },
	 			    		       axisLabel: {
		 			                      textStyle: {
		 			                          color: '#fff',
		 			                         fontSize:14,
		 			                      }
		 			                  },
		 			                  axisLine: {
		 			                      lineStyle: {
		 			                          color: '#fff'
		 			                      }  
		 			                  },
	 			    		    },
	 			    		    yAxis: {
	 			    		        type: 'value',
	 			    		       axisLabel: {
	 			  		            textStyle: {
	 			  		                color: '#fff',
	 			  		                fontSize:14,
	 			  		            }
	 			  		        },
	 			  		        axisLine: {
	 			                      lineStyle: {
	 			                          color: '#fff'
	 			                      }  
	 			                  }
	 			    		    },
	 			    		    series: chartData.seriesData
	 			    	};
	 			       // 使用刚指定的配置项和数据显示图表。
	 			        myChart.setOption(option);
	 				}
	 			}
	 			
	 		},error: function(request) {
	 			layer.msg(networkErrorMsg, {time: 2000});
	 	     }
	 	})
	}
};

var deviceMap = new Map();
var weatherMap = new Map();

$(document).ready(function(){	
	/*实时数据,图文数据,历史数据切换选择事件*/
	$('#dataTypeId').on('click','li',function(){
		$(this).addClass("onlike").siblings().removeClass("onlike");//默认选中当前选项
		var index = $(this).index();
		if (index == 0) {
			$("#historyDataDivId").hide();
			$("#picArchiveDataDivId").hide();
			$("#realTimeDataDivId").show();
			if($('.police').width()=='330'){
				$("#spreadDiv").show();
				$("#shrinkBtn").hide();
			}else{
				$("#spreadDiv").hide();
				$("#shrinkBtn").show();
			}
		}
		if (index == 1) {
			$('.police').css('width','330px');
			$('.police').css('left',"");
			$("#spreadDiv").hide();
			$("#historyDataDivId").hide();
			$("#realTimeDataDivId").hide();
			$("#picArchiveDataDivId").show();
		}
		if (index == 2) {
			$('.police').css('width','auto');
			$('.police').css('left','0');
			$("#spreadDiv").hide();
			$("#shrinkBtn").show();
			$("#tuwenshujuLi").hide();
			$("#realTimeDataDivId").hide();
			$("#picArchiveDataDivId").hide();
			$("#historyDataDivId").show();
			$("#historyDataDetailUlId").each(function(){
			      $(this).find("li").first().addClass("onAcTive").siblings().removeClass("onAcTive");
			});
			$("#gasHistoryDataDivId").show();
			$("#gasHistoryChartDivId").hide();
			$("#weatherHistoryDataDivId").hide();
			$("#weatherHistoryChartDivId").hide();
			$("#videoHistoryDataDivId").hide();
			$("#historyGasDataStartTime").val(getStartCurrentDay());
			$("#historyGasDataEndTime").val(getEndCurrentDay());
			pageNum=1; 
			pageSize=10;
			$("#historyGasDataPage").show();
			getHistoryGasDataFn();
		}
	});
	
	/*历史数据下选项卡切换选择事件*/
	$('#historyDataDetailUlId').on('click','li',function(){
		$(this).addClass("onAcTive").siblings().removeClass("onAcTive");//默认选中当前选项
		var index = $(this).index();
		if (index == 0) {
			$("#gasHistoryDataDivId").show();
			$("#gasHistoryChartDivId").hide();
			$("#weatherHistoryDataDivId").hide();
			$("#weatherHistoryChartDivId").hide();
			$("#videoHistoryDataDivId").hide();
			$("#historyGasDataStartTime").val(getStartCurrentDay());
			$("#historyGasDataEndTime").val(getEndCurrentDay());
			pageNum=1; 
			pageSize=10;
			$("#historyGasDataPage").html("");
			$("#historyGasDataPage").show();
			getHistoryGasDataFn();
		}
		if (index == 1) {
			$("#gasHistoryDataDivId").hide();
			$("#gasHistoryChartDivId").show();
			$("#weatherHistoryDataDivId").hide();
			$("#weatherHistoryChartDivId").hide();
			$("#videoHistoryDataDivId").hide();
			$("#historyGasChartStartTime").val(getStartCurrentDay());
			$("#historyGasChartEndTime").val(getEndCurrentDay());
			getHistoryGasChartFn("qry");
		}
		if (index == 2) {
			$("#gasHistoryDataDivId").hide();
			$("#gasHistoryChartDivId").hide();
			$("#weatherHistoryDataDivId").show();
			$("#weatherHistoryChartDivId").hide();
			$("#videoHistoryDataDivId").hide();
			$("#historyWeatherDataStartTime").val(getStartCurrentDay());
			$("#historyWeatherDataEndTime").val(getEndCurrentDay());
			pageNum=1; 
			pageSize=10;
			$("#historyWeatherDataPage").html("");
			$("#historyWeatherDataPage").show();
			getHistoryWeatherDataFn();
		}
		if (index == 3) {
			$("#gasHistoryDataDivId").hide();
			$("#gasHistoryChartDivId").hide();
			$("#weatherHistoryDataDivId").hide();
			$("#weatherHistoryChartDivId").show();
			$("#videoHistoryDataDivId").hide();
			$("#historyWeatherChartStartTime").val(getStartCurrentDay());
			$("#historyWeatherChartEndTime").val(getEndCurrentDay());
			getHistoryWeatherChartFn();
		}
		if (index == 4) {
			$("#gasHistoryDataDivId").hide();
			$("#gasHistoryChartDivId").hide();
			$("#weatherHistoryDataDivId").hide();
			$("#weatherHistoryChartDivId").hide();
			$("#videoHistoryDataDivId").show();
			$("#historyVideoStartTime").val(getStartCurrentDay());
			$("#historyVideoEndTime").val(getEndCurrentDay());
			
			//获取视频数据
			getHistoryVideoFn();
		}
	});
	
	/*设备配置中切换设备选择事件*/
	$('#deviceDetectorListId').on('click','li',function(){
		var gdId = $(this).find("span:eq(0)").attr("id");
		$("#deviceDetectorId").val(gdId);
		$.ajax({
			type : "post",
			url : baseUrl+"/webApi/realTimeData/qryDeviceConfigure.jhtml",
			dataType:"json",
			headers: {"Content-Type": "application/json;charset=utf-8"},
			ladeview:false,
			success : function(data) {	
				if(data.httpCode==200){
					if(data.data){
						if (!$.isEmptyObject(data.data.gasDetectorDataVo) && data.data.gasDetectorDataVo.length>0) {
							var gddData = data.data.gasDetectorDataVo;
							for (var i = 0; i < gddData.length; i++) {
								if (gddData[i].gasDetectorId == gdId) {
									if (gddData[i].gasName!=null && gddData[i].gasName!=null != "") {
										$("#tongdaoValue_"+gddData[i].detectorPosition).text(gddData[i].gasName);
									}else{
										$("#tongdaoValue_"+gddData[i].detectorPosition).text("未设置");
									}
									
									$("#tongdaoId_"+gddData[i].detectorPosition).val(gddData[i].gasId);
									$("#gddId_"+gddData[i].detectorPosition).val(gddData[i].id);
								}
							}
						}
					}
				}
			},error: function(request) {
				layer.msg(networkErrorMsg, {time: 2000});
	        }
		})  
		$(this).addClass("selectOn").siblings().removeClass("selectOn");//默认选中当前选项	
		
	});
	
	/*给串口的li绑定事件*/
	$('#allPortDiv').on('click','li',function(){
		if($(this).text()){
		   $("#serialPortNo").text($(this).text()); 
		}			
		$("#allPortDiv").hide();   
	});
	
	/*给气体类型的li绑定事件*/
	$('#tongdao_1').on('click','li',function(){
		$("#tongdaoId_1").val($(this).attr("name"));
		if($(this).text()){
		   $("#tongdaoValue_1").text($(this).text()); 
		}			
		$("#tongdao_1").hide();   
	});
	$('#tongdao_2').on('click','li',function(){
		$("#tongdaoId_2").val($(this).attr("name"));
		if($(this).text()){
		   $("#tongdaoValue_2").text($(this).text()); 
		}			
		$("#tongdao_2").hide();   
	});
	$('#tongdao_3').on('click','li',function(){
		$("#tongdaoId_3").val($(this).attr("name"));
		if($(this).text()){
		   $("#tongdaoValue_3").text($(this).text()); 
		}			
		$("#tongdao_3").hide();   
	});
	$('#tongdao_4').on('click','li',function(){
		$("#tongdaoId_4").val($(this).attr("name"));
		if($(this).text()){
		   $("#tongdaoValue_4").text($(this).text()); 
		}			
		$("#tongdao_4").hide();   
	});
	$('#tongdao_5').on('click','li',function(){
		$("#tongdaoId_5").val($(this).attr("name"));
		if($(this).text()){
		   $("#tongdaoValue_5").text($(this).text()); 
		}			
		$("#tongdao_5").hide();   
	});
	$('#tongdao_6').on('click','li',function(){
		$("#tongdaoId_6").val($(this).attr("name"));
		if($(this).text()){
		   $("#tongdaoValue_6").text($(this).text()); 
		}			
		$("#tongdao_6").hide();   
	});
	$('#tongdao_7').on('click','li',function(){
		$("#tongdaoId_7").val($(this).attr("name"));
		if($(this).text()){
		   $("#tongdaoValue_7").text($(this).text()); 
		}			
		$("#tongdao_7").hide();   
	});

	/*给气体历史数据设备的li绑定事件*/
	$('#historyGasDataDeviceSelectDiv').on('click','li',function(){
		$("#historyGasDataDeviceSelectDiv").val($(this).attr("name"));
		if($(this).text()){
		   $("#historyGasDataDeviceSelectId").text($(this).text()); 
		}			
		$("#historyGasDataDeviceSelectDiv").hide();   
	});
	
	/*给气体历史图表设备的dd绑定事件*/
	$('#historyGasChartDeviceDiv').on('click','dd',function(){
		getHistoryGasChartFn($(this).attr("name"));
	});
	
	var intervalTime=$("#timeSpaceInput").val();
	if(intervalTime=="" || intervalTime<300){
		intervalTime = 300*1000;
	}else{
		intervalTime = intervalTime*1000*2;
	}
	setInterval("circulateExecute();",intervalTime);
	
	$('span[name="deviceIcon"]').each(function(){
		deviceMap.set($(this).attr("id"),new Date().getTime());
	});
	
	alarmAudio = document.getElementById("alarm_audio");
	
	$("#cb_alarm_audio").change(function(){
		if(!$(this).is(':checked')){
			if(!alarmAudio.paused){
				alarmAudio.pause();
			}
		}
	});
	
});


function circulateExecute(){
	var currentTime = new Date().getTime();
	var intervalTime=$("#timeSpaceInput").val();
	if(intervalTime=="" || intervalTime<120){
		intervalTime = 120*1000;
	}else{
		intervalTime = intervalTime*1000;
	}
	var num=0;
	
	var deferred = $.Deferred();
	$('span[name="deviceIcon"]').each(function(){
		if(currentTime-deviceMap.get($(this).attr("id"))>intervalTime){
			$(this).css('background','#999');
			var deviceContentId = $(this).attr("id").replace("deviceSpan_","");
			$("#deviceContent_"+deviceContentId).find('span[class="dateNumber"]').text("");
			$("#deviceContent_"+deviceContentId).find('li').css('background','none');
			$("#deviceContent_"+deviceContentId).find('li').css('color','#fff');
			$("#battery_"+deviceContentId).text('');
			global_realtimeData.delete(deviceContentId);
			num++
		}
		deferred.resolve();
	});
	
	if (currentTime - weatherMap.get("weatherDev")>intervalTime) {
		$("#realTimeWindSpeed").text('');
		 $("#realTimeWindDirection").text('');
		 $("#realTimePmValue").text('');
		 $("#realTimeTemperature").text('');
		 $("#realTimeHumidity").text('');
		 $("#realTimePressure").text('');
		 $("#realTimeNoise").text('');
	}
	
	deferred.done(function(){
		if(num==deviceMap.size){
			$('#alarmOpenMove').attr('alarm','false');
			if(!alarmAudio.paused){
				alarmAudio.pause();
			}
		}
	});
}

function getHistoryVideoFn(){
	$.ajax({
		type : "post",
		url : baseUrl+"/webApi/camera/qryCameraFile.jhtml",
		dataType:"json",
		data: JSON.stringify({
			 'startTime': $("#historyVideoStartTime").val(),
			 'endTime': $("#historyVideoEndTime").val(),
			 'ip':$("#historyVideoIp").val(),
		 }),
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				if(data.data){
					var html="";
					if (data.data!=null&& data.data.length>0) {
						var videoData = data.data;
						for (var i = 0; i < videoData.length; i++) {
							html+="<tr><td>"+(i+1)+"</td>"
								+"<td>"+videoData[i].time+"</td>"
								+"<td>"+videoData[i].ip+"</td>"
								+"<td>"+videoData[i].fileName+"</td>"
//								+"<td><a href='"+offLineMapUrl+"/camera/"+videoData[i].dir+"/"+videoData[i].path+"' target='_blank' class='delA'>查看</a>&nbsp;&nbsp;"
								+"<td><a href='"+videoData[i].dirPath+"' target='_blank' class='delA'>查看</a>&nbsp;&nbsp;"
								+"<a href='javascript:void(0);' onClick='removeRaido(\""+videoData[i].dir+"\",\""+videoData[i].path+"\")' class='delA'>删除</a></td></tr>";
						}
					}
					$("#historyVideoBody").html(html);
				}
			}
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	}) 
}
function removeRaido(dir,path){
	$.ajax({
		type : "post",
		url : baseUrl+"/webApi/camera/deleeteCameraFile.jhtml",
		dataType:"json",
		data: JSON.stringify({'dir':dir,'path':path}),
		ladeview:false,
		headers: {"Content-Type": "application/json;charset=utf-8"},
		success : function(data) {	
			if(data.httpCode==200){
				getHistoryVideoFn();
			}
		},error: function(request) {
			layer.msg(networkErrorMsg, {time: 2000});
        }
	}) 
}






