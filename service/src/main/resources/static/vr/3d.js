﻿

var x3d_browser = null;
var x3d_context = null;
var listener = null;

function setNodeField(nodeName, fieldName, v) {
    if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
    if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
    var node = x3d_context.getNode(nodeName);
    if (node) {
        var field = node.getField(fieldName);
        if (field) {
            field.value = v;
            //field.dispose();
        }
        //node.dispose();
    }
}
function getNodeField(nodeName, fieldName)
{
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var scriptnode = x3d_context.getNode(nodeName);
	if (scriptnode)
	{
		var field = scriptnode.getField(fieldName);
		if (field)
		{
			return field;
		}
	}
	return null;
}

// 初始化控件接口
function initAjax3d()
{
	if (x3d_browser==null)
	{
		x3d_browser = document.PowerSpotLight.getBrowser();	 
		x3d_context = x3d_browser.getExecutionContext();
		if (x3d_browser != null)
		{
			listener = new Object();
			listener.browserChanged = contextChanged;
			x3d_browser.registerBrowserInterest("AddBrowserInterest", listener);
		}
	}
}
// 回调函数，自动更新 browser
function contextChanged(evt)
{
	if (evt == 0)		
	{
		x3d_browser = document.PowerSpotLight.getBrowser();	 
		x3d_context = x3d_browser.getExecutionContext();
	}
}

// 恢复相机
function reset_viewpoint()
{
	switchOperationMode("EAGLEEYE");
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	x3d_browser.gotoGeoViewpoint(46.898776,87.456808,3143.296612,-63.560402,-0.019026,0.001144,1.57,1);
}

// 切换操作模式，name="EAGLEEYE","EXAMINE","WALK","FLY","NONE"
function switchOperationMode(name)
{
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var navnode = x3d_context.getNode("RtnavNavigationInfo");
	if (navnode)
	{
		navnode.type[0] = name;
	}
}

//启动相机动画
function cameraAnimation(name)
{
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var mark_scriptnode = x3d_context.getNode("interface_script");

	if (mark_scriptnode)
	{
		var camera = mark_scriptnode.getField("cameraAnimation");
		if (camera)
			camera.value = name;
	}
}

// 暂停继续相机动画
// resume : resume = true
// pause : resume = false
function resumeCameraAnimation(resume)
{
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var mark_scriptnode = x3d_context.getNode("interface_script");
	if (mark_scriptnode)
	{
		var camera = mark_scriptnode.getField("cameraResume");
		if (camera)
			camera.value = resume;
	}
}

// 模型闪烁, name 模型名称
function flashObject(name)
{
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var scriptnode = x3d_context.getNode("interface_script");
	if (scriptnode)
	{
		var field = scriptnode.getField("flashObject");
		if (field)
		{
			field.value = name;
		}
	}
}

// 显示或隐藏一个节点, name 节点名称, show 布尔变量（true 显示, false 隐藏）
function showNode(name, show)
{
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var scriptnode = x3d_context.getNode("interface_script");
	if (scriptnode)
	{
		var field = null;
		if (show)
		{
			field = scriptnode.getField("showObject");
		}
		else
		{
			field = scriptnode.getField("hideObject");
		}
		if (field)
		{
			field.value = name;
		}
	}
}

// 获取相机的位置和方向
var cameraPosition = null;
var cameraOrientation = null;
function getCamera()
{
	if (cameraPosition == null || cameraOrientation == null)
	{
		if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
		if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
		var scriptnode = x3d_context.getNode("interface_script");
		if (scriptnode)
		{
			if (cameraPosition == null)
			{
				cameraPosition = scriptnode.getField("cameraPosition");
			}
			if (cameraOrientation == null)
			{
				cameraOrientation = scriptnode.getField("cameraOrientation");
			}
		}
	}
	//alert("x : "+cameraPosition.x+", y : "+cameraPosition.y+", z : "+cameraPosition.z);
	//alert("x : "+cameraOrientation.x+", y : "+cameraOrientation.y+", z : "+cameraOrientation.z+", angle : "+cameraOrientation.angle);
	return cameraPosition.x + "," + cameraPosition.y + "," + cameraPosition.z + "," + 
	cameraOrientation.x + "," + cameraOrientation.y + ","+cameraOrientation.z + "," + cameraOrientation.angle;
}

// 获取鼠标在屏幕上的位置
var mousePos = null;
function getMousePos()
{
	if (mousePos == null)
	{
		if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
		if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
		var scriptnode = x3d_context.getNode("interface_script");
		if (scriptnode)
		{
			mousePos = scriptnode.getField("mousePos");
		}
	}
	return mousePos;
}

// 获取鼠标点击的位置
var pickPoint = null;
function getMousePickPoint()
{
	if (pickPoint == null)
	{
		if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
		if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
		var scriptnode = x3d_context.getNode("interface_script");
		if (scriptnode)
		{
			pickPoint = scriptnode.getField("pickPoint");
		}
	}
	//alert("x : "+pickPoint.x+", y : "+pickPoint.y+", z : "+pickPoint.z);
	return pickPoint.x + "," + pickPoint.y+"," + pickPoint.z;
}

// 获取鼠标点击的模型名称
var pickName = null;
function getMousePickName()
{
	if (pickName == null)
	{
		if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
		if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
		var scriptnode = x3d_context.getNode("interface_script");
		if (scriptnode)
		{
			pickName = scriptnode.getField("pickName");
		}
	}
	//alert("pickName : "+pickName);
	return pickName.value;
}

// 相机切换到指定的位置和方向
// fov 相机视角宽度, 一般取0.785398
// mode 模式, 0 相机平滑过渡, 1 相机直接切换
function gotoViewpoint(posX, posY, posZ, rotX, rotY, rotZ, rotAngle, fov, mode)
{
	resumeCameraAnimation(false);
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	x3d_browser.gotoViewpoint(posX, posY, posZ, rotX, rotY, rotZ, rotAngle, fov, mode);
}
// str="posX,posY,posZ,rotX,rotY,rotZ,rotAngle,fov,mode"
// fov 可以默认值取 0.785398，mode 可以默认值取 0
function gotoViewpoint2(str)
{
	var strArray = str.split(" ");
	var posX = parseFloat(strArray[0]);
	var posY = parseFloat(strArray[1]);
	var posZ = parseFloat(strArray[2]);
	var rotX = parseFloat(strArray[3]);
	var rotY = parseFloat(strArray[4]);
	var rotZ = parseFloat(strArray[5]);
	var rotAngle = parseFloat(strArray[6]);
	var fov = 0.785398;
	var mode = 0;
	if (strArray.length > 8)
	{
		fov = parseFloat(strArray[7]);
		mode = parseFloat(strArray[8]);
	}
	resumeCameraAnimation(false);
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	x3d_browser.gotoViewpoint(posX, posY, posZ, rotX, rotY, rotZ, rotAngle, fov, mode);
}

// 视点跳转到模型附近
// x, y, z 视点距离模型的偏移 默认值取 0
// x_a, y_a, z_a 视点的绕三个坐标轴的旋转角度 默认值取 0
function gotoModel(name, x, y, z, x_a, y_a, z_a, jump)
{
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var scriptnode = x3d_context.getNode("interface_script");
	if (scriptnode)
	{
		var field = scriptnode.getField("gotoModel");
		if (field)
		{
			resumeCameraAnimation(false);
			field.value = name + ";" + x + ";" + y + ";" + z + ";" + x_a + ";" + y_a + ";" + z_a + ";" + jump;
		}
	}
}

// 视点跳转到模型附近
function gotoModel2(str)
{ 
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var scriptnode = x3d_context.getNode("interface_script");
	if (scriptnode)
	{
		var field = scriptnode.getField("gotoModel");
		if (field)
		{
			var strArray = str.split(",");
			var name=strArray[0];
			var x,y,z,x_a,y_a,z_a=0;
			field.value = name + ";" + x + ";" + y + ";" + z + ";" + x_a + ";" + y_a + ";" + z_a;
		}
	}
}

//相机跳转, 使传入的两个点都在视野范围内, str = "x1,y1,z1,x2,y2,z2"
function lookTwoPoint(str)
{
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var scriptnode = x3d_context.getNode("markScript");
	if (scriptnode)
	{
		var field = scriptnode.getField("lookTwoPoint");
		if (field)
		{
			field.value = str;
		}
	}
}

function setWhichChoice(name, whichChoice)
{
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var scriptnode = x3d_context.getNode(name);
	if (scriptnode)
	{
		var field = scriptnode.getField("whichChoice");
		if (field)
		{
			field.value = whichChoice;
		}
	}
}

//测量距离
function measureDistanceAction(e){
	
	setNodeField("SmeasureD", "coefficient", 330);
	
	//changeAColor(e);
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var node = x3d_context.getNode("SmeasureD"); 
	if (node)
	{
		var field = node.getField("measureDistance");
		if (field)
		{
			field.value="true";
		}
	}
}

// 测量高度
function measureHeightAction(e){
	//changeAColor(e);
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var node = x3d_context.getNode("SmeasureH"); 
	if (node)
	{
		var field = node.getField("measureHeight");
		if (field)
		{
			field.value="true";
		}
	}
}

//测量面积
function measureAreaAction(e){

	setNodeField("SmeasureA", "coefficient", 330);

	//changeAColor(e);
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var node = x3d_context.getNode("SmeasureA"); 
	if (node)
	{
		var field = node.getField("measureArea");
		if (field)
		{
			field.value="true";
		}
	}
}

//清除测量划线
function delMeasureLineAction(e){
	//changeAColor(e);
	// 清除测量画线
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var node = x3d_context.getNode("SmeasureD"); 
	if (node)
	{
		var field = node.getField("del");
		if (field)
		{
			field.value="true";
		}
	}
	node = x3d_context.getNode("SmeasureH"); 
	if (node)
	{
		var field = node.getField("del");
		if (field)
		{
			field.value="true";
		}
	}
	node = x3d_context.getNode("SmeasureA"); 
	if (node)
	{
		var field = node.getField("del");
		if (field)
		{
			field.value="true";
		}
	}
}

// 生成一个用户标签, 参数"id,targetname,x,y,z,url,name,size;parent,parent,..."
// id 是标签id, targetname 是标签挂接的模型名称, xyz 是标签位置, url 是标签图标, name 是用户输入的标签名称
// parent,parent,... 是父节点的嵌套关系
function createUserLabel(str)
{
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var scriptnode = x3d_context.getNode("label_script");
	if (scriptnode)
	{
		var field = scriptnode.getField("createUserLabel");
		if (field)
		{
			field.value = str;
		}
	}
}

// 批量生成用户标签, 参数"labelstr|labelstr", 每一个 labelstr 结构和 createUserLabel 的参数一样
function createUserLabels(str)
{
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var scriptnode = x3d_context.getNode("label_script");
	if (scriptnode)
	{
		var field = scriptnode.getField("createUserLabels");
		if (field)
		{
			field.value = str;
		}
	}
}

// 删除所有用户标签
function removeAllUserLabels()
{
	setNodeField("label_script", "removeAllUserLabels", true);
}

// 生成一个搜索结果显示标签, 参数"id,targetname,url,name,x,y,z"
// id 是标签id, targetname 是标签挂接的模型名称, url 是标签图标, name 是用户输入的标签名称
function createSearchLabel(str)
{
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var scriptnode = x3d_context.getNode("label_script");
	if (scriptnode)
	{
		var field = scriptnode.getField("createSearchLabel");
		if (field)
		{
			field.value = str;
		}
	}
}

// 批量生成搜索结果显示标签, 参数"labelstr|labelstr", 每一个 labelstr 结构和 createSearchLabel 的参数一样
function createSearchLabels(str)
{
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var scriptnode = x3d_context.getNode("label_script");
	if (scriptnode)
	{
		var field = scriptnode.getField("createSearchLabels");
		if (field)
		{
			field.value = str;
		}
	}
}

function searchModel(name)
{
	var id = name;
	var target_name = name;
	var png = "1112.png";
	var equName = name;
	var str = id + "," + target_name + "," + png + "," + equName + "," + "1474.352417,2.325627,-60.619015";
	
	createSearchLabel(str);
}

function deleteLabel(id)
{
	alert(id);
}

// 删除一个标签
function removeLabel(id)
{
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var scriptnode = x3d_context.getNode("label_script");
	if (scriptnode)
	{
		var field = scriptnode.getField("removeLabel");
		if (field)
		{
			field.value = id;
		}
	}
}

// 删除 n 个标签, ids = "id|id|id|......"
function removeLabels(ids)
{
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var scriptnode = x3d_context.getNode("label_script");
	if (scriptnode)
	{
		var field = scriptnode.getField("removeLabels");
		if (field)
		{
			field.value = id;
		}
	}
}

// 删除所有搜索标签
function removeAllSearchLabels()
{
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var scriptnode = x3d_context.getNode("label_script");
	if (scriptnode)
	{
		var field = scriptnode.getField("removeAllSearchLabels");
		if (field)
		{
			field.value = true;
		}
	}
}

var click3d_observer = null;
var mark_label = false;
var has_mark = false;
var mark_label2 = false;
var has_mark2 = false;
var mark_gas = false;

function click3D(v)
{
	// 标注一个临时标签
	if (mark_label)
	{
		has_mark = true;
		var label_pos = getMousePickPoint();
		var target_name = getMousePickName();
		var str = label_pos + "," + target_name;
		setNodeField("label_script", "set_circle_fire_pos_radius", true);
		setNodeField("label_script", "setMarkLabelPos", str);
	}
	else if (mark_label2)
	{
		has_mark2 = true;
		var label_pos = getMousePickPoint();
		var target_name = getMousePickName();
		var str = label_pos + "," + target_name;
		setNodeField("label_script", "setMarkLabelPos2", str);
	}
	else if (mark_gas)
	{
		mark_gas = false;
		// 调页面上 gas()
	}
}

function add3DClick()
{
	has_mark = false;
	has_mark2 = false;
	if (click3d_observer != null)
	{
		return;
	}
	
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var node = x3d_context.getNode("interface_script");
	if (node)
	{
		var pick_changed = node.getField("pick_changed");
		if (pick_changed)
		{
			click3d_observer = new Object;
			click3d_observer.fieldChanged = click3D;
			click3d_observer.field = pick_changed;
			pick_changed.registerFieldInterest("AddInterest", click3d_observer);
		}
	}
}

function beginMarkGas()
{
	mark_gas = true;
	add3DClick();
}

// 监听的变量变化时，调用次函数
var field_observer = null;
function field_changed(v)
{
	// 标注一个临时标签
	if (v.value == 1)
	{
		// 暂停
	}
	else if (v.value == 2)
	{
		// 继续
	}
}
// 监听变量，暂停、继续
function listenField()
{
	if (field_observer != null)
	{
		return;
	}

	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var node = x3d_context.getNode("button_ui_script");
	if (node)
	{
		var field = node.getField("button_state_changed");
		if (field)
		{
			field_observer = new Object;
			field_observer.fieldChanged = field_changed;
			field_observer.field = field;
			field.registerFieldInterest("AddInterest", field_observer);
		}
	}
}

// 设置标签种类
function markLabelPng(png)
{
	mark_label = true;
	add3DClick();
	setNodeField("label_script", "markLabelPng", png);
}
function markLabelPng2(png)
{
	mark_label2 = true;
	add3DClick();
	setNodeField("label_script", "markLabelPng2", png);
}

// 结束标注标签
function stopMarkLabel()
{
	mark_label = false;
	setNodeField("label_script", "stopMarkLabel", true);
}
function stopMarkLabel2()
{
	mark_label2 = false;
	setNodeField("label_script", "stopMarkLabel2", true);
}

// 保存标签示例
function savelabel()
{
	stopMarkLabel();

	var name = "医院";
	var label_pos = getMousePickPoint();
	var target_name = getMousePickName();
	var png = "1112.png";
	
	// "id,targetname,x,y,z,url,name;parent,parent,..."
	var str = "id1111," + target_name + "," + label_pos + "," + png + "," + name + ",12;" + "hospital";
	
	str = "id1111,Line92630,730.6422119140625,-3.5495338439941406,711.371826171875,1112.png,医院;hospital";
	
	createUserLabel(str);
}

function removeModel(name)
{
	setNodeField("interface_script", "removeModel", name);
}

//////////////////////////////////////////////////////////////

function LoadEmission(lat, lng, name, playSpeed) {
	loadEmissionBinFile((lat - 0.0007) + "," + (lng - 0.001) + "," + name + ",25," + playSpeed);
}

//气体扩散计算完成，开始粒子系统展示
function loadEmissionBinFile(str) {
    if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
    if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
    var scriptnode = x3d_context.getNode("ps_script");
    if (scriptnode) {
        var field = scriptnode.getField("setPS");
        if (field) {
            field.value = str;
        }
    }
	//showNode("qiti_tran", true);
}
function setAndShowLabel(str) {
    if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
    if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
    var scriptnode = x3d_context.getNode("label_script");
    if (scriptnode) {
        var field = scriptnode.getField("setAndShowLabel");
        if (field) {
            field.value = str;
        }
    }
}
// 显示或隐藏一个标签节点, id 标签名称, show 布尔变量（true 显示, false 隐藏）
function showLabel(id, show)
{
	if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
	if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
	var scriptnode = x3d_context.getNode("label_script");
	if (scriptnode)
	{
		var field = null;
		if (show)
		{
			field = scriptnode.getField("showLabel");
		}
		else
		{
			field = scriptnode.getField("hideLabel");
		}
		if (field)
		{
			field.value = id;
		}
	}
}

//气体扩散停止
function stopPS(t) {
    if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
    if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
    var scriptnode = x3d_context.getNode("ps_script");
    if (scriptnode) {
        var field = scriptnode.getField("stopPS");
        if (field) {
            field.value = t;
        }
    }
}

// 获取相机的GEO位置
var geo_camera_pos = null;
function getCamera()
{
	if (geo_camera_pos == null)
	{
		if (x3d_browser == null) x3d_browser = document.PowerSpotLight.getBrowser();
		if (x3d_context == null) x3d_context = x3d_browser.getExecutionContext();
		var scriptnode = x3d_context.getNode("CPWorld_script");
		if (scriptnode)
		{
			if (geo_camera_pos == null)
			{
				geo_camera_pos = scriptnode.getField("geo_camera_pos");
			}
		}
	}
	return cameraPosition.x + "," + cameraPosition.y + "," + cameraPosition.z; 
}

function gotoGeoMapLL(lat, lng)
{
	setNodeField("CPWorld_script", "gotoGeoMapLL", ""+lat+","+lng);
}
function gotoGeoMapLevel(level)
{
	setNodeField("CPWorld_script", "gotoGeoMapLevel", level);
}


// 释放控件接口
function closeAjax3d()
{
	x3d_browser = null;
	x3d_context = null;
	listener = null;
	cameraPosition = null;
	cameraOrientation = null;
	mousePos = null;
	pickPoint = null;
	pickName = null;
	click3d_observer = null;
	mark_label = false;
	message_observer = null;
	connected_observer = null;
	pickWndPos = null;
	label_circle_observer = null;
	
	oil_start_pos = null;
	oil_end_pos = null;
	bomb_pos = null;
	fire_pos = null;
	circle_fire_pos = null;
	light_params = null;
}
