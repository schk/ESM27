FROM registry.docker-cn.com/library/java:8-jre
MAINTAINER Yu Zhantao <yuzhantao@qq.com>
ARG JAR_FILE
ADD ${JAR_FILE} /apps
WORKDIR /apps/
EXPOSE 12686
ENTRYPOINT ["java","-jar","./ROOT.war"]