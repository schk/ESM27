var map,plot,feature,icon;

$(document).ready(function(){

     // 自定义分辨率和瓦片坐标系
     var resolutions = [];
     var maxZoom = 18;
  
     // 计算百度使用的分辨率
     for (var i = 0; i <= maxZoom; i++) {
         resolutions[i] = Math.pow(2, maxZoom - i);
     }
     var tilegrid = new ol.tilegrid.TileGrid({
         origin: [0, 0],
         resolutions: resolutions    // 设置分辨率
     });
  
     // 创建百度地图的数据源
     var baiduSource = new ol.source.TileImage({
         projection: 'EPSG:3857',
         tileGrid: tilegrid,
         tileUrlFunction: function (tileCoord, pixelRatio, proj) {
             var z = tileCoord[0];
             var x = tileCoord[1];
             var y = tileCoord[2];
  
             // 百度瓦片服务url将负数使用M前缀来标识
             if (x < 0) {
                 x = -x;
             }
             if (y < 0) {
                 y = -y;
             }
  
             return "http://127.0.0.1/tile/" + z + "/" + x + "/" + y + ".png";
         }
     });
  
     // 百度地图层
     var baiduMapLayer2 = new ol.layer.Tile({
         source: baiduSource
     });

     // 比例线
     var scaleLineControl = new ol.control.ScaleLine({className:'ol-scale-line'});

     // 创建地图
     map =new ol.Map({
        controls: ol.control.defaults({
            attributionOptions: {
              collapsible: false
            },
            zoom: false
          }).extend([
            scaleLineControl
         ]),
         layers: [
             baiduMapLayer2
         ],
         view: new ol.View({
             // 设置成都为地图中心
             center: ol.proj.transform([125.020124,46.604929], 'EPSG:4326', 'EPSG:3857'),
             zoom: 9,
             minZoom: 3,
             maxZoom: 18,
         }),
         target: 'map'
     });


     //测距 面积
     var MeasureTool = new ol.control.MeasureTool({
        sphereradius : 6378137,//sphereradius
     });

     map.addControl(MeasureTool)

     //地图点击事件
     map.on('click', function (event) {
        console.log(event)
        feature = map.forEachFeatureAtPixel(event.pixel, function (feature) {
          return feature
        })
        if (feature && feature.get('isPlot') && !plot.plotDraw.isDrawing()) {
          plot.plotEdit.activate(feature);
        } else {
          plot.plotEdit.deactivate();
        }
    });

    plot = new olPlot(map, {
        zoomToExtent: true
    })
    

    // 绘制结束后，添加到FeatureOverlay显示。
    function onDrawEnd (event) {
        var feature = event.feature;
        // 开始编辑
        plot.plotEdit.activate(feature);
    }

    plot.plotDraw.on('drawEnd', onDrawEnd)
    plot.plotDraw.on('active_textArea', function (event) {
        var style = plot.plotUtils.getPlotTextStyleCode(event.overlay)
        console.log(style)
    })

    // var vienna = new ol.Overlay({
    //     position: ol.proj.fromLonLat([125.020124,46.604929]),
    //     element: $('<img src="images/mee-01.png" />')[0]
    //   });
    // map.addOverlay(vienna);
    
});

// 指定标绘类型，开始绘制。
function activate (type) {
    plot.plotEdit.deactivate();
    plot.plotDraw.active(type);
}

// 删除选中标绘
function remove () {
    if (feature && feature.get('isPlot') && !plot.plotDraw.isDrawing()) {
        plot.plotUtils.removeFeatures(feature);
        plot.plotEdit.deactivate();
    }
    //plot.plotEdit.remove();
}

// 重新移动到地图初始位置。
function moveCenter () {
    map.getView().animate({zoom: 9}, {center: ol.proj.transform([125.020124,46.604929], 'EPSG:4326', 'EPSG:3857')});
}

// 放大地图
function zoom(z){
    
    if(z == 1){
        map.getView().animate({zoom: map.getView().getZoom() + 1});
    }else{
        map.getView().animate({zoom: map.getView().getZoom() - 1});
    }
}

// 添加Maker
function addMaker(type){
    alert(type);
    switch(type)
    {
        case '1':
            icon = "images/mee-01.png";
            break;
        case '2':
            icon = "images/mee-02.png";
            break;
        case '3':
            icon = "images/mee-03.png";
            break;
        case '4':
            icon = "images/mee-04.png";
            break;
        case '5':
            icon = "images/mee-05.png";
            break;
        case '6':
            icon = "images/mee-06.png";
            break;
        case '7':
            icon = "images/mee-07.png";
            break;
        case '8':
            icon = "images/mee-08.png";
            break;
        case '9':
            icon = "images/mee-09.png";
            break;
        case '10':
            icon = "images/mee-10.png";
            break;
        case '11':
            icon = "images/mee-11.png";
            break;
        case '12':
            icon = "images/mee-12.png";
            break;
        case '13':
            icon = "images/mee-13.png";
            break;
        case '14':
            icon = "images/mee-14.png";
            break;
        case '15':
            icon = "images/mee-15.png";
            break;
        case '16':
            icon = "images/mee-16.png";
            break;
        case '17':
            icon = "images/mee-17.png";
            break;
        case '18':
            icon = "images/mee-18.png";
            break;
        case '19':
            icon = "images/mee-19.png";
            break;
    }
    activate('TextArea');
}

//截图
function camera(){
    map.once('postcompose', function(event) {
        var canvas = event.context.canvas;
        if (navigator.msSaveBlob) {
          navigator.msSaveBlob(canvas.msToBlob(), 'map.png');
        } else {
          canvas.toBlob(function(blob) {
            saveAs(blob, 'map.png');
          });
        }
      });
    map.renderSync();
}

$(document).keydown(function(event){
    if(event.keyCode == 46){
        if (feature && feature.get('isPlot') && !plot.plotDraw.isDrawing()) {
            plot.plotUtils.removeFeatures(feature);
            plot.plotEdit.deactivate();
        }
    }  
});